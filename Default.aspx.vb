﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class _Default
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsClef As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim SiOK As Boolean = False
        Dim UtiV3 As Virtualia.Version3.Utilisateur.UtilisateurV3 = Nothing
        Dim NomCnx As String = ""
        Dim PrenomCnx As String = ""
        Dim InitialesCnx As String = ""
        Dim FiltreEta As String = ""
        Dim FiltreV3 As String = ""
        Dim ContexteUti As Virtualia.Net.ServiceSessions.ContexteUtilisateurType
        Dim SessionUtlisateur As Virtualia.Net.Session.ObjetSession
        Dim CacheCookie As Virtualia.Net.VCaches.CacheIdentification = Nothing
        Dim Url As String = ""
        Dim ChaineV3 As String = ""

        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        WsClef = Strings.Replace(WebFct.PointeurGlobal.VirModele.InstanceProduit.ClefProduit, "-", "")

        '************* Version 3.7 *******************************************************************************
        ChaineV3 = Request.Form("ClefV4") 'Issu du POST - Intranet RH
        If ChaineV3 Is Nothing OrElse ChaineV3 = "" Then
            ChaineV3 = Request.QueryString("ClefV4") 'Issu du GET - VirtualiaRHV2.exe
        End If
        '** Uniquement pour test V3
        Dim ObjetTest As New Virtualia.Net.Session.ClefV3Connexion("")
        ChaineV3 = ObjetTest.Chaine_Test("Virtualia", "DRH", 0)
        '**
        '******************** Uniquement pour tests ********************************
        'Call InitialiserTestsV4()
        '********************************************************************************
        If ChaineV3 IsNot Nothing AndAlso ChaineV3 <> "" Then
            Dim ObjetClefV3 As Virtualia.Net.Session.ClefV3Connexion = Nothing
            Dim ChainedeConnexion As String = Session.SessionID
            Try
                ChainedeConnexion &= VI.PointVirgule & System.Net.Dns.GetHostEntry(Request.UserHostAddress).HostName
            Catch ex As Exception
                ChainedeConnexion &= VI.PointVirgule & Server.HtmlDecode(Request.UserHostName)
            End Try
            ChainedeConnexion &= VI.PointVirgule & Server.HtmlEncode(Request.UserHostAddress)
            ChainedeConnexion &= VI.PointVirgule & Server.HtmlEncode(Request.LogonUserIdentity.Name)
            ObjetClefV3 = New Virtualia.Net.Session.ClefV3Connexion(ChaineV3)
            If ObjetClefV3.SiAccesOK() = False Then
                Response.Redirect(WebFct.PointeurGlobal.UrlDestination("", 0, ""), True)
                Exit Sub
            End If
            'If ObjetClefV3.SiOuvrirSession(WebFct.PointeurGlobal, ChainedeConnexion) = False Then
            '    Response.Redirect(WebFct.PointeurGlobal.UrlDestination("", 0, ""), True)
            '    Exit Sub
            'End If
            CacheCookie = WebFct.LireCookie(Me)
            If CacheCookie IsNot Nothing Then
                CacheCookie.NumeroSession = Session.SessionID
            End If
            'WebFct.EcrireCookie(Me, CacheCookie)
            NomCnx = ObjetClefV3.ObjetUti_CnxV3.Nom
            UtiV3 = ObjetClefV3.ObjetUti_CnxV3
        Else
            CacheCookie = WebFct.LireCookie(Me)
            If CacheCookie IsNot Nothing AndAlso CacheCookie.NumeroSession = Session.SessionID Then
                NomCnx = WebFct.PointeurGlobal.IDUtilisateur(Session.SessionID)
            End If
            If NomCnx = "" Then
                Response.Redirect(WebFct.PointeurGlobal.UrlDestination("", 0, ""), True)
                Exit Sub
            End If
            If CacheCookie IsNot Nothing AndAlso CacheCookie.ModedeConnexion = "V3" Then
                Dim IdentificationV3 As Virtualia.Version3.Utilisateur.UtilisateurV3
                IdentificationV3 = New Virtualia.Version3.Utilisateur.UtilisateurV3(System.Configuration.ConfigurationManager.AppSettings("RepertoireUtilisateurV3"), NomCnx)
                If IdentificationV3 IsNot Nothing AndAlso IdentificationV3.Nom.ToUpper = NomCnx.ToUpper Then
                    UtiV3 = IdentificationV3
                End If
            Else
                ContexteUti = WebFct.PointeurGlobal.ContexteUtilisateur(Session.SessionID)
                FiltreEta = ContexteUti.Filtre_Etablissement
            End If
        End If
        '** V3
        If UtiV3 IsNot Nothing Then
            FiltreEta = UtiV3.FiltreEtablissementr
            FiltreV3 = FiltreUtiVersion3(UtiV3)
            PrenomCnx = UtiV3.Prenom
            InitialesCnx = UtiV3.Initiales
            'For Each OutilV3 In UtiV3.Database.ListeOutils
            '    If OutilV3.Numero > 13 Then
            '        Exit For
            '    End If
            '    If OutilV3.Autorisation = "1" And OutilV3.Numero <> 11 Then
            '        SiAccesModuleSeulV3 = False
            '        Exit For
            '    End If
            'Next
        End If
        '**

        If Session.Item("IDVirtualia") Is Nothing Then
            Session.Add("IDVirtualia", Session.SessionID)
        End If

        SessionUtlisateur = WebFct.PointeurGlobal.ItemSessionModule(Session.SessionID)
        If SessionUtlisateur Is Nothing Then
            SessionUtlisateur = WebFct.PointeurGlobal.AjouterUneSessionModule(Session.SessionID, NomCnx, FiltreEta, FiltreV3)
            SessionUtlisateur.V_PrenomdeConnexion = PrenomCnx
            SessionUtlisateur.V_InitialesdeConnexion = InitialesCnx
        End If
        SessionUtlisateur.V_NomdUtilisateurSgbd = NomCnx
        Try
            SessionUtlisateur.ID_Machine = System.Net.Dns.GetHostEntry(Request.UserHostAddress).HostName
        Catch ex As Exception
            SessionUtlisateur.ID_Machine = Server.HtmlDecode(Request.UserHostName)
        End Try
        SessionUtlisateur.ID_AdresseIP = Request.UserHostAddress
        SessionUtlisateur.ID_LogonIdentite = Request.LogonUserIdentity.Name

        If CType(WebFct.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID) Is Nothing Then
            Exit Sub
        End If
        Call EcrireLog("Module Données personnelles - " & SessionUtlisateur.V_NomdeConnexion & " - " & Session.SessionID)
        Url = "~/Fenetres/Commun/FrmAttente.aspx"
        Response.Redirect(Url & "?Index=7&Lien=~/Fenetres/Module/FrmInfosPersonnelles.aspx")
    End Sub

    Private Function VerificationdelAppel(ByVal Valeur As Integer) As Boolean
        Dim Verif As Integer = Day(Now) + Month(Now) + Hour(Now) + Year(Now) - 1951
        If Verif = Valeur Or Verif = Valeur + 1 Then
            Return True
        End If
        Return False
    End Function

    Private Sub InitialiserTestsV4()
        Dim ClientSession As Virtualia.Net.WebService.ServiceWcfSessions
        Dim CacheCookie As Virtualia.Net.VCaches.CacheIdentification = Nothing

        ClientSession = New Virtualia.Net.WebService.ServiceWcfSessions
        If ClientSession.ID_Generique("Initialisation") = False Then
            Response.Redirect(WebFct.PointeurGlobal.UrlDestination("", 0, ""), True)
            Exit Sub
        End If
        If ClientSession.ID_Generique(Session.SessionID) = False Then
            Response.Redirect(WebFct.PointeurGlobal.UrlDestination("", 0, ""), True)
            Exit Sub
        End If
        CacheCookie = WebFct.LireCookie(Me)
        CacheCookie.NumeroSession = Session.SessionID
        WebFct.EcrireCookie(Me, CacheCookie)
    End Sub

    Private ReadOnly Property FiltreUtiVersion3(ByVal UtilisateurV3 As Virtualia.Version3.Utilisateur.UtilisateurV3) As String
        Get
            If UtilisateurV3.ListeDatabase(0).Liste_Filtre_Contenu Is Nothing OrElse UtilisateurV3.ListeDatabase(0).Liste_Filtre_Contenu.Count = 0 Then
                Return ""
            End If
            If UtilisateurV3.ListeDatabase(0).Liste_Filtre_Type(0) IsNot Nothing AndAlso UtilisateurV3.ListeDatabase(0).Liste_Filtre_Type(0) = "Sql" Then
                Return UtilisateurV3.ListeDatabase(0).Liste_Filtre_Contenu(0)
            End If
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim ChaineSql As String
            Dim Valeurs As System.Text.StringBuilder
            Dim IndiceK As Integer
            Dim DateFin = System.DateTime.Now.ToShortDateString

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WebFct.PointeurGlobal.VirModele, WebFct.PointeurGlobal.VirInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", DateFin, VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = True
            Constructeur.SiHistoriquedeSituation = False
            Constructeur.SiForcerClauseDistinct = False
            Constructeur.LettreAlias = "fi"
            Constructeur.NoInfoSelection(0, CInt(UtilisateurV3.ListeDatabase(0).Filtre_Objet)) = CInt(UtilisateurV3.ListeDatabase(0).Filtre_Information)
            Valeurs = New System.Text.StringBuilder
            For IndiceK = 0 To UtilisateurV3.ListeDatabase(0).Liste_Filtre_Contenu.Count - 1
                Valeurs.Append(UtilisateurV3.ListeDatabase(0).Liste_Filtre_Contenu(IndiceK) & VI.PointVirgule)
            Next IndiceK
            Select Case UtilisateurV3.ListeDatabase(0).Filtre_Sens
                Case "0"
                    Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Valeurs.ToString
                Case "1"
                    Constructeur.ValeuraComparer(0, VI.Operateurs.ET, VI.Operateurs.Egalite, False) = Valeurs.ToString
            End Select

            ChaineSql = Constructeur.OrdreSqlDynamique
            Constructeur = Nothing
            Return ChaineSql
        End Get
    End Property

    Private Sub EcrireLog(ByVal Msg As String)
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim NomLog As String
        Dim SysFicLog As String = "WcfConnexions.log"
        Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)

        NomLog = Virtualia.Systeme.Constantes.DossierVirtualiaService("Logs") & SysFicLog
        FicStream = New System.IO.FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
        FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)
        FicWriter.WriteLine(Format(System.DateTime.Now, "g") & Space(1) & Msg)
        FicWriter.Flush()
        FicWriter.Close()
    End Sub
End Class