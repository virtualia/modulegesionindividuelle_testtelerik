﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PerLangues.ascx.vb" Inherits="Virtualia.Net.PerLangues" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Standards/VReferentiel.ascx" tagname="VReference" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>

<style type="text/css">
    .Std_Etiquette
    {  
        background-color:#B0E0D7;
        color:#142425;
        border-color:#B0E0D7;
        border-style:outset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:5px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        height:20px;
    }
     .Std_Donnee
    {  
        background-color:white;
        color:black;
        border-color:#B0E0D7;
        border-style:inset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:1px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        height:16px;
    }
</style>
<asp:Table ID="CadrePerLangue" runat="server" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545" Width="500px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitre" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiDate" runat="server" CssClass="Std_Etiquette" Width="125px" Text="Date Appréciation" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiLangue" runat="server" CssClass="Std_Etiquette" Width="250px" Text="Langue" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiNiveau" runat="server" CssClass="Std_Etiquette" Width="160px" Text="Niveau" >
                        </asp:Label>
                    </asp:TableCell>             
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Ligne_01" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL1_Date" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="120" V_Information="0" V_SiDonneeDico="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:10px" DonTabIndex="31" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL1_02" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="120" V_Information="2" V_SiDonneeDico="true"
                                       DonWidth="250px" DonTabIndex="32"/>
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL1_03" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="120" V_Information="3" V_SiDonneeDico="true"
                                       DonWidth="160px" DonTabIndex="33"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Ligne_02" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL2_Date" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="120" V_Information="0" V_SiDonneeDico="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:10px" DonTabIndex="34" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL2_02" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="120" V_Information="2" V_SiDonneeDico="true"
                                       DonWidth="250px" DonTabIndex="35"/>
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL2_03" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="120" V_Information="3" V_SiDonneeDico="true"
                                       DonWidth="160px" DonTabIndex="36"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Ligne_03" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL3_Date" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="120" V_Information="0" V_SiDonneeDico="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:10px" DonTabIndex="37" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL3_02" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="120" V_Information="2" V_SiDonneeDico="true"
                                       DonWidth="250px" DonTabIndex="38"/>
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL3_03" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="120" V_Information="3" V_SiDonneeDico="true"
                                       DonWidth="160px" DonTabIndex="39"/>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:Table ID="CadreRefLangue" runat="server">
     <asp:TableRow>
         <asp:TableCell ID="CellReference" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupReferentiel" runat="server" TargetControlID="HPopupRef" PopupControlID="PanelRefPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelRefPopup" runat="server">
                <Virtualia:VReference ID="RefVirtualia" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupRef" runat="server" Value="0" />
        </asp:TableCell>
     </asp:TableRow>       
</asp:Table>

