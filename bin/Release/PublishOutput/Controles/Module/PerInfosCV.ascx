﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PerInfosCV.ascx.vb" Inherits="Virtualia.Net.PerInfosCV" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Module/PerDiplomes.ascx" tagname="VPerDiplome" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Module/PerLangues.ascx" tagname="VPerLangue" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Module/PerBrevetsPro.ascx" tagname="VPerBrevet" tagprefix="Virtualia" %>

<style type="text/css">
    .Gen_Titre
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
     .Gen_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:5px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
      .Gen_EtiInfo
    {  
        background-color:#E2FDF7;
        color:#124545;
        border-style:unset;
        border-width:1px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:5px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:5px;
        word-wrap:normal;
        height:18px;
        width:710px;
    }
</style>
<asp:Table ID="CadreGeneralCV" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
      <asp:TableCell>
        <asp:Table ID="CadreTitreGeneral" runat="server" Height="50px" CellPadding="0" Width="750px" 
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#124545">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="EtiTitre" runat="server" Text="DIPLOMES ET QUALIFICATIONS" Height="25px" Width="746px" Cssclass="Gen_Titre">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
        </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfosCV" runat="server" BackColor="#E2F5F1" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiPerDiplome" runat="server" Cssclass="Gen_EtiInfo"
                            Text="DIPLOMES" Tooltip="" >
                        </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VPerDiplome ID="CadreDiplome" runat="server"></Virtualia:VPerDiplome>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="25px">
                        <asp:Label ID="EtiUploadDiplome" runat="server" Cssclass="Gen_EtiInfo"
                            Text="Pièces justificatives : Copie des diplômes" 
                            Tooltip="Vous pouvez déposer ici les documents justifiant de l'acquisition des diplômes." >
                        </asp:Label>         
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" BackColor="WhiteSmoke">
                        <ajaxToolkit:AjaxFileUpload ID="AjaxUploadDiplome" ThrobberID="myThrobber" ContextKeys="Virtualia"
                            AllowedFileTypes=""
                            MaximumNumberOfFiles="5" runat="server" Width="730px" BorderWidth="2px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiPerLangue" runat="server" Cssclass="Gen_EtiInfo"
                            Text="LANGUES ETRANGERES" Tooltip="" >
                        </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VPerLangue ID="CadreLangue" runat="server"></Virtualia:VPerLangue>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="25px">
                        <asp:Label ID="EtiUploadLangue" runat="server" Cssclass="Gen_EtiInfo"
                            Text="Pièces justificatives" 
                            Tooltip="Vous pouvez déposer ici les documents justifiant du niveau évalué dans la langue étrangère." >
                        </asp:Label>         
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" BackColor="WhiteSmoke">
                        <ajaxToolkit:AjaxFileUpload ID="AjaxFileUploadLangue" ThrobberID="myThrobber" ContextKeys="Virtualia"
                            AllowedFileTypes=""
                            MaximumNumberOfFiles="3" runat="server" Width="730px" BorderWidth="2px" />
                    </asp:TableCell>
                </asp:TableRow>     
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiPerQualification" runat="server" Cssclass="Gen_EtiInfo"
                            Text="QUALIFICATIONS PROFESSIONNELLES" Tooltip="" >
                        </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VPerBrevet ID="CadreBrevetPro" runat="server"></Virtualia:VPerBrevet>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="25px">
                        <asp:Label ID="EtiUploadBrevet" runat="server" Cssclass="Gen_EtiInfo"
                            Text="Pièces justificatives" 
                            Tooltip="Vous pouvez déposer ici les documents justifiant de la qualification professionnelle." >
                        </asp:Label>         
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" BackColor="WhiteSmoke">
                        <ajaxToolkit:AjaxFileUpload ID="AjaxFileUploadBrevet" ThrobberID="myThrobber" ContextKeys="Virtualia"
                            AllowedFileTypes=""
                            MaximumNumberOfFiles="5" runat="server" Width="730px" BorderWidth="2px" />
                    </asp:TableCell>
                </asp:TableRow>          
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
