﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PerInfosGenerales.ascx.vb" Inherits="Virtualia.Net.PerInfosGenerales" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Standards/VReferentiel.ascx" tagname="VReference" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VSixBoutonRadio.ascx" tagname="VSixBoutonRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Module/PerEnfants.ascx" tagname="VPerEnfant" tagprefix="Virtualia" %>

<style type="text/css">
    .Gen_Titre
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
     .Gen_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:5px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
      .Gen_EtiInfo
    {  
        background-color:#E2FDF7;
        color:#124545;
        border-style:unset;
        border-width:1px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:5px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:5px;
        word-wrap:normal;
        height:18px;
        width:710px;
    }
</style>
<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
      <asp:TableCell>
        <asp:Table ID="CadreTitreGeneral" runat="server" Height="50px" CellPadding="0" Width="750px" 
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#124545">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="EtiTitre" runat="server" Text="DONNEES PERSONNELLES" Height="25px" Width="746px" Cssclass="Gen_Titre">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
        </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreEtatCivil" runat="server" BackColor="#E2F5F1" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiIdentite" runat="server" Cssclass="Gen_EtiInfo"
                           Text="ETAT-CIVIL" Tooltip="" >
                        </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreQualite" runat="server" Height="25px" Width="720px" CellPadding="0" CellSpacing="0">
                            <asp:TableRow> 
                                 <asp:TableCell Width="510px" HorizontalAlign="Left">
                                    <Virtualia:VTrioHorizontalRadio ID="RadioHA01" runat="server" V_PointdeVue="1" V_Objet="1" V_Information="1" V_SiDonneeDico="true"
                                           V_SiAutoPostBack="true" V_Groupe="Qualite" V_TabIndex="1"
                                           RadioGaucheWidth="130px" RadioCentreWidth="130px" RadioDroiteVisible="false"
                                           RadioGaucheText="Monsieur" RadioCentreText="Madame" RadioDroiteText=""
                                           RadioGaucheStyle="margin-left: 5px;" />
                                 </asp:TableCell>
                             </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreNomPrenom" runat="server" Height="50px" Width="720px" CellPadding="0" CellSpacing="0">
                             <asp:TableRow>
                                <asp:TableCell Width="710px" HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHA02" runat="server" EtiText="Nom d'usage ou nom usuel" 
                                       EtiTooltip="Il s'agit du nom d'usage. Le plus souvent il s'agit du nom de naissance ou du nom du conjoint pour les personnes mariées"
                                       V_PointdeVue="1" V_Objet="1" V_Information="2" V_SiDonneeDico="true"
                                       EtiWidth="320px" DonWidth="280px" DonTabIndex="2"/>
                                </asp:TableCell>
                             </asp:TableRow>
                             <asp:TableRow>
                                <asp:TableCell Width="710px" HorizontalAlign="Left" ColumnSpan="2">
                                     <Virtualia:VCoupleEtiDonnee ID="InfoHA13" runat="server" EtiText="Nom de naissance si différent du nom d'usage"
                                       EtiTooltip="Encore appelé nom patronymique ou nom de jeune fille"
                                       V_PointdeVue="1" V_Objet="1" V_Information="13" V_SiDonneeDico="true"
                                       EtiWidth="320px" DonWidth="280px" DonTabIndex="3"/> 
                                </asp:TableCell>
                             </asp:TableRow>
                             <asp:TableRow>
                                <asp:TableCell Width="330px" HorizontalAlign="Left">
                                     <Virtualia:VCoupleEtiDonnee ID="InfoHA03" runat="server" EtiText="Prénom d'usage"
                                       V_PointdeVue="1" V_Objet="1" V_Information="3" V_SiDonneeDico="true"
                                       EtiWidth="120px" DonWidth="200px" DonTabIndex="4"/>
                                </asp:TableCell>
                                <asp:TableCell Width="380px" HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHA14" runat="server" EtiText="Autres prénoms"
                                       V_PointdeVue="1" V_Objet="1" V_Information="14" V_SiDonneeDico="true"
                                       EtiWidth="120px" DonWidth="250px" DonTabIndex="5"/>  
                                </asp:TableCell>
                              </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreNaissance" runat="server" Height="40px" Width="720px" CellPadding="0" CellSpacing="0">
                          <asp:TableRow VerticalAlign="Middle">
                            <asp:TableCell Width="200px" HorizontalAlign="Left">
                                <Virtualia:VCoupleEtiDate ID="InfoDA04" runat="server" TypeCalendrier="Standard" EtiWidth="100px"
                                           V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="1" V_Information="4" DonTabIndex="6"/>
                            </asp:TableCell>
                            <asp:TableCell Width="200px" HorizontalAlign="Left">
                                <Virtualia:VCoupleEtiDonnee ID="InfoHA05" runat="server"
                                       V_PointdeVue="1" V_Objet="1" V_Information="5" V_SiDonneeDico="true"
                                       EtiWidth="20px" DonWidth="150px" EtiText="à" DonTabIndex="7"
                                       EtiStyle="margin-left: 1px;"/>
                            </asp:TableCell>
                            <asp:TableCell Width="300px" HorizontalAlign="Left">
                                <Virtualia:VDuoEtiquetteCommande ID="DontabA06" runat="server" 
                                       V_PointdeVue="1" V_Objet="1" V_Information="6" V_SiDonneeDico="true"
                                       EtiWidth="100px" DonWidth="200px" DonTabIndex="8"
                                       EtiStyle="margin-left: 1px;"/>
                            </asp:TableCell>
                            <asp:TableCell Width="20px"></asp:TableCell>
                          </asp:TableRow>
                       </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreNIR" runat="server" Height="30px" Width="720px" CellPadding="0" CellSpacing="0">
                             <asp:TableRow VerticalAlign="Middle">
                                <asp:TableCell Width="372px" HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHA11" runat="server"
                                       V_PointdeVue="1" V_Objet="1" V_Information="11" V_SiDonneeDico="true"
                                       EtiWidth="250px" DonWidth="120px" DonTabIndex="9"/>
                                </asp:TableCell>
                                <asp:TableCell Width="350px" HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHA12" runat="server"
                                       V_PointdeVue="1" V_Objet="1" V_Information="12" V_SiDonneeDico="true"
                                       EtiWidth="30px" EtiText="Clé" DonWidth="20px" DonTabIndex="10"/> 
                                </asp:TableCell>
                             </asp:TableRow>
                        </asp:Table>                
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="25px">
                        <asp:Label ID="EtiUploadEtatCiv" runat="server" Cssclass="Gen_EtiInfo"
                       Text="Pièces justificatives : Copie de la carte d'identité et copie de la carte Vitale" 
                       Tooltip="Vous pouvez déposer ici les documents justifiant de l'état-civil." >
                    </asp:Label>         
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" BackColor="WhiteSmoke">
                        <ajaxToolkit:AjaxFileUpload ID="AjaxUploadEtatCiv" ThrobberID="myThrobber" ContextKeys="Virtualia"
                            AllowedFileTypes=""
                            MaximumNumberOfFiles="3" runat="server" Width="730px" BorderWidth="2px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="25px">
                        <asp:Label ID="EtiTitreSitFam" runat="server" Cssclass="Gen_EtiInfo"
                           Text="SITUATION DE FAMILLE" Tooltip="" >
                        </asp:Label>         
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="25px">
                        <asp:Label ID="EtiSitFam" runat="server" Cssclass="Gen_EtiInfo" Style="margin-left:5px"
                           Text="Vous êtes" Tooltip="" >
                        </asp:Label>         
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow> 
                    <asp:TableCell Width="665px" Height="30px" HorizontalAlign="Center" VerticalAlign="Middle">
                        <Virtualia:VSixBoutonRadio ID="RadioXA09" runat="server" V_PointdeVue="1" V_Objet="1" V_Information="9" V_SiDonneeDico="true"
                            V_SiAutoPostBack="true" V_Groupe="SitFam" V_TabIndex="11"
                            VRadioN1Width="110px" VRadioN2Width="110px" VRadioN3Width="110px" VRadioN4Width="110px" VRadioN5Width="110px" VRadioN6Width="110px"
                            VRadioN1Text="Célibataire" VRadioN2Text="Marié(e)" VRadioN3Text="Pacsé(e)" VRadioN4Text="Vie maritale" VRadioN5Text="Veuf(ve)" VRadioN6Text="Divorcé(e)"
                            VRadioN1Style="margin-left:1px;" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Height="25px">
                        <asp:Label ID="EtiConjoint" runat="server" Cssclass="Gen_EtiInfo" Style="margin-left:5px"
                           Text="Eventuellement, situation du conjoint" Tooltip="Si le conjoint est fonctionnaire, ces informations doivent être renseignées" >
                        </asp:Label>         
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreConjoint" runat="server" Width="710px" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545" CellPadding="0" CellSpacing="0">
                             <asp:TableRow>
                                <asp:TableCell Width="400px" HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHB01" runat="server" EtiText="Nom du conjoint" 
                                       EtiTooltip=""
                                       V_PointdeVue="1" V_Objet="2" V_Information="1" V_SiDonneeDico="true"
                                       EtiWidth="150px" DonWidth="240px" DonTabIndex="12"/>
                                </asp:TableCell>
                                <asp:TableCell Width="330px" HorizontalAlign="Left">
                                     <Virtualia:VCoupleEtiDonnee ID="InfoHB02" runat="server" EtiText="Prénom"
                                       V_PointdeVue="1" V_Objet="2" V_Information="2" V_SiDonneeDico="true"
                                       EtiWidth="120px" DonWidth="185px" DonTabIndex="13"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHB04" runat="server" EtiText="Employeur" 
                                       EtiTooltip=""
                                       V_PointdeVue="1" V_Objet="2" V_Information="4" V_SiDonneeDico="true"
                                       EtiWidth="150px" DonWidth="240px" DonTabIndex="14"/>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabB03" runat="server" EtiText="Profession"
                                       V_PointdeVue="1" V_Objet="2" V_Information="3" V_SiDonneeDico="true"
                                       EtiWidth="120px" DonWidth="190px" DonTabIndex="15" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCocheSimple ID="CocheB07" runat="server" V_PointdeVue="1" V_Objet="2" V_Information="7" V_SiDonneeDico="true" 
                                        V_Width="200px" V_Style="margin-left:5px" V_Text="Est fonctionnaire" V_TabIndex="16" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                     <asp:TableCell Width="372px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHC50" runat="server" V_SiDonneeDico="false" DonTabIndex="17"
                            EtiWidth="120px" DonWidth="60px" EtiText="Nombre d'enfants" />
                     </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <Virtualia:VPerEnfant ID="CadreEnfant" runat="server"></Virtualia:VPerEnfant>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiDomicile" runat="server" Cssclass="Gen_EtiInfo"
                           Text="ADRESSE DU DOMICILE" Tooltip="" >
                        </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                      <asp:Table ID="CadreDomicile" runat="server" Width="500px" CellPadding="0" CellSpacing="0" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545">
                        <asp:TableRow>
                             <asp:TableCell>
                                 <Virtualia:VCoupleEtiDonnee ID="InfoHD01" runat="server" 
                                    V_PointdeVue="1" V_Objet="4" V_Information="1" V_SiDonneeDico="true"
                                    DonWidth="300px" DonTabIndex="50"/>
                             </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                             <asp:TableCell>
                                 <Virtualia:VCoupleEtiDonnee ID="InfoHD02" runat="server"
                                    V_PointdeVue="1" V_Objet="4" V_Information="2" V_SiDonneeDico="true"
                                    DonWidth="300px" DonTabIndex="51"/>
                             </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                             <asp:TableCell>
                                 <Virtualia:VCoupleEtiDonnee ID="InfoHD03" runat="server"
                                    V_PointdeVue="1" V_Objet="4" V_Information="3" V_SiDonneeDico="true"
                                    DonWidth="300px" DonTabIndex="52"/>
                             </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table ID="CP" runat="server" Height="22px" Width="495px" CellPadding="0" CellSpacing="0">
                                    <asp:TableRow> 
                                         <asp:TableCell Width="145px" HorizontalAlign="Left">  
                                             <Virtualia:VCoupleEtiDonnee ID="InfoHD04" runat="server"
                                                V_PointdeVue="1" V_Objet="4" V_Information="4" V_SiDonneeDico="true"
                                                DonWidth="40px" EtiText="Code postal et ville" DonTabIndex="53"/>
                                         </asp:TableCell>
                                         <asp:TableCell>         
                                             <Virtualia:VCoupleEtiDonnee ID="InfoHD05" runat="server" 
                                                V_PointdeVue="1" V_Objet="4" V_Information="5" V_SiDonneeDico="true"
                                                EtiWidth="0px" EtiVisible="false" DonWidth="250px" DonTabIndex="54"/>
                                         </asp:TableCell>
                                    </asp:TableRow> 
                                 </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiRIB" runat="server" Cssclass="Gen_EtiInfo"
                           Text="RELEVE D'IDENTITE BANCAIRE - IBAN" Tooltip="" >
                        </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                     <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreIban" runat="server" CellPadding="0" CellSpacing="0" Width="500px" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHE07" runat="server" EtiText="IBAN"
                                       V_PointdeVue="1" V_Objet="5" V_Information="7" V_SiDonneeDico="true"
                                       EtiWidth="130px" DonWidth="350px" DonHeight="20px" DonTabIndex="55"
                                       EtiStyle="margin-top:2px" DonStyle="text-align: center"/> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                     <Virtualia:VCoupleEtiDonnee ID="InfoHE08" runat="server"
                                       V_PointdeVue="1" V_Objet="5" V_Information="8" V_SiDonneeDico="true"
                                       EtiWidth="130px" DonWidth="200px" DonTabIndex="56"
                                       EtiStyle="margin-left: 2px;" DonStyle="text-align: center"/> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                     <Virtualia:VCoupleEtiDonnee ID="InfoHE05" runat="server"
                                       V_PointdeVue="1" V_Objet="5" V_Information="5" V_SiDonneeDico="true"
                                       EtiWidth="220px" DonWidth="250px" DonTabIndex="57"
                                       EtiStyle="margin-left: 2px;" DonStyle="text-align: center"/> 
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                     </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="25px">
                        <asp:Label ID="EtiJustificatifRIB" runat="server" Cssclass="Gen_EtiInfo"
                       Text="Pièces justificatives : RIB - IBAN" 
                       Tooltip="Vous pouvez déposer ici les documents justifiant du compte bancaire." >
                    </asp:Label>         
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" BackColor="WhiteSmoke">
                        <ajaxToolkit:AjaxFileUpload ID="AjaxFileUploadIBAN" ThrobberID="myThrobber" ContextKeys="Virtualia"
                            AllowedFileTypes=""
                            MaximumNumberOfFiles="1" runat="server" Width="730px" BorderWidth="2px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiCoordonnees" runat="server" Cssclass="Gen_EtiInfo"
                           Text="COORDONNEES PERSONNELLES" Tooltip="" >
                        </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                     <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCoordonnee" runat="server" CellPadding="0" CellSpacing="0" Width="550px" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545">
                            <asp:TableRow>
                                <asp:TableCell>
                                     <Virtualia:VCoupleEtiDonnee ID="InfoHD06" runat="server"
                                        V_PointdeVue="1" V_Objet="4" V_Information="6" V_SiDonneeDico="true"
                                        DonWidth="150px" DonTabIndex="58"/>
                                 </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                     <Virtualia:VCoupleEtiDonnee ID="InfoHD11" runat="server"
                                        V_PointdeVue="1" V_Objet="4" V_Information="11" V_SiDonneeDico="true"
                                        DonWidth="150px" DonTabIndex="59"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHD10" runat="server"
                                        V_PointdeVue="1" V_Objet="4" V_Information="10" V_SiDonneeDico="true"
                                        DonWidth="350px" DonTabIndex="60"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                     </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiAprevenir" runat="server" Cssclass="Gen_EtiInfo"
                           Text="PERSONNE A PREVENIR" Tooltip="Personne à prévenir en cas d'accident" >
                        </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                     <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreAprevenir" runat="server" CellPadding="0" CellSpacing="0" Width="550px" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP01" runat="server" V_PointdeVue="1" V_Objet="30" V_Information="1" V_SiDonneeDico="true" DonWidth="300px" DonTabIndex="61" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP06" runat="server" V_PointdeVue="1" V_Objet="30" V_Information="6" V_SiDonneeDico="true" DonWidth="150px" DonTabIndex="62" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP10" runat="server" V_PointdeVue="1" V_Objet="30" V_Information="10" V_SiDonneeDico="true" DonWidth="350px" DonTabIndex="63" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" Height="25px">
                                    <asp:Label ID="EtiAdrAPrevenir" runat="server" Cssclass="Gen_EtiInfo"
                                       Text="Eventuellement indiquer une adresse" Tooltip="" >
                                    </asp:Label>         
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP02" runat="server" V_PointdeVue="1" V_Objet="30" V_Information="2" V_SiDonneeDico="true" DonWidth="300px" DonTabIndex="64" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP03" runat="server" V_PointdeVue="1" V_Objet="30" V_Information="3" V_SiDonneeDico="true" DonWidth="300px" DonTabIndex="65" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="CPAPrevenir" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHP04" runat="server" V_PointdeVue="1" V_Objet="30" V_Information="4" V_SiDonneeDico="true" DonWidth="40px" EtiText="Code postal et ville" DonTabIndex="66" />
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHP05" runat="server" V_PointdeVue="1" V_Objet="30" V_Information="5" V_SiDonneeDico="true" EtiWidth="0px" EtiVisible="false" DonWidth="250px" DonTabIndex="67" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow> 
    <asp:TableRow>
         <asp:TableCell ID="CellReference" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupReferentiel" runat="server" TargetControlID="HPopupRef" PopupControlID="PanelRefPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelRefPopup" runat="server">
                <Virtualia:VReference ID="RefVirtualia" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupRef" runat="server" Value="0" />
        </asp:TableCell>
     </asp:TableRow>
 </asp:Table>