﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PerAffectationPublic.ascx.vb" Inherits="Virtualia.Net.PerAffectationPublic" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Standards/VReferentiel.ascx" tagname="VReference" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>

<style type="text/css">
    .Gen_Titre
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
     .Gen_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:5px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
      .Gen_EtiInfo
    {  
        background-color:#E2FDF7;
        color:#124545;
        border-style:unset;
        border-width:1px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:5px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:5px;
        word-wrap:normal;
        height:18px;
        width:710px;
    }
</style>
<asp:Table ID="CadreGeneralAff" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
      <asp:TableCell>
        <asp:Table ID="CadreTitreGeneral" runat="server" Height="50px" CellPadding="0" Width="750px" 
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#124545">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="EtiTitre" runat="server" Text="AFFECTATIONS" Height="25px" Width="746px" Cssclass="Gen_Titre">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
        </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfosAff" runat="server" BackColor="#E2F5F1" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiPerEtablissement" runat="server" Cssclass="Gen_EtiInfo"
                            Text="ETABLISSEMENT" Tooltip="" >
                        </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreEtablissement" runat="server" Width="500px" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545" CellPadding="0" CellSpacing="0">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" Width="250px">
                                    <Virtualia:VCoupleEtiDate ID="InfoDT00" runat="server" TypeCalendrier="Standard" EtiWidth="120px"
                                           V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="26" V_Information="0" DonTabIndex="1"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabT01" runat="server"
                                       V_PointdeVue="1" V_Objet="26" V_Information="1" V_SiDonneeDico="true"
                                       DonTabIndex="2"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabT10" runat="server"
                                       V_PointdeVue="1" V_Objet="26" V_Information="10" V_SiDonneeDico="true"
                                       DonTabIndex="3"/> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDate ID="InfoDT12" runat="server" TypeCalendrier="Standard" SiDateFin="true" EtiWidth="170px"
                                           V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="26" V_Information="12" DonTabIndex="4"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabT11" runat="server" V_PointdeVue="1" V_Objet="26" V_Information="11" V_SiDonneeDico="true"
                                       DonTabIndex="5"/> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center"> 
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoVT13" runat="server"
                                       V_PointdeVue="1" V_Objet="26" V_Information="13" V_SiDonneeDico="true"
                                       EtiWidth="402px" EtiHeight="35px" DonWidth="400px" DonTabIndex="6"
                                       DonTextMode="true" DonHeight="70px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiPerFonction" runat="server" Cssclass="Gen_EtiInfo"
                            Text="FONCTION" Tooltip="" >
                        </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreFonction" runat="server" Width="600px" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545" CellPadding="0" CellSpacing="0">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" Width="300px">
                                    <Virtualia:VCoupleEtiDate ID="InfoDF00" runat="server" TypeCalendrier="Standard" EtiWidth="170px"
                                                V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="17" V_Information="0" DonTabIndex="1"/>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" Width="300px">
                                    <Virtualia:VCoupleEtiDate ID="InfoDF07" runat="server" TypeCalendrier="Standard" SiDateFin="true" EtiWidth="80px" EtiStyle="margin-left:2px"
                                                V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="17" V_Information="7" DonTabIndex="2"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabF01" runat="server"
                                        V_PointdeVue="1" V_Objet="17" V_Information="1" V_SiDonneeDico="true"
                                        DonTabIndex="3" DonWidth="490px"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabF02" runat="server"
                                        V_PointdeVue="1" V_Objet="17" V_Information="2" V_SiDonneeDico="true"
                                        DonTabIndex="4" DonWidth="490px"/> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabF03" runat="server"
                                        V_PointdeVue="1" V_Objet="17" V_Information="3" V_SiDonneeDico="true"
                                        DonTabIndex="5" DonWidth="490px"/> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabF04" runat="server"
                                        V_PointdeVue="1" V_Objet="17" V_Information="4" V_SiDonneeDico="true"
                                        DonTabIndex="6" DonWidth="490px"/> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabF14" runat="server"
                                        V_PointdeVue="1" V_Objet="17" V_Information="14" V_SiDonneeDico="true"
                                        DonTabIndex="7" DonWidth="490px"/> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabF15" runat="server"
                                        V_PointdeVue="1" V_Objet="17" V_Information="15" V_SiDonneeDico="true"
                                        DonTabIndex="8" DonWidth="490px"/> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Height="40px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiEmploi" runat="server" Height="20px" Width="350px"
                                        Text="Emploi / Fonction / Poste"
                                        BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabF05" runat="server"
                                        V_PointdeVue="1" V_Objet="17" V_Information="5" V_SiDonneeDico="true"
                                        DonTabIndex="9" DonWidth="490px"/> 
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiPerAdrPro" runat="server" Cssclass="Gen_EtiInfo"
                            Text="COORDONNEES PROFESSIONNELLES" Tooltip="" >
                        </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreAdrPro" runat="server" Width="600px" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545" CellPadding="0" CellSpacing="0">
                             <asp:TableRow> 
                                <asp:TableCell ColumnSpan="2" Width="600px" HorizontalAlign="Left"> 
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP07" runat="server"
                                        V_PointdeVue="1" V_Objet="24" V_Information="7" V_SiDonneeDico="true"
                                        EtiWidth="200px" DonWidth="500px" DonTabIndex="1"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow> 
                                <asp:TableCell ColumnSpan="2" Width="600px" HorizontalAlign="Left"> 
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP11" runat="server"
                                        V_PointdeVue="1" V_Objet="24" V_Information="11" V_SiDonneeDico="true"
                                        EtiWidth="200px" DonWidth="200px" DonTabIndex="2"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow> 
                                <asp:TableCell Width="350px" HorizontalAlign="Left">          
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP02" runat="server"
                                        V_PointdeVue="1" V_Objet="24" V_Information="2" V_SiDonneeDico="true"
                                        EtiWidth="200px" DonWidth="200px" DonTabIndex="3"/>
                                </asp:TableCell>
                                <asp:TableCell Width="400px" HorizontalAlign="Left"> 
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP08" runat="server"
                                        V_PointdeVue="1" V_Objet="24" V_Information="8" V_SiDonneeDico="true"
                                        EtiWidth="100px" DonWidth="200px" DonTabIndex="4"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow> 
                                <asp:TableCell ColumnSpan="2" Width="350px" HorizontalAlign="Left">          
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP04" runat="server"
                                        V_PointdeVue="1" V_Objet="24" V_Information="4" V_SiDonneeDico="true"
                                        EtiWidth="200px" DonWidth="200px" DonTabIndex="5"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                              <asp:TableCell ColumnSpan="2">
                                  <asp:Table ID="Bureau" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                                    <asp:TableRow>
                                        <asp:TableCell ColumnSpan="2" Width="375px" HorizontalAlign="Center">
                                            <asp:Label ID="EtiAdresseBureau" runat="server" Text="Adresse des bureaux"
                                              Height="20px" Width="300px"
                                              BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                              BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true"
                                              Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                              style="margin-top: 20px; margin-left: 4px; margin-bottom: 2px; text-indent: 5px; text-align: center">
                                            </asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell ColumnSpan="2" Width="375px" HorizontalAlign="Left">
                                            <Virtualia:VDuoEtiquetteCommande ID="DontabP09" runat="server" 
                                              V_PointdeVue="1" V_Objet="24" V_Information="9" V_SiDonneeDico="true"
                                              EtiWidth="120px" DonWidth="510px" DonTabIndex="12"/> 
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow> 
                                        <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">  
                                            <Virtualia:VCoupleEtiDonnee ID="InfoHP01" runat="server"
                                              V_PointdeVue="1" V_Objet="24" V_Information="1" V_SiDonneeDico="true"
                                              EtiWidth="200px" DonWidth="420px" DonTabIndex="13"/>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow> 
                                        <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">          
                                            <Virtualia:VCoupleEtiDonnee ID="InfoHP10" runat="server" 
                                              V_PointdeVue="1" V_Objet="24" V_Information="10" V_SiDonneeDico="true"
                                              EtiWidth="200px" DonWidth="420px" DonTabIndex="14"/>
                                        </asp:TableCell>
                                    </asp:TableRow> 
                                    <asp:TableRow> 
                                        <asp:TableCell Width="375px" HorizontalAlign="Left">  
                                            <Virtualia:VCoupleEtiDonnee ID="InfoHP12" runat="server"
                                              V_PointdeVue="1" V_Objet="24" V_Information="12" V_SiDonneeDico="true"
                                              EtiWidth="120px" DonWidth="120px" DonTabIndex="15"/>
                                        </asp:TableCell>
                                        <asp:TableCell Width="375px" HorizontalAlign="Left">          
                                            <Virtualia:VCoupleEtiDonnee ID="InfoHP13" runat="server" 
                                              V_PointdeVue="1" V_Objet="24" V_Information="13" V_SiDonneeDico="true"
                                              EtiWidth="120px" DonWidth="125px" DonTabIndex="16"/>
                                        </asp:TableCell>
                                    </asp:TableRow> 
                                    <asp:TableRow>
                                        <asp:TableCell ColumnSpan="2" Width="375px" HorizontalAlign="Left">
                                            <Virtualia:VDuoEtiquetteCommande ID="DontabP14" runat="server" 
                                              V_PointdeVue="1" V_Objet="24" V_Information="14" V_SiDonneeDico="true"
                                              EtiWidth="120px" DonWidth="220px" DonTabIndex="17"/> 
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Height="12px" Columnspan="2"></asp:TableCell>
                                    </asp:TableRow>
                                  </asp:Table>
                              </asp:TableCell>
                            </asp:TableRow>   
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
         </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
         <asp:TableCell ID="CellReference" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupReferentiel" runat="server" TargetControlID="HPopupRef" PopupControlID="PanelRefPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelRefPopup" runat="server">
                <Virtualia:VReference ID="RefVirtualia" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupRef" runat="server" Value="0" />
        </asp:TableCell>
     </asp:TableRow>
</asp:Table>
