﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PerDiplomes.ascx.vb" Inherits="Virtualia.Net.PerDiplomes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Standards/VReferentiel.ascx" tagname="VReference" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>

<style type="text/css">
    .Std_Etiquette
    {  
        background-color:#B0E0D7;
        color:#142425;
        border-color:#B0E0D7;
        border-style:outset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:5px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        height:20px;
    }
     .Std_Donnee
    {  
        background-color:white;
        color:black;
        border-color:#B0E0D7;
        border-style:inset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:1px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        height:16px;
    }
</style>
<asp:Table ID="CadrePerDiplome" runat="server" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545" Width="710px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitre" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiDate" runat="server" CssClass="Std_Etiquette" Width="125px" Text="Date du diplôme" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiDiplome" runat="server" CssClass="Std_Etiquette" Width="250px" Text="Diplôme" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiNiveau" runat="server" CssClass="Std_Etiquette" Width="80px" Text="Niveau" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiSpecialite" runat="server" CssClass="Std_Etiquette" Width="240px" Text="Specialité" >
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Ligne_01" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL1_Date" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="6" V_Information="0" V_SiDonneeDico="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:10px" DonTabIndex="1" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL1_01" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="6" V_Information="1" V_SiDonneeDico="true"
                                       DonWidth="250px" DonTabIndex="2"/>
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL1_02" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="6" V_Information="2" V_SiDonneeDico="true"
                                       DonWidth="80px" DonTabIndex="3"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL1_03" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="6" V_Information="3" V_SiDonneeDico="true"
                                       DonWidth="240px" DonTabIndex="4"/>
                     </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Ligne_02" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL2_Date" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="6" V_Information="0" V_SiDonneeDico="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:10px" DonTabIndex="5" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL2_01" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="6" V_Information="1" V_SiDonneeDico="true"
                                       DonWidth="250px" DonTabIndex="6"/>
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL2_02" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="6" V_Information="2" V_SiDonneeDico="true"
                                       DonWidth="80px" DonTabIndex="7"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL2_03" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="6" V_Information="3" V_SiDonneeDico="true"
                                       DonWidth="240px" DonTabIndex="8"/>
                     </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Ligne_03" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL3_Date" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="6" V_Information="0" V_SiDonneeDico="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:10px" DonTabIndex="9" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL3_01" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="6" V_Information="1" V_SiDonneeDico="true"
                                       DonWidth="250px" DonTabIndex="10"/>
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL3_02" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="6" V_Information="2" V_SiDonneeDico="true"
                                       DonWidth="80px" DonTabIndex="11"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL3_03" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="6" V_Information="3" V_SiDonneeDico="true"
                                       DonWidth="240px" DonTabIndex="12"/>
                     </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Ligne_04" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL4_Date" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="6" V_Information="0" V_SiDonneeDico="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:10px" DonTabIndex="13" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL4_01" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="6" V_Information="1" V_SiDonneeDico="true"
                                       DonWidth="250px" DonTabIndex="14"/>
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL4_02" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="6" V_Information="2" V_SiDonneeDico="true"
                                       DonWidth="80px" DonTabIndex="15"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL4_03" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="6" V_Information="3" V_SiDonneeDico="true"
                                       DonWidth="240px" DonTabIndex="16"/>
                     </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Ligne_05" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL5_Date" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="6" V_Information="0" V_SiDonneeDico="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:10px" DonTabIndex="17" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL5_01" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="6" V_Information="1" V_SiDonneeDico="true"
                                       DonWidth="250px" DonTabIndex="18"/>
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL5_02" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="6" V_Information="2" V_SiDonneeDico="true"
                                       DonWidth="80px" DonTabIndex="19"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL5_03" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="6" V_Information="3" V_SiDonneeDico="true"
                                       DonWidth="240px" DonTabIndex="20"/>
                     </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:Table ID="CadreRefDiplome" runat="server">
     <asp:TableRow>
         <asp:TableCell ID="CellReference" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupReferentiel" runat="server" TargetControlID="HPopupRef" PopupControlID="PanelRefPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelRefPopup" runat="server">
                <Virtualia:VReference ID="RefVirtualia" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupRef" runat="server" Value="0" />
        </asp:TableCell>
     </asp:TableRow>       
</asp:Table>
