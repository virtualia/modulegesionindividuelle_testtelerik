﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VListeCombo" Codebehind="VListeCombo.ascx.vb" %>

<asp:Table ID="ComboVirtualia" runat="server" CellPadding="1" CellSpacing="0" > 
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="Etiquette" runat="server" Height="20px" Width="200px"
                    BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                    BorderWidth="2px" ForeColor="#142425"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 4px; font-style: oblique;
                    text-indent: 5px; text-align: left"></asp:Label>
        </asp:TableCell>
        <asp:TableCell>
            <asp:DropDownList ID="VComboBox" runat="server" Height="20px" Width="300px"
            AutoPostBack="true" BackColor="#D7FAF3" ForeColor="#142425"
            style="border-color: #7EC8BE; border-width: 2px; border-style: inset;
                 display: table-cell; font-style: oblique; " >
            </asp:DropDownList>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>