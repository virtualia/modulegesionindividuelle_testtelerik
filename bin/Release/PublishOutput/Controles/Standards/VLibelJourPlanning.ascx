﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VLibelJourPlanning" Codebehind="VLibelJourPlanning.ascx.vb" %>

<%@ Register src="~/Controles/Standards/VListeCombo.ascx" tagname="VListeCombo" tagprefix="Virtualia" %>

<asp:Table ID="TableGlobale" runat="server">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="TableEntete" runat="server" Height="22px" Width="825px" CellPadding="0" CellSpacing="0" 
                        BorderStyle="None" BorderColor="#216B68" BorderWidth="1px" HorizontalAlign= "Center"
                        style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VListeCombo ID="LstLibelMois" runat="server" EtiVisible="true" EtiText=" de " EtiWidth="60px"
                             V_NomTable="Mois" LstWidth="180px" LstBorderColor="#B0E0D7" LstBackColor="#EEECFD"
                              EtiStyle="text-align:center" EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VListeCombo ID="LstNbMois" runat="server" EtiVisible="true" EtiText="sur" EtiWidth="90px"
                             V_NomTable="Nombre" LstWidth="90px" LstBorderColor="#B0E0D7" LstBackColor="#EEECFD"
                             EtiStyle="text-align:center" EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" />
                    </asp:TableCell>
                    <asp:TableCell ID="CellPagination" Visible="false">
                        <asp:Table ID="VPagination" runat="server" HorizontalAlign="Center" Visible="false"
                                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" >
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:ImageButton ID="PagePrecedente" runat="server" ImageUrl="~/Images/Boutons/PagePrecedente.bmp" 
                                     Height="17px" Width="24px" ToolTip="Page précédente" />
                                </asp:TableCell>
                                 <asp:TableCell>
                                    <asp:Label ID="PageEtiText" runat="server" Height="17px" Width="80px" BackColor="#8DA8A3" ForeColor="#E9FDF9" 
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                        style="margin-left: 0px; margin-top: 0px; text-indent: 1px; text-align: center; vertical-align: middle" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:ImageButton ID="PageSuivante" runat="server" ImageUrl="~/Images/Boutons/PageSuivante.bmp" 
                                    Height="17px" Width="24px" ToolTip="Page suivante" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiSelection" runat="server" Height="20px" Width="330px" BackColor="WhiteSmoke" ForeColor="#064F4D" Visible="false"
                            BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="Annee" 
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                            style="margin-left: 20px; margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="20px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="LibellesPlanning" runat="server" Width="825px" CellPadding="0" CellSpacing="0" 
                 HorizontalAlign="Center" style="position:static; z-index:99" >
                <asp:TableRow>
                    <asp:TableCell>
                         <asp:Table ID="LibelGauche" runat="server" Height="22px" Width="65px" CellPadding="0" CellSpacing="0" 
                                    BorderStyle="None" BorderColor="#216B68" BorderWidth="1px" HorizontalAlign="Left">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="EtiGauche" runat="server" Height="20px" Width="65px" BackColor="WhiteSmoke" ForeColor="#064F4D"
                                            BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Visible="false"
                                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                            style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell>
                        <div style=" width: 5px"></div>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Table ID="Semaine00" runat="server" Height="20px" Width="140px" CellPadding="0" CellSpacing="0" 
                                    BorderStyle="None" BorderColor="#216B68" BorderWidth="1px" style="position:static; z-index:99" >
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="7">
                                    <asp:Label ID="NumSemaine00" runat="server" Height="18px" Width="138px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                            BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="" Visible="false"
                                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                            style="margin-top: 1px; text-indent: 1px; text-align: center; border-bottom-style: none;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="Lundi00" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                            BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="L"
                                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                            style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="Mardi00" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                            BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="M"
                                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                            style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="Mercredi00" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                            BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="M"
                                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                            style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="Jeudi00" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                            BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="J"
                                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                            style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="Vendredi00" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                            BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="V"
                                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                            style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="Samedi00" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                            BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="S"
                                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                            style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="Dimanche00" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                            BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="D"
                                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                            style="margin-top: 1px; text-indent: 1px; text-align: center" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                <asp:TableCell>
                    <div style="width: 3px"></div>
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Table ID="Semaine01" runat="server" Height="20px" Width="140px" CellPadding="0" CellSpacing="0" 
                                BorderStyle="None" BorderColor="#216B68" BorderWidth="1px" style="position:static; z-index:99" >
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="7">
                                        <asp:Label ID="NumSemaine01" runat="server" Height="18px" Width="138px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="" Visible="false"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-bottom-style: none;" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Label ID="Lundi01" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="L"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Mardi01" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="M"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Mercredi01" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="M"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Jeudi01" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="J"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Vendredi01" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="V"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Samedi01" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="S"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Dimanche01" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="D"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                        <asp:TableCell>
                            <div style="width: 3px"></div>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Table ID="Semaine02" runat="server" Height="20px" Width="140px" CellPadding="0" CellSpacing="0" 
                                        BorderStyle="None" BorderColor="#216B68" BorderWidth="1px" style="position:static; z-index:99" >
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="7">
                                        <asp:Label ID="NumSemaine02" runat="server" Height="18px" Width="138px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="" Visible="false"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-bottom-style: none;" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Label ID="Lundi02" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="L"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Mardi02" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="M"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Mercredi02" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="M"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Jeudi02" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="J"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Vendredi02" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="V"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Samedi02" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="S"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Dimanche02" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="D"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                        <asp:TableCell>
                            <div style="width: 3px"></div>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Table ID="Semaine03" runat="server" Height="20px" Width="140px" CellPadding="0" CellSpacing="0" 
                                        BorderStyle="None" BorderColor="#216B68" BorderWidth="1px" style="position:static; z-index:99" >
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="7">
                                        <asp:Label ID="NumSemaine03" runat="server" Height="18px" Width="138px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="" Visible="false"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-bottom-style: none;" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Label ID="Lundi03" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="L"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Mardi03" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="M"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Mercredi03" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="M"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Jeudi03" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="J"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Vendredi03" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="V"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Samedi03" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="S"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Dimanche03" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="D"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                        <asp:TableCell>
                            <div style="width: 3px"></div>
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:Table ID="Semaine04" runat="server" Height="20px" Width="140px" CellPadding="0" CellSpacing="0" 
                                        BorderStyle="None" BorderColor="#216B68" BorderWidth="1px" style="position:static; z-index:99" >
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="7">
                                        <asp:Label ID="NumSemaine04" runat="server" Height="18px" Width="138px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="" Visible="false"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-bottom-style: none;" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Label ID="Lundi04" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="L"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Mardi04" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="M"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Mercredi04" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="M"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Jeudi04" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="J"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Vendredi04" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="V"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Samedi04" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="S"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Dimanche04" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="D"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                        <asp:TableCell>
                            <div style="width: 3px"></div>
                        </asp:TableCell>
                        <asp:TableCell>
                             <asp:Table ID="Semaine05" runat="server" Height="22px" Width="37px" CellPadding="0" CellSpacing="0" 
                                        BorderStyle="None" BorderColor="#216B68" BorderWidth="1px" style="position:static; z-index:99" >
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="7">
                                        <asp:Label ID="NumSemaine05" runat="server" Height="18px" Width="36px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="" Visible="false"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-bottom-style: none;" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Label ID="Lundi05" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="L"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none;" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="Mardi05" runat="server" Height="18px" Width="18px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="M"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                                style="margin-top: 1px; text-indent: 1px; text-align: center" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:HiddenField ID="HSelMois" runat="server" Value="" />
            <asp:HiddenField ID="HSelNombre" runat="server" Value="" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
 
 
