﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VPlanningAnnuel.ascx.vb" Inherits="Virtualia.Net.VPlanningAnnuel" %>

<%@ Register src="~/Controles/Standards/VLibelJourPlanning.ascx" tagname="VLibelJour" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VPlanningMensuel.ascx" tagname="VMensuel" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VLegendeCouleurs.ascx" tagname="VLegende" tagprefix="Virtualia" %>

<asp:UpdatePanel ID="UpdatePanelCol" runat="server">
   <ContentTemplate>
        <asp:Timer ID="TimerPlanning" runat="server" Interval="100" Enabled="false">
        </asp:Timer>
        <asp:Table ID="CadrePlanning" runat="server" BorderStyle="Ridge" BorderWidth="2px" Width="825px"
         BackColor="#E9FDF9" BorderColor="#B0E0D7" style="margin-left: 3px; margin-top: 3px;"   
         HorizontalAlign="Center" CellPadding="0" CellSpacing="0">
              <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                          <asp:Label ID="Etiquette" runat="server" Text="Planning annuel" Height="20px" Width="320px"
                            BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Notset"
                            BorderWidth="1px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="text-indent: 5px; text-align: center; margin-top: 5px;">
                          </asp:Label>          
                    </asp:TableCell>
                  </asp:TableRow>
                  <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top" ForeColor="White" Font-Bold="false" Height="15px">
                        <asp:UpdateProgress ID="UpdateAttente" runat="server">
                            <ProgressTemplate>
                                <asp:Table ID="CadreAttente" runat="server" BackColor="Transparent">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Image ID="ImgAttente" runat="server" Height="30px" Width="30px" ImageUrl="~/Images/General/Loading.gif" />
                                        </asp:TableCell>
                                        <asp:TableCell Width="10px" Height="10px" />
                                        <asp:TableCell>
                                            <asp:Label ID="EtiAttente" runat="server" Width="280px" Text="... Veuillez patienter ..." ForeColor="White" BorderStyle="None" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </ProgressTemplate>
                       </asp:UpdateProgress>
                    </asp:TableCell>
              </asp:TableRow>
              <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VLibelJour ID="LibelJours" runat="server" SiPaginationVisible="true" SiLstNbMoisVisible="true" SiEtiSemaineVisible="false" />
                    </asp:TableCell>      
              </asp:TableRow>
              <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>      
              </asp:TableRow>
              <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM00" runat="server" />
                    </asp:TableCell>      
              </asp:TableRow>
              <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
              </asp:TableRow>
              <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM01" runat="server" />
                    </asp:TableCell>      
              </asp:TableRow>
              <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
              </asp:TableRow>
              <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM02" runat="server" />
                    </asp:TableCell>      
              </asp:TableRow>
              <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
              </asp:TableRow>
              <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM03" runat="server" />
                    </asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM04" runat="server" />
                    </asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM05" runat="server" />
                    </asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM06" runat="server" />
                    </asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM07" runat="server" />
                    </asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM08" runat="server" />
                    </asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM09" runat="server" />
                    </asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM10" runat="server" />
                    </asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM11" runat="server" />
                    </asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM12" runat="server" />
                    </asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM13" runat="server" />
                    </asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM14" runat="server" />
                    </asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM15" runat="server" />
                    </asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM16" runat="server" />
                    </asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VMensuel ID="PlanningM17" runat="server"/>
                    </asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell Height="3px"></asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VLegende ID="Legende" runat="server" />
                    </asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                    <asp:TableCell Height="5px"></asp:TableCell>      
             </asp:TableRow>
             <asp:TableRow>
                <asp:TableCell>
                    <asp:HiddenField ID="HIndex" runat="server" Value="0" />
                    <asp:HiddenField ID="HPage" runat="server" Value="0" />
                </asp:TableCell>
             </asp:TableRow>
        </asp:Table>
     </ContentTemplate>
</asp:UpdatePanel>
