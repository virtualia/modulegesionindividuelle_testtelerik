﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VPagePaysage.ascx.vb" Inherits="Virtualia.Net.VPagePaysage" %>

<asp:Table ID="PagePaysage" runat="server" CellPadding="1" CellSpacing="0" Width="1050px" BackColor="White"
        BorderStyle="None" BorderWidth="1px" BorderColor="#B0E0D7" HorizontalAlign="Center" Font-Names="Courier New" Font-Size="Small" > 
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3" Height="10px">
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left"> 
            <asp:Label ID="EtiDateJour" runat="server" Height="12px" Width="600px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="True"
                Font-Bold="true" Font-Names="Courier New" Font-Size="X-Small"
                style="margin-top: 0px; margin-left: 20px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Right"> 
            <asp:Label ID="EtiPagination" runat="server" Height="12px" Width="200px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="True"
                Font-Bold="true" Font-Names="Courier New" Font-Size="X-Small"
                style="margin-top: 0px; text-indent: 5px; text-align: right; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Left"> 
            <asp:Label ID="EtiNbPages" runat="server" Height="12px" Width="100px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="True"
                Font-Bold="true" Font-Names="Courier New" Font-Size="X-Small"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne01" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne02" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne03" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne04" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne05" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne06" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne07" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne08" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne09" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne10" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne11" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne12" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne13" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne14" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne15" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne16" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne17" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne18" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne19" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne20" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne21" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne22" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne23" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne24" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne25" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne26" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne27" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne28" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne29" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne30" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne31" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne32" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne33" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne34" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne35" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne36" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne37" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne38" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne39" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne40" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne41" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne42" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne43" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <asp:Label ID="Ligne44" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3" Height="10px">
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<div style="page-break-after:always;"></div>
