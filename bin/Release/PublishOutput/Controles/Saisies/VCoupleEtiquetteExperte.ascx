﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VCoupleEtiquetteExperte" Codebehind="VCoupleEtiquetteExperte.ascx.vb" %>

<asp:Table ID="CadreExperte" runat="server" CellPadding="1" CellSpacing="0">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="Etiquette" runat="server" Height="20px" Width="170px"
                    BackColor="#8DA8A3" BorderColor="#C0BAE5"  BorderStyle="Outset"
                    BorderWidth="2px" ForeColor="#E9FDF9" Font-Italic="true"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 4px; text-indent: 5px; text-align: left" >
            </asp:Label>
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiDonnee" runat="server" Height="20px" Width="300px"
                    BackColor="#C5E0DA" BorderColor="#C0BAE5"  BorderStyle="Inset"
                    BorderWidth="2px" ForeColor="#586361" Visible="true"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                    style="margin-top: 2px; margin-left: 1px; text-indent: 5px; text-align: left" >
            </asp:Label>
        </asp:TableCell>
    </asp:TableRow>
  </asp:Table>
