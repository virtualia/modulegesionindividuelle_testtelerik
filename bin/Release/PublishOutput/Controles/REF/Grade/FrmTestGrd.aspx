﻿<%@ Page Language="vb" MasterPageFile="~/PagesMaitre/VirtualiaMain.master" AutoEventWireup="false" CodeBehind="FrmTestGrd.aspx.vb" Inherits="Virtualia.Net.WebForm1" %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.master" %>
<%@ Register Src="~/Controles/REF/Grade/VReferentielGrades.ascx" TagName="VRefGrades" TagPrefix="Virtualia" %>

<asp:Content ID="CadreAdmin" runat="server" ContentPlaceHolderID="Principal">
    <asp:Table ID="CadreComplet" runat="server" HorizontalAlign="Center">
        <asp:TableRow>
            <asp:TableCell>
                <Virtualia:VRefGrades ID="ListeGrades" runat="server"></Virtualia:VRefGrades>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
