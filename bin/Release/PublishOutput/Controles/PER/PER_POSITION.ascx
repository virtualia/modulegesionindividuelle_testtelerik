﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_POSITION" Codebehind="PER_POSITION.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" tagname="VCoupleEtiquetteExperte" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>

 <asp:Table ID="CadreControle" runat="server" Height="30px" Width="600px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
   <asp:TableRow> 
     <asp:TableCell> 
       <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="530px" SiColonneSelect="true"
                                        SiCaseAcocher="false"/>
     </asp:TableCell>
   </asp:TableRow>
   <asp:TableRow>
     <asp:TableCell>
      <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
        BorderColor="#B0E0D7" Height="750px" Width="520px" HorizontalAlign="Center" style="margin-top: 3px;">
        <asp:TableRow>
          <asp:TableCell>
            <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                            Width="160px" CellPadding="0" CellSpacing="0"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Nouvelle fiche">
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Supprimer la fiche">
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="false">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="550px" 
                CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="Position administrative" Height="20px" Width="300px"
                            BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px;
                            font-style: oblique; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="550px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="250px">
                        <Virtualia:VCoupleEtiDate ID="InfoD00" runat="server" TypeCalendrier="Standard"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="13" V_Information="0" DonTabIndex="1"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" Width="250px">
                        <Virtualia:VCoupleEtiDate ID="InfoD07" runat="server" TypeCalendrier="Standard" SiDateFin="true" EtiWidth="80px" EtiStyle="margin-left:2px"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="13" V_Information="7" DonTabIndex="2"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                         <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server"
                           V_PointdeVue="1" V_Objet="13" V_Information="1" V_SiDonneeDico="true"
                           DonTabIndex="3"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                         <Virtualia:VDuoEtiquetteCommande ID="Dontab05" runat="server"
                           V_PointdeVue="1" V_Objet="13" V_Information="5" V_SiDonneeDico="true"
                           DonTabIndex="4" EtiText="Administration"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                         <Virtualia:VDuoEtiquetteCommande ID="Dontab11" runat="server"
                           V_PointdeVue="1" V_Objet="13" V_Information="11" V_SiDonneeDico="true"
                           DonTabIndex="5"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                         <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" EtiWidth="300px"
                           V_PointdeVue="1" V_Objet="13" V_Information="2" V_SiDonneeDico="true"
                           DonWidth="80px" DonTabIndex="6" EtiStyle="text-align:center" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                         <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server" EtiWidth="300px"
                           V_PointdeVue="1" V_Objet="13" V_Information="12" V_SiDonneeDico="true"
                           DonWidth="80px" DonTabIndex="7" EtiStyle="text-align:center"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                         <Virtualia:VDuoEtiquetteCommande ID="Dontab08" runat="server"
                           V_PointdeVue="1" V_Objet="13" V_Information="8" V_SiDonneeDico="true"
                           DonTabIndex="8"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow Height="25px">
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                         <asp:Label ID="EtiArrete" runat="server" Height="20px" Width="300px"
                           Text="Date et référence de l'arrété"
                           BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                           BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                         </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <Virtualia:VCoupleEtiDate ID="InfoD03" runat="server" TypeCalendrier="Standard" EtiVisible="false"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="13" V_Information="3" DonTabIndex="9"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                         <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server"
                           V_PointdeVue="1" V_Objet="13" V_Information="4" V_SiDonneeDico="true"
                           DonWidth="200px" DonTabIndex="10"  EtiVisible="false" DonStyle="margin-left:2px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                         <asp:Label ID="EtiDecret" runat="server" Height="20px" Width="350px"
                           Text="Date du décret et date de parution au JO du décret"
                           BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                           BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                         </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Table ID="CadreJO" runat="server">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDate ID="InfoD09" runat="server" TypeCalendrier="Standard" EtiWidth="120px"
                                                    V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="13" V_Information="9" DonTabIndex="11"/>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDate ID="InfoD10" runat="server" TypeCalendrier="Standard" EtiWidth="120px" EtiText="Date de parution"
                                                    V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="13" V_Information="10" DonTabIndex="12"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                         <asp:Label ID="EtiGestion" runat="server" Height="20px" Width="350px"
                           Text="Compléments d'information - Mode de gestion"
                           BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Outset"
                           BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           Style="margin-top:10px;margin-bottom:10px">
                         </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                         <Virtualia:VCocheSimple ID="Coche18" runat="server"
                           V_PointdeVue="1" V_Objet="13" V_Information="18" V_SiDonneeDico="true"
                           V_Width="300px" V_Style="margin-left:120px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                         <Virtualia:VCocheSimple ID="Coche19" runat="server"
                           V_PointdeVue="1" V_Objet="13" V_Information="19" V_SiDonneeDico="true"
                           V_Width="300px" V_Style="margin-left:120px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                         <Virtualia:VCocheSimple ID="Coche20" runat="server"
                           V_PointdeVue="1" V_Objet="13" V_Information="20" V_SiDonneeDico="true"
                           V_Width="300px" V_Style="margin-left:120px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                         <asp:Label ID="EtiRetraite" runat="server" Height="20px" Width="350px"
                           Text="Compléments d'information - Retraite et pension"
                           BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Outset"
                           BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           Style="margin-top:10px">
                         </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                         <Virtualia:VCoupleEtiDonnee ID="InfoH17" runat="server"
                           V_PointdeVue="1" V_Objet="13" V_Information="17" V_SiDonneeDico="true"
                           EtiWidth="200px" DonWidth="80px" DonTabIndex="13" EtiStyle="margin-left:30px; margin-top:5px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                         <Virtualia:VCocheSimple ID="Coche13" runat="server"
                           V_PointdeVue="1" V_Objet="13" V_Information="13" V_SiDonneeDico="true"
                           V_Width="300px" V_Style="margin-left:30px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                         <Virtualia:VCocheSimple ID="Coche14" runat="server"
                           V_PointdeVue="1" V_Objet="13" V_Information="14" V_SiDonneeDico="true"
                           V_Width="300px" V_Style="margin-left:30px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                         <Virtualia:VCocheSimple ID="Coche15" runat="server"
                           V_PointdeVue="1" V_Objet="13" V_Information="15" V_SiDonneeDico="true"
                           V_Width="300px" V_Style="margin-left:30px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                         <Virtualia:VCocheSimple ID="Coche16" runat="server"
                           V_PointdeVue="1" V_Objet="13" V_Information="16" V_SiDonneeDico="true"
                           V_Width="300px" V_Style="margin-left:30px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px" Columnspan="2"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Panel ID="CadreExpert" runat="server" BorderStyle="None" BorderWidth="1px"
                BorderColor="#B0E0D7" Height="200px" Width="520px" HorizontalAlign="Left"
                style="margin-top: 5px;  vertical-align: middle">
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH01" runat="server"
                   V_PointdeVue="1" V_Objet="13" V_InfoExperte="930" V_SiDonneeDico="true"
                   EtiHeight="20px" EtiWidth="250px" DonWidth="80px" DonHeight="20px"
                   EtiStyle="margin-left: 40px; margin-top: 5px" DonStyle="margin-top: 9px; text-align: center"/>
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH02" runat="server"
                   V_PointdeVue="1" V_Objet="13" V_InfoExperte="931" V_SiDonneeDico="true"
                   EtiHeight="20px" EtiWidth="250px" DonWidth="80px" DonHeight="20px"
                   EtiStyle="margin-left: 40px;" DonStyle="text-align: center;"/>
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH03" runat="server"
                   V_PointdeVue="1" V_Objet="13" V_InfoExperte="777" V_SiDonneeDico="true"
                   EtiHeight="20px" EtiWidth="250px" DonWidth="200px" DonHeight="20px"
                   EtiStyle="margin-left: 40px;" DonStyle="text-align: center;"/>
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH04" runat="server"
                   V_PointdeVue="1" V_Objet="13" V_InfoExperte="670" V_SiDonneeDico="true"
                   EtiHeight="20px" EtiWidth="250px" DonWidth="120px" DonHeight="20px"
                   EtiStyle="margin-left: 40px;" DonStyle="text-align: center;"/>
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH05" runat="server"
                   V_PointdeVue="1" V_Objet="13" V_InfoExperte="932" V_SiDonneeDico="true"
                   EtiHeight="20px" EtiWidth="250px" DonWidth="120px" DonHeight="20px"
                   EtiStyle="margin-left: 40px;" DonStyle="text-align: center;"/>
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH06" runat="server"
                   V_PointdeVue="1" V_Objet="13" V_InfoExperte="933" V_SiDonneeDico="true"
                   EtiHeight="20px" EtiWidth="250px" DonWidth="120px" DonHeight="20px"
                   EtiStyle="margin-left: 40px;" DonStyle="text-align: center;"/>
              </asp:Panel>
          </asp:TableCell>
        </asp:TableRow>       
      </asp:Table>
     </asp:TableCell>
   </asp:TableRow>
 </asp:Table>