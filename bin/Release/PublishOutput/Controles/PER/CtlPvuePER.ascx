﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlPvuePER.ascx.vb" Inherits="Virtualia.Net.CtlPvuePER" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Standards/VMenuCommun.ascx" tagname="VMenu" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VReferentiel.ascx" tagname="VReference" tagprefix="Virtualia" %>
<%@ Register Src="~/Controles/REF/Grade/VReferentielGrades.ascx" TagName="VRefGrade" TagPrefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>
<%@ Register src="PER_ETATCIVIL.ascx" tagname="PER_ETATCIVIL" tagprefix="Virtualia" %>
<%@ Register src="PER_DOMICILE.ascx" tagname="PER_DOMICILE" tagprefix="Virtualia" %>
<%@ Register src="PER_BANQUE.ascx" tagname="PER_BANQUE" tagprefix="Virtualia" %>
<%@ Register src="PER_ENFANT.ascx" tagname="PER_ENFANT" tagprefix="Virtualia" %>
<%@ Register src="PER_COLLECTIVITE.ascx" tagname="PER_COLLECTIVITE" tagprefix="Virtualia" %>
<%@ Register src="PER_ADMORIGINE.ascx" tagname="PER_ADMORIGINE" tagprefix="Virtualia" %>
<%@ Register src="PER_LOLF.ascx" tagname="PER_LOLF" tagprefix="Virtualia" %>
<%@ Register src="PER_AFFECTATION.ascx" tagname="PER_AFFECTATION" tagprefix="Virtualia" %>
<%@ Register src="PER_ADRESSEPRO.ascx" tagname="PER_ADRESSEPRO" tagprefix="Virtualia" %>
<%@ Register src="PER_AFFILIATION.ascx" tagname="PER_AFFILIATION" tagprefix="Virtualia" %>
<%@ Register src="PER_MUTUELLE.ascx" tagname="PER_MUTUELLE" tagprefix="Virtualia" %>
<%@ Register src="PER_ABSENCE.ascx" tagname="PER_ABSENCE" tagprefix="Virtualia" %>
<%@ Register src="PER_STATUT.ascx" tagname="PER_STATUT" tagprefix="Virtualia" %>
<%@ Register src="PER_POSITION.ascx" tagname="PER_POSITION" tagprefix="Virtualia" %>
<%@ Register src="PER_GRADE.ascx" tagname="PER_GRADE" tagprefix="Virtualia" %>
<%@ Register src="PER_INDEMNITE.ascx" tagname="PER_INDEMNITE" tagprefix="Virtualia" %>
<%@ Register src="PER_HEURESUP.ascx" tagname="PER_HEURESUP" tagprefix="Virtualia" %>
<%@ Register src="PER_STATUT_DETACHE.ascx" tagname="PER_STATUT_DETACHE" tagprefix="Virtualia" %>
<%@ Register src="PER_POSITION_DETACHE.ascx" tagname="PER_POSITION_DETACHE" tagprefix="Virtualia" %>
<%@ Register src="PER_GRADE_DETACHE.ascx" tagname="PER_GRADE_DETACHE" tagprefix="Virtualia" %>
<%@ Register src="PER_VARIABLE_PAIE.ascx" tagname="PER_VARIABLE_PAIE" tagprefix="Virtualia" %>
<%@ Register src="PER_SIFAC.ascx" tagname="PER_SIFAC" tagprefix="Virtualia" %>

<asp:Table ID="CadreGlobal" runat="server" HorizontalAlign="Center" Width="1140px">
        <asp:TableRow>
        <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
            <asp:Label ID="EtiTitre" runat="server" Text="SITUATION EN GESTION" Height="22px" Width="700px"
                        BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#D7FAF3"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                        style="font-style: normal; text-indent: 5px; text-align: center;">
                </asp:Label>     
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
            <Virtualia:VMenu ID="PER_MENU" runat="server" V_Appelant="PER"></Virtualia:VMenu>
        </asp:TableCell>
        <asp:TableCell ID="CellulePER" runat="server" Width="800px" VerticalAlign="Top">
            <asp:MultiView ID="MultiPER" runat="server" ActiveViewIndex="0">
                <asp:View ID="VueEtatCivil" runat="server">
                    <Virtualia:PER_ETATCIVIL ID="PER_ETATCIVIL_1" runat="server"></Virtualia:PER_ETATCIVIL>
                </asp:View>
                <asp:View ID="VueAdresse" runat="server">
                    <Virtualia:PER_DOMICILE ID="PER_DOMICILE_4" runat="server"></Virtualia:PER_DOMICILE>
                </asp:View>
                <asp:View ID="VueBanque" runat="server">
                    <Virtualia:PER_BANQUE ID="PER_BANQUE_5" runat="server"></Virtualia:PER_BANQUE>
                </asp:View>
                <asp:View ID="VueEnfant" runat="server">
                    <Virtualia:PER_ENFANT ID="PER_ENFANT_3" runat="server"></Virtualia:PER_ENFANT>
                </asp:View>
                <asp:View ID="VueAdministration" runat="server">
                    <Virtualia:PER_COLLECTIVITE ID="PER_COLLECTIVITE_26" runat="server"></Virtualia:PER_COLLECTIVITE>
                </asp:View>
                <asp:View ID="VueAdmOrigine" runat="server">
                    <Virtualia:PER_ADMORIGINE ID="PER_ADMORIGINE_25" runat="server"></Virtualia:PER_ADMORIGINE>
                </asp:View>
                <asp:View ID="VueLOLF" runat="server">
                    <Virtualia:PER_LOLF ID="PER_LOLF_92" runat="server"></Virtualia:PER_LOLF>
                </asp:View>
                <asp:View ID="VueAffectation" runat="server">
                    <Virtualia:PER_AFFECTATION ID="PER_AFFECTATION_17" runat="server"></Virtualia:PER_AFFECTATION>
                </asp:View>
                <asp:View ID="VueAdressePro" runat="server">
                    <Virtualia:PER_ADRESSEPRO ID="PER_ADRESSEPRO_24" runat="server"></Virtualia:PER_ADRESSEPRO>
                </asp:View>
                <asp:View ID="VueCaisse" runat="server">
                    <Virtualia:PER_AFFILIATION ID="PER_AFFILIATION_27" runat="server"></Virtualia:PER_AFFILIATION>
                </asp:View>
                <asp:View ID="VueMutuelle" runat="server">
                    <Virtualia:PER_MUTUELLE ID="PER_MUTUELLE_59" runat="server"></Virtualia:PER_MUTUELLE>
                </asp:View>
                <asp:View ID="VueAbsence" runat="server">
                    <Virtualia:PER_ABSENCE ID="PER_ABSENCE_15" runat="server"></Virtualia:PER_ABSENCE>
                </asp:View>
                <asp:View ID="VueStatut" runat="server">
                    <Virtualia:PER_STATUT ID="PER_STATUT_12" runat="server"></Virtualia:PER_STATUT>
                </asp:View>
                <asp:View ID="VuePosition" runat="server">
                    <Virtualia:PER_POSITION ID="PER_POSITION_13" runat="server"></Virtualia:PER_POSITION>
                </asp:View>
                <asp:View ID="VueGrade" runat="server">
                    <Virtualia:PER_GRADE ID="PER_GRADE_14" runat="server"></Virtualia:PER_GRADE>
                </asp:View>
                <asp:View ID="VueStatutDetache" runat="server">
                    <Virtualia:PER_STATUT_DETACHE ID="PER_STATUT_54" runat="server"></Virtualia:PER_STATUT_DETACHE>
                </asp:View>
                <asp:View ID="VuePositionDetache" runat="server">
                    <Virtualia:PER_POSITION_DETACHE ID="PER_POSITION_55" runat="server"></Virtualia:PER_POSITION_DETACHE>
                </asp:View>
                <asp:View ID="VueGradeDetache" runat="server">
                    <Virtualia:PER_GRADE_DETACHE ID="PER_GRADE_56" runat="server"></Virtualia:PER_GRADE_DETACHE>
                </asp:View>
                <asp:View ID="VuePrime" runat="server">
                    <Virtualia:PER_INDEMNITE ID="PER_INDEMNITE_21" runat="server"></Virtualia:PER_INDEMNITE>
                </asp:View>
                <asp:View ID="VueVariablePaie" runat="server">
                    <Virtualia:PER_VARIABLE_PAIE ID="PER_VARIABLE_PAIE_67" runat="server"></Virtualia:PER_VARIABLE_PAIE>
                </asp:View>
                <asp:View ID="VueHeureSup" runat="server">
                    <Virtualia:PER_HEURESUP ID="PER_HEURESUP_61" runat="server"></Virtualia:PER_HEURESUP>
                </asp:View>
                <asp:View ID="VueSifac" runat="server">
                    <Virtualia:PER_SIFAC ID="PER_SIFAC_180" runat="server"></Virtualia:PER_SIFAC>
                </asp:View>
            </asp:MultiView>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ID="CellMessage" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupMsg" runat="server" TargetControlID="HPopupMsg" PopupControlID="PanelMsgPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelMsgPopup" runat="server">
                <Virtualia:VMessage id="MsgVirtualia" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupMsg" runat="server" Value="0" />
        </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ID="CellReference" Visible="false">
                <ajaxToolkit:ModalPopupExtender ID="PopupReferentiel" runat="server" TargetControlID="HPopupRef" PopupControlID="PanelRefPopup" BackgroundCssClass="modalBackground" />
                <asp:Panel ID="PanelRefPopup" runat="server">
                    <Virtualia:VReference ID="RefVirtualia" runat="server" />
                </asp:Panel>
                <asp:HiddenField ID="HPopupRef" runat="server" Value="0" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
             <asp:TableCell ID="CellRefGrade" Visible="false">
                 <ajaxToolkit:ModalPopupExtender ID="PopupRefGrade" runat="server" TargetControlID="HPopupRefGrade" PopupControlID="PanelRefGradePopup" BackgroundCssClass="modalBackground" />
                 <asp:Panel ID="PanelRefGradePopup" runat="server">
                    <Virtualia:VRefGrade ID="RefGradeGrilles" runat="server"/>
                </asp:Panel>
                <asp:HiddenField ID="HPopupRefGrade" runat="server" Value="0" />
             </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
            </asp:TableCell>
    </asp:TableRow>
</asp:Table>
   
