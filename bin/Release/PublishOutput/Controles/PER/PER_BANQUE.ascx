﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_BANQUE" Codebehind="PER_BANQUE.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
 
 <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
    BorderColor="#B0E0D7" Height="250px" Width="500px" HorizontalAlign="Center">
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
            CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="false"
            BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
            Width="70px" HorizontalAlign="Right" style="margin-top: 3px; margin-right:3px">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Bottom">
                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                    BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                    </asp:Button>
                </asp:TableCell>
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="500px"
            CellSpacing="0" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="Etiquette" runat="server" Text="RIB - Relevé d'identité bancaire" Height="20px" Width="310px"
                        BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#D7FAF3"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px;
                        font-style: oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="RIB" runat="server" CellPadding="0" CellSpacing="0" Width="436px"
             HorizontalAlign="Center">
             <asp:TableRow>
               <asp:TableCell HorizontalAlign="Center" ColumnSpan="4" Width="436px">
                     <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV05" runat="server"
                       V_PointdeVue="1" V_Objet="5" V_Information="5" V_SiDonneeDico="true"
                       EtiWidth="436px" DonWidth="434px" DonHeight="20px" DonTabIndex="1"
                       EtiStyle="margin-left: 0px;text-align: center" DonStyle="text-align: center"/> 
                </asp:TableCell> 
             </asp:TableRow>
             <asp:TableRow>
                <asp:TableCell Height="5px" ColumnSpan="4"></asp:TableCell>
             </asp:TableRow>
             <asp:TableRow>
                <asp:TableCell Width="96px">
                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV01" runat="server" DonTextMode="false"
                       V_PointdeVue="1" V_Objet="5" V_Information="1" V_SiDonneeDico="true"
                       EtiWidth="92px" DonWidth="90px" DonHeight="20px" DonTabIndex="2"
                       EtiStyle="margin-left:2px;text-align: center" 
                       DonStyle="margin-left:2px;text-align: center" CadreWidth="92px"/> 
                </asp:TableCell>
                <asp:TableCell Width="96px">
                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV02" runat="server" DonTextMode="false"
                       V_PointdeVue="1" V_Objet="5" V_Information="2" V_SiDonneeDico="true"
                       EtiWidth="92px" DonWidth="90px" DonHeight="20px" DonTabIndex="3"
                       EtiStyle="margin-left:2px;text-align: center" 
                       DonStyle="margin-left:2px;text-align: center" CadreWidth="92px"/> 
                </asp:TableCell>
                <asp:TableCell Width="176px">
                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV03" runat="server" DonTextMode="false"
                       V_PointdeVue="1" V_Objet="5" V_Information="3" V_SiDonneeDico="true"
                       EtiWidth="172px" DonWidth="170px" DonHeight="20px" DonTabIndex="4"
                       EtiStyle="margin-left:2px;text-align: center" 
                       DonStyle="margin-left:2px;text-align: center" CadreWidth="172px"/>  
                </asp:TableCell>
                <asp:TableCell Width="66px">
                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV04" runat="server" DonTextMode="false"
                       V_PointdeVue="1" V_Objet="5" V_Information="4" V_SiDonneeDico="true"
                       EtiWidth="62px" DonWidth="60px" DonHeight="20px" DonTabIndex="5"
                       EtiStyle="margin-left:2px;text-align: center" 
                       DonStyle="margin-left:2px;text-align: center" CadreWidth="62px"/> 
                </asp:TableCell>
             </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="Iban" runat="server" CellPadding="0" CellSpacing="0" Width="500px">
            <asp:TableRow>
                <asp:TableCell Height="7px"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV07" runat="server" DonTextMode="false"
                       V_PointdeVue="1" V_Objet="5" V_Information="7" V_SiDonneeDico="true"
                       EtiWidth="354px" DonWidth="352px" DonHeight="20px" DonTabIndex="6"
                       EtiStyle="margin-top:2px" DonStyle="text-align: center"/> 
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                     <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server"
                       V_PointdeVue="1" V_Objet="5" V_Information="8" V_SiDonneeDico="true"
                       EtiWidth="92px" DonWidth="254px" DonTabIndex="7"
                       EtiStyle="margin-left: 0px;" DonStyle="text-align: center"/> 
                </asp:TableCell>
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="15px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="Reglement" runat="server" CellPadding="0" CellSpacing="0" Width="436px">
             <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left">
                     <Virtualia:VDuoEtiquetteCommande ID="Dontab06" runat="server" 
                       V_PointdeVue="1" V_Objet="5" V_Information="6" V_SiDonneeDico="true"
                       EtiWidth="200px" DonWidth="236px" DonTabIndex="8"
                       EtiStyle="margin-left: 30px;"/>
                </asp:TableCell>
             </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="10px"></asp:TableCell>
    </asp:TableRow>
 </asp:Table>       
 