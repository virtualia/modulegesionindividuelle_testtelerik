﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_ENFANT" Codebehind="PER_ENFANT.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" tagname="VCoupleEtiquetteExperte" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
 
 <asp:Table ID="CadreControle" runat="server" Height="30px" Width="450px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
   <asp:TableRow> 
     <asp:TableCell> 
       <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="430px" SiColonneSelect="true"
                                        SiCaseAcocher="false"/>
     </asp:TableCell>
   </asp:TableRow>
   <asp:TableRow>
     <asp:TableCell>
       <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
        BorderColor="#B0E0D7" Height="230px" Width="450px" HorizontalAlign="Center">
        <asp:TableRow>
          <asp:TableCell>
            <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                            Width="160px" CellPadding="0" CellSpacing="0"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Nouvelle fiche">
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Supprimer la fiche">
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="false">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreTitre" runat="server" Height="30px" CellPadding="0" Width="450px"  
                CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="Enfants" Height="20px" Width="300px"
                            BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px;
                            font-style: oblique; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="Identite" runat="server" Height="40px" CellPadding="0" CellSpacing="0">
                 <asp:TableRow>
                    <asp:TableCell Width="450px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDate ID="InfoD00" runat="server" TypeCalendrier="Standard" EtiWidth="150px"
                               V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="3" V_Information="0" DonTabIndex="1"/>
                    </asp:TableCell>
                 </asp:TableRow>
                 <asp:TableRow>   
                    <asp:TableCell Width="450px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="1" V_Objet="3" V_Information="2" V_SiDonneeDico="true"
                           EtiWidth="100px" DonWidth="200px" DonTabIndex="2"/>
                    </asp:TableCell>
                 </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell Width="450px" HorizontalAlign="Left">
                         <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_PointdeVue="1" V_Objet="3" V_Information="1" V_SiDonneeDico="true"
                           EtiWidth="100px" DonWidth="200px" DonTabIndex="3"/> 
                    </asp:TableCell>
                 </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="Sexe" runat="server" CellPadding="0" CellSpacing="0">
                  <asp:TableRow> 
                    <asp:TableCell Width="110px" HorizontalAlign="Left">
                        <asp:Label ID="EtiSexe" runat="server" Height="20px" Width="100px" Text="Sexe"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 2px; margin-left: 5px; text-indent: 5px; text-align: left">
                        </asp:Label> 
                    </asp:TableCell>
                    <asp:TableCell Width="340px" HorizontalAlign="Left">
                        <Virtualia:VTrioHorizontalRadio ID="RadioH03" runat="server" V_Groupe="Sexe"
                            V_PointdeVue="1" V_Objet="3" V_Information="3" V_SiDonneeDico="true"
                            RadioGaucheWidth="122px" RadioCentreWidth="123px" RadioDroiteWidth="0px" 
                            RadioGaucheText="Masculin" RadioCentreText="Féminin" RadioDroiteVisible="false" 
                            RadioGaucheStyle="margin-left: 5px;"/>
                    </asp:TableCell>
                  </asp:TableRow> 
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="ACharge" runat="server" CellPadding="0" CellSpacing="0">
                 <asp:TableRow>
                    <asp:TableCell Width="110px" HorizontalAlign="Left">    
                        <asp:Label ID="EtiCharge" runat="server" Height="35px" Width="100px" Text="Donnant droit au SFT"
                           BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                           BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 2px; margin-left: 5px; text-indent: 5px; text-align: left">
                        </asp:Label> 
                     </asp:TableCell>     
                     <asp:TableCell Width="340px" HorizontalAlign="Left">       
                        <Virtualia:VTrioHorizontalRadio ID="RadioH04" runat="server" V_Groupe="ACharge"
                           V_PointdeVue="1" V_Objet="3" V_Information="4" V_SiDonneeDico="true"
                           RadioGaucheWidth="80px" RadioCentreWidth="80px" RadioDroiteWidth="80px" 
                           RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText="Autre" 
                           RadioGaucheStyle="margin-left: 5px;"/>
                    </asp:TableCell>
                 </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="Naissance" runat="server" CellPadding="0" CellSpacing="0">
                 <asp:TableRow> 
                    <asp:TableCell Width="450px" HorizontalAlign="Left">
                         <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server"
                            V_PointdeVue="1" V_Objet="3" V_Information="11" V_SiDonneeDico="true"
                            EtiWidth="150px" DonWidth="200px" DonTabIndex="6"
                            DonStyle="margin-Top: 5px" EtiStyle="margin-Top: 5px"/> 
                    </asp:TableCell>
                 </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
             <asp:Panel ID="CadreExpert" runat="server" BorderStyle="None" BorderWidth="1px"
                BorderColor="#B0E0D7" Height="70px" Width="450px" HorizontalAlign="Left"
                style="margin-top: 5px;  vertical-align: middle">
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH01" runat="server"
                   V_PointdeVue="1" V_Objet="3" V_InfoExperte="515" V_SiDonneeDico="true"
                   EtiHeight="20px" EtiWidth="170px" DonWidth="40px" DonHeight="20px"
                   EtiStyle="margin-left: 100px; margin-top: 2px" DonStyle="margin-top: 6px; text-align: center"/>
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH02" runat="server"
                   V_PointdeVue="1" V_Objet="3" V_InfoExperte="516" V_SiDonneeDico="true"
                   EtiHeight="20px" EtiWidth="170px" DonWidth="40px" DonHeight="20px"
                   EtiStyle="margin-left: 100px; margin-top: 2px" DonStyle="margin-top: 2px; text-align: center"/>
             </asp:Panel>
          </asp:TableCell>
        </asp:TableRow>
       </asp:Table>
     </asp:TableCell>
   </asp:TableRow>
 </asp:Table>