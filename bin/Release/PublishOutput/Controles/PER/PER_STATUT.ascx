﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_STATUT" Codebehind="PER_STATUT.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" tagname="VCoupleEtiquetteExperte" tagprefix="Virtualia" %>

 <asp:Table ID="CadreControle" runat="server" Height="30px" Width="600px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
     <asp:TableRow> 
       <asp:TableCell> 
         <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="530px" SiColonneSelect="true" SiCaseAcocher="false"/>
       </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow> 
         <asp:TableCell> 
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
                BorderColor="#B0E0D7" Height="390px" Width="520px" HorizontalAlign="Center" 
                style="margin-top: 10px;">
                <asp:TableRow>
                    <asp:TableCell>
                      <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                        Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Bottom">
                                <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                                    Width="160px" CellPadding="0" CellSpacing="0"
                                    BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                                BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                                Tooltip="Nouvelle fiche">
                                            </asp:Button>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                                BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                                Tooltip="Supprimer la fiche">
                                            </asp:Button>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>    
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Bottom">
                                <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                                    BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                    CellPadding="0" CellSpacing="0" Visible="false">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                                BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                                </asp:Button>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>  
                            </asp:TableCell>
                        </asp:TableRow>
                      </asp:Table>    
                    </asp:TableCell>
                </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="550px" 
                            CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Statut" Height="20px" Width="300px"
                                        BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                                        BorderWidth="2px" ForeColor="#D7FAF3"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px;
                                        font-style: oblique; text-indent: 5px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>      
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                 </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="550px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDate ID="InfoD00" runat="server" TypeCalendrier="Standard" EtiWidth="170px"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="12" V_Information="0" DonTabIndex="1"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" Width="400px">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" V_PointdeVue="1" V_Objet="12" V_Information="1" V_SiDonneeDico="true" DonTabIndex="2"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server" V_PointdeVue="1" V_Objet="12" V_Information="2" V_SiDonneeDico="true" DonTabIndex="3"/> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <asp:Table ID="CadreArrete" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDate ID="InfoD03" runat="server" TypeCalendrier="Standard" EtiWidth="200px"
                                                       V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="12" V_Information="3" DonTabIndex="4"/>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" EtiVisible="false" DonWidth="140px" 
                                                       V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="12" V_Information="4" DonTabIndex="5" DonStyle="margin-left:2px;margin-top:0px"/>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiContrat" runat="server" Height="20px" Width="250px" Text="Si non titulaire et/ou contractuel"
                                        BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 8px; margin-bottom: 8px; text-indent: 5px; text-align: center">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDate ID="InfoD08" runat="server" TypeCalendrier="Standard" EtiWidth="170px" SiDateFin="true" EtiText="Date de fin de contrat"
                                          V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="12" V_Information="8" DonTabIndex="6"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" V_PointdeVue="1" V_Objet="12" V_Information="7" V_SiDonneeDico="true" DonWidth="20px" DonTabIndex="7"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiRemu" runat="server" Height="20px" Width="250px" Text="Rémunération"
                                        BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 8px; margin-bottom: 8px; text-indent: 5px; text-align: center">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                     <Virtualia:VDuoEtiquetteCommande ID="Dontab05" runat="server" V_PointdeVue="1" V_Objet="12" V_Information="5" V_SiDonneeDico="true" DonTabIndex="8"/> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab06" runat="server"  V_PointdeVue="1" V_Objet="12" V_Information="6" V_SiDonneeDico="true" DonTabIndex="9"/>  
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" Columnspan="2"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>      
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Panel ID="CadreExpert" runat="server" BorderStyle="none" BorderWidth="1px"
                            BorderColor="#B0E0D7" Height="70px" Width="520px" HorizontalAlign="Left"
                            style="margin-top: 5px;  vertical-align: middle">
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH01" runat="server"
                               V_PointdeVue="1" V_Objet="12" V_InfoExperte="534" V_SiDonneeDico="true"
                               EtiHeight="20px" EtiWidth="170px" DonWidth="100px" DonHeight="20px"
                               EtiStyle="margin-left: 50px; margin-top: 5px" DonStyle="margin-top: 9px; text-align: center"/>
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH02" runat="server"
                               V_PointdeVue="1" V_Objet="12" V_InfoExperte="552" V_SiDonneeDico="true"
                               EtiHeight="20px" EtiWidth="170px" DonWidth="200px" DonHeight="20px"
                               EtiStyle="margin-left: 50px;" DonStyle="text-align: center;"/>
                        </asp:Panel>       
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
         </asp:TableCell>
     </asp:TableRow>
 </asp:Table>
 