﻿Option Explicit On
Option Strict On
Option Compare Text
Namespace Session
    Public Class ClefV3Connexion
        Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates
        Private WsRhFct As Virtualia.Systeme.Fonctions.Generales
        Private WsChainePostee As String
        Private WsSiAccesManager As Boolean = False
        Private WsSiAccesGRH As Boolean = False
        Private WsAppliAccedee As String = ""
        Private WsUtilisateurV3 As Virtualia.Version3.Utilisateur.UtilisateurV3
        Private WsNomCnxV3 As String
        Private WsIdentifiant As Integer = 0
        Private WsDetailErr As String = ""

        Public ReadOnly Property SiAcces_Manager As Boolean
            Get
                Return WsSiAccesManager
            End Get
        End Property

        Public ReadOnly Property SiAcces_DRH As Boolean
            Get
                Return WsSiAccesGRH
            End Get
        End Property

        Public ReadOnly Property Appli_Accedee As String
            Get
                Return WsAppliAccedee
            End Get
        End Property

        Public ReadOnly Property Nom_CnxV3 As String
            Get
                Return WsNomCnxV3
            End Get
        End Property

        Public ReadOnly Property Detail_ErreurCnx As String
            Get
                Return WsDetailErr
            End Get
        End Property

        Public ReadOnly Property ObjetUti_CnxV3 As Virtualia.Version3.Utilisateur.UtilisateurV3
            Get
                Return WsUtilisateurV3
            End Get
        End Property

        Public ReadOnly Property Identifiant As Integer
            Get
                Return WsIdentifiant
            End Get
        End Property

        Public Function SiAccesOK() As Boolean
            Dim Chaine As String
            Dim Separateur As String
            Dim VerifV4 As Integer
            Dim TableauData(0) As String

            WsDetailErr = ""
            Chaine = WsRhFct.HashSHA(Now.Hour & WsRhDates.DateduJour)
            If Chaine.Length > 15 Then
                Separateur = Strings.Left(Chaine, 15).ToLower
            Else
                Separateur = Chaine.ToLower
            End If

            WsNomCnxV3 = ""
            WsAppliAccedee = ""
            VerifV4 = Now.Hour + Now.Day + Now.Month + Now.Year - 1951
            WsSiAccesManager = False
            WsSiAccesGRH = False

            If WsChainePostee.Contains(Separateur) = False Then
                Return False
            End If
            TableauData = Strings.Split(WsChainePostee, Separateur, -1)
            '** 1ère vérification Heure et jour de l'appel
            If IsNumeric(TableauData(1)) = False Then
                WsDetailErr = "Heure non numérique"
                Return False
            End If
            If VerifV4 = CInt(TableauData(1)) Then
                WsAppliAccedee = TableauData(2)
                If WsAppliAccedee = "" Then
                    WsDetailErr = "Application erronée"
                    Return False
                End If
            Else
                WsDetailErr = "Heure erronée"
                Return False
            End If
            If IsNumeric(TableauData(0)) Then
                WsIdentifiant = CInt(CInt(TableauData(0)) / 17)
                If WsRhFct.HashSHA(TableauData(3).ToLower & CStr(WsIdentifiant)) = TableauData(4) Then
                    WsNomCnxV3 = TableauData(3)
                Else
                    WsDetailErr = "Paramètres de connexion invalide"
                    Return False
                End If
            Else
                WsDetailErr = "Paramètres de connexion invalide"
                Return False
            End If
            If WsAppliAccedee.StartsWith("Self") = False Then
                If WsAppliAccedee.StartsWith("Manager") = True Then
                    WsSiAccesManager = True
                Else
                    WsSiAccesGRH = VerificationUtiVersion3()
                    If WsSiAccesGRH = False Then
                        WsDetailErr = "Accés GRH invalide"
                        Return False
                    End If
                End If
            End If

            Return True
        End Function

        Public Function SiOuvrirSession(ByVal AppGlobal As Virtualia.Net.Session.ObjetGlobalBase, ByVal ChaineCnx As String) As Boolean
            Dim Pw As String
            Dim Chaine As String = WsRhFct.DecryptageVirtualia(WsUtilisateurV3.MotDePasse)
            Pw = WsRhFct.HashSHA(Chaine)
            Return AppGlobal.VirClientSessionWcf.Si_ConnexionEstAutorisee(AppGlobal.VirNoBd, "V3", ChaineCnx, WsUtilisateurV3.Nom, Pw)
        End Function

        Public Function Chaine_Test(ByVal NomUser As String, ByVal Appli As String, ByVal Ide As Integer) As String
            Dim Chaine As String
            Dim Separateur As String
            Dim VerifV4 As Integer

            Chaine = WsRhFct.HashSHA(Now.Hour & WsRhDates.DateduJour)
            If Chaine.Length > 15 Then
                Separateur = Strings.Left(Chaine, 15).ToLower
            Else
                Separateur = Chaine.ToLower
            End If
            VerifV4 = Now.Hour + Now.Day + Now.Month + Now.Year - 1951
            Chaine = CStr(CInt(Ide * 17)) & Separateur & VerifV4 & Separateur & Appli & Separateur
            Chaine &= NomUser & Separateur & WsRhFct.HashSHA(NomUser.ToLower & CStr(Ide))
            Return Chaine
        End Function

        Private Function VerificationUtiVersion3() As Boolean
            If WsNomCnxV3 = "" Then
                Return False
            End If
            WsUtilisateurV3 = New Virtualia.Version3.Utilisateur.UtilisateurV3(System.Configuration.ConfigurationManager.AppSettings("RepertoireUtilisateurV3"), WsNomCnxV3)
            If WsUtilisateurV3 IsNot Nothing AndAlso WsUtilisateurV3.Nom.ToUpper = WsNomCnxV3.ToUpper Then
                Return True
            End If
            Return False
        End Function

        Public Sub New(ByVal ChainePOST As String)
            WsChainePostee = ChainePOST
            WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates
            WsRhFct = New Virtualia.Systeme.Fonctions.Generales
        End Sub
    End Class
End Namespace