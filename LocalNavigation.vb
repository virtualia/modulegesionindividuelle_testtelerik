﻿Option Explicit On
Option Strict On
Option Compare Text
Namespace Individuel
    Public Class LocalNavigation
        Private WsParent As Virtualia.Net.Session.ObjetSession
        Private WsDossierPER As Virtualia.Net.Individuel.DossierIndividu
        Private WsIdentifiantCourant As Integer = 0

        Public ReadOnly Property VParent() As Virtualia.Net.Session.ObjetSession
            Get
                Return WsParent
            End Get
        End Property

        Public Property DossierPER As Virtualia.Net.Individuel.DossierIndividu
            Get
                Return WsDossierPER
            End Get
            Set(ByVal value As Virtualia.Net.Individuel.DossierIndividu)
                WsDossierPER = value
            End Set
        End Property

        Public Property Identifiant_Courant() As Integer
            Get
                Return WsIdentifiantCourant
            End Get
            Set(ByVal value As Integer)
                WsIdentifiantCourant = value
            End Set
        End Property

        Public Sub New(ByVal Host As Virtualia.Net.Session.ObjetSession)
            WsParent = Host
        End Sub
    End Class
End Namespace