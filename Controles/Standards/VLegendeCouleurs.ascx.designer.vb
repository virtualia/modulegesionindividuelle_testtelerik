﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Controles_VLegendeCouleurs
    
    '''<summary>
    '''Contrôle VLegende.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VLegende As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiCouleur00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCouleur00 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiLegende00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLegende00 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiCouleur01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCouleur01 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiLegende01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLegende01 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiCouleur02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCouleur02 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiLegende02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLegende02 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiCouleur03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCouleur03 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiLegende03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLegende03 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiCouleur04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCouleur04 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiLegende04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLegende04 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiCouleur05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCouleur05 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiLegende05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLegende05 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiCouleur06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCouleur06 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiLegende06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLegende06 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiCouleur07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCouleur07 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiLegende07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLegende07 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiCouleur08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCouleur08 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiLegende08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLegende08 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiCouleur09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCouleur09 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiLegende09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLegende09 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiCouleur10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCouleur10 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiLegende10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLegende10 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiCouleur11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCouleur11 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiLegende11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLegende11 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiCouleur12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCouleur12 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiLegende12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLegende12 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiCouleur13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCouleur13 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiLegende13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLegende13 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiCouleur14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCouleur14 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiLegende14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLegende14 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiCouleur15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCouleur15 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiLegende15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLegende15 As Global.Telerik.Web.UI.RadLabel
End Class
