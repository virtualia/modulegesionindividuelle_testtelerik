﻿Option Strict On
Option Explicit On
Option Compare Text
Partial Class Controles_VMessage
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Retour_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
    Public Event ValeurRetour As Retour_MsgEventHandler
    Private WsNomStateCache As String = "VMessageModal"
    Private WsCtl_Cache As Virtualia.Net.VCaches.CacheControleMessage

    Protected Overridable Sub ReponseRetour(ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
        RaiseEvent ValeurRetour(Me, e)
    End Sub

    Private Property CacheVirControle As Virtualia.Net.VCaches.CacheControleMessage
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.VCaches.CacheControleMessage)
            End If
            Dim NewCache As Virtualia.Net.VCaches.CacheControleMessage
            NewCache = New Virtualia.Net.VCaches.CacheControleMessage
            Return NewCache
        End Get
        Set(value As Virtualia.Net.VCaches.CacheControleMessage)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Public WriteOnly Property AfficherMessage() As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        Set(ByVal value As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
            Dim TableauW(0) As String
            WsCtl_Cache = CacheVirControle

            WsCtl_Cache.Emetteur = value.Emetteur
            EtiTitre.Text = value.TitreMessage
            EtiMsg.Text = value.ContenuMessage
            WsCtl_Cache.ChaineDatas = value.ChaineDatas
            WsCtl_Cache.NumeroObjet = value.NumeroObjet
            CacheVirControle = WsCtl_Cache

            TableauW = Strings.Split(value.NatureMessage, ";", -1)
            CmdOui.Text = TableauW(0)
            If TableauW.Count = 1 Then
                CellNon.Visible = False
                CellCancel.Visible = False
                Exit Property
            End If
            CellNon.Visible = True
            CmdNon.Text = TableauW(1)
            If TableauW.Count = 2 Then
                CellCancel.Visible = False
                Exit Property
            End If
            CellCancel.Visible = True
            CmdCancel.Text = TableauW(2)
        End Set
    End Property

    Protected Sub Cmd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CmdOui.Click, CmdNon.Click, CmdCancel.Click
        WsCtl_Cache = CacheVirControle
        If WsCtl_Cache.Emetteur = "" Then
            Exit Sub
        End If
        Dim Reponse As String = CType(sender, System.Web.UI.WebControls.Button).Text
        Dim Evenement As New Virtualia.Systeme.Evenements.MessageRetourEventArgs(WsCtl_Cache.Emetteur, WsCtl_Cache.NumeroObjet, WsCtl_Cache.ChaineDatas, Reponse)
        ReponseRetour(Evenement)
    End Sub

End Class
