﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Controles_VFiltreListe
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateCache As String = "VListeFiltree"
    Private WsCtl_Cache As Virtualia.Net.VCaches.CacheControleListe
    '
    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Private Property CacheVirControle As Virtualia.Net.VCaches.CacheControleListe
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.VCaches.CacheControleListe)
            End If
            Dim NewCache As Virtualia.Net.VCaches.CacheControleListe
            NewCache = New Virtualia.Net.VCaches.CacheControleListe
            Return NewCache
        End Get
        Set(value As Virtualia.Net.VCaches.CacheControleListe)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Public Property SiColonneSelect() As Boolean
        Get
            Return GridDonnee.AutoGenerateSelectButton
        End Get
        Set(ByVal value As Boolean)
            GridDonnee.AutoGenerateSelectButton = value
        End Set
    End Property

    Public Property SiListeFiltreVisible() As Boolean
        Get
            Return LstFiltres.Visible
        End Get
        Set(ByVal value As Boolean)
            LstFiltres.Visible = value
        End Set
    End Property

    Public Property EtiTextFiltre As String
        Get
            Return LstFiltres.EtiText
        End Get
        Set(value As String)
            LstFiltres.EtiText = value
        End Set
    End Property

    Public Property EtiTextWidth As System.Web.UI.WebControls.Unit
        Get
            Return LstFiltres.EtiWidth
        End Get
        Set(value As System.Web.UI.WebControls.Unit)
            LstFiltres.EtiWidth = value
        End Set
    End Property

    Public Property CadreWidth() As System.Web.UI.WebControls.Unit
        Get
            Return GridDonnee.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            GridDonnee.Width = value
        End Set
    End Property

    Public Property BackColorCaption() As System.Drawing.Color
        Get
            Return GridDonnee.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            GridDonnee.BackColor = value
            GridDonnee.HeaderStyle.BackColor = value
        End Set
    End Property

    Public Property ForeColorCaption() As System.Drawing.Color
        Get
            Return GridDonnee.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            GridDonnee.ForeColor = value
            GridDonnee.HeaderStyle.ForeColor = value
        End Set
    End Property

    Public Property BackColorRow() As System.Drawing.Color
        Get
            Return GridDonnee.RowStyle.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            GridDonnee.RowStyle.BackColor = value
        End Set
    End Property

    Public Property ForeColorRow() As System.Drawing.Color
        Get
            Return GridDonnee.RowStyle.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            GridDonnee.RowStyle.ForeColor = value
        End Set
    End Property

    Public Property TaillePage() As Integer
        Get
            Return GridDonnee.PageSize
        End Get
        Set(ByVal value As Integer)
            GridDonnee.PageSize = value
        End Set
    End Property

    Public ReadOnly Property TotalLignes() As Integer
        Get
            Dim VCache As List(Of String) = V_Liste
            If VCache Is Nothing Then
                Return 0
            End If
            Return VCache.Count - 1
        End Get
    End Property

    Public Property Centrage_Colonne(ByVal Index As Integer) As Short
        Get
            If Index > GridDonnee.Columns.Count Then
                Return 0
                Exit Property
            End If
            Select Case GridDonnee.Columns.Item(Index).ItemStyle.HorizontalAlign
                Case Is = HorizontalAlign.Left
                    Return 0
                Case Is = HorizontalAlign.Center
                    Return 1
                Case Is = HorizontalAlign.Right
                    Return 2
                Case Else
                    Return 0
            End Select
        End Get
        Set(ByVal value As Short)
            If Index > GridDonnee.Columns.Count Then
                Exit Property
            End If
            Select Case value
                Case 0 'Left
                    GridDonnee.Columns.Item(Index).ItemStyle.HorizontalAlign = HorizontalAlign.Left
                Case 1 'Centré
                    GridDonnee.Columns.Item(Index).ItemStyle.HorizontalAlign = HorizontalAlign.Center
                Case 2 'Right
                    GridDonnee.Columns.Item(Index).ItemStyle.HorizontalAlign = HorizontalAlign.Right
            End Select
        End Set
    End Property

    Public WriteOnly Property V_Filtres(ByVal Libelles As List(Of String)) As List(Of String)
        Set(ByVal value As List(Of String))
            LstFiltres.V_Libel = Libelles
            LstFiltres.V_Liste = value
        End Set
    End Property

    Public Property V_Liste() As List(Of String)
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Liste_Valeurs
        End Get
        Set(ByVal value As List(Of String))
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Liste_Valeurs = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_LibelCaption() As List(Of String)
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Libelles_Caption
        End Get
        Set(ByVal value As List(Of String))
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Libelles_Caption = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_LibelColonne() As List(Of String)
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Libelles_Colonne
        End Get
        Set(ByVal value As List(Of String))
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Libelles_Colonne = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_ItemSelectionne() As String
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Valeur_ItemSelectionne
        End Get
        Set(ByVal value As String)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Valeur_ItemSelectionne = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_NumColonne_Filtre As Integer
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Colonne_Filtre
        End Get
        Set(value As Integer)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Colonne_Filtre = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call FaireListe(0)
        DDateFin.DateDebut = HSelDebut.Value
    End Sub

    Protected Sub GridDonnee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridDonnee.SelectedIndexChanged
        Try
            V_ItemSelectionne = GridDonnee.SelectedDataKey.Item(0).ToString

            Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
            Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(GridDonnee.SelectedDataKey.Item(0).ToString)
            Saisie_Change(Evenement)
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub FaireListe(ByVal NoPage As Integer)
        Dim LstValeurs As List(Of String)
        Dim LstLibels As List(Of String)
        Dim LstColonnes As List(Of String)
        Dim TableauCol(0) As String
        Dim IndiceI As Integer
        Dim IndiceA As Integer
        Dim VClasseGrid As Virtualia.Systeme.Datas.ObjetIlistGrid
        Dim Datas As ArrayList
        Dim Cpt As Integer = 0
        Dim SiAfaire As Boolean = False

        WsCtl_Cache = CacheVirControle

        If HSelFiltre.Value = "" Then
            HSelFiltre.Value = "Toutes les valeurs"
            If HSelDebut.Value = "" Then
                DDateDebut.DonText = "01/01/" & Year(Now) - 5
                HSelDebut.Value = "01/01/" & Year(Now) - 5
            End If
            If HSelFin.Value = "" Then
                DDateFin.DonText = "31/12/" & Strings.Right(WebFct.ViRhDates.DateduJour, 4)
                HSelFin.Value = "31/12/" & Strings.Right(WebFct.ViRhDates.DateduJour, 4)
            End If
        End If
        If HSelDebut.Value = "" Then
            HSelDebut.Value = "01/01/1980"
        End If
        If HSelFin.Value = "" Then
            HSelFin.Value = "31/12/2050"
        End If

        'Colonnes
        LstColonnes = WsCtl_Cache.Libelles_Colonne
        IndiceI = 1
        For IndiceA = 0 To GridDonnee.Columns.Count - 1
            GridDonnee.Columns.Item(IndiceA).HeaderText = "Colonne_" & IndiceI.ToString
            IndiceI += 1
        Next IndiceA

        If LstColonnes IsNot Nothing Then
            For IndiceI = 0 To LstColonnes.Count - 1
                If LstColonnes.Item(IndiceI) = "" Then
                    Exit For
                End If
                If LstColonnes.Item(IndiceI) = "Clef" Then
                    TableauCol(0) = GridDonnee.Columns.Item(IndiceI).ToString
                    GridDonnee.DataKeyNames = TableauCol
                    GridDonnee.Columns.Item(IndiceI).Visible = False
                Else
                    GridDonnee.Columns.Item(IndiceI).Visible = True
                    GridDonnee.Columns.Item(IndiceI).HeaderText = LstColonnes.Item(IndiceI)
                End If
            Next
            If IndiceI < GridDonnee.Columns.Count Then
                For IndiceA = IndiceI To GridDonnee.Columns.Count - 1
                    GridDonnee.Columns.Item(IndiceA).Visible = False
                Next IndiceA
            End If
        End If

        Datas = New ArrayList
        LstValeurs = WsCtl_Cache.Liste_Valeurs

        If LstValeurs IsNot Nothing Then
            For IndiceI = 0 To LstValeurs.Count - 1
                If LstValeurs.Item(IndiceI) = "" Then
                    Exit For
                End If
                SiAfaire = False
                TableauCol = Strings.Split(LstValeurs.Item(IndiceI), VI.Tild, -1)
                If HSelFiltre.Value = "Toutes les valeurs" Then
                    SiAfaire = True
                Else
                    If TableauCol(WsCtl_Cache.Colonne_Filtre) = HSelFiltre.Value Then
                        SiAfaire = True
                    End If
                End If
                If SiAfaire = True Then
                    Select Case WebFct.ViRhDates.ComparerDates(TableauCol(0), HSelDebut.Value)
                        Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                            Select Case WebFct.ViRhDates.ComparerDates(TableauCol(0), HSelFin.Value)
                                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                    VClasseGrid = New Virtualia.Systeme.Datas.ObjetIlistGrid
                                    For IndiceA = 0 To TableauCol.Count - 1
                                        VClasseGrid.Valeur(IndiceA) = TableauCol(IndiceA)
                                    Next IndiceA
                                    Datas.Add(VClasseGrid)
                                    Cpt += 1
                            End Select
                    End Select
                End If
            Next IndiceI
        Else
            VClasseGrid = New Virtualia.Systeme.Datas.ObjetIlistGrid
            Datas.Add(VClasseGrid)
        End If
        GridDonnee.DataSource = Datas
        GridDonnee.DataBind()

        LstLibels = V_LibelCaption
        Select Case Cpt
            Case Is = 0
                GridDonnee.Caption = LstLibels.Item(0)
            Case Is = 1
                GridDonnee.Caption = LstLibels.Item(1)
            Case Else
                GridDonnee.Caption = GridDonnee.Rows.Count.ToString & Space(1) & LstLibels.Item(2)
        End Select
    End Sub

    Protected Sub LstFiltres_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles LstFiltres.ValeurChange
        HSelFiltre.Value = e.Valeur
    End Sub

    Protected Sub FDateDebut_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles DDateDebut.ValeurChange
        HSelDebut.Value = e.Valeur
    End Sub

    Protected Sub FDateFin_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles DDateFin.ValeurChange
        HSelFin.Value = e.Valeur
    End Sub

End Class
