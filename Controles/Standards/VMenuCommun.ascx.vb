﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VMenuCommun
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Public Delegate Sub Dossier_ClickEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event Menu_Click As Dossier_ClickEventHandler
    Private WsNomState As String = "TreeMenu"
    Private WsListeMenu As List(Of Virtualia.Net.Controles.ItemMenuCommun)
    Private WsAppelant As String
    Private expanded As TreeNode
    Private NodeExpanded As TreeNode

    Protected Overridable Sub VMenu_Click(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent Menu_Click(Me, e)
    End Sub

    Public Property V_Appelant As String
        Get
            Return WsAppelant
        End Get
        Set(ByVal value As String)
            WsAppelant = value
        End Set
    End Property

    Private Property V_DataPathItemSel As String
        Get
            Dim CacheArmoire As ArrayList

            CacheArmoire = CType(Me.ViewState(WsNomState), ArrayList)
            If CacheArmoire Is Nothing Then
                Return ""
            End If
            Return CacheArmoire(0).ToString
        End Get
        Set(ByVal value As String)
            Dim CacheArmoire As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                Me.ViewState.Remove(WsNomState)
            End If
            CacheArmoire = New ArrayList
            CacheArmoire.Add(value)
            Me.ViewState.Add(WsNomState, CacheArmoire)
        End Set
    End Property

    Private Sub FaireListeOrganisee()
        Dim IndiceA As Integer
        Dim RuptureN1 As String = ""
        Dim RuptureN As String = ""
        Dim RuptureN3 As String = ""
        Dim comp As String = ""
        Dim comp3 As String = ""
        Dim UrlImageN1 = "~/Images/Armoire/BleuFermer16.bmp"
        Dim UrlImagePer = "~/Images/Armoire/FicheBleue.bmp"
        Dim NewNoeudN As Telerik.Web.UI.RadTreeNode = Nothing
        Dim NewNoeudN1 As Telerik.Web.UI.RadTreeNode = Nothing
        Dim NewNoeudN2 As Telerik.Web.UI.RadTreeNode = Nothing
        Dim NewNoeudN3 As Telerik.Web.UI.RadTreeNode = Nothing


        TreeListeMenu.Nodes.Clear()
        'TreeListeMenu.LevelStyles.Clear()

        Call ChargerMenu()

        For IndiceA = 0 To WsListeMenu.Count - 1
            Select Case WsListeMenu.Item(IndiceA).Groupe

                'Case Is <> RuptureN
                '    RuptureN = "INFORMATIONS PERSONNELLES"
                '    NewNoeudN1 = Nothing

                '    NewNoeudN = New TreeNode(RuptureN, "N1" & VI.Tild & IndiceA.ToString)
                '    NewNoeudN.PopulateOnDemand = False
                '    NewNoeudN.SelectAction = TreeNodeSelectAction.SelectExpand
                '    TreeListeMenu.Nodes.Add(NewNoeudN)

                Case Is <> RuptureN1
                    If IndiceA < 19 Then
                        RuptureN = "INFORMATIONS PERSONNELLES"
                    ElseIf IndiceA > 18 And IndiceA < 48 Then
                        RuptureN = "INFORMATIONS ADMINISTRATIVES"
                    Else
                        RuptureN = "INFORMATIONS COMPLEMENTAIRES "
                    End If
                    Select Case RuptureN
                        Case Is <> comp
                            NewNoeudN = New Telerik.Web.UI.RadTreeNode()
                            NewNoeudN.Text = RuptureN
                            NewNoeudN.Value = "N1" & VI.Tild & IndiceA.ToString
                            'NewNoeudN = New TreeNode(RuptureN, "N1" & VI.Tild & IndiceA.ToString)
                    End Select

                    'NewNoeudN. = False
                    'NewNoeudN. = TreeNodeSelectAction.SelectExpand

                    RuptureN1 = WsListeMenu.Item(IndiceA).Groupe
                    NewNoeudN2 = Nothing

                    NewNoeudN1 = New Telerik.Web.UI.RadTreeNode()
                    NewNoeudN1.Text = RuptureN1
                    NewNoeudN1.Value = "N1" & VI.Tild & IndiceA.ToString
                    NewNoeudN1.ImageUrl = UrlImageN1
                    'NewNoeudN1.PopulateOnDemand = False
                    'NewNoeudN1.SelectAction = TreeNodeSelectAction.SelectExpand
                    If RuptureN <> "INFORMATIONS COMPLEMENTAIRES " Then
                        NewNoeudN.Nodes.Add(NewNoeudN1)
                    End If
                    If RuptureN <> comp Then
                        TreeListeMenu.Nodes.Add(NewNoeudN)
                    End If
            End Select

            If NewNoeudN1 IsNot Nothing Then
                If IndiceA > 2 And IndiceA < 6 Then
                    RuptureN3 = "Autres adresses"
                    If RuptureN3 <> comp3 Then
                        NewNoeudN3 = New Telerik.Web.UI.RadTreeNode()
                        NewNoeudN3.Text = RuptureN3
                        NewNoeudN3.ImageUrl = UrlImageN1
                        'NewNoeudN3.PopulateOnDemand = False
                        'NewNoeudN3.SelectAction = TreeNodeSelectAction.SelectExpand
                        NewNoeudN1.Nodes.Add(NewNoeudN3)
                    End If
                    comp3 = RuptureN3

                    NewNoeudN2 = New Telerik.Web.UI.RadTreeNode()
                    NewNoeudN2.Text = WsListeMenu.Item(IndiceA).Intitule
                    NewNoeudN2.Value = CStr(WsListeMenu.Item(IndiceA).Index_Vue)
                    NewNoeudN2.ImageUrl = UrlImagePer
                    'NewNoeudN2.PopulateOnDemand = False
                    'NewNoeudN2.SelectAction = TreeNodeSelectAction.SelectExpand

                    NewNoeudN3.Nodes.Add(NewNoeudN2)
                ElseIf IndiceA > 13 And IndiceA < 20 Then
                    RuptureN3 = "CV "
                    If RuptureN3 <> comp3 Then
                        NewNoeudN3 = New Telerik.Web.UI.RadTreeNode()
                        NewNoeudN3.Text = RuptureN3
                        NewNoeudN3.ImageUrl = UrlImageN1
                        'NewNoeudN3.PopulateOnDemand = False
                        'NewNoeudN3.SelectAction = TreeNodeSelectAction.SelectExpand
                        NewNoeudN1.Nodes.Add(NewNoeudN3)
                    End If
                    comp3 = RuptureN3

                    NewNoeudN2 = New Telerik.Web.UI.RadTreeNode()
                    NewNoeudN2.Text = WsListeMenu.Item(IndiceA).Intitule
                    NewNoeudN2.Value = CStr(WsListeMenu.Item(IndiceA).Index_Vue)
                    NewNoeudN2.ImageUrl = UrlImagePer
                    'NewNoeudN2.PopulateOnDemand = False
                    'NewNoeudN2.SelectAction = TreeNodeSelectAction.SelectExpand
                    NewNoeudN3.Nodes.Add(NewNoeudN2)

                ElseIf IndiceA > 23 And IndiceA < 29 Then
                    RuptureN3 = "Multi-affectations "
                    If RuptureN3 <> comp3 Then
                        NewNoeudN3 = New Telerik.Web.UI.RadTreeNode()
                        NewNoeudN3.Text = RuptureN3
                        NewNoeudN3.ImageUrl = UrlImageN1
                        'NewNoeudN3.PopulateOnDemand = False
                        'NewNoeudN3.SelectAction = TreeNodeSelectAction.SelectExpand
                        NewNoeudN1.Nodes.Add(NewNoeudN3)
                    End If
                    comp3 = RuptureN3


                    NewNoeudN2 = New Telerik.Web.UI.RadTreeNode()
                    NewNoeudN2.Text = WsListeMenu.Item(IndiceA).Intitule
                    NewNoeudN2.Value = CStr(WsListeMenu.Item(IndiceA).Index_Vue)
                    NewNoeudN2.ImageUrl = UrlImagePer
                    'NewNoeudN2.PopulateOnDemand = False
                    'NewNoeudN2.SelectAction = TreeNodeSelectAction.SelectExpand
                    NewNoeudN3.Nodes.Add(NewNoeudN2)


                Else

                    NewNoeudN2 = New Telerik.Web.UI.RadTreeNode()
                    NewNoeudN2.Text = WsListeMenu.Item(IndiceA).Intitule
                    NewNoeudN2.Value = CStr(WsListeMenu.Item(IndiceA).Index_Vue)
                    NewNoeudN2.ImageUrl = UrlImagePer
                    'NewNoeudN2.PopulateOnDemand = False
                    'NewNoeudN2.SelectAction = TreeNodeSelectAction.SelectExpand
                    If RuptureN <> "INFORMATIONS COMPLEMENTAIRES " Then
                        NewNoeudN1.Nodes.Add(NewNoeudN2)
                    Else
                        NewNoeudN.Nodes.Add(NewNoeudN2)
                    End If
                End If
            End If
            comp = RuptureN
        Next IndiceA

        Call StylerlArmoire(2)

        RuptureN1 = V_DataPathItemSel
        If RuptureN1 <> "" Then
            Try
                NewNoeudN1 = TreeListeMenu.FindNodeByText(RuptureN1)
                NewNoeudN1.Selected = True
            Catch ex As Exception
                NewNoeudN1 = Nothing
            End Try
        End If
        'TreeListeMenu.ExpandAll()

    End Sub


    Private Sub StylerlArmoire(ByVal Profondeur As Integer)
        Dim Vstyle As System.Web.UI.WebControls.TreeNodeStyle
        'TreeListeMenu.LevelStyles.Clear()

        If Profondeur = 1 Then

            Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None
            Vstyle.BackColor = WebFct.ConvertCouleur("#B0E0D7")

            'TreeListeMenu.LevelStyles.Add(Vstyle)
            Exit Sub
        End If

        Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
        Vstyle.ChildNodesPadding = New Unit(10)
        Vstyle.HorizontalPadding = New Unit(20)
        Vstyle.NodeSpacing = New Unit(1)
        Vstyle.Font.Name = "Trebuchet MS"
        Vstyle.Font.Italic = True
        Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
        Vstyle.Font.Bold = False
        Vstyle.BorderWidth = New Unit(1)
        Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.Ridge
        Vstyle.BackColor = WebFct.ConvertCouleur("#142425")
        Vstyle.ForeColor = WebFct.ConvertCouleur("#B0E0D7")

        'TreeListeMenu.LevelStyles.Add(Vstyle)

        If Profondeur = 2 Then

            Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None
            Vstyle.BackColor = WebFct.ConvertCouleur("#B0E0D7")

            'TreeListeMenu.LevelStyles.Add(Vstyle)
            Exit Sub
        End If

        Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
        Vstyle.ChildNodesPadding = New Unit(10)
        Vstyle.HorizontalPadding = New Unit(20)
        Vstyle.NodeSpacing = New Unit(1)
        Vstyle.Font.Name = "Trebuchet MS"
        Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
        Vstyle.Font.Italic = True
        Vstyle.Font.Bold = False
        Vstyle.BorderWidth = New Unit(0)
        Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.Ridge
        Vstyle.BackColor = WebFct.ConvertCouleur("#1C5151")
        Vstyle.ForeColor = WebFct.ConvertCouleur("#B0E0D7")

        'TreeListeMenu.LevelStyles.Add(Vstyle)

        If Profondeur = 3 Then
            Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None
            Vstyle.BackColor = WebFct.ConvertCouleur("#B0E0D7")

            'TreeListeMenu.LevelStyles.Add(Vstyle)
            Exit Sub
        End If

        Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
        Vstyle.ChildNodesPadding = New Unit(10)
        Vstyle.HorizontalPadding = New Unit(20)
        Vstyle.NodeSpacing = New Unit(1)
        Vstyle.Font.Name = "Trebuchet MS"
        Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
        Vstyle.Font.Italic = True
        Vstyle.Font.Bold = False
        Vstyle.BorderWidth = New Unit(0)
        Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.Ridge
        Vstyle.BackColor = WebFct.ConvertCouleur("#137A76")
        Vstyle.ForeColor = WebFct.ConvertCouleur("#B0E0D7")

        'TreeListeMenu.LevelStyles.Add(Vstyle)

        If Profondeur = 4 Then
            Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
            Vstyle.Font.Name = "Trebuchet MS"
            Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
            Vstyle.Font.Italic = True
            Vstyle.Font.Bold = False
            Vstyle.BorderWidth = New Unit(0)
            Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None
            Vstyle.BackColor = WebFct.ConvertCouleur("#B0E0D7")

            'TreeListeMenu.LevelStyles.Add(Vstyle)
        End If

    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call FaireListeOrganisee()
        'If NodeExpanded IsNot Nothing Then
        '    If NodeExpanded.Text = "INFORMATIONS PERSONNELLES" Then
        '        TreeListeMenu.Nodes(0).Expand()
        '        TreeListeMenu.Nodes(1).Collapse()
        '        TreeListeMenu.Nodes(2).Collapse()
        '    ElseIf NodeExpanded.Text = "INFORMATIONS ADMINISTRATIVES" Then
        '        TreeListeMenu.Nodes(0).Collapse()
        '        TreeListeMenu.Nodes(1).Expand()
        '        TreeListeMenu.Nodes(2).Collapse()
        '    ElseIf NodeExpanded.Text = "INFORMATIONS COMPLEMENTAIRES " Then
        '        TreeListeMenu.Nodes(0).Collapse()
        '        TreeListeMenu.Nodes(1).Collapse()
        '        TreeListeMenu.Nodes(2).Expand()
        '    End If
        'Else
        '    TreeListeMenu.CollapseAll()
        'End If
    End Sub

    Private Sub ChargerMenu()
        Dim Chaine As String
        Dim Menu As Virtualia.Net.Controles.ItemMenuCommun
        WsListeMenu = New List(Of Virtualia.Net.Controles.ItemMenuCommun)

        Select Case WsAppelant
            Case "PER"
                Chaine = "Etat-civil et Domiciliations"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Etat-civil"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaCivil
                Menu.Index_Vue = 0
                Menu.ID_Vue = "VueEtatCivil"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Pièces d’identité"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaEtranger
                Menu.Index_Vue = 1
                Menu.ID_Vue = "VuePieceIdentite"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Domicile"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAdresse
                Menu.Index_Vue = 2
                Menu.ID_Vue = "VueAdresse"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Résidence secondaire"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAdresse2
                Menu.Index_Vue = 3
                Menu.ID_Vue = "VueAdresse2"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Résidence de congés"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAdresseConge
                Menu.Index_Vue = 4
                Menu.ID_Vue = "VueResidenceConge"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Adresse à l'étranger"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAdresseHorsFr
                Menu.Index_Vue = 5
                Menu.ID_Vue = "VueAdresseEtranger"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Domiciliation bancaire"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaBanque
                Menu.Index_Vue = 6
                Menu.ID_Vue = "VueBanque"
                WsListeMenu.Add(Menu)


                'Menu = New Virtualia.Net.Controles.ItemMenuCommun
                'Menu.Groupe = Chaine
                'Menu.Intitule = "Adresse personnelle"
                'Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                'Menu.Numero_Objet = VI.ObjetPer.ObaAdresse
                'Menu.Index_Vue = 1
                'Menu.ID_Vue = "VueAdresse"
                'WsListeMenu.Add(Menu)

                'Menu = New Virtualia.Net.Controles.ItemMenuCommun
                'Menu.Groupe = Chaine
                'Menu.Intitule = "Fiche Banque RIB et IBAN"
                'Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                'Menu.Numero_Objet = VI.ObjetPer.ObaBanque
                'Menu.Index_Vue = 2
                'Menu.ID_Vue = "VueBanque"
                'WsListeMenu.Add(Menu)

                'Chaine = "ENFANTS"
                'Menu = New Virtualia.Net.Controles.ItemMenuCommun
                'Menu.Groupe = Chaine
                'Menu.Intitule = "Liste des enfants"
                'Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                'Menu.Numero_Objet = VI.ObjetPer.ObaEnfant
                'Menu.Index_Vue = 3
                'Menu.ID_Vue = "VueEnfant"
                'WsListeMenu.Add(Menu)


                Chaine = "Situation familiale"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Enfants"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaEnfant
                Menu.Index_Vue = 7
                Menu.ID_Vue = "VueEnfant"
                WsListeMenu.Add(Menu)

                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Conjoint"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaConjoint
                Menu.Index_Vue = 8
                Menu.ID_Vue = "VueConjoint"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Personne à prévenir"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaPrevenir
                Menu.Index_Vue = 9
                Menu.ID_Vue = "VuePrevenir"
                WsListeMenu.Add(Menu)

                Chaine = "Décorations"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Décorations attribuées"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaDecoration
                Menu.Index_Vue = 10
                Menu.ID_Vue = "VueDecoration"
                WsListeMenu.Add(Menu)

                Chaine = "Diplômes et qualifications"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Diplômes"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaDiplome
                Menu.Index_Vue = 11
                Menu.ID_Vue = "VueDiplome"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Qualifications"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaCompetence
                Menu.Index_Vue = 12
                Menu.ID_Vue = "VueQualif"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Spécialités"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaSpecialite
                Menu.Index_Vue = 13
                Menu.ID_Vue = "VueSpecialite"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Thèse"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaThese
                Menu.Index_Vue = 14
                Menu.ID_Vue = "Vuethese"
                WsListeMenu.Add(Menu)



                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Expérience"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaExperience
                Menu.Index_Vue = 15
                Menu.ID_Vue = "VueExperience"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Langues étrangères"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaLangue
                Menu.Index_Vue = 16
                Menu.ID_Vue = "VueLangueEtrangeres"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Acquis techniques"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAcquis
                Menu.Index_Vue = 17
                Menu.ID_Vue = "VueAcquis"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Stages de formation"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaStageCv
                Menu.Index_Vue = 18
                Menu.ID_Vue = "VueStage"
                WsListeMenu.Add(Menu)



                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Loisirs"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaLoisir
                Menu.Index_Vue = 19
                Menu.ID_Vue = "VueLoisir"
                WsListeMenu.Add(Menu)


                '2) INFORMATIONS ADMINISTRATIVES**************************************************************************


                Chaine = "Affectations"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Administration / Etablissement"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaSociete
                Menu.Index_Vue = 20
                Menu.ID_Vue = "VueAdministration"
                WsListeMenu.Add(Menu)



                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Identifications"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaExterne
                Menu.Index_Vue = 21
                Menu.ID_Vue = "VueIdentification"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Adresse professionnelle"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAdrPro
                Menu.Index_Vue = 22
                Menu.ID_Vue = "VueAdressePro"
                WsListeMenu.Add(Menu)



                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Affectation fonctionnelle"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaOrganigramme
                Menu.Index_Vue = 23
                Menu.ID_Vue = "VueAffectation"
                WsListeMenu.Add(Menu)



                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Seconde affectation"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAffectation2nd
                Menu.Index_Vue = 24
                Menu.ID_Vue = "VueAffectation2nd"
                WsListeMenu.Add(Menu)



                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Troisième affectation"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAffectation2nd
                Menu.Index_Vue = 25
                Menu.ID_Vue = "VueAffectation3rd"
                WsListeMenu.Add(Menu)



                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Quatrième affectation"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAffectation2nd
                Menu.Index_Vue = 26
                Menu.ID_Vue = "VueAffectation4rth"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Cinquième affectation"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAffectation2nd
                Menu.Index_Vue = 27
                Menu.ID_Vue = "VueAffectation5th"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Sixième  affectation"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAffectation2nd
                Menu.Index_Vue = 28
                Menu.ID_Vue = "VueAffectation6th"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Affectation LOLF"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaLolf
                Menu.Index_Vue = 29
                Menu.ID_Vue = "VueLolf"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Affectation emploi budgétaire"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaPostebud
                Menu.Index_Vue = 30
                Menu.ID_Vue = "VueEmploiBud"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Activités annexes"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaActiAnnexes
                Menu.Index_Vue = 31
                Menu.ID_Vue = "VueActiAnnexe"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Activités internes"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaActiInternes
                Menu.Index_Vue = 32
                Menu.ID_Vue = "VueActiInterne"
                WsListeMenu.Add(Menu)




                Chaine = "Carrière "
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Situation statutaire"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaStatut
                Menu.Index_Vue = 33
                Menu.ID_Vue = "VueStatut"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Position et modalité de service"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaActivite
                Menu.Index_Vue = 34
                Menu.ID_Vue = "VuePosition"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Corps, Grade et échelon"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaGrade
                Menu.Index_Vue = 35
                Menu.ID_Vue = "VueGrade"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Intégration et Reclassement"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaIntegrationCorps
                Menu.Index_Vue = 36
                Menu.ID_Vue = "VueIntegration"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Bonification"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaNotation
                Menu.Index_Vue = 37
                Menu.ID_Vue = "VueBonification"
                WsListeMenu.Add(Menu)



                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Sanctions"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaSanction
                Menu.Index_Vue = 38
                Menu.ID_Vue = "VueSanction"
                WsListeMenu.Add(Menu)




                Chaine = "Double carrière - Détachement interne"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Situation statutaire - seconde carrière"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaStatutDetache
                Menu.Index_Vue = 39
                Menu.ID_Vue = "VueStatutDetache"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Position et modalité de service - seconde carrière"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaPositionDetache
                Menu.Index_Vue = 40
                Menu.ID_Vue = "VuePositionDetache"
                WsListeMenu.Add(Menu)



                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Corps, Grade et échelon - seconde carrière"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaGradeDetache
                Menu.Index_Vue = 41
                Menu.ID_Vue = "VueGradeDetache"
                WsListeMenu.Add(Menu)



                Chaine = "Anciennetés de services et cumuls d’emploi"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Services civils"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaOrigine
                Menu.Index_Vue = 42
                Menu.ID_Vue = "VueServiceCivil"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Services militaires"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaArmee
                Menu.Index_Vue = 43
                Menu.ID_Vue = "VueArmee"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Cumuls d'emploi et de rémunération"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaCumulRem
                Menu.Index_Vue = 44
                Menu.ID_Vue = "VueCumulRem"
                WsListeMenu.Add(Menu)



                Chaine = "Hygiène et sécurité"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Visite médicale"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaMedical
                Menu.Index_Vue = 45
                Menu.ID_Vue = "VueMedical"
                WsListeMenu.Add(Menu)



                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Handicap"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaHandicap
                Menu.Index_Vue = 46
                Menu.ID_Vue = "VueHandicap"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Vaccination"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaVaccination
                Menu.Index_Vue = 47
                Menu.ID_Vue = "VueVaccination"
                WsListeMenu.Add(Menu)



                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Prévention et risques professionnels"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaPrevention
                Menu.Index_Vue = 48
                Menu.ID_Vue = "VuePrevention"
                WsListeMenu.Add(Menu)


                '3) INFORMATIONS COMPLEMENTAIRES *********************************

                Chaine = "INFORMATIONS COMPLEMENTAIRES"
                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Arrêts de droit et justificatifs"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAbsence
                Menu.Index_Vue = 49
                Menu.ID_Vue = "VueAbsence"
                WsListeMenu.Add(Menu)



                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Agenda d'évènements"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAgenda
                Menu.Index_Vue = 50
                Menu.ID_Vue = "VueAgenda"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Mandats de délégation	"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaEligible
                Menu.Index_Vue = 51
                Menu.ID_Vue = "VueMandat"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Avantages en nature"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaAvantage
                Menu.Index_Vue = 52
                Menu.ID_Vue = "VueAvantage"
                WsListeMenu.Add(Menu)



                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Affiliations sociales"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaCaisse
                Menu.Index_Vue = 53
                Menu.ID_Vue = "VueAffiliation"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Mutuelle"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaMutuelle
                Menu.Index_Vue = 54
                Menu.ID_Vue = "VueMutuelle"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Matériel mis à disposition"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaMateriel
                Menu.Index_Vue = 55
                Menu.ID_Vue = "VueMateriel"
                WsListeMenu.Add(Menu)


                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Annotations"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaMemento
                Menu.Index_Vue = 56
                Menu.ID_Vue = "VueAnnotation"
                WsListeMenu.Add(Menu)



                Menu = New Virtualia.Net.Controles.ItemMenuCommun
                Menu.Groupe = Chaine
                Menu.Intitule = "Documents dématérialisés"
                Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                Menu.Numero_Objet = VI.ObjetPer.ObaDocuments
                Menu.Index_Vue = 57
                Menu.ID_Vue = "VueDocument"
                WsListeMenu.Add(Menu)



                '    Chaine = "Situation ADMINISTRATIVE"
                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Administration / Etablissement"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '    Menu.Numero_Objet = VI.ObjetPer.ObaSociete
                '    Menu.Index_Vue = 4
                '    Menu.ID_Vue = "VueAdministration"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Services antèrieurs"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '    Menu.Numero_Objet = VI.ObjetPer.ObaOrigine
                '    Menu.Index_Vue = 5
                '    Menu.ID_Vue = "VueAdmOrigine"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    If System.Configuration.ConfigurationManager.AppSettings("TypeBudget") = "SIFAC" Then
                '        Menu.Intitule = "Ventilation budgétaire"
                '        Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '        Menu.Numero_Objet = 180
                '        Menu.Index_Vue = 6
                '        Menu.ID_Vue = "VueLOLF"
                '    Else
                '        Menu.Intitule = "Affectation LOLF"
                '        Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '        Menu.Numero_Objet = VI.ObjetPer.ObaLolf
                '        Menu.Index_Vue = 6
                '        Menu.ID_Vue = "VueLOLF"
                '    End If
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Affectation fonctionnelle"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '    Menu.Numero_Objet = VI.ObjetPer.ObaOrganigramme
                '    Menu.Index_Vue = 7
                '    Menu.ID_Vue = "VueAffectation"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Adresse professionnelle"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '    Menu.Numero_Objet = VI.ObjetPer.ObaAdrPro
                '    Menu.Index_Vue = 8
                '    Menu.ID_Vue = "VueAdressePro"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Affiliations sociales"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '    Menu.Numero_Objet = VI.ObjetPer.ObaCaisse
                '    Menu.Index_Vue = 9
                '    Menu.ID_Vue = "VueCaisse"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Mutuelle"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '    Menu.Numero_Objet = VI.ObjetPer.ObaMutuelle
                '    Menu.Index_Vue = 10
                '    Menu.ID_Vue = "VueMutuelle"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Absences et congés maladie"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '    Menu.Numero_Objet = VI.ObjetPer.ObaAbsence
                '    Menu.Index_Vue = 11
                '    Menu.ID_Vue = "VueAbsence"
                '    WsListeMenu.Add(Menu)

                '    Chaine = "CARRIERE"
                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Situation statutaire"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '    Menu.Numero_Objet = VI.ObjetPer.ObaStatut
                '    Menu.Index_Vue = 12
                '    Menu.ID_Vue = "VueStatut"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Position et modalité de service"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '    Menu.Numero_Objet = VI.ObjetPer.ObaActivite
                '    Menu.Index_Vue = 13
                '    Menu.ID_Vue = "VuePosition"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Corps, Grade et échelon"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '    Menu.Numero_Objet = VI.ObjetPer.ObaGrade
                '    Menu.Index_Vue = 14
                '    Menu.ID_Vue = "VueGrade"
                '    WsListeMenu.Add(Menu)

                '    Chaine = "DOUBLE CARRIERE - DETACHEMENT INTERNE"
                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Situation statutaire"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '    Menu.Numero_Objet = VI.ObjetPer.ObaStatutDetache
                '    Menu.Index_Vue = 15
                '    Menu.ID_Vue = "VueStatutDetache"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Position et modalité de service"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '    Menu.Numero_Objet = VI.ObjetPer.ObaPositionDetache
                '    Menu.Index_Vue = 16
                '    Menu.ID_Vue = "VuePositionDetache"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Corps, Grade et échelon"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '    Menu.Numero_Objet = VI.ObjetPer.ObaGradeDetache
                '    Menu.Index_Vue = 17
                '    Menu.ID_Vue = "VueGradeDetache"
                '    WsListeMenu.Add(Menu)

                '    Chaine = "INDEMNITES ET HEURES SUPPLEMENTAIRES"
                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Primes et indemnités permanentes"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '    Menu.Numero_Objet = VI.ObjetPer.ObaIndemnite
                '    Menu.Index_Vue = 18
                '    Menu.ID_Vue = "VuePrime"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Variables de paie et allocations"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '    Menu.Numero_Objet = VI.ObjetPer.ObaVariablePaie
                '    Menu.Index_Vue = 19
                '    Menu.ID_Vue = "VueVariablePaie"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Heures supplémentaires"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueApplicatif
                '    Menu.Numero_Objet = VI.ObjetPer.ObaHeuresSup
                '    Menu.Index_Vue = 20
                '    Menu.ID_Vue = "VueHeureSup"
                '    WsListeMenu.Add(Menu)
                'Case "REF"

                '    Chaine = "REGLES GENERALES ET VALEURS PAR DEFAUT"
                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Absences"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueAbsences
                '    Menu.Numero_Objet = 1
                '    Menu.Index_Vue = 0
                '    Menu.ID_Vue = "VueAbsences"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Présences"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVuePresences
                '    Menu.Numero_Objet = 1
                '    Menu.Index_Vue = 0
                '    Menu.ID_Vue = "VuePresences"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Couleurs du planning"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVuePaie
                '    Menu.Numero_Objet = 1
                '    Menu.Index_Vue = 0
                '    Menu.ID_Vue = "VueCouleurs"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Jour ouvrable"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVuePaie
                '    Menu.Numero_Objet = 1
                '    Menu.Index_Vue = 0
                '    Menu.ID_Vue = "VueJourOuvrable"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Jours fériés mobiles"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVuePaie
                '    Menu.Numero_Objet = 1
                '    Menu.Index_Vue = 0
                '    Menu.ID_Vue = "VueJoursFeries"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Droits à congés annuels"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVuePaie
                '    Menu.Numero_Objet = 1
                '    Menu.Index_Vue = 0
                '    Menu.ID_Vue = "VueDroitsConges"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Droits à congés maladies"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVuePaie
                '    Menu.Numero_Objet = 1
                '    Menu.Index_Vue = 0
                '    Menu.ID_Vue = "VueMaladies"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Temps de travail légal"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVuePaie
                '    Menu.Numero_Objet = 1
                '    Menu.Index_Vue = 0
                '    Menu.ID_Vue = "VueTravailLegal"
                '    WsListeMenu.Add(Menu)

                '    Chaine = "REGLES LOCALES"
                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Etablissements"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueEtablissement
                '    Menu.Numero_Objet = 1
                '    Menu.Index_Vue = 0
                '    Menu.ID_Vue = "VueEtablissements"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Pays"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVuePays
                '    Menu.Numero_Objet = 1
                '    Menu.Index_Vue = 0
                '    Menu.ID_Vue = "VuePays"
                '    WsListeMenu.Add(Menu)

                '    Chaine = "CYCLES DE TRAVAIL"
                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Cycles de travail"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueCycle
                '    Menu.Numero_Objet = 1
                '    Menu.Index_Vue = 0
                '    Menu.ID_Vue = "VueCyclesTravail"
                '    WsListeMenu.Add(Menu)

                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Unités de cycle ou cycles de base"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueBaseHebdo
                '    Menu.Numero_Objet = 1
                '    Menu.Index_Vue = 0
                '    Menu.ID_Vue = "VueCyclesUnite"
                '    WsListeMenu.Add(Menu)

                '    Chaine = "REGLES DE VALORISATION"
                '    Menu = New Virtualia.Net.Controles.ItemMenuCommun
                '    Menu.Groupe = Chaine
                '    Menu.Intitule = "Règles de valorisation des heures travaillées"
                '    Menu.Point_de_Vue = VI.PointdeVue.PVueValTemps
                '    Menu.Numero_Objet = 1
                '    Menu.Index_Vue = 0
                '    Menu.ID_Vue = "VueValTemps"
                '    WsListeMenu.Add(Menu)

        End Select

    End Sub

    'Private Sub TreeListeMenu_TreeNodeExpanded(ByVal sender As Object, ByVal e As TreeNodeEventArgs) Handles TreeListeMenu.TreeNodeExpanded
    '    NodeExpanded = e.Node
    '    'TreeListeMenu.CollapseAll()
    '    'e.Node.Expand()
    'End Sub

    Private Sub TreeListeMenu_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeListeMenu.NodeClick
        V_DataPathItemSel = CType(sender, Telerik.Web.UI.RadTreeView).SelectedNode.FullPath

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs = Nothing
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(CType(sender, Telerik.Web.UI.RadTreeView).SelectedNode.Value,
                                                            CType(sender, Telerik.Web.UI.RadTreeView).SelectedNode.Text)
        VMenu_Click(Evenement)
    End Sub

End Class