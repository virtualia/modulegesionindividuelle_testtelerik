﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VReferentiel
    Inherits Virtualia.Net.Controles.SystemeReferences
    Public Delegate Sub EventHandler(ByVal sender As Object, ByVal e As EventArgs)
    Public Event RetourEventHandler As EventHandler

    Public Property SiCommandesVisible As Boolean
        Get
            Return CellCommandes.Visible
        End Get
        Set(value As Boolean)
            CellCommandes.Visible = value
            CommandeBlanc.Visible = Not value
            CommandeRetour.Visible = Not value
        End Set
    End Property

    Public Sub InitialiserPointdeVue(ByVal PtdeVue As Integer)
        Dim Predicat As Virtualia.Ressources.Predicats.PredicateSysRef
        TreeListeRef.Nodes.Clear()
        Dim Chaine As String
        Try
            Chaine = V_WebFonction.PointeurGlobal.VirModele.Item(PtdeVue - 1).Intitule
        Catch ex As Exception
            Exit Sub
        End Try
        V_NomTable = Chaine
        Predicat = New Virtualia.Ressources.Predicats.PredicateSysRef(Chaine, "")
        If V_WebFonction.PointeurContexte.SysRef_Listes.FindAll(AddressOf Predicat.RechercherTable).Count > 0 Then
            V_WebFonction.PointeurContexte.SysRef_Listes.RemoveAll(AddressOf Predicat.RechercherTable)
        End If
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Me.IsPostBack = False Then
            Exit Sub
        End If
        If V_PointdeVue = VI.PointdeVue.PVueGeneral Then
            RadioV1.Visible = False
            RadioV2.Visible = True
        Else
            RadioV1.Visible = True
            RadioV2.Visible = False
            If V_NomTable = "" Then
                Call InitialiserPointdeVue(V_PointdeVue)
            End If
        End If
        VRecherche.Text = V_Lettre
        Call FaireListesReferences()
    End Sub

    Protected Sub TreeListeRef_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeListeRef.SelectedNodeChanged
        Dim Tableaudata(0) As String
        Dim SelPVue As Integer
        Dim SelIde As Integer
        Dim SelCode As String
        Dim SelText As String
        Dim Tampon As List(Of String)
        Dim TableauSel(0) As String
        Dim I As Integer

        Tableaudata = Strings.Split(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value, VI.Tild, -1)
        If IsNumeric(Tableaudata(0)) = False Then
            Exit Sub
        End If
        If CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.SelectAction = TreeNodeSelectAction.SelectExpand Then
            Exit Sub
        End If

        SelIde = CInt(Tableaudata(0))
        SelText = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Text
        SelCode = Tableaudata(2)
        SelPVue = CInt(Tableaudata(3))

        Select Case SelPVue
            Case VI.PointdeVue.PVueGrades
                Tampon = New List(Of String)
                TableauSel = Strings.Split(SelCode, VI.PointVirgule, -1)
                For I = 0 To TableauSel.Count - 1
                    Tampon.Add(TableauSel(I))
                Next I
                V_WebFonction.PointeurContexte.TsTampon_StringList = Tampon

            Case VI.PointdeVue.PVueGrilles
                Tampon = New List(Of String)
                TableauSel = Strings.Split(SelCode, VI.PointVirgule, -1)
                For I = 0 To TableauSel.Count - 1
                    Tampon.Add(TableauSel(I))
                Next I
                V_WebFonction.PointeurContexte.TsTampon_StringList = Tampon
                SelText = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Parent.Parent.Text

            Case VI.PointdeVue.PVueEtablissement
                If V_WebFonction.PointeurUtilisateur.Etablissement <> "" Then
                    If SelText <> V_WebFonction.PointeurUtilisateur.Etablissement Then
                        Exit Sub
                    End If
                End If
        End Select
        V_Valeur_Selectionnee = SelText

        Dim Evenement As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs
        If V_SiMajMenuVisible = True Then
            Evenement = New Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs("Sysref", Me.V_IdAppelant, SelPVue, V_NomTable, SelIde, SelText, "Maj")
        Else
            Evenement = New Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs(V_IdAppelant, V_ObjetAppelant, SelPVue, V_NomTable, SelIde, SelText, SelCode)
        End If
        V_Selection(Evenement)
    End Sub

    Private Sub InitialiserBoutons()
        Dim IndiceI As Integer = 0
        Dim Ctl As Control
        Do
            Ctl = V_WebFonction.VirWebControle(CadreLettre, "Button", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            CType(Ctl, Button).BackColor = V_WebFonction.ConvertCouleur("#124545")
            CType(Ctl, Button).ForeColor = V_WebFonction.ConvertCouleur("#D7FAF3")
            IndiceI += 1
        Loop
    End Sub

    Protected Sub BoutonLettre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonA.Click, ButtonB.Click, ButtonC.Click, ButtonD.Click, ButtonE.Click, ButtonF.Click, ButtonG.Click, ButtonH.Click, ButtonI.Click, ButtonJ.Click, ButtonK.Click, ButtonL.Click, ButtonM.Click, ButtonN.Click, ButtonO.Click, ButtonP.Click, ButtonQ.Click, ButtonR.Click, ButtonS.Click, ButtonT.Click, ButtonU.Click, ButtonV.Click, ButtonW.Click, ButtonX.Click, ButtonY.Click, ButtonZ.Click, ButtonTout.Click
        Call InitialiserBoutons()
        If CType(sender, Button).ID = "ButtonTout" Then
            V_Lettre = ""
        Else
            V_Lettre = Strings.Right(CType(sender, Button).ID, 1)
        End If
        CType(sender, Button).BackColor = V_WebFonction.ConvertCouleur("#D7FAF3")
        CType(sender, Button).ForeColor = V_WebFonction.ConvertCouleur("#124545")
        VRecherche.Text = V_Lettre
        V_SiTriOrganise = False
        RadioV0.Checked = True
    End Sub

    Protected Sub VRecherche_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles VRecherche.TextChanged
        Call InitialiserBoutons()
        If V_Lettre = "" Then
            ButtonTout.BackColor = V_WebFonction.ConvertCouleur("#D7FAF3")
            ButtonTout.ForeColor = V_WebFonction.ConvertCouleur("#124545")
        End If
        V_Lettre = VRecherche.Text
        V_SiTriOrganise = False
        RadioV0.Checked = True
    End Sub

    Protected Sub RadioCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioV0.CheckedChanged, RadioV1.CheckedChanged, RadioV2.CheckedChanged
        Select Case Strings.Right(CType(sender, System.Web.UI.WebControls.RadioButton).ID, 1)
            Case "0"
                V_SiTriOrganise = False
            Case Else
                V_SiTriOrganise = True
        End Select
        RadioV0.Font.Bold = False
        RadioV1.Font.Bold = False
        RadioV2.Font.Bold = False
        CType(sender, System.Web.UI.WebControls.RadioButton).Font.Bold = True
        V_Lettre = ""
        VRecherche.Text = ""
    End Sub

    Protected Sub CommandeBlanc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeBlanc.Click
        Dim Evenement As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs(V_IdAppelant, V_ObjetAppelant, 0, "", 0, "", "")
        V_Selection(Evenement)
    End Sub

    Protected Sub CommandeRetour_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeRetour.Click
        RaiseEvent RetourEventHandler(Me, e)
    End Sub

    Protected Sub CommandeNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeNew.Click
        V_Valeur_Selectionnee = ""
        Dim Evenement As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs("SysRef", 1, V_PointdeVue, V_NomTable, 0, "", "New")
        V_Selection(Evenement)
    End Sub

    Protected Sub CommandeSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeSupp.Click
        If V_Valeur_Selectionnee = "" Then
            Exit Sub
        End If
        Dim Evenement As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs("SysRef", 1, V_PointdeVue, V_NomTable, 0, "", "Supp")
        V_Selection(Evenement)
    End Sub

    Private Sub FaireListesReferences()
        Dim I As Integer
        Dim NewNoeud As TreeNode
        TreeListeRef.Nodes.Clear()

        If V_DuoPVueInverse(0) = 0 Or V_NomTable = "Nationalité" Then
            Call FaireListeSimple(Nothing)
        Else
            For I = 0 To 1
                V_PointdeVue = V_DuoPVueInverse(I)
                V_NomTable = V_DuoNomTable(I)
                NewNoeud = New TreeNode(V_NomTable, V_NomTable)
                NewNoeud.PopulateOnDemand = False
                NewNoeud.ImageUrl = "~/Images/Armoire/VertFonceOuvert16.bmp"
                NewNoeud.SelectAction = TreeNodeSelectAction.Expand
                TreeListeRef.Nodes.Add(NewNoeud)
                Call FaireListeSimple(TreeListeRef.Nodes(I))
                TreeListeRef.Nodes(I).Expand()
            Next I
        End If
    End Sub

    Private Sub FaireListeSimple(ByVal Noeud As TreeNode)
        If V_PointdeVue = VI.PointdeVue.PVueGeneral And V_NomTable = "" Then
            Exit Sub
        End If
        Dim Chaine As String = ""
        If Noeud IsNot Nothing Then
            If V_NomTable = "" Then
                Try
                    Chaine = V_WebFonction.PointeurGlobal.VirModele.Item(V_PointdeVue - 1).Intitule
                    Noeud.Text = Strings.Right(Chaine, Chaine.Length - 5)
                    V_NomTable = Chaine
                Catch ex As Exception
                    Exit Sub
                End Try
            Else
                Noeud.Text = V_NomTable
            End If
        ElseIf V_NomTable = "" Then
            Try
                Chaine = V_WebFonction.PointeurGlobal.VirModele.Item(V_PointdeVue - 1).Intitule
                V_NomTable = Chaine
            Catch ex As Exception
                Exit Sub
            End Try
        End If
        If V_NomTable.Contains("DICTIONNAIRE") Then
            V_NomTable = "Paramètres généraux"
        End If
        If V_PointdeVue = VI.PointdeVue.PVueGeneral Then
            EtiTitre.Text = V_NomTable
        ElseIf IsNumeric(Strings.Left(V_NomTable, 2)) Then
            EtiTitre.Text = V_NomTable.Substring(5)
        Else
            EtiTitre.Text = V_NomTable
        End If
        '**** Listes adaptées à un point de vue
        Select Case V_PointdeVue
            Case Is = VI.PointdeVue.PVueGrades
                Call FaireListeGrades(Noeud)
                Exit Sub
            Case Is = VI.PointdeVue.PVuePosteBud
                Call FaireListePosteBud(Noeud)
                Exit Sub
            Case Is = VI.PointdeVue.PVueFormation
                Call FaireListeStages(Noeud)
                Exit Sub
            Case Is = VI.PointdeVue.PVueCommission
                Call FaireListeCommissions(Noeud)
                Exit Sub
            Case Is = VI.PointdeVue.PVueMission
                Call FaireListeMissions(Noeud)
                Exit Sub
            Case Is = VI.PointdeVue.PVuePosteFct
                Call FaireListePostesFonctionnels(Noeud)
                Exit Sub
            Case Is = VI.PointdeVue.PVueLolf
                Call FaireListeLolf(Noeud)
                Exit Sub
            Case Is = VI.PointdeVue.PVueBaseHebdo, VI.PointdeVue.PVueCycle, VI.PointdeVue.PVueFonctionSupport, VI.PointdeVue.PVueActivite
                Call FaireListeParEtablissement(Noeud)
                Exit Sub
            Case Is = VI.PointdeVue.PVueDirection, VI.PointdeVue.PVueEtablissement, VI.PointdeVue.PVueItineraire, VI.PointdeVue.PVuePays, VI.PointdeVue.PVueInterface
                Call FaireListeClassique(Noeud)
                Exit Sub
        End Select

        Dim LstObjets As List(Of Virtualia.Net.ServiceServeur.VirRequeteType) = Nothing
        Dim VImage As String = ""
        Dim CptSel As Integer = 0
        Dim FicheRef As Virtualia.Ressources.Datas.ObjetDossierREF
        Dim SiAfaire As Boolean = False
        Dim Predicat As Virtualia.Ressources.Predicats.PredicateSysRef
        Dim LstFiches As List(Of Virtualia.Ressources.Datas.ObjetDossierREF)

        If V_WebFonction.PointeurContexte.SysRef_Listes.Count = 0 Then
            SiAfaire = True
        Else
            Predicat = New Virtualia.Ressources.Predicats.PredicateSysRef(V_NomTable, "")
            LstFiches = V_WebFonction.PointeurContexte.SysRef_Listes.FindAll(AddressOf Predicat.RechercherTable)
            If LstFiches.Count = 0 Then
                SiAfaire = True
            End If
            Predicat = Nothing
            LstFiches = Nothing
        End If

        If SiAfaire = True Then
            Select Case V_PointdeVue
                Case VI.PointdeVue.PVueApplicatif, VI.PointdeVue.PVuePerExterne
                    LstObjets = ListePersonnes(V_PointdeVue)
                    If LstObjets IsNot Nothing AndAlso LstObjets.Count > 250 Then
                        If V_Lettre = "" Then
                            V_Lettre = "A"
                            VRecherche.Text = "A"
                        End If
                    End If
                Case 99 'Point de vue Système
                    Select Case Noeud.Text
                        Case "$CatégorieRH"
                            LstObjets = V_WebFonction.PointeurGlobal.TabledesCategories
                    End Select
                Case VI.PointdeVue.PVueGeneral
                    Select Case V_NomTable
                        Case "$GestionnaireRH"
                            LstObjets = ListeUtiVersion3()
                        Case Else
                            If V_NomTable = "Tables Générales" Then
                                Select Case V_WebFonction.PointeurGlobal.VirNomModule
                                    Case "TempsTravail"
                                        LstObjets = V_WebFonction.PointeurGlobal.ListeTablesGenerales(VI.CategorieRH.TempsdeTravail)
                                End Select
                            Else
                                LstObjets = V_WebFonction.PointeurGlobal.LireTableGeneraleSimple(V_NomTable)
                            End If
                    End Select
                Case Else
                    LstObjets = V_WebFonction.PointeurGlobal.LireTableSysReference(V_PointdeVue, V_WebFonction.PointeurUtilisateur.V_NomdUtilisateurSgbd)
            End Select
            If LstObjets IsNot Nothing AndAlso LstObjets.Count > 0 Then
                For Each ResRequete In LstObjets
                    Chaine = ResRequete.Ide_Dossier & VI.Tild & ResRequete.Valeurs(0) & VI.Tild
                    If ResRequete.Valeurs.Count > 1 Then
                        Chaine &= ResRequete.Valeurs(1)
                    End If
                    FicheRef = New Virtualia.Ressources.Datas.ObjetDossierREF(V_WebFonction.PointeurGlobal.VirModele, V_PointdeVue, V_NomTable, Chaine)
                    '** Filtres Module
                    Select Case V_PointdeVue
                        Case VI.PointdeVue.PVuePaie
                            Select Case V_WebFonction.PointeurGlobal.VirNomModule
                                Case "TempsTravail"
                                    Select Case FicheRef.Valeur
                                        Case "Congés maladie", "Couleurs du planning", "Droits congés annuels", "Jour ouvrable", "Jours fériés mobiles", "Temps de travail"
                                            V_WebFonction.PointeurContexte.SysRef_Listes.Add(FicheRef)
                                    End Select
                            End Select
                        Case Else
                            V_WebFonction.PointeurContexte.SysRef_Listes.Add(FicheRef)
                    End Select
                    '**
                Next
            End If
        End If

        Predicat = New Virtualia.Ressources.Predicats.PredicateSysRef(V_NomTable, V_Lettre)
        LstFiches = New List(Of Virtualia.Ressources.Datas.ObjetDossierREF)
        LstFiches = V_WebFonction.PointeurContexte.SysRef_Listes.FindAll(AddressOf Predicat.RechercherLettre)
        If LstFiches.Count = 0 Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If

        Dim NewNoeud As TreeNode

        For Each FicheRef In LstFiches
            VImage = "~/Images/Armoire/FicheBleuStandard16.bmp"
            Select Case V_PointdeVue
                Case VI.PointdeVue.PVueGeneral
                    If V_SiTriOrganise = False Then
                        NewNoeud = New TreeNode(FicheRef.Valeur,
                                        FicheRef.V_Identifiant & VI.Tild & FicheRef.Valeur & VI.Tild & FicheRef.Code & VI.Tild & FicheRef.V_PointdeVue)
                    Else
                        NewNoeud = New TreeNode(FicheRef.Code & Strings.Space(5) & FicheRef.Valeur,
                                       FicheRef.V_Identifiant & VI.Tild & FicheRef.Valeur & VI.Tild & FicheRef.Code & VI.Tild & FicheRef.V_PointdeVue)
                    End If
                Case Else
                    NewNoeud = New TreeNode(FicheRef.Valeur,
                                               FicheRef.V_Identifiant & VI.Tild & FicheRef.Valeur & VI.Tild & FicheRef.Code & VI.Tild & FicheRef.V_PointdeVue)
            End Select
            NewNoeud.ImageUrl = VImage
            NewNoeud.PopulateOnDemand = False
            NewNoeud.SelectAction = TreeNodeSelectAction.Select
            If Noeud Is Nothing Then
                TreeListeRef.Nodes.Add(NewNoeud)
            Else
                Noeud.ChildNodes.Add(NewNoeud)
            End If
            If V_Valeur_Selectionnee <> "" Then
                If NewNoeud.Text = V_Valeur_Selectionnee Then
                    V_VirtuelPath = NewNoeud.ValuePath
                End If
            End If
            CptSel += 1
        Next

        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select
        If V_VirtuelPath <> "" Then
            Try
                TreeListeRef.FindNode(V_VirtuelPath).Selected = V_SiMajMenuVisible
                TreeListeRef.FindNode(V_VirtuelPath).Parent.Expanded = True
            Catch ex As Exception
                Exit Try
            End Try
        End If
        Dim NoeudRecherche As TreeNode
        Select Case V_PointdeVue
            Case VI.PointdeVue.PVueGeneral
                NoeudRecherche = V_AlleraInfoDico(TreeListeRef, 0, False, V_NomTable)
            Case Else
                NoeudRecherche = V_AlleraInfoDico(TreeListeRef, 0, True, "")
        End Select
    End Sub

    Private Sub FaireListeClassique(ByVal Noeud As TreeNode)
        Dim ClefListe As String
        Dim NoeudN1 As TreeNode = Nothing
        Dim CptSel As Integer = 0

        Select Case V_PointdeVue
            Case VI.PointdeVue.PVueEtablissement
                Dim CollWork As Virtualia.Ressources.Datas.ObjetEtablissement = V_WebFonction.PointeurReferentiel.PointeurEtablissements
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.ETA_IDENTITE)
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.ETA_IDENTITE
                If CollWork Is Nothing Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                TreeListeRef.MaxDataBindDepth = 0
                LstFiches = CollWork.ListeDesEtablissements(V_Lettre)
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For Each FicheRef In LstFiches
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Denomination & VI.Tild & FicheRef.Denomination & VI.Tild & VI.PointdeVue.PVueEtablissement
                    NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, FicheRef.Denomination, ClefListe)
                    CptSel += 1
                Next



            Case VI.PointdeVue.PVueDirection
                Dim CollWork As Virtualia.Ressources.Datas.ObjetDirections = V_WebFonction.PointeurReferentiel.PointeurDirection
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.ORG_DIRECTION)
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.ORG_DIRECTION
                If CollWork Is Nothing Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                TreeListeRef.MaxDataBindDepth = 0
                LstFiches = CollWork.ListeDesDirections(V_Lettre)
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For Each FicheRef In LstFiches
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Denomination & VI.Tild & FicheRef.Denomination & VI.Tild & VI.PointdeVue.PVueDirection
                    NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, FicheRef.Denomination, ClefListe)
                    CptSel += 1
                Next

            Case VI.PointdeVue.PVuePays
                Dim CollWork As Virtualia.Ressources.Datas.ObjetPays = V_WebFonction.PointeurReferentiel.PointeurPays
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.PAYS_DESCRIPTION)
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.PAYS_DESCRIPTION
                If CollWork Is Nothing Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                TreeListeRef.MaxDataBindDepth = 0
                LstFiches = CollWork.ListeDesPays(V_Lettre)
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For Each FicheRef In LstFiches
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild & FicheRef.Intitule & VI.Tild & VI.PointdeVue.PVuePays
                    NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, FicheRef.Intitule, ClefListe)
                    CptSel += 1
                Next

            Case VI.PointdeVue.PVueInterface
                Dim CollWork As Virtualia.Ressources.Datas.ObjetLogicielExterne = V_WebFonction.PointeurReferentiel.PointeurLogicielExterne
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.OUT_LOGICIEL)
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.OUT_LOGICIEL
                If CollWork Is Nothing Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                TreeListeRef.MaxDataBindDepth = 0
                LstFiches = CollWork.ListeDesLogiciels
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For Each FicheRef In LstFiches
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Nom & VI.Tild & FicheRef.Nom & VI.Tild & VI.PointdeVue.PVueInterface
                    NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, FicheRef.Nom, ClefListe)
                    CptSel += 1
                Next

            Case VI.PointdeVue.PVueItineraire
                Dim CollWork As Virtualia.Ressources.Datas.ObjetItineraire = V_WebFonction.PointeurReferentiel.PointeurItineraire
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.ITI_DISTANCE)
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.ITI_DISTANCE
                If CollWork Is Nothing Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                Dim LuN1 As String
                Dim LuValeur As String
                Dim RuptN1 As String = "(Vide)"
                Dim VNoeud As TreeNode = Nothing

                TreeListeRef.MaxDataBindDepth = 1
                LstFiches = CollWork.ListeDesVilles
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For Each FicheRef In LstFiches
                    LuN1 = FicheRef.Villedepart
                    LuValeur = FicheRef.Villearrivee
                    Select Case LuN1
                        Case Is <> RuptN1
                            RuptN1 = LuN1
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & LuN1 & VI.Tild & VI.PointdeVue.PVueItineraire
                            NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, LuN1, ClefListe)
                    End Select
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & LuN1 & LuValeur & VI.Tild & VI.PointdeVue.PVueItineraire
                    VNoeud = V_AjouterNoeudValeur(TreeListeRef, NoeudN1, LuValeur, ClefListe)
                    CptSel += 1
                Next

        End Select
        If V_VirtuelPath <> "" Then
            Try
                TreeListeRef.FindNode(V_VirtuelPath).Selected = V_SiMajMenuVisible
                TreeListeRef.FindNode(V_VirtuelPath).Parent.Expanded = True
            Catch ex As Exception
                Exit Try
            End Try
        End If
        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = V_AlleraInfoDico(TreeListeRef, 0, True, "")
        If NoeudRecherche IsNot Nothing And V_SiMajMenuVisible = True Then
            NoeudRecherche.ExpandAll()
        End If
    End Sub

    Private Sub FaireListeParEtablissement(ByVal Noeud As TreeNode)
        Dim ClefListe As String
        Dim NoeudN1 As TreeNode = Nothing
        Dim CptSel As Integer = 0
        Dim SelEta As String = V_NomTable
        If V_NomTable = "TOUS" Then
            SelEta = ""
        End If
        Select Case V_PointdeVue
            Case VI.PointdeVue.PVueBaseHebdo
                Dim CollWork As Virtualia.Ressources.Datas.ObjetUniteCycle = V_WebFonction.PointeurReferentiel.PointeurUniteCycles
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.CYC_IDENTIFICATION)
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.CYC_IDENTIFICATION
                If CollWork Is Nothing Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                Select Case V_SiTriOrganise
                    Case False 'Liste Alpha
                        TreeListeRef.MaxDataBindDepth = 0
                        LstFiches = CollWork.ListeDesUnites(V_Lettre, SelEta)
                        If LstFiches.Count = 0 Then
                            EtiStatus1.Text = "Aucune référence"
                            Exit Sub
                        End If
                        For Each FicheRef In LstFiches
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild & FicheRef.Etablissement & VI.Tild & VI.PointdeVue.PVueBaseHebdo
                            NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, FicheRef.Intitule, ClefListe)
                            CptSel += 1
                        Next

                    Case True 'Liste Organisée
                        Dim LuN1 As String
                        Dim LuValeur As String
                        Dim RuptN1 As String = "(Vide)"
                        Dim VNoeud As TreeNode = Nothing

                        If V_SiMajMenuVisible = True Then
                            TreeListeRef.MaxDataBindDepth = 0
                            LstFiches = CollWork.ListeDesUnites(SelEta)
                        Else
                            TreeListeRef.MaxDataBindDepth = 1
                            LstFiches = CollWork.ListeDesUnites("")
                        End If
                        If LstFiches.Count = 0 Then
                            EtiStatus1.Text = "Aucune référence"
                            Exit Sub
                        End If

                        For Each FicheRef In LstFiches
                            LuN1 = FicheRef.Etablissement
                            LuValeur = FicheRef.Intitule
                            If V_SiMajMenuVisible = True Then
                                NoeudN1 = Noeud
                            Else
                                Select Case LuN1
                                    Case Is <> RuptN1
                                        RuptN1 = LuN1
                                        ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & LuN1 & VI.Tild & VI.PointdeVue.PVueBaseHebdo
                                        NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, LuN1, ClefListe)
                                End Select
                            End If
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & LuN1 & LuValeur & VI.Tild & VI.PointdeVue.PVueBaseHebdo
                            VNoeud = V_AjouterNoeudValeur(TreeListeRef, NoeudN1, LuValeur, ClefListe)
                            CptSel += 1
                        Next
                End Select

            Case VI.PointdeVue.PVueCycle
                Dim CollWork As Virtualia.Ressources.Datas.ObjetCycleTravail = V_WebFonction.PointeurReferentiel.PointeurCyclesTravail
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.TRA_IDENTIFICATION)
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.TRA_IDENTIFICATION
                If CollWork Is Nothing Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                Select Case V_SiTriOrganise
                    Case False 'Liste Alpha
                        TreeListeRef.MaxDataBindDepth = 0
                        LstFiches = CollWork.ListeDesCycles(V_Lettre, SelEta)
                        If LstFiches.Count = 0 Then
                            EtiStatus1.Text = "Aucune référence"
                            Exit Sub
                        End If
                        For Each FicheRef In LstFiches
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild & FicheRef.Etablissement & VI.Tild & VI.PointdeVue.PVueCycle
                            NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, FicheRef.Intitule, ClefListe)
                            CptSel += 1
                        Next

                    Case True 'Liste Organisée
                        Dim LuN1 As String
                        Dim LuValeur As String
                        Dim RuptN1 As String = "(Vide)"
                        Dim VNoeud As TreeNode = Nothing

                        If V_SiMajMenuVisible = True Then
                            TreeListeRef.MaxDataBindDepth = 0
                            LstFiches = CollWork.ListeDesCycles(SelEta)
                        Else
                            TreeListeRef.MaxDataBindDepth = 1
                            LstFiches = CollWork.ListeDesCycles("")
                        End If
                        If LstFiches.Count = 0 Then
                            EtiStatus1.Text = "Aucune référence"
                            Exit Sub
                        End If

                        For Each FicheRef In LstFiches
                            LuN1 = FicheRef.Etablissement
                            LuValeur = FicheRef.Intitule
                            If V_SiMajMenuVisible = True Then
                                NoeudN1 = Noeud
                            Else
                                Select Case LuN1
                                    Case Is <> RuptN1
                                        RuptN1 = LuN1
                                        ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & LuN1 & VI.Tild & VI.PointdeVue.PVueCycle
                                        NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, LuN1, ClefListe)
                                End Select
                            End If
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & LuN1 & LuValeur & VI.Tild & VI.PointdeVue.PVueCycle
                            VNoeud = V_AjouterNoeudValeur(TreeListeRef, NoeudN1, LuValeur, ClefListe)
                            CptSel += 1
                        Next
                End Select

            Case VI.PointdeVue.PVueFonctionSupport
                Dim CollWork As Virtualia.Ressources.Datas.ObjetMesureActivite = V_WebFonction.PointeurReferentiel.PointeurActiviteMesures
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.FCT_SUPPORT)
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.FCT_SUPPORT
                If CollWork Is Nothing Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                Select Case V_SiTriOrganise
                    Case False 'Liste Alpha
                        TreeListeRef.MaxDataBindDepth = 0
                        LstFiches = CollWork.ListeFonctionsSupport(V_Lettre, SelEta)
                        If LstFiches.Count = 0 Then
                            EtiStatus1.Text = "Aucune référence"
                            Exit Sub
                        End If
                        For Each FicheRef In LstFiches
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild & FicheRef.Etablissement & VI.Tild & VI.PointdeVue.PVueFonctionSupport
                            NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, FicheRef.Intitule, ClefListe)
                            CptSel += 1
                        Next

                    Case True 'Liste Organisée
                        Dim LuN1 As String
                        Dim LuValeur As String
                        Dim RuptN1 As String = "(Vide)"
                        Dim VNoeud As TreeNode = Nothing

                        If V_SiMajMenuVisible = True Then
                            TreeListeRef.MaxDataBindDepth = 0
                            LstFiches = CollWork.ListeFonctionsSupport(SelEta)
                        Else
                            TreeListeRef.MaxDataBindDepth = 1
                            LstFiches = CollWork.ListeFonctionsSupport("")
                        End If
                        If LstFiches.Count = 0 Then
                            EtiStatus1.Text = "Aucune référence"
                            Exit Sub
                        End If

                        For Each FicheRef In LstFiches
                            LuN1 = FicheRef.Etablissement
                            LuValeur = FicheRef.Intitule
                            If V_SiMajMenuVisible = True Then
                                NoeudN1 = Noeud
                            Else
                                Select Case LuN1
                                    Case Is <> RuptN1
                                        RuptN1 = LuN1
                                        ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & LuN1 & VI.Tild & VI.PointdeVue.PVueFonctionSupport
                                        NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, LuN1, ClefListe)
                                End Select
                            End If
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & LuN1 & LuValeur & VI.Tild & VI.PointdeVue.PVueFonctionSupport
                            VNoeud = V_AjouterNoeudValeur(TreeListeRef, NoeudN1, LuValeur, ClefListe)
                            CptSel += 1
                        Next
                End Select

            Case VI.PointdeVue.PVueActivite
                Dim CollWork As Virtualia.Ressources.Datas.ObjetMesureActivite = V_WebFonction.PointeurReferentiel.PointeurActiviteMesures
                Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.ONIC_MISSION)
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.ONIC_MISSION
                If CollWork Is Nothing Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                Select Case V_SiTriOrganise
                    Case False 'Liste Alpha
                        TreeListeRef.MaxDataBindDepth = 0
                        LstFiches = CollWork.ListeDesMisions(SelEta)
                        If LstFiches.Count = 0 Then
                            EtiStatus1.Text = "Aucune référence"
                            Exit Sub
                        End If
                        For Each FicheRef In LstFiches
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild & FicheRef.Etablissement & VI.Tild & VI.PointdeVue.PVueActivite
                            NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, FicheRef.Intitule, ClefListe)
                            CptSel += 1
                        Next

                    Case True 'Liste Organisée
                        Dim LuN1 As String
                        Dim LuValeur As String
                        Dim RuptN1 As String = "(Vide)"
                        Dim VNoeud As TreeNode = Nothing

                        If V_SiMajMenuVisible = True Then
                            TreeListeRef.MaxDataBindDepth = 0
                            LstFiches = CollWork.ListeDesMisions(SelEta)
                        Else
                            TreeListeRef.MaxDataBindDepth = 1
                            LstFiches = CollWork.ListeDesMisions("")
                        End If
                        If LstFiches.Count = 0 Then
                            EtiStatus1.Text = "Aucune référence"
                            Exit Sub
                        End If

                        For Each FicheRef In LstFiches
                            LuN1 = FicheRef.Etablissement
                            LuValeur = FicheRef.Intitule
                            If V_SiMajMenuVisible = True Then
                                NoeudN1 = Noeud
                            Else
                                Select Case LuN1
                                    Case Is <> RuptN1
                                        RuptN1 = LuN1
                                        ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & LuN1 & VI.Tild & VI.PointdeVue.PVueActivite
                                        NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, LuN1, ClefListe)
                                End Select
                            End If
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & LuN1 & LuValeur & VI.Tild & VI.PointdeVue.PVueActivite
                            VNoeud = V_AjouterNoeudValeur(TreeListeRef, NoeudN1, LuValeur, ClefListe)
                            CptSel += 1
                        Next
                End Select
        End Select
        If V_VirtuelPath <> "" Then
            Try
                TreeListeRef.FindNode(V_VirtuelPath).Selected = V_SiMajMenuVisible
                TreeListeRef.FindNode(V_VirtuelPath).Parent.Expanded = True
            Catch ex As Exception
                Exit Try
            End Try
        End If
        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = V_AlleraInfoDico(TreeListeRef, 0, True, "")
        If NoeudRecherche IsNot Nothing And V_SiMajMenuVisible = True Then
            NoeudRecherche.ExpandAll()
        End If
    End Sub

    Private Sub FaireListeStages(ByVal Noeud As TreeNode)
        Dim CollStages As Virtualia.Ressources.Datas.ObjetStageFormation = V_WebFonction.PointeurReferentiel.PointeurStagesFormation
        Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.FOR_IDENTIFICATION)
        Dim FicheRef As Virtualia.TablesObjet.ShemaREF.FOR_IDENTIFICATION
        Dim ClefListe As String
        Dim NoeudN1 As TreeNode = Nothing
        Dim CptSel As Integer = 0

        If CollStages Is Nothing Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If

        Select Case V_SiTriOrganise
            Case False 'Liste Alpha
                TreeListeRef.MaxDataBindDepth = 0
                If V_SiMajMenuVisible = True Then
                    LstFiches = CollStages.ListeDesStages_Alpha_Plan(V_Lettre, V_NomTable)
                Else
                    If CollStages.NombredeStages > 500 And V_Lettre = "" Then
                        V_Lettre = "A"
                        VRecherche.Text = "A"
                    End If
                    LstFiches = CollStages.ListeDesStages_Alpha_Etablissement(V_Lettre, "")
                End If
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For Each FicheRef In LstFiches
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild & FicheRef.Domaine & VI.Tild & VI.PointdeVue.PVueFormation
                    NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, FicheRef.Intitule, ClefListe)
                    CptSel += 1
                Next

            Case True '** "1" 'Liste Organisée
                Dim LuN1 As String
                Dim LuN2 As String
                Dim LuN3 As String
                Dim LuN4 As String
                Dim RuptN1 As String = "(Vide)"
                Dim RuptN2 As String = "(Vide)"
                Dim RuptN3 As String = "(Vide)"
                Dim RuptN4 As String = "(Vide)"
                Dim NoeudN2 As TreeNode = Nothing
                Dim NoeudN3 As TreeNode = Nothing
                Dim NoeudN4 As TreeNode = Nothing

                V_Lettre = ""
                VRecherche.Text = ""

                TreeListeRef.MaxDataBindDepth = 2
                If V_SiMajMenuVisible = True Then
                    LstFiches = CollStages.ListeDesStages("Plan", V_NomTable)
                Else
                    LstFiches = CollStages.ListeDesStages("", "")
                End If
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If

                For Each FicheRef In LstFiches
                    LuN1 = FicheRef.PlandeFormation
                    LuN2 = FicheRef.Domaine
                    LuN3 = FicheRef.Theme
                    LuN4 = FicheRef.Intitule
                    If V_SiMajMenuVisible = True Then
                        NoeudN1 = Noeud
                    Else
                        Select Case LuN1
                            Case Is <> RuptN1
                                RuptN1 = LuN1
                                RuptN2 = "(Vide)"
                                RuptN3 = "(Vide)"
                                RuptN4 = "(Vide)"
                                ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & LuN1 & VI.Tild & VI.PointdeVue.PVueFormation
                                NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, LuN1, ClefListe)
                        End Select
                    End If
                    Select Case LuN2
                        Case Is <> RuptN2
                            RuptN2 = LuN2
                            RuptN3 = "(Vide)"
                            RuptN4 = "(Vide)"
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & LuN1 & LuN2 & VI.Tild & VI.PointdeVue.PVueFormation
                            NoeudN2 = V_AjouterNoeudValeur(TreeListeRef, NoeudN1, LuN2, ClefListe)
                    End Select
                    Select Case LuN3
                        Case Is <> RuptN3
                            RuptN3 = LuN3
                            RuptN4 = "(Vide)"
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN3 & VI.Tild & LuN1 & LuN2 & LuN3 & VI.Tild & VI.PointdeVue.PVueFormation
                            NoeudN3 = V_AjouterNoeudValeur(TreeListeRef, NoeudN2, LuN3, ClefListe)
                    End Select
                    Select Case LuN4
                        Case Is <> RuptN4
                            RuptN4 = LuN4
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN4 & VI.Tild & LuN1 & LuN2 & LuN3 & LuN4 & VI.Tild & VI.PointdeVue.PVueFormation
                            NoeudN4 = V_AjouterNoeudValeur(TreeListeRef, NoeudN3, LuN4, ClefListe)
                    End Select
                    CptSel += 1
                Next
        End Select
        If V_VirtuelPath <> "" Then
            Try
                TreeListeRef.FindNode(V_VirtuelPath).Selected = V_SiMajMenuVisible
                TreeListeRef.FindNode(V_VirtuelPath).Parent.Expanded = True
            Catch ex As Exception
                Exit Try
            End Try
        End If
        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = V_AlleraInfoDico(TreeListeRef, 0, True, "")
        If NoeudRecherche IsNot Nothing And V_SiMajMenuVisible = True Then
            NoeudRecherche.ExpandAll()
        End If
    End Sub

    Private Sub FaireListeCommissions(ByVal Noeud As TreeNode)
        Dim CollCommis As Virtualia.Ressources.Datas.ObjetCommission = V_WebFonction.PointeurReferentiel.PointeurCommissions
        Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.ACT_COMMISSION)
        Dim FicheRef As Virtualia.TablesObjet.ShemaREF.ACT_COMMISSION
        Dim ClefListe As String
        Dim NoeudN1 As TreeNode = Nothing
        Dim CptSel As Integer = 0
        Dim DateJour As String = V_WebFonction.ViRhDates.DateduJour(False)
        Dim SiInterne As Boolean = True

        If CollCommis Is Nothing Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If
        If V_NomTable <> "Interne" Then
            SiInterne = False
        End If

        Select Case V_SiTriOrganise
            Case False 'Liste Alpha
                TreeListeRef.MaxDataBindDepth = 0
                LstFiches = CollCommis.ListeDesCommissions(SiInterne, V_Lettre)
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For Each FicheRef In LstFiches
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild & FicheRef.Numero & VI.Tild & VI.PointdeVue.PVueCommission
                    NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, FicheRef.Intitule & " - " & FicheRef.Secteur_Activite & " - " & FicheRef.Organisme_Competent, ClefListe)
                    CptSel += 1
                Next

            Case True 'Liste Organisée
                Dim LuN1 As String
                Dim LuN2 As String
                Dim LuValeur As String
                Dim RuptN1 As String = "(Vide)"
                Dim RuptN2 As String = "(Vide)"
                Dim NoeudN2 As TreeNode = Nothing
                Dim VNoeud As TreeNode = Nothing

                LstFiches = CollCommis.ListeDesCommissions(SiInterne, "", "Activité")
                TreeListeRef.MaxDataBindDepth = 1
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If

                For Each FicheRef In LstFiches
                    LuN1 = FicheRef.Secteur_Activite
                    LuN2 = FicheRef.Organisme_Competent
                    LuValeur = FicheRef.Intitule
                    Select Case LuN1
                        Case Is <> RuptN1
                            RuptN1 = LuN1
                            RuptN2 = "(Vide)"
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & LuN1 & VI.Tild & VI.PointdeVue.PVueCommission
                            NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, LuN1, ClefListe)
                    End Select
                    Select Case LuN2
                        Case Is <> RuptN2
                            RuptN2 = LuN2
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & LuN1 & LuN2 & VI.Tild & VI.PointdeVue.PVueCommission
                            NoeudN2 = V_AjouterNoeudValeur(TreeListeRef, NoeudN1, LuN2, ClefListe)
                    End Select
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & LuN1 & LuN2 & LuValeur & VI.Tild & VI.PointdeVue.PVueCommission
                    VNoeud = V_AjouterNoeudValeur(TreeListeRef, NoeudN2, LuValeur, ClefListe)
                    CptSel += 1
                Next
        End Select
        If V_VirtuelPath <> "" Then
            Try
                TreeListeRef.FindNode(V_VirtuelPath).Selected = V_SiMajMenuVisible
                TreeListeRef.FindNode(V_VirtuelPath).Parent.Expanded = True
            Catch ex As Exception
                Exit Try
            End Try
        End If
        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = V_AlleraInfoDico(TreeListeRef, 0, True, "")
        If NoeudRecherche IsNot Nothing And V_SiMajMenuVisible = True Then
            NoeudRecherche.ExpandAll()
        End If
    End Sub

    Private Sub FaireListePostesFonctionnels(ByVal Noeud As TreeNode)
        Dim CollPostes As Virtualia.Ressources.Datas.ObjetPosteFonctionnel = V_WebFonction.PointeurReferentiel.PointeurPostesFonctionnels
        Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.PST_IDENTIFICATION)
        Dim FicheRef As Virtualia.TablesObjet.ShemaREF.PST_IDENTIFICATION
        Dim ClefListe As String
        Dim NoeudN1 As TreeNode = Nothing
        Dim CptSel As Integer = 0
        Dim DateJour As String = V_WebFonction.ViRhDates.DateduJour(False)
        Dim SiInterne As Boolean = True

        If CollPostes Is Nothing Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If

        Select Case V_SiTriOrganise
            Case False 'Liste Alpha
                TreeListeRef.MaxDataBindDepth = 0
                LstFiches = CollPostes.ListeDesPostes_Alpha(V_Lettre, V_NomTable)
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For Each FicheRef In LstFiches
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild &
                                    FicheRef.Numero_du_poste & VI.Tild & VI.PointdeVue.PVuePosteFct
                    NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, FicheRef.Intitule & " - " &
                                                 FicheRef.Numero_du_poste & " - " &
                                                 FicheRef.Affectation_Organigramme("", 1), ClefListe)
                    CptSel += 1
                Next

            Case True 'Liste Organisée
                Dim LuN1 As String
                Dim LuN2 As String
                Dim LuValeur As String
                Dim RuptN1 As String = "(Vide)"
                Dim RuptN2 As String = "(Vide)"
                Dim NoeudN2 As TreeNode = Nothing
                Dim VNoeud As TreeNode = Nothing

                If V_SiMajMenuVisible = True Then
                    LstFiches = CollPostes.ListeDesPostes(DateJour, V_NomTable)
                    TreeListeRef.MaxDataBindDepth = 0
                Else
                    LstFiches = CollPostes.ListeDesPostes(DateJour, "")
                    TreeListeRef.MaxDataBindDepth = 1
                End If
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If

                For Each FicheRef In LstFiches
                    LuN1 = FicheRef.Affectation_Organigramme("", 1)
                    LuN2 = FicheRef.Affectation_Organigramme("", 2)
                    LuValeur = FicheRef.Intitule

                    If V_SiMajMenuVisible = True Then
                        NoeudN1 = Noeud
                    Else
                        Select Case LuN1
                            Case Is <> RuptN1
                                RuptN1 = LuN1
                                RuptN2 = "(Vide)"
                                ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild &
                                        LuN1 & VI.Tild & VI.PointdeVue.PVuePosteFct
                                NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, LuN1, ClefListe)
                        End Select
                    End If
                    Select Case LuN2
                        Case Is <> RuptN2
                            RuptN2 = LuN2
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild &
                                    LuN1 & LuN2 & VI.Tild & VI.PointdeVue.PVuePosteFct
                            NoeudN2 = V_AjouterNoeudValeur(TreeListeRef, NoeudN1, LuN2, ClefListe)
                    End Select
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild &
                                    LuN1 & LuN2 & LuValeur & VI.Tild & VI.PointdeVue.PVuePosteFct
                    VNoeud = V_AjouterNoeudValeur(TreeListeRef, NoeudN2, LuValeur, ClefListe)
                    CptSel += 1
                Next
        End Select
        If V_VirtuelPath <> "" Then
            Try
                TreeListeRef.FindNode(V_VirtuelPath).Selected = V_SiMajMenuVisible
                TreeListeRef.FindNode(V_VirtuelPath).Parent.Expanded = True
            Catch ex As Exception
                Exit Try
            End Try
        End If
        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = V_AlleraInfoDico(TreeListeRef, 0, True, "")
        If NoeudRecherche IsNot Nothing And V_SiMajMenuVisible = True Then
            NoeudRecherche.ExpandAll()
        End If
    End Sub

    Private Sub FaireListeMissions(ByVal Noeud As TreeNode)
        Dim CollMission As Virtualia.Ressources.Datas.ObjetMission = V_WebFonction.PointeurReferentiel.PointeurFraisMission
        Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.MIS_CARACTERISTIC)
        Dim FicheRef As Virtualia.TablesObjet.ShemaREF.MIS_CARACTERISTIC
        Dim ClefListe As String
        Dim NoeudN1 As TreeNode = Nothing
        Dim CptSel As Integer = 0
        Dim DateJour As String = V_WebFonction.ViRhDates.DateduJour(False)

        If CollMission Is Nothing Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If
        If V_SiMajMenuVisible = False Then
            V_NomTable = Strings.Right(V_WebFonction.ViRhDates.DateduJour(False), 4)
        End If

        Select Case V_SiTriOrganise
            Case False 'Liste Alpha
                TreeListeRef.MaxDataBindDepth = 0
                LstFiches = CollMission.ListeDesMissions_Alpha(V_Lettre, V_NomTable)
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For Each FicheRef In LstFiches
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Intitule & VI.Tild &
                                    FicheRef.Numero & VI.Tild & VI.PointdeVue.PVueMission
                    NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, FicheRef.Intitule & " - " & FicheRef.ObjetdelaMission & " - ", ClefListe)
                    CptSel += 1
                Next

            Case True 'Liste Organisée
                Dim LuN1 As String
                Dim LuN2 As String
                Dim LuValeur As String
                Dim RuptN1 As String = "(Vide)"
                Dim RuptN2 As String = "(Vide)"
                Dim NoeudN2 As TreeNode = Nothing
                Dim VNoeud As TreeNode = Nothing

                LstFiches = CollMission.ListeDesMissions(V_NomTable, 0)
                TreeListeRef.MaxDataBindDepth = 1
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If

                For Each FicheRef In LstFiches
                    LuN1 = V_WebFonction.ViRhDates.MoisEnClair(FicheRef.MoisMission)
                    LuN2 = FicheRef.ObjetdelaMission
                    LuValeur = FicheRef.Intitule

                    Select Case LuN1
                        Case Is <> RuptN1
                            RuptN1 = LuN1
                            RuptN2 = "(Vide)"
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & LuN1 & VI.Tild & VI.PointdeVue.PVueMission
                            NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, LuN1, ClefListe)
                    End Select
                    Select Case LuN2
                        Case Is <> RuptN2
                            RuptN2 = LuN2
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & LuN1 & LuN2 & VI.Tild & VI.PointdeVue.PVueMission
                            NoeudN2 = V_AjouterNoeudValeur(TreeListeRef, NoeudN1, LuN2, ClefListe)
                    End Select
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & LuN1 & LuN2 & LuValeur & VI.Tild & VI.PointdeVue.PVueMission
                    VNoeud = V_AjouterNoeudValeur(TreeListeRef, NoeudN2, LuValeur, ClefListe)
                    CptSel += 1
                Next
        End Select
        If V_VirtuelPath <> "" Then
            Try
                TreeListeRef.FindNode(V_VirtuelPath).Selected = V_SiMajMenuVisible
                TreeListeRef.FindNode(V_VirtuelPath).Parent.Expanded = True
            Catch ex As Exception
                Exit Try
            End Try
        End If
        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = V_AlleraInfoDico(TreeListeRef, 0, True, "")
        If NoeudRecherche IsNot Nothing And V_SiMajMenuVisible = True Then
            NoeudRecherche.ExpandAll()
        End If
    End Sub

    Private Sub FaireListeLolf(ByVal Noeud As TreeNode)
        Dim CollLolf As Virtualia.Ressources.Datas.ObjetLOLF = V_WebFonction.PointeurReferentiel.PointeurLOLF
        Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.LOLF_MISSION)
        Dim FicheRef As Virtualia.TablesObjet.ShemaREF.LOLF_MISSION
        Dim LstPgm As List(Of Virtualia.TablesObjet.ShemaREF.LOLF_PROGRAMME)
        Dim FichePgm As Virtualia.TablesObjet.ShemaREF.LOLF_PROGRAMME
        Dim LstAction As List(Of Virtualia.TablesObjet.ShemaREF.LOLF_ACTION)
        Dim FicheAction As Virtualia.TablesObjet.ShemaREF.LOLF_ACTION
        Dim LstBop As List(Of Virtualia.TablesObjet.ShemaREF.LOLF_BOP)
        Dim FicheBop As Virtualia.TablesObjet.ShemaREF.LOLF_BOP
        Dim LstAE As List(Of Virtualia.TablesObjet.ShemaREF.LOLF_AUTORISATION_EMPLOI)
        Dim FicheAE As Virtualia.TablesObjet.ShemaREF.LOLF_AUTORISATION_EMPLOI
        Dim LstUO As List(Of Virtualia.TablesObjet.ShemaREF.LOLF_UO)
        Dim FicheUO As Virtualia.TablesObjet.ShemaREF.LOLF_UO
        Dim ClefListe As String
        Dim LuN1 As String
        Dim LuN2 As String
        Dim LuN3 As String
        Dim LuN4 As String
        Dim LuValeur As String
        Dim NoeudN1 As TreeNode = Nothing
        Dim NoeudN2 As TreeNode = Nothing
        Dim NoeudN3 As TreeNode = Nothing
        Dim NoeudN4 As TreeNode = Nothing
        Dim VNoeud As TreeNode = Nothing
        Dim CptSel As Integer = 0

        If CollLolf Is Nothing Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If
        LstFiches = CollLolf.ListeDesMissions()
        TreeListeRef.MaxDataBindDepth = 3
        If LstFiches.Count = 0 Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If

        For Each FicheRef In LstFiches
            LuN1 = FicheRef.Intitule
            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild &
                    LuN1 & VI.Tild & VI.PointdeVue.PVueLolf
            NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, LuN1, ClefListe)

            LstPgm = FicheRef.ListedesProgrammes("")
            If LstPgm IsNot Nothing Then
                For Each FichePgm In LstPgm
                    LuN2 = FichePgm.Numero & " - " & FichePgm.Intitule
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & LuN1 & LuN2 & VI.Tild & VI.PointdeVue.PVueLolf
                    NoeudN2 = V_AjouterNoeudValeur(TreeListeRef, NoeudN1, LuN2, ClefListe)

                    LstAction = FichePgm.ListedesActions("Numero")
                    If LstAction IsNot Nothing Then
                        LuN3 = "Actions et Sous-actions"
                        ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & LuN1 & LuN2 & LuN3 & VI.Tild & VI.PointdeVue.PVueLolf
                        NoeudN3 = V_AjouterNoeudValeur(TreeListeRef, NoeudN2, LuN3, ClefListe)

                        For Each FicheAction In LstAction
                            LuN4 = FicheAction.NumerodelAction & " - " & FicheAction.Intitule
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN4 & VI.Tild & LuN1 & LuN2 & LuN3 & LuN4 & VI.Tild & VI.PointdeVue.PVueLolf
                            NoeudN4 = V_AjouterNoeudValeur(TreeListeRef, NoeudN3, LuN4, ClefListe)

                            LstAE = FicheAction.ListedesAutorisations("Numero")
                            For Each FicheAE In LstAE
                                LuValeur = FicheAE.NumerodelAutorisation & " - " & FicheAE.CategorieFonctionnelle & " ( Nombre = " & FicheAE.Nombre.ToString & ")"
                                ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild &
                                    LuN1 & LuN2 & LuN3 & LuN4 & LuValeur & VI.Tild & VI.PointdeVue.PVueLolf
                                VNoeud = V_AjouterNoeudValeur(TreeListeRef, NoeudN4, LuValeur, ClefListe)
                                CptSel += 1
                            Next
                        Next
                    End If

                    LstBop = FichePgm.ListedesBOPs("Numero")
                    If LstBop IsNot Nothing Then
                        LuN3 = "Budget Opérationnel de Programme"
                        ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & LuN1 & LuN2 & LuN3 & VI.Tild & VI.PointdeVue.PVueLolf
                        NoeudN3 = V_AjouterNoeudValeur(TreeListeRef, NoeudN2, LuN3, ClefListe)

                        For Each FicheBop In LstBop
                            LuN4 = FicheBop.NumeroduBOP & " - " & FicheBop.Intitule
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN4 & VI.Tild & LuN1 & LuN2 & LuN3 & LuN4 & VI.Tild & VI.PointdeVue.PVueLolf
                            NoeudN4 = V_AjouterNoeudValeur(TreeListeRef, NoeudN3, LuN4, ClefListe)

                            LstUO = FicheBop.ListedesUOs("Numero")
                            For Each FicheUO In LstUO
                                LuValeur = FicheUO.NumerodelUO & " - " & FicheUO.Intitule
                                ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & LuN1 & LuN2 & LuN3 & LuN4 & LuValeur & VI.Tild & VI.PointdeVue.PVueLolf
                                VNoeud = V_AjouterNoeudValeur(TreeListeRef, NoeudN4, LuValeur, ClefListe)
                                CptSel += 1
                            Next
                        Next
                    End If
                Next
            End If
        Next
        If V_VirtuelPath <> "" Then
            Try
                TreeListeRef.FindNode(V_VirtuelPath).Selected = V_SiMajMenuVisible
                TreeListeRef.FindNode(V_VirtuelPath).Parent.Expanded = True
            Catch ex As Exception
                Exit Try
            End Try
        End If
        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = V_AlleraInfoDico(TreeListeRef, 0, True, "")
        If NoeudRecherche IsNot Nothing And V_SiMajMenuVisible = True Then
            NoeudRecherche.ExpandAll()
        End If
    End Sub

    Private Sub FaireListePosteBud(ByVal Noeud As TreeNode)
        Dim CollPostes As Virtualia.Ressources.Datas.ObjetPosteBudgetaire = V_WebFonction.PointeurReferentiel.PointeurPostesBudgetaires
        Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.ORG_BUDGETAIRE)
        Dim FicheRef As Virtualia.TablesObjet.ShemaREF.ORG_BUDGETAIRE
        Dim ClefListe As String
        Dim NoeudN1 As TreeNode = Nothing
        Dim CptSel As Integer = 0
        Dim DateJour As String = V_WebFonction.ViRhDates.DateduJour(False)

        If CollPostes Is Nothing Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If

        Select Case V_SiTriOrganise
            Case False 'Liste Alpha
                TreeListeRef.MaxDataBindDepth = 0
                LstFiches = CollPostes.ListeDesPostes_Alpha(V_Lettre, V_NomTable)
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For Each FicheRef In LstFiches
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & FicheRef.Numero_EB & VI.Tild & FicheRef.Filiere_Definition(DateJour) & VI.Tild & VI.PointdeVue.PVuePosteBud
                    NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, FicheRef.Numero_EB & " - " & FicheRef.Intitule__Definition & " - " & FicheRef.Budget__Definition(DateJour), ClefListe)
                    CptSel += 1
                Next

            Case True 'Liste Organisée
                Dim LuN1 As String
                Dim LuN2 As String
                Dim LuN3 As String
                Dim LuValeur As String
                Dim RuptN1 As String = "(Vide)"
                Dim RuptN2 As String = "(Vide)"
                Dim RuptN3 As String = "(Vide)"
                Dim NoeudN2 As TreeNode = Nothing
                Dim NoeudN3 As TreeNode = Nothing
                Dim VNoeud As TreeNode = Nothing

                If V_SiMajMenuVisible = True Then
                    LstFiches = CollPostes.ListeDesPostes(V_NomTable, DateJour)
                    TreeListeRef.MaxDataBindDepth = 1
                Else
                    LstFiches = CollPostes.ListeDesPostes("", DateJour)
                    TreeListeRef.MaxDataBindDepth = 2
                End If
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If

                For Each FicheRef In LstFiches
                    LuN1 = FicheRef.Filiere_Definition(DateJour)
                    LuN2 = FicheRef.Secteur_Definition
                    LuN3 = FicheRef.Intitule__Definition
                    LuValeur = FicheRef.Numero_EB & " - " & FicheRef.Budget__Definition

                    If V_SiMajMenuVisible = True Then
                        NoeudN1 = Noeud
                    Else
                        Select Case LuN1
                            Case Is <> RuptN1
                                RuptN1 = LuN1
                                RuptN2 = "(Vide)"
                                RuptN3 = "(Vide)"
                                ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & LuN1 & VI.Tild & VI.PointdeVue.PVuePosteBud
                                NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, LuN1, ClefListe)
                        End Select
                    End If

                    Select Case LuN2
                        Case Is <> RuptN2
                            RuptN2 = LuN2
                            RuptN3 = "(Vide)"
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & LuN1 & LuN2 & VI.Tild & VI.PointdeVue.PVuePosteBud
                            NoeudN2 = V_AjouterNoeudValeur(TreeListeRef, NoeudN1, LuN2, ClefListe)
                    End Select
                    Select Case LuN3
                        Case Is <> RuptN3
                            RuptN3 = LuN3
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN3 & VI.Tild & LuN1 & LuN2 & LuN3 & VI.Tild & VI.PointdeVue.PVuePosteBud
                            NoeudN3 = V_AjouterNoeudValeur(TreeListeRef, NoeudN2, LuN3, ClefListe)
                    End Select
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & LuN1 & LuN2 & LuN3 & LuValeur & VI.Tild & VI.PointdeVue.PVuePosteBud
                    VNoeud = V_AjouterNoeudValeur(TreeListeRef, NoeudN3, LuValeur, ClefListe)
                    CptSel += 1
                Next
        End Select
        If V_VirtuelPath <> "" Then
            Try
                TreeListeRef.FindNode(V_VirtuelPath).Selected = V_SiMajMenuVisible
                TreeListeRef.FindNode(V_VirtuelPath).Parent.Expanded = True
            Catch ex As Exception
                Exit Try
            End Try
        End If
        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = V_AlleraInfoDico(TreeListeRef, 0, True, "")
        If NoeudRecherche IsNot Nothing And V_SiMajMenuVisible = True Then
            NoeudRecherche.ExpandAll()
        End If
    End Sub

    Private Sub FaireListeGrades(ByVal Noeud As TreeNode)
        Dim Collgrades As Virtualia.Ressources.Datas.ObjetGradeGrille = V_WebFonction.PointeurReferentiel.PointeurGradeGrilles
        Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.GRD_GRADE)
        Dim FicheRef As Virtualia.TablesObjet.ShemaREF.GRD_GRADE
        Dim FicheGrille As Virtualia.TablesObjet.ShemaREF.GRD_GRILLE
        Dim FicheEch As Virtualia.TablesObjet.ShemaREF.GRD_ECHELON
        Dim ClefListe As String
        Dim IndiceK As Integer
        Dim NoeudN1 As TreeNode = Nothing
        Dim NoeudN2 As TreeNode = Nothing
        Dim NoeudN3 As TreeNode = Nothing
        Dim LuN1 As String
        Dim LuN2 As String
        Dim LuN3 As String
        Dim CptSel As Integer = 0
        Dim ClefSel As String

        If Collgrades Is Nothing Then
            EtiStatus1.Text = "Aucune référence"
            Exit Sub
        End If

        Select Case V_SiTriOrganise
            Case False 'Liste Alpha
                If V_SiMajMenuVisible = False And V_Date_Selectionnee <> "" Then
                    TreeListeRef.MaxDataBindDepth = 1
                Else
                    TreeListeRef.MaxDataBindDepth = 0
                End If
                LstFiches = Collgrades.ListeDesGrades(V_Lettre, V_NomTable)
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If
                For Each FicheRef In LstFiches
                    LuN1 = FicheRef.Intitule
                    ClefSel = LuN1 & VI.PointVirgule & FicheRef.Categorie & VI.PointVirgule & FicheRef.Filiere & VI.PointVirgule & FicheRef.Corps

                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & ClefSel & VI.Tild & VI.PointdeVue.PVueGrades

                    NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, LuN1, ClefListe)
                    CptSel += 1

                    If V_SiMajMenuVisible = False And V_Date_Selectionnee <> "" Then 'Liste des échelons à la date du
                        FicheGrille = FicheRef.Fiche_Grille_Valable(V_Date_Selectionnee)
                        If FicheGrille IsNot Nothing Then
                            LuN2 = V_Date_Selectionnee
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & LuN1 & LuN2 & VI.Tild & VI.PointdeVue.PVueGrilles
                            NoeudN2 = V_AjouterNoeudValeur(TreeListeRef, NoeudN1, "Grille indiciaire valable au : " & LuN2, ClefListe)
                            For IndiceK = 0 To FicheGrille.ListedesEchelons("").Count - 1
                                FicheEch = FicheGrille.ListedesEchelons("").Item(IndiceK)
                                If FicheEch.AliasEchelon = "" Then
                                    Exit For
                                End If
                                LuN3 = ""
                                If FicheEch.Chevron <> "" Then
                                    LuN3 &= "Chevron = " & FicheEch.Chevron & Strings.Space(2)
                                End If
                                LuN3 &= FicheEch.AliasEchelon & Strings.Space(2)
                                LuN3 &= "Majoré = " & FicheEch.IndiceMajore & Strings.Space(2)
                                LuN3 &= "Brut = " & FicheEch.IndiceBrut & Strings.Space(2)

                                ClefSel = LuN1 & VI.PointVirgule & FicheRef.Categorie & VI.PointVirgule & FicheRef.Filiere & VI.PointVirgule & FicheRef.Corps

                                ClefSel &= VI.PointVirgule & FicheGrille.Intitule & VI.PointVirgule & FicheEch.AliasEchelon & VI.PointVirgule & FicheEch.IndiceMajore &
                                        VI.PointVirgule & FicheEch.IndiceBrut

                                ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN3.Trim & VI.Tild & ClefSel & VI.Tild & VI.PointdeVue.PVueGrilles

                                NoeudN3 = V_AjouterNoeudValeur(TreeListeRef, NoeudN2, LuN3.Trim, ClefListe)
                            Next IndiceK
                        End If
                    End If
                Next

            Case True 'Liste Organisée
                Dim LuN4 As String
                Dim LuValeur As String
                Dim RuptN1 As String = "(Vide)"
                Dim RuptN2 As String = "(Vide)"
                Dim RuptN3 As String = "(Vide)"
                Dim RuptN4 As String = "(Vide)"
                Dim NoeudN4 As TreeNode = Nothing
                Dim VNoeud As TreeNode = Nothing

                If V_SiMajMenuVisible = True Then
                    LstFiches = Collgrades.ListeDesGradesTries("Gestion", V_NomTable)
                    TreeListeRef.MaxDataBindDepth = 2
                Else
                    LstFiches = Collgrades.ListeDesGradesTries("", "")
                    TreeListeRef.MaxDataBindDepth = 3
                End If
                If LstFiches.Count = 0 Then
                    EtiStatus1.Text = "Aucune référence"
                    Exit Sub
                End If

                For Each FicheRef In LstFiches
                    LuN1 = FicheRef.Gestion
                    LuN2 = FicheRef.Categorie
                    LuN3 = FicheRef.Filiere
                    LuN4 = FicheRef.Corps
                    LuValeur = FicheRef.Intitule
                    ClefSel = LuValeur & VI.PointVirgule & LuN2 & VI.PointVirgule & LuN3 & VI.PointVirgule & LuN4

                    If V_SiMajMenuVisible = True Then
                        NoeudN1 = Noeud
                    Else
                        Select Case LuN1
                            Case Is <> RuptN1
                                RuptN1 = LuN1
                                RuptN2 = "(Vide)"
                                RuptN3 = "(Vide)"
                                RuptN4 = "(Vide)"
                                ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN1 & VI.Tild & LuN1 & VI.Tild & VI.PointdeVue.PVueGrades
                                NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Noeud, LuN1, ClefListe)
                        End Select
                    End If
                    Select Case LuN2
                        Case Is <> RuptN2
                            RuptN2 = LuN2
                            RuptN3 = "(Vide)"
                            RuptN4 = "(Vide)"
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN2 & VI.Tild & LuN1 & LuN2 & VI.Tild & VI.PointdeVue.PVueGrades
                            NoeudN2 = V_AjouterNoeudValeur(TreeListeRef, NoeudN1, LuN2, ClefListe)
                    End Select
                    Select Case LuN3
                        Case Is <> RuptN3
                            RuptN3 = LuN3
                            RuptN4 = "(Vide)"
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN3 & VI.Tild & LuN1 & LuN2 & LuN3 & VI.Tild & VI.PointdeVue.PVueGrades
                            NoeudN3 = V_AjouterNoeudValeur(TreeListeRef, NoeudN2, LuN3, ClefListe)
                    End Select
                    Select Case LuN4
                        Case Is <> RuptN4
                            RuptN4 = LuN4
                            ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuN4 & VI.Tild & LuN1 & LuN2 & LuN3 & LuN4 & VI.Tild & VI.PointdeVue.PVueGrades
                            NoeudN4 = V_AjouterNoeudValeur(TreeListeRef, NoeudN3, LuN4, ClefListe)
                    End Select
                    ClefListe = FicheRef.Ide_Dossier & VI.Tild & LuValeur & VI.Tild & ClefSel & VI.Tild & VI.PointdeVue.PVueGrades
                    VNoeud = V_AjouterNoeudValeur(TreeListeRef, NoeudN4, LuValeur, ClefListe)
                    CptSel += 1
                Next
        End Select
        If V_VirtuelPath <> "" Then
            Try
                TreeListeRef.FindNode(V_VirtuelPath).Selected = V_SiMajMenuVisible
                TreeListeRef.FindNode(V_VirtuelPath).Parent.Expanded = True
            Catch ex As Exception
                Exit Try
            End Try
        End If
        Select Case CptSel
            Case Is = 1
                EtiStatus1.Text = "Une référence"
            Case Else
                EtiStatus1.Text = CptSel.ToString & " références"
        End Select

        Dim NoeudRecherche As TreeNode
        NoeudRecherche = V_AlleraInfoDico(TreeListeRef, 0, True, "")
        If NoeudRecherche IsNot Nothing Then
            If V_SiMajMenuVisible = True Then
                NoeudRecherche.ExpandAll()
            ElseIf V_Date_Selectionnee = "" Then
                NoeudRecherche.ExpandAll()
            End If
        End If
    End Sub

    Private ReadOnly Property ListePersonnes(ByVal PointDevue As Integer) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
        Get
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim ObjetStructure As Virtualia.Systeme.Sgbd.Sql.SqlInterne_Structure
            Dim LstSel As List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection)
            Dim ItemSel As Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection
            Dim LstExt As List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction)

            LstSel = New List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection)
            Select Case PointDevue
                Case VI.PointdeVue.PVueApplicatif
                    ItemSel = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Selection(VI.ObjetPer.ObaSociete, 1)
                    LstSel.Add(ItemSel)
            End Select

            LstExt = New List(Of Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction)
            Select Case PointDevue
                Case VI.PointdeVue.PVueApplicatif
                    LstExt.Add(New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction(VI.ObjetPer.ObaCivil, 2, 1))
                    LstExt.Add(New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction(VI.ObjetPer.ObaCivil, 3, 2))
                Case VI.PointdeVue.PVuePerExterne
                    LstExt.Add(New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction(1, 1, 1))
                    LstExt.Add(New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Extraction(1, 2, 2))
            End Select

            ObjetStructure = New Virtualia.Systeme.Sgbd.Sql.SqlInterne_Structure(PointDevue)
            ObjetStructure.SiForcerClauseDistinct = True
            ObjetStructure.Operateur_Liaison = VI.Operateurs.ET
            ObjetStructure.SiPasdeTriSurIdeDossier = True

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(V_WebFonction.PointeurGlobal.VirModele, V_WebFonction.PointeurGlobal.VirInstanceBd)
            Constructeur.StructureRequete = ObjetStructure
            Constructeur.ListeInfosAExtraire = LstExt
            Constructeur.ListeInfosASelectionner = LstSel

            Return V_WebFonction.PointeurGlobal.VirServiceServeur.RequeteSql_ToListeType(V_WebFonction.PointeurGlobal.VirNomUtilisateur, PointDevue, 1, Constructeur.OrdreSqlDynamique)
        End Get
    End Property

    Private Function ListeUtiVersion3() As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
        If My.Computer.FileSystem.DirectoryExists(System.Configuration.ConfigurationManager.AppSettings("RepertoireUtilisateurV3")) = False Then
            Return Nothing
        End If
        Dim LstFichiers As List(Of String)
        Dim LstResultat As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
        Dim UtilisateurV3 As Virtualia.Version3.Utilisateur.UtilisateurV3
        Dim LstPVue As List(Of Virtualia.Version3.Utilisateur.UtiV3PointDeVue)
        Dim UtiOK As Virtualia.Net.ServiceServeur.VirRequeteType
        Dim NomUti As String = ""

        LstFichiers = My.Computer.FileSystem.GetFiles(System.Configuration.ConfigurationManager.AppSettings("RepertoireUtilisateurV3")).ToList
        LstResultat = New List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
        For Each Fichier In LstFichiers
            If Fichier.ToUpper.EndsWith(".XML") = True Then
                NomUti = Strings.Left(My.Computer.FileSystem.GetFileInfo(Fichier).Name, My.Computer.FileSystem.GetFileInfo(Fichier).Name.Length - 4)
                UtilisateurV3 = New Virtualia.Version3.Utilisateur.UtilisateurV3(System.Configuration.ConfigurationManager.AppSettings("RepertoireUtilisateurV3"), NomUti)
                LstPVue = UtilisateurV3.Database(0).ListePointsdeVue
                For Each Pvue In LstPVue
                    If Pvue.Numero = VI.PointdeVue.PVueFormation And Pvue.Autorisation = "Ecriture" Then
                        UtiOK = New Virtualia.Net.ServiceServeur.VirRequeteType
                        UtiOK.Ide_Dossier = 9999
                        UtiOK.Valeurs = New List(Of String)
                        UtiOK.Valeurs.Add(NomUti)
                        LstResultat.Add(UtiOK)
                    End If
                Next
            End If
        Next
        Return LstResultat
    End Function
End Class