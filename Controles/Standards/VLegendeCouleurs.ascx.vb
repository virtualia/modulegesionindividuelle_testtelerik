﻿Option Strict On
Option Explicit On
Option Compare Text
Partial Class Controles_VLegendeCouleurs
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsInstanceCouleur As Virtualia.Systeme.Planning.CouleursJour

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call FaireLegendes()
    End Sub

    Private Sub FaireLegendes()
        Dim Ctl As Control
        Dim VirControle As System.Web.UI.WebControls.Label
        Dim IndiceI As Integer = 0
        WsInstanceCouleur = V_WebFonction.PointeurUtilisateur.V_PointeurGlobal.InstanceCouleur
        Do
            Ctl = V_WebFonction.VirWebControle(Me.VLegende, "EtiLegende", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
            If WsInstanceCouleur.LegendePlanning(IndiceI) <> "" Then
                VirControle.Text = WsInstanceCouleur.LegendePlanning(IndiceI)
                VirControle.Visible = True
            Else
                VirControle.Visible = False
            End If
            IndiceI += 1
        Loop
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.VLegende, "EtiCouleur", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
            If WsInstanceCouleur.LegendePlanning(IndiceI) <> "" Then
                VirControle.BackColor = WsInstanceCouleur.Item(IndiceI).Couleur_Web
                VirControle.ForeColor = WsInstanceCouleur.Item(IndiceI).Couleur_Etiquette
                VirControle.Visible = True
            Else
                VirControle.Visible = False
            End If
            IndiceI += 1
        Loop
    End Sub

End Class
