﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VPlanningMensuel
    
    '''<summary>
    '''Contrôle VPlanningMois.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VPlanningMois As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VMois.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VMois As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiMois.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiMois As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiAnnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiAnnee As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VJour00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour00 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM00 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM00 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour01 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM01 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM01 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour02 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM02 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM02 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour03 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM03 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM03 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour04 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM04 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM04 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour05 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM05 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM05 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour06 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM06 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM06 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour07 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM07 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM07 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour08 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM08 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM08 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour09 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM09 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM09 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour10 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM10 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM10 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour11 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM11 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM11 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour12 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM12 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM12 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour13 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM13 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM13 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour14 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM14 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM14 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour15 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM15 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM15 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour16 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM16 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM16 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour17 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM17 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM17 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour18 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM18 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM18 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour19 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM19 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM19 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour20 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM20 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM20 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour21 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM21 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM21 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour22 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM22 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM22 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour23 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM23 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM23 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour24 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM24 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM24 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour25 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM25 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM25 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour26 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM26 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM26 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour27 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM27 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM27 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour28.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour28 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM28.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM28 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM28.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM28 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour29.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour29 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM29.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM29 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM29.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM29 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour30.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour30 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM30.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM30 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM30.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM30 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour31 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM31 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM31 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour32 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM32 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM32 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour33.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour33 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM33.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM33 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM33.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM33 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour34.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour34 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM34.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM34 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM34.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM34 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour35.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour35 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM35.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM35 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM35.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM35 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle VJour36.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VJour36 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM36.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM36 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM36.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM36 As Global.System.Web.UI.WebControls.Button
End Class
