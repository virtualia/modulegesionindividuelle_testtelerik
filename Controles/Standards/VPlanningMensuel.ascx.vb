﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VPlanningMensuel
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsCtl_Cache As Virtualia.Net.VCaches.CacheCalendrier
    Private WsNomStateCache As String = "VPlanningMensuel"
    Private WsInstanceCouleur As Virtualia.Systeme.Planning.CouleursJour

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Public Property Identifiant(ByVal DateValeur As String) As Integer
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Ide_Dossier
        End Get
        Set(ByVal value As Integer)
            If value = 0 Then
                Exit Property
            End If
            If DateValeur = "" Then
                DateValeur = V_WebFonction.ViRhDates.DateduJour
            End If
            WsCtl_Cache = CacheVirControle
            If WsCtl_Cache.Ide_Dossier = value And WsCtl_Cache.Date_Valeur = DateValeur Then
                Exit Property
            End If
            WsCtl_Cache = New Virtualia.Net.VCaches.CacheCalendrier
            WsCtl_Cache.Ide_Dossier = value
            WsCtl_Cache.Date_Valeur = DateValeur
            WsCtl_Cache.Annee_Selection = V_WebFonction.ViRhDates.DateTypee(DateValeur).Year
            WsCtl_Cache.Mois_Selection = V_WebFonction.ViRhDates.DateTypee(DateValeur).Month
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Private Property CacheVirControle As Virtualia.Net.VCaches.CacheCalendrier
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.VCaches.CacheCalendrier)
            End If
            Dim NewCache As Virtualia.Net.VCaches.CacheCalendrier
            NewCache = New Virtualia.Net.VCaches.CacheCalendrier
            NewCache.Annee_Selection = System.DateTime.Now.Year
            NewCache.Mois_Selection = System.DateTime.Now.Month
            NewCache.Coche_Butoir = ""
            Return NewCache
        End Get
        Set(value As Virtualia.Net.VCaches.CacheCalendrier)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Public Property SiMoisVisible() As Boolean
        Get
            Return VPlanningMois.Visible
        End Get
        Set(ByVal value As Boolean)
            VPlanningMois.Visible = value
        End Set
    End Property

    Public Property LibelleMois() As String
        Get
            Return EtiMois.Text
        End Get
        Set(ByVal value As String)
            EtiMois.Text = value
        End Set
    End Property

    Public Property LibelleAnnee() As String
        Get
            Return EtiAnnee.Text
        End Get
        Set(ByVal value As String)
            EtiAnnee.Text = value
        End Set
    End Property

    Private WriteOnly Property SiJourVisible(ByVal NoJour As Integer, ByVal Prefixe As String) As Boolean
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.Visible = value
            If value = False Then
                VirControle.BackColor = Drawing.Color.Transparent
                VirControle.BorderStyle = BorderStyle.None
                VirControle.Text = ""
                VirControle.ToolTip = ""
            Else
                VirControle.BorderStyle = BorderStyle.Solid
            End If
        End Set
    End Property

    Public Property SiJourEnable(ByVal NoJour As Integer, ByVal Prefixe As String) As Boolean
        Get
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Return False
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            Return VirControle.Enabled
        End Get
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.Enabled = value
        End Set
    End Property

    Private WriteOnly Property VText(ByVal NoJour As Integer, ByVal Prefixe As String) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.Text = value
        End Set
    End Property

    Private WriteOnly Property VDemiJourToolTip(ByVal NoJour As Integer, ByVal Prefixe As String) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.ToolTip = value
        End Set
    End Property

    Private WriteOnly Property VBorduresColor(ByVal Prefixe As String) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim IndiceI As Integer
            For IndiceI = 0 To 36
                VBorderColor(IndiceI, Prefixe) = value
            Next IndiceI
        End Set
    End Property

    Private WriteOnly Property VBackColor(ByVal NoJour As Integer, ByVal Prefixe As String) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.BackColor = value
            If VirControle.BackColor.GetBrightness > 0.35 Then
                VirControle.ForeColor = Drawing.Color.Black
            Else
                VirControle.ForeColor = Drawing.Color.White
            End If
        End Set
    End Property

    Private WriteOnly Property VBorderColor(ByVal NoJour As Integer, ByVal Prefixe As String) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.BorderColor = value
        End Set
    End Property

    Public WriteOnly Property VJoursStyle() As String
        Set(ByVal value As String)
            Dim IndiceI As Integer
            For IndiceI = 0 To 36
                VJourStyle(IndiceI, "CalAM") = value
                VJourStyle(IndiceI, "CalPM") = value
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property VJourStyle(ByVal NoJour As Integer, ByVal Prefixe As String) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Dim StyleBordure As String = "border-top-style"
            Dim NumBouton As Integer

            If Prefixe = "CalAM" Then
                StyleBordure = "border-bottom-style"
            End If
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            NumBouton = CInt(Strings.Right(VirControle.ID, 2))
            If NumBouton = NoJour Then
                VirControle.Style.Remove(StyleBordure)
                VirControle.Style.Add(StyleBordure, value)
            End If
        End Set
    End Property

    Public WriteOnly Property VFontSize(ByVal NoJour As Integer, ByVal Prefixe As String) As System.Web.UI.WebControls.FontUnit
        Set(ByVal value As System.Web.UI.WebControls.FontUnit)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.Font.Size = value
        End Set
    End Property

    Public WriteOnly Property VFontBold(ByVal NoJour As Integer, ByVal Prefixe As String) As Boolean
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.Font.Bold = value
        End Set
    End Property

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If Me.IsPostBack Then
            WsCtl_Cache = CacheVirControle
            Call FairePlanning()
        End If
    End Sub

    Private Sub FairePlanning()
        Dim LstAbs As List(Of Virtualia.Net.Controles.ItemJourCalendrier) = Nothing
        Dim FicheAbs As Virtualia.Net.Controles.ItemJourCalendrier = Nothing
        Dim Mois As Integer
        Dim Annee As Integer
        Dim DateW As String
        Dim DateFin As String
        Dim NoJour As Integer
        Dim IndiceJ As Integer
        Dim IndiceCtl As Integer
        Dim Ide_Dossier As Integer = WsCtl_Cache.Ide_Dossier
        Dim Couleur As Drawing.Color

        WsInstanceCouleur = V_WebFonction.PointeurUtilisateur.V_PointeurGlobal.InstanceCouleur

        For IndiceCtl = 0 To 36
            SiJourEnable(IndiceCtl, "CalAM") = True
            SiJourEnable(IndiceCtl, "CalPM") = True
            SiJourVisible(IndiceCtl, "CalAM") = False
            SiJourVisible(IndiceCtl, "CalPM") = False
        Next IndiceCtl
        Annee = WsCtl_Cache.Annee_Selection
        Mois = WsCtl_Cache.Mois_Selection
        DateW = "01/" & Strings.Format(Mois, "00") & "/" & Annee
        DateFin = V_WebFonction.ViRhDates.DateSaisieVerifiee("31" & "/" & Strings.Format(Mois, "00") & "/" & Annee.ToString)

        If Ide_Dossier > 0 Then
            If V_WebFonction.PointeurUtilisateur IsNot Nothing Then
                LstAbs = V_WebFonction.PointeurUtilisateur.V_ObjetCalendrier(Ide_Dossier, "01/01/" & Annee)
            End If
        End If
        NoJour = Weekday(CDate(DateW), Microsoft.VisualBasic.FirstDayOfWeek.Monday) - 1
        LibelleMois = V_WebFonction.ViRhDates.MoisEnClair(CDate(DateW).Month)
        LibelleAnnee = CDate(DateW).Year.ToString
        IndiceJ = NoJour
        Do 'Traitement des jours du mois
            SiJourVisible(IndiceJ, "CalAM") = True
            SiJourVisible(IndiceJ, "CalPM") = True
            VJourStyle(IndiceJ, "CalAM") = "none"
            VJourStyle(IndiceJ, "CalPM") = "none"
            VBorderColor(IndiceJ, "CalAM") = V_WebFonction.ConvertCouleur("#216B68")
            VBorderColor(IndiceJ, "CalPM") = V_WebFonction.ConvertCouleur("#216B68")
            VText(IndiceJ, "CalAM") = CInt(Strings.Left(DateW, 2)).ToString
            VDemiJourToolTip(IndiceJ, "CalAM") = V_WebFonction.ViRhDates.ClairDate(DateW, True)
            VDemiJourToolTip(IndiceJ, "CalPM") = ""
            If V_WebFonction.ViRhDates.SiJourOuvre(DateW, True) = True Then
                VBackColor(IndiceJ, "CalAM") = WsInstanceCouleur.Item(0).Couleur_Web
                VBackColor(IndiceJ, "CalPM") = WsInstanceCouleur.Item(0).Couleur_Web
            Else
                VBackColor(IndiceJ, "CalAM") = WsInstanceCouleur.Item(1).Couleur_Web
                VBackColor(IndiceJ, "CalPM") = WsInstanceCouleur.Item(1).Couleur_Web
                If V_WebFonction.ViRhDates.SiJourFerie(DateW) = True Then
                    VBorderColor(IndiceJ, "CalAM") = Drawing.Color.Red
                    VBorderColor(IndiceJ, "CalPM") = Drawing.Color.Red
                End If
            End If
            '*** Le Planning de la personne ***********************
            FicheAbs = Nothing
            If Ide_Dossier > 0 Then
                If LstAbs IsNot Nothing Then
                    FicheAbs = LstAbs.Find(Function(Recherche) Recherche.Date_Valeur_ToDate = CDate(DateW))
                End If
                If FicheAbs IsNot Nothing Then
                    Select Case FicheAbs.ObjetOrigine
                        Case VI.ObjetPer.ObaFormation
                            Couleur = WsInstanceCouleur.Item(WsInstanceCouleur.IndexCouleurPlanning("Formation")).Couleur_Web
                        Case VI.ObjetPer.ObaMission
                            Couleur = WsInstanceCouleur.Item(WsInstanceCouleur.IndexCouleurPlanning("Mission")).Couleur_Web
                        Case VI.ObjetPer.ObaSaisieIntranet
                            Couleur = WsInstanceCouleur.Item(4).Couleur_Web
                        Case Else
                            Couleur = WsInstanceCouleur.Item(WsInstanceCouleur.IndexCouleurPlanning(FicheAbs.Intitule)).Couleur_Web
                    End Select
                    Select Case FicheAbs.Caracteristique
                        Case VI.NumeroPlage.Jour_Absence, VI.NumeroPlage.Jour_Formation, VI.NumeroPlage.Jour_Mission
                            VJourStyle(IndiceJ, "CalAM") = "none"
                            VBackColor(IndiceJ, "CalAM") = Couleur
                            VDemiJourToolTip(IndiceJ, "CalAM") = FicheAbs.Intitule
                            VBackColor(IndiceJ, "CalPM") = Couleur
                            VDemiJourToolTip(IndiceJ, "CalPM") = FicheAbs.Intitule
                            SiJourEnable(IndiceJ, "CalAM") = False
                            SiJourEnable(IndiceJ, "CalPM") = False
                        Case VI.NumeroPlage.Plage1_Absence, VI.NumeroPlage.Plage1_Formation, VI.NumeroPlage.Plage1_Mission
                            VJourStyle(IndiceJ, "CalAM") = "solid"
                            VBackColor(IndiceJ, "CalAM") = Couleur
                            VDemiJourToolTip(IndiceJ, "CalAM") = FicheAbs.Intitule
                            SiJourEnable(IndiceJ, "CalAM") = False
                        Case VI.NumeroPlage.Plage2_Absence, VI.NumeroPlage.Plage2_Formation, VI.NumeroPlage.Plage2_Mission
                            VJourStyle(IndiceJ, "CalAM") = "solid"
                            VBackColor(IndiceJ, "CalPM") = Couleur
                            VDemiJourToolTip(IndiceJ, "CalPM") = FicheAbs.Intitule
                            SiJourEnable(IndiceJ, "CalPM") = False
                    End Select
                End If
            End If
            DateW = V_WebFonction.ViRhDates.CalcDateMoinsJour(DateW, "1", "0")
            If CDate(DateW) > CDate(DateFin) Then
                Exit Do
            End If
            NoJour = Weekday(DateValue(DateW), Microsoft.VisualBasic.FirstDayOfWeek.Monday) - 1
            IndiceJ += 1
        Loop
    End Sub

End Class