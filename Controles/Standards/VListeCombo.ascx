﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VListeCombo" Codebehind="VListeCombo.ascx.vb" %>
<link href="../../VirtualiaSkin/Dropdown.VirtualiaSkin.css" rel="stylesheet" type="text/css" />
<link href="../../VirtualiaSkin/Label.VirtualiaSkin.css" rel="stylesheet" type="text/css" />



<asp:Table ID="ComboVirtualia" runat="server" CellPadding="1" CellSpacing="0" > 
    <asp:TableRow>
        <asp:TableCell>
            <telerik:RadLabel ID="Etiquette" runat="server" Height="20px" Width="200px"
                    BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                    BorderWidth="2px" ForeColor="#142425"
                    Font-Bold="False"
                    style="margin-top: 0px; margin-left: 4px; font-style: oblique;
                    text-indent: 5px; text-align: left"></telerik:RadLabel>
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadDropDownList ID="VComboBox" runat="server" Visible="True">
            </telerik:RadDropDownList>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>