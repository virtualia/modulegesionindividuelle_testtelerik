﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class Controles_VMenuIcones
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Private Sub CmdOKClick(sender As Object, e As Systeme.Evenements.AppelChoixEventArgs) Handles Cmd01.CmdOK_Click, _
        Cmd02.CmdOK_Click, Cmd03.CmdOK_Click, Cmd04.CmdOK_Click, Cmd05.CmdOK_Click, Cmd06.CmdOK_Click, Cmd07.CmdOK_Click, _
        Cmd08.CmdOK_Click, Cmd09.CmdOK_Click, Cmd10.CmdOK_Click, Cmd11.CmdOK_Click, Cmd12.CmdOK_Click

        If e.Valeurs Is Nothing Then
            Exit Sub
        End If
        Dim TabValeurs As ArrayList = e.Valeurs
        Dim UrlWse As String = ""
        If TabValeurs Is Nothing Then
            Exit Sub
        End If
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        If IsNumeric(TabValeurs(0)) Then
            If Session.Item("NumeroLogo") IsNot Nothing Then
                Session.Remove("NumeroLogo")
            End If
            Session.Add("NumeroLogo", CInt(TabValeurs(0)))
        End If

        If TabValeurs(2).ToString = "" Then
            If TabValeurs(1) Is Nothing Then
                Exit Sub
            End If
            If TabValeurs(1).ToString = "" Then
                Exit Sub
            End If
            Server.Transfer(TabValeurs(1).ToString)
        End If

        Dim ChaineUrl As String = "~/Fenetres/Commun/FrmAttente.aspx"

        Select Case TabValeurs(2).ToString
            Case "FRM"
                ChaineUrl &= "&Index=" & CInt(TabValeurs(0))
                ChaineUrl &= "&Lien=" & TabValeurs(1).ToString
            Case "ID", "V3"
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
                ChaineUrl &= "&Lien=" & WebFct.PointeurGlobal.UrlDestination(Session.SessionID, CInt(TabValeurs(0)), TabValeurs(3).ToString)
            Case "VUE"
                ChaineUrl &= "&Index=" & CInt(TabValeurs(0))
                ChaineUrl &= "&Lien=" & TabValeurs(1).ToString
                If TabValeurs(3) IsNot Nothing Then
                    If TabValeurs(3).ToString <> "" Then
                        ChaineUrl &= "&Param=" & TabValeurs(3).ToString
                        If Strings.Left(TabValeurs(3).ToString, 4) = "Synt" Then
                            If WebFct.PointeurUtilisateur.Param_Etablissement = "(Tous)" Then
                                WebFct.PointeurUtilisateur.Param_Etablissement = ""
                            End If
                        End If
                    End If
                End If
        End Select

        Response.Redirect(ChaineUrl, True)

    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        Cmd06.SiVisible = True
    End Sub
End Class