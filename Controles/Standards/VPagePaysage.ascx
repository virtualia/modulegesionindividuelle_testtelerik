﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VPagePaysage.ascx.vb" Inherits="Virtualia.Net.VPagePaysage" %>
<link href="../../VirtualiaSkin/Label.VirtualiaSkin.css" rel="stylesheet" type="text/css" />

<asp:Table ID="PagePaysage" runat="server" CellPadding="1" CellSpacing="0" Width="1050px" BackColor="White"
        BorderStyle="None" BorderWidth="1px" BorderColor="#B0E0D7" HorizontalAlign="Center" Font-Names="Courier New" Font-Size="Small" > 
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3" Height="10px">
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left"> 
            <telerik:RadLabel ID="EtiDateJour" runat="server" Height="12px" Width="600px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="True" Skin="Silk"
                Font-Bold="true" Font-Names="Courier New" Font-Size="X-Small"
                style="margin-top: 0px; margin-left: 20px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Right"> 
            <telerik:RadLabel ID="EtiPagination" runat="server" Height="12px" Width="200px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="True" Skin="Silk"
                Font-Bold="true" Font-Names="Courier New" Font-Size="X-Small"
                style="margin-top: 0px; text-indent: 5px; text-align: right; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Left"> 
            <telerik:RadLabel ID="EtiNbPages" runat="server" Height="12px" Width="100px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="True" Skin="Silk" Skin="Silk"
                Font-Bold="true" Font-Names="Courier New" Font-Size="X-Small"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne01" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne02" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne03" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne04" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne05" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne06" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne07" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne08" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne09" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne10" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne11" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne12" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne13" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne14" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne15" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne16" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne17" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne18" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne19" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne20" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne21" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne22" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne23" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne24" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne25" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne26" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne27" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne28" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne29" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne30" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne31" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne32" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne33" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne34" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne35" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne36" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne37" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>  
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne38" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne39" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne40" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne41" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne42" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne43" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3">
            <telerik:RadLabel ID="Ligne44" runat="server" Height="14px" Width="1000px"
                BackColor="White" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="Black" Font-Italic="False" Skin="Silk"
                Font-Bold="False" Font-Names="Courier New" Font-Size="90%"
                style="margin-top: 0px; text-align: left; padding-top: 0px;">
            </telerik:RadLabel>    
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="3" Height="10px">
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<div style="page-break-after:always;"></div>
