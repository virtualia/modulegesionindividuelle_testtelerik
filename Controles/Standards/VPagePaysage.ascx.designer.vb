﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VPagePaysage
    
    '''<summary>
    '''Contrôle PagePaysage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PagePaysage As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiDateJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDateJour As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiPagination.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiPagination As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle EtiNbPages.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiNbPages As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne01 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne02 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne03 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne04 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne05 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne06 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne07 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne08 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne09 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne10 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne11 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne12 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne13 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne14 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne15 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne16 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne17 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne18 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne19 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne20 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne21 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne22 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne23 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne24 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne25 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne26 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne27 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne28.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne28 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne29.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne29 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne30.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne30 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne31 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne32 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne33.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne33 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne34.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne34 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne35.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne35 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne36.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne36 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne37.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne37 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne38.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne38 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne39.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne39 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne40.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne40 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne41.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne41 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne42.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne42 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne43.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne43 As Global.Telerik.Web.UI.RadLabel
    
    '''<summary>
    '''Contrôle Ligne44.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne44 As Global.Telerik.Web.UI.RadLabel
End Class
