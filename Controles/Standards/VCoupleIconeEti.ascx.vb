﻿Option Strict Off
Option Explicit On
Option Compare Text
Public Class Controles_VCoupleIconeEti
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Click_EventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs)
    Public Event CmdOK_Click As Click_EventHandler
    Private WsIndex As Integer
    Private WsNavigateURL As String
    Private WsParametreURL As String
    Private WsParametreFRM As String

    Protected Overridable Sub VCommande_Click(ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs)
        RaiseEvent CmdOK_Click(Me, e)
    End Sub

    Public Property VIndex As Integer
        Get
            Return WsIndex
        End Get
        Set(value As Integer)
            WsIndex = value
        End Set
    End Property

    Public Property SiVisible As Boolean
        Get
            Return VIcone.Visible
        End Get
        Set(value As Boolean)
            VIcone.Visible = value
        End Set
    End Property

    Public Property NavigateURL As String
        Get
            Return WsNavigateURL
        End Get
        Set(value As String)
            WsNavigateURL = value
        End Set
    End Property

    Public Property ParametreURL As String
        Get
            Return WsParametreURL
        End Get
        Set(value As String)
            WsParametreURL = value
        End Set
    End Property

    Public Property ParametreFRM As String
        Get
            Return WsParametreFRM
        End Get
        Set(value As String)
            WsParametreFRM = value
        End Set
    End Property

    Public Property IconeURL As String
        Get
            Return CmdChoix.Image.ImageUrl
        End Get
        Set(value As String)
            CmdChoix.Image.ImageUrl = value
        End Set
    End Property

    Public Property IconeTooltip As String
        Get
            Return CmdChoix.ToolTip
        End Get
        Set(ByVal value As String)
            CmdChoix.ToolTip = value
        End Set
    End Property

    Public Property IconeBackColor As System.Drawing.Color
        Get
            Return VIcone.BackColor
        End Get
        Set(value As System.Drawing.Color)
            VIcone.BackColor = value
        End Set
    End Property

    Public WriteOnly Property EtiStyle As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                Etiquette.Style.Remove(Strings.Trim(TableauW(0)))
                Etiquette.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property EtiText As String
        Get
            Return Etiquette.Text
        End Get
        Set(ByVal value As String)
            Etiquette.Text = value
        End Set
    End Property

    Public Property EtiBackColor As System.Drawing.Color
        Get
            Return Etiquette.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BackColor = value
        End Set
    End Property

    Public Property EtiForeColor As System.Drawing.Color
        Get
            Return Etiquette.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.ForeColor = value
        End Set
    End Property

    Public Property EtiTooltip As String
        Get
            Return Etiquette.ToolTip
        End Get
        Set(ByVal value As String)
            Etiquette.ToolTip = value
        End Set
    End Property

    Public Property EtiVisible As Boolean
        Get
            Return Etiquette.Visible
        End Get
        Set(ByVal value As Boolean)
            Etiquette.Visible = value
        End Set
    End Property

    Private Sub CmdChoix_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles CmdChoix.Click
        Dim Evenement As Virtualia.Systeme.Evenements.AppelChoixEventArgs
        Dim TabValeurs As New ArrayList
        TabValeurs.Add(VIndex)
        TabValeurs.Add(NavigateURL)
        TabValeurs.Add(ParametreURL)
        TabValeurs.Add(ParametreFRM)
        Evenement = New Virtualia.Systeme.Evenements.AppelChoixEventArgs(TabValeurs)
        VCommande_Click(Evenement)
    End Sub
End Class