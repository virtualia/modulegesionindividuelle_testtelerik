﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VLegendeCouleurs" Codebehind="VLegendeCouleurs.ascx.vb" %>
<link href="../../VirtualiaSkin/Label.VirtualiaSkin.css" rel="stylesheet" type="text/css" />


<asp:Table ID="VLegende" runat="server" Width="800px" CellPadding="0" CellSpacing="5" 
                        BorderStyle="None" HorizontalAlign="Center" >
    <asp:TableRow Height="20" HorizontalAlign="Center" VerticalAlign="Middle">
        <asp:TableCell>
            <telerik:RadLabel ID="EtiCouleur00" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiLegende00" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiCouleur01" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiLegende01" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiCouleur02" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiLegende02" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiCouleur03" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiLegende03" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow Height="20" HorizontalAlign="Center" VerticalAlign="Middle">
        <asp:TableCell>
            <telerik:RadLabel ID="EtiCouleur04" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiLegende04" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiCouleur05" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiLegende05" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiCouleur06" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiLegende06" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiCouleur07" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiLegende07" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow Height="20" HorizontalAlign="Center" VerticalAlign="Middle">
        <asp:TableCell>
            <telerik:RadLabel ID="EtiCouleur08" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiLegende08" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiCouleur09" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiLegende09" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiCouleur10" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiLegende10" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiCouleur11" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiLegende11" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow Height="20" HorizontalAlign="Center" VerticalAlign="Middle">
        <asp:TableCell>
            <telerik:RadLabel ID="EtiCouleur12" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiLegende12" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiCouleur13" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiLegende13" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiCouleur14" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiLegende14" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiCouleur15" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <telerik:RadLabel ID="EtiLegende15" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
