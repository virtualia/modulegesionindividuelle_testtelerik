﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VDuoEtiquetteCommande" Codebehind="VDuoEtiquetteCommande.ascx.vb" %>
<link href="../../VirtualiaSkin/Label.VirtualiaSkin.css" rel="stylesheet" type="text/css" />


<asp:Table ID="VStandard" runat="server" CellPadding="1" CellSpacing="0">
    <asp:TableRow>
        <asp:TableCell>
            <Telerik:RadLabel ID="Etiquette" runat="server" Font-Size="Medium"
                    style="margin-top: 0px;  text-indent: 5px; text-align: left" >
            </Telerik:RadLabel>
        </asp:TableCell>
        <asp:TableCell>
            <Telerik:RadButton ID="DonneeTable" runat="server" Height="25px" Width="300px" Visible="true" CommandName="Ref" OnCommand="DonneeTable_Commande"
                    BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset" BorderWidth="2px" ForeColor="#25355A" Skin="Silk" EnableEmbeddedSkins="true"
                    Font-Bold="false" Font-Names="Calibri" Font-Size="Medium" Font-Italic="false"
                    style="padding-left: 1px; margin-top: 2px; text-align: center" > 
            </Telerik:RadButton>
        </asp:TableCell>
    </asp:TableRow>
  </asp:Table>
