﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VTrioVerticalRadio" Codebehind="VTrioVerticalRadio.ascx.vb" %>

    <asp:table ID="CadreOption" runat="server" height="60px" CellPadding="1" CellSpacing="0">
         <asp:TableRow>
             <asp:TableCell>
                 <telerik:RadRadioButton ID="RadioV0" runat="server" Text="Option N1" Visible="true"
                      AutoPostBack="true" ValidationGroup="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                      BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                      Font-Bold="false" Skin="Silk" EnableEmbeddedSkins="true"
                      style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                      text-indent: 5px; text-align: center; vertical-align: middle" />
             </asp:TableCell>
         </asp:TableRow>
         <asp:TableRow>
             <asp:TableCell>
                 <telerik:RadRadioButton ID="RadioV1" runat="server" Text="Option N2" 
                      AutoPostBack="true" ValidationGroup="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                      BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                      Font-Bold="false" Skin="Silk" EnableEmbeddedSkins="true"
                      style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                      text-indent: 5px; text-align: center; vertical-align: middle" />
            </asp:TableCell>
         </asp:TableRow>
         <asp:TableRow>
             <asp:TableCell>
                 <telerik:RadRadioButton ID="RadioV2" runat="server" Text="Option N3" 
                      AutoPostBack="true" ValidationGroup="OptionV" ForeColor="#142425" Height="18px" Width="200px"
                      BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                      Font-Bold="false" Skin="Silk" EnableEmbeddedSkins="true"
                      style="margin-top: 0px; margin-left: 5px; font-style: oblique;
                      text-indent: 5px; text-align: center; vertical-align: middle" />
             </asp:TableCell>
        </asp:TableRow>
    </asp:table>