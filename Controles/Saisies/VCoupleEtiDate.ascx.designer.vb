﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VCoupleEtiDate
    
    '''<summary>
    '''Contrôle VSaisieDate.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VSaisieDate As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VStandardDate.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VStandardDate As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CellEtiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEtiquette As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreDonnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreDonnee As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CellCommande.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellCommande As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CmdCalendrier.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CmdCalendrier As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Contrôle Donnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Donnee As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle CellCalPopup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellCalPopup As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle PopupCal.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PopupCal As Global.AjaxControlToolkit.ModalPopupExtender
    
    '''<summary>
    '''Contrôle PanelCalPopup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelCalPopup As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle CalendrierMensuel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalendrierMensuel As Global.Virtualia.Net.VSaisieCalendrier
    
    '''<summary>
    '''Contrôle HPopupCal.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HPopupCal As Global.System.Web.UI.WebControls.HiddenField
End Class
