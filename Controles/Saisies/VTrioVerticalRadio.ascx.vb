﻿Option Strict Off
Option Explicit On
Option Compare Text
Partial Class Controles_VTrioVerticalRadio
    Inherits Virtualia.Net.Controles.ObjetWebControlSaisie
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WsSiEnLectureSeule As Boolean = False

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public Property V_SiAutoPostBack() As Boolean
        Get
            Return RadioV0.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            If V_SiEnLectureSeule = True Then
                value = False
            End If
            RadioV0.AutoPostBack = value
            RadioV1.AutoPostBack = value
            RadioV2.AutoPostBack = value
        End Set
    End Property

    Public Property V_SiEnLectureSeule() As Boolean
        Get
            Return WsSiEnLectureSeule
        End Get
        Set(ByVal value As Boolean)
            WsSiEnLectureSeule = value
            If value = True Then
                V_SiAutoPostBack = False
            End If
        End Set
    End Property

    Public Property V_Groupe() As String
        Get
            Return RadioV0.ValidationGroup
        End Get
        Set(ByVal value As String)
            RadioV0.ValidationGroup = value
            RadioV1.ValidationGroup = value
            RadioV2.ValidationGroup = value
        End Set
    End Property

    Public Property RadioGaucheText() As String
        Get
            Return RadioV0.Text
        End Get
        Set(ByVal value As String)
            RadioV0.Text = value
        End Set
    End Property

    Public Property RadioGaucheCheck() As Boolean
        Get
            Return RadioV0.Checked
        End Get
        Set(ByVal value As Boolean)
            If value = True Then
                RadioV0.Checked = True
                RadioV1.Checked = False
                RadioV2.Checked = False
                RadioV0.Font.Bold = True
                RadioV1.Font.Bold = False
                RadioV2.Font.Bold = False
            End If
        End Set
    End Property

    Public Property RadioGaucheHeight() As System.Web.UI.WebControls.Unit
        Get
            Return RadioV0.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioV0.Height = value
        End Set
    End Property

    Public Property RadioGaucheWidth() As System.Web.UI.WebControls.Unit
        Get
            Return RadioV0.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioV0.Width = value
        End Set
    End Property

    Public Property RadioGaucheBackColor() As System.Drawing.Color
        Get
            Return RadioV0.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioV0.BackColor = value
        End Set
    End Property

    Public Property RadioGaucheForeColor() As System.Drawing.Color
        Get
            Return RadioV0.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioV0.ForeColor = value
        End Set
    End Property

    Public Property RadioGaucheBorderColor() As System.Drawing.Color
        Get
            Return RadioV0.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioV0.BorderColor = value
        End Set
    End Property

    Public Property RadioGaucheBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return RadioV0.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioV0.BorderWidth = value
        End Set
    End Property

    Public Property RadioGaucheBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return RadioV0.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            RadioV0.BorderStyle = value
        End Set
    End Property

    Public Property RadioGaucheVisible() As Boolean
        Get
            Return RadioV0.Visible
        End Get
        Set(ByVal value As Boolean)
            RadioV0.Visible = value
        End Set
    End Property

    Public Property RadioCentreText() As String
        Get
            Return RadioV1.Text
        End Get
        Set(ByVal value As String)
            RadioV1.Text = value
        End Set
    End Property

    Public Property RadioCentreCheck() As Boolean
        Get
            Return RadioV1.Checked
        End Get
        Set(ByVal value As Boolean)
            If value = True Then
                RadioV0.Checked = False
                RadioV1.Checked = True
                RadioV2.Checked = False
                RadioV0.Font.Bold = False
                RadioV1.Font.Bold = True
                RadioV2.Font.Bold = False
            End If
        End Set
    End Property

    Public Property RadioCentreHeight() As System.Web.UI.WebControls.Unit
        Get
            Return RadioV1.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioV1.Height = value
        End Set
    End Property

    Public Property RadioCentreWidth() As System.Web.UI.WebControls.Unit
        Get
            Return RadioV1.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioV1.Width = value
        End Set
    End Property

    Public Property RadioCentreBackColor() As System.Drawing.Color
        Get
            Return RadioV1.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioV1.BackColor = value
        End Set
    End Property

    Public Property RadioCentreForeColor() As System.Drawing.Color
        Get
            Return RadioV1.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioV1.ForeColor = value
        End Set
    End Property

    Public Property RadioCentreBorderColor() As System.Drawing.Color
        Get
            Return RadioV1.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioV1.BorderColor = value
        End Set
    End Property

    Public Property RadioCentreBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return RadioV1.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioV1.BorderWidth = value
        End Set
    End Property

    Public Property RadioCentreBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return RadioV1.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            RadioV1.BorderStyle = value
        End Set
    End Property

    Public Property RadioCentreVisible() As Boolean
        Get
            Return RadioV1.Visible
        End Get
        Set(ByVal value As Boolean)
            RadioV1.Visible = value
        End Set
    End Property

    Public Property RadioDroiteText() As String
        Get
            Return RadioV2.Text
        End Get
        Set(ByVal value As String)
            RadioV2.Text = value
        End Set
    End Property

    Public Property RadioDroiteCheck() As Boolean
        Get
            Return RadioV2.Checked
        End Get
        Set(ByVal value As Boolean)
            If value = True Then
                RadioV0.Checked = False
                RadioV1.Checked = False
                RadioV2.Checked = True
                RadioV0.Font.Bold = False
                RadioV1.Font.Bold = False
                RadioV2.Font.Bold = True
            End If
        End Set
    End Property

    Public Property RadioDroiteHeight() As System.Web.UI.WebControls.Unit
        Get
            Return RadioV2.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioV2.Height = value
        End Set
    End Property

    Public Property RadioDroiteWidth() As System.Web.UI.WebControls.Unit
        Get
            Return RadioV2.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioV2.Width = value
        End Set
    End Property

    Public Property RadioDroiteBackColor() As System.Drawing.Color
        Get
            Return RadioV2.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioV2.BackColor = value
        End Set
    End Property

    Public Property RadioDroiteForeColor() As System.Drawing.Color
        Get
            Return RadioV2.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioV2.ForeColor = value
        End Set
    End Property

    Public Property RadioDroiteBorderColor() As System.Drawing.Color
        Get
            Return RadioV2.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            RadioV2.BorderColor = value
        End Set
    End Property

    Public Property RadioDroiteBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return RadioV2.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            RadioV2.BorderWidth = value
        End Set
    End Property

    Public Property RadioDroiteBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return RadioV2.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            RadioV2.BorderStyle = value
        End Set
    End Property

    Public Property RadioDroiteVisible() As Boolean
        Get
            Return RadioV2.Visible
        End Get
        Set(ByVal value As Boolean)
            RadioV2.Visible = value
        End Set
    End Property

    Public WriteOnly Property RadioGaucheStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                RadioV0.Style.Remove(Strings.Trim(TableauW(0)))
                RadioV0.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property RadioCentreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                RadioV1.Style.Remove(Strings.Trim(TableauW(0)))
                RadioV1.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property RadioDroiteStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                RadioV2.Style.Remove(Strings.Trim(TableauW(0)))
                RadioV2.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Protected Sub RadioVirtualia_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioV0.CheckedChanged, RadioV1.CheckedChanged, RadioV2.CheckedChanged
        If V_SiEnLectureSeule = True Then
            Exit Sub
        End If
        RadioV0.Font.Bold = False
        RadioV1.Font.Bold = False
        RadioV2.Font.Bold = False
        CType(sender, Telerik.Web.UI.RadRadioButton).Font.Bold = True
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(CType(sender, Telerik.Web.UI.RadRadioButton).Text)
        Saisie_Change(Evenement)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If V_SiDonneeDico = True Then
            Select Case RadioV0.Checked
                Case True
                    RadioV0.BackColor = V_InverseBackColor
                Case False
                    RadioV0.BackColor = V_EtiBackcolor
            End Select
            RadioV0.ForeColor = V_EtiForecolor
            RadioV0.BorderColor = V_EtiBordercolor
            Select Case RadioV1.Checked
                Case True
                    RadioV1.BackColor = V_InverseBackColor
                Case False
                    RadioV1.BackColor = V_EtiBackcolor
            End Select
            RadioV1.ForeColor = V_EtiForecolor
            RadioV1.BorderColor = V_EtiBordercolor
            Select Case RadioV2.Checked
                Case True
                    RadioV2.BackColor = V_InverseBackColor
                Case False
                    RadioV2.BackColor = V_EtiBackcolor
            End Select
            RadioV2.ForeColor = V_EtiForecolor
            RadioV2.BorderColor = V_EtiBordercolor

            RadioV0.Font.Name = V_FontName
            RadioV0.Font.Size = V_FontTaille
            RadioV0.Font.Italic = V_FontItalic
            RadioV1.Font.Name = V_FontName
            RadioV1.Font.Size = V_FontTaille
            RadioV1.Font.Italic = V_FontItalic
            RadioV2.Font.Name = V_FontName
            RadioV2.Font.Size = V_FontTaille
            RadioV2.Font.Italic = V_FontItalic
        End If
    End Sub
End Class
