﻿Option Strict Off
Option Explicit On
Option Compare Text
Partial Class Controles_VCocheSimple
    Inherits Virtualia.Net.Controles.ObjetWebControlSaisie
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WsSiEnLectureSeule As Boolean = False
    Private WsSiModeCaractere As Boolean = False

    Public Property V_SiAutoPostBack() As Boolean
        Get
            Return CaseACocher.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            If V_SiEnLectureSeule = True Then
                CaseACocher.AutoPostBack = False
                Exit Property
            End If
            CaseACocher.AutoPostBack = value
        End Set
    End Property

    Public Property V_SiEnLectureSeule() As Boolean
        Get
            Return WsSiEnLectureSeule
        End Get
        Set(ByVal value As Boolean)
            WsSiEnLectureSeule = value
            If value = True Then
                V_SiAutoPostBack = False
            End If
        End Set
    End Property

    Public Property V_SiModeCaractere As Boolean
        Get
            Return WsSiModeCaractere
        End Get
        Set(ByVal value As Boolean)
            WsSiModeCaractere = value
        End Set
    End Property

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public WriteOnly Property V_Style() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CaseACocher.Style.Remove(Strings.Trim(TableauW(0)))
                CaseACocher.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property V_Text() As String
        Get
            Return CaseACocher.Text
        End Get
        Set(ByVal value As String)
            CaseACocher.Text = value
        End Set
    End Property

    Public Property V_Check() As Boolean
        Get
            Return CaseACocher.Checked
        End Get
        Set(ByVal value As Boolean)
            CaseACocher.Checked = value
            CaseACocher.Font.Bold = value
        End Set
    End Property

    Public Property V_Height() As System.Web.UI.WebControls.Unit
        Get
            Return CaseACocher.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            CaseACocher.Height = value
        End Set
    End Property

    Public Property V_Width() As System.Web.UI.WebControls.Unit
        Get
            Return CaseACocher.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            CaseACocher.Width = value
        End Set
    End Property

    Public Property V_BackColor() As System.Drawing.Color
        Get
            Return CaseACocher.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            CaseACocher.BackColor = value
        End Set
    End Property

    Public Property V_ForeColor() As System.Drawing.Color
        Get
            Return CaseACocher.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            CaseACocher.ForeColor = value
        End Set
    End Property

    Public Property V_BorderColor() As System.Drawing.Color
        Get
            Return CaseACocher.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            CaseACocher.BorderColor = value
        End Set
    End Property

    Public Property V_BorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return CaseACocher.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            CaseACocher.BorderWidth = value
        End Set
    End Property

    Public Property V_BorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return CaseACocher.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            CaseACocher.BorderStyle = value
        End Set
    End Property

    Public Property V_Visible() As Boolean
        Get
            Return CaseACocher.Visible
        End Get
        Set(ByVal value As Boolean)
            CaseACocher.Visible = value
        End Set
    End Property

    Public Property DonTabIndex() As Short
        Get
            Return CaseACocher.TabIndex
        End Get
        Set(ByVal value As Short)
            CaseACocher.TabIndex = value
            MyBase.V_TabIndex = value
        End Set
    End Property

    Protected Sub CaseACocher_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CaseACocher.CheckedChanged
        If V_SiEnLectureSeule = True Then
            Exit Sub
        End If
        CType(sender, Telerik.Web.UI.RadCheckBox).Font.Bold = CType(sender, Telerik.Web.UI.RadCheckBox).Checked
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs = Nothing
        If CType(sender, Telerik.Web.UI.RadCheckBox).Checked = True Then
            Select Case V_SiModeCaractere
                Case True
                    Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(CType(sender, Telerik.Web.UI.RadCheckBox).Text)
                Case False
                    Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Oui")
            End Select
        Else
            Select Case V_SiModeCaractere
                Case True
                    Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("")
                Case False
                    Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Non")
            End Select
        End If
        Saisie_Change(Evenement)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If V_SiDonneeDico = True And V_Text = "" Then
            CaseACocher.Text = V_WebFonction.PointeurDicoInfo(V_PointdeVue, V_Objet, V_Information).Etiquette
        End If
        If V_SiDonneeDico = True Then
            'CaseACocher.BackColor = V_EtiBackcolor
            'CaseACocher.ForeColor = V_EtiForecolor
            'CaseACocher.BorderColor = V_EtiBordercolor

            CaseACocher.Font.Name = V_FontName
            CaseACocher.Font.Size = V_FontTaille
            CaseACocher.Font.Italic = V_FontItalic
        End If
    End Sub
End Class