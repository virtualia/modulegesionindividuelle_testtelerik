﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VSaisieCalendrier
    
    '''<summary>
    '''Contrôle VCalendrier.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VCalendrier As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitre As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CellCalTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellCalTitre As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTitre As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CmdCancel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CmdCancel As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CadreLstAnnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreLstAnnee As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiLstAnnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLstAnnee As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CalLstAnnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalLstAnnee As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Contrôle CmdOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CmdOK As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CadreLstMois.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreLstMois As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiLstMois.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLstMois As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CalLstMois.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalLstMois As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Contrôle CalLibelles.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalLibelles As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiLundi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLundi As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiMardi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiMardi As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiMercredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiMercredi As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiJeudi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJeudi As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiVendredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiVendredi As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSamedi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSamedi As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiDimanche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDimanche As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CalSemaine00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalSemaine00 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM00 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM01 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM02 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM03 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM04 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM05 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM06 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM00 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM01 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM02 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM03 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM04 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM05 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM06 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalSemaine01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalSemaine01 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM07 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM08 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM09 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM10 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM11 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM12 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM13 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM07 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM08 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM09 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM10 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM11 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM12 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM13 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalSemaine02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalSemaine02 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM14 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM15 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM16 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM17 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM18 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM19 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM20 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM14 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM15 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM16 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM17 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM18 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM19 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM20 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalSemaine03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalSemaine03 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM21 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM22 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM23 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM24 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM25 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM26 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM27 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM21 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM22 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM23 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM24 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM25 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM26 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM27 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalSemaine04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalSemaine04 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM28.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM28 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM29.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM29 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM30.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM30 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM31 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM32 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM33.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM33 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM34.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM34 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM28.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM28 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM29.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM29 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM30.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM30 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM31 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM32 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM33.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM33 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM34.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM34 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalSemaine05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalSemaine05 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CalAM35.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM35 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalAM36.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM36 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalDateSel00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalDateSel00 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CalPM35.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM35 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalPM36.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM36 As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CalDateSel01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalDateSel01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CalRadio.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalRadio As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CadreOptionJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreOptionJour As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle RadioV0.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioV0 As Global.System.Web.UI.WebControls.RadioButton
    
    '''<summary>
    '''Contrôle RadioV1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioV1 As Global.System.Web.UI.WebControls.RadioButton
    
    '''<summary>
    '''Contrôle RadioV2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioV2 As Global.System.Web.UI.WebControls.RadioButton
End Class
