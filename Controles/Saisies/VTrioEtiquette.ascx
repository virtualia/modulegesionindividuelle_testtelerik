﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VTrioEtiquette" Codebehind="VTrioEtiquette.ascx.vb" %>
<link href="../../VirtualiaSkin/Label.VirtualiaSkin.css" rel="stylesheet" type="text/css" />

<asp:Table ID="CadreLabel" runat="server" Height="20px" Width="306px" HorizontalAlign="Center" CellPadding="1" CellSpacing="0" >
    <asp:TableRow>
        <asp:TableCell>
            <Telerik:RadLabel ID="LabelGauche" runat="server" Height="18px" Width="100px"
                BackColor="#B0E0D7" Visible="true" BorderColor="#B0E0D7" 
                BorderStyle="Inset" BorderWidth="2px" ForeColor="#142425"
                Font-Bold="False"
                style="margin-top: 0px; margin-left: 4px; font-style: oblique;
                text-indent: 2px; text-align: center"></Telerik:RadLabel>
        </asp:TableCell>
        <asp:TableCell>
            <Telerik:RadLabel ID="LabelCentre" runat="server" Height="18px" Width="100px"
                BackColor="#B0E0D7" Visible="true" BorderColor="#B0E0D7"  
                BorderStyle="Inset" BorderWidth="2px" ForeColor="#142425"
                Font-Bold="False"
                style="font-style: oblique; text-indent: 2px; text-align: center"></Telerik:RadLabel>
        </asp:TableCell>
        <asp:TableCell>
            <Telerik:RadLabel ID="LabelDroite" runat="server" Height="18px" Width="100px"
                BackColor="#B0E0D7" Visible="true" BorderColor="#B0E0D7" 
                BorderStyle="Inset" BorderWidth="2px" ForeColor="#142425"
                Font-Bold="False" 
                style="font-style: oblique; text-indent: 2px; text-align: center"></Telerik:RadLabel>
         </asp:TableCell> 
    </asp:TableRow>               
</asp:Table>      

