﻿Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Telerik.Web.UI

Partial Class Controles_VCoupleEtiDonnee
    Inherits Virtualia.Net.Controles.ObjetWebControlSaisie
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Dim WsNature As String = ""

    '***********************************************************

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public Property V_SiAutoPostBack() As Boolean
        Get
            Return Donnee.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            If V_SiEnLectureSeule = True Then
                Donnee.AutoPostBack = False
                Exit Property
            End If
            Donnee.AutoPostBack = value
        End Set
    End Property

    Public Property V_SiEnLectureSeule() As Boolean
        Get
            Return Donnee.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Donnee.ReadOnly = value
            If value = True Then
                V_SiAutoPostBack = False
            End If
        End Set
    End Property

    Public Property SiCryptage() As Boolean
        Get
            If Donnee.TextMode = TextBoxMode.Password Then
                Return True
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            If value = True Then
                Donnee.TextMode = Telerik.Web.UI.InputMode.Password
            Else
                Donnee.TextMode = Telerik.Web.UI.InputMode.SingleLine
            End If
        End Set
    End Property

    Public WriteOnly Property DonStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                Donnee.Style.Remove(Strings.Trim(TableauW(0)))
                Donnee.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property DonText() As String
        Get
            Return Donnee.Text
        End Get
        Set(ByVal value As String)
            Donnee.Text = value
        End Set
    End Property

    Public Property DonHeight() As System.Web.UI.WebControls.Unit
        Get
            Return Donnee.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Donnee.Height = value
        End Set
    End Property

    Public Property DonWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Donnee.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Donnee.Width = value
        End Set
    End Property

    Public Property DonBackColor() As System.Drawing.Color
        Get
            Return Donnee.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Donnee.BackColor = value
        End Set
    End Property

    Public Property DonForeColor() As System.Drawing.Color
        Get
            Return Donnee.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Donnee.ForeColor = value
        End Set
    End Property

    Public Property DonBorderColor() As System.Drawing.Color
        Get
            Return Donnee.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Donnee.BorderColor = value
        End Set
    End Property

    Public Property DonBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Donnee.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Donnee.BorderWidth = value
        End Set
    End Property

    Public Property DonBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return Donnee.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            Donnee.BorderStyle = value
        End Set
    End Property

    Public Property DonTooltip() As String
        Get
            Return Donnee.ToolTip
        End Get
        Set(ByVal value As String)
            Donnee.ToolTip = value
        End Set
    End Property

    Public Property DonVisible() As Boolean
        Get
            Return Donnee.Visible
        End Get
        Set(ByVal value As Boolean)
            Donnee.Visible = value
        End Set
    End Property

    Public Property DonMaxLength() As Integer
        Get
            Return Donnee.MaxLength
        End Get
        Set(ByVal value As Integer)
            Donnee.MaxLength = value
        End Set
    End Property

    Public Property DonTabIndex() As Short
        Get
            Return Donnee.TabIndex
        End Get
        Set(ByVal value As Short)
            Donnee.TabIndex = value
            MyBase.V_TabIndex = value
        End Set
    End Property

    Public WriteOnly Property EtiStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                'Etiquette.Style.Remove(Strings.Trim(TableauW(0)))
                'Etiquette.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property EtiText() As String
        Get
            Return Etiquette.Text
        End Get
        Set(ByVal value As String)
            Etiquette.Text = value
        End Set
    End Property

    Public Property EtiHeight() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Height = value
        End Set
    End Property

    Public Property EtiWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Width = value
        End Set
    End Property

    Public Property EtiBackColor() As System.Drawing.Color
        Get
            Return Etiquette.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BackColor = value
        End Set
    End Property

    Public Property EtiForeColor() As System.Drawing.Color
        Get
            Return Etiquette.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.ForeColor = value
        End Set
    End Property

    Public Property EtiBorderColor() As System.Drawing.Color
        Get
            Return Etiquette.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BorderColor = value
        End Set
    End Property

    Public Property EtiBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.BorderWidth = value
        End Set
    End Property

    Public Property EtiBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return Etiquette.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            Etiquette.BorderStyle = value
        End Set
    End Property

    Public Property EtiTooltip() As String
        Get
            Return Etiquette.ToolTip
        End Get
        Set(ByVal value As String)
            Etiquette.ToolTip = value
        End Set
    End Property

    Public Property EtiVisible() As Boolean
        Get
            Return Etiquette.Visible
        End Get
        Set(ByVal value As Boolean)
            Etiquette.Visible = value
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If V_SiDonneeDico = True And Etiquette.Text = "" Then
                Etiquette.Text = V_WebFonction.PointeurDicoInfo(V_PointdeVue, V_Objet, V_Information).Etiquette
            End If
            If V_SiDonneeDico = True And Donnee.MaxLength = 0 Then
                Donnee.MaxLength = V_WebFonction.PointeurDicoInfo(V_PointdeVue, V_Objet, V_Information).LongueurMaxi
                V_Nature = V_WebFonction.PointeurDicoInfo(V_PointdeVue, V_Objet, V_Information).VNature
                Select Case V_Nature
                    Case VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeHeure
                        Donnee.Style.Remove("text-align")
                        Donnee.Style.Add("text-align", "center")
                    Case VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeCalculee
                        Donnee.Style.Remove("text-align")
                        Donnee.Style.Add("text-align", "center")
                        V_Format = V_WebFonction.PointeurDicoInfo(V_PointdeVue, V_Objet, V_Information).Format
                        If Donnee.Text <> "" Then
                            Dim Montant As Double = V_WebFonction.ViRhFonction.ConversionDouble(Donnee.Text)
                            Select Case V_Format
                                Case VI.FormatDonnee.Fixe
                                    Donnee.Text = Strings.Format(Montant, "0")
                                Case VI.FormatDonnee.Decimal1
                                    Donnee.Text = Strings.Format(Montant, "0.0")
                                Case VI.FormatDonnee.Decimal1, VI.FormatDonnee.Monetaire
                                    Donnee.Text = Strings.Format(Montant, "0.00")
                                Case VI.FormatDonnee.Decimal3
                                    Donnee.Text = Strings.Format(Montant, "0.000")
                                Case VI.FormatDonnee.Decimal4
                                    Donnee.Text = Strings.Format(Montant, "0.0000")
                                Case VI.FormatDonnee.Decimal5
                                    Donnee.Text = Strings.Format(Montant, "0.00000")
                                Case VI.FormatDonnee.Decimal6
                                    Donnee.Text = Strings.Format(Montant, "0.000000")
                            End Select
                        End If
                End Select
            End If
        Catch ex As Exception
        End Try

        If V_SiDonneeDico = True Then
            'Donnee.BorderColor = V_DonBordercolor
            'Etiquette.BackColor = V_EtiBackcolor
            'Etiquette.ForeColor = V_EtiForecolor
            'Etiquette.BorderColor = V_EtiBordercolor

            'Donnee.Font.Name = V_FontName
            'Donnee.Font.Size = V_FontTaille
            'Etiquette.Font.Name = V_FontName
            'Etiquette.Font.Size = V_FontTaille
            'Etiquette.Font.Bold = V_FontBold
            'Etiquette.Font.Italic = V_FontItalic
        End If
    End Sub

    Protected Sub Donnee_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Donnee.TextChanged
        Dim Chaine As String = ""
        If V_SiDonneeDico = True Then
            V_Nature = V_WebFonction.PointeurDicoInfo(V_PointdeVue, V_Objet, V_Information).VNature
            V_Format = V_WebFonction.PointeurDicoInfo(V_PointdeVue, V_Objet, V_Information).Format
        End If

        If Donnee.Text <> "" Then
            Chaine = V_WebFonction.Controle_LostFocusStd(V_Nature, V_Format, Donnee.Text)
        End If
        Donnee.Text = Chaine

        If V_SiEnLectureSeule = True Then
            Exit Sub
        End If

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Chaine)
        Saisie_Change(Evenement)
    End Sub
    Public Sub SetValidator(ByVal value As String)
        If value = "required" Then
            Dim requiredvalidator As New RequiredFieldValidator
            requiredvalidator.ControlToValidate = "Donnee"
            requiredvalidator.Attributes.Add("runat", "server")
            requiredvalidator.ErrorMessage = "veuillez remplir ce champ"
            Dim tablecell As New TableCell()
            tablecell.Controls.Add(requiredvalidator)
            Dim row As New TableRow()
            row.Cells.Add(tablecell)
            'CadreTextBox.Rows.Add(row)
            Donnee.Attributes.Add("Style", "margin-top: 17px;")
        End If

        If value = "email" Then
            Dim emailregex As New RegularExpressionValidator
            emailregex.ControlToValidate = "Donnee"
            emailregex.Attributes.Add("runat", "server")
            emailregex.ValidationExpression = "\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
            emailregex.ErrorMessage = "Format d'email invalide"
            Dim tablecell As New TableCell()
            tablecell.Controls.Add(emailregex)
            Dim row As New TableRow()
            row.Cells.Add(tablecell)
            'CadreTextBox.Rows.Add(row)
            Donnee.Attributes.Add("Style", "margin-top: 17px;")
        End If

        If value = "range" Then
            Dim range As New RangeValidator
            range.ControlToValidate = "Donnee"
            range.Attributes.Add("runat", "server")
            range.MinimumValue = "0"
            range.MaximumValue = "2000"
            range.ErrorMessage = "[0..2000]"
            Dim tablecell As New TableCell()
            tablecell.Controls.Add(range)
            Dim row As New TableRow()
            row.Cells.Add(tablecell)
            'CadreTextBox.Rows.Add(row)
            Donnee.Attributes.Add("Style", "margin-top: 19px;")
        End If

    End Sub


    'Public WriteOnly Property SetValidatorText() As String

    '    Set(ByVal value As String)
    '        Validation_Donnee.ErrorMessage = value
    '        Donnee.CssClass = "validator_align"
    '    End Set
    'End Property

    Public Sub ControlDonnee(ByVal param As String)

        If param = "entier" Then
            Donnee.Attributes.Add("onkeypress", "return isNumber(event);")
            Donnee.Attributes.Add("oncopy", "return false;")
            Donnee.Attributes.Add("onpaste", "return false;")
        End If

        If param = "char" Then
            Donnee.Attributes.Add("onkeypress", "return isLetter(event);")
            Donnee.Attributes.Add("oncopy", "return false;")
            Donnee.Attributes.Add("onpaste", "return false;")
        End If

        If param = "date" Then
            Donnee.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
            Donnee.Attributes.Add("placeholder", "DD/MM/AAAA")
            Donnee.Attributes.Add("oncopy", "return false;")
            Donnee.Attributes.Add("onpaste", "return false;")
        End If


        If param = "nv_char" Then
            Donnee.Attributes.Add("onkeypress", "return isLetter(event);")
        End If

        If param = "nv_entier" Then
            Donnee.Attributes.Add("onkeypress", "return isNumber(event);")
        End If
    End Sub

End Class
