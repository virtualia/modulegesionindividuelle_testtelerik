﻿Option Strict On
Option Explicit On
Option Compare Text
Partial Class Controles_VCoupleVerticalEtiquetteExperte
    Inherits Virtualia.Net.Controles.ObjetWebControlSaisie
    Private WsInfoExperte As Short = 0

    Public Property V_InfoExperte() As Short
        Get
            Return WsInfoExperte
        End Get
        Set(ByVal value As Short)
            WsInfoExperte = value
        End Set
    End Property

    Public WriteOnly Property V_Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_SiDonneeDico = False Then
                Exit Property
            End If
            If V_PointdeVue = 0 Or V_Objet = 0 Or V_InfoExperte = 0 Then
                Exit Property
            End If
            DonText = V_WebFonction.InfoExperte(V_PointdeVue, V_Objet, V_InfoExperte, value)
        End Set
    End Property

    Public WriteOnly Property DonStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                EtiDonnee.Style.Remove(Strings.Trim(TableauW(0)))
                EtiDonnee.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property DonText() As String
        Get
            Return EtiDonnee.Text
        End Get
        Set(ByVal value As String)
            EtiDonnee.Text = value
        End Set
    End Property

    Public Property DonHeight() As System.Web.UI.WebControls.Unit
        Get
            Return EtiDonnee.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            EtiDonnee.Height = value
        End Set
    End Property

    Public Property DonWidth() As System.Web.UI.WebControls.Unit
        Get
            Return EtiDonnee.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            EtiDonnee.Width = value
        End Set
    End Property

    Public Property DonBackColor() As System.Drawing.Color
        Get
            Return EtiDonnee.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            EtiDonnee.BackColor = value
        End Set
    End Property

    Public Property DonForeColor() As System.Drawing.Color
        Get
            Return EtiDonnee.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            EtiDonnee.ForeColor = value
        End Set
    End Property

    Public Property DonBorderColor() As System.Drawing.Color
        Get
            Return EtiDonnee.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            EtiDonnee.BorderColor = value
        End Set
    End Property

    Public Property DonBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return EtiDonnee.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            EtiDonnee.BorderWidth = value
        End Set
    End Property

    Public Property DonBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return EtiDonnee.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            EtiDonnee.BorderStyle = value
        End Set
    End Property

    Public Property DonTooltip() As String
        Get
            Return EtiDonnee.ToolTip
        End Get
        Set(ByVal value As String)
            EtiDonnee.ToolTip = value
        End Set
    End Property

    Public Property DonVisible() As Boolean
        Get
            Return EtiDonnee.Visible
        End Get
        Set(ByVal value As Boolean)
            EtiDonnee.Visible = value
        End Set
    End Property

    Public Property DonTabIndex() As Short
        Get
            Return EtiDonnee.TabIndex
        End Get
        Set(ByVal value As Short)
            EtiDonnee.TabIndex = value
        End Set
    End Property

    Public WriteOnly Property EtiStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                Etiquette.Style.Remove(Strings.Trim(TableauW(0)))
                Etiquette.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property EtiText() As String
        Get
            Return Etiquette.Text
        End Get
        Set(ByVal value As String)
            Etiquette.Text = value
        End Set
    End Property

    Public Property EtiHeight() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Height = value
        End Set
    End Property

    Public Property EtiWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Width = value
        End Set
    End Property

    Public Property EtiBackColor() As System.Drawing.Color
        Get
            Return Etiquette.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BackColor = value
        End Set
    End Property

    Public Property EtiForeColor() As System.Drawing.Color
        Get
            Return Etiquette.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.ForeColor = value
        End Set
    End Property

    Public Property EtiBorderColor() As System.Drawing.Color
        Get
            Return Etiquette.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BorderColor = value
        End Set
    End Property

    Public Property EtiBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.BorderWidth = value
        End Set
    End Property

    Public Property EtiBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return Etiquette.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            Etiquette.BorderStyle = value
        End Set
    End Property

    Public Property EtiTooltip() As String
        Get
            Return Etiquette.ToolTip
        End Get
        Set(ByVal value As String)
            Etiquette.ToolTip = value
        End Set
    End Property

    Public Property EtiVisible() As Boolean
        Get
            Return Etiquette.Visible
        End Get
        Set(ByVal value As Boolean)
            Etiquette.Visible = value
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If V_SiDonneeDico = True Then
            If Etiquette.Text = "" Then
                Etiquette.Text = V_WebFonction.PointeurDicoExperte(V_PointdeVue, V_Objet, V_InfoExperte).Intitule
            End If
            Etiquette.ToolTip = V_WebFonction.PointeurDicoExperte(V_PointdeVue, V_Objet, V_InfoExperte).Description
        End If
    End Sub
End Class
