﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Controles_VReliquatAnciennete
    Inherits Virtualia.Net.Controles.ObjetWebControlSaisie
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    '***********************************************************
    Private WsSiAncienneteVisible As Boolean = True
    Private WsNatureDonnee As Short = 0
    Private WsFormatDonnee As Short = 0
    Private WsNomState As String = "Reliquat"

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public Property V_SiAutoPostBack() As Boolean
        Get
            Return DateAnciennete.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            If V_SiEnLectureSeule = True Then
                value = False
            End If
            DateAnciennete.AutoPostBack = value
            Jours.AutoPostBack = value
            Mois.AutoPostBack = value
            Annees.AutoPostBack = value
        End Set
    End Property

    Public Property V_SiEnLectureSeule() As Boolean
        Get
            Return DateAnciennete.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            DateAnciennete.ReadOnly = value
            Jours.ReadOnly = value
            Mois.ReadOnly = value
            Annees.ReadOnly = value
            If value = True Then
                V_SiAutoPostBack = False
            End If
        End Set
    End Property

    Public Property V_DateValeur() As String
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) Is Nothing Then
                VCache = New ArrayList
                VCache.Add(V_WebFonction.ViRhDates.DateduJour)
            Else
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
            End If
            Return VCache(0).ToString
        End Get
        Set(ByVal value As String)
            If Me.ViewState(WsNomState) IsNot Nothing Then
                Me.ViewState.Remove(WsNomState)
            End If
            Dim VCache As New ArrayList
            VCache.Add(value)
            Me.ViewState.Add(WsNomState, VCache)
        End Set
    End Property

    Public Property Anciennete_Numerique() As Integer
        Get
            Dim JJ As Integer = 0
            Dim MM As Integer = 0
            Dim AA As Integer = 0
            Dim Chaine As String

            If IsNumeric(Annees.Text) Then
                AA = CInt(Annees.Text)
            End If
            If IsNumeric(Mois.Text) Then
                MM = CInt(Mois.Text)
            End If
            If IsNumeric(Jours.Text) Then
                JJ = CInt(Jours.Text)
            End If
            Chaine = Strings.Format(AA, "00") & Strings.Format(MM, "00") & Strings.Format(JJ, "00")
            Return CInt(Chaine)
        End Get
        Set(ByVal value As Integer)
            Dim JJ As String
            Dim MM As String
            Dim AA As String
            Dim Chaine As String

            If value = 0 Then
                DateAnciennete.Text = ""
                Annees.Text = ""
                Mois.Text = ""
                Jours.Text = ""
                If Me.ViewState(WsNomState) IsNot Nothing Then
                    Me.ViewState.Remove(WsNomState)
                End If
                Exit Property
            End If
            Chaine = Strings.Format(value, "000000")
            AA = Strings.Left(Chaine, 2)
            MM = Strings.Mid(Chaine, 3, 2)
            JJ = Strings.Right(Chaine, 2)
            Annees.Text = AA
            Mois.Text = MM
            Jours.Text = JJ
            If WsSiAncienneteVisible = True Then
                DateAnciennete.Text = V_WebFonction.ViRhDates.CalculerDateAvancement(V_DateValeur, "000000", "000000", Chaine)
            End If
        End Set
    End Property

    Private Property Anciennete_Date() As String
        Get
            Return DateAnciennete.Text
        End Get
        Set(ByVal value As String)
            Dim DateW As String
            Dim Chaine As String

            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) Is Nothing Then
                VCache = New ArrayList
                VCache.Add(V_WebFonction.ViRhDates.DateduJour)
            Else
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
            End If
            Select Case V_WebFonction.ViRhDates.ComparerDates(value, VCache(0).ToString)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                    DateAnciennete.Text = ""
                    Annees.Text = ""
                    Mois.Text = ""
                    Jours.Text = ""
                    Exit Property
            End Select
            DateAnciennete.Text = value
            Select Case Val(Strings.Left(value, 2))
                Case Is > Val(Strings.Left(VCache(0).ToString, 2))
                    DateW = V_WebFonction.ViRhDates.CalcDateMoinsJour(value, "0", "1")
                Case Else
                    DateW = V_WebFonction.ViRhDates.DateStandardVirtualia(value)
            End Select
            Chaine = Strings.Right(V_WebFonction.ViRhDates.DureeDates360(DateW, VCache(0).ToString), 6)
            Annees.Text = Strings.Left(Chaine, 2)
            Mois.Text = Strings.Mid(Chaine, 3, 2)
            Jours.Text = Strings.Right(Chaine, 2)
        End Set
    End Property

    Protected Sub DateAnciennete_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DateAnciennete.TextChanged
        Dim Chaine As String = ""

        If DateAnciennete.Text <> "" Then
            Chaine = V_WebFonction.Controle_LostFocusStd(VI.NatureDonnee.DonneeDate, VI.FormatDonnee.Standard, DateAnciennete.Text)
        End If
        Anciennete_Date = Chaine

        Dim VCache As ArrayList
        Dim Anc As Integer
        If Me.ViewState(WsNomState) Is Nothing Then
            VCache = New ArrayList
            VCache.Add(V_WebFonction.ViRhDates.DateduJour)
        Else
            VCache = CType(Me.ViewState(WsNomState), ArrayList)
        End If
        Anc = Anciennete_Numerique

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Anc.ToString)
        Saisie_Change(Evenement)
    End Sub

    Protected Sub Jours_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Jours.TextChanged
        Dim Chaine As String = ""
        Dim Anc As Integer
        If Jours.Text <> "" Then
            Chaine = V_WebFonction.Controle_LostFocusStd(VI.NatureDonnee.DonneeNumerique, VI.FormatDonnee.Fixe, Jours.Text)
        End If
        Jours.Text = Chaine
        If Chaine = "" Then
            Exit Sub
        End If
        Dim VCache As ArrayList
        If Me.ViewState(WsNomState) Is Nothing Then
            VCache = New ArrayList
            VCache.Add(V_WebFonction.ViRhDates.DateduJour)
        Else
            VCache = CType(Me.ViewState(WsNomState), ArrayList)
        End If
        Anc = Anciennete_Numerique
        If WsSiAncienneteVisible = True Then
            Anciennete_Numerique = Anc
        End If

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Anc.ToString)
        Saisie_Change(Evenement)
    End Sub

    Protected Sub Mois_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Mois.TextChanged
        Dim Chaine As String = ""
        Dim Anc As Integer
        If Mois.Text <> "" Then
            Chaine = V_WebFonction.Controle_LostFocusStd(VI.NatureDonnee.DonneeNumerique, VI.FormatDonnee.Fixe, Mois.Text)
        End If
        Mois.Text = Chaine
        If Chaine = "" Then
            Exit Sub
        End If
        Dim VCache As ArrayList
        If Me.ViewState(WsNomState) Is Nothing Then
            VCache = New ArrayList
            VCache.Add(V_WebFonction.ViRhDates.DateduJour)
        Else
            VCache = CType(Me.ViewState(WsNomState), ArrayList)
        End If
        Anc = Anciennete_Numerique
        If WsSiAncienneteVisible = True Then
            Anciennete_Numerique = Anc
        End If

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Anc.ToString)
        Saisie_Change(Evenement)
    End Sub

    Protected Sub Annees_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Annees.TextChanged
        Dim Chaine As String = ""
        Dim Anc As Integer
        If Annees.Text <> "" Then
            Chaine = V_WebFonction.Controle_LostFocusStd(VI.NatureDonnee.DonneeNumerique, VI.FormatDonnee.Fixe, Annees.Text)
        End If
        Annees.Text = Chaine
        If Chaine = "" Then
            Exit Sub
        End If
        Dim VCache As ArrayList
        If Me.ViewState(WsNomState) Is Nothing Then
            VCache = New ArrayList
            VCache.Add(V_WebFonction.ViRhDates.DateduJour)
        Else
            VCache = CType(Me.ViewState(WsNomState), ArrayList)
        End If
        Anc = Anciennete_Numerique
        If WsSiAncienneteVisible = True Then
            Anciennete_Numerique = Anc
        End If

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Anc.ToString)
        Saisie_Change(Evenement)
    End Sub

    Public WriteOnly Property DonStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                DateAnciennete.Style.Remove(Strings.Trim(TableauW(0)))
                DateAnciennete.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
                Jours.Style.Remove(Strings.Trim(TableauW(0)))
                Jours.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
                Mois.Style.Remove(Strings.Trim(TableauW(0)))
                Mois.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
                Annees.Style.Remove(Strings.Trim(TableauW(0)))
                Annees.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property DonHeight() As System.Web.UI.WebControls.Unit
        Get
            Return DateAnciennete.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            DateAnciennete.Height = value
            Jours.Height = value
            Mois.Height = value
            Annees.Height = value
        End Set
    End Property

    Public Property DonBackColor() As System.Drawing.Color
        Get
            Return DateAnciennete.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            DateAnciennete.BackColor = value
            Jours.BackColor = value
            Mois.BackColor = value
            Annees.BackColor = value
        End Set
    End Property

    Public Property DonForeColor() As System.Drawing.Color
        Get
            Return DateAnciennete.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            DateAnciennete.ForeColor = value
            Jours.ForeColor = value
            Mois.ForeColor = value
            Annees.ForeColor = value
        End Set
    End Property

    Public Property DonBorderColor() As System.Drawing.Color
        Get
            Return DateAnciennete.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            DateAnciennete.BorderColor = value
            Jours.BorderColor = value
            Mois.BorderColor = value
            Annees.BorderColor = value
        End Set
    End Property

    Public Property DonBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return DateAnciennete.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            DateAnciennete.BorderWidth = value
            Jours.BorderWidth = value
            Mois.BorderWidth = value
            Annees.BorderWidth = value
        End Set
    End Property

    Public Property DonBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return DateAnciennete.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            DateAnciennete.BorderStyle = value
            Jours.BorderStyle = value
            Mois.BorderStyle = value
            Annees.BorderStyle = value
        End Set
    End Property

    Public Property AncienneteTooltip() As String
        Get
            Return DateAnciennete.ToolTip
        End Get
        Set(ByVal value As String)
            DateAnciennete.ToolTip = value
        End Set
    End Property

    Public Property AncienneteVisible() As Boolean
        Get
            Return DateAnciennete.Visible
        End Get
        Set(ByVal value As Boolean)
            DateAnciennete.Visible = value
            WsSiAncienneteVisible = value
        End Set
    End Property

    Public Property AncienneteTabIndex() As Short
        Get
            Return DateAnciennete.TabIndex
        End Get
        Set(ByVal value As Short)
            DateAnciennete.TabIndex = value
        End Set
    End Property

    Public WriteOnly Property EtiStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                Etiquette.Style.Remove(Strings.Trim(TableauW(0)))
                Etiquette.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property EtiText() As String
        Get
            Return Etiquette.Text
        End Get
        Set(ByVal value As String)
            Etiquette.Text = value
        End Set
    End Property

    Public Property EtiHeight() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Height
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Height = value
        End Set
    End Property

    Public Property EtiWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.Width = value
        End Set
    End Property

    Public Property EtiBackColor() As System.Drawing.Color
        Get
            Return Etiquette.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BackColor = value
        End Set
    End Property

    Public Property EtiForeColor() As System.Drawing.Color
        Get
            Return Etiquette.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.ForeColor = value
        End Set
    End Property

    Public Property EtiBorderColor() As System.Drawing.Color
        Get
            Return Etiquette.BorderColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            Etiquette.BorderColor = value
        End Set
    End Property

    Public Property EtiBorderWidth() As System.Web.UI.WebControls.Unit
        Get
            Return Etiquette.BorderWidth
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Etiquette.BorderWidth = value
        End Set
    End Property

    Public Property EtiBorderStyle() As System.Web.UI.WebControls.BorderStyle
        Get
            Return Etiquette.BorderStyle
        End Get
        Set(ByVal value As System.Web.UI.WebControls.BorderStyle)
            Etiquette.BorderStyle = value
        End Set
    End Property

    Public Property EtiTooltip() As String
        Get
            Return Etiquette.ToolTip
        End Get
        Set(ByVal value As String)
            Etiquette.ToolTip = value
        End Set
    End Property

    Public Property EtiVisible() As Boolean
        Get
            Return Etiquette.Visible
        End Get
        Set(ByVal value As Boolean)
            Etiquette.Visible = value
        End Set
    End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If V_SiDonneeDico = True And Etiquette.Text = "" Then
            Etiquette.Text = V_WebFonction.PointeurDicoInfo(V_PointdeVue, V_Objet, V_Information).Etiquette
        End If
        If V_SiDonneeDico = True Then
            'Etiquette.BackColor = V_EtiBackcolor
            'Etiquette.ForeColor = V_EtiForecolor
            'Etiquette.BorderColor = V_EtiBordercolor

            'DateAnciennete.BorderColor = V_DonBordercolor
            'Jours.BorderColor = V_DonBordercolor
            'Mois.BorderColor = V_DonBordercolor
            'Annees.BorderColor = V_DonBordercolor

            'DateAnciennete.Font.Name = V_FontName
            'DateAnciennete.Font.Size = V_FontTaille
            'Jours.Font.Name = V_FontName
            'Jours.Font.Size = V_FontTaille
            'Mois.Font.Name = V_FontName
            'Mois.Font.Size = V_FontTaille
            'Annees.Font.Name = V_FontName
            'Annees.Font.Size = V_FontTaille
            'Etiquette.Font.Name = V_FontName
            'Etiquette.Font.Size = V_FontTaille
            'Etiquette.Font.Bold = V_FontBold
            'Etiquette.Font.Italic = V_FontItalic
        End If
    End Sub
End Class
