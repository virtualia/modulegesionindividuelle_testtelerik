﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VCoupleEtiDonnee"
    ClassName="VCoupleEtiDonnee" EnableViewState="true" CodeBehind="VCoupleEtiDonnee.ascx.vb" %>

<link href="../../VirtualiaSkin/Textbox.VirtualiaSkin.css" rel="stylesheet" type="text/css" />
<link href="../../VirtualiaSkin/Label.VirtualiaSkin.css" rel="stylesheet" type="text/css" />

<style type="text/css">
    .validator_align {
          margin-top : 20px;
    }   
    .cell_margin {
          margin-left : 100px;
    }

    .form__group {
  white-space: nowrap;
  text-overflow: ellipsis;

 
}

    </style>


<script type="text/javascript"  lang="js"> 

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function isLetter(evt) {
        evt = (evt) ? evt : event;
        var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
            ((evt.which) ? evt.which : 0));
        if (charCode > 31 && (charCode < 65 || charCode > 90) &&
            (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
        }
        
    
</script>
    <asp:Table ID="VStandard" runat="server" CellPadding="1" CellSpacing="0" CssClass="form__group">
    <asp:TableRow>
        <asp:TableCell ID="CellEtiquette">
            <Telerik:RadLabel ID="Etiquette" runat="server" Height="20px" Width="170px"
                    style="margin-top: 0px; margin-left: 4px; text-indent: 5px; text-align: left" >
            </Telerik:RadLabel>
        </asp:TableCell>
        <asp:TableCell>
            <Telerik:RadTextBox ID="Donnee" runat="server" Visible="True" Font-Size="Medium"
                Font-Names="Calibri">
                </Telerik:RadTextBox>
        </asp:TableCell>
    </asp:TableRow>
  </asp:Table>
                   
<%--            <asp:Label ID="Etiquette" runat="server" Height="20px" Width="170px"
                BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                Style="margin-top: 0px; margin-left: 4px; text-indent: 5px; text-align: left;">
            </asp:Label>--%>
            <%--<asp:Table ID="CadreTextBox" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>--%>
                        
                       <%-- <asp:TextBox ID="Donnee" runat="server" Height="16px" Width="300px" MaxLength="0" 
                            BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset"
                            BorderWidth="2px" ForeColor="Black" 
                            Visible="true" AutoPostBack="false" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                            Style="margin-top: 1px; text-indent: 1px; text-align: left" TextMode="SingleLine">
                        </asp:TextBox>--%>
                   <%-- </asp:TableCell>
                </asp:TableRow>
            </asp:Table>--%>




