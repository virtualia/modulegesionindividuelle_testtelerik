﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VZoneSaisie
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomState As String = "ZoneElement"
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public Property SiVisible As Boolean
        Get
            Return Zone.Visible
        End Get
        Set(ByVal value As Boolean)
            Zone.Visible = value
        End Set
    End Property

    Public Property SiNonSaisissable As Boolean
        Get
            Return DonZone.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            DonZone.ReadOnly = value
        End Set
    End Property

    Public WriteOnly Property DonStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                DonZone.Style.Remove(Strings.Trim(TableauW(0)))
                DonZone.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property SiComboEnable As Boolean
        Get
            Return ComboZone.Enabled
        End Get
        Set(value As Boolean)
            ComboZone.Enabled = value
        End Set
    End Property

    Public Property SiComboVisible As Boolean
        Get
            Return ComboZone.Visible
        End Get
        Set(ByVal value As Boolean)
            ComboZone.Visible = value
            Select Case value
                Case True
                    ComboZone.Width = New Unit(360)
                    DonZone.Width = New Unit(100)
                    EtiMasque.Width = New Unit(20)
                    EtiMasque.Visible = False
                Case False
                    DonZone.Width = New Unit(360)
                    ComboZone.Width = New Unit(100)
                    EtiMasque.Width = New Unit(100)
                    EtiMasque.Visible = True
            End Select
        End Set
    End Property

    Public Property Etiquette As String
        Get
            Return EtiZone.Text
        End Get
        Set(ByVal value As String)
            EtiZone.Text = value
        End Set
    End Property

    Public Property NatureDonnee As String
        Get
            Dim VCache As List(Of String)

            VCache = CType(Me.ViewState(WsNomState), List(Of String))
            If VCache Is Nothing Then
                Return ""
            End If
            If VCache(0) Is Nothing Then
                Return ""
            End If
            Return VCache(0).ToString
        End Get
        Set(ByVal value As String)
            Dim VCache As List(Of String)
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), List(Of String))
                VCache(0) = value
                Me.ViewState.Remove(WsNomState)
            End If
            VCache = New List(Of String)
            VCache.Add(value)
            VCache.Add("") 'Format
            VCache.Add("") 'Valeur Sélectionnée
            Me.ViewState.Add(WsNomState, VCache)
            If value = "Table" Then
                SiComboVisible = True
                DonZone.ReadOnly = True
            Else
                SiComboVisible = False
                DonZone.ReadOnly = False
            End If
        End Set
    End Property

    Public Property FormatDonnee As String
        Get
            Dim VCache As List(Of String)

            VCache = CType(Me.ViewState(WsNomState), List(Of String))
            If VCache Is Nothing Then
                Return ""
            End If
            Return VCache(1).ToString
        End Get
        Set(ByVal value As String)
            Dim VCache As List(Of String)
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), List(Of String))
                VCache(1) = value
                Me.ViewState.Remove(WsNomState)
                Me.ViewState.Add(WsNomState, VCache)
            End If
        End Set
    End Property

    Public Property ValeurSelectionnee As String
        Get
            Dim VCache As List(Of String)

            VCache = CType(Me.ViewState(WsNomState), List(Of String))
            If VCache Is Nothing Then
                Return ""
            End If
            Return VCache(2).ToString
        End Get
        Set(ByVal value As String)
            Dim VCache As List(Of String)
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), List(Of String))
                VCache(2) = value
                Me.ViewState.Add(WsNomState, VCache)
            End If
            Try
                ComboZone.Text = value
            Catch ex As Exception
                Exit Try
            End Try
        End Set
    End Property

    Public Property ValeursCombo As List(Of String)
        Get
            Dim IndiceI As Integer
            Dim LstValeurs As New List(Of String)
            For IndiceI = 0 To ComboZone.Items.Count - 1
                LstValeurs.Add(ComboZone.Items(IndiceI).Text)
            Next IndiceI
            Return LstValeurs
        End Get
        Set(ByVal value As List(Of String))
            If value Is Nothing Then
                Exit Property
            End If
            ComboZone.Items.Clear()
            Dim IndiceI As Integer
            For IndiceI = 0 To value.Count - 1
                ComboZone.Items.Add(value.Item(IndiceI))
            Next IndiceI
        End Set
    End Property

    Public Property Longueur As Integer
        Get
            Return DonZone.MaxLength
        End Get
        Set(ByVal value As Integer)
            DonZone.MaxLength = value
        End Set
    End Property

    Public Property TexteSaisi As String
        Get
            Return DonZone.Text
        End Get
        Set(ByVal value As String)
            DonZone.Text = value.Trim
        End Set
    End Property

    Private Sub DonZone_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DonZone.TextChanged
        Dim Chaine As String = ""
        If DonZone.Text <> "" Then
            Chaine = WebFct.LostFocusStd(NatureDonnee, FormatDonnee, Longueur, DonZone.Text)
        End If
        DonZone.Text = Chaine

        'Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        'Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Chaine)
        'Saisie_Change(Evenement)
    End Sub

    Private Sub ComboZone_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboZone.SelectedIndexChanged
        Dim Tableaudata(0) As String
        Dim Chaine As String = ""

        Tableaudata = Strings.Split(ComboZone.SelectedItem.Text, " - ")
        ValeurSelectionnee = ComboZone.SelectedItem.Text
        DonZone.Text = Tableaudata(0)
        If Etiquette = "Dept et code poste" Or Etiquette = "Code VB" Or Etiquette = "Code VBA" Or Etiquette = "Imputation PCE" Then
            Try
                Chaine = Tableaudata(1)
                If Tableaudata.Count > 2 Then
                    Chaine &= Strings.Space(1) & Tableaudata(2)
                End If
            Catch ex As Exception
                Chaine = ""
            End Try
        Else
            Exit Sub
        End If
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(Etiquette, Chaine)
        Saisie_Change(Evenement)
        
    End Sub
End Class