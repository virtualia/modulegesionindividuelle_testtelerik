﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VCoupleVerticalEtiDonnee" Codebehind="VCoupleVerticalEtiDonnee.ascx.vb" %>

<link href="../../VirtualiaSkin/Textbox.VirtualiaSkin.css" rel="stylesheet" type="text/css" />
<link href="../../VirtualiaSkin/Label.VirtualiaSkin.css" rel="stylesheet" type="text/css" />

<style type="text/css">
    .validator_align {
          margin-top : 20px;
    }   
    .cell_margin {
          margin-left : 100px;
    }

    .form__group {
  white-space: nowrap;
  text-overflow: ellipsis;

 
}

    </style>


<script type="text/javascript"  lang="js"> 

    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function isLetter(evt) {
        evt = (evt) ? evt : event;
        var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
            ((evt.which) ? evt.which : 0));
        if (charCode > 31 && (charCode < 65 || charCode > 90) &&
            (charCode < 97 || charCode > 122)) {
            return false;
        }
        return true;
        }
        
    
</script>
  <asp:Table ID="CadreEtiData" runat="server" CellPadding="1" CellSpacing="0" CssClass="form__group">
    <asp:TableRow>
        <asp:TableCell CssClass="cell_margin">      
                        <Telerik:RadLabel ID="Etiquette" runat="server" >
                         </Telerik:RadLabel> 
            </asp:TableCell>
        </asp:TableRow>
    <asp:TableRow>
              <asp:TableCell>      
                        <Telerik:RadTextBox ID="Donnee" runat="server" Visible="True" Font-Size="Medium"
                            Font-Names="Calibri">
                     </Telerik:RadTextBox>  
            </asp:TableCell>
        </asp:TableRow>
      </asp:Table>