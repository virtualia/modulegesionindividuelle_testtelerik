﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VReliquatAnciennete" Codebehind="VReliquatAnciennete.ascx.vb" %>
<link href="../../VirtualiaSkin/Label.VirtualiaSkin.css" rel="stylesheet" type="text/css" />
<link href="../../VirtualiaSkin/Textbox.VirtualiaSkin.css" rel="stylesheet" type="text/css" />

<asp:Table ID="VAnciennete" runat="server" CellPadding="1" CellSpacing="0">
    <asp:TableRow>
        <asp:TableCell>
            <Telerik:RadLabel ID="Etiquette" runat="server" Height="20px" Width="170px"
                    BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                    BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                    Font-Bold="False" Font-Size="Small"
                    style="margin-top: 0px; margin-left: 4px; text-indent: 5px; text-align: left" >
            </Telerik:RadLabel>
        </asp:TableCell>
        <asp:TableCell>
            <Telerik:RadTextBox ID="Annees" runat="server" Height="16px" Width="60px" MaxLength="2"
                    Skin="Silk" EnableEmbeddedSkins="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: center" TextMode="SingleLine"
                    ToolTip="Nombre d'années de reliquat"> 
            </Telerik:RadTextBox>
        </asp:TableCell>
        <asp:TableCell>
            <Telerik:RadTextBox ID="Mois" runat="server" Height="16px" Width="30px" MaxLength="2"
                    Skin="Silk" EnableEmbeddedSkins="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: center" TextMode="SingleLine"
                    ToolTip="Nombre de mois de reliquat">  
            </Telerik:RadTextBox>
        </asp:TableCell>
        <asp:TableCell>
            <Telerik:RadTextBox ID="Jours" runat="server" Height="16px" Width="30px" MaxLength="2"
                    Skin="Silk" EnableEmbeddedSkins="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: center" TextMode="SingleLine"
                    ToolTip="Nombre de jours de reliquat">  
            </Telerik:RadTextBox>
        </asp:TableCell>
        <asp:TableCell>
            <Telerik:RadTextBox ID="DateAnciennete" runat="server" Height="16px" Width="120px" MaxLength="10"
                    Skin="Silk" EnableEmbeddedSkins="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
            </Telerik:RadTextBox>       
        </asp:TableCell>
    </asp:TableRow>
  </asp:Table>