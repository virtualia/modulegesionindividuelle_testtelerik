﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetre_PER_ETATCIVIL
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsNumObjet As Integer = VI.ObjetPer.ObaCivil
    Private WsNomTable As String = "PER_ETATCIVIL"
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
    Private WsDossierPer As Virtualia.Net.Individuel.DossierIndividu
    Private WsParent As Individuel.EnsembleDossiers
    Private Nouvelleconnex As Boolean = False


    Private ReadOnly Property V_Contexte As Virtualia.Net.Individuel.LocalNavigation
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
        End Get
    End Property
    '
    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Private Function SiErreurSpecifique() As List(Of String)
        Dim Ide_Dossier As Integer = V_Identifiant
        Dim TabErreurs As New List(Of String)
        Dim CacheData As List(Of String)
        Dim CacheDoublon As List(Of Integer)

        CacheData = V_CacheMaj

        Dim Dossier As Virtualia.Ressources.Datas.ObjetDossierPER
        If Ide_Dossier > 0 Then
            Dossier = V_WebFonction.PointeurDossier(Ide_Dossier)
        Else
            Dossier = Nothing
        End If
        If CacheData.Item(2) = "" Or CacheData.Item(3) = "" Or CacheData.Item(4) = "" Then
            TabErreurs.Add("Le Nom, le prénom et la date de naissance sont obligatoires.")
            Return TabErreurs
        End If
        '** Mise en Forme Nom et Prénom
        CacheData.Item(2) = V_WebFonction.ViRhFonction.Lettre1Capi(CacheData.Item(2), 2)
        If V_WebFonction.PointeurConfiguration IsNot Nothing Then
            If V_WebFonction.PointeurConfiguration.Parametres_Saisie.Si_Nom_En_Majuscule = True Then
                CacheData.Item(2) = CacheData.Item(2).ToUpper
            End If
        End If
        CacheData.Item(3) = V_WebFonction.ViRhFonction.Lettre1Capi(CacheData.Item(3), 1)
        '** Contrôle Unicité Nom, Prénom, Date de naissance
        CacheDoublon = V_WebFonction.PointeurGlobal.ControleDoublon(VI.PointdeVue.PVueApplicatif, Ide_Dossier, _
                                                              2, CacheData.Item(2), CacheData.Item(3), CacheData.Item(4))
        If CacheDoublon IsNot Nothing Then
            TabErreurs.Add("Il existe déjà un dossier ayant la même identification sous le N° " & CacheDoublon(0).ToString)
            Return TabErreurs
        End If
        '** Contrôle Unicité N° NIR
        If CacheData.Item(11) <> "" Then
            CacheDoublon = V_WebFonction.PointeurGlobal.ControleDoublon(VI.PointdeVue.PVueApplicatif, Ide_Dossier, 11, CacheData.Item(11))
            If CacheDoublon IsNot Nothing Then
                TabErreurs.Add("Il existe déjà un dossier ayant le même NIR sous le N° " & CacheDoublon(0).ToString)
                Return TabErreurs
            End If
        End If
        '** Contrôle de cohérence
        If CacheData.Item(1) = "Monsieur" Then
            CacheData.Item(8) = "Masculin"
        ElseIf CacheData.Item(1) <> "" Then
            CacheData.Item(8) = "Féminin"
        End If
        If CacheData.Item(8) = "Masculin" Then
            CacheData.Item(1) = "Monsieur"
        ElseIf CacheData.Item(8) = "Féminin" Then
            Select Case CacheData.Item(1)
                Case Is = "Madame", "Mademoiselle"
                    Exit Select
                Case Else
                    CacheData.Item(1) = "Madame"
            End Select
        End If
        If V_IndexFiche <> -1 Then
            If Ide_Dossier > 0 Then
                WsFiche = CType(Dossier.Item(V_IndexFiche), Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL)
                If WsFiche Is Nothing Then
                    Return Nothing
                End If
                If WsFiche.SiOK_DateNaissance(CacheData.Item(4)) = False Then
                    TabErreurs.Add("Date de naissance erronée.")
                End If
                If V_WebFonction.PointeurConfiguration IsNot Nothing Then
                    If CacheData.Item(11) <> "" And CacheData.Item(12) <> "" Then
                        If V_WebFonction.PointeurConfiguration.Parametres_Saisie.Si_Controle_NIR = True Then
                            If WsFiche.SiOK_NIR(CacheData.Item(11), CacheData.Item(12)) = False Then
                                TabErreurs.Add("Numéro d'Identification NIR erronée ou Clef NIR erronée.")
                            End If
                        End If
                    End If
                Else
                    If WsFiche.SiOK_NIR(CacheData.Item(11), CacheData.Item(12)) = False Then
                        TabErreurs.Add("Numéro d'Identification NIR erronée ou Clef NIR erronée.")
                    End If
                End If
            End If
            If TabErreurs.Count > 0 Then
                Return TabErreurs
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Public WriteOnly Property Identifiant As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                Call InitialiserControles()
                V_Identifiant = value
                'Spécifique Fiche Etat-Civil
                If V_Fiche IsNot Nothing Then
                    WsFiche = CType(V_Fiche, Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL)
                    If V_WebFonction.PointeurConfiguration IsNot Nothing Then
                        'If V_WebFonction.PointeurConfiguration.Parametres_Saisie.Si_Nom_En_Majuscule = True Then
                        'WsFiche.SiNom_En_Majuscule = True
                        'End If
                    Else
                        WsFiche.SiNom_En_Majuscule = True
                    End If
                End If
                End If
        End Set
    End Property

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee.Item(NumInfo)
            End If
            VirControle.V_SiAutoPostBack = Not CadreCmdOK.Visible
            IndiceI += 1
        Loop

        Dim VirRadio As Controles_VTrioHorizontalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirRadio = CType(Ctl, Controles_VTrioHorizontalRadio)
            If CacheDonnee.Item(NumInfo) IsNot Nothing Then
                If V_IndexFiche <> -1 And CacheDonnee.Item(NumInfo) <> "" Then
                    Select Case CacheDonnee.Item(NumInfo)
                        Case Is = VirRadio.RadioGaucheText
                            VirRadio.RadioGaucheCheck = True
                        Case Is = VirRadio.RadioCentreText
                            VirRadio.RadioCentreCheck = True
                        Case Is = VirRadio.RadioDroiteText
                            VirRadio.RadioDroiteCheck = True
                    End Select
                End If
            End If
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeDate.DonText = ""
            Else
                VirDonneeDate.DonText = CacheDonnee(NumInfo).ToString
            End If
            If VirDonneeDate.SiDateFin = True Then
                VirDonneeDate.DateDebut = CacheDonnee(0).ToString
            End If
            IndiceI += 1
        Loop

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirDonneeTable.DonText = ""
            Else
                VirDonneeTable.DonText = CacheDonnee.Item(NumInfo)
            End If
            IndiceI += 1
        Loop
    End Sub

    Private Sub TraiterExpertes()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiquetteExperte
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreExpert, "ExprH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiquetteExperte)
            VirControle.V_Identifiant = V_Identifiant
            IndiceI += 1
        Loop
    End Sub

    Protected Sub InfoH11_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles InfoH11.Load
        InfoH11.DonMaxLength = CInt(11)
    End Sub
    Private Sub InfoD_ValeurChange(sender As Object, e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoD04.ValeurChange, InfoD10.ValeurChange, InfoD17.ValeurChange
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VCoupleEtiDate).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub
    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH02.ValeurChange,
    InfoH03.ValeurChange, InfoH05.ValeurChange, InfoH11.ValeurChange, InfoH12.ValeurChange, InfoH13.ValeurChange, InfoH14.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee.Item(NumInfo) <> e.Valeur Then
                    CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub
    Protected Sub RadioH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles RadioH01.ValeurChange, RadioH08.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VTrioHorizontalRadio).ID, 2))
        If CacheDonnee IsNot Nothing Then
            CadreCmdOK.Visible = True
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub Dontab_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles Dontab01.AppelTable, _
    Dontab06.AppelTable, Dontab07.AppelTable, Dontab08.AppelTable, Dontab09.AppelTable

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VDuoEtiquetteCommande).ID, 2))
        Select Case NumInfo
            Case 1
                RadioH01.Visible = True
            Case 8
                RadioH08.Visible = True
            Case Else
                Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
                Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(e.ControleAppelant, e.ObjetAppelant, e.PointdeVueInverse, e.NomdelaTable)
                Call V_AppelTable(Evenement)
        End Select
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            Dim NumInfo As Integer
            Dim Ctl As Control
            Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
            Dim CacheDonnee As List(Of String) = V_CacheMaj

            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab" & Strings.Right(IDAppelant, 2), 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonText = value
            If CacheDonnee IsNot Nothing Then
                If CacheDonnee.Item(NumInfo) Is Nothing Then
                    VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                Else
                    If CacheDonnee.Item(NumInfo) <> value Then
                        VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                        CadreCmdOK.Visible = True
                    End If
                End If
                Call V_ValeurChange(NumInfo, value)
            End If
        End Set
    End Property


    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_Objet(0) = WsNumObjet
        V_NomTableSgbd = WsNomTable
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.V_SiEnLectureSeule = True Then
            CadreCmdOK.Visible = False
            Call InitialiserControles()
        End If
        Call LireLaFiche()
        If CadreExpert.Visible = True Then
            Call TraiterExpertes()
        Else
            If V_Identifiant = 0 Then
                Call V_CommandeNewDossier(VI.PointdeVue.PVueApplicatif)
            End If
        End If

        'CadreInfo.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Cadre")
        'CadreInfo.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        'Etiquette.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Titre")
        'Etiquette.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Police_Claire")
        'Etiquette.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        WsDossierPer = V_Contexte.DossierPER

    End Sub

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click

        Dim TabOK As New List(Of String)
        'V_Contexte.DossierPER = V_ListeDossiers.ItemDossier(V_Identifiant)
        'WsDossierPer = V_Contexte.DossierPER

        Call V_MajFiche()
        CadreCmdOK.Visible = False
        'Dim TitreMsg As String = "Contrôle préalable à la mise à jour du dossier de " & WsDossierPer.FicheEtatCivil.Nom & Strings.Space(1) & WsDossierPer.FicheEtatCivil.Prenom
        'TabOK.Add("Le dossier de " & WsDossierPer.FicheEtatCivil.Nom & Strings.Space(1) & WsDossierPer.FicheEtatCivil.Prenom & " a été mis à jour.")
        'Call V_MessageDialog(V_WebFonction.Evt_MessageInformatif(WsNumObjet, TitreMsg, TabOK, V_Identifiant))
    End Sub

    Private Sub InitialiserControles()
        RadioH01.Visible = False
        RadioH08.Visible = False

        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            'VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            VirControle.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            'VirDonneeTable.DonBackColor = Drawing.Color.White
            VirDonneeTable.DonText = ""
            VirDonneeTable.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            'VirDonneeDate.DonBackColor = Drawing.Color.White
            VirDonneeDate.DonText = ""
            VirDonneeDate.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
    End Sub

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property CadreExpertStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreExpert.Style.Remove(Strings.Trim(TableauW(0)))
                CadreExpert.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public Property CadreExpertVisible() As Boolean
        Get
            Return CadreExpert.Visible
        End Get
        Set(ByVal value As Boolean)
            CadreExpert.Visible = value
        End Set
    End Property
End Class
