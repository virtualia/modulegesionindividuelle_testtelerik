﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class CtlPvuePER
    
    '''<summary>
    '''Contrôle CadreGlobal.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreGlobal As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTitre As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PER_MENU.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_MENU As Global.Virtualia.Net.VMenuCommun
    
    '''<summary>
    '''Contrôle CellulePER.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellulePER As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle MultiPER.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MultiPER As Global.System.Web.UI.WebControls.MultiView
    
    '''<summary>
    '''Contrôle VueEtatCivil.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueEtatCivil As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_ETATCIVIL_1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_ETATCIVIL_1 As Global.Virtualia.Net.Fenetre_PER_ETATCIVIL
    
    '''<summary>
    '''Contrôle VuePieceIdentite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VuePieceIdentite As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_PIECE_IDENTITE1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_PIECE_IDENTITE1 As Global.Virtualia.Net.Fenetre_PER_PIECE_IDENTITE
    
    '''<summary>
    '''Contrôle VueAdresse.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAdresse As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_DOMICILE_1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_DOMICILE_1 As Global.Virtualia.Net.Fenetre_PER_DOMICILE
    
    '''<summary>
    '''Contrôle VueAdresse2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAdresse2 As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_DOMICILE_2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_DOMICILE_2 As Global.Virtualia.Net.Fenetre_PER_DOMICILE2
    
    '''<summary>
    '''Contrôle VueResidenceConge.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueResidenceConge As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_RESIDENCE_CONGE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_RESIDENCE_CONGE As Global.Virtualia.Net.Fenetre_PER_RESIDENCE_CONGE
    
    '''<summary>
    '''Contrôle VueAdresseEtranger.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAdresseEtranger As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_ADRESSE_ETRANGER.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_ADRESSE_ETRANGER As Global.Virtualia.Net.Fenetre_PER_ADRESSE_ETRANGER
    
    '''<summary>
    '''Contrôle VueBanque.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueBanque As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_BANQUE_5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_BANQUE_5 As Global.Virtualia.Net.Fenetre_PER_BANQUE
    
    '''<summary>
    '''Contrôle VueEnfant.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueEnfant As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_ENFANT_3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_ENFANT_3 As Global.Virtualia.Net.Fenetre_PER_ENFANT
    
    '''<summary>
    '''Contrôle VueConjoint.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueConjoint As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_CONJOINT1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_CONJOINT1 As Global.Virtualia.Net.Fenetre_PER_CONJOINT
    
    '''<summary>
    '''Contrôle VuePrevenir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VuePrevenir As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_APREVENIR2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_APREVENIR2 As Global.Virtualia.Net.Fenetre_PER_APREVENIR
    
    '''<summary>
    '''Contrôle VueDecoration.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueDecoration As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_DECORATION.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_DECORATION As Global.Virtualia.Net.Fenetre_PER_DECORATION
    
    '''<summary>
    '''Contrôle VueDiplome.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueDiplome As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_DIPLOME.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_DIPLOME As Global.Virtualia.Net.Fenetre_PER_DIPLOME
    
    '''<summary>
    '''Contrôle VueQualif.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueQualif As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_QUALIFICATION.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_QUALIFICATION As Global.Virtualia.Net.Fenetre_PER_QUALIFICATION
    
    '''<summary>
    '''Contrôle VueSpecialite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueSpecialite As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_SPECIALITE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_SPECIALITE As Global.Virtualia.Net.Fenetre_PER_SPECIALITE
    
    '''<summary>
    '''Contrôle Vuethese.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Vuethese As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_THESE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_THESE As Global.Virtualia.Net.Fenetre_PER_THESE
    
    '''<summary>
    '''Contrôle VueExperience.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueExperience As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_EXPERIENCE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_EXPERIENCE As Global.Virtualia.Net.Fenetre_PER_EXPERIENCE
    
    '''<summary>
    '''Contrôle VueLangueEtrangeres.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueLangueEtrangeres As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_LANG_ETRANGER.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_LANG_ETRANGER As Global.Virtualia.Net.Fenetre_PER_LANG_ETRANGER
    
    '''<summary>
    '''Contrôle VueAcquis.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAcquis As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_ACQUIS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_ACQUIS As Global.Virtualia.Net.Fenetre_PER_ACQUIS
    
    '''<summary>
    '''Contrôle VueStage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueStage As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_STAGE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_STAGE As Global.Virtualia.Net.Fenetre_PER_STAGE
    
    '''<summary>
    '''Contrôle VueLoisir.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueLoisir As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_LOISIR.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_LOISIR As Global.Virtualia.Net.Fenetre_PER_LOISIR
    
    '''<summary>
    '''Contrôle VueAdministration.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAdministration As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_COLLECTIVITE_26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_COLLECTIVITE_26 As Global.Virtualia.Net.Fenetre_PER_COLLECTIVITE
    
    '''<summary>
    '''Contrôle VueIdentification.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueIdentification As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_IDENTIFICATION.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_IDENTIFICATION As Global.Virtualia.Net.Fenetre_PER_IDENTIFICATION
    
    '''<summary>
    '''Contrôle VueAdressePro.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAdressePro As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_ADRESSEPRO_24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_ADRESSEPRO_24 As Global.Virtualia.Net.Fenetre_PER_ADRESSEPRO
    
    '''<summary>
    '''Contrôle VueAffectation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAffectation As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_AFFECTATION_17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_AFFECTATION_17 As Global.Virtualia.Net.Fenetre_PER_AFFECTATION
    
    '''<summary>
    '''Contrôle VueAffectation2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAffectation2 As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_AFFECTATION2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_AFFECTATION2 As Global.Virtualia.Net.Fenetre_PER_AFFECTATION2
    
    '''<summary>
    '''Contrôle VueAffectation3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAffectation3 As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_AFFECTATION3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_AFFECTATION3 As Global.Virtualia.Net.Fenetre_PER_AFFECTATION3
    
    '''<summary>
    '''Contrôle VueAffectation4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAffectation4 As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_AFFECTATION4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_AFFECTATION4 As Global.Virtualia.Net.Fenetre_PER_AFFECTATION4
    
    '''<summary>
    '''Contrôle VueAffectation5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAffectation5 As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_AFFECTATION5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_AFFECTATION5 As Global.Virtualia.Net.Fenetre_PER_AFFECTATION5
    
    '''<summary>
    '''Contrôle VueAffectation6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAffectation6 As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_AFFECTATION6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_AFFECTATION6 As Global.Virtualia.Net.Fenetre_PER_AFFECTATION6
    
    '''<summary>
    '''Contrôle VueLOLF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueLOLF As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_LOLF_92.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_LOLF_92 As Global.Virtualia.Net.Fenetre_PER_LOLF
    
    '''<summary>
    '''Contrôle VueEmploiBud.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueEmploiBud As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_EMPLOIBUD.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_EMPLOIBUD As Global.Virtualia.Net.Fenetre_PER_EMPLOIBUD
    
    '''<summary>
    '''Contrôle VueActiAnnexe.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueActiAnnexe As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_ACTIANNEXE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_ACTIANNEXE As Global.Virtualia.Net.Fenetre_PER_ACTIANNEXE
    
    '''<summary>
    '''Contrôle VueActiInterne.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueActiInterne As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_ACTI_INTERNE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_ACTI_INTERNE As Global.Virtualia.Net.Fenetre_PER_ACTI_INTERNE
    
    '''<summary>
    '''Contrôle VueStatut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueStatut As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_STATUT_12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_STATUT_12 As Global.Virtualia.Net.Fenetre_PER_STATUT
    
    '''<summary>
    '''Contrôle VuePosition.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VuePosition As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_POSITION_13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_POSITION_13 As Global.Virtualia.Net.Fenetre_PER_POSITION
    
    '''<summary>
    '''Contrôle VueGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueGrade As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_GRADE_14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_GRADE_14 As Global.Virtualia.Net.Fenetre_PER_GRADE
    
    '''<summary>
    '''Contrôle VueIntegration.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueIntegration As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_INTEGRATION.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_INTEGRATION As Global.Virtualia.Net.Fenetre_PER_INTEGRATION
    
    '''<summary>
    '''Contrôle VueBonification.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueBonification As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_BONIFICATION.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_BONIFICATION As Global.Virtualia.Net.Fenetre_PER_BONIFICATION
    
    '''<summary>
    '''Contrôle VueSanction.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueSanction As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_SANCTION.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_SANCTION As Global.Virtualia.Net.Fenetre_PER_SANCTION
    
    '''<summary>
    '''Contrôle VueStatutDetache.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueStatutDetache As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_STATUT_54.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_STATUT_54 As Global.Virtualia.Net.Fenetre_PER_STATUT_DETACHE
    
    '''<summary>
    '''Contrôle VuePositionDetache.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VuePositionDetache As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_POSITION_55.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_POSITION_55 As Global.Virtualia.Net.Fenetre_PER_POSITION_DETACHE
    
    '''<summary>
    '''Contrôle VueGradeDetache.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueGradeDetache As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_GRADE_56.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_GRADE_56 As Global.Virtualia.Net.Fenetre_PER_GRADE_DETACHE
    
    '''<summary>
    '''Contrôle VueServiceCivil.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueServiceCivil As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_SERVICECIVIL.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_SERVICECIVIL As Global.Virtualia.Net.Fenetre_PER_SERVICECIVIL
    
    '''<summary>
    '''Contrôle VueArmee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueArmee As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_SERVICEMILITAIRE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_SERVICEMILITAIRE As Global.Virtualia.Net.Fenetre_PER_SERVICEMILITAIRE
    
    '''<summary>
    '''Contrôle VueCumulRem.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueCumulRem As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_CUMULREM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_CUMULREM As Global.Virtualia.Net.Fenetre_PER_CUMULREM
    
    '''<summary>
    '''Contrôle VueMedical.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueMedical As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_VISITE_MEDICAL.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_VISITE_MEDICAL As Global.Virtualia.Net.Fenetre_PER_VISITE_MEDICAL
    
    '''<summary>
    '''Contrôle VueHandicap.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueHandicap As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_HANDICAP.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_HANDICAP As Global.Virtualia.Net.Fenetre_PER_HANDICAP
    
    '''<summary>
    '''Contrôle VueVaccination.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueVaccination As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_VACCINATION.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_VACCINATION As Global.Virtualia.Net.Fenetre_PER_VACCINATION
    
    '''<summary>
    '''Contrôle VuePrevention.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VuePrevention As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_PREVENTION.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_PREVENTION As Global.Virtualia.Net.Fenetre_PER_PREVENTION
    
    '''<summary>
    '''Contrôle VueAbsence.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAbsence As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_ABSENCE_15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_ABSENCE_15 As Global.Virtualia.Net.Fenetre_PER_ABSENCE
    
    '''<summary>
    '''Contrôle VueAgenda.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAgenda As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_AGENDA.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_AGENDA As Global.Virtualia.Net.Fenetre_PER_AGENDA
    
    '''<summary>
    '''Contrôle VueMandat.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueMandat As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_MANDAT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_MANDAT As Global.Virtualia.Net.Fenetre_PER_MANDAT
    
    '''<summary>
    '''Contrôle VueAvantage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAvantage As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_AVANTAGE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_AVANTAGE As Global.Virtualia.Net.Fenetre_PER_AVANTAGE
    
    '''<summary>
    '''Contrôle VueAffiliation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAffiliation As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_AFFILIATION.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_AFFILIATION As Global.Virtualia.Net.Fenetre_PER_AFFILIATION
    
    '''<summary>
    '''Contrôle VueMutuelle.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueMutuelle As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_MUTUELLE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_MUTUELLE As Global.Virtualia.Net.Fenetre_PER_MUTUELLE
    
    '''<summary>
    '''Contrôle VueMateriel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueMateriel As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_MATERIEL.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_MATERIEL As Global.Virtualia.Net.Fenetre_PER_MATERIEL
    
    '''<summary>
    '''Contrôle VueAnnotation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAnnotation As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_ANNOTATION.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_ANNOTATION As Global.Virtualia.Net.Fenetre_PER_ANNOTATION
    
    '''<summary>
    '''Contrôle VueDocument.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueDocument As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_DOCUMENT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_DOCUMENT As Global.Virtualia.Net.Fenetre_PER_DOCUMENT
    
    '''<summary>
    '''Contrôle CellMessage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellMessage As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle PopupMsg.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PopupMsg As Global.AjaxControlToolkit.ModalPopupExtender
    
    '''<summary>
    '''Contrôle PanelMsgPopup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelMsgPopup As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle MsgVirtualia.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MsgVirtualia As Global.Virtualia.Net.Controles_VMessage
    
    '''<summary>
    '''Contrôle HPopupMsg.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HPopupMsg As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle CellReference.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellReference As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle PopupReferentiel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PopupReferentiel As Global.AjaxControlToolkit.ModalPopupExtender
    
    '''<summary>
    '''Contrôle PanelRefPopup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelRefPopup As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle RefVirtualia.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RefVirtualia As Global.Virtualia.Net.VReferentiel
    
    '''<summary>
    '''Contrôle HPopupRef.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HPopupRef As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle CellRefGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellRefGrade As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle PopupRefGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PopupRefGrade As Global.AjaxControlToolkit.ModalPopupExtender
    
    '''<summary>
    '''Contrôle PanelRefGradePopup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelRefGradePopup As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle RefGradeGrilles.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RefGradeGrilles As Global.Virtualia.Net.VReferentielGrades
    
    '''<summary>
    '''Contrôle HPopupRefGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HPopupRefGrade As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle HSelIde.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HSelIde As Global.System.Web.UI.WebControls.HiddenField
End Class
