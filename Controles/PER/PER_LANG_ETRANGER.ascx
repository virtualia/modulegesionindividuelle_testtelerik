﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_LANG_ETRANGER" Codebehind="PER_LANG_ETRANGER.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" tagname="VCoupleEtiquetteExperte" tagprefix="Virtualia" %>
<%@ Register Src="~/Controles/Standards/VListeGrid.ascx" TagName="VListeGridd" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDate.ascx" TagName="VCoupleEtiDate" TagPrefix="Virtualia" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="450px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VListeGridd ID="ListeGrille" runat="server" CadreWidth="430px" SiColonneSelect="true"
                SiCaseAcocher="false" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
                BorderColor="#B0E0D7" Height="230px" Width="450px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                            Width="244px" HorizontalAlign="Right" Style="margin-top: 3px; margin-right: 3px">
                            <asp:TableRow>
                                <asp:TableCell VerticalAlign="Bottom">
                                    <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp"
                                        Width="160px" CellPadding="0" CellSpacing="0"
                                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                    BorderStyle="None" Style="margin-left: 10px; text-align: right;"
                                                    ToolTip="Nouvelle fiche"></asp:Button>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                    BorderStyle="None" Style="margin-left: 10px; text-align: right;"
                                                    ToolTip="Supprimer la fiche"></asp:Button>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                                <asp:TableCell VerticalAlign="Bottom">
                                    <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        CellPadding="0" CellSpacing="0" Visible="false">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                    BorderStyle="None" Style="margin-left: 6px; text-align: right;"></asp:Button>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="30px" CellPadding="0" Width="450px"
                            CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Langues étrangéres" Height="20px" Width="300px"
                                        BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                                        BorderWidth="2px" ForeColor="#D7FAF3"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                        Style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="Identite" runat="server" Height="40px" CellPadding="0" CellSpacing="0">                            
                             <asp:TableRow>
                                <asp:TableCell Width="450px" HorizontalAlign="Left">
                                 <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" 
                                    V_PointdeVue="1" V_Objet="9" V_Information="1" V_SiDonneeDico="true"
                                    DonWidth="200px" DonTabIndex="1"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>    
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
