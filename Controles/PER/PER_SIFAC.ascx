﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_SIFAC.ascx.vb" Inherits="Virtualia.Net.PER_SIFAC" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="600px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
   <asp:TableRow> 
     <asp:TableCell> 
       <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="680px" SiColonneSelect="true" SiCaseAcocher="false"/>
     </asp:TableCell>
   </asp:TableRow>
   <asp:TableRow>
     <asp:TableCell>
       <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
        BorderColor="#B0E0D7" Height="350px" Width="700px" HorizontalAlign="Center" style="margin-top: 3px;">
        <asp:TableRow>
          <asp:TableCell>
            <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                            Width="160px" CellPadding="0" CellSpacing="0"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Nouvelle fiche">
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Supprimer la fiche">
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="false">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="700px" 
                CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="Ventilation budgétaire - Interface SIFAC" Height="20px" Width="300px"
                            BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px;
                            font-style: oblique; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="700px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server"
                           V_PointdeVue="1" V_Objet="180" V_Information="0" V_SiDonneeDico="true" EtiWidth="120px" DonWidth="100px" DonTabIndex="1"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                         <Virtualia:VCocheSimple ID="Coche03" runat="server"
                           V_PointdeVue="1" V_Objet="180" V_Information="3" V_SiDonneeDico="true" V_Width="300px" V_Style="margin-left:2px"
                              V_Text="Traitement principal" V_SiAutoPostBack="true" /> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow Height="40px">
                      <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server"
                           V_PointdeVue="1" V_Objet="180" V_Information="2" V_SiDonneeDico="true" EtiWidth="250px" DonWidth="120px" DonTabIndex="2"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Table ID="CadreClefInterne" runat="server">
                            <asp:TableRow>
                                <asp:TableCell ID="CellNoOrdre">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server"
                                         V_PointdeVue="1" V_Objet="180" V_Information="4" V_SiDonneeDico="true" EtiWidth="250px" DonWidth="80px" DonTabIndex="4"
                                          EtiText="Numéro d'ordre" DonStyle="text-align:center" V_SiEnLectureSeule="true"/>
                                </asp:TableCell>
                                <asp:TableCell ID="CellCodePrime" runat="server" Visible="false">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab13" runat="server" V_PointdeVue="1" V_Objet="180" V_Information="13" V_SiDonneeDico="true"
                                        Etitext="Code de la prime" DonTabIndex="4" EtiWidth="150px" DonWidth="120px" DonStyle="text-align:center"/> 
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                  <asp:TableRow>
                    <asp:TableCell Height="12px" Columnspan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab07" runat="server"
                           V_PointdeVue="1" V_Objet="180" V_Information="7" V_SiDonneeDico="true" DonTabIndex="6" EtiWidth="250px" DonWidth="120px" DonStyle="text-align:center"/> 
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab08" runat="server"
                           V_PointdeVue="1" V_Objet="180" V_Information="8" V_SiDonneeDico="true" DonTabIndex="7" EtiVisible="false" DonWidth="300px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab09" runat="server"
                           V_PointdeVue="1" V_Objet="180" V_Information="9" V_SiDonneeDico="true" DonTabIndex="8" EtiWidth="250px" DonWidth="120px" DonStyle="text-align:center"/> 
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab10" runat="server"
                           V_PointdeVue="1" V_Objet="180" V_Information="10" V_SiDonneeDico="true" DonTabIndex="9" EtiVisible="false" DonWidth="300px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab11" runat="server"
                           V_PointdeVue="1" V_Objet="180" V_Information="11" V_SiDonneeDico="true" DonTabIndex="10" EtiWidth="250px" DonWidth="120px" DonStyle="text-align:center"/> 
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab12" runat="server"
                           V_PointdeVue="1" V_Objet="180" V_Information="12" V_SiDonneeDico="true" DonTabIndex="11" EtiVisible="false" DonWidth="300px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow Height="40px">
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server" V_PointdeVue="1" V_Objet="180" V_Information="14" V_SiDonneeDico="true"
                           EtiWidth="250px" DonWidth="120px" DonTabIndex="13" EtiText="Pourcentage" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px" Columnspan="2"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
          </asp:TableCell>
        </asp:TableRow>   
      </asp:Table>
     </asp:TableCell>
   </asp:TableRow>
 </asp:Table>
