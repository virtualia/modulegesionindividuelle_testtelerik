﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Fenetre_PER_POSITION
    
    '''<summary>
    '''Contrôle CadreControle.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreControle As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle ListeGrille.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeGrille As Global.Virtualia.Net.Controles_VListeGrid
    
    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreCommandes.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCommandes As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreCmdNewSupp.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmdNewSupp As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CommandeNew.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeNew As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CommandeSupp.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeSupp As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CadreCmdOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmdOK As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CommandeOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeOK As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CadreTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitre As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreDon.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreDon As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoD00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoD00 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle InfoD07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoD07 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle Dontab01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab01 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle Dontab05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab05 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle Dontab11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab11 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle InfoH02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH02 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH12 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle Dontab08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Dontab08 As Global.Virtualia.Net.Controles_VDuoEtiquetteCommande
    
    '''<summary>
    '''Contrôle EtiArrete.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiArrete As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoD03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoD03 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle InfoH04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH04 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle EtiDecret.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDecret As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreJO.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreJO As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle InfoD09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoD09 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle InfoD10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoD10 As Global.Virtualia.Net.VCoupleEtiDate
    
    '''<summary>
    '''Contrôle EtiGestion.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiGestion As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Coche18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Coche18 As Global.Virtualia.Net.Controles_VCocheSimple
    
    '''<summary>
    '''Contrôle Coche19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Coche19 As Global.Virtualia.Net.Controles_VCocheSimple
    
    '''<summary>
    '''Contrôle Coche20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Coche20 As Global.Virtualia.Net.Controles_VCocheSimple
    
    '''<summary>
    '''Contrôle EtiRetraite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiRetraite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoH17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH17 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle Coche13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Coche13 As Global.Virtualia.Net.Controles_VCocheSimple
    
    '''<summary>
    '''Contrôle Coche14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Coche14 As Global.Virtualia.Net.Controles_VCocheSimple
    
    '''<summary>
    '''Contrôle Coche15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Coche15 As Global.Virtualia.Net.Controles_VCocheSimple
    
    '''<summary>
    '''Contrôle Coche16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Coche16 As Global.Virtualia.Net.Controles_VCocheSimple
    
    '''<summary>
    '''Contrôle CadreExpert.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreExpert As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle ExprH01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ExprH01 As Global.Virtualia.Net.Controles_VCoupleEtiquetteExperte
    
    '''<summary>
    '''Contrôle ExprH02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ExprH02 As Global.Virtualia.Net.Controles_VCoupleEtiquetteExperte
    
    '''<summary>
    '''Contrôle ExprH03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ExprH03 As Global.Virtualia.Net.Controles_VCoupleEtiquetteExperte
    
    '''<summary>
    '''Contrôle ExprH04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ExprH04 As Global.Virtualia.Net.Controles_VCoupleEtiquetteExperte
    
    '''<summary>
    '''Contrôle ExprH05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ExprH05 As Global.Virtualia.Net.Controles_VCoupleEtiquetteExperte
    
    '''<summary>
    '''Contrôle ExprH06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ExprH06 As Global.Virtualia.Net.Controles_VCoupleEtiquetteExperte
End Class
