﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetre_PER_HEURESUP
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsNumObjet As Integer = VI.ObjetPer.ObaHeuresSup
    Private WsNomTable As String = "PER_HEURESUP"
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_HEURESUP

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then

                Dim ColHisto As New List(Of Integer)
                ColHisto.Add(0)
                ColHisto.Add(13)
                ColHisto.Add(1)
                ColHisto.Add(4)
                V_CacheColHisto = ColHisto

                V_Identifiant = value
                Call ActualiserListe()
            End If
        End Set
    End Property

    Private Sub ActualiserListe()
        CadreCmdOK.Visible = False
        Dim LstLibels As New List(Of String)
        LstLibels.Add("Aucune situation")
        LstLibels.Add("Une situation")
        LstLibels.Add("situations")
        ListeGrille.V_LibelCaption = LstLibels

        Dim LstColonnes As New List(Of String)
        LstColonnes.Add("date d'effet")
        LstColonnes.Add("montant total")
        LstColonnes.Add("nombre T1")
        LstColonnes.Add("nombre T2")
        LstColonnes.Add("Clef")
        ListeGrille.V_LibelColonne = LstColonnes

        If V_Identifiant > 0 Then
            Call InitialiserControles()
            If V_IndexFiche = -1 Then
                ListeGrille.V_Liste = Nothing
                Exit Sub
            End If
            ListeGrille.V_Liste = V_ListeFiches
        End If
    End Sub

    Protected Sub ListeGrille_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ListeGrille.ValeurChange
        If e.Valeur = "" Then
            Exit Sub
        End If
        V_Occurence = e.Valeur
    End Sub

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee.Item(NumInfo)
            End If
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeDate.DonText = ""
            Else
                VirDonneeDate.DonText = CacheDonnee(NumInfo).ToString
            End If
            If VirDonneeDate.SiDateFin = True Then
                VirDonneeDate.DateDebut = CacheDonnee(0).ToString
            End If
            IndiceI += 1
        Loop
    End Sub

    Protected Sub CommandeNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeNew.Click
        Call InitialiserControles()
        Call V_CommandeNewFiche()
        CadreCmdOK.Visible = True
    End Sub

    Protected Sub CommandeSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeSupp.Click
        Call V_CommandeSuppFiche()
    End Sub

    Public WriteOnly Property RetourDialogueSupp(ByVal Cmd As String) As String
        Set(ByVal value As String)
            V_RetourDialogueSupp(Cmd) = value
            Call ActualiserListe()
        End Set
    End Property

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Call CalculSpecifique_HeureSupp()
        Call V_MajFiche()
        Call ActualiserListe()
        CadreCmdOK.Visible = False
    End Sub

    Private Sub InfoD_ValeurChange(sender As Object, e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoD00.ValeurChange, InfoD14.ValeurChange,
            InfoD16.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VCoupleEtiDate).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee.Item(NumInfo) <> e.Valeur Then
                    CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH01.ValeurChange,
        InfoH02.ValeurChange, InfoH03.ValeurChange, InfoH04.ValeurChange, InfoH05.ValeurChange, InfoH06.ValeurChange,
        InfoH07.ValeurChange, InfoH08.ValeurChange, InfoH09.ValeurChange, InfoH10.ValeurChange, InfoH11.ValeurChange,
        InfoH12.ValeurChange, InfoH13.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee.Item(NumInfo) <> e.Valeur Then
                    CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
            Call CalculSpecifique_HeureSupp()
        End If
    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoV15.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleVerticalEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee.Item(NumInfo) <> e.Valeur Then
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_Objet(TypeListe.SiListeGrid) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        ListeGrille.Centrage_Colonne(0) = 1
        ListeGrille.Centrage_Colonne(1) = 2
        ListeGrille.Centrage_Colonne(2) = 1
        ListeGrille.Centrage_Colonne(3) = 1
        ListeGrille.Centrage_Colonne(4) = 1
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.V_SiEnLectureSeule = True Then
            CadreCommandes.Visible = False
            Call InitialiserControles()
        End If
        Call LireLaFiche()

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Cadre")
        CadreInfo.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelTranche1.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelTranche1.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelTranche1.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelTranche2.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelTranche2.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelTranche2.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelTranche3.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelTranche3.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelTranche3.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelTranche4.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelTranche4.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelTranche4.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiHSUP.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        EtiHSUP.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiHSUP.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiHSUPMontant.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        EtiHSUPMontant.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiHSUPMontant.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiHSUPNombre.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        EtiHSUPNombre.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiHSUPNombre.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiHSUPTaux.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        EtiHSUPTaux.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiHSUPTaux.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")

    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            VirControle.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            If Me.V_SiEnLectureSeule = False Then
                Select Case CInt(Strings.Right(VirControle.ID, 2))
                    Case 1, 4, 7, 10
                        VirControle.V_SiAutoPostBack = True
                End Select
            End If
            IndiceI += 1
        Loop
        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            VirDonneeDate.DonBackColor = Drawing.Color.White
            VirDonneeDate.DonText = ""
            VirDonneeDate.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
    End Sub

    Private Sub CalculSpecifique_HeureSupp()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim Zcalcul As Double
        Dim Ztotal As Double = 0

        If CacheDonnee.Item(1) <> "" And CacheDonnee.Item(2) <> "" Then
            Zcalcul = V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(1)) _
                * V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(2))
            CacheDonnee.Item(3) = CStr(Math.Round(Zcalcul, 2))
            Ztotal += Math.Round(Zcalcul, 2)
        End If
        If CacheDonnee.Item(4) <> "" And CacheDonnee.Item(5) <> "" Then
            Zcalcul = V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(4)) _
                * V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(5))
            CacheDonnee.Item(6) = CStr(Math.Round(Zcalcul, 2))
            Ztotal += Math.Round(Zcalcul, 2)
        End If
        If CacheDonnee.Item(7) <> "" And CacheDonnee.Item(8) <> "" Then
            Zcalcul = V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(7)) _
                * V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(8))
            CacheDonnee.Item(9) = CStr(Math.Round(Zcalcul, 2))
            Ztotal += Math.Round(Zcalcul, 2)
        End If
        If CacheDonnee.Item(10) <> "" And CacheDonnee.Item(11) <> "" Then
            Zcalcul = V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(10)) _
                * V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(11))
            CacheDonnee.Item(12) = CStr(Math.Round(Zcalcul, 2))
            Ztotal += Math.Round(Zcalcul, 2)
        End If
        CacheDonnee.Item(13) = CStr(Math.Round(Ztotal, 2))
        V_CacheMaj = CacheDonnee
    End Sub

End Class
