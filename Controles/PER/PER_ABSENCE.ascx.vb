﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetre_PER_ABSENCE
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsNumObjet As Integer = VI.ObjetPer.ObaAbsence
    Private WsNomTable As String = "PER_ABSENCE"
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_ABSENCE

    Private Const DATABS_DEBUTINCLUS = "Date de début incluse"
    Private Const DATABS_DEBUTAPRESMIDI = "Date de début après-midi"
    Private Const DATABS_FININCLUS = "Date de fin incluse"
    Private Const DATABS_FINMATIN = "Date de fin matin"

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property CadreExpertStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreExpert.Style.Remove(Strings.Trim(TableauW(0)))
                CadreExpert.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then

                Dim ColHisto As New List(Of Integer)
                ColHisto.Add(0)
                ColHisto.Add(1)
                ColHisto.Add(2)
                ColHisto.Add(3)
                ColHisto.Add(99)
                V_CacheColHisto = ColHisto

                V_Identifiant = value
                Call ActualiserListe()
            End If
        End Set
    End Property

    Private Sub ActualiserListe()
        CadreCmdOK.Visible = False
        Dim LstLibels As New List(Of String)
        LstLibels.Add("Aucune situation")
        LstLibels.Add("Une situation")
        LstLibels.Add("situations")
        ListeGrille.V_LibelCaption = LstLibels

        Dim LstColonnes As New List(Of String)
        LstColonnes.Add("date d'effet")
        LstColonnes.Add("nature")
        LstColonnes.Add("Nb jours calendaires")
        LstColonnes.Add("Nb jours ouvrés")
        LstColonnes.Add("date de fin")
        LstColonnes.Add("Clef")
        ListeGrille.V_LibelColonne = LstColonnes

        If V_Identifiant > 0 Then
            Call InitialiserControles()
            If V_IndexFiche = -1 Then
                ListeGrille.V_Liste = Nothing
                Exit Sub
            End If
            Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaPER.PER_ABSENCE) = V_WebFonction.ViRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_ABSENCE)(V_ListeFiches("", ""))
            Dim LstNatureAbs As List(Of String) = Nothing
            If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                LstNatureAbs = (From instance In LstFiches Order By instance.Nature Select instance.Nature).Distinct.ToList
            End If
            ListeGrille.V_Filtres(LstLibels) = LstNatureAbs
            ListeGrille.V_Liste = V_ListeFiches
        End If

    End Sub

    Protected Sub ListeGrille_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ListeGrille.ValeurChange
        If e.Valeur = "" Then
            Exit Sub
        End If
        V_Occurence = e.Valeur
    End Sub

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee.Item(NumInfo)
            End If
            VirControle.V_SiAutoPostBack = Not CadreCmdOK.Visible
            IndiceI += 1
        Loop

        Dim VirVertical As Controles_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirVertical.DonText = ""
            Else
                VirVertical.DonText = CacheDonnee.Item(NumInfo)
            End If
            VirVertical.V_SiAutoPostBack = Not CadreCmdOK.Visible
            IndiceI += 1
        Loop

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirDonneeTable.DonText = ""
            Else
                VirDonneeTable.DonText = CacheDonnee.Item(NumInfo)
            End If
            IndiceI += 1
        Loop

        Dim VirCoche As Controles_VCocheSimple
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Coche", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirCoche = CType(Ctl, Controles_VCocheSimple)
            VirCoche.V_SiAutoPostBack = Not CadreCmdOK.Visible
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirCoche.V_Check = False
            Else
                If CacheDonnee.Item(NumInfo).ToLower = "oui" Then
                    VirCoche.V_Check = True
                Else
                    VirCoche.V_Check = False
                End If
            End If
            IndiceI += 1
        Loop

        LabelCarence.Visible = False
        LabelCarence.Text = ""
        LabelDateSaisie.Text = ""
        CheckAbsenceDebut.Checked = True
        CheckAbsenceFin.Checked = True
        If CacheDonnee.Item(0) Is Nothing Or CacheDonnee.Item(9) Is Nothing Then
            Exit Sub
        End If
        If CacheDonnee.Item(0) = "" Or CacheDonnee.Item(9) = "" Then
            Exit Sub
        End If
        LabelDateSaisie.Text = CacheDonnee.Item(18)
        Select Case CacheDonnee.Item(14)
            Case DATABS_DEBUTINCLUS & " - " & DATABS_FININCLUS
                CheckAbsenceDebut.Checked = True
                CheckAbsenceDebut.Text = "Inclus"
                CheckAbsenceFin.Checked = True
                CheckAbsenceFin.Text = "Inclus"
            Case DATABS_DEBUTAPRESMIDI & " - " & DATABS_FININCLUS
                CheckAbsenceDebut.Checked = False
                CheckAbsenceDebut.Text = "Après-midi"
                CheckAbsenceFin.Checked = True
                CheckAbsenceFin.Text = "Inclus"
            Case DATABS_DEBUTAPRESMIDI & " - " & DATABS_FINMATIN
                CheckAbsenceDebut.Checked = False
                CheckAbsenceDebut.Text = "Après-midi"
                CheckAbsenceFin.Checked = False
                CheckAbsenceFin.Text = "Matin"
            Case DATABS_DEBUTINCLUS & " - " & DATABS_FINMATIN
                CheckAbsenceDebut.Checked = True
                CheckAbsenceDebut.Text = "Inclus"
                CheckAbsenceFin.Checked = False
                CheckAbsenceFin.Text = "Matin"
        End Select

        Dim SiNatureMaladie As Boolean = False
        If Strings.InStr(CacheDonnee.Item(1), "Maladie") > 0 Then
            SiNatureMaladie = True
        End If
        Select Case SiNatureMaladie
            Case True
                Select Case Strings.InStr(CacheDonnee.Item(1), "Longue")
                    Case Is > 0
                        SiNatureMaladie = False
                End Select
                Select Case Strings.InStr(CacheDonnee.Item(1), "Grave")
                    Case Is > 0
                        SiNatureMaladie = False
                End Select
        End Select
        If SiNatureMaladie = True Then
            If CDate(CacheDonnee.Item(0)) > CDate("01/01/2012") And CDate(CacheDonnee.Item(0)) < CDate("01/01/2014") Then
                LabelCarence.Visible = True
                If CacheDonnee.Item(16).ToLower = "oui" Then
                    LabelCarence.Text = "sans jour de carence"
                Else
                    LabelCarence.Text = "dont 1 jour de carence"
                End If
            End If
        Else
            Coche16.V_Check = False
        End If

    End Sub

    Protected Sub CommandeNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeNew.Click
        Call InitialiserControles()
        Call V_CommandeNewFiche()
        CadreCmdOK.Visible = True
    End Sub

    Protected Sub CommandeSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeSupp.Click
        Call V_CommandeSuppFiche()
    End Sub

    Public WriteOnly Property RetourDialogueSupp(ByVal Cmd As String) As String
        Set(ByVal value As String)
            V_RetourDialogueSupp(Cmd) = value
            Call ActualiserListe()
        End Set
    End Property

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Call V_MajFiche()
        Call ActualiserListe()
        CadreCmdOK.Visible = False
    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH00.ValeurChange, _
    InfoH02.ValeurChange, InfoH03.ValeurChange, InfoH04.ValeurChange, InfoH06.ValeurChange, InfoH07.ValeurChange, _
    InfoH08.ValeurChange, InfoH09.ValeurChange, InfoH12.ValeurChange, InfoH13.ValeurChange, InfoH15.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee.Item(NumInfo) <> e.Valeur Then
                    CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoV05.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleVerticalEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee.Item(NumInfo) <> e.Valeur Then
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub Dontab_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles Dontab01.AppelTable, _
    Dontab10.AppelTable, Dontab11.AppelTable

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VDuoEtiquetteCommande).ID, 2))
        Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(e.ControleAppelant, e.ObjetAppelant, e.PointdeVueInverse, e.NomdelaTable)
        Call V_AppelTable(Evenement)
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            Dim NumInfo As Integer
            Dim Ctl As Control
            Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
            Dim CacheDonnee As List(Of String) = V_CacheMaj

            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab" & Strings.Right(IDAppelant, 2), 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonText = value
            If CacheDonnee IsNot Nothing Then
                If CacheDonnee.Item(NumInfo) Is Nothing Then
                    VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                Else
                    If CacheDonnee.Item(NumInfo) <> value Then
                        VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                        CadreCmdOK.Visible = True
                    End If
                End If
                Call V_ValeurChange(NumInfo, value)
            End If
        End Set
    End Property

    Protected Sub Coche_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles Coche16.ValeurChange, _
    Coche17.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCocheSimple).ID, 2))
        If CacheDonnee IsNot Nothing Then
            CadreCmdOK.Visible = True
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_Objet(TypeListe.SiListeGrid) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        V_SiEnLectureSeule = True
        ListeGrille.Centrage_Colonne(0) = 1
        ListeGrille.Centrage_Colonne(1) = 0
        ListeGrille.Centrage_Colonne(2) = 1
        ListeGrille.Centrage_Colonne(3) = 1
        ListeGrille.Centrage_Colonne(4) = 1
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call LireLaFiche()
        Call TraiterExpertes()

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Cadre")
        CadreInfo.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelAccident.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Sous-Titre")
        LabelAccident.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelAccident.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelDateSaisie.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelDateSaisie.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelDateSaisie.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelDuree.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelDuree.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelDuree.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelJourDT.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelJourPT.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelMaladie.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Sous-Titre")
        LabelMaladie.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelMaladie.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelNbjCalendaires.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelNbjOuvrables.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelNbjOuvres.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelNbjTravailles.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelRemuneration.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        LabelRemuneration.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        LabelRemuneration.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        CheckAbsenceDebut.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        CheckAbsenceDebut.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        CheckAbsenceDebut.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        CheckAbsenceFin.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        CheckAbsenceFin.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        CheckAbsenceFin.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")

    End Sub

    Private Sub TraiterExpertes()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiquetteExperte
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreExpert, "ExprH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiquetteExperte)
            VirControle.V_Identifiant = V_Identifiant
            IndiceI += 1
        Loop
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            VirControle.V_SiEnLectureSeule = MyBase.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirVertical As Controles_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            VirVertical.V_SiEnLectureSeule = MyBase.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonBackColor = Drawing.Color.White
            VirDonneeTable.DonText = ""
            VirDonneeTable.V_SiEnLectureSeule = MyBase.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirExperte As Controles_VCoupleEtiquetteExperte
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "ExprH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirExperte = CType(Ctl, Controles_VCoupleEtiquetteExperte)
            VirExperte.DonText = ""
            IndiceI += 1
        Loop
        CadreCommandes.Visible = Not MyBase.V_SiEnLectureSeule
    End Sub

End Class
