﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_AFFILIATION" Codebehind="PER_AFFILIATION.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>

 <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
    BorderColor="#B0E0D7" Height="530px" Width="700px" HorizontalAlign="Center"
    style="position:relative">
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
            CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="false"
            BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
            Width="70px" HorizontalAlign="Right" style="margin-top: 3px; margin-right:3px">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Bottom">
                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                    BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                    </asp:Button>
                </asp:TableCell>
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" 
            CellSpacing="0" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="LabelTitre" runat="server" Text="Affiliations sociales" Height="20px" Width="330px"
                        BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 0px; margin-left: 4px; margin-bottom: 0px;
                        font-style: oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="TableauDon" runat="server" Height="40px" CellPadding="0" Width="620px" 
            CellSpacing="0" HorizontalAlign="Left">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                    <asp:Label ID="EtiRetraite" runat="server" Text="Affiliation - Caisse de retraite"
                        Height="20px" Width="550px" BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Outset"
                        BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 10px; margin-left: 3px; margin-bottom: 1px; text-indent: 5px; text-align: left">
                    </asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server"
                        V_PointdeVue="1" V_Objet="27" V_Information="2" V_SiDonneeDico="true"
                        DonWidth ="450px" DonTabIndex="1" EtiVisible="false"/>
                </asp:TableCell>
                <asp:TableCell>    
                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server"
                        V_PointdeVue="1" V_Objet="27" V_Information="1" V_SiDonneeDico="true" EtiWidth="30px"
                        DonWidth="110px" DonTabIndex="2"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
            </asp:TableRow> 
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                    <asp:Label ID="EtiPrevoyance" runat="server" Text="Affiliation - Caisse de prévoyance"
                        Height="20px" Width="550px" BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Outset"
                        BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 5px; margin-left: 3px; margin-bottom: 1px; text-indent: 5px; text-align: left">
                    </asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab04" runat="server"
                        V_PointdeVue="1" V_Objet="27" V_Information="4" V_SiDonneeDico="true"
                        DonWidth ="450px" DonTabIndex="3" EtiVisible="false"/>
                </asp:TableCell>
                <asp:TableCell>    
                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server"
                        V_PointdeVue="1" V_Objet="27" V_Information="3" V_SiDonneeDico="true" EtiWidth="30px"
                        DonWidth="110px" DonTabIndex="4"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
            </asp:TableRow> 
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                    <asp:Label ID="EtiRetraiteCompl" runat="server" Text="Affiliation - Retraite complémentaire"
                        Height="20px" Width="550px" BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Outset"
                        BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 5px; margin-left: 3px; margin-bottom: 1px; text-indent: 5px; text-align: left">
                    </asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab06" runat="server"
                        V_PointdeVue="1" V_Objet="27" V_Information="6" V_SiDonneeDico="true"
                        DonWidth ="450px" DonTabIndex="5" EtiVisible="false"/>
                </asp:TableCell>
                <asp:TableCell>    
                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server"
                        V_PointdeVue="1" V_Objet="27" V_Information="5" V_SiDonneeDico="true" EtiWidth="30px"
                        DonWidth="110px" DonTabIndex="6"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
            </asp:TableRow> 
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                    <asp:Label ID="EtiAssuranceMaladie" runat="server" Text="Affiliation - Caisse d'assurance maladie"
                        Height="20px" Width="550px" BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Outset"
                        BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 5px; margin-left: 3px; margin-bottom: 1px; text-indent: 5px; text-align: left">
                    </asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab07" runat="server"
                        V_PointdeVue="1" V_Objet="27" V_Information="7" V_SiDonneeDico="true"
                        DonWidth ="450px" DonTabIndex="7" EtiVisible="false"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>    
                    <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server"
                        V_PointdeVue="1" V_Objet="27" V_Information="10" V_SiDonneeDico="true" EtiWidth="370px"
                        DonWidth="110px" DonTabIndex="8"
                        Etistyle="margin-left: 3px; text-indent: 5px; text-align: center"/>
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left">  
                    <asp:Label ID="LabelEuro" runat="server" Text="€" Height="20px" Width="90px"
                        BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                        ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                        font-style: oblique; text-indent: 0px; text-align: left;">
                    </asp:Label> 
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
            </asp:TableRow> 
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                    <asp:Label ID="EtiChomage" runat="server" Text="Affiliation - Caisse d'assurance chômage"
                        Height="20px" Width="550px" BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Outset"
                        BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 5px; margin-left: 3px; margin-bottom: 1px; text-indent: 5px; text-align: left">
                    </asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab08" runat="server"
                        V_PointdeVue="1" V_Objet="27" V_Information="8" V_SiDonneeDico="true"
                        DonWidth ="450px" DonTabIndex="9" EtiVisible="false"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left"  ColumnSpan="2">
                    <asp:Label ID="EtiAccident" runat="server" Text="Affiliation - Caisse accident du travail"
                        Height="20px" Width="550px" BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Outset"
                        BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 5px; margin-left: 3px; margin-bottom: 1px; text-indent: 5px; text-align: left">
                    </asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab09" runat="server"
                        V_PointdeVue="1" V_Objet="27" V_Information="9" V_SiDonneeDico="true"
                        DonWidth ="450px" DonTabIndex="10" EtiVisible="false"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="12px" Columnspan="2"></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
 </asp:Table>