﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_HANDICAP" Codebehind="PER_HANDICAP.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" tagname="VCoupleEtiquetteExperte" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>

 
  <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
    BorderColor="#B0E0D7" Height="200px" Width="800px" HorizontalAlign="Center">
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
            CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="false"
            BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
            Width="70px" HorizontalAlign="Right" style="margin-top: 3px; margin-right:3px">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Bottom">
                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                    BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                    </asp:Button>
                </asp:TableCell>
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" 
            CellSpacing="0" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="Etiquette" runat="server" Text="Handicap" Height="20px" Width="300px"
                        BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#D7FAF3"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px;
                        font-style: oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="Adresse" runat="server" Height="22px" Width="495px" CellPadding="0" CellSpacing="0">
          <asp:TableRow>
              <asp:TableCell HorizontalAlign="left">  
                    <Virtualia:VCocheSimple ID="Coche01" runat="server"
                        V_PointdeVue="1" V_Objet="39" V_Information="1" V_SiDonneeDico="true"
                        V_Width="300px" V_Style="margin-left:0px"/>  
                 </asp:TableCell>
          </asp:TableRow>
            <asp:TableRow>
             <asp:TableCell>  
                 <Virtualia:VDuoEtiquetteCommande ID="Dontab04" runat="server" 
                    V_PointdeVue="1" V_Objet="39" V_Information="4" V_SiDonneeDico="true"
                     DonWidth="200px" DonTabIndex="10"/>        
             </asp:TableCell>
            </asp:TableRow>
          </asp:Table>    
      </asp:TableCell>
    </asp:TableRow>       
 </asp:Table>

