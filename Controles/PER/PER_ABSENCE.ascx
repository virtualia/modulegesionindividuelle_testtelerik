﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_ABSENCE" Codebehind="PER_ABSENCE.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VFiltreListe.ascx" tagname="VFiltreListe" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" tagname="VCoupleEtiquetteExperte" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>

 <asp:Table ID="CadreControle" runat="server" Height="30px" Width="720px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow> 
         <asp:TableCell HorizontalAlign="Left"> 
            <Virtualia:VFiltreListe ID="ListeGrille" runat="server" CadreWidth="700px" SiColonneSelect="true" SiCaseAcocher="false"/>
         </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
                BorderColor="#B0E0D7" Height="620px" Width="630px" HorizontalAlign="Center" style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                            Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                            <asp:TableRow>
                                <asp:TableCell VerticalAlign="Bottom">
                                    <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                                                Width="160px" CellPadding="0" CellSpacing="0"
                                                BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                                     BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                     BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                                     Tooltip="Nouvelle fiche" >
                                                 </asp:Button>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                 <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                                     BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                     BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                                     Tooltip="Supprimer la fiche" >
                                                 </asp:Button>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>    
                                </asp:TableCell>
                                <asp:TableCell VerticalAlign="Bottom">
                                    <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                                                BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                CellPadding="0" CellSpacing="0" Visible="false" >
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                                     BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                                     Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                     BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                                 </asp:Button>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>    
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="600px" 
                            CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Text="Absence" Height="20px" Width="300px"
                                        BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                                        BorderWidth="2px" ForeColor="#D7FAF3"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px;
                                        font-style: oblique; text-indent: 5px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>      
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreAbsence" runat="server" CellPadding="0" CellSpacing="0" Width="600px">
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" >
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server"
                                       V_PointdeVue="1" V_Objet="15" V_Information="1" V_SiDonneeDico="true"
                                       EtiWidth="120px" DonWidth="440px" Etitext="Absence" DonTabIndex="1"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDates" runat="server" CellPadding="0" CellSpacing="0" Width="510px">
                            <asp:TableRow>
                                <asp:TableCell Height="8px" ColumnSpan="3"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                  <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server"
                                       V_PointdeVue="1" V_Objet="15" V_Information="0" V_SiDonneeDico="true"
                                       EtiWidth="80px" DonWidth="80px" DonTabIndex="2"
                                       Etistyle="margin-left: 44px;"/>
                                </asp:TableCell>
                                <asp:TableCell>
                                  <asp:CheckBox ID="CheckAbsenceDebut" runat="server" Text="Inclus" Visible="true" AutoPostBack="true"
                                      BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                                      ForeColor="#142425" Height="20px" Width="100px" Checked="false"
                                      Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                      style="margin-top: 0px; margin-left: 10px; font-style: oblique;
                                      text-indent: 5px; text-align: left"/>
                                </asp:TableCell>
                                <asp:TableCell>
                                  <asp:Label ID="LabelDuree" runat="server" Height="20px" Width="184px" Text="Date de saisie"
                                        BackColor="#CAEBE4" BorderColor="#D7FAF3"  BorderStyle="Ridge"
                                        BorderWidth="2px" ForeColor="#0E5F5C" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 50px; text-indent: 5px; text-align: Center">
                                  </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server"
                                       V_PointdeVue="1" V_Objet="15" V_Information="9" V_SiDonneeDico="true"
                                       EtiWidth="80px" DonWidth="80px" DonTabIndex="3"
                                       Etistyle="margin-left: 44px;"/>
                                </asp:TableCell>
                                <asp:TableCell>
                                  <asp:CheckBox ID="CheckAbsenceFin" runat="server" Text="Inclus" Visible="true" AutoPostBack="true"
                                      BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px"
                                      ForeColor="#142425" Height="20px" Width="100px" Checked="false"
                                      Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small"
                                      style="margin-top: 0px; margin-left: 10px; font-style: oblique;
                                      text-indent: 5px; text-align: left"/>
                                </asp:TableCell>
                                <asp:TableCell>
                                  <asp:Label ID="LabelDateSaisie" runat="server" Height="20px" Width="184px" Text=""
                                        BackColor="#CAEBE4" BorderColor="#D7FAF3"  BorderStyle="Ridge"
                                        BorderWidth="2px" ForeColor="#0E5F5C" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 50px; text-indent: 5px; text-align: Center">
                                  </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="3"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                         <asp:Table ID="CadreDurees" runat="server" CellPadding="0" CellSpacing="0" Width="380px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server"
                                       V_PointdeVue="1" V_Objet="15" V_Information="2" V_SiDonneeDico="true"
                                       EtiWidth="150px" DonWidth="50px" DonTabIndex="4"
                                       Etistyle="margin-left: 130px;"/>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">  
                                    <asp:Label ID="LabelNbjCalendaires" runat="server" Text="jours" Height="20px" Width="35px"
                                        BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                        ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: oblique; text-indent: 2px; text-align: left;">
                                    </asp:Label> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server"
                                       V_PointdeVue="1" V_Objet="15" V_Information="3" V_SiDonneeDico="true"
                                       EtiWidth="150px" DonWidth="50px" DonTabIndex="5"
                                       Etistyle="margin-left: 130px;"/>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">  
                                    <asp:Label ID="LabelNbjOuvres" runat="server" Text="jours" Height="20px" Width="35px"
                                        BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                        ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: oblique; text-indent: 2px; text-align: left;">
                                    </asp:Label> 
                                </asp:TableCell>    
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server"
                                       V_PointdeVue="1" V_Objet="15" V_Information="4" V_SiDonneeDico="true"
                                       EtiWidth="150px" DonWidth="50px" DonTabIndex="6"
                                       Etistyle="margin-left: 130px;"/>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">  
                                    <asp:Label ID="LabelNbjOuvrables" runat="server" Text="jours" Height="20px" Width="35px"
                                        BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                        ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: oblique; text-indent: 2px; text-align: left;">
                                    </asp:Label> 
                                </asp:TableCell>    
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server"
                                       V_PointdeVue="1" V_Objet="15" V_Information="8" V_SiDonneeDico="True"
                                       EtiWidth="150px" DonWidth="50px" DonTabIndex="7" EtiText="Jours travaillés"
                                       Etistyle="margin-left: 130px;"/>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">  
                                    <asp:Label ID="LabelNbjTravailles" runat="server" Text="jours" Height="20px" Width="35px"
                                        BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                        ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                        font-style: oblique; text-indent: 2px; text-align: left;">
                                    </asp:Label> 
                                </asp:TableCell>    
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="8px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreRemuMaladie" runat="server" Height="40px" CellPadding="0" CellSpacing="0" Width="450px">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Left">
                                        <asp:Label ID="LabelMaladie" runat="server" Text="Congés de maladie ordinaire"
                                            Height="20px" Width="200px"
                                            BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Groove"
                                            BorderWidth="2px" ForeColor="#D7FAF3"
                                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                            style="margin-top: 5px; margin-left: 5px; margin-bottom: 0px;
                                            font-style: oblique; text-indent: 5px; text-align: center;">
                                        </asp:Label>          
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Left">
                                        <asp:Label ID="LabelRemuneration" runat="server" Text="Maintien de la rémunération"
                                            Height="20px" Width="240px" 
                                            BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Groove"
                                            BorderWidth="2px" ForeColor="#D7FAF3"
                                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                            style="margin-top: 5px; margin-left: 5px; margin-bottom: 0px;
                                            font-style: oblique; text-indent: 5px; text-align: center;">
                                        </asp:Label>          
                                    </asp:TableCell>     
                                </asp:TableRow>
                        </asp:Table>
                        <asp:Table ID="CadreMaintienRemuMaladie" runat="server" Height="40px" CellPadding="0" CellSpacing="0" Width="450px">
                                <asp:TableRow>
                                  <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server"
                                       V_PointdeVue="1" V_Objet="15" V_Information="6" V_SiDonneeDico="True"
                                       EtiWidth="182px" DonWidth="50px" DonTabIndex="8"
                                       Etistyle="margin-left: 213px;"/>
                                  </asp:TableCell>
                                  <asp:TableCell HorizontalAlign="Left">  
                                    <asp:Label ID="LabelJourPT" runat="server" Text="jours" Height="20px" Width="50px"
                                        BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                        ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 2px; margin-bottom: 0px;
                                        font-style: oblique; text-indent: 2px; text-align: left;">
                                    </asp:Label> 
                                  </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                  <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server"
                                       V_PointdeVue="1" V_Objet="15" V_Information="7" V_SiDonneeDico="True"
                                       EtiWidth="182px" DonWidth="50px" DonTabIndex="9"
                                       Etistyle="margin-left: 213px;"/>
                                  </asp:TableCell>
                                  <asp:TableCell HorizontalAlign="Left">  
                                    <asp:Label ID="LabelJourDT" runat="server" Text="jours" Height="20px" Width="50px"
                                        BackColor="Transparent" BorderStyle="none"  BorderWidth="0px"
                                        ForeColor="#2FA49B" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 2px; margin-bottom: 0px;
                                        font-style: oblique; text-indent: 2px; text-align: left;">
                                    </asp:Label> 
                                  </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Right">  
                                        <asp:Label ID="LabelCarence" runat="server" Text="dont 1 jour de carence" Height="20px" Width="268px"
                                            BackColor="Transparent" BorderStyle="none"  BorderWidth="0px" Visible="false"
                                            ForeColor="#124545" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                            style="margin-top: 0px; margin-right: 20px; margin-bottom: 0px;
                                            font-style: oblique; text-indent: 2px; text-align: left;">
                                        </asp:Label> 
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                   <asp:TableCell ColumnSpan="2" HorizontalAlign="Right">
                                         <Virtualia:VCocheSimple ID="Coche16" runat="server"
                                           V_PointdeVue="1" V_Objet="15" V_Information="16" V_SiDonneeDico="true"
                                           V_Width="268px" V_Style="margin-right:20px"/> 
                                    </asp:TableCell>
                                </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitreAccident" runat="server" Height="40px" CellPadding="0" CellSpacing="0" Width="500px">
                           <asp:TableRow>
                               <asp:TableCell HorizontalAlign="Left">
                                   <asp:Label ID="LabelAccident" runat="server" Text="Accident du travail"
                                       Height="20px" Width="200px"
                                       BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Groove"
                                       BorderWidth="2px" ForeColor="#D7FAF3"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 5px; margin-left: 5px; margin-bottom: 0px;
                                       font-style: oblique; text-indent: 5px; text-align: center;">
                                  </asp:Label>          
                               </asp:TableCell>
                           </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDateAccident" runat="server" Height="40px" CellPadding="0" CellSpacing="0" Width="600px">
                           <asp:TableRow>
                               <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH15" runat="server"
                                       V_PointdeVue="1" V_Objet="15" V_Information="15" V_SiDonneeDico="True"
                                       EtiWidth="140px" DonWidth="80px" DonTabIndex="10"/>
                               </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab11" runat="server"
                                       V_PointdeVue="1" V_Objet="15" V_Information="11" V_SiDonneeDico="true"
                                       EtiWidth="140px" DonWidth="400px" DonTabIndex="11"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab10" runat="server"
                                       V_PointdeVue="1" V_Objet="15" V_Information="10" V_SiDonneeDico="true"
                                       EtiWidth="140px" DonWidth="400px" DonTabIndex="12"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreAccident" runat="server" Height="40px" CellPadding="0" CellSpacing="0" Width="500px">
                           <asp:TableRow>
                               <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server"
                                       V_PointdeVue="1" V_Objet="15" V_Information="12" V_SiDonneeDico="True"
                                       EtiWidth="220px" DonWidth="80px" DonTabIndex="13"/>
                               </asp:TableCell>
                               <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server"
                                       V_PointdeVue="1" V_Objet="15" V_Information="13" V_SiDonneeDico="True"
                                       EtiWidth="120px" DonWidth="50px" DonTabIndex="14"/>
                               </asp:TableCell>
                           </asp:TableRow>
                           <asp:TableRow>
                                <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">
                                     <Virtualia:VCocheSimple ID="Coche17" runat="server"
                                       V_PointdeVue="1" V_Objet="15" V_Information="17" V_SiDonneeDico="true"
                                       V_Width="262px" V_Style="margin-left:4px"/> 
                                </asp:TableCell>
                             </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="TableauCommentaire" runat="server" CellPadding="0" CellSpacing="0" Width="600px">
                            <asp:TableRow>
                                <asp:TableCell Height="10px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV05" runat="server"
                                       V_PointdeVue="1" V_Objet="15" V_Information="5" V_SiDonneeDico="true"
                                       EtiWidth="586px" DonWidth="584px" DonHeight="20px" DonTabIndex="25"
                                       EtiStyle="text-align:center; margin-left: 5px;" Donstyle="margin-left: 5px;"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Visible="false">
                        <asp:Panel ID="CadreExpert" runat="server" BorderStyle="None" BorderWidth="1px"
                            BorderColor="#B0E0D7" Height="100px" Width="600px" HorizontalAlign="Left"
                            style="margin-top: 5px;  vertical-align: middle">
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH01" runat="server"
                               V_PointdeVue="1" V_Objet="15" V_InfoExperte="687" V_SiDonneeDico="true"
                               EtiHeight="20px" EtiWidth="250px" DonWidth="50px" DonHeight="20px"
                               EtiStyle="margin-left: 40px; margin-top: 5px" DonStyle="margin-top: 9px; text-align: center"/>
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH02" runat="server"
                               V_PointdeVue="1" V_Objet="15" V_InfoExperte="711" V_SiDonneeDico="true"
                               EtiHeight="20px" EtiWidth="250px" DonWidth="50px" DonHeight="20px"
                               EtiStyle="margin-left: 40px;" DonStyle="text-align: center;"/>
                            <Virtualia:VCoupleEtiquetteExperte ID="ExprH03" runat="server"
                               V_PointdeVue="1" V_Objet="15" V_InfoExperte="938" V_SiDonneeDico="true"
                               EtiHeight="20px" EtiWidth="250px" DonWidth="80px" DonHeight="20px"
                               EtiStyle="margin-left: 40px;" DonStyle="text-align: center;"/>
                        </asp:Panel>       
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
              </asp:Table>
        </asp:TableCell>
     </asp:TableRow>
 </asp:Table>
