﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetre_PER_DOMICILE
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsNumObjet As Integer = VI.ObjetPer.ObaAdresse
    Private WsNomTable As String = "PER_DOMICILE"
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_DOMICILE
    Private WsDossierPer As Virtualia.Net.Individuel.DossierIndividu


    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property Identifiant As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                Call InitialiserControles()
                V_Identifiant = value
            End If
        End Set
    End Property

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee.Item(NumInfo)
            End If
            VirControle.V_SiAutoPostBack = Not CadreCmdOK.Visible
            IndiceI += 1
        Loop

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirDonneeTable.DonText = ""
            Else
                VirDonneeTable.DonText = CacheDonnee.Item(NumInfo)
            End If
            IndiceI += 1
        Loop

        Dim VirCoche As Controles_VCocheSimple
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Coche", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirCoche = CType(Ctl, Controles_VCocheSimple)
            VirCoche.V_SiAutoPostBack = Not CadreCmdOK.Visible
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirCoche.V_Check = False
            Else
                If CacheDonnee.Item(NumInfo) = "Oui" Then
                    VirCoche.V_Check = True
                Else
                    VirCoche.V_Check = False
                End If
            End If
            IndiceI += 1
        Loop
    End Sub

    Private Sub TraiterExpertes()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiquetteExperte
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreExpert, "ExprH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiquetteExperte)
            VirControle.V_Identifiant = V_Identifiant
            IndiceI += 1
        Loop
    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH01.ValeurChange, _
    InfoH02.ValeurChange, InfoH03.ValeurChange, InfoH04.ValeurChange, InfoH05.ValeurChange, InfoH06.ValeurChange, _
    InfoH08.ValeurChange, InfoH09.ValeurChange, InfoH10.ValeurChange, InfoH11.ValeurChange, InfoH13.ValeurChange, _
    InfoH19.ValeurChange, InfoH18.ValeurChange, InfoH20.ValeurChange, InfoH22.ValeurChange, InfoH21.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee.Item(NumInfo) <> e.Valeur Then
                    CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub Dontab_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles Dontab07.AppelTable, _
    Dontab12.AppelTable

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VDuoEtiquetteCommande).ID, 2))
        Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(e.ControleAppelant, e.ObjetAppelant, e.PointdeVueInverse, e.NomdelaTable)
        Call V_AppelTable(Evenement)
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            Dim NumInfo As Integer
            Dim Ctl As Control
            Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
            Dim CacheDonnee As List(Of String) = V_CacheMaj

            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab" & Strings.Right(IDAppelant, 2), 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonText = value
            If CacheDonnee IsNot Nothing Then
                If CacheDonnee.Item(NumInfo) Is Nothing Then
                    VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                Else
                    If CacheDonnee.Item(NumInfo) <> value Then
                        VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                        CadreCmdOK.Visible = True
                    End If
                End If
                Call V_ValeurChange(NumInfo, value)
            End If
        End Set
    End Property

    Protected Sub Coche_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles Coche14.ValeurChange, _
    Coche15.ValeurChange, Coche16.ValeurChange, Coche17.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCocheSimple).ID, 2))
        If CacheDonnee IsNot Nothing Then
            CadreCmdOK.Visible = True
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_Objet(0) = WsNumObjet
        V_NomTableSgbd = WsNomTable
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.V_SiEnLectureSeule = True Then
            CadreCmdOK.Visible = False
            Call InitialiserControles()
        End If
        Call LireLaFiche()
        Call TraiterExpertes()
        If V_IndexFiche = -1 Then
            Call V_CommandeNewFiche()
        End If

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Cadre")
        CadreInfo.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiReseaux.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Titre")
        EtiReseaux.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Police_Claire")
        EtiReseaux.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")

    End Sub

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click

        Dim TabOK As New List(Of String)
        V_Contexte.DossierPER = V_ListeDossiers.ItemDossier(V_Identifiant)
        WsDossierPer = V_Contexte.DossierPER

        Call V_MajFiche()
        CadreCmdOK.Visible = False
        Dim TitreMsg As String = "Contrôle préalable à la mise à jour du dossier de " & WsDossierPer.FicheEtatCivil.Nom & Strings.Space(1) & WsDossierPer.FicheEtatCivil.Prenom
        TabOK.Add("Le dossier de " & WsDossierPer.FicheEtatCivil.Nom & Strings.Space(1) & WsDossierPer.FicheEtatCivil.Prenom & " a été mis à jour.")
        Call V_MessageDialog(V_WebFonction.Evt_MessageInformatif(WsNumObjet, TitreMsg, TabOK, V_Identifiant))
    End Sub

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property CadreExpertStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreExpert.Style.Remove(Strings.Trim(TableauW(0)))
                CadreExpert.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            VirControle.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonBackColor = Drawing.Color.White
            VirDonneeTable.DonText = ""
            VirDonneeTable.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
    End Sub


    Private ReadOnly Property V_Contexte As Virtualia.Net.Individuel.LocalNavigation
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
        End Get
    End Property

    Private ReadOnly Property V_ListeDossiers As Virtualia.Net.Individuel.EnsembleDossiers
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).EnsemblePER
        End Get
    End Property

End Class

