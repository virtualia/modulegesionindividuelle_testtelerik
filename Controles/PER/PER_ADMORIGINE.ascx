﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_ADMORIGINE" Codebehind="PER_ADMORIGINE.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" tagname="VCoupleEtiquetteExperte" tagprefix="Virtualia" %>
 
 <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
    BorderColor="#B0E0D7" Height="720px" Width="700px" HorizontalAlign="Center"
    style="position:relative">
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
            CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="false"
            BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
            Width="70px" HorizontalAlign="Right" style="margin-top: 3px; margin-right:3px">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Bottom">
                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                        BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                    </asp:Button>
                </asp:TableCell>
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreDonnee" runat="server" Width="700px" CellPadding="0" 
                CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Label ID="EtiIdentite" runat="server" Text="Services antérieurs" Height="20px" Width="300px"
                            BackColor="#2FA49B" BorderColor="#B0E0D7"  BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px;
                            font-style: oblique; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left"> 
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" 
                            V_PointdeVue="1" V_Objet="25" V_Information="1" V_SiDonneeDico="true"
                            EtiWidth="200px" DonWidth="450px" DonTabIndex="1"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left"> 
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab23" runat="server" 
                            V_PointdeVue="1" V_Objet="25" V_Information="23" V_SiDonneeDico="true"
                            EtiWidth="200px" DonWidth="450px" DonTabIndex="2"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2">
                        <asp:Table ID="EntreesFP" runat="server" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell> 
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV02" runat="server"
                                       V_PointdeVue="1" V_Objet="25" V_Information="2" V_SiDonneeDico="true"
                                       EtiWidth="150px" EtiHeight="35px" DonWidth="120px" DonTabIndex="3"
                                       DonTextMode="false" DonHeight="16px"
                                       EtiStyle="text-align:center;margin-top:10px" DonStyle="text-align:center;margin-left:15px"/>
                                </asp:TableCell>
                                <asp:TableCell> 
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV03" runat="server"
                                       V_PointdeVue="1" V_Objet="25" V_Information="3" V_SiDonneeDico="true"
                                       EtiWidth="150px" EtiHeight="35px" DonWidth="120px" DonTabIndex="4"
                                       DonTextMode="false" DonHeight="16px"
                                       EtiStyle="text-align:center;margin-top:10px" DonStyle="text-align:center;margin-left:15px"/>
                                </asp:TableCell>
                                <asp:TableCell> 
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV04" runat="server"
                                       V_PointdeVue="1" V_Objet="25" V_Information="4" V_SiDonneeDico="true"
                                       EtiWidth="150px" EtiHeight="35px" DonWidth="120px" DonTabIndex="5"
                                       DonTextMode="false" DonHeight="16px"
                                       EtiStyle="text-align:center;margin-top:10px" DonStyle="text-align:center;margin-left:15px"/>
                                </asp:TableCell>
                                <asp:TableCell> 
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV05" runat="server"
                                       V_PointdeVue="1" V_Objet="25" V_Information="5" V_SiDonneeDico="true"
                                       EtiWidth="150px" EtiHeight="35px" DonWidth="120px" DonTabIndex="6"
                                       DonTextMode="false" DonHeight="16px"
                                       EtiStyle="text-align:center;margin-top:10px" DonStyle="text-align:center;margin-left:15px"/>
                                </asp:TableCell> 
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Label ID="EtiServices" runat="server" Text="Services validés" Height="20px" Width="300px"
                            BackColor="#2FA49B" BorderColor="#B0E0D7"  BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 15px; margin-left: 4px; margin-bottom:5px; font-style: oblique; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left"> 
                        <asp:Table ID="CadreSerEffectifs" runat="server">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server"
                                       V_PointdeVue="1" V_Objet="25" V_Information="6" V_SiDonneeDico="true"
                                       EtiWidth="300px" DonWidth="40px" DonTabIndex="7" 
                                       EtiText="Totalité des services antérieurs validés" DonTooltip="Nombre d'années"/>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left"> 
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="25" V_Information="7" V_SiDonneeDico="true"
                                       DonWidth="40px" DonTabIndex="8" DonTooltip="Nombre de mois"/>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left"> 
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="25" V_Information="8" V_SiDonneeDico="true"
                                       DonWidth="40px" DonTabIndex="9" DonTooltip="Nombre de jours"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="3" HorizontalAlign="Left"> 
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab09" runat="server" 
                                       V_PointdeVue="1" V_Objet="25" V_Information="9" V_SiDonneeDico="true"
                                       EtiWidth="300px" DonWidth="150px" DonTabIndex="10"/> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px" ColumnSpan="3"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server"
                                       V_PointdeVue="1" V_Objet="25" V_Information="14" V_SiDonneeDico="true"
                                       EtiWidth="300px" DonWidth="40px" DonTabIndex="10" 
                                       EtiText="Partie validée au titre des services publics" DonTooltip="Nombre d'années"/>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left"> 
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH15" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="25" V_Information="15" V_SiDonneeDico="true"
                                       DonWidth="40px" DonTabIndex="11" DonTooltip="Nombre de mois"/>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left"> 
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH16" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="25" V_Information="16" V_SiDonneeDico="true"
                                       DonWidth="40px" DonTabIndex="12" DonTooltip="Nombre de jours"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH17" runat="server"
                                       V_PointdeVue="1" V_Objet="25" V_Information="17" V_SiDonneeDico="true"
                                       EtiWidth="300px" DonWidth="40px" DonTabIndex="13" 
                                       EtiText="Partie validée au titre des services privés" DonTooltip="Nombre d'années"/>
                                </asp:TableCell>
                                <asp:TableCell RowSpan="1" HorizontalAlign="Left"> 
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH18" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="25" V_Information="18" V_SiDonneeDico="true"
                                       DonWidth="40px" DonTabIndex="14" DonTooltip="Nombre de mois"/>
                                </asp:TableCell>
                                <asp:TableCell RowSpan="1" HorizontalAlign="Left"> 
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH19" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="25" V_Information="19" V_SiDonneeDico="true"
                                       DonWidth="40px" DonTabIndex="15" DonTooltip="Nombre de jours"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="15px" ColumnSpan="3"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH20" runat="server"
                                       V_PointdeVue="1" V_Objet="25" V_Information="20" V_SiDonneeDico="true"
                                       EtiWidth="300px" DonWidth="40px" DonTabIndex="16" 
                                       EtiText="Services conduisant à Pension" DonTooltip="Nombre d'années"/>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left"> 
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH21" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="25" V_Information="21" V_SiDonneeDico="true"
                                       DonWidth="40px" DonTabIndex="17" DonTooltip="Nombre de mois"/>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left"> 
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH22" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="25" V_Information="22" V_SiDonneeDico="true"
                                       DonWidth="40px" DonTabIndex="18" DonTooltip="Nombre de jours"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Label ID="EtiMobilite" runat="server" Text="Mobilité" Height="20px" Width="300px"
                           BackColor="#2FA49B" BorderColor="#B0E0D7"  BorderStyle="Groove"
                           BorderWidth="2px" ForeColor="#D7FAF3"
                           Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 15px; margin-left: 4px; margin-bottom:5px; font-style: oblique; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCocheSimple ID="Coche24" runat="server"
                           V_PointdeVue="1" V_Objet="25" V_Information="24" V_SiDonneeDico="true" V_Width="200px" V_Style="margin-left:5px"/> 
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" Width="400px">
                        <Virtualia:VCoupleEtiDate ID="InfoD25" runat="server" TypeCalendrier="Standard" EtiWidth="200px"
                               V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="25" V_Information="25" DonTabIndex="19"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                        <asp:Label ID="EtiEcole" runat="server" Text="Ecole de fonctionnaire" Height="20px" Width="300px"
                           BackColor="#2FA49B" BorderColor="#B0E0D7"  BorderStyle="Groove"
                           BorderWidth="2px" ForeColor="#D7FAF3"
                           Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                           style="margin-top: 15px; margin-left: 4px; margin-bottom:5px; font-style: oblique; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Left"> 
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab11" runat="server" V_PointdeVue="1" V_Objet="25" V_Information="11" V_SiDonneeDico="true"
                           EtiWidth="250px" DonWidth="350px" DonTabIndex="20"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left"> 
                        <Virtualia:VCoupleEtiDate ID="InfoD12" runat="server" TypeCalendrier="Standard" EtiWidth="40px"
                               V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="25" V_Information="12" DonTabIndex="21"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDate ID="InfoD13" runat="server" TypeCalendrier="Standard" EtiWidth="40px" SiDateFin="true"
                               V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="25" V_Information="13" DonTabIndex="22" EtiStyle="margin-left:2px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Center"> 
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV10" runat="server"
                           V_PointdeVue="1" V_Objet="25" V_Information="10" V_SiDonneeDico="true"
                           EtiWidth="602px" DonWidth="600px" DonTabIndex="23"
                           DonTextMode="true" DonHeight="60px" EtiStyle="margin-top:10px; text-align:center"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px" Columnspan="2"></asp:TableCell>
                </asp:TableRow>
        </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Panel ID="CadreExpert" runat="server" BorderStyle="none" BorderWidth="1px"
                BorderColor="#B0E0D7" Height="300px" Width="600px" HorizontalAlign="Left"
                style="margin-top: 5px; vertical-align: middle; text-align:center">
           <Virtualia:VCoupleEtiquetteExperte ID="ExprH01" runat="server"
               V_PointdeVue="1" V_Objet="25" V_InfoExperte="612" V_SiDonneeDico="true"
               EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px"
               EtiStyle="margin-left: 50px; margin-top: 5px" DonStyle="margin-top: 9px; text-align: center"/>
           <Virtualia:VCoupleEtiquetteExperte ID="ExprH02" runat="server"
               V_PointdeVue="1" V_Objet="25" V_InfoExperte="616" V_SiDonneeDico="true"
               EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px"
               EtiStyle="margin-left: 50px;" DonStyle="text-align: center;"/>
           <Virtualia:VCoupleEtiquetteExperte ID="ExprH03" runat="server"
               V_PointdeVue="1" V_Objet="25" V_InfoExperte="610" V_SiDonneeDico="true"
               EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px"
               EtiStyle="margin-left: 50px;" DonStyle="text-align: center;"/>
            <Virtualia:VCoupleEtiquetteExperte ID="ExprH04" runat="server"
               V_PointdeVue="1" V_Objet="25" V_InfoExperte="615" V_SiDonneeDico="true"
               EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px"
               EtiStyle="margin-left: 50px;" DonStyle="text-align: center;"/>
            <Virtualia:VCoupleEtiquetteExperte ID="ExprH05" runat="server"
               V_PointdeVue="1" V_Objet="25" V_InfoExperte="850" V_SiDonneeDico="true"
               EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px"
               EtiStyle="margin-left: 50px;" DonStyle="text-align: center;"/>
            <Virtualia:VCoupleEtiquetteExperte ID="ExprH06" runat="server"
               V_PointdeVue="1" V_Objet="25" V_InfoExperte="853" V_SiDonneeDico="true"
               EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px"
               EtiStyle="margin-left: 50px;" DonStyle="text-align: center;"/>
            <Virtualia:VCoupleEtiquetteExperte ID="ExprH07" runat="server"
               V_PointdeVue="1" V_Objet="25" V_InfoExperte="851" V_SiDonneeDico="true"
               EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px"
               EtiStyle="margin-left: 50px;" DonStyle="text-align: center;"/>
            <Virtualia:VCoupleEtiquetteExperte ID="ExprH08" runat="server"
               V_PointdeVue="1" V_Objet="25" V_InfoExperte="852" V_SiDonneeDico="true"
               EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px"
               EtiStyle="margin-left: 50px;" DonStyle="text-align: center;"/>
            <Virtualia:VCoupleEtiquetteExperte ID="ExprH09" runat="server"
               V_PointdeVue="1" V_Objet="25" V_InfoExperte="854" V_SiDonneeDico="true"
               EtiHeight="20px" EtiWidth="300px" DonWidth="200px" DonHeight="20px"
               EtiStyle="margin-left: 50px;" DonStyle="text-align: center;"/>
          </asp:Panel>
      </asp:TableCell>
    </asp:TableRow>
 </asp:Table>