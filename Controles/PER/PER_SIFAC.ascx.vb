﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class PER_SIFAC
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsNumObjet As Integer = 180
    Private WsNomTable As String = "PER_SIFAC"
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_SIFAC

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then

                Dim ColHisto As New List(Of Integer)
                ColHisto.Add(0)
                ColHisto.Add(1)
                ColHisto.Add(2)
                ColHisto.Add(7)
                ColHisto.Add(14)
                V_CacheColHisto = ColHisto

                V_Identifiant = value
                Call ActualiserListe()
            End If
        End Set
    End Property
    Private Sub ActualiserListe()
        CadreCmdOK.Visible = False
        Dim LstLibels As New List(Of String)
        LstLibels.Add("Aucune situation")
        LstLibels.Add("Une situation")
        LstLibels.Add("situations")
        ListeGrille.V_LibelCaption = LstLibels

        Dim LstColonnes As New List(Of String)
        LstColonnes.Add("date d'effet")
        LstColonnes.Add("Clef complète")
        LstColonnes.Add("Numdos")
        LstColonnes.Add("centre de coûts")
        LstColonnes.Add("taux ou montant")
        LstColonnes.Add("Clef")
        ListeGrille.V_LibelColonne = LstColonnes

        If V_Identifiant > 0 Then
            Call InitialiserControles()
            If V_IndexFiche = -1 Then
                ListeGrille.V_Liste = Nothing
                Exit Sub
            End If
            ListeGrille.V_Liste = V_ListeFiches
        End If
    End Sub
    Protected Sub ListeGrille_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ListeGrille.ValeurChange
        If e.Valeur = "" Then
            Exit Sub
        End If
        V_Occurence = e.Valeur
    End Sub

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee.Item(NumInfo)
            End If
            IndiceI += 1
        Loop

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirDonneeTable.DonText = ""
            Else
                VirDonneeTable.DonText = CacheDonnee(NumInfo)
            End If
            IndiceI += 1
        Loop

        Dim VirCoche As Controles_VCocheSimple
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Coche", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirCoche = CType(Ctl, Controles_VCocheSimple)
            VirCoche.V_SiAutoPostBack = True
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirCoche.V_Check = False
                VirCoche.V_Text = "Traitement principal"
                CellCodePrime.Visible = False
            ElseIf CacheDonnee.item(NumInfo) = "P" Then
                VirCoche.V_Check = True
                VirCoche.V_Text = "Primes"
                CellCodePrime.Visible = True
            Else
                VirCoche.V_Check = False
                VirCoche.V_Text = "Traitement principal"
                CellCodePrime.Visible = False
            End If
            IndiceI += 1
        Loop
    End Sub

    Protected Sub CommandeNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeNew.Click
        Call InitialiserControles()
        Call V_CommandeNewFiche()
        CadreCmdOK.Visible = True
    End Sub

    Protected Sub CommandeSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeSupp.Click
        Call V_CommandeSuppFiche()
    End Sub

    Public WriteOnly Property RetourDialogueSupp(ByVal Cmd As String) As String
        Set(ByVal value As String)
            V_RetourDialogueSupp(Cmd) = value
            Call ActualiserListe()
        End Set
    End Property

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim LstErreurs As List(Of String) = SiMajPossible()
        If LstErreurs Is Nothing Then
            Call V_MajFiche()
            Call ActualiserListe()
            CadreCmdOK.Visible = False
            Exit Sub
        End If
        Call V_MessageDialog(V_WebFonction.Evt_MessageBloquant(WsNumObjet, "Mise à jour impossible", LstErreurs))
    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH00.ValeurChange,
    InfoH02.ValeurChange, InfoH14.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee.Item(NumInfo) <> e.Valeur Then
                    CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub Dontab_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles Dontab13.AppelTable,
    Dontab07.AppelTable, Dontab08.AppelTable, Dontab09.AppelTable, Dontab10.AppelTable, Dontab11.AppelTable, Dontab12.AppelTable

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VDuoEtiquetteCommande).ID, 2))
        Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(e.ControleAppelant, e.ObjetAppelant, e.PointdeVueInverse, e.NomdelaTable)
        Call V_AppelTable(Evenement)
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String, ByVal Codification As String) As String
        Set(ByVal value As String)
            Dim NumInfo As Integer
            Dim Ctl As Control
            Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
            Dim CacheDonnee As List(Of String) = V_CacheMaj

            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab" & Strings.Right(IDAppelant, 2), 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            Select Case NumInfo
                Case 7, 9, 11
                    VirDonneeTable.DonText = Codification
                    Call V_ValeurChange(NumInfo, Codification)
                    Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab" & Strings.Format(NumInfo + 1, "00"), 0)
                    If Ctl IsNot Nothing Then
                        CType(Ctl, Controles_VDuoEtiquetteCommande).DonText = value
                        Call V_ValeurChange(NumInfo + 1, value)
                    End If
                Case 8, 10, 12
                    VirDonneeTable.DonText = value
                    Call V_ValeurChange(NumInfo, value)
                    Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab" & Strings.Format(NumInfo - 1, "00"), 0)
                    If Ctl IsNot Nothing Then
                        CType(Ctl, Controles_VDuoEtiquetteCommande).DonText = Codification
                        Call V_ValeurChange(NumInfo - 1, Codification)
                    End If
                Case 13
                    Dim LstTabDecision As List(Of Virtualia.TablesObjet.ShemaREF.OUT_DECISION) = Nothing

                    '' ****** AKR 22/02/2018
                    'Dim SessionPaie As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
                    'LstTabDecision = SessionPaie.ObjetEnsemble.VParent.VirReferentiel.ListeTablesDecision(24)
                    'If LstTabDecision Is Nothing Then
                    '    Codification = ""
                    'Else
                    '    For Each FicheREF In LstTabDecision
                    '        If FicheREF.ValeurInterne(0) = value Then
                    '            Codification = FicheREF.CodeExterne(1)
                    '            Exit For
                    '        End If
                    '    Next
                    'End If
                    ''***** AKR FIN
                    VirDonneeTable.DonText = Codification
                    Call V_ValeurChange(NumInfo, Codification)
            End Select
            If CacheDonnee IsNot Nothing Then
                If CacheDonnee.Item(NumInfo) Is Nothing Then
                    VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                Else
                    If CacheDonnee.Item(NumInfo) <> value Then
                        VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                        CadreCmdOK.Visible = True
                    End If
                End If
            End If
        End Set
    End Property

    Protected Sub Coche_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles Coche03.ValeurChange
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCocheSimple).ID, 2))
        If CacheDonnee IsNot Nothing Then
            CadreCmdOK.Visible = True
            Select Case e.Valeur
                Case "Oui"
                    Call V_ValeurChange(NumInfo, "P")
                Case Else
                    Call V_ValeurChange(NumInfo, "T")
            End Select
        End If
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_Objet(TypeListe.SiListeGrid) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        ListeGrille.Centrage_Colonne(0) = 1
        ListeGrille.Centrage_Colonne(1) = 1
        ListeGrille.Centrage_Colonne(2) = 1
        ListeGrille.Centrage_Colonne(3) = 1
        ListeGrille.Centrage_Colonne(4) = 1
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.V_SiEnLectureSeule = True Then
            CadreCommandes.Visible = False
            Call InitialiserControles()
        End If
        Call LireLaFiche()

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Cadre")
        CadreInfo.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            VirControle.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonBackColor = Drawing.Color.White
            VirDonneeTable.DonText = ""
            IndiceI += 1
        Loop
    End Sub

    Private Function SiMajPossible() As List(Of String)
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim TableauErreur As List(Of String) = Nothing
        If CacheDonnee.Item(0) = "" Then
            TableauErreur = New List(Of String)
            TableauErreur.Add("La saisie de la date de valeur est obligatoire")
        End If
        If CacheDonnee.Item(7) = "" Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie du centre de coûts est obligatoire")
        End If
        If CacheDonnee.Item(9) = "" Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie du domaine fonctionnel est obligatoire")
        End If
        If CacheDonnee.Item(14) = "" OrElse V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(14), 2) = 0 Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La saisie d'un pourcentage est obligatoire")
        End If
        If CacheDonnee.Item(2) = "" OrElse IsNumeric(CacheDonnee.Item(2)) = False Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("Le numéro de dossier DGFIP (Numdos) est obligatoire")
            Return TableauErreur
        End If
        'Dim SiOK As Boolean
        ''**** AKR 22/02/2018
        'Dim SessionPaie As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        'Dim Dossier As Virtualia.Metier.ETPPaie.ObjetDossierPER

        Dim UtiSession As Virtualia.Net.Individuel.LocalNavigation = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
        Dim Dossier = UtiSession.DossierPER
        ''***** FIN AKR

        'Dim DossierBASE As Virtualia.Metier.ETPPaie.DGFIP.ObjetDossierBASE
        Dim LstFichesPER As List(Of Virtualia.TablesObjet.ShemaPER.PER_SIFAC)
        Dim LstFichesSIFAC As List(Of Virtualia.TablesObjet.ShemaPER.PER_SIFAC)
        'Dossier = SessionPaie.ObjetEnsemble.ItemDossier(V_Identifiant)
        If Dossier Is Nothing Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("Erreur d'identification")
            Return TableauErreur
        End If

        'DossierBASE = Dossier.PointeurObjetBASEPAY
        'If DossierBASE IsNot Nothing Then
        '    SiOK = False
        '    For Each ElementBA In DossierBASE.ListeBASPAY
        '        If CInt(CacheDonnee.Item(2)) = CInt(ElementBA.Numero_Dossier) Then
        '            SiOK = True
        '            Exit For
        '        End If
        '        If SiOK = False Then
        '            If TableauErreur Is Nothing Then
        '                TableauErreur = New List(Of String)
        '            End If
        '            TableauErreur.Add("Le numéro de dossier DGFIP (Numdos) n'est pas valide")
        '            Return TableauErreur
        '        End If
        '    Next
        'End If
        LstFichesPER = V_WebFonction.ViRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_SIFAC)(V_ListeFiches("", ""))
        If CacheDonnee.Item(3) = "" Then
            CacheDonnee.Item(3) = "T"
        End If
        Select Case CacheDonnee.Item(3)
            Case "P"
                If CacheDonnee.Item(13) = "" Then
                    If TableauErreur Is Nothing Then
                        TableauErreur = New List(Of String)
                    End If
                    TableauErreur.Add("La saisie du code prime est obligatoire")
                End If
        End Select
        Dim NumOrdre As Integer
        Dim TotPercent As Double
        NumOrdre = CInt(V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(4), 0))
        TotPercent = V_WebFonction.ViRhFonction.ConversionDouble(CacheDonnee.Item(14), 2)
        If LstFichesPER Is Nothing OrElse LstFichesPER.Count = 0 Then
            NumOrdre = 1
        Else
            Select Case CacheDonnee.Item(3)
                Case "P"
                    LstFichesSIFAC = (From Fiche In LstFichesPER Where Fiche.Date_de_Valeur = CacheDonnee.Item(0) And Fiche.Type_VB = CacheDonnee.Item(3) _
                                          And Fiche.NumDos_DGFIP = CInt(CacheDonnee.Item(2)) And Fiche.CodePrime = CacheDonnee.Item(13) And Fiche.NoOrdre <> NumOrdre).ToList
                Case Else
                    LstFichesSIFAC = (From Fiche In LstFichesPER Where Fiche.Date_de_Valeur = CacheDonnee.Item(0) And Fiche.Type_VB = CacheDonnee.Item(3) _
                                          And Fiche.NumDos_DGFIP = CInt(CacheDonnee.Item(2)) And Fiche.NoOrdre <> NumOrdre).ToList
            End Select
            If LstFichesSIFAC Is Nothing OrElse LstFichesSIFAC.Count = 0 Then
                NumOrdre = 1
            Else
                NumOrdre = LstFichesSIFAC.Count + 1
                For Each FichePER In LstFichesSIFAC
                    TotPercent += FichePER.Pourcentage
                Next
            End If
        End If
        CacheDonnee.Item(4) = CStr(NumOrdre)
        If TotPercent > 100 Then
            If TableauErreur Is Nothing Then
                TableauErreur = New List(Of String)
            End If
            TableauErreur.Add("La somme des pourcentages ne peut excéder 100% pour une même date de valeur et un même dossier DGFIP.")
        End If
        CacheDonnee.Item(1) = Strings.Format(CInt(CacheDonnee.Item(2)), "00") & CacheDonnee.Item(3) & CacheDonnee.Item(4) & CacheDonnee.Item(13)
        V_CacheMaj = CacheDonnee
        Return TableauErreur
    End Function
End Class