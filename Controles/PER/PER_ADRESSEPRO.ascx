﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_ADRESSEPRO" Codebehind="PER_ADRESSEPRO.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>

 <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
    BorderColor="#B0E0D7" Height="570px" Width="750px" HorizontalAlign="Center"
    style="position:relative">
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
            CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="false"
            BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
            Width="70px" HorizontalAlign="Right" style="margin-top: 3px; margin-right:3px">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Bottom">
                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                    BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                    </asp:Button>
                </asp:TableCell>
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" 
            CellSpacing="0" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                    <asp:Label ID="EtiCoordonnes" runat="server" Text="Coordonnées professionnelles" Height="20px" Width="300px"
                        BackColor="#2FA49B" BorderColor="#B0E0D7"  BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#D7FAF3"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px;
                        font-style: oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell ColumnSpan="2" Width="600px" HorizontalAlign="Left"> 
                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server"
                        V_PointdeVue="1" V_Objet="24" V_Information="7" V_SiDonneeDico="true"
                        EtiWidth="200px" DonWidth="500px" DonTabIndex="1"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell ColumnSpan="2" Width="600px" HorizontalAlign="Left"> 
                    <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server"
                        V_PointdeVue="1" V_Objet="24" V_Information="11" V_SiDonneeDico="true"
                        EtiWidth="200px" DonWidth="200px" DonTabIndex="2"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell Width="350px" HorizontalAlign="Left">          
                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server"
                        V_PointdeVue="1" V_Objet="24" V_Information="2" V_SiDonneeDico="true"
                        EtiWidth="200px" DonWidth="200px" DonTabIndex="3"/>
                </asp:TableCell>
                <asp:TableCell Width="400px" HorizontalAlign="Left"> 
                    <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server"
                        V_PointdeVue="1" V_Objet="24" V_Information="8" V_SiDonneeDico="true"
                        EtiWidth="100px" DonWidth="200px" DonTabIndex="4"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell ColumnSpan="2" Width="350px" HorizontalAlign="Left">          
                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server"
                        V_PointdeVue="1" V_Objet="24" V_Information="4" V_SiDonneeDico="true"
                        EtiWidth="200px" DonWidth="200px" DonTabIndex="5"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="12px" ColumnSpan="2"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell ColumnSpan="2" Width="350px" HorizontalAlign="Left">          
                    <Virtualia:VCoupleEtiDonnee ID="InfoH15" runat="server"
                        V_PointdeVue="1" V_Objet="24" V_Information="15" V_SiDonneeDico="true"
                        EtiWidth="200px" DonWidth="100px" DonTabIndex="6"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell ColumnSpan="2" Width="350px" HorizontalAlign="Left">          
                    <Virtualia:VCoupleEtiDonnee ID="InfoH16" runat="server"
                        V_PointdeVue="1" V_Objet="24" V_Information="16" V_SiDonneeDico="true"
                        EtiWidth="200px" DonWidth="160px" DonTabIndex="7"/>
                </asp:TableCell>
            </asp:TableRow> 
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" Width="400px" HorizontalAlign="Left"> 
                    <Virtualia:VCoupleEtiDonnee ID="InfoH17" runat="server"
                        V_PointdeVue="1" V_Objet="24" V_Information="17" V_SiDonneeDico="true"
                        EtiWidth="200px" DonWidth="300px" DonTabIndex="8"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell ColumnSpan="2" Width="350px" HorizontalAlign="Left">          
                    <Virtualia:VCoupleEtiDonnee ID="InfoH19" runat="server"
                        V_PointdeVue="1" V_Objet="24" V_Information="19" V_SiDonneeDico="true"
                        EtiWidth="200px" DonWidth="300px" DonTabIndex="9"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" Width="375px" HorizontalAlign="Left">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab06" runat="server" 
                        V_PointdeVue="1" V_Objet="24" V_Information="6" V_SiDonneeDico="true"
                        EtiWidth="200px" DonWidth="450px" DonTabIndex="10"/> 
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" Width="375px" HorizontalAlign="Left">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab05" runat="server" 
                        V_PointdeVue="1" V_Objet="24" V_Information="5" V_SiDonneeDico="true"
                        EtiWidth="200px" DonWidth="450px" DonTabIndex="11"/> 
                </asp:TableCell>
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="Bureau" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" Width="375px" HorizontalAlign="Center">
                    <asp:Label ID="EtiAdresseBureau" runat="server" Text="Adresse des bureaux"
                      Height="20px" Width="300px"
                      BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Outset"
                      BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true"
                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                      style="margin-top: 20px; margin-left: 4px; margin-bottom: 2px; text-indent: 5px; text-align: center">
                    </asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" Width="375px" HorizontalAlign="Left">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab09" runat="server" 
                      V_PointdeVue="1" V_Objet="24" V_Information="9" V_SiDonneeDico="true"
                      EtiWidth="120px" DonWidth="510px" DonTabIndex="12"/> 
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="15px" ColumnSpan="2"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">  
                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server"
                      V_PointdeVue="1" V_Objet="24" V_Information="1" V_SiDonneeDico="true"
                      EtiWidth="200px" DonWidth="420px" DonTabIndex="13"/>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow> 
                <asp:TableCell ColumnSpan="2" HorizontalAlign="Left">          
                    <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" 
                      V_PointdeVue="1" V_Objet="24" V_Information="10" V_SiDonneeDico="true"
                      EtiWidth="200px" DonWidth="420px" DonTabIndex="14"/>
                </asp:TableCell>
            </asp:TableRow> 
            <asp:TableRow> 
                <asp:TableCell Width="375px" HorizontalAlign="Left">  
                    <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server"
                      V_PointdeVue="1" V_Objet="24" V_Information="12" V_SiDonneeDico="true"
                      EtiWidth="120px" DonWidth="120px" DonTabIndex="15"/>
                </asp:TableCell>
                <asp:TableCell Width="375px" HorizontalAlign="Left">          
                    <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" 
                      V_PointdeVue="1" V_Objet="24" V_Information="13" V_SiDonneeDico="true"
                      EtiWidth="120px" DonWidth="125px" DonTabIndex="16"/>
                </asp:TableCell>
            </asp:TableRow> 
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" Width="375px" HorizontalAlign="Left">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab14" runat="server" 
                      V_PointdeVue="1" V_Objet="24" V_Information="14" V_SiDonneeDico="true"
                      EtiWidth="120px" DonWidth="220px" DonTabIndex="17"/> 
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="12px" Columnspan="2"></asp:TableCell>
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>   
 </asp:Table>       
 