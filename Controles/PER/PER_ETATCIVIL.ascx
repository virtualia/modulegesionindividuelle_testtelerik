﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_ETATCIVIL" Codebehind="PER_ETATCIVIL.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" tagname="VCoupleEtiquetteExperte" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
 
 <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
    BorderColor="#B0E0D7" Height="800px" Width="800px" HorizontalAlign="Center" BackColor="#B0E0D7">
    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell>
            <asp:Table ID="CadreCmdOK" runat="server" HorizontalAlign="Left" Style="margin-top: 3px; margin-right: 3px">
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Bottom">
                                <telerik:RadButton ID="CommandeOK" runat="server" Text="Valider" 
                                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#000" Skin="Silk" EnableEmbeddedSkins="true"
                                    Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Font-Italic="true"
                                    BorderStyle="None" Style="margin-left: 6px; text-align: Right;">
                                    <Icon PrimaryIconCssClass="rbOk" PrimaryIconBottom="8px"  />
                                </telerik:RadButton>                                    
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitre" runat="server" Height="40px" Width="750px" CellPadding="0" 
                 CellSpacing="0" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label ID="Etiquette" runat="server" Text="Etat-Civil" Height="20px" Width="150px"
                                BackColor="#2FA49B" BorderColor="#B0E0D7"  BorderStyle="Groove"
                                BorderWidth="2px" ForeColor="#D7FAF3"
                                Font-Bold="True" Font-Names="Calibri" Font-Size="Medium"
                                style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px;
                                text-indent: 5px; text-align: center;">
                            </asp:Label>          
                        </asp:TableCell>      
                    </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Qualite" runat="server" Height="25px" Width="720px" CellPadding="0" CellSpacing="0">
                <asp:TableRow> 
                     <asp:TableCell Width="210px" HorizontalAlign="Left"> 
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" 
                               V_PointdeVue="1" V_Objet="1" V_Information="1" V_SiDonneeDico="true"
                               EtiWidth="100px" DonWidth="100px" DonTabIndex="1" style="margin-left:30px"/>
                     </asp:TableCell>
                     <asp:TableCell Width="510px" HorizontalAlign="Left">
                        <Virtualia:VTrioHorizontalRadio ID="RadioH01" runat="server" V_SiAutoPostBack="true" V_Groupe="Qualite"
                               RadioGaucheWidth="130px" RadioCentreWidth="130px" RadioDroiteVisible="false"
                               RadioGaucheText="Monsieur" RadioCentreText="Madame" RadioDroiteText=""
                               RadioGaucheStyle="margin-left: 5px;" Visible="False"/>
                     </asp:TableCell>
                 </asp:TableRow>
                 <asp:TableRow>
                        <asp:TableCell Height="3px"></asp:TableCell>
                 </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="NomPrenom" runat="server" Height="50px" Width="720px" CellPadding="0" CellSpacing="0">
                 <asp:TableRow>
                    <asp:TableCell Width="310px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server"
                           V_PointdeVue="1" V_Objet="1" V_Information="2" V_SiDonneeDico="true"
                           EtiWidth="100px" DonWidth="200px"  DonHeight="30px" DonBackColor="WhiteSmoke" DonTabIndex="2"/>
                    </asp:TableCell>
                    <asp:TableCell Width="310px" HorizontalAlign="Left">
                         <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server"
                           V_PointdeVue="1" V_Objet="1" V_Information="13" V_SiDonneeDico="true"
                           EtiWidth="100px" DonWidth="200px" DonHeight="30px" DonBackColor="WhiteSmoke" DonTabIndex="3"/> 
                    </asp:TableCell>
                    <asp:TableCell Width="100px"></asp:TableCell>
                 </asp:TableRow>
                <asp:TableRow>
                        <asp:TableCell Height="10px"></asp:TableCell>
                 </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell Width="310px" HorizontalAlign="Left">
                         <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server"
                           V_PointdeVue="1" V_Objet="1" V_Information="3" V_SiDonneeDico="true"
                           EtiWidth="100px" DonWidth="200px" DonHeight="30px" DonBackColor="WhiteSmoke" DonTabIndex="4"/>
                    </asp:TableCell>
                    <asp:TableCell Width="310px" HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server"
                           V_PointdeVue="1" V_Objet="1" V_Information="14" V_SiDonneeDico="true"
                           EtiWidth="100px" DonWidth="200px" DonHeight="30px" DonBackColor="WhiteSmoke" DonTabIndex="5"/>  
                    </asp:TableCell>
                    <asp:TableCell Width="100px"></asp:TableCell>
                  </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>       
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Naissance" runat="server" Height="40px" Width="720px" CellPadding="0" CellSpacing="0">
              <asp:TableRow VerticalAlign="Middle">
                <asp:TableCell Width="200px" HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDate ID="InfoD04" runat="server" TypeCalendrier="Standard" EtiWidth="100px"
                               V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="1" V_Information="4" DonTabIndex="6"/>
                </asp:TableCell>
                <asp:TableCell Width="200px" HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server"
                           V_PointdeVue="1" V_Objet="1" V_Information="5" V_SiDonneeDico="true"
                           EtiWidth="20px" DonWidth="150px" EtiText="à" DonHeight="30px" DonBackColor="WhiteSmoke" DonTabIndex="7"
                           EtiStyle="margin-left: 1px;"/>
                </asp:TableCell>
                <asp:TableCell Width="300px" HorizontalAlign="Left">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab06" runat="server" 
                           V_PointdeVue="1" V_Objet="1" V_Information="6" V_SiDonneeDico="true"
                           EtiWidth="100px" DonWidth="200px" DonTabIndex="8"
                           EtiStyle="margin-left: 1px;"/>
                </asp:TableCell>
                <asp:TableCell Width="20px"></asp:TableCell>
              </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px"></asp:TableCell>
                </asp:TableRow>
           </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell> 
            <asp:Table ID="Sexe" runat="server" Height="30px" Width="720px" CellPadding="0" CellSpacing="0">
              <asp:TableRow VerticalAlign="Middle"> 
                <asp:TableCell Width="260px" HorizontalAlign="Left"> 
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab07" runat="server" 
                           V_PointdeVue="1" V_Objet="1" V_Information="7" V_SiDonneeDico="true"
                           EtiWidth="100px" DonWidth="200px" DonTabIndex="9"
                           EtiStyle="margin-left: 4px;"/>
                </asp:TableCell>
                <asp:TableCell Width="160px" HorizontalAlign="Left"> 
                     <Virtualia:VDuoEtiquetteCommande ID="Dontab08" runat="server" 
                           V_PointdeVue="1" V_Objet="1" V_Information="8" V_SiDonneeDico="true"
                           EtiWidth="70px" DonWidth="80px" DonTabIndex="10"
                           EtiStyle="margin-left: 1px;"/>
                </asp:TableCell>
                <asp:TableCell Width="320px" HorizontalAlign="Left">        
                    <Virtualia:VTrioHorizontalRadio ID="RadioH08" runat="server" V_Groupe="Sexe" V_SiAutoPostBack="true"
                       RadioGaucheWidth="110px" RadioCentreWidth="110px" RadioDroiteWidth="0px" 
                       RadioGaucheText="Masculin" RadioCentreText="Féminin" RadioDroiteVisible="false" 
                       RadioGaucheStyle="margin-left: 5px;" Visible="False"/>
                </asp:TableCell>
             </asp:TableRow>
             <asp:TableRow>
                <asp:TableCell Height="10px"></asp:TableCell>
             </asp:TableRow>
          </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Sitfam" runat="server" Height="30px" Width="720px" CellPadding="0" CellSpacing="0">
             <asp:TableRow>
                <asp:TableCell Width="260px" HorizontalAlign="Left">
                    <Virtualia:VDuoEtiquetteCommande ID="Dontab09" runat="server" 
                           V_PointdeVue="1" V_Objet="1" V_Information="9" V_SiDonneeDico="true"
                           EtiHeight="40px" EtiWidth="100px" DonWidth="150px" DonTabIndex="11"/>
                </asp:TableCell>
                <asp:TableCell Width="160px" HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDate ID="InfoD10" runat="server" TypeCalendrier="Standard" EtiWidth="70px" 
                               V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="1" V_Information="10" DonTabIndex="12" EtiStyle="margin-left: 1px;"/>
                </asp:TableCell>
                <asp:TableCell Width="300px"></asp:TableCell>
             </asp:TableRow> 
             <asp:TableRow>
                <asp:TableCell Height="10px"></asp:TableCell>
             </asp:TableRow>
          </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="NIR" runat="server" Height="30px" Width="720px" CellPadding="0" CellSpacing="0">
             <asp:TableRow VerticalAlign="Middle">
                <asp:TableCell Width="362px" HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server"
                       V_PointdeVue="1" V_Objet="1" V_Information="11" V_SiDonneeDico="true"
                       EtiWidth="250px" DonWidth="160px" DonHeight="30px" DonBackColor="WhiteSmoke" DonTabIndex="13"/>
                </asp:TableCell>
                <asp:TableCell Width="52px" HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH12" runat="server"
                       V_PointdeVue="1" V_Objet="1" V_Information="12" V_SiDonneeDico="true"
                       EtiWidth="30px" EtiText="Clé" DonWidth="50px" DonHeight="30px" DonBackColor="WhiteSmoke" DonTabIndex="14"/> 
                </asp:TableCell>
                 <asp:TableCell Width="170px" HorizontalAlign="Left">
                    <Virtualia:VCoupleEtiDate ID="InfoD17" runat="server" TypeCalendrier="Standard"
                       V_PointdeVue="1" V_Objet="1" V_Information="17" V_SiDonneeDico="true"
                       EtiHeight="40px" EtiWidth="120px" DonWidth="80px" DonTabIndex="15"
                       V_SiEnLectureSeule="True" EtiStyle="margin-left: 1px;"/> 
                </asp:TableCell>
                <asp:TableCell Width="50px"></asp:TableCell>
             </asp:TableRow>
            </asp:Table>                
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
             <asp:Panel ID="CadreExpert" runat="server" BorderStyle="None" BorderWidth="1px"
                   BorderColor="#B0E0D7" Height="160px" Width="750px" HorizontalAlign="Left"
                   style="margin-top: 5px;  vertical-align: middle">
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH01" runat="server"
                   V_PointdeVue="1" V_Objet="1" V_InfoExperte="502" V_SiDonneeDico="true"
                   EtiHeight="20px" EtiWidth="270px" DonWidth="40px" DonHeight="20px"
                   EtiStyle="margin-left: 150px; margin-top: 5px" DonStyle="margin-top: 9px; text-align: center"/>
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH02" runat="server"
                   V_PointdeVue="1" V_Objet="1" V_InfoExperte="511" V_SiDonneeDico="true"
                   EtiHeight="20px" EtiWidth="270px" DonWidth="200px" DonHeight="20px"
                   EtiStyle="margin-left: 150px;" DonStyle="text-align: center;"/>
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH03" runat="server"
                   V_PointdeVue="1" V_Objet="1" V_InfoExperte="506" V_SiDonneeDico="true"
                   EtiHeight="20px" EtiWidth="270px" DonWidth="200px" DonHeight="20px"
                   EtiStyle="margin-left: 150px;" DonStyle="text-align: center;"/>
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH04" runat="server"
                   V_PointdeVue="1" V_Objet="1" V_InfoExperte="503" V_SiDonneeDico="true"
                   EtiHeight="20px" EtiWidth="270px" DonWidth="120px" DonHeight="20px"
                   EtiStyle="margin-left: 150px;" DonStyle="text-align: center;"/>
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH05" runat="server"
                   V_PointdeVue="1" V_Objet="1" V_InfoExperte="504" V_SiDonneeDico="true"
                   EtiHeight="20px" EtiWidth="270px" DonWidth="120px" DonHeight="20px"
                   EtiStyle="margin-left: 150px;" DonStyle="text-align: center;"/>
            </asp:Panel>       
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

