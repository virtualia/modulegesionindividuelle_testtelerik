﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_AFFECTATION3" Codebehind="PER_AFFECTATION3.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" tagname="VCoupleEtiquetteExperte" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>

 <asp:Table ID="CadreControle" runat="server" Height="30px" Width="600px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
   <asp:TableRow> 
     <asp:TableCell> 
       <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="680px" SiColonneSelect="true"
                                               SiCaseAcocher="false"/>
     </asp:TableCell>
   </asp:TableRow>
   <asp:TableRow>
     <asp:TableCell>
          <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
            BorderColor="#B0E0D7" Height="550px" Width="700px" HorizontalAlign="Center" style="margin-top: 3px;">
            <asp:TableRow>
              <asp:TableCell>
                <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                    Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Bottom">
                            <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                                Width="160px" CellPadding="0" CellSpacing="0"
                                BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                            BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                            BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                            Tooltip="Nouvelle fiche" >
                                        </asp:Button>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                            BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                            BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                            Tooltip="Supprimer la fiche" >
                                        </asp:Button>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>    
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Bottom">
                            <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                                BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                CellPadding="0" CellSpacing="0" Visible="false" >
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                            BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                            BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                            </asp:Button>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>  
                        </asp:TableCell>
                    </asp:TableRow>
                  </asp:Table>
              </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
              <asp:TableCell>
                  <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="700px" 
                    CellSpacing="0" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="Etiquette" runat="server" Text="3éme Affectation fonctionnelle" Height="20px" Width="310px"
                                BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                                BorderWidth="2px" ForeColor="#D7FAF3"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px;
                                font-style: oblique; text-indent: 5px; text-align: center;">
                            </asp:Label>          
                        </asp:TableCell>      
                    </asp:TableRow>
                  </asp:Table>
              </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
              <asp:TableCell>
                  <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="700px">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" Width="300px">
                            <Virtualia:VCoupleEtiDate ID="InfoD00" runat="server" TypeCalendrier="Standard" EtiWidth="170px"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="141" V_Information="0" DonTabIndex="1"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left" Width="300px">
                            <Virtualia:VCoupleEtiDate ID="InfoD07" runat="server" TypeCalendrier="Standard" SiDateFin="true" EtiWidth="80px" EtiStyle="margin-left:2px"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="141" V_Information="7" DonTabIndex="2"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server"
                               V_PointdeVue="1" V_Objet="141" V_Information="1" V_SiDonneeDico="true"
                               DonTabIndex="3" DonWidth="490px"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server"
                               V_PointdeVue="1" V_Objet="141" V_Information="2" V_SiDonneeDico="true"
                               DonTabIndex="4" DonWidth="490px"/> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab03" runat="server"
                               V_PointdeVue="1" V_Objet="141" V_Information="3" V_SiDonneeDico="true"
                               DonTabIndex="5" DonWidth="490px"/> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab04" runat="server"
                               V_PointdeVue="1" V_Objet="141" V_Information="4" V_SiDonneeDico="true"
                               DonTabIndex="6" DonWidth="490px"/> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab14" runat="server"
                               V_PointdeVue="1" V_Objet="141" V_Information="14" V_SiDonneeDico="true"
                               DonTabIndex="7" DonWidth="490px"/> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab15" runat="server"
                               V_PointdeVue="1" V_Objet="141" V_Information="15" V_SiDonneeDico="true"
                               DonTabIndex="8" DonWidth="490px"/> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow Height="40px">
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="EtiEmploi" runat="server" Height="20px" Width="350px"
                               Text="Emploi / Fonction / Poste"
                               BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                               BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                            </asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab05" runat="server"
                               V_PointdeVue="1" V_Objet="141" V_Information="5" V_SiDonneeDico="true"
                               DonTabIndex="9" DonWidth="490px"/> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab17" runat="server"
                               V_PointdeVue="1" V_Objet="141" V_Information="17" V_SiDonneeDico="true"
                               DonTabIndex="10" DonWidth="490px"/> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server"
                               V_PointdeVue="1" V_Objet="141" V_Information="13" V_SiDonneeDico="true"
                               DonWidth="120px" DonTabIndex="11"/>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Right">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" Visible="false"
                               V_PointdeVue="1" V_Objet="141" V_Information="6" V_SiDonneeDico="true"
                               DonWidth="80px" DonTabIndex="12" DonStyle="margin-left:2px"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow Height="40px"> 
                        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                            <asp:Label ID="EtiComplement" runat="server" Height="20px" Width="350px"
                               Text="Informations complémentaires"
                               BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                               BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                               Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                            </asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab16" runat="server"
                               V_PointdeVue="1" V_Objet="141" V_Information="16" V_SiDonneeDico="true"
                               DonTabIndex="13" EtiWidth="250px" DonWidth="300px"/> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <Virtualia:VDuoEtiquetteCommande ID="Dontab18" runat="server"
                               V_PointdeVue="1" V_Objet="141" V_Information="18" V_SiDonneeDico="true"
                               DonTabIndex="14" DonWidth="490px"/> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server"
                               V_PointdeVue="1" V_Objet="141" V_Information="11" V_SiDonneeDico="true"
                               EtiWidth="120px" DonWidth="490px" DonTabIndex="15"/>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                            <Virtualia:VCocheSimple ID="Coche12" runat="server"
                               V_PointdeVue="1" V_Objet="141" V_Information="12" V_SiDonneeDico="true"
                               V_Width="300px" V_Style="margin-left:4px"/> 
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="12px" Columnspan="2"></asp:TableCell>
                    </asp:TableRow>
                  </asp:Table>
              </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
              <asp:TableCell>
                <asp:Panel ID="CadreExpert" runat="server" BorderStyle="none" BorderWidth="1px"
                    BorderColor="#B0E0D7" Height="250px" Width="520px" HorizontalAlign="Left"
                    style="margin-top: 5px;  vertical-align: middle">
                    <Virtualia:VCoupleEtiquetteExperte ID="ExprH01" runat="server"
                       V_PointdeVue="1" V_Objet="141" V_InfoExperte="871" V_SiDonneeDico="true"
                       EtiHeight="20px" EtiWidth="250px" DonWidth="200px" DonHeight="20px"
                       EtiStyle="margin-left: 40px; margin-top: 5px" DonStyle="margin-top: 9px; text-align: center"/>
                    <Virtualia:VCoupleEtiquetteExperte ID="ExprH02" runat="server"
                       V_PointdeVue="1" V_Objet="141" V_InfoExperte="872" V_SiDonneeDico="true"
                       EtiHeight="20px" EtiWidth="250px" DonWidth="200px" DonHeight="20px"
                       EtiStyle="margin-left: 40px;" DonStyle="text-align: center;"/>
                    <Virtualia:VCoupleEtiquetteExperte ID="ExprH03" runat="server"
                       V_PointdeVue="1" V_Objet="141" V_InfoExperte="873" V_SiDonneeDico="true"
                       EtiHeight="20px" EtiWidth="250px" DonWidth="200px" DonHeight="20px"
                       EtiStyle="margin-left: 40px;" DonStyle="text-align: center;"/>
                    <Virtualia:VCoupleEtiquetteExperte ID="ExprH04" runat="server"
                       V_PointdeVue="1" V_Objet="141" V_InfoExperte="874" V_SiDonneeDico="true"
                       EtiHeight="20px" EtiWidth="250px" DonWidth="200px" DonHeight="20px"
                       EtiStyle="margin-left: 40px;" DonStyle="text-align: center;"/>
                    <Virtualia:VCoupleEtiquetteExperte ID="ExprH05" runat="server"
                       V_PointdeVue="1" V_Objet="141" V_InfoExperte="875" V_SiDonneeDico="true"
                       EtiHeight="20px" EtiWidth="250px" DonWidth="200px" DonHeight="20px"
                       EtiStyle="margin-left: 40px;" DonStyle="text-align: center;"/>
                    <Virtualia:VCoupleEtiquetteExperte ID="ExprH06" runat="server"
                       V_PointdeVue="1" V_Objet="141" V_InfoExperte="876" V_SiDonneeDico="true"
                       EtiHeight="20px" EtiWidth="250px" DonWidth="200px" DonHeight="20px"
                       EtiStyle="margin-left: 40px;" DonStyle="text-align: center;"/>
                     <Virtualia:VCoupleEtiquetteExperte ID="ExprH07" runat="server"
                       V_PointdeVue="1" V_Objet="141" V_InfoExperte="604" V_SiDonneeDico="true"
                       EtiHeight="20px" EtiWidth="250px" DonWidth="200px" DonHeight="20px"
                       EtiStyle="margin-left: 40px;" DonStyle="text-align: center;"/>
                  </asp:Panel>
              </asp:TableCell>
            </asp:TableRow>     
         </asp:Table>
     </asp:TableCell>
   </asp:TableRow>
 </asp:Table>
        
        