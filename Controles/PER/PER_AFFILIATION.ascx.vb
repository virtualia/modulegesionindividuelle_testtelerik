﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Fenetre_PER_AFFILIATION
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsNumObjet As Integer = VI.ObjetPer.ObaCaisse
    Private WsNomTable As String = "PER_AFFILIATION"
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_AFFILIATION

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property Identifiant As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                Call InitialiserControles()
                V_Identifiant = value
            End If
        End Set
    End Property

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee.Item(NumInfo)
            End If
            VirControle.V_SiAutoPostBack = Not CadreCmdOK.Visible
            IndiceI += 1
        Loop

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                VirDonneeTable.DonText = ""
            Else
                VirDonneeTable.DonText = CacheDonnee.Item(NumInfo)
            End If
            IndiceI += 1
        Loop
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_Objet(0) = WsNumObjet
        V_NomTableSgbd = WsNomTable
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.V_SiEnLectureSeule = True Then
            CadreCmdOK.Visible = False
            Call InitialiserControles()
        End If
        Call LireLaFiche()
        If V_IndexFiche = -1 Then
            Call V_CommandeNewFiche()
        End If

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Cadre")
        CadreInfo.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelTitre.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Titre")
        LabelTitre.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Police_Claire")
        LabelTitre.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiAccident.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        EtiAccident.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiAccident.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiAssuranceMaladie.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        EtiAssuranceMaladie.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiAssuranceMaladie.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiChomage.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        EtiChomage.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiChomage.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiPrevoyance.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        EtiPrevoyance.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiPrevoyance.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiRetraite.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        EtiRetraite.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiRetraite.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        EtiRetraiteCompl.BackColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")
        EtiRetraiteCompl.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_ForeColor")
        EtiRetraiteCompl.BorderColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Bordure")
        LabelEuro.ForeColor = V_WebFonction.CouleurCharte(V_PointdeVue, "Etiquette_BackColor")

    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH01.ValeurChange, InfoH03.ValeurChange, InfoH03.ValeurChange, InfoH05.ValeurChange, InfoH10.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee.Item(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee.Item(NumInfo) <> e.Valeur Then
                    CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub Dontab_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) Handles Dontab02.AppelTable, _
        Dontab04.AppelTable, Dontab06.AppelTable, Dontab07.AppelTable, Dontab08.AppelTable, Dontab09.AppelTable

        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VDuoEtiquetteCommande).ID, 2))
        Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
        Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(e.ControleAppelant, e.ObjetAppelant, e.PointdeVueInverse, e.NomdelaTable)
        Call V_AppelTable(Evenement)
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            Dim NumInfo As Integer
            Dim Ctl As Control
            Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
            Dim CacheDonnee As List(Of String) = V_CacheMaj

            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab" & Strings.Right(IDAppelant, 2), 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonText = value
            If CacheDonnee IsNot Nothing Then
                If CacheDonnee.Item(NumInfo) Is Nothing Then
                    VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                Else
                    If CacheDonnee.Item(NumInfo) <> value Then
                        VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                        CadreCmdOK.Visible = True
                    End If
                End If
                Call V_ValeurChange(NumInfo, value)
            End If
        End Set
    End Property

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Call V_MajFiche()
        CadreCmdOK.Visible = False
    End Sub

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            VirControle.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            VirDonneeTable.DonBackColor = Drawing.Color.White
            VirDonneeTable.DonText = ""
            VirDonneeTable.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
    End Sub

End Class
