﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class PerDiplomes
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsDossierPer As Virtualia.Net.Individuel.DossierIndividu

    Private ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Private ReadOnly Property V_Contexte As Virtualia.Net.Individuel.LocalNavigation
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
        End Get
    End Property

    Private Sub PerDiplomes_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If HPopupRef.Value = "1" Then
            CellReference.Visible = True
            PopupReferentiel.Show()
        Else
            CellReference.Visible = False
            Call LireLaFiche()
            Call ControlDonnee()
        End If
    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NoLigne As Integer
        Dim Ctl As Control
        Dim IndiceI As Integer = 0
        Dim SiLectureOnly As Boolean = False

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerDiplome, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            NumInfo = VirDonneeTable.V_Information
            NoLigne = CInt(Strings.Mid(VirDonneeTable.ID, 8, 1))
            VirDonneeTable.DonText = ValeurLue(NumInfo, NoLigne - 1)
            VirDonneeTable.V_SiEnLectureSeule = SiLectureOnly
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerDiplome, "Donnee", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            NumInfo = VirDonneeDate.V_Information
            NoLigne = CInt(Strings.Mid(VirDonneeDate.ID, 8, 1))
            VirDonneeDate.DonText = ValeurLue(NumInfo, NoLigne - 1)
            VirDonneeDate.V_SiEnLectureSeule = SiLectureOnly
            VirDonneeDate.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

    End Sub

    Private Sub DonneeDate_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles DonneeL1_Date.ValeurChange, DonneeL2_Date.ValeurChange,
            DonneeL3_Date.ValeurChange, DonneeL4_Date.ValeurChange, DonneeL5_Date.ValeurChange

        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer.EnCours_Creation = True Then
            Exit Sub
        End If
        If WsDossierPer Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CType(sender, VCoupleEtiDate).V_Information
        Dim NumObjet As Integer = CType(sender, VCoupleEtiDate).V_Objet
        Dim NoLigne As Integer = CInt(Strings.Mid(CType(sender, VCoupleEtiDate).ID, 8, 1))
        If WsDossierPer.DonneeLue(NumObjet, NumInfo, NoLigne - 1) <> e.Valeur Then
            CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
            WsDossierPer.TableauMaj(NumObjet, NumInfo, NoLigne - 1) = e.Valeur
        End If
    End Sub

    Private Sub Dontab_AppelTable(sender As Object, e As AppelTableEventArgs) Handles DontabL1_01.AppelTable, DontabL1_02.AppelTable, DontabL1_03.AppelTable,
        DontabL2_01.AppelTable, DontabL2_02.AppelTable, DontabL2_03.AppelTable, DontabL3_01.AppelTable, DontabL3_02.AppelTable, DontabL3_03.AppelTable,
        DontabL4_01.AppelTable, DontabL4_02.AppelTable, DontabL4_03.AppelTable, DontabL5_01.AppelTable, DontabL5_02.AppelTable, DontabL5_03.AppelTable,
        DontabL1_04.AppelTable, DontabL2_04.AppelTable, DontabL3_04.AppelTable, DontabL4_04.AppelTable, DontabL5_04.AppelTable

        RefVirtualia.V_PointdeVue = e.PointdeVueInverse
        RefVirtualia.V_NomTable = e.NomdelaTable
        RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        HPopupRef.Value = "1"
        CellReference.Visible = True
        PopupReferentiel.Show()
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            WsDossierPer = V_Contexte.DossierPER
            If WsDossierPer.EnCours_Creation = True Then
                Exit Property
            End If
            If WsDossierPer Is Nothing Then
                Exit Property
            End If
            Dim Ctl As Control
            Dim NumInfo As Integer
            Dim NumObjet As Integer
            Dim NoLigne As Integer
            Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerDiplome, "Dontab" & Strings.Right(IDAppelant, 5), 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            NumInfo = VirDonneeTable.V_Information
            NumObjet = VirDonneeTable.V_Objet
            NoLigne = CInt(Strings.Mid(VirDonneeTable.ID, 8, 1))
            VirDonneeTable.DonText = value
            If WsDossierPer.DonneeLue(NumObjet, NumInfo, NoLigne - 1) <> value Then
                VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                WsDossierPer.TableauMaj(NumObjet, NumInfo, NoLigne - 1) = value
            End If
        End Set
    End Property

    Protected Sub Referentiel_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles RefVirtualia.RetourEventHandler
        HPopupRef.Value = "0"
        CellReference.Visible = False
    End Sub

    Protected Sub Referentiel_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) Handles RefVirtualia.ValeurSelectionnee
        HPopupRef.Value = "0"
        CellReference.Visible = False
        Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoInfo As Integer, ByVal NoRang As Integer) As String
        Get
            If WsDossierPer Is Nothing And V_Contexte IsNot Nothing Then
                WsDossierPer = V_Contexte.DossierPER
                If WsDossierPer Is Nothing Then
                    Return ""
                End If
            End If
            Return WsDossierPer.DonneeLue(VI.ObjetPer.ObaDiplome, NoInfo, NoRang)
        End Get
    End Property



    'Private WriteOnly Property TableauMaj(ByVal NumInfo As Integer, ByVal NoRang As Integer) As String
    '    Set(value As String)
    '        If WsDossierPer Is Nothing Then
    '            Try
    '                WsDossierPer = V_Contexte.DossierPER
    '            Catch ex As Exception
    '                Exit Property
    '            End Try
    '            If WsDossierPer Is Nothing Then
    '                Exit Property
    '            End If
    '        End If
    '        Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENFANT) = WsDossierPer.ListeEnfants
    '        Dim ConstructeurFiche As Virtualia.TablesObjet.ShemaPER.VConstructeur
    '        Dim FicheMAJ As Virtualia.Systeme.MetaModele.VIR_FICHE
    '        Dim TableauData As ArrayList

    '        If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
    '            ConstructeurFiche = New Virtualia.TablesObjet.ShemaPER.VConstructeur(V_WebFonction.PointeurGlobal.VirModele.InstanceProduit.ClefModele)
    '            FicheMAJ = ConstructeurFiche.V_NouvelleFiche(VI.ObjetPer.ObaEnfant, WsDossierPer.Identifiant)
    '            LstFiches.Add(CType(FicheMAJ, Virtualia.TablesObjet.ShemaPER.PER_ENFANT))
    '        ElseIf NoRang > LstFiches.Count - 1 Then
    '            ConstructeurFiche = New Virtualia.TablesObjet.ShemaPER.VConstructeur(V_WebFonction.PointeurGlobal.VirModele.InstanceProduit.ClefModele)
    '            FicheMAJ = ConstructeurFiche.V_NouvelleFiche(VI.ObjetPer.ObaEnfant, WsDossierPer.Identifiant)
    '            LstFiches.Add(CType(FicheMAJ, Virtualia.TablesObjet.ShemaPER.PER_ENFANT))
    '        Else
    '            FicheMAJ = LstFiches.Item(NoRang)
    '        End If
    '        TableauData = FicheMAJ.V_TableauData
    '        TableauData(NumInfo) = value
    '        FicheMAJ.V_TableauData = TableauData
    '        WsDossierPer.ListeEnfants = LstFiches
    '    End Set
    'End Property


    Private Sub ControlDonnee()
        DonneeL1_Date.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        DonneeL1_Date.Attributes.Add("placeholder", "DD/MM/AAAA")
        DonneeL2_Date.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        DonneeL2_Date.Attributes.Add("placeholder", "DD/MM/AAAA")
        DonneeL3_Date.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        DonneeL3_Date.Attributes.Add("placeholder", "DD/MM/AAAA")
        DonneeL4_Date.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        DonneeL4_Date.Attributes.Add("placeholder", "DD/MM/AAAA")
        DonneeL5_Date.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        DonneeL5_Date.Attributes.Add("placeholder", "DD/MM/AAAA")





    End Sub

End Class