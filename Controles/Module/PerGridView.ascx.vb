﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes

Partial Class PerGridView
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsSiCaptionVisible As Boolean = True
    Private WsSiStyleBulletin As Boolean = False
    Private WsSiStyleReleve As Boolean = False
    Private WsSiStyleTempsTravail As Boolean = False
    Private WsSiStyleAlterne As Boolean = False
    Private WsNatureCoche As Integer = 0 'Valeur par Défaut Détail
    Private WsNomStateCache As String = "VListeGrid"
    Private WsCtl_Cache As Virtualia.Net.VCaches.CacheControleListe
    '
    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Private Property CacheVirControle As Virtualia.Net.VCaches.CacheControleListe
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.VCaches.CacheControleListe)
            End If
            Dim NewCache As Virtualia.Net.VCaches.CacheControleListe
            NewCache = New Virtualia.Net.VCaches.CacheControleListe
            Return NewCache
        End Get
        Set(value As Virtualia.Net.VCaches.CacheControleListe)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Public Property SiStyleAlterne As Boolean
        Get
            Return WsSiStyleAlterne
        End Get
        Set(ByVal value As Boolean)
            WsSiStyleAlterne = value
        End Set
    End Property

    Public Property SiStyleTempsTravail As Boolean
        Get
            Return WsSiStyleTempsTravail
        End Get
        Set(ByVal value As Boolean)
            WsSiStyleTempsTravail = value
        End Set
    End Property

    Public Property SiStyleBulletin As Boolean
        Get
            Return WsSiStyleBulletin
        End Get
        Set(ByVal value As Boolean)
            WsSiStyleBulletin = value
        End Set
    End Property

    Public Property SiStyleReleve As Boolean
        Get
            Return WsSiStyleReleve
        End Get
        Set(ByVal value As Boolean)
            WsSiStyleReleve = value
        End Set
    End Property

    Public Property SiCaptionVisible As Boolean
        Get
            Return WsSiCaptionVisible
        End Get
        Set(ByVal value As Boolean)
            WsSiCaptionVisible = value
        End Set
    End Property

    'Public Property SiCaseAcocher As Boolean
    '    Get
    '        Return CaseACocher.Visible
    '    End Get
    '    Set(ByVal value As Boolean)
    '        CaseACocher.Visible = value
    '        If value = False Then
    '            HSelCoche.Value = "1"
    '        End If
    '    End Set
    'End Property

    'Public Property TexteCaseACocher As String
    '    Get
    '        Return CaseACocher.Text
    '    End Get
    '    Set(ByVal value As String)
    '        CaseACocher.Text = value
    '    End Set
    'End Property

    Public Property NatureCaseACocher As Integer
        Get
            Return WsNatureCoche
        End Get
        Set(ByVal value As Integer)
            WsNatureCoche = value
        End Set
    End Property

    Public Property ValeurCoche As Boolean
        Get
            If HSelCoche.Value = "1" Then
                Return True
            Else
                Return False
            End If
        End Get
        Set(value As Boolean)
            If value = True Then
                HSelCoche.Value = "1"
            Else
                HSelCoche.Value = "0"
            End If
        End Set
    End Property

    Public Property SiColonneSelect As Boolean
        Get
            Return GridDonnee.AutoGenerateSelectButton
        End Get
        Set(ByVal value As Boolean)
            GridDonnee.AutoGenerateSelectButton = value
        End Set
    End Property

    Public Property CadreWidth As System.Web.UI.WebControls.Unit
        Get
            Return GridDonnee.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            GridDonnee.Width = value
        End Set
    End Property

    Public Property SiPagination As Boolean
        Get
            Return GridDonnee.AllowPaging
        End Get
        Set(ByVal value As Boolean)
            GridDonnee.AllowPaging = value
            If value = True Then
                GridDonnee.PagerSettings.Mode = PagerButtons.Numeric
                GridDonnee.PagerSettings.Position = PagerPosition.TopAndBottom
                If WebFct Is Nothing Then
                    WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
                End If
                GridDonnee.PagerStyle.BackColor = WebFct.ConvertCouleur("#OE5F5C")
                GridDonnee.PagerStyle.ForeColor = WebFct.ConvertCouleur("#D7FAF3")
            End If
        End Set
    End Property

    Public Property BackColorCaption As System.Drawing.Color
        Get
            Return GridDonnee.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            GridDonnee.BackColor = value
            GridDonnee.HeaderStyle.BackColor = value
        End Set
    End Property

    Public Property ForeColorCaption As System.Drawing.Color
        Get
            Return GridDonnee.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            GridDonnee.ForeColor = value
            GridDonnee.HeaderStyle.ForeColor = value
        End Set
    End Property

    Public Property BackColorRow As System.Drawing.Color
        Get
            Return GridDonnee.RowStyle.BackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            GridDonnee.RowStyle.BackColor = value
        End Set
    End Property

    Public Property ForeColorRow As System.Drawing.Color
        Get
            Return GridDonnee.RowStyle.ForeColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            GridDonnee.RowStyle.ForeColor = value
        End Set
    End Property

    Public Property TaillePage As Integer
        Get
            Return GridDonnee.PageSize
        End Get
        Set(ByVal value As Integer)
            GridDonnee.PageSize = value
        End Set
    End Property

    Public ReadOnly Property TotalLignes As Integer
        Get
            Dim VCache As List(Of String) = V_Liste
            If VCache Is Nothing Then
                Return 0
            End If
            Return VCache.Count - 1
        End Get
    End Property

    'Public Property CaseDetailCochee As Boolean
    '    Get
    '        Return CaseACocher.Checked
    '    End Get
    '    Set(ByVal value As Boolean)
    '        CaseACocher.Checked = value
    '        If value = True Then
    '            HSelCoche.Value = "1"
    '        Else
    '            HSelCoche.Value = "0"
    '        End If
    '    End Set
    'End Property

    Public WriteOnly Property Alignement As Integer
        Set(ByVal value As Integer)
            Dim IndiceI As Integer
            For IndiceI = 0 To GridDonnee.Columns.Count - 1
                Select Case value
                    Case 0 'Left
                        GridDonnee.Columns.Item(IndiceI).ItemStyle.HorizontalAlign = HorizontalAlign.Left
                    Case 1 'Centré
                        GridDonnee.Columns.Item(IndiceI).ItemStyle.HorizontalAlign = HorizontalAlign.Center
                    Case 2 'Right
                        GridDonnee.Columns.Item(IndiceI).ItemStyle.HorizontalAlign = HorizontalAlign.Right
                End Select
            Next IndiceI
        End Set
    End Property

    Public Property Centrage_Colonne(ByVal Index As Integer) As Integer
        Get
            If Index > GridDonnee.Columns.Count Then
                Return 0
                Exit Property
            End If
            Select Case GridDonnee.Columns.Item(Index).ItemStyle.HorizontalAlign
                Case Is = HorizontalAlign.Left
                    Return 0
                Case Is = HorizontalAlign.Center
                    Return 1
                Case Is = HorizontalAlign.Right
                    Return 2
                Case Else
                    Return 0
            End Select
        End Get
        Set(ByVal value As Integer)
            If Index > GridDonnee.Columns.Count Then
                Exit Property
            End If
            Select Case value
                Case 0 'Left
                    GridDonnee.Columns.Item(Index).ItemStyle.HorizontalAlign = HorizontalAlign.Left
                Case 1 'Centré
                    GridDonnee.Columns.Item(Index).ItemStyle.HorizontalAlign = HorizontalAlign.Center
                Case 2 'Right
                    GridDonnee.Columns.Item(Index).ItemStyle.HorizontalAlign = HorizontalAlign.Right
            End Select
        End Set
    End Property

    Public Property ItemSelectionne As Integer
        Get
            Return GridDonnee.SelectedIndex
        End Get
        Set(value As Integer)
            GridDonnee.SelectedIndex = value
        End Set
    End Property

    Public Property V_Liste() As List(Of String)
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Liste_Valeurs
        End Get
        Set(ByVal value As List(Of String))
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Liste_Valeurs = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_LibelCaption() As List(Of String)
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Libelles_Caption
        End Get
        Set(ByVal value As List(Of String))
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Libelles_Caption = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_LibelColonne() As List(Of String)
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Libelles_Colonne
        End Get
        Set(ByVal value As List(Of String))
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Libelles_Colonne = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_ItemSelectionne() As String
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Valeur_ItemSelectionne
        End Get
        Set(ByVal value As String)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Valeur_ItemSelectionne = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property V_ValeurNulle As String
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Valeur_Nulle
        End Get
        Set(value As String)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Valeur_Nulle = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If GridDonnee.AllowPaging = True And GridDonnee.PageIndex > 0 Then
            Call FaireListe(GridDonnee.PageIndex)
        Else
            Call FaireListe(0)
        End If
    End Sub

    Protected Sub GridDonnee_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridDonnee.PageIndexChanging
        If e.NewPageIndex >= 0 And e.NewPageIndex < GridDonnee.PageCount Then
            Call FaireListe(e.NewPageIndex)
        End If
    End Sub

    Protected Sub GridDonnee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridDonnee.SelectedIndexChanged
        Try
            V_ItemSelectionne = GridDonnee.SelectedDataKey.Item(0).ToString

            Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
            Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(GridDonnee.SelectedDataKey.Item(0).ToString)
            Saisie_Change(Evenement)
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub FaireListe(ByVal NoPage As Integer)
        Dim LstValeurs As List(Of String)
        Dim LstLibels As List(Of String)
        Dim LstColonnes As List(Of String)
        Dim TableauCol(0) As String
        Dim TableauClef(0) As String
        Dim IndiceI As Integer
        Dim IndiceA As Integer
        Dim VClasseGrid As Virtualia.Systeme.Datas.ObjetIlistGrid
        Dim Datas As ArrayList
        Dim Cpt As Integer = 0
        WsCtl_Cache = CacheVirControle

        'Colonnes
        LstColonnes = WsCtl_Cache.Libelles_Colonne


        If LstColonnes IsNot Nothing Then
            For IndiceI = 0 To LstColonnes.Count - 1
                Select Case LstColonnes.Item(IndiceI)
                    Case "Clef"
                        TableauCol(0) = GridDonnee.Columns.Item(IndiceI).ToString
                        GridDonnee.DataKeyNames = TableauCol
                        GridDonnee.Columns.Item(IndiceI).Visible = False
                    Case Else
                        GridDonnee.Columns.Item(IndiceI).Visible = True
                        GridDonnee.Columns.Item(IndiceI).HeaderText = LstColonnes.Item(IndiceI)
                End Select
            Next IndiceI
            If IndiceI < GridDonnee.Columns.Count Then
                For IndiceA = IndiceI To GridDonnee.Columns.Count - 1
                    GridDonnee.Columns.Item(IndiceA).Visible = False
                Next IndiceA
            End If
        End If

        Datas = New ArrayList
        LstValeurs = WsCtl_Cache.Liste_Valeurs

        If LstValeurs IsNot Nothing Then
            For IndiceI = 0 To LstValeurs.Count - 1
                TableauCol = Strings.Split(LstValeurs.Item(IndiceI), VI.Tild, -1)
                Select Case HSelCoche.Value
                    Case "1"
                        Select Case WsNatureCoche
                            Case Is = 0 'Détail
                                VClasseGrid = New Virtualia.Systeme.Datas.ObjetIlistGrid
                                For IndiceA = 0 To TableauCol.Count - 1
                                    VClasseGrid.Valeur(IndiceA) = ValeurEditee(WsCtl_Cache.Valeur_Nulle, TableauCol(IndiceA))
                                Next IndiceA
                                Datas.Add(VClasseGrid)
                                Cpt += 1
                            Case Is = 1 'Uniquement les erreurs
                                If Strings.InStr(TableauCol(TableauCol.Count - 1), "_") > 0 Then
                                    TableauClef = Strings.Split(TableauCol(TableauCol.Count - 1), "_", -1)
                                    If IsNumeric(TableauClef(0)) Then
                                        If CInt(TableauClef(0)) >= 900000 Then
                                            VClasseGrid = New Virtualia.Systeme.Datas.ObjetIlistGrid
                                            For IndiceA = 0 To TableauCol.Count - 1
                                                VClasseGrid.Valeur(IndiceA) = TableauCol(IndiceA)
                                            Next IndiceA
                                            Datas.Add(VClasseGrid)
                                            Cpt += 1
                                        End If
                                    End If
                                End If
                        End Select
                    Case Else
                        Select Case WsNatureCoche
                            Case Is = 0 'Pas de détail
                                If IsNumeric(TableauCol(TableauCol.Count - 1)) Then
                                    If CInt(TableauCol(TableauCol.Count - 1)) >= 100000 Then
                                        VClasseGrid = New Virtualia.Systeme.Datas.ObjetIlistGrid
                                        For IndiceA = 0 To TableauCol.Count - 1
                                            VClasseGrid.Valeur(IndiceA) = TableauCol(IndiceA)
                                        Next IndiceA
                                        Datas.Add(VClasseGrid)
                                        Cpt += 1
                                    End If
                                End If
                            Case Else
                                VClasseGrid = New Virtualia.Systeme.Datas.ObjetIlistGrid
                                For IndiceA = 0 To TableauCol.Count - 1
                                    VClasseGrid.Valeur(IndiceA) = ValeurEditee(WsCtl_Cache.Valeur_Nulle, TableauCol(IndiceA))
                                Next IndiceA
                                Datas.Add(VClasseGrid)
                                Cpt += 1
                        End Select
                End Select
            Next IndiceI
        Else
            VClasseGrid = New Virtualia.Systeme.Datas.ObjetIlistGrid
            Datas.Add(VClasseGrid)
        End If
        GridDonnee.DataSource = Datas
        GridDonnee.DataBind()
        If SiPagination = True Then
            GridDonnee.PageIndex = NoPage
        End If

        If SiCaptionVisible = True Then
            LstLibels = WsCtl_Cache.Libelles_Caption
            Select Case Cpt
                Case Is = 0
                    GridDonnee.Caption = LstLibels.Item(0)
                    GridDonnee.AutoGenerateSelectButton = False
                Case Is = 1
                    GridDonnee.Caption = LstLibels.Item(1)
                Case Else
                    GridDonnee.Caption = GridDonnee.Rows.Count.ToString & Space(1) & LstLibels.Item(2)
            End Select
        Else
            GridDonnee.Caption = ""
        End If

        If SiStyleTempsTravail = True Then
            Call StylerTempsTravail()
            Exit Sub
        End If

        If WsSiStyleBulletin = True Then
            Call StylerBulletin()
        ElseIf WsSiStyleReleve = True Then
            Call StylerReleve_Err()
        ElseIf WsSiStyleAlterne = True Then
            Call StylerAlternance()
        Else
            Select Case HSelCoche.Value
                Case "1"
                    Call StylerReleve()
            End Select
        End If
    End Sub

    Public Sub StylerBulletin()
        Dim IndiceI As Integer

        GridDonnee.GridLines = GridLines.Vertical
        GridDonnee.RowStyle.BackColor = Drawing.Color.White
        GridDonnee.AlternatingRowStyle.BackColor = Drawing.Color.White
        For IndiceI = 0 To GridDonnee.Rows.Count - 1
            Select Case GridDonnee.Rows.Item(IndiceI).Cells.Item(0).Text
                Case Is = "CUMULS", "NET", "TOTAUX"
                    GridDonnee.Rows.Item(IndiceI).Cells.Item(0).Font.Bold = True
                    GridDonnee.Rows.Item(IndiceI).Cells.Item(0).Font.Italic = True
                    GridDonnee.Rows.Item(IndiceI).BackColor = GridDonnee.BackColor
                    GridDonnee.Rows.Item(IndiceI).ForeColor = Drawing.Color.White
            End Select
        Next IndiceI
    End Sub

    Public Sub StylerReleve()
        Dim IndiceI As Integer

        GridDonnee.RowStyle.BackColor = Drawing.Color.White
        GridDonnee.AlternatingRowStyle.BackColor = Drawing.Color.White
        For IndiceI = 0 To GridDonnee.DataKeys.Count - 1
            If IsNumeric(GridDonnee.DataKeys.Item(IndiceI).Value) Then
                Select Case CInt(GridDonnee.DataKeys.Item(IndiceI).Value)
                    Case Is >= 100000
                        GridDonnee.Rows.Item(IndiceI).Font.Bold = True
                        GridDonnee.Rows.Item(IndiceI).Font.Italic = True
                        GridDonnee.Rows.Item(IndiceI).BackColor = GridDonnee.BackColor
                    Case Else
                        GridDonnee.Rows.Item(IndiceI).Font.Bold = False
                        GridDonnee.Rows.Item(IndiceI).Font.Italic = False
                End Select
            End If
        Next IndiceI
    End Sub

    Public Sub StylerTempsTravail()
        Dim IndiceI As Integer
        Dim IndiceK As Integer
        Dim ITip As Integer = 0
        Dim ICouleur As Integer = 0
        Dim TableauData(0) As String

        For IndiceI = 0 To GridDonnee.Columns.Count - 1
            If GridDonnee.Columns.Item(IndiceI).HeaderText = "Tip" Then
                ITip = IndiceI
            End If
            If GridDonnee.Columns.Item(IndiceI).HeaderText = "Couleur" Then
                ICouleur = IndiceI
                Exit For
            End If
        Next IndiceI
        If ITip = 0 Or ICouleur = 0 Then
            Exit Sub
        End If

        For IndiceI = 0 To GridDonnee.Rows.Count - 1
            GridDonnee.RowStyle.BackColor = Drawing.Color.White
            GridDonnee.AlternatingRowStyle.BackColor = Drawing.Color.White
            If GridDonnee.Rows.Item(IndiceI).Cells.Item(ITip).Text <> "" Then
                TableauData = Strings.Split(HttpUtility.HtmlDecode(GridDonnee.Rows.Item(IndiceI).Cells.Item(ITip).Text), "$", -1)
                For IndiceK = 0 To TableauData.Count - 1
                    GridDonnee.Rows.Item(IndiceI).Cells.Item(IndiceK + 3).ToolTip = TableauData(IndiceK)
                Next IndiceK
            End If
            If GridDonnee.Rows.Item(IndiceI).Cells.Item(ICouleur).Text <> "" Then
                TableauData = Strings.Split(HttpUtility.HtmlDecode(GridDonnee.Rows.Item(IndiceI).Cells.Item(ICouleur).Text), "$", -1)
                For IndiceK = 0 To TableauData.Count - 1
                    If TableauData(IndiceK) <> "" Then
                        GridDonnee.Rows.Item(IndiceI).Cells.Item(IndiceK + 3).BackColor = WebFct.ConvertCouleur(TableauData(IndiceK))
                    Else
                        GridDonnee.Rows.Item(IndiceI).Cells.Item(IndiceK + 3).BackColor = Drawing.Color.White
                    End If
                Next IndiceK
            End If
        Next IndiceI
        GridDonnee.Columns.Item(ITip).Visible = False
        GridDonnee.Columns.Item(ICouleur).Visible = False
    End Sub

    Public Sub StylerReleve_Err()
        Dim IndiceI As Integer
        Dim TableauIde(0) As String

        GridDonnee.RowStyle.BackColor = Drawing.Color.White
        GridDonnee.AlternatingRowStyle.BackColor = Drawing.Color.White
        For IndiceI = 0 To GridDonnee.DataKeys.Count - 1
            If IsNumeric(GridDonnee.DataKeys.Item(IndiceI).Value) Then
                Select Case CInt(GridDonnee.DataKeys.Item(IndiceI).Value)
                    Case Is >= 900000
                        GridDonnee.Rows.Item(IndiceI).Font.Bold = True
                        GridDonnee.Rows.Item(IndiceI).Font.Italic = True
                        GridDonnee.Rows.Item(IndiceI).BackColor = GridDonnee.BackColor
                End Select
            Else
                TableauIde = Strings.Split(GridDonnee.DataKeys.Item(IndiceI).Value.ToString, "_", -1)
                If IsNumeric(TableauIde(0)) Then
                    Select Case CInt(TableauIde(0))
                        Case Is >= 900000
                            GridDonnee.Rows.Item(IndiceI).Font.Bold = True
                            GridDonnee.Rows.Item(IndiceI).Font.Italic = False
                            GridDonnee.Rows.Item(IndiceI).ForeColor = Drawing.Color.Red
                        Case Else
                            GridDonnee.Rows.Item(IndiceI).Font.Bold = False
                            GridDonnee.Rows.Item(IndiceI).Font.Italic = False
                            GridDonnee.Rows.Item(IndiceI).ForeColor = WebFct.ConvertCouleur("#142525")
                    End Select
                End If
            End If
        Next IndiceI
    End Sub

    Public Sub StylerAlternance()
        Dim IndiceI As Integer

        For IndiceI = 0 To GridDonnee.Rows.Count - 1
            GridDonnee.RowStyle.BackColor = Drawing.Color.White
            GridDonnee.AlternatingRowStyle.BackColor = Drawing.Color.White
            GridDonnee.Rows.Item(IndiceI).ForeColor = WebFct.ConvertCouleur("#142425")
            Select Case IndiceI Mod 2
                Case 0
                    GridDonnee.Rows.Item(IndiceI).BackColor = Drawing.Color.White
                Case 1
                    GridDonnee.Rows.Item(IndiceI).BackColor = WebFct.ConvertCouleur("#D7FAF3")
            End Select
        Next IndiceI
    End Sub

    'Private Sub CaseACocher_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CaseACocher.CheckedChanged
    '    If CaseACocher.Checked = True Then
    '        HSelCoche.Value = "1"
    '    Else
    '        HSelCoche.Value = "0"
    '    End If
    'End Sub

    Public ReadOnly Property V_Tableau(ByVal Separateur As String) As ArrayList
        Get
            Dim TabRes As New ArrayList
            Dim Chaine As New System.Text.StringBuilder
            Dim IndiceC As Integer
            Dim IndiceR As Integer
            Dim SiMultiple As Boolean = SiPagination

            For IndiceC = 0 To GridDonnee.Columns.Count - 1
                If GridDonnee.Columns.Item(IndiceC).Visible = False Then
                    Exit For
                End If
                Chaine.Append(HttpUtility.HtmlDecode(GridDonnee.Columns.Item(IndiceC).HeaderText) & Separateur)
            Next IndiceC
            TabRes.Add(Chaine.ToString)

            If SiMultiple = True Then
                SiPagination = False
                Call FaireListe(0)
            End If
            For IndiceR = 0 To GridDonnee.Rows.Count - 1
                Chaine.Clear()
                For IndiceC = 0 To GridDonnee.Columns.Count - 1
                    If GridDonnee.Columns.Item(IndiceC).Visible = False Then
                        Exit For
                    End If
                    Chaine.Append(HttpUtility.HtmlDecode(GridDonnee.Rows.Item(IndiceR).Cells.Item(IndiceC).Text) & Separateur)
                Next IndiceC
                TabRes.Add(Chaine.ToString)
            Next IndiceR

            SiPagination = SiMultiple

            Return TabRes
        End Get
    End Property

    Private Function ValeurEditee(ByVal ValBlanche As String, ByVal Source As String) As String
        If Source <> ValBlanche Then
            Return Source
        End If
        Return ""
    End Function


    Public Property IsVisible As Boolean
        Get
            Return GridDonnee.Visible
        End Get
        Set(value As Boolean)
            GridDonnee.Visible = value
        End Set
    End Property
End Class























'Private WebFct As Virtualia.Net.Controles.WebFonctions
'Private WsDossierPer As Virtualia.Net.Individuel.DossierIndividu
'Dim WsDataTable As New DataTable()

'Dim WsFirstName As String
'Dim WsLastName As String
'Private WsButtonClicked As Integer
'Dim WsDateNaissance As String
'Dim WsLieuNaissance As String
'Dim inter As Boolean = False
'Dim table As New DataTable()


'Private ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
'    Get
'        If WebFct Is Nothing Then
'            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
'        End If
'        Return WebFct
'    End Get
'End Property

'Private ReadOnly Property V_Contexte As Virtualia.Net.Individuel.LocalNavigation
'    Get
'        Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
'    End Get
'End Property

'Private Sub PerGridView_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender

'    'table.Rows.Add("17/12/1994", "AAA", "BBB", "CCC")
'    'grid.DataSource = table
'    'WsDossierPer = V_Contexte.DossierPER
'    ''CadrePerGridView.DataSource = table
'    'If CadrePerGridView.Rows.Count = 0 Then

'    '    InfoTable.Rows.Add(InfoTable.NewRow())
'    '    CadrePerGridView.DataSource = InfoTable
'    '    CadrePerGridView.DataBind()
'    '    CadrePerGridView.Rows(0).Cells.Clear()
'    '    CadrePerGridView.Rows(0).Cells.Add(New TableCell())
'    '    CadrePerGridView.Rows(0).Cells(0).ColumnSpan = InfoTable.Columns.Count
'    '    CadrePerGridView.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
'    'Else
'    '    'Dim tb As TextBox = New TextBox()
'    '    'tb.Text = DateNaissance
'    '    'Dim tbc = New TableCell()
'    '    'tbc.Controls.Add(tb)
'    '    'InfoTable.Rows.Add(InfoTable.NewRow())
'    '    'MAJ_Tab(InfoTable)
'    '    If WsDossierPer.buttonclicked > 1 Then
'    '        For IndiceI = 2 To WsDossierPer.buttonclicked
'    '            Ajout_ligne_vide(InfoTable)
'    '        Next
'    '    Else
'    '        Ajout_ligne(InfoTable)
'    '    End If
'    '    'CadrePerGridView.Rows(0).Cells.Clear()
'    '    'Ajout_ligne_vide(InfoTable)
'    '    CadrePerGridView.DataSource = InfoTable
'    gridd.DataBind()
'    '    'CadrePerGridView.Rows(0).Cells(0).Equals(tbc)
'    '    'CadrePerGridView.Rows(0).Cells(0).ColumnSpan = InfoTable.Columns.Count
'    '    'CadrePerGridView.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
'    'End If

'End Sub

''Protected Sub GridView1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles grid.SelectedIndexChanged

''End Sub

'Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

'    'PopulateGridview()

'End Sub
'Protected Sub PopulateGridview()

'    Dim dtbl = New DataTable()




'    If (InfoTable.Rows.Count > 0) Then

'        gridd.DataSource = InfoTable
'        gridd.DataBind()

'    Else
'        InfoTable.Rows.Add(InfoTable.NewRow())
'        gridd.DataSource = InfoTable
'        gridd.DataBind()
'        'CadrePerGridView.Rows(0).Cells.Clear()
'        'CadrePerGridView.Rows(0).Cells.Add(New TableCell())
'        'CadrePerGridView.Rows(0).Cells(0).ColumnSpan = InfoTable.Columns.Count
'        'CadrePerGridView.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center

'    End If

'End Sub
'Protected Sub CadrePerGridView_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)


'    'If (e.CommandName.Equals("AddNew")) Then
'    '    WsDossierPer = V_Contexte.DossierPER
'    '    WsDossierPer.buttonclicked += 1
'    '    Dim dRow As DataRow
'    '    dRow = InfoTable.NewRow
'    '    InfoTable.Rows.Add(dRow)
'    '    'InfoTable.AcceptChanges()
'    '    'Ajout_ligne_vide(InfoTable)
'    '    MAJ_Tab(InfoTable)
'    'End If
'    ''PopulateGridview()
'    ''Dim VirControle As System.Web.UI.WebControls.TextBox
'    ''Dim IndiceI = 0
'    ''Dim Ctl As Control

'    ''Do
'    ''    Ctl = V_WebFonction.VirWebControle(Me.CadrePerGridView, "txtFirstNameFooter", IndiceI)
'    ''    If Ctl Is Nothing Then
'    ''        Exit Do
'    ''    End If
'    ''    'NoLigne = CInt(Strings.Right(Ctl.ID, 1))
'    ''    VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
'    ''    Dim x = VirControle.Text
'    ''    'VirControle.Text = ValeurLue(1, NoLigne - 1)
'    ''    'VirControle.ReadOnly = SiLectureOnly
'    ''    VirControle.AutoPostBack = False
'    ''    IndiceI += 1
'    ''Loop

'End Sub

'Protected Sub MAJ_Tab(ByVal table As DataTable)
'    'Dim tbdn = New TextBox
'    'tbdn.Text = DateNaissance
'    'Dim tbln = New TextBox
'    'tbdn.Text = LieuNaissance
'    'Dim tbnom = New TextBox
'    'tbdn.Text = Nom
'    'Dim tbprenom = New TextBox
'    'tbdn.Text = Prenom
'    'table.Clear()
'    'table.Rows.Add(tbdn, tbln, tbnom, tbprenom)
'End Sub

'Protected Sub Ajout_ligne_vide(ByVal table As DataTable)
'    'Dim tbdn = New TextBox

'    'Dim tbln = New TextBox

'    'Dim tbnom = New TextBox

'    'Dim tbprenom = New TextBox
'    'table.Rows.Add(tbdn, tbln, tbnom, tbprenom)
'End Sub

'Protected Sub Ajout_ligne(ByVal table As DataTable)
'    'Dim tbdn = New TextBox
'    'tbdn.Text = WsDossierPer.DateNaissanceEnfant

'    'Dim tbln = New TextBox
'    'tbln.Text = WsDossierPer.LieuNEnfant
'    'Dim tbnom = New TextBox
'    'tbnom.Text = WsDossierPer.NomEnfant
'    'Dim tbprenom = New TextBox
'    'tbprenom.Text = WsDossierPer.PrenomEnfant
'    'table.Rows.Add(tbdn, tbln, tbnom, tbprenom)
'End Sub


'Protected Sub NomChange(ByVal sender As Object, ByVal e As EventArgs)
'    'WsDossierPer = V_Contexte.DossierPER
'    'WsDossierPer.NomEnfant = CType(sender, TextBox).Text
'    'inter = True

'End Sub
'Protected Sub PrenomChange(ByVal sender As Object, ByVal e As EventArgs)
'    'WsDossierPer = V_Contexte.DossierPER
'    'WsDossierPer.PrenomEnfant = CType(sender, TextBox).Text
'    'inter = True

'End Sub
'Protected Sub DateNaiChange(ByVal sender As Object, ByVal e As EventArgs)
'    'WsDossierPer = V_Contexte.DossierPER
'    'WsDossierPer.DateNaissanceEnfant = CType(sender, TextBox).Text

'    'inter = True

'End Sub
'Protected Sub LieuNaiChange(ByVal sender As Object, ByVal e As EventArgs)
'    'WsDossierPer = V_Contexte.DossierPER
'    'WsDossierPer.LieuNEnfant = CType(sender, TextBox).Text
'    'inter = True

'End Sub

'Public Property Nom As String
'    Get

'        If WsFirstName Is Nothing Then
'            Return ""
'        End If
'        Return WsFirstName
'    End Get
'    Set(value As String)
'        WsFirstName = value
'    End Set
'End Property


'Public Property DateNaissance As String
'    Get

'        If WsDateNaissance Is Nothing Then
'            Return ""
'        End If
'        Return WsDateNaissance
'    End Get
'    Set(value As String)
'        WsDateNaissance = value
'    End Set
'End Property


'Public Property LieuNaissance As String
'    Get

'        If WsLieuNaissance Is Nothing Then
'            Return ""
'        End If
'        Return WsLieuNaissance
'    End Get
'    Set(value As String)
'        WsLieuNaissance = value
'    End Set
'End Property

'Public Property Prenom As String
'    Get

'        If WsLastName Is Nothing Then
'            Return ""
'        End If
'        Return WsLastName
'    End Get
'    Set(value As String)
'        WsLastName = value
'    End Set
'End Property

'Public Property InfoTable As DataTable
'    Get

'        If WsDataTable Is Nothing Then
'            Return Nothing
'        End If
'        If WsDataTable.Columns.Count = 0 Then
'            WsDataTable.Columns.Add("DateNaissance", GetType(TextBox))
'            WsDataTable.Columns.Add("LieuNaissance", GetType(TextBox))
'            WsDataTable.Columns.Add("Nom", GetType(TextBox))
'            WsDataTable.Columns.Add("Prenom", GetType(TextBox))
'        End If
'        Return WsDataTable
'    End Get
'    Set(value As DataTable)
'        WsDataTable = value
'    End Set
'End Property
''Public Property buttonclicked As Integer
''    Get
''        'WsButtonClicked = WsCompteurButtonClicked
''        Return WsButtonClicked
''    End Get
''    Set(value As Integer)
''        WsButtonClicked = value
''    End Set
''End Property

'Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAjouter.Click
'    WsDossierPer = V_Contexte.DossierPER
'    Try
'        WsDossierPer.tableenfant.Columns.Add("DateNaissance", Type.GetType("System.String"))
'        WsDossierPer.tableenfant.Columns.Add("Nom", Type.GetType("System.String"))
'        WsDossierPer.tableenfant.Columns.Add("Prenom", Type.GetType("System.String"))
'        WsDossierPer.tableenfant.Columns.Add("LieuNaissance", Type.GetType("System.String"))

'    Catch ex As Exception

'    End Try


'    WsDossierPer.tableenfant.Rows.Add(TbDN.Text, tbNom.Text, tbPrenom.Text, tbLN.Text)
'    gridd.DataSource = WsDossierPer.tableenfant
'End Sub

'End Class


