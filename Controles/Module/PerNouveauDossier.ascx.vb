﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.Evenements

Public Class PerNouveauDossier
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsDossierPer As Virtualia.Net.Individuel.DossierIndividu
    Private WsNomTable As String = "PER_ETATCIVIL"
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
    Private WsNumObjet As Integer = VI.ObjetPer.ObaCivil
    Private WsParent As Individuel.EnsembleDossiers
    Private Ide_Dossier As Integer
    Private WsCtl_Cache As Virtualia.Net.VCaches.CacheWebControle
    Private WsDossier As Virtualia.Net.Individuel.DossierIndividu
    Private WsEnsembleDossier As Virtualia.Net.Individuel.EnsembleDossiers
    Private WsEnsemble As Virtualia.Net.Individuel.EnsembleDossiers


    Protected Sub PerNouveauDossier_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        'Dim Objetglobal = New Session.ObjetGlobal
        'Dim ensembledossier = New Individuel.EnsembleDossiers(Objetglobal)
        'Dim Dossiervide = New Individuel.DossierIndividu(ensembledossier)
        'V_NomTableSgbd = WsNomTable
        'WsDossierPer = V_Contexte.DossierPER
        'Dim ensembledossier = New Individuel.EnsembleDossiers(Objetglobal)
        'Dim Dossiervide = New Individuel.DossierIndividu(ensembledossier)
        'Ide_Dossier = Dossiervide.getId
        If WsDossierPer Is Nothing Then
            If V_Contexte IsNot Nothing Then
                'V_ListeDossiers.Identifiant(False) = 0
                'If V_ListeDossiers.Count > 0 Then
                '    For Each dossier In V_ListeDossiers
                '        If dossier.FicheEtatCivil Is Nothing Then
                '            V_Contexte.DossierPER = dossier
                '        End If
                '    Next
                'End If
                If V_ListeDossiers.Item(0).ControleNvDossier = True Then
                    V_Contexte.DossierPER = V_ListeDossiers.ItemDossier(V_Identifiant)
                    V_Contexte.Identifiant_Courant = 0
                End If
                'If V_ListeDossiers.Count > 1 Then
                '    If V_ListeDossiers.ItemDossier(1).V_NvDossier = True Then
                '        V_Contexte.DossierPER = Dossiervide
                '    End If
                'End If
                WsDossierPer = V_Contexte.DossierPER
                WsDossierPer.EnCours_Creation = True
                'Ide_Dossier = V_Identifiant
            End If
        Else
            If InfoHA11.DonText = "" Then
                WsDossierPer.FicheEtatCivil.Numero_d_identification_nation = ""
                WsDossierPer.FicheEtatCivil.Cle_securite_sociale = ""
            End If
        End If

        'InfoHA02.ControlDonnee("nv_char")
        'InfoHA03.ControlDonnee("nv_char")
        'InfoHA11.ControlDonnee("nv_entier")
        TbDN.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        TbDN.Attributes.Add("placeholder", "DD/MM/AAAA")

    End Sub



    Private ReadOnly Property V_ListeDossiers As Virtualia.Net.Individuel.EnsembleDossiers
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).EnsemblePER
        End Get
    End Property

    Private ReadOnly Property V_Contexte As Virtualia.Net.Individuel.LocalNavigation
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
        End Get
    End Property

    Private Sub InfoH_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles InfoHA02.ValeurChange, InfoHA03.ValeurChange, InfoHA11.ValeurChange


        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleVerticalEtiDonnee).ID, 2))
        Dim NumObjet As Integer = CType(sender, Controles_VCoupleVerticalEtiDonnee).V_Objet
        V_Objet(0) = NumObjet
        If WsDossierPer.DonneeLue(NumObjet, NumInfo) <> e.Valeur Then
            CType(sender, Controles_VCoupleVerticalEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
            WsDossierPer.TableauMaj(NumObjet, NumInfo) = e.Valeur
            'Call V_ValeurChange(NumInfo, e.Valeur, CType(sender, Controles_VCoupleEtiDonnee).DonTabIndex)
        End If
    End Sub


    Private Sub InfoD_ValeurChange(sender As Object, ByVal e As EventArgs) Handles TbDN.TextChanged

        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer Is Nothing Then
            Exit Sub
        End If
        If WsDossierPer.DonneeLue(1, 4) <> TbDN.Text Then
            WsDossierPer.TableauMaj(1, 4) = TbDN.Text
            'WsDossierPer.FicheEtatCivil.Datation_de_la_naissance = TbDN.Text
        End If

    End Sub
    Private Function SiErreurSpecifique() As List(Of String)
        Dim TabErreurs As New List(Of String)
        Dim ChaineDoublon As List(Of Integer)
        Dim Chaine As String = ""
        Dim DossierPER As Virtualia.Ressources.Datas.ObjetDossierPER = Nothing



        'Ide_Dossier = WsDossierPer.getId
        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer.DonneeLue(1, 2) <> InfoHA02.DonText Then
            WsDossierPer.TableauMaj(1, 2) = InfoHA02.DonText
        End If
        If WsDossierPer.DonneeLue(1, 3) <> InfoHA03.DonText Then
            WsDossierPer.TableauMaj(1, 3) = InfoHA03.DonText
        End If
        If WsDossierPer.DonneeLue(1, 4) <> TbDN.Text Then
            WsDossierPer.TableauMaj(1, 4) = TbDN.Text
        End If
        If WsDossierPer.DonneeLue(1, 11) <> InfoHA11.DonText Then
            WsDossierPer.TableauMaj(1, 11) = InfoHA11.DonText
        End If

        DossierPER = V_WebFonction.PointeurGlobal.NouveauDossier(WsDossierPer.FicheEtatCivil.Nom, WsDossierPer.FicheEtatCivil.Prenom, WsDossierPer.FicheEtatCivil.Date_de_naissance)
        Ide_Dossier = DossierPER.V_Identifiant
        V_Contexte.Identifiant_Courant = Ide_Dossier
        'Call V_ValeurChange(4, TbDN.Text)

        If WsDossierPer.FicheEtatCivil.Numero_d_identification_nation <> "" Then
            Try
                Dim totalmodulo = Convert.ToDouble(WsDossierPer.FicheEtatCivil.Numero_d_identification_nation) Mod 97
                Dim numNir = 97 - totalmodulo
                WsDossierPer.TableauMaj(1, 12) = Convert.ToString(numNir)
            Catch ex As Exception
                TabErreurs.Add("Le numéro de sécurité sociale ne peut pas contenir des lettres.")
                Return TabErreurs
            End Try

            If WsDossierPer.FicheEtatCivil.Numero_d_identification_nation.Length <> 13 Then
                TabErreurs.Add("Le numéro de sécurité sociale doit contenir 13 chiffres.")
                Return TabErreurs
            End If

            Dim nir_annee As String = WsDossierPer.FicheEtatCivil.Numero_d_identification_nation.Substring(1, 2)
            Dim date_annee As String = Right(DossierPER.Date_de_Naissance, 2)
            If nir_annee <> date_annee Then
                TabErreurs.Add("Votre numéro de sécurité sociale ne correspond pas avec votre date de naissance.")
                Return TabErreurs
            End If
            If WsDossierPer.FicheEtatCivil.Numero_d_identification_nation.Substring(3, 2) <> DossierPER.Date_de_Naissance.Substring(3, 2) Then
                TabErreurs.Add("Votre numéro de sécurité sociale ne correspond pas avec votre date de naissance.")
                Return TabErreurs
            End If
        End If
        If V_Contexte.DossierPER IsNot Nothing Then
            Chaine = V_Contexte.DossierPER.FicheEtatCivil.Qualite
            Chaine &= Strings.Space(1) & V_Contexte.DossierPER.FicheEtatCivil.Nom
            Chaine &= Strings.Space(1) & V_Contexte.DossierPER.FicheEtatCivil.Prenom
            Chaine &= Strings.Space(1) & V_Contexte.DossierPER.FicheEtatCivil.Date_de_naissance
            Chaine &= " (Ide V : " & Ide_Dossier & ")"
        End If



        Dim Dossier As Virtualia.Ressources.Datas.ObjetDossierPER
        If Ide_Dossier > 0 Then
            Dossier = V_WebFonction.PointeurDossier(Ide_Dossier)
        Else
            Dossier = Nothing
        End If
        If DossierPER.Nom = "" Or DossierPER.Prenom = "" Or DossierPER.Date_de_Naissance = "" Then
            TabErreurs.Add("Le Nom, le prénom et la date de naissance sont obligatoires.")
            Return TabErreurs
        End If
        '** Mise en Forme Nom et Prénom
        DossierPER.Nom = V_WebFonction.ViRhFonction.Lettre1Capi(DossierPER.Nom, 2)
        'If V_WebFonction.PointeurConfiguration IsNot Nothing Then
        '    If V_WebFonction.PointeurConfiguration.Parametres_Saisie.Si_Nom_En_Majuscule = True Then
        '        CacheData.Item(2) = CacheData.Item(2).ToUpper
        '    End If
        'End If
        DossierPER.Prenom = V_WebFonction.ViRhFonction.Lettre1Capi(DossierPER.Prenom, 1)
        '** Contrôle Unicité Nom, Prénom, Date de naissance
        ChaineDoublon = V_WebFonction.PointeurGlobal.ControleDoublon(VI.PointdeVue.PVueApplicatif, Ide_Dossier,
                                                                     2, DossierPER.Nom, DossierPER.Prenom, DossierPER.Date_de_Naissance)
        If ChaineDoublon IsNot Nothing Then
                   TabErreurs.Add("Il existe déjà un dossier ayant la même identification sous le N° " & ChaineDoublon(0).ToString)
                   Return TabErreurs
               End If
        '** Contrôle Unicité N° NIR
        If WsDossierPer.FicheEtatCivil.Numero_d_identification_nation <> "" Then
            ChaineDoublon = V_WebFonction.PointeurGlobal.ControleDoublon(VI.PointdeVue.PVueApplicatif, Ide_Dossier, 11, WsDossierPer.FicheEtatCivil.Numero_d_identification_nation)
            If ChaineDoublon IsNot Nothing Then
                TabErreurs.Add("Il existe déjà un dossier ayant le même NIR sous le N° " & ChaineDoublon(0).ToString)
                Return TabErreurs
            End If
        End If
        'If V_IndexFiche <> -1 Then




        If Ide_Dossier > 0 Then
            If WsDossierPer Is Nothing Then
                Return Nothing
            End If
            'WsDossierPer = V_ListeDossiers.Item(0)
            If WsDossierPer.FicheEtatCivil.SiOK_DateNaissance(DossierPER.Date_de_Naissance) = False Then
                TabErreurs.Add("Date de naissance erronée.")
            End If
            If V_WebFonction.PointeurConfiguration IsNot Nothing Then
                If WsDossierPer.FicheEtatCivil.Numero_d_identification_nation <> "" Then
                    Dim totalmodulo = Convert.ToDouble(WsDossierPer.FicheEtatCivil.Numero_d_identification_nation) Mod 97
                    Dim numNir = 97 - totalmodulo
                    If Convert.ToString(numNir) <> "" Then

                        If V_WebFonction.PointeurConfiguration.Parametres_Saisie.Si_Controle_NIR = True Then
                            If WsDossierPer.FicheEtatCivil.SiOK_NIR(WsDossierPer.FicheEtatCivil.Numero_d_identification_nation, Convert.ToString(numNir)) = False Then
                                TabErreurs.Add("Numéro d'Identification NIR erronée ou Clef NIR erronée.")
                            End If
                        End If
                    End If
                End If
            Else
                Dim totalmodulo = Convert.ToDouble(WsDossierPer.FicheEtatCivil.Numero_d_identification_nation) Mod 97
                Dim numNir = 97 - totalmodulo
                If WsFiche.SiOK_NIR(WsDossierPer.FicheEtatCivil.Numero_d_identification_nation, Convert.ToString(numNir)) = False Then
                    TabErreurs.Add("Numéro d'Identification NIR erronée ou Clef NIR erronée.")
                End If
            End If
               End If
               If TabErreurs.Count > 0 Then
                   Return TabErreurs
               Else
                   Return Nothing
               End If
        'Else
        'Return Nothing
        'End If
    End Function
    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnValider.Click

        Dim TableauErreur As List(Of String) = SiErreurSpecifique()
        Dim TabMsg As New List(Of String)
        'Dim Cretour As Boolean
        LabelErreur.Text = ""


        If WsDossierPer.Count = 0 Then
            Exit Sub
        End If
        WsDossierPer.V_NvDossier = True

        If TableauErreur Is Nothing Then
            WsDossierPer.TableErreur = False
        ElseIf TableauErreur IsNot Nothing Then
            For Each Element In TableauErreur
                LabelErreur.Text += Element + "<br/>"
                'ModalPopupExtender1.Show()
            Next
            WsDossierPer.TableErreur = True
        Else
            If WsDossierPer Is Nothing Then
                Exit Sub
            End If

        End If

    End Sub

    Protected Sub BtnAnnuler_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAnnuler.Click

        For Each dossier In V_ListeDossiers
            If dossier.FicheEtatCivil IsNot Nothing Then
                If V_ListeDossiers.Item(0).Nom_NV = dossier.FicheEtatCivil.Nom And V_ListeDossiers.Item(0).Prenom_NV = dossier.FicheEtatCivil.Prenom And V_ListeDossiers.Item(0).DN_NV = dossier.FicheEtatCivil.Date_de_naissance Then
                    V_Contexte.DossierPER = dossier
                End If
            End If
        Next
        V_ListeDossiers.ItemDossier(0).Clear()
        LabelErreur.Text = ""
        'WsDossierPer.EnCours_Creation = False
        V_ListeDossiers.Item(0).ControleNvDossier = False
        V_ListeDossiers.Item(0).creation_annuler = True
        'V_Contexte.DossierPER = V_ListeDossiers.ItemDossier(V_Identifiant)


    End Sub

End Class