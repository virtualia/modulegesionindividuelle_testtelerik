﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class PerFormation
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsDossierPer As Virtualia.Net.Individuel.DossierIndividu


    Private ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Private ReadOnly Property V_Contexte As Virtualia.Net.Individuel.LocalNavigation
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
        End Get
    End Property

    Private Sub PerFormation_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Call Actualiser()
    End Sub

    Private Sub Actualiser()
        Dim LstLibels As New List(Of String)
        LstLibels.Add("Aucune session effecuée")
        LstLibels.Add("Une session effectuée")
        LstLibels.Add("sessions effectuées")
        ListeGrille.V_LibelCaption = LstLibels

        Dim LstColonnes As New List(Of String)
        LstColonnes.Add("du")
        LstColonnes.Add("au")
        LstColonnes.Add("intitulé du stage")
        LstColonnes.Add("suivi")
        LstColonnes.Add("domaine")
        LstColonnes.Add("jours")
        LstColonnes.Add("Clef")
        ListeGrille.V_LibelColonne = LstColonnes

        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer Is Nothing Then
            ListeGrille.V_Liste = Nothing
            Exit Sub
        End If
        Dim LstSuivis As List(Of String) = Nothing
        Dim Chaine As System.Text.StringBuilder
        Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaPER.PER_SESSION) =
            V_WebFonction.ViRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_SESSION)(WsDossierPer.Liste_Fiches("PER_SESSION"))

        If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
            LstSuivis = (From instance In LstFiches Order By instance.Suite_donnee Select instance.Suite_donnee).Distinct.ToList
            If LstSuivis IsNot Nothing Then
                LstSuivis.Add("")
            End If
        End If
        ListeGrille.V_Filtres(LstLibels) = LstSuivis
        LstSuivis = New List(Of String)
        For Each FichePER In LstFiches
            Chaine = New System.Text.StringBuilder
            Chaine.Append(FichePER.Date_de_Valeur & VI.Tild)
            Chaine.Append(FichePER.Date_de_Fin & VI.Tild)
            Chaine.Append(FichePER.Intitule & VI.Tild)
            Chaine.Append(FichePER.Suite_donnee & VI.Tild)
            Chaine.Append(FichePER.Domaine & VI.Tild)
            Chaine.Append(FichePER.Duree_en_heures & VI.Tild)
            Chaine.Append(FichePER.Date_de_Valeur & FichePER.Rang_Session)
            LstSuivis.Add(Chaine.ToString)
        Next
        ListeGrille.V_Liste = LstSuivis
    End Sub
End Class