﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PerNouveauDossier.ascx.vb" Inherits="Virtualia.Net.PerNouveauDossier" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDate.ascx" TagName="VCoupleEtiDate" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>


    <script type="text/javascript">
        var $jqDate = jQuery('input[name="jqueryDate"]');

        //Bind keyup/keydown to the input
        $jqDate.bind('keyup', 'keydown', function (e) {

            //To accomdate for backspacing, we detect which key was pressed - if backspace, do nothing:
            if (e.which !== 8) {
                var numChars = $jqDate.val().length;
                if (numChars === 2 || numChars === 5) {
                    var thisVal = $jqDate.val();
                    thisVal += '/';
                    $jqDate.val(thisVal);
                }
            }
        });
    </script>
    <style type="text/css">
        .roundedcorner {
            font-size: 11pt;
            margin-left: auto;
            margin-right: auto;
            margin-top: 1px;
            margin-bottom: 1px;
            padding: 3px;
            border-top: 1px solid;
            border-left: 1px solid;
            border-right: 1px solid;
            border-bottom: 1px solid;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
        }

        .titre {
            font-size: 25px;
            margin-top: 1px;
            margin-bottom: 1px;
            margin-left: 100px;
            padding: 3px;
            text-align: center;
        }

        .background {
            background-color: black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .popup {
            background-color: #fff;
            border-width: 3px;
            border-color: black;
            border-style: solid;
            padding-top: 10px;
            padding-left: 50px;
            width: 440px;
            height: 600px;
        }

        .popup2 {
            background-color: #E2F5F1;
            border-width: 3px;
            border-color: black;
            border-style: solid;
            padding-top: 10px;
            padding-left: 10px;
            width: 400px;
            height: 200px;
        }
        .divlabel {
            display: inline-block;
        }
        .btncancel {
            display: none;
        }
        .LabelErreur {
            font-size: 15px;
            color: black;
            text-align:center;
            padding-right: 30px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 1px;
            margin-bottom: 1px;
        }
    </style>
</head>
 
<body>


        <table >
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="Etiquette" runat="server" Text="Créer un nouveau dossier" Height="20px" Width="200px"
                        BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#D7FAF3"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        Style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoHA02" runat="server" EtiText="Nom d'usage ou nom usuel"
                        EtiTooltip="Il s'agit du nom d'usage. Le plus souvent il s'agit du nom de naissance ou du nom du conjoint pour les personnes mariées"
                        V_PointdeVue="1" V_Objet="1" V_Information="2" V_SiDonneeDico="true"
                        EtiWidth="170px" DonWidth="210px" DonTabIndex="2" />
                    <br />


                </td>
            </tr>
            <tr>
                <td>

                    <asp:TableCell Width="330px" HorizontalAlign="Left">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoHA03" runat="server" EtiText="Prénom d'usage"
                            V_PointdeVue="1" V_Objet="1" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="120px" DonWidth="210px" DonTabIndex="4" />
                    </asp:TableCell>
                    <br />


                </td>
            </tr>
            <tr>
                <td>

                    <asp:Table ID="VStandard" runat="server" CellPadding="1" CellSpacing="0">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Label Text="Date de naissance" ID="Label1" runat="server" Height="20px" Width="120px"
                                    BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                                    BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                    Style="margin-top: 0px; margin-left: 4px; text-indent: 5px; text-align: left"></asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="TbDN" runat="server" Height="16px" Width="210px" MaxLength="0"
                                    BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset"
                                    BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                    Style="margin-top: 1px; text-indent: 1px; text-align: left" TextMode="SingleLine">
                                </asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <ajaxToolkit:CalendarExtender ID="Calendar1" PopupButtonID="imgPopup" runat="server" TargetControlID="TbDN" Format="dd/MM/yyyy"></ajaxToolkit:CalendarExtender>
                    <br />
                    <br />


                </td>
            </tr>
            <tr>
                <td>
                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoHA11" runat="server"
                        V_PointdeVue="1" V_Objet="1" V_Information="11" V_SiDonneeDico="true"
                        EtiWidth="240px" DonWidth="210px" DonTabIndex="9" />
                    <br />


                </td>
            </tr>
            <tr>
                <asp:Label runat="server"></asp:Label>
            </tr>
            <tr>
                <td>
                    <asp:Table ID="CadreCmdOK" runat="server" HorizontalAlign="Left" Style="margin-top: 3px; margin-right: 3px">
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Bottom">
                                <telerik:RadButton ID="BtnValider" runat="server" Text="Valider" 
                                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#000" Skin="Silk" EnableEmbeddedSkins="true"
                                    Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Font-Italic="true"
                                    BorderStyle="None" Style="margin-left: 6px; text-align: Right;">
                                    <Icon PrimaryIconCssClass="rbOk" PrimaryIconBottom="8px"  />
                                </telerik:RadButton>
                                    
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <asp:Table ID="Table1" runat="server" HorizontalAlign="Left" Style="margin-top: 3px; margin-right: 3px">
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Bottom">
                                <telerik:RadButton ID="BtnAnnuler" runat="server" Text="Annuler" 
                                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#000" Skin="Silk" EnableEmbeddedSkins="true"
                                    Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Font-Italic="true"
                                    BorderStyle="None" Style="margin-left: 6px; text-align: Right;" OnClientClick="this.close();">
                               <Icon PrimaryIconCssClass="rbCancel" PrimaryIconBottom="8px" />
                                </telerik:RadButton>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
                <td>
                    <telerik:RadButton Skin="Silk" CssClass="btncancel" ID="BtnCancel" runat="server" OnClientClick="this.close();" Font-Size="Larger"  />
                </td>
            </tr>


        </table>
        <br /><br /><br />
        <table>

            <tr>
                <td>
        <asp:Label HorizontalAlign="Left" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" ID="LabelErreur" runat="server" Style="color: red; text-align:left"></asp:Label>
                    </td>
            </tr>
                </table>
</body>
</html>
