﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PerEnfants.ascx.vb" Inherits="Virtualia.Net.PerEnfants" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .Std_Etiquette
    {  
        background-color:#B0E0D7;
        color:#142425;
        border-color:#B0E0D7;
        border-style:outset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:5px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        height:20px;
    }
     .Std_Donnee
    {  
        background-color:white;
        color:black;
        border-color:#B0E0D7;
        border-style:inset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:1px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        height:16px;
    }
</style>
<asp:Table ID="CadrePerEnfant" runat="server" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545" Width="710px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitre" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiDate" runat="server" CssClass="Std_Etiquette" Width="125px" Text="Date de naissance" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiPrenom" runat="server" CssClass="Std_Etiquette" Width="185px" Text="Prénom usuel" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiNom" runat="server" CssClass="Std_Etiquette" Width="200px" Text="Nom de famille" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiSexe" runat="server" CssClass="Std_Etiquette" Width="200px" Text="Sexe de l'enfant" >
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="Row_ligne_01">
        <asp:TableCell>
            <asp:Table ID="Ligne_01" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="Date_L1" runat="server" TypeCalendrier="Standard" DontabIndex="21"
                            V_SiDonneeDico="false" EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:10px" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Prenom_L1" runat="server" CssClass="Std_Donnee" Width="180px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Nom_L1" runat="server" CssClass="Std_Donnee" Width="200px" TextMode="SingleLine" TabIndex="23"> 
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Radio_L1" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true" V_Groupe="SexeEnfant_L1" V_TabIndex="24"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Masculin" RadioCentreText="Féminin" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Acharge_L1" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true"  V_TabIndex="48"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow Visible="True" ID="Row_ligne_02">
        <asp:TableCell>
            <asp:Table ID="Ligne_02" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="Date_L2" runat="server" TypeCalendrier="Standard" DontabIndex="25"
                            V_SiDonneeDico="false" EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:10px" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Prenom_L2" runat="server" CssClass="Std_Donnee" Width="180px" TextMode="SingleLine" TabIndex="26">  
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Nom_L2" runat="server" CssClass="Std_Donnee" Width="200px" TextMode="SingleLine" TabIndex="27"> 
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Radio_L2" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true" V_Groupe="SexeEnfant_L2" V_TabIndex="28"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Masculin" RadioCentreText="Féminin" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Acharge_L2" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true"  V_TabIndex="48"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow Visible="True" ID="Row_ligne_03">
        <asp:TableCell>
            <asp:Table ID="Ligne_03" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="Date_L3" runat="server" TypeCalendrier="Standard" DontabIndex="29"
                            V_SiDonneeDico="false" EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:10px" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Prenom_L3" runat="server" CssClass="Std_Donnee" Width="180px" TextMode="SingleLine" TabIndex="30"> 
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Nom_L3" runat="server" CssClass="Std_Donnee" Width="200px" TextMode="SingleLine" TabIndex="31"> 
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Radio_L3" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true" V_Groupe="SexeEnfant_L3" V_TabIndex="32"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Masculin" RadioCentreText="Féminin" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Acharge_L3" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true"  V_TabIndex="48"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow Visible="True" ID="Row_ligne_04">
        <asp:TableCell>
            <asp:Table ID="Ligne_04" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="Date_L4" runat="server" TypeCalendrier="Standard" DontabIndex="33"
                            V_SiDonneeDico="false" EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:10px" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Prenom_L4" runat="server" CssClass="Std_Donnee" Width="180px" TextMode="SingleLine" TabIndex="34">
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Nom_L4" runat="server" CssClass="Std_Donnee" Width="200px" TextMode="SingleLine" TabIndex="35"> 
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Radio_L4" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true" V_Groupe="SexeEnfant_L4" V_TabIndex="36"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Masculin" RadioCentreText="Féminin" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Acharge_L4" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true"  V_TabIndex="48"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow Visible="False" ID="Row_ligne_05">
        <asp:TableCell>
            <asp:Table ID="Ligne_05" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="Date_L5" runat="server" TypeCalendrier="Standard" DontabIndex="37"
                            V_SiDonneeDico="false" EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:10px" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Prenom_L5" runat="server" CssClass="Std_Donnee" Width="180px" TextMode="SingleLine" TabIndex="38"> 
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Nom_L5" runat="server" CssClass="Std_Donnee" Width="200px" TextMode="SingleLine" TabIndex="39">
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Radio_L5" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true" V_Groupe="SexeEnfant_L5" V_TabIndex="40"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Masculin" RadioCentreText="Féminin" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Acharge_L5" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true" V_TabIndex="48"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow Visible="False" ID="Row_ligne_06">
        <asp:TableCell>
            <asp:Table ID="Ligne_06" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="Date_L6" runat="server" TypeCalendrier="Standard" DontabIndex="41"
                            V_SiDonneeDico="false" EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:10px"  />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Prenom_L6" runat="server" CssClass="Std_Donnee" Width="180px" TextMode="SingleLine" TabIndex="42"> 
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Nom_L6" runat="server" CssClass="Std_Donnee" Width="200px" TextMode="SingleLine" TabIndex="43"> 
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Radio_L6" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true" V_Groupe="SexeEnfant_L6" V_TabIndex="44"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Masculin" RadioCentreText="Féminin" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Acharge_L6" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true"  V_TabIndex="48" V_Groupe="Acharge_L6"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow Visible="False" ID="Row_ligne_07">
        <asp:TableCell>
            <asp:Table ID="Ligne_07" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="Date_L7" runat="server" TypeCalendrier="Standard" DontabIndex="45"
                            V_SiDonneeDico="false" EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:10px" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Prenom_L7" runat="server" CssClass="Std_Donnee" Width="180px" TextMode="SingleLine" TabIndex="46">  
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Nom_L7" runat="server" CssClass="Std_Donnee" Width="200px" TextMode="SingleLine" TabIndex="47"> 
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Radio_L7" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true" V_Groupe="SexeEnfant_L7"  V_TabIndex="48"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Masculin" RadioCentreText="Féminin" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Acharge_L7" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true"  V_TabIndex="48"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow Visible="False" ID="Row_ligne_08">
        <asp:TableCell>
            <asp:Table ID="Ligne_08" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="Date_L8" runat="server" TypeCalendrier="Standard" DontabIndex="45"
                            V_SiDonneeDico="false" EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:10px" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Prenom_L8" runat="server" CssClass="Std_Donnee" Width="180px" TextMode="SingleLine" TabIndex="46">  
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Nom_L8" runat="server" CssClass="Std_Donnee" Width="200px" TextMode="SingleLine" TabIndex="47"> 
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Radio_L8" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true" V_Groupe="SexeEnfant_L8"  V_TabIndex="48"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Masculin" RadioCentreText="Féminin" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Acharge_L8" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true" V_TabIndex="48"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow Visible="False" ID="Row_ligne_09">
        <asp:TableCell>
            <asp:Table ID="Ligne_09" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="Date_L9" runat="server" TypeCalendrier="Standard" DontabIndex="45"
                            V_SiDonneeDico="false" EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:10px" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Prenom_L9" runat="server" CssClass="Std_Donnee" Width="180px" TextMode="SingleLine" TabIndex="46">  
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Nom_L9" runat="server" CssClass="Std_Donnee" Width="200px" TextMode="SingleLine" TabIndex="47"> 
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Radio_L9" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true" V_Groupe="SexeEnfant_L9"  V_TabIndex="48"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Masculin" RadioCentreText="Féminin" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VTrioHorizontalRadio ID="Acharge_L9" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true" V_TabIndex="48"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText=""
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
 

 
