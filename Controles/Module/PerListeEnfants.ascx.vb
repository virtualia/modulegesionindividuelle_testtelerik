﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Partial Class PerListeEnfants
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsDossierPer As Virtualia.Net.Individuel.DossierIndividu

    Private ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Private ReadOnly Property V_Contexte As Virtualia.Net.Individuel.LocalNavigation
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
        End Get
    End Property

    Private Sub PerEnfants_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        'InfoHC05.ControlDonnee("entier")
        'Dim st As String
        'For Each s As String In Request.Params
        '    If s.Contains(st) Then st = Request(s)
        'Next
        InitialiserControles()
        If WsDossierPer Is Nothing Then
            Try
                WsDossierPer = V_Contexte.DossierPER
            Catch ex As Exception
            End Try
        End If

        AutoEnfant(CadrePerEnfant, myDropDown.SelectedIndex + 1)
        InitialiserControles()


        'If WsDossierPer IsNot Nothing Then
        '    WsDossierPer.AutoEnfant(CadrePerEnfant, myDropDown.SelectedIndex)
        'End If


        Call LireLaFiche()
    End Sub

    Private Sub LireLaFiche()
        Dim NoLigne As Integer
        Dim NoInfo As Integer
        Dim Ctl As Control
        Dim IndiceI As Integer = 0
        Dim SiLectureOnly As Boolean = False

        Dim VirControle As System.Web.UI.WebControls.TextBox
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerEnfant, "Nom_", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NoLigne = CInt(Strings.Right(Ctl.ID, 1))
            VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
            VirControle.Text = ValeurLue(1, NoLigne - 1)
            VirControle.ReadOnly = SiLectureOnly
            VirControle.AutoPostBack = False
            IndiceI += 1
        Loop
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerEnfant, "Prenom_", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NoLigne = CInt(Strings.Right(Ctl.ID, 1))
            VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
            VirControle.Text = ValeurLue(2, NoLigne - 1)
            VirControle.ReadOnly = SiLectureOnly
            VirControle.AutoPostBack = False
            IndiceI += 1
        Loop
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerEnfant, "Lieu_", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NoLigne = CInt(Strings.Right(Ctl.ID, 1))
            VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
            VirControle.Text = ValeurLue(11, NoLigne - 1)
            VirControle.ReadOnly = SiLectureOnly
            VirControle.AutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerEnfant, "Date_", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NoLigne = CInt(Strings.Right(Ctl.ID, 1))
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            VirDonneeDate.DonText = ValeurLue(0, NoLigne - 1)
            VirDonneeDate.V_SiEnLectureSeule = SiLectureOnly
            VirDonneeDate.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirDonneeD As TextBox
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerEnfant, "Dt_", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NoLigne = CInt(Strings.Right(Ctl.ID, 1))
            VirDonneeD = CType(Ctl, TextBox)
            VirDonneeD.Text = ValeurLue(0, NoLigne - 1)
            VirDonneeD.ReadOnly = SiLectureOnly
            VirDonneeD.AutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirRadioH As Controles_VTrioHorizontalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerEnfant, "Radio_", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NoInfo = CInt(Strings.Right(Ctl.ID, 2))
            NoLigne = CInt(Strings.Mid(Ctl.ID, 8, 1))
            VirRadioH = CType(Ctl, Controles_VTrioHorizontalRadio)
            VirRadioH.V_SiEnLectureSeule = SiLectureOnly
            VirRadioH.V_SiAutoPostBack = True
            Select Case NoInfo
                Case 3
                    Select Case ValeurLue(NoInfo, NoLigne - 1)
                        Case Is = "Masculin"
                            VirRadioH.RadioGaucheCheck = True
                        Case Is = "Féminin"
                            VirRadioH.RadioCentreCheck = True
                        Case Else
                            VirRadioH.RadioGaucheCheck = False
                            VirRadioH.RadioCentreCheck = False

                    End Select
                    IndiceI += 1
                Case 4
                    Select Case ValeurLue(NoInfo, NoLigne - 1)
                        Case Is = "Oui"
                            VirRadioH.RadioGaucheCheck = True
                        Case Is = "Non"
                            VirRadioH.RadioCentreCheck = True
                        Case Else
                            VirRadioH.RadioGaucheCheck = False
                            VirRadioH.RadioCentreCheck = False
                    End Select
                    IndiceI += 1
            End Select

        Loop

    End Sub

    Private Sub Date_ValeurChange(ByVal sender As Object, ByVal e As DonneeChangeEventArgs)

        Dim NoLigne As Integer = CInt(Strings.Right(CType(sender, VCoupleEtiDate).ID, 1))
        If ValeurLue(0, NoLigne - 1) <> e.Valeur Then
            CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
            TableauMaj(0, NoLigne - 1) = e.Valeur
        End If
    End Sub


    Private Sub Lieu_TextChanged(sender As Object, e As EventArgs)

        Dim NoLigne As Integer = CInt(Strings.Right(CType(sender, TextBox).ID, 1))
        If ValeurLue(11, NoLigne - 1) <> CType(sender, TextBox).Text Then
            TableauMaj(11, NoLigne - 1) = CType(sender, TextBox).Text
        End If
    End Sub



    Private Sub Nom_TextChanged(sender As Object, e As EventArgs)

        Dim NoLigne As Integer = CInt(Strings.Right(CType(sender, TextBox).ID, 1))
        If ValeurLue(1, NoLigne - 1) <> CType(sender, TextBox).Text Then
            TableauMaj(1, NoLigne - 1) = CType(sender, TextBox).Text
        End If
    End Sub



    Private Sub Prenom_TextChanged(sender As Object, e As EventArgs)

        Dim NoLigne As Integer = CInt(Strings.Right(CType(sender, TextBox).ID, 1))
        If ValeurLue(2, NoLigne - 1) <> CType(sender, TextBox).Text Then
            TableauMaj(2, NoLigne - 1) = CType(sender, TextBox).Text
        End If
    End Sub

    Private Sub dropdown_SelectedItemChanged(sender As Object, e As EventArgs)

        Dim NoLigne As Integer = CInt(Strings.Right(CType(sender, DropDownList).ID, 1))
        If ValeurLue(3, NoLigne - 1) <> CType(sender, DropDownList).SelectedValue Then
            TableauMaj(3, NoLigne - 1) = CType(sender, DropDownList).SelectedValue
        End If
    End Sub



    Private Sub Radio_ValeurChange(sender As Object, e As DonneeChangeEventArgs)

        Dim NoLigne As Integer = CInt(Strings.Mid(CType(sender, Controles_VTrioHorizontalRadio).ID, 8, 1))
        Dim NoInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VTrioHorizontalRadio).ID, 2))
        If ValeurLue(NoInfo, NoLigne - 1) <> e.Valeur Then
            TableauMaj(NoInfo, NoLigne - 1) = e.Valeur
        End If
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoInfo As Integer, ByVal NoRang As Integer) As String
        Get
            If WsDossierPer Is Nothing Then
                Try
                    WsDossierPer = V_Contexte.DossierPER
                Catch ex As Exception
                    Return ""
                End Try
                If WsDossierPer Is Nothing Then
                    Return ""
                End If
            End If
            Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENFANT) = WsDossierPer.ListeEnfants
            If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                Return ""
            End If
            If NoRang > LstFiches.Count - 1 Then
                Return ""
            End If
            Return LstFiches.Item(NoRang).V_TableauData(NoInfo).ToString
        End Get
    End Property

    'Protected Sub CommandeClear1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear_L1.Click, btnClear_L2.Click, btnClear_L3.Click, btnClear_L4.Click, btnClear_L5.Click

    '    Dim NoLigne As Integer = CInt(Strings.Right(CType(sender, Button).ID, 1))


    '    TableauMaj(0, NoLigne - 1) = ""
    '    TableauMaj(1, NoLigne - 1) = ""
    '    TableauMaj(2, NoLigne - 1) = ""
    '    TableauMaj(11, NoLigne - 1) = ""
    '    TableauMaj(3, NoLigne - 1) = ""
    '    TableauMaj(4, NoLigne - 1) = ""

    'End Sub



    Private WriteOnly Property TableauMaj(ByVal NumInfo As Integer, ByVal NoRang As Integer) As String
        Set(value As String)
            If WsDossierPer Is Nothing Then
                Try
                    WsDossierPer = V_Contexte.DossierPER
                Catch ex As Exception
                    Exit Property
                End Try
                If WsDossierPer Is Nothing Then
                    Exit Property
                End If
            End If
            Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaPER.PER_ENFANT) = WsDossierPer.ListeEnfants
            Dim ConstructeurFiche As Virtualia.TablesObjet.ShemaPER.VConstructeur
            Dim FicheMAJ As Virtualia.Systeme.MetaModele.VIR_FICHE
            Dim TableauData As ArrayList

            If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                ConstructeurFiche = New Virtualia.TablesObjet.ShemaPER.VConstructeur(V_WebFonction.PointeurGlobal.VirModele.InstanceProduit.ClefModele)
                FicheMAJ = ConstructeurFiche.V_NouvelleFiche(VI.ObjetPer.ObaEnfant, WsDossierPer.Identifiant)
                LstFiches.Add(CType(FicheMAJ, Virtualia.TablesObjet.ShemaPER.PER_ENFANT))
            ElseIf NoRang > LstFiches.Count - 1 Then
                ConstructeurFiche = New Virtualia.TablesObjet.ShemaPER.VConstructeur(V_WebFonction.PointeurGlobal.VirModele.InstanceProduit.ClefModele)
                FicheMAJ = ConstructeurFiche.V_NouvelleFiche(VI.ObjetPer.ObaEnfant, WsDossierPer.Identifiant)
                LstFiches.Add(CType(FicheMAJ, Virtualia.TablesObjet.ShemaPER.PER_ENFANT))
            Else
                FicheMAJ = LstFiches.Item(NoRang)
            End If
            TableauData = FicheMAJ.V_TableauData
            TableauData(NumInfo) = value
            FicheMAJ.V_TableauData = TableauData
            WsDossierPer.ListeEnfants = LstFiches
        End Set
    End Property

    Protected Sub ItemChange(ByVal sender As Object, ByVal e As EventArgs) Handles myDropDown.SelectedIndexChanged
        AutoEnfant(CadrePerEnfant, myDropDown.SelectedValue)
    End Sub


    Public Sub AutoEnfant(ByVal nomcadre As Table, ByVal IndiceI As Integer)
        For IndiceI = 0 To IndiceI - 1
            Dim rowcadre As TableRow = New TableRow()
            Dim cellcadre As TableCell = New TableCell()
            Dim tableligne As Table = New Table()
            tableligne.Attributes.Add("runat", "server")
            tableligne.Attributes.Add("CellPadding", "1")
            tableligne.Attributes.Add("CellSpacing", "0")
            Dim rowligne As TableRow = New TableRow()

            Dim cellligne1 As TableCell = New TableCell()
            Dim textboxdate As New TextBox() With {
                .AutoPostBack = True,
                .Width = 121,
                .TextMode = TextBoxMode.SingleLine,
                .ID = "dt_L" & IndiceI + 1
                }
            AddHandler textboxdate.TextChanged, AddressOf Date_ValeurChange
            textboxdate.AutoPostBack = True
            textboxdate.Width = 121
            textboxdate.Attributes.Add("TextMode", "SingleLine")
            textboxdate.ID = "dt_L" & IndiceI + 1
            cellligne1.Controls.Add(textboxdate)




            Dim cellligne2 As TableCell = New TableCell()
            Dim textboxlieu As TextBox = New TextBox()
            With textboxlieu
                .Visible = True
            End With
            textboxlieu.AutoPostBack = True
            textboxlieu.Width = 169
            textboxlieu.Attributes.Add("TextMode", "SingleLine")
            textboxlieu.ID = "Lieu_L" & IndiceI + 1
            cellligne2.Controls.Add(textboxlieu)

            'AddHandler textboxlieu.TextChanged, AddressOf OnTextChanged


            Dim cellligne3 As TableCell = New TableCell()
            Dim textboxprenom As TextBox = New TextBox()
            textboxprenom.Attributes.Add("runat", "server")
            textboxprenom.Attributes.Add("CssClass", "Std_Donnee")
            textboxprenom.Width = 170
            textboxprenom.Attributes.Add("TextMode", "SingleLine")
            textboxprenom.ID = "Prenom_L" & IndiceI + 1
            cellligne3.Controls.Add(textboxprenom)


            Dim cellligne4 As TableCell = New TableCell()
            Dim textboxnom As TextBox = New TextBox()
            textboxnom.Attributes.Add("runat", "server")
            textboxnom.Attributes.Add("CssClass", "Std_Donnee")
            textboxnom.Width = 170
            textboxnom.Attributes.Add("TextMode", "SingleLine")
            textboxnom.ID = "Nom_L" & IndiceI + 1
            cellligne4.Controls.Add(textboxnom)


            Dim cellligne5 As TableCell = New TableCell()
            Dim dropdownsexe As DropDownList = New DropDownList()
            dropdownsexe.Attributes.Add("runat", "server")
            dropdownsexe.Attributes.Add("AutoPostBack", "True")
            dropdownsexe.Width = 100
            Dim listitemmasculin As ListItem = New ListItem()
            listitemmasculin.Attributes.Add("Selected", "True")
            listitemmasculin.Attributes.Add("Value", "Masculin")
            listitemmasculin.Text = "Masculin"
            Dim listitemfeminin As ListItem = New ListItem()
            listitemfeminin.Attributes.Add("Value", "Féminin")
            listitemfeminin.Text = "Féminin"
            dropdownsexe.Items.Add(listitemmasculin)
            dropdownsexe.Items.Add(listitemfeminin)
            cellligne5.Controls.Add(dropdownsexe)
            AddHandler dropdownsexe.SelectedIndexChanged, AddressOf dropdown_SelectedItemChanged
            'CadrePerEnfant.Controls.Add(cellligne5)


            Dim cellligne6 As TableCell = New TableCell()
            Dim dropdowncharge As DropDownList = New DropDownList()
            dropdowncharge.Attributes.Add("runat", "server")
            dropdowncharge.Attributes.Add("AutoPostBack", "True")
            dropdowncharge.Width = 100
            Dim listitemoui As ListItem = New ListItem()
            listitemoui.Attributes.Add("Selected", "True")
            listitemoui.Attributes.Add("Value", "Oui")
            listitemoui.Text = "Oui"
            Dim listitemnon As ListItem = New ListItem()
            listitemnon.Attributes.Add("Value", "Non")
            listitemnon.Text = "Non"
            dropdowncharge.Items.Add(listitemoui)
            dropdowncharge.Items.Add(listitemnon)
            cellligne6.Controls.Add(dropdowncharge)

            rowligne.Cells.Add(cellligne1)
            rowligne.Cells.Add(cellligne2)
            rowligne.Cells.Add(cellligne3)
            rowligne.Cells.Add(cellligne4)
            rowligne.Cells.Add(cellligne5)
            rowligne.Cells.Add(cellligne6)

            tableligne.Rows.Add(rowligne)

            cellcadre.Controls.Add(tableligne)

            rowcadre.Cells.Add(cellcadre)
            nomcadre.Rows.Add(rowcadre)
            nomcadre.Visible = True
        Next
    End Sub




    Private Sub InitialiserControles()

        Dim Ctl As Control
        Dim VirControle As TextBox
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerEnfant, "dt_L", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, TextBox)
            AddHandler VirControle.TextChanged, AddressOf Date_ValeurChange
            IndiceI += 1
        Loop
    End Sub
End Class