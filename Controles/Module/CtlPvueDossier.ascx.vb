﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class CtlPvueDossier
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateIde As String = "MenuPERIde"

    Public Property V_Identifiant As Integer
        Get
            If Me.ViewState(WsNomStateIde) Is Nothing Then
                Return 0
            End If
            Return CType(Me.ViewState(WsNomStateIde), Integer)
        End Get
        Set(ByVal value As Integer)
            If value = 0 Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                If CType(Me.ViewState(WsNomStateIde), Integer) = value Then
                    Exit Property
                End If
                Me.ViewState.Remove(WsNomStateIde)
            End If
            Me.ViewState.Add(WsNomStateIde, value)
        End Set
    End Property

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    'Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
    '    If HPopupRef.Value = "1" Then
    '        CellReference.Visible = True
    '        PopupReferentiel.Show()
    '    Else
    '        CellReference.Visible = False
    '    End If
    'End Sub




    'Protected Sub PER_AppelTable(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs) _
    'Handles Per_InfoGen.AppelTable

    '    Select Case e.ObjetAppelant
    '        Case VI.ObjetPer.ObaCivil
    '            WebFct.PointeurContexte.SysRef_IDVueRetour = Per_InfoGen.ID
    '        Case Else
    '            Exit Sub
    '    End Select

    '    RefVirtualia.V_PointdeVue = e.PointdeVueInverse
    '    RefVirtualia.V_NomTable = e.NomdelaTable
    '    Select Case e.ObjetAppelant
    '        Case VI.ObjetPer.ObaCivil
    '            If e.ControleAppelant = "Dontab06" Then
    '                RefVirtualia.V_DuoTable(e.PointdeVueInverse, e.NomdelaTable, e.PointdeVueInverse) = "Pays"
    '            End If
    '            RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
    '    End Select
    '    RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
    '    HPopupRef.Value = "1"
    '    CellReference.Visible = True
    '    PopupReferentiel.Show()
    'End Sub

    'Protected Sub MsgVirtualia_ValeurRetour(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
    '    HPopupMsg.Value = "0"
    '    CellMessage.Visible = False
    '    Select Case e.Emetteur
    '        Case Is = "SuppFiche"
    '            Select Case e.NumeroObjet
    '                Case VI.ObjetPer.ObaEnfant
    '                    Per_InfoGen.V_RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
    '            End Select
    '        Case Is = "SuppDossier"
    '            Exit Sub
    '        Case Is = "MajDossier"
    '            Select Case e.NumeroObjet
    '                Case VI.ObjetPer.ObaCivil
    '                    Per_InfoGen.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
    '            End Select
    '    End Select
    'End Sub

    'Protected Sub MessageDialogue(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    'Handles Per_InfoGen.MessageDialogue
    '    HPopupMsg.Value = "1"
    '    CellMessage.Visible = True
    '    MsgVirtualia.AfficherMessage = e
    '    PopupMsg.Show()
    'End Sub

    'Protected Sub MessageSaisie(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) _
    'Handles Per_InfoGen.MessageSaisie
    '    HPopupMsg.Value = "0"
    '    CellMessage.Visible = False
    'End Sub

End Class