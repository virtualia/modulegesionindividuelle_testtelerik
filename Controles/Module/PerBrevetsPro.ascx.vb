﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class PerBrevetsPro
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsDossierPer As Virtualia.Net.Individuel.DossierIndividu

    Private ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property
    Private ReadOnly Property V_Contexte As Virtualia.Net.Individuel.LocalNavigation
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
        End Get
    End Property

    Private Sub PerBrevetsPro_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If HPopupRef.Value = "1" Then
            CellReference.Visible = True
            PopupReferentiel.Show()
        Else
            CellReference.Visible = False
            Call LireLaFiche()
            Call ControlDonnee()

            DonneeL1_Date.Si_DateDebutFin(DonneeL1_Date, DonneeL1_Datefin) = False
            DonneeL2_Date.Si_DateDebutFin(DonneeL2_Date, DonneeL2_Datefin) = False
            DonneeL3_Date.Si_DateDebutFin(DonneeL3_Date, DonneeL3_Datefin) = False
            DonneeL4_Date.Si_DateDebutFin(DonneeL4_Date, DonneeL4_Datefin) = False
            DonneeL5_Date.Si_DateDebutFin(DonneeL5_Date, DonneeL5_Datefin) = False

        End If

    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim NoLigne As Integer
        Dim Ctl As Control
        Dim IndiceI As Integer = 0
        Dim SiLectureOnly As Boolean = False

        Dim VirControle As Controles_VCoupleEtiDonnee
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerBrevetPro, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            NumInfo = VirControle.V_Information
            NoLigne = CInt(Strings.Mid(VirControle.ID, 7, 1))
            VirControle.DonText = ValeurLue(NumInfo, NoLigne - 1)
            VirControle.V_SiEnLectureSeule = SiLectureOnly
            VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerBrevetPro, "Dontab", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            NumInfo = VirDonneeTable.V_Information
            NoLigne = CInt(Strings.Mid(VirDonneeTable.ID, 8, 1))
            VirDonneeTable.DonText = ValeurLue(NumInfo, NoLigne - 1)
            VirDonneeTable.V_SiEnLectureSeule = SiLectureOnly
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerBrevetPro, "Donnee", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            NumInfo = VirDonneeDate.V_Information
            NoLigne = CInt(Strings.Mid(VirDonneeDate.ID, 8, 1))
            VirDonneeDate.DonText = ValeurLue(NumInfo, NoLigne - 1)
            VirDonneeDate.V_SiEnLectureSeule = SiLectureOnly
            VirDonneeDate.V_SiAutoPostBack = True
            IndiceI += 1
        Loop

    End Sub

    Private Sub InfoH_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles InfoHL1_02.ValeurChange, InfoHL1_03.ValeurChange,
            InfoHL1_02.ValeurChange, InfoHL1_03.ValeurChange, InfoHL2_02.ValeurChange, InfoHL2_03.ValeurChange, InfoHL3_02.ValeurChange, InfoHL3_03.ValeurChange,
            InfoHL4_02.ValeurChange, InfoHL4_03.ValeurChange, InfoHL5_02.ValeurChange, InfoHL5_03.ValeurChange


        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer.EnCours_Creation = True Then
            Exit Sub
        End If
        If WsDossierPer Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CType(sender, Controles_VCoupleEtiDonnee).V_Information
        Dim NumObjet As Integer = CType(sender, Controles_VCoupleEtiDonnee).V_Objet
        Dim NoLigne As Integer = CInt(Strings.Mid(CType(sender, Controles_VCoupleEtiDonnee).ID, 7, 1))
        If WsDossierPer.DonneeLue(NumObjet, NumInfo, NoLigne - 1) <> e.Valeur Then
            CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
            WsDossierPer.TableauMaj(NumObjet, NumInfo, NoLigne - 1) = e.Valeur
        End If
    End Sub

    Private Sub DonneeDate_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles DonneeL1_Date.ValeurChange, DonneeL2_Date.ValeurChange,
            DonneeL3_Date.ValeurChange, DonneeL4_Date.ValeurChange, DonneeL5_Date.ValeurChange, DonneeL1_Datefin.ValeurChange, DonneeL2_Datefin.ValeurChange,
            DonneeL3_Datefin.ValeurChange, DonneeL4_Datefin.ValeurChange, DonneeL5_Datefin.ValeurChange

        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer.EnCours_Creation = True Then
            Exit Sub
        End If
        If WsDossierPer Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CType(sender, VCoupleEtiDate).V_Information
        Dim NumObjet As Integer = CType(sender, VCoupleEtiDate).V_Objet
        Dim NoLigne As Integer = CInt(Strings.Mid(CType(sender, VCoupleEtiDate).ID, 8, 1))

        If WsDossierPer.DonneeLue(NumObjet, NumInfo, NoLigne - 1) <> e.Valeur Then
            CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
            WsDossierPer.TableauMaj(NumObjet, NumInfo, NoLigne - 1) = e.Valeur
        End If

        Select Case NoLigne
            Case 1
                DonneeL1_Date.Si_DateDebutFin(DonneeL1_Date, DonneeL1_Datefin) = True

                If DonneeL1_Date.DonText = "" Then
                    DonneeL1_Datefin.DonText = ""
                End If

            Case 2
                DonneeL2_Date.Si_DateDebutFin(DonneeL2_Date, DonneeL2_Datefin) = True
                If DonneeL2_Date.DonText = "" Then
                    DonneeL2_Datefin.DonText = ""
                End If

            Case 3
                DonneeL3_Date.Si_DateDebutFin(DonneeL3_Date, DonneeL3_Datefin) = True
                If DonneeL3_Date.DonText = "" Then
                    DonneeL3_Datefin.DonText = ""
                End If

            Case 4
                DonneeL4_Date.Si_DateDebutFin(DonneeL4_Date, DonneeL4_Datefin) = True

            Case 5
                DonneeL5_Date.Si_DateDebutFin(DonneeL5_Date, DonneeL5_Datefin) = True
        End Select

    End Sub

    Private WriteOnly Property VerifDateDebutFin(ByVal NoLigne As Integer) As Boolean
        Set(ByVal value As Boolean)
            Select Case NoLigne
                Case 1
                    DonneeL1_Datefin.DateDebut = DonneeL1_Date.DonText
                    DonneeL1_Datefin.V_SiEnable = value
                Case 2
                    DonneeL2_Datefin.DateDebut = DonneeL2_Date.DonText
                    DonneeL2_Datefin.V_SiEnable = value
                Case 3
                    DonneeL3_Datefin.DateDebut = DonneeL3_Date.DonText
                    DonneeL3_Datefin.V_SiEnable = value
                Case 4
                    DonneeL4_Datefin.DateDebut = DonneeL4_Date.DonText
                    DonneeL4_Datefin.V_SiEnable = value
                Case 5
                    DonneeL5_Datefin.DateDebut = DonneeL5_Date.DonText
                    DonneeL5_Datefin.V_SiEnable = value
            End Select
        End Set
    End Property

    Private Sub Dontab_AppelTable(sender As Object, e As AppelTableEventArgs) Handles DontabL1_01.AppelTable, DontabL2_01.AppelTable, DontabL3_01.AppelTable,
        DontabL4_01.AppelTable, DontabL5_01.AppelTable

        RefVirtualia.V_PointdeVue = e.PointdeVueInverse
        RefVirtualia.V_NomTable = e.NomdelaTable
        RefVirtualia.V_Appelant(e.ObjetAppelant) = e.ControleAppelant
        HPopupRef.Value = "1"
        CellReference.Visible = True
        PopupReferentiel.Show()
    End Sub

    Public WriteOnly Property Dontab_RetourAppelTable(ByVal IDAppelant As String) As String
        Set(ByVal value As String)
            WsDossierPer = V_Contexte.DossierPER
            If WsDossierPer.EnCours_Creation = True Then
                Exit Property
            End If
            If WsDossierPer Is Nothing Then
                Exit Property
            End If
            Dim Ctl As Control
            Dim NumInfo As Integer
            Dim NumObjet As Integer
            Dim NoLigne As Integer
            Dim VirDonneeTable As Controles_VDuoEtiquetteCommande
            Ctl = V_WebFonction.VirWebControle(Me.CadrePerBrevetPro, "Dontab" & Strings.Right(IDAppelant, 5), 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirDonneeTable = CType(Ctl, Controles_VDuoEtiquetteCommande)
            NumInfo = VirDonneeTable.V_Information
            NumObjet = VirDonneeTable.V_Objet
            NoLigne = CInt(Strings.Mid(VirDonneeTable.ID, 8, 1))
            VirDonneeTable.DonText = value
            If WsDossierPer.DonneeLue(NumObjet, NumInfo, NoLigne - 1) <> value Then
                VirDonneeTable.DonBackColor = V_WebFonction.CouleurMaj
                WsDossierPer.TableauMaj(NumObjet, NumInfo, NoLigne - 1) = value
            End If
        End Set
    End Property

    Protected Sub Referentiel_RetourEventHandler(ByVal sender As Object, ByVal e As System.EventArgs) Handles RefVirtualia.RetourEventHandler
        HPopupRef.Value = "0"
        CellReference.Visible = False
    End Sub

    Protected Sub Referentiel_ValeurSelectionnee(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs) Handles RefVirtualia.ValeurSelectionnee
        HPopupRef.Value = "0"
        CellReference.Visible = False
        Dontab_RetourAppelTable(e.ControleAppelant) = e.Valeur
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoInfo As Integer, ByVal NoRang As Integer) As String
        Get
            If WsDossierPer Is Nothing Then
                Try
                    WsDossierPer = V_Contexte.DossierPER
                Catch ex As Exception
                    Return ""
                End Try
                If WsDossierPer Is Nothing Then
                    Return ""
                End If
            End If
            Return WsDossierPer.DonneeLue(VI.ObjetPer.ObaCompetence, NoInfo, NoRang)
        End Get
    End Property

    Private Sub ControlDonnee()

        DonneeL1_Date.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        DonneeL1_Date.Attributes.Add("placeholder", "DD/MM/AAAA")
        DonneeL2_Date.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        DonneeL2_Date.Attributes.Add("placeholder", "DD/MM/AAAA")
        DonneeL3_Date.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        DonneeL3_Date.Attributes.Add("placeholder", "DD/MM/AAAA")
        DonneeL4_Date.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        DonneeL4_Date.Attributes.Add("placeholder", "DD/MM/AAAA")
        DonneeL5_Date.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        DonneeL5_Date.Attributes.Add("placeholder", "DD/MM/AAAA")

        DonneeL1_Datefin.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        DonneeL1_Datefin.Attributes.Add("placeholder", "DD/MM/AAAA")
        DonneeL2_Datefin.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        DonneeL2_Datefin.Attributes.Add("placeholder", "DD/MM/AAAA")
        DonneeL3_Datefin.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        DonneeL3_Datefin.Attributes.Add("placeholder", "DD/MM/AAAA")
        DonneeL4_Datefin.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        DonneeL4_Datefin.Attributes.Add("placeholder", "DD/MM/AAAA")
        DonneeL5_Datefin.Attributes.Add("onkeyup", "this.value=this.value.replace(/^(\d\d)(\d)$/g,'$1/$2').replace(/^(\d\d\/\d\d)(\d+)$/g,'$1/$2').replace(/[^\d\/]/g,'')")
        DonneeL5_Datefin.Attributes.Add("placeholder", "DD/MM/AAAA")

    End Sub
End Class