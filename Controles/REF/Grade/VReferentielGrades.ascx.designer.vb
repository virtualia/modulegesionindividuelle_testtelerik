﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VReferentielGrades
    
    '''<summary>
    '''Contrôle CadreReferentiel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreReferentiel As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreLettre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreLettre As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle ButtonA.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonA As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonB.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonB As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonC.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonC As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonD.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonD As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonE.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonE As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonF As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonG.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonG As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonH.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonH As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonI.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonI As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonJ.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonJ As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonK As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonL.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonL As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonM As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonN.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonN As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonO.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonO As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonP.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonP As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonQ.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonQ As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonR.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonR As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonS As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonT.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonT As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonU.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonU As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonV.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonV As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonW.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonW As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonX.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonX As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonY.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonY As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonZ.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonZ As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle ButtonTout.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ButtonTout As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CadreCmdOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmdOK As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CommandeOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeOK As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CommandeRetour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeRetour As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle UpdatePanelGestion.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelGestion As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Contrôle CadreControle.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreControle As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreRadio.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreRadio As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle RadioV0.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioV0 As Global.System.Web.UI.WebControls.RadioButton
    
    '''<summary>
    '''Contrôle RadioV1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioV1 As Global.System.Web.UI.WebControls.RadioButton
    
    '''<summary>
    '''Contrôle PanelTreeRef.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelTreeRef As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle TreeListeRef.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TreeListeRef As Global.System.Web.UI.WebControls.TreeView
    
    '''<summary>
    '''Contrôle EtiStatus.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiStatus As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Grille.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Grille As Global.Virtualia.Net.GRD_GRILLE
End Class
