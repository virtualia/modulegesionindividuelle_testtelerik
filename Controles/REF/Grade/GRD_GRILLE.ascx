﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GRD_GRILLE.ascx.vb" Inherits="Virtualia.Net.GRD_GRILLE" %>

<%@ Register Src="~/Controles/REF/Grade/GRD_ECHELON.ascx" TagName="VEchelon" TagPrefix="Virtualia" %>

<style type="text/css">
    .LEtiquette
    {  
        background-color:#B0E0D7;
        color:#122425;
        border-color:#B0E0D7;
        border-style:outset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:oblique;
        font-weight:bold;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        vertical-align:middle;
        height:20px;
    }
</style>

<asp:Table ID="CadreGrille" runat="server" BackColor="#5E9598" BorderColor="#B0E0D7" BorderStyle="None">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitre" runat="server" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreIdentite" runat="server">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="EtiTitre" runat="server" CssClass="LEtiquette" Width="140px" Visible="true" Text="Nom de la grille">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:TextBox ID="DonTitre" runat="server" CssClass="LEtiquette" Width="400px" Visible="true" MaxLength="120" Text="" BackColor="White" TextMode="SingleLine">
                                    </asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="EtiRevalorisation" runat="server" CssClass="LEtiquette" Width="140px" Visible="true" Text="Revalorisations">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:DropDownList ID="VComboDates" runat="server" Height="22px" Width="180px" AutoPostBack="true" BackColor="#D7FAF3" ForeColor="#122425"
                                            style="border-color: #7EC8BE; border-width: 2px; border-style: inset; display: table-cell; font-style: oblique; " >
                                    </asp:DropDownList>
                                </asp:TableCell>
                            </asp:TableRow>
                         </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreSousTitre" runat="server" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="EtiFiller" runat="server" BorderStyle="None" BackColor="Transparent" Width="22px" Visible="true" Text="">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="EtiEchelon" runat="server" CssClass="LEtiquette" Width="142px" Visible="true" Text="Echelon">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="EtiChevron" runat="server" CssClass="LEtiquette" Width="82px" Visible="true" Text="Chevron">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="EtiBrut" runat="server" CssClass="LEtiquette" Width="92px" Visible="true" Text="Indice brut">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="EtiMajoré" runat="server" CssClass="LEtiquette" Width="92px" Visible="true" Text="Indice majoré">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="EtiDureeMini" runat="server" CssClass="LEtiquette" Width="102px" Visible="true" Text="Durée Mini">
                                    </asp:Label>
                                </asp:TableCell>
                                 <asp:TableCell>
                                    <asp:Label ID="EtiDureeMaxi" runat="server" CssClass="LEtiquette" Width="102px" Visible="true" Text="Durée Maxi">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="EtiDureeMoyenne" runat="server" CssClass="LEtiquette" Width="102px" Visible="true" Text="Durée Moyenne">
                                    </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Label ID="EtiGroupe" runat="server" CssClass="LEtiquette" Width="82px" Visible="true" Text="Groupe">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon01" runat="server" Numero_Ligne="1"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon02" runat="server" Numero_Ligne="2"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon03" runat="server" Numero_Ligne="3"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon04" runat="server" Numero_Ligne="4"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon05" runat="server" Numero_Ligne="5"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon06" runat="server" Numero_Ligne="6"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon07" runat="server" Numero_Ligne="7"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon08" runat="server" Numero_Ligne="8"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon09" runat="server" Numero_Ligne="9"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon10" runat="server" Numero_Ligne="10"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon11" runat="server" Numero_Ligne="11"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon12" runat="server" Numero_Ligne="12"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon13" runat="server" Numero_Ligne="13"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon14" runat="server" Numero_Ligne="14"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon15" runat="server" Numero_Ligne="15"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon16" runat="server" Numero_Ligne="16"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon17" runat="server" Numero_Ligne="17"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon18" runat="server" Numero_Ligne="18"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon19" runat="server" Numero_Ligne="19"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="Echelon20" runat="server" Numero_Ligne="20"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
           <Virtualia:VEchelon ID="EchelonTotal" runat="server" Numero_Ligne="99" V_SiEnLectureSeule="true"></Virtualia:VEchelon>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
