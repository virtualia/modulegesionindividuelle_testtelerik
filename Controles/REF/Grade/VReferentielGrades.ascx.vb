﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class VReferentielGrades
    Inherits Virtualia.Net.Controles.SystemeReferences
    Public Delegate Sub EventHandler(ByVal sender As Object, ByVal e As EventArgs)
    Public Event RetourEventHandler As EventHandler
    Private Sub VReferentielGrades_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If V_ObjetAppelant = 2 Then
            Grille.V_SiEnLectureSeule = True
        End If
        If TreeListeRef.Nodes.Count = 0 Then
            Call ChargerGrades()
        End If
    End Sub
    Private Sub ChargerGrades()
        Dim Collgrades As Virtualia.Ressources.Datas.ObjetGradeGrille
        Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaREF.GRD_GRADE) = Nothing
        Dim FicheRef As Virtualia.TablesObjet.ShemaREF.GRD_GRADE
        Dim NouveauNoeud As TreeNode
        Dim Cpt As Integer = 0
        Dim PathExpand As String = ""

        Grille.Nom_Grade_Grille = ""
        Grille.Liste_Grilles = Nothing
        Collgrades = V_WebFonction.PointeurReferentiel.PointeurGradeGrilles
        If Collgrades Is Nothing Then
            EtiStatus.Text = "Aucune référence"
            Exit Sub
        End If
        Select Case V_SiTriOrganise
            Case False 'Alphabétique
                LstFiches = Collgrades.ListeDesGrades(V_Lettre, "")
                TreeListeRef.MaxDataBindDepth = 1
            Case True
                LstFiches = Collgrades.ListeDesGradesTries("", "")
                TreeListeRef.MaxDataBindDepth = 3
        End Select
        If LstFiches.Count = 0 Then
            EtiStatus.Text = "Aucune référence"
            Exit Sub
        End If
        Select Case V_SiTriOrganise
            Case False 'Alphabétique
                For Each FicheRef In LstFiches
                    NouveauNoeud = New TreeNode(FicheRef.Intitule, CStr(FicheRef.Ide_Dossier))
                    NouveauNoeud.ImageUrl = "~/Images/Armoire/FicheVerte.bmp"
                    NouveauNoeud.PopulateOnDemand = False
                    NouveauNoeud.ShowCheckBox = False
                    NouveauNoeud.SelectAction = TreeNodeSelectAction.Select
                    TreeListeRef.Nodes.Add(NouveauNoeud)
                    If FicheRef.Intitule = V_Valeur_Selectionnee Then
                        V_VirtuelPath = NouveauNoeud.ValuePath
                    End If
                    Cpt += 1
                Next
            Case True
                Dim NoeudN1 As TreeNode = Nothing
                Dim NoeudN2 As TreeNode = Nothing
                Dim NoeudN3 As TreeNode = Nothing
                Dim NoeudN4 As TreeNode = Nothing
                Dim RuptN1 As String = "(Vide)"
                Dim RuptN2 As String = "(Vide)"
                Dim RuptN3 As String = "(Vide)"
                Dim RuptN4 As String = "(Vide)"
                For Each FicheRef In LstFiches
                    Select Case FicheRef.Gestion
                        Case Is <> RuptN1
                            RuptN1 = FicheRef.Gestion
                            RuptN2 = "(Vide)"
                            RuptN3 = "(Vide)"
                            RuptN4 = "(Vide)"
                            NoeudN1 = V_AjouterNoeudValeur(TreeListeRef, Nothing, FicheRef.Gestion, FicheRef.Gestion)
                            NoeudN1.ImageUrl = "~/Images/Armoire/BleuStandardFermer16.bmp"
                    End Select
                    Select Case FicheRef.Categorie
                        Case Is <> RuptN2
                            RuptN2 = FicheRef.Categorie
                            RuptN3 = "(Vide)"
                            RuptN4 = "(Vide)"
                            NoeudN2 = V_AjouterNoeudValeur(TreeListeRef, NoeudN1, FicheRef.Categorie, RuptN1 & VI.Tild & FicheRef.Categorie)
                    End Select
                    Select Case FicheRef.Filiere
                        Case Is <> RuptN3
                            RuptN3 = FicheRef.Filiere
                            RuptN4 = "(Vide)"
                            NoeudN3 = V_AjouterNoeudValeur(TreeListeRef, NoeudN2, FicheRef.Filiere, RuptN1 & VI.Tild & RuptN2 & VI.Tild & FicheRef.Filiere)
                    End Select
                    Select Case FicheRef.Corps
                        Case Is <> RuptN4
                            RuptN4 = FicheRef.Corps
                            NoeudN4 = V_AjouterNoeudValeur(TreeListeRef, NoeudN3, FicheRef.Corps, RuptN1 & VI.Tild & RuptN2 & VI.Tild & RuptN3 & VI.Tild & FicheRef.Corps)
                    End Select
                    NouveauNoeud = V_AjouterNoeudValeur(TreeListeRef, NoeudN4, FicheRef.Intitule, CStr(FicheRef.Ide_Dossier))
                    NouveauNoeud.ImageUrl = "~/Images/Armoire/FicheVerte.bmp"
                    If FicheRef.Intitule = V_Valeur_Selectionnee Then
                        V_VirtuelPath = NouveauNoeud.ValuePath
                        PathExpand = NoeudN1.ValuePath
                    End If
                    Cpt += 1
                Next
        End Select

        Select Case V_SiTriOrganise
            Case True ' Tri organisé
                TreeListeRef.CollapseAll()
                If PathExpand <> "" Then
                    Try
                        NouveauNoeud = TreeListeRef.FindNode(PathExpand)
                        If NouveauNoeud IsNot Nothing Then
                            If NouveauNoeud.Parent IsNot Nothing Then
                                NouveauNoeud.Parent.Expand()
                            End If
                            NouveauNoeud.Expand()
                        End If
                    Catch ex As Exception
                        TreeListeRef.ExpandAll()
                    End Try
                End If
        End Select
        If V_VirtuelPath <> "" Then
            Try
                NouveauNoeud = TreeListeRef.FindNode(V_VirtuelPath)
                If NouveauNoeud IsNot Nothing Then
                    NouveauNoeud.Selected = True
                    Grille.Identifiant_Grade(V_Date_Selectionnee) = CInt(NouveauNoeud.Value)
                    Select Case V_SiTriOrganise
                        Case True ' Tri organisé
                            If NouveauNoeud.Parent IsNot Nothing Then
                                NouveauNoeud.Parent.Expand()
                            End If
                            If NouveauNoeud.Parent.Parent IsNot Nothing Then
                                NouveauNoeud.Parent.Parent.Expand()
                            End If
                            If NouveauNoeud.Parent.Parent.Parent IsNot Nothing Then
                                NouveauNoeud.Parent.Parent.Parent.Expand()
                            End If
                    End Select
                Else
                    Grille.Identifiant_Grade("") = 0
                End If
            Catch ex As Exception
                Grille.Identifiant_Grade("") = 0
                Exit Try
            End Try
        Else
            Grille.Identifiant_Grade("") = 0
        End If
        Select Case Cpt
            Case Is = 1
                EtiStatus.Text = "Une référence"
            Case Else
                EtiStatus.Text = Cpt.ToString & " références"
        End Select
    End Sub

    Protected Sub RadioCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioV0.CheckedChanged, RadioV1.CheckedChanged
        Select Case Strings.Right(CType(sender, System.Web.UI.WebControls.RadioButton).ID, 1)
            Case "0"
                V_SiTriOrganise = False
            Case Else
                V_SiTriOrganise = True
        End Select
        RadioV0.Font.Bold = False
        RadioV1.Font.Bold = False
        CType(sender, System.Web.UI.WebControls.RadioButton).Font.Bold = True
        V_Lettre = ""
        TreeListeRef.Nodes.Clear()
    End Sub

    Private Sub TreeListeRef_SelectedNodeChanged(sender As Object, e As EventArgs) Handles TreeListeRef.SelectedNodeChanged
        Dim SelIde As Integer = 0
        Dim SelText As String = ""
        If IsNumeric(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value) = False Then
            Exit Sub
        End If
        SelIde = CInt(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value)
        SelText = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Text
        V_Valeur_Selectionnee = SelText
        Grille.Identifiant_Grade(V_Date_Selectionnee) = SelIde
    End Sub

    Protected Sub BoutonLettre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButtonA.Click, ButtonB.Click, ButtonC.Click, ButtonD.Click, ButtonE.Click, ButtonF.Click, ButtonG.Click, ButtonH.Click, ButtonI.Click, ButtonJ.Click, ButtonK.Click, ButtonL.Click, ButtonM.Click, ButtonN.Click, ButtonO.Click, ButtonP.Click, ButtonQ.Click, ButtonR.Click, ButtonS.Click, ButtonT.Click, ButtonU.Click, ButtonV.Click, ButtonW.Click, ButtonX.Click, ButtonY.Click, ButtonZ.Click, ButtonTout.Click
        Call InitialiserBoutons()
        If CType(sender, Button).ID = "ButtonTout" Then
            V_Lettre = ""
        Else
            V_Lettre = Strings.Right(CType(sender, Button).ID, 1)
        End If
        CType(sender, Button).BackColor = V_WebFonction.ConvertCouleur("#D7FAF3")
        CType(sender, Button).ForeColor = V_WebFonction.ConvertCouleur("#124545")
        V_SiTriOrganise = False
        RadioV0.Checked = True
        RadioV0.Font.Bold = True
        RadioV1.Font.Bold = False
        V_Valeur_Selectionnee = ""
        TreeListeRef.Nodes.Clear()
    End Sub

    Private Sub InitialiserBoutons()
        Dim IndiceI As Integer = 0
        Dim Ctl As Control
        Do
            Ctl = V_WebFonction.VirWebControle(CadreLettre, "Button", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            CType(Ctl, Button).BackColor = V_WebFonction.ConvertCouleur("#124545")
            CType(Ctl, Button).ForeColor = V_WebFonction.ConvertCouleur("#D7FAF3")
            IndiceI += 1
        Loop
    End Sub

    Protected Sub CommandeRetour_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeRetour.Click
        RaiseEvent RetourEventHandler(Me, e)
    End Sub

    Private Sub CommandeOK_Click(sender As Object, e As EventArgs) Handles CommandeOK.Click
        If Grille.Fiche_Grade Is Nothing Then
            Exit Sub
        End If
        Dim ChaineSel As String = Grille.V_EchelonCourant
        Dim Evenement As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs(V_IdAppelant, 1, VI.PointdeVue.PVueGrades, V_NomTable, Grille.Fiche_Grade.Ide_Dossier, ChaineSel, "Select")
        V_Selection(Evenement)
    End Sub
End Class