﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VReferentielGrades.ascx.vb" Inherits="Virtualia.Net.VReferentielGrades" %>

<%@ Register Src="~/Controles/REF/Grade/GRD_GRILLE.ascx" TagName="VGrille" TagPrefix="Virtualia" %>

<style type="text/css">
    .LaLettre
    {  
        background-color:#124545;
        color:#D7FAF3;
        border-color:#B0E0D7;
        border-style:inset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        padding-left:2px;
        vertical-align:middle;
        word-wrap:normal;
        height:24px;
        width:24px;
    }
    .Loption
    {  
        background-color:#7D9F99;
        color:#E9FDF9;
        border-color:#B0E0D7;
        border-style:inset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:oblique;
        font-weight:bold;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        padding-left:2px;
        text-align:left;
        text-indent:5px;
        vertical-align:middle;
        word-wrap:normal;
        height:20px;
        width:220px;
    }
</style>

 <asp:Table ID="CadreReferentiel" runat="server" BackColor="#5E9598" BorderColor="#B0E0D7" BorderStyle="Ridge" BorderWidth="4px" Width="690px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreLettre" runat="server" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="ButtonA" runat="server" Text="A" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonB" runat="server" Text="B" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonC" runat="server" Text="C" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonD" runat="server" Text="D" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonE" runat="server" Text="E" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonF" runat="server" Text="F" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonG" runat="server" Text="G" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonH" runat="server" Text="H" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonI" runat="server" Text="I" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonJ" runat="server" Text="J" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonK" runat="server" Text="K" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonL" runat="server" Text="L" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonM" runat="server" Text="M" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonN" runat="server" Text="N" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonO" runat="server" Text="O" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonP" runat="server" Text="P" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonQ" runat="server" Text="Q" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonR" runat="server" Text="R" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonS" runat="server" Text="S" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonT" runat="server" Text="T" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonU" runat="server" Text="U" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonV" runat="server" Text="V" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonW" runat="server" Text="W" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonX" runat="server" Text="X" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonY" runat="server" Text="Y" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonZ" runat="server" Text="Z" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell Width="80px">
                        <asp:Button ID="ButtonTout" runat="server" Text="@" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                                BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                CellPadding="0" CellSpacing="0" Visible="true" >
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                            BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                            BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                         </asp:Button>
                                    </asp:TableCell>
                                </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="CommandeRetour" runat="server" Height="20px" Width="20px" BackColor="Transparent" ForeColor="Black"
                            BorderStyle="None" BorderColor="#216B68" BorderWidth="1px" Text=" x" ToolTip="Fermer"
                            Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Medium" Font-Italic="false"
                            style="margin-top:0px;text-indent:0px;text-align:center;vertical-align: middle" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:UpdatePanel ID="UpdatePanelGestion" runat="server">
                <ContentTemplate>
                    <asp:Table ID="CadreControle" runat="server" BorderStyle="None" BorderWidth="2px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Table ID="CadreRadio" runat="server" HorizontalAlign="Center">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:RadioButton ID="RadioV0" runat="server" Text="Liste alphabétique" AutoPostBack="true" GroupName="OptionV" CssClass="Loption" Width="435px" Checked="true"/>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:RadioButton ID="RadioV1" runat="server" Text="Liste organisée" AutoPostBack="true" GroupName="OptionV" CssClass="Loption" Width="435px"/>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Panel ID="PanelTreeRef" runat="server" Width="880px" Height="300px" BackColor="Snow"
                                    Wrap="true" Style="margin-top: 0px; vertical-align: top; overflow:auto; text-align: left">
                                    <asp:TreeView ID="TreeListeRef" runat="server" MaxDataBindDepth="2" BorderStyle="None" BorderWidth="2px" BorderColor="Snow" 
                                        ForeColor="#142425" ShowCheckBoxes="None" Width="775px" Height="290px" Font-Bold="False" NodeIndent="10" BackColor="Snow" 
                                        LeafNodeStyle-HorizontalPadding="8px" RootNodeStyle-Font-Bold="True" RootNodeStyle-ImageUrl="~/Images/Armoire/FicheBleue.bmp" 
                                        RootNodeStyle-Font-Italic="False" >
                                        <SelectedNodeStyle BackColor="#6C9690" BorderColor="#E3924F" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" />
                                    </asp:TreeView>
                                </asp:Panel>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="EtiStatus" runat="server" Height="16px" Width="880px" BackColor="#B0E0D7" Visible="true" BorderColor="#B0E0D7" BorderStyle="None" ForeColor="#124545" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Style="text-align:center" />
                                </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                             <asp:TableCell HorizontalAlign="Center">
                                <Virtualia:VGrille ID="Grille" runat="server"></Virtualia:VGrille>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>