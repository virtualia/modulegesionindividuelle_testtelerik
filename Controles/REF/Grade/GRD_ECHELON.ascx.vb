﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class GRD_ECHELON
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WsNoLigne As Integer
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public Property Numero_Ligne As Integer
        Get
            Return WsNoLigne
        End Get
        Set(value As Integer)
            WsNoLigne = value
            If value = 99 Then
                CommandeSelect.Visible = False
                DonEchelon.Width = New Unit(162)
                DonEchelon.BackColor = Drawing.Color.Transparent
                DonEchelon.BorderStyle = BorderStyle.None
                DonChevron.BackColor = Drawing.Color.Transparent
                DonChevron.BorderStyle = BorderStyle.None
                DonGroupe.BackColor = Drawing.Color.Transparent
                DonGroupe.BorderStyle = BorderStyle.None
            End If
        End Set
    End Property

    Public WriteOnly Property FicheEchelon As Virtualia.TablesObjet.ShemaREF.GRD_ECHELON
        Set(value As Virtualia.TablesObjet.ShemaREF.GRD_ECHELON)
            DonEchelon.Text = ""
            DonChevron.Text = ""
            DonIndiceBrut.Text = ""
            DonIndiceMajore.Text = ""
            DonDureeMini.Text = ""
            DonDureeMaxi.Text = ""
            DonDureeMoyenne.Text = ""
            DonGroupe.Text = ""
            If value Is Nothing OrElse value.AliasEchelon = "" Then
                Exit Property
            End If
            DonEchelon.Text = value.AliasEchelon
            DonChevron.Text = value.Chevron
            DonIndiceBrut.Text = value.IndiceBrut
            DonIndiceMajore.Text = value.IndiceMajore
            DonDureeMini.Text = value.DureeMini
            DonDureeMaxi.Text = value.DureeMaxi
            DonDureeMoyenne.Text = value.DureeMoyenne
            DonGroupe.Text = value.GroupeResidence
        End Set
    End Property
    Public Property V_BackColor As Drawing.Color
        Get
            Return DonEchelon.BackColor
        End Get
        Set(value As Drawing.Color)
            DonEchelon.BackColor = value
            DonChevron.BackColor = value
            DonIndiceBrut.BackColor = value
            DonIndiceMajore.BackColor = value
            DonDureeMini.BackColor = value
            DonDureeMaxi.BackColor = value
            DonDureeMoyenne.BackColor = value
            DonGroupe.BackColor = value
        End Set
    End Property
    Public Property V_SiEnLectureSeule As Boolean
        Get
            Return DonEchelon.ReadOnly
        End Get
        Set(value As Boolean)
            DonEchelon.ReadOnly = value
            DonChevron.ReadOnly = value
            DonIndiceBrut.ReadOnly = value
            DonIndiceMajore.ReadOnly = value
            DonDureeMini.ReadOnly = value
            DonDureeMaxi.ReadOnly = value
            DonDureeMoyenne.ReadOnly = value
            DonGroupe.ReadOnly = value
        End Set
    End Property

    Public Property V_Selection_Visible As Boolean
        Get
            Return CommandeSelect.Visible
        End Get
        Set(value As Boolean)
            CommandeSelect.Visible = value
        End Set
    End Property

    Public Property V_Chevron_Visible As Boolean
        Get
            Return DonChevron.Visible
        End Get
        Set(value As Boolean)
            DonChevron.Visible = value
        End Set
    End Property

    Public ReadOnly Property V_ChaineRef As String
        Get
            Dim Chaine As String = DonEchelon.Text & VI.PointVirgule & DonChevron.Text & VI.PointVirgule
            Chaine &= DonIndiceBrut.Text & VI.PointVirgule & DonIndiceMajore.Text
            Return Chaine
        End Get
    End Property

    Public Property Donnee(ByVal Nom As String) As String
        Get
            Select Case Nom
                Case "Brut"
                    Return DonIndiceBrut.Text
                Case "Chevron"
                    Return DonChevron.Text
                Case "Echelon"
                    Return DonEchelon.Text
                Case "Majoré"
                    Return DonIndiceMajore.Text
                Case "Mini"
                    Return DonDureeMini.Text
                Case "Maxi"
                    Return DonDureeMaxi.Text
                Case "Moyenne"
                    Return DonDureeMoyenne.Text
            End Select
            Return ""
        End Get
        Set(value As String)
            Select Case Nom
                Case "Brut"
                    DonIndiceBrut.Text = value
                Case "Chevron"
                    DonChevron.Text = value
                Case "Echelon"
                    DonEchelon.Text = value
                Case "Majoré"
                    DonIndiceMajore.Text = value
                Case "Mini"
                    DonDureeMini.Text = value
                Case "Maxi"
                    DonDureeMaxi.Text = value
                Case "Moyenne"
                    DonDureeMoyenne.Text = value
            End Select
        End Set
    End Property
    Private Sub CommandeSelect_Click(sender As Object, e As ImageClickEventArgs) Handles CommandeSelect.Click
        V_BackColor = V_WebFonction.ViRhFonction.ConvertCouleur("#B0E0D7")
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs = Nothing
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Select", CStr(WsNoLigne))
        Saisie_Change(Evenement)
    End Sub
    Private Sub Echelon_TextChanged(sender As Object, e As EventArgs) Handles DonEchelon.TextChanged
        If IsNumeric(DonEchelon.Text) = True Then
            If CInt(DonEchelon.Text) = 0 Or CInt(DonEchelon.Text) > 20 Then
                DonEchelon.Text = ""
            End If
        End If
    End Sub
    Private Sub Indice_TextChanged(sender As Object, e As EventArgs) Handles DonIndiceBrut.TextChanged, DonIndiceMajore.TextChanged
        If IsNumeric(CType(sender, TextBox).Text) = False Then
            CType(sender, TextBox).Text = ""
            Exit Sub
        End If
        If CInt(CType(sender, TextBox).Text) > 3000 Then
            CType(sender, TextBox).Text = ""
        End If
    End Sub
    Private Sub Dure_TextChanged(sender As Object, e As EventArgs) Handles DonDureeMini.TextChanged, DonDureeMaxi.TextChanged, DonDureeMoyenne.TextChanged
        Dim Chaine As String = CType(sender, TextBox).Text
        If Chaine.Length <> 6 Then
            CType(sender, TextBox).Text = ""
            Exit Sub
        End If
        If IsNumeric(Chaine) = False Then
            CType(sender, TextBox).Text = ""
            Exit Sub
        End If
        Dim AA As String = Strings.Left(Chaine, 2)
        Dim MM As String = Strings.Mid(Chaine, 3, 2)
        Dim JJ As String = Strings.Right(Chaine, 2)
        If CInt(AA) > 30 Then
            AA = "00"
            Chaine = "00" & MM & JJ
        End If
        If CInt(MM) > 11 Then
            MM = "00"
            Chaine = AA & "00" & JJ
        End If
        If CInt(JJ) > 30 Then
            Chaine = AA & MM & "00"
        End If
        CType(sender, TextBox).Text = Chaine
    End Sub
End Class