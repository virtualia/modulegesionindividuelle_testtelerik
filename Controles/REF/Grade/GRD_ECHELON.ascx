﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="GRD_ECHELON.ascx.vb" Inherits="Virtualia.Net.GRD_ECHELON" %>

<style type="text/css">
    .LaDonnee
    {  
        background-color:white;
        color:black;
        border-color:#B0E0D7;
        border-style:inset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:1px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        padding-left:2px;
        vertical-align:middle;
        word-wrap:normal;
        height:16px;
        width:120px;
    }
</style>

<asp:Table ID="LigneEchelon" runat="server" BackColor="#5E9598" BorderStyle="none">
    <asp:TableRow>
        <asp:TableCell>
            <asp:ImageButton ID="CommandeSelect" runat="server" Width="20px" Height="20px" BorderStyle="None" ImageUrl="~/Images/General/CarreUniversel.bmp" 
                            ImageAlign="Middle" style="margin-left: 1px;">
            </asp:ImageButton>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="DonEchelon" runat="server" CssClass="LaDonnee" Width="140px" MaxLength="20" ReadOnly="false" Visible="true" AutoPostBack="false" TextMode="SingleLine" TabIndex="1"> 
            </asp:TextBox>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="DonChevron" runat="server" CssClass="LaDonnee" Width="80px" MaxLength="15" ReadOnly="false" Visible="true" AutoPostBack="false" TextMode="SingleLine" TabIndex="2"> 
            </asp:TextBox>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="DonIndiceBrut" runat="server" CssClass="LaDonnee" Width="90px" MaxLength="4" ReadOnly="false" Visible="true" AutoPostBack="false" TextMode="SingleLine" TabIndex="2"> 
            </asp:TextBox>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="DonIndiceMajore" runat="server" CssClass="LaDonnee" Width="90px" MaxLength="4" ReadOnly="false" Visible="true" AutoPostBack="false" TextMode="SingleLine" TabIndex="2"> 
            </asp:TextBox>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="DonDureeMini" runat="server" CssClass="LaDonnee" Width="100px" MaxLength="6" ReadOnly="false" Visible="true" AutoPostBack="false" TextMode="SingleLine" TabIndex="3"> 
            </asp:TextBox>
        </asp:TableCell>
         <asp:TableCell>
            <asp:TextBox ID="DonDureeMaxi" runat="server" CssClass="LaDonnee" Width="100px" MaxLength="6" ReadOnly="false" Visible="true" AutoPostBack="false" TextMode="SingleLine" TabIndex="4"> 
            </asp:TextBox>
        </asp:TableCell>
         <asp:TableCell>
            <asp:TextBox ID="DonDureeMoyenne" runat="server" CssClass="LaDonnee" Width="100px" MaxLength="6" ReadOnly="false" Visible="true" AutoPostBack="false" TextMode="SingleLine" TabIndex="5"> 
            </asp:TextBox>
        </asp:TableCell>
         <asp:TableCell>
            <asp:TextBox ID="DonGroupe" runat="server" CssClass="LaDonnee" Width="80px" MaxLength="20" ReadOnly="false" Visible="true" AutoPostBack="false" TextMode="SingleLine" TabIndex="6"> 
            </asp:TextBox>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
