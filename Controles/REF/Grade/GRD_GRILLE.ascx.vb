﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class GRD_GRILLE
    Inherits System.Web.UI.UserControl
    Private WsPvue As Integer = VI.PointdeVue.PVueGrilles
    Private WsNumObjet As Integer = 1
    Private WsNomTable As String = "GRD_GRILLE"
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Public WriteOnly Property V_SiEnLectureSeule As Boolean
        Set(value As Boolean)
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.GRD_ECHELON
            Dim IndiceI As Integer = 0
            Do
                Ctl = V_WebFonction.VirWebControle(Me.CadreGrille, "Echelon", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                If TypeOf (Ctl) Is Virtualia.Net.GRD_ECHELON Then
                    VirControle = CType(Ctl, Virtualia.Net.GRD_ECHELON)
                    VirControle.V_SiEnLectureSeule = value
                End If
                IndiceI += 1
            Loop
        End Set
    End Property

    Public Property Fiche_Grade As Virtualia.TablesObjet.ShemaREF.GRD_GRADE
        Get
            If Session.Item("GradeCourant") Is Nothing Then
                Return Nothing
            End If
            Return CType(Session.Item("GradeCourant"), Virtualia.TablesObjet.ShemaREF.GRD_GRADE)
        End Get
        Set(value As Virtualia.TablesObjet.ShemaREF.GRD_GRADE)
            Dim LstGrilles As List(Of Virtualia.TablesObjet.ShemaREF.GRD_GRILLE)
            Dim Doublon As String = ""
            Session.Remove("GradeCourant")
            Session.Remove("GrilleCourante")
            Session.Remove("EchelonCourant")
            Session.Add("GradeCourant", value)
            VComboDates.Items.Clear()
            If value.Echelle_de_remuneration = "" Then
                Nom_Grade_Grille = value.Intitule.ToUpper
            Else
                Nom_Grade_Grille = value.Echelle_de_remuneration
            End If
            LstGrilles = value.ListedesGrilles
            If LstGrilles Is Nothing OrElse LstGrilles.Count = 0 Then
                Call InitialiserControles()
                Exit Property
            End If
            For Each Grille In LstGrilles
                VComboDates.Items.Add(New ListItem(Grille.Date_de_Valeur, Grille.Date_de_Valeur))
            Next
            If value.V_Tag = "" Then
                VComboDates.Text = LstGrilles(LstGrilles.Count - 1).Date_de_Valeur
                Fiche_Grille = LstGrilles(LstGrilles.Count - 1)
            Else
                Dim I As Integer
                For I = LstGrilles.Count - 1 To 0 Step -1
                    If LstGrilles(I).Date_Valeur_ToDate <= V_WebFonction.ViRhDates.DateTypee(value.V_Tag, True) Then
                        VComboDates.Text = LstGrilles(I).Date_de_Valeur
                        Fiche_Grille = LstGrilles(I)
                        Exit For
                    End If
                Next
            End If
        End Set
    End Property

    Public WriteOnly Property Identifiant_Grade(ByVal DateGrille As String) As Integer
        Set(value As Integer)
            If value = 0 Then
                Call InitialiserControles()
                Exit Property
            End If
            Dim FicheREF As Virtualia.TablesObjet.ShemaREF.GRD_GRADE
            Dim Collgrades As Virtualia.Ressources.Datas.ObjetGradeGrille = V_WebFonction.PointeurReferentiel.PointeurGradeGrilles
            If Collgrades Is Nothing Then
                Exit Property
            End If
            FicheREF = Collgrades.Fiche_Grade(value)
            If FicheREF Is Nothing Then
                Exit Property
            End If
            FicheREF.V_Tag = DateGrille
            Fiche_Grade = FicheREF
        End Set
    End Property
    Public Property Fiche_Grille As Virtualia.TablesObjet.ShemaREF.GRD_GRILLE
        Get
            If Session.Item("GrilleCourante") Is Nothing Then
                Return Nothing
            End If
            Return CType(Session.Item("GrilleCourante"), Virtualia.TablesObjet.ShemaREF.GRD_GRILLE)
        End Get
        Set(value As Virtualia.TablesObjet.ShemaREF.GRD_GRILLE)
            Dim LstEchelons As List(Of Virtualia.TablesObjet.ShemaREF.GRD_ECHELON)
            Dim FicheREF As Virtualia.TablesObjet.ShemaREF.GRD_ECHELON
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.GRD_ECHELON
            Dim IndiceI As Integer = 0

            Session.Remove("GrilleCourante")
            Session.Remove("EchelonCourant")
            Session.Add("GrilleCourante", value)
            Call InitialiserControles()
            LstEchelons = value.ListedesEchelons("Majoré")
            If LstEchelons Is Nothing OrElse LstEchelons.Count = 0 Then
                Exit Property
            End If
            For Each FicheREF In LstEchelons
                If FicheREF.AliasEchelon = "" Then
                    Exit For
                End If
                Ctl = V_WebFonction.VirWebControle(Me.CadreGrille, "Echelon", IndiceI)
                If Ctl IsNot Nothing AndAlso TypeOf (Ctl) Is Virtualia.Net.GRD_ECHELON Then
                    VirControle = CType(Ctl, Virtualia.Net.GRD_ECHELON)
                    VirControle.Visible = True
                    VirControle.FicheEchelon = FicheREF
                End If
                IndiceI += 1
            Next
            FicheREF = New Virtualia.TablesObjet.ShemaREF.GRD_ECHELON
            FicheREF.AliasEchelon = "Ind. moyens/Total durées"
            FicheREF.IndiceBrut = CStr(value.Brut_Moyen)
            FicheREF.IndiceMajore = CStr(value.Majore_Moyen)
            FicheREF.DureeMini = value.Total_Duree("Mini")
            FicheREF.DureeMaxi = value.Total_Duree("Maxi")
            FicheREF.DureeMoyenne = value.Total_Duree("Moyenne")
            EchelonTotal.Visible = True
            EchelonTotal.FicheEchelon = FicheREF
        End Set
    End Property

    Public Property Nom_Grade_Grille As String
        Get
            Return DonTitre.Text
        End Get
        Set(value As String)
            DonTitre.Text = value
        End Set
    End Property

    Public Property Liste_Grilles As List(Of String)
        Get
            If VComboDates.Items.Count = 0 Then
                Return Nothing
            End If
            Dim LstElements As List(Of String) = New List(Of String)
            For Each Element As ListItem In VComboDates.Items
                LstElements.Add(Element.Text)
            Next
            Return LstElements
        End Get
        Set(value As List(Of String))
            VComboDates.Items.Clear()
            If value Is Nothing OrElse value.Count = 0 Then
                Exit Property
            End If
            For Each Element In value
                VComboDates.Items.Add(New ListItem(Element, Element))
            Next
        End Set
    End Property

    Public Property V_EchelonCourant As String
        Get
            If Session.Item("EchelonCourant") Is Nothing Then
                Return Nothing
            End If
            Return Session.Item("EchelonCourant").ToString
        End Get
        Set(value As String)
            Session.Remove("EchelonCourant")
            Session.Add("EchelonCourant", value)
        End Set
    End Property
    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Virtualia.Net.GRD_ECHELON
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreGrille, "Echelon", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            If TypeOf (Ctl) Is Virtualia.Net.GRD_ECHELON Then
                VirControle = CType(Ctl, Virtualia.Net.GRD_ECHELON)
                VirControle.V_BackColor = Drawing.Color.White
                VirControle.Visible = Not (VirControle.V_SiEnLectureSeule)
                VirControle.FicheEchelon = Nothing
            End If
            IndiceI += 1
        Loop
    End Sub

    Private Sub Echelon_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles Echelon01.ValeurChange, Echelon02.ValeurChange, Echelon03.ValeurChange,
            Echelon04.ValeurChange, Echelon05.ValeurChange, Echelon06.ValeurChange, Echelon07.ValeurChange, Echelon08.ValeurChange, Echelon09.ValeurChange, Echelon10.ValeurChange,
            Echelon11.ValeurChange, Echelon12.ValeurChange, Echelon13.ValeurChange, Echelon14.ValeurChange, Echelon15.ValeurChange, Echelon16.ValeurChange, Echelon17.ValeurChange,
            Echelon18.ValeurChange, Echelon19.ValeurChange, Echelon20.ValeurChange
        Dim Ctl As Control
        Dim VirControle As Virtualia.Net.GRD_ECHELON
        Dim IndiceI As Integer = 0
        Select Case e.Parametre
            Case "Select"
                Do
                    Ctl = V_WebFonction.VirWebControle(Me.CadreGrille, "Echelon", IndiceI)
                    If Ctl Is Nothing Then
                        Exit Do
                    End If
                    If TypeOf (Ctl) Is Virtualia.Net.GRD_ECHELON Then
                        VirControle = CType(Ctl, Virtualia.Net.GRD_ECHELON)
                        If VirControle.Numero_Ligne = CInt(e.Valeur) Then
                            V_EchelonCourant = VirControle.V_ChaineRef
                        Else
                            VirControle.V_BackColor = Drawing.Color.White
                        End If
                    End If
                    IndiceI += 1
                Loop
        End Select

    End Sub

    Private Sub VComboDates_SelectedIndexChanged(sender As Object, e As EventArgs) Handles VComboDates.SelectedIndexChanged
        Dim GradeCourant As Virtualia.TablesObjet.ShemaREF.GRD_GRADE
        Dim LstGrilles As List(Of Virtualia.TablesObjet.ShemaREF.GRD_GRILLE)

        Call InitialiserControles()
        GradeCourant = Fiche_Grade
        If GradeCourant Is Nothing Then
            Exit Sub
        End If
        GradeCourant.V_Tag = VComboDates.SelectedItem.Text
        LstGrilles = GradeCourant.ListedesGrilles
        If LstGrilles Is Nothing OrElse LstGrilles.Count = 0 Then
            Exit Sub
        End If
        For Each Grille In LstGrilles
            If Grille.Date_de_Valeur = VComboDates.SelectedItem.Text Then
                Fiche_Grille = Grille
                Exit For
            End If
        Next
    End Sub
End Class