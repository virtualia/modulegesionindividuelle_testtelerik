﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VZoneSaisie.ascx.vb" Inherits="Virtualia.Net.VZoneSaisie" %>

<asp:Table ID="Zone" runat="server" CellPadding="1" CellSpacing="0" Width="680px" BorderStyle="NotSet" 
           BorderWidth="1px" BorderColor="#B0E0D7" Height="30px" HorizontalAlign="Center" > 
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left"> 
            <asp:Label ID="EtiZone" runat="server" Height="20px" Width="200px"
                BackColor="#6D9092" BorderColor="#D7FAF3" BorderStyle="None" Text=""
                BorderWidth="1px" ForeColor="White" Font-Italic="False"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                style="margin-top: 5px; margin-left: 5px; margin-bottom: 0px;
                font-style: normal; text-indent: 5px; text-align:  left; padding-top: 0px;">
            </asp:Label>    
        </asp:TableCell>   
        <asp:TableCell HorizontalAlign="Left">  
            <asp:TextBox ID="DonZone" runat="server" Text="" Visible="true" AutoPostBack="False"
                BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px"
                ForeColor="#142425" Height="16px" Width="100px"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                style="margin-top: 2px; margin-left: 0px; font-style: normal;
                text-indent: 5px; text-align: center"/>         
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Left"> 
             <asp:DropDownList ID="ComboZone" runat="server" Height="22px" Width="360px"
                AutoPostBack="true" BackColor="#E2F5F1" ForeColor="#142425"
                style="margin-top: 2px; border-color: #7EC8BE; border-width: 2px; border-style: inset;
                     display: table-cell; font-style: normal; ">
            </asp:DropDownList>
        </asp:TableCell> 
        <asp:TableCell HorizontalAlign="Left"> 
            <asp:Label ID="EtiMasque" runat="server" Height="20px" Width="20px"
                BackColor="Transparent" BorderStyle="None" Text=""
                Font-Italic="False" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                style="margin-top: 5px; margin-left: 0px; margin-bottom: 0px;
                font-style: normal; text-indent: 2px; text-align:  left; padding-top: 0px;">
            </asp:Label>            
        </asp:TableCell>  
    </asp:TableRow>
</asp:Table>