﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VLine.ascx.vb" Inherits="Virtualia.Net.VLine" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>


<style type="text/css">
    .Std_Etiquette
    {  
        background-color:#B0E0D7;
        color:#142425;
        border-color:#B0E0D7;
        border-style:outset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:5px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        height:20px;
    }
     .Std_Donnee
    {  
        background-color:white;
        color:black;
        border-color:#B0E0D7;
        border-style:inset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:1px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        height:16px;
    }
</style>


<asp:Table ID="Line" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="Date_L6" runat="server" TypeCalendrier="Standard" DontabIndex="44"
                            V_SiDonneeDico="false" EtiVisible="false" EtiForeColor="Black" Width="120px" DonStyle="margin-left:10px" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Lieu_L6" runat="server" CssClass="Std_Donnee" Width="170px" TextMode="SingleLine" DontabIndex="45"> 
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Prenom_L6" runat="server" CssClass="Std_Donnee" Width="170px" TextMode="SingleLine" DontabIndex="46"> 
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="Nom_L6" runat="server" CssClass="Std_Donnee" Width="170px" TextMode="SingleLine" DontabIndex="47">
                        </asp:TextBox>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="90px">
                        <Virtualia:VTrioHorizontalRadio ID="Radio_L603" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true" V_Groupe="SexeEnfant_L5" V_TabIndex="48"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Masculin" RadioCentreText="Féminin" RadioDroiteText="" RadioCentreBorderStyle ="None"  RadioGaucheBorderStyle="None"
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="90px">
                        <Virtualia:VTrioHorizontalRadio ID="Radio_L604" runat="server" V_SiDonneeDico="false"
                           V_SiAutoPostBack="true" V_Groupe="EnfantCharge_L5" V_TabIndex="49"
                           RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteVisible="false"
                           RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText="" RadioCentreBorderStyle ="None"  RadioGaucheBorderStyle="None"
                           RadioGaucheStyle="margin-left: 1px;" />
                     </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="90px">
                       <asp:Table ID="CadreCmdClear_L6" runat="server" Height="16px" CellPadding="0"
                            CellSpacing="0" BackImageUrl="~/Images/Boutons/supprimer.bmp" Visible="true"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
                            Width="10px" HorizontalAlign="Right" Style="margin-top: 3px; margin-right: 3px">
                            <asp:TableRow>
                                <asp:TableCell VerticalAlign="Bottom">
                                    <asp:Button ID="btnClear_L6" runat="server" Width="10px" Height="16px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" Style="margin-left: 6px; text-align: right;">
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                      </asp:Table>
                   </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
   

   