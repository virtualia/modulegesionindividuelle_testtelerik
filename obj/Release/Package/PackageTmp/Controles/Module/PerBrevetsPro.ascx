﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PerBrevetsPro.ascx.vb" Inherits="Virtualia.Net.PerBrevetsPro" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Standards/VReferentiel.ascx" tagname="VReference" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>

<style type="text/css">
    .Std_Etiquette
    {  
        background-color:#B0E0D7;
        color:#142425;
        border-color:#B0E0D7;
        border-style:outset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:5px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        height:20px;
    }
     .Std_Donnee
    {  
        background-color:white;
        color:black;
        border-color:#B0E0D7;
        border-style:inset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:1px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        height:16px;
    }
</style>
<asp:Table ID="CadrePerBrevetPro" runat="server" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545" Width="800px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitre" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiDate" runat="server" CssClass="Std_Etiquette" Width="125px" Text="Date de validité" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiBrevet" runat="server" CssClass="Std_Etiquette" Width="250px" Text="Qualification" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiOrganisme" runat="server" CssClass="Std_Etiquette" Width="200px" Text="Organisme" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiNumero" runat="server" CssClass="Std_Etiquette" Width="120px" Text="Numéro" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiDatefin" runat="server" CssClass="Std_Etiquette" Width="120px" Text="Fin de validité" >
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Ligne_01" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL1_Date" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="34" V_Information="0" V_SiDonneeDico="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:5px" DonTabIndex="41" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL1_01" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="34" V_Information="1" V_SiDonneeDico="true"
                                       DonWidth="252px" DonTabIndex="42"/>
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoHL1_02" runat="server" EtiVisible="false"
                                       EtiTooltip="Organisme ayant délivré ou certifié la qualification professionnelle"
                                       V_PointdeVue="1" V_Objet="34" V_Information="2" V_SiDonneeDico="true"
                                       DonWidth="200px" DonTabIndex="43"/> 
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHL1_03" runat="server" EtiVisible="false"
                                       EtiTooltip=""
                                       V_PointdeVue="1" V_Objet="34" V_Information="3" V_SiDonneeDico="true"
                                       DonWidth="120px" DonTabIndex="44"/> 
                     </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL1_Datefin" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="34" V_Information="4" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:5px" DonTabIndex="41" />       
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Ligne_02" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL2_Date" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="34" V_Information="0" V_SiDonneeDico="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:5px" DonTabIndex="45" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL2_01" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="34" V_Information="1" V_SiDonneeDico="true"
                                       DonWidth="252px" DonTabIndex="46"/>
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoHL2_02" runat="server" EtiVisible="false"
                                       EtiTooltip="Organisme ayant délivré ou certifié la qualification professionnelle"
                                       V_PointdeVue="1" V_Objet="34" V_Information="2" V_SiDonneeDico="true"
                                       DonWidth="200px" DonTabIndex="47"/> 
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHL2_03" runat="server" EtiVisible="false"
                                       EtiTooltip=""
                                       V_PointdeVue="1" V_Objet="34" V_Information="3" V_SiDonneeDico="true"
                                       DonWidth="120px" DonTabIndex="48"/> 
                     </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL2_Datefin" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="34" V_Information="4" V_SiDonneeDico="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:5px" DonTabIndex="41" />       
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Ligne_03" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL3_Date" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="34" V_Information="0" V_SiDonneeDico="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:5px" DonTabIndex="49" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL3_01" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="34" V_Information="1" V_SiDonneeDico="true"
                                       DonWidth="252px" DonTabIndex="50"/>
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoHL3_02" runat="server" EtiVisible="false"
                                       EtiTooltip="Organisme ayant délivré ou certifié la qualification professionnelle"
                                       V_PointdeVue="1" V_Objet="34" V_Information="2" V_SiDonneeDico="true"
                                       DonWidth="200px" DonTabIndex="51"/> 
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHL3_03" runat="server" EtiVisible="false"
                                       EtiTooltip=""
                                       V_PointdeVue="1" V_Objet="34" V_Information="3" V_SiDonneeDico="true"
                                       DonWidth="120px" DonTabIndex="52"/> 
                     </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL3_Datefin" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="34" V_Information="4" V_SiDonneeDico="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:5px" DonTabIndex="41" />       
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Ligne_04" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL4_Date" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="34" V_Information="0" V_SiDonneeDico="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:5px" DonTabIndex="53" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL4_01" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="34" V_Information="1" V_SiDonneeDico="true"
                                       DonWidth="252px" DonTabIndex="54"/>
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoHL4_02" runat="server" EtiVisible="false"
                                       EtiTooltip="Organisme ayant délivré ou certifié la qualification professionnelle"
                                       V_PointdeVue="1" V_Objet="34" V_Information="2" V_SiDonneeDico="true"
                                       DonWidth="200px" DonTabIndex="55"/> 
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHL4_03" runat="server" EtiVisible="false"
                                       EtiTooltip=""
                                       V_PointdeVue="1" V_Objet="34" V_Information="3" V_SiDonneeDico="true"
                                       DonWidth="120px" DonTabIndex="56"/> 
                     </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL4_Datefin" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="34" V_Information="4" V_SiDonneeDico="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:5px" DonTabIndex="41" />       
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Ligne_05" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL5_Date" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="34" V_Information="0" V_SiDonneeDico="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:5px" DonTabIndex="53" />       
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VDuoEtiquetteCommande ID="DontabL5_01" runat="server" EtiVisible="false"
                                       V_PointdeVue="1" V_Objet="34" V_Information="1" V_SiDonneeDico="true"
                                       DonWidth="252px" DonTabIndex="54"/>
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoHL5_02" runat="server" EtiVisible="false"
                                       EtiTooltip="Organisme ayant délivré ou certifié la qualification professionnelle"
                                       V_PointdeVue="1" V_Objet="34" V_Information="2" V_SiDonneeDico="true"
                                       DonWidth="200px" DonTabIndex="55"/> 
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center" Width="200px">
                        <Virtualia:VCoupleEtiDonnee ID="InfoHL5_03" runat="server" EtiVisible="false"
                                       EtiTooltip=""
                                       V_PointdeVue="1" V_Objet="34" V_Information="3" V_SiDonneeDico="true"
                                       DonWidth="120px" DonTabIndex="56"/> 
                     </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="DonneeL5_Datefin" runat="server" TypeCalendrier="Standard" 
                            V_PointdeVue="1" V_Objet="34" V_Information="4" V_SiDonneeDico="true"
                            EtiVisible="false" EtiForeColor="Black" DonStyle="margin-left:5px" DonTabIndex="41" />       
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:Table ID="CadreRefBrevet" runat="server">
     <asp:TableRow>
         <asp:TableCell ID="CellReference" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupReferentiel" runat="server" TargetControlID="HPopupRef" PopupControlID="PanelRefPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelRefPopup" runat="server">
                <Virtualia:VReference ID="RefVirtualia" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupRef" runat="server" Value="0" />
        </asp:TableCell>
     </asp:TableRow>       
</asp:Table>
