﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PerListeEnfants.ascx.vb" Inherits="Virtualia.Net.PerListeEnfants" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VLine.ascx" tagname="VLine" tagprefix="Virtualia" %>


<style type="text/css">
    .Std_Etiquette
    {  
        background-color:#B0E0D7;
        color:#142425;
        border-color:#B0E0D7;
        border-style:outset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:5px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        height:20px;
    }
     .Std_Donnee
    {  
        background-color:white;
        color:black;
        border-color:#B0E0D7;
        border-style:inset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:1px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        height:16px;
    }
</style>


<asp:Table runat="server" HorizontalAlign="Left">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="myLabel" runat="server" Text="Nombre d'enfants" />
            <asp:DropDownList ID="myDropDown" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ItemChange" > 
	            <asp:ListItem Value="valeur1" Text="1" Selected="True"  /> 
	            <asp:ListItem Value="valeur2" Text="2" /> 
	            <asp:ListItem Value="valeur3" Text="3" /> 	           
                <asp:ListItem Value="valeur4" Text="4" /> 
	            <asp:ListItem Value="valeur5" Text="5" /> 
	            <asp:ListItem Value="valeur6" Text="6" /> 	            
                <asp:ListItem Value="valeur7" Text="7" /> 
	            <asp:ListItem Value="valeur8" Text="8" /> 
	            <asp:ListItem Value="valeur9" Text="9" /> 
	            <asp:ListItem Value="valeur10" Text="10" /> 
            </asp:DropDownList> 
            </asp:TableCell>
    </asp:TableRow>
    </asp:Table>

<asp:Table  ID="CadrePerEnfant" runat="server" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545" Width="810px" >
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitre" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiDate" runat="server" CssClass="Std_Etiquette" Width="120px" Height="18px" Text="Date de naissance" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiLieu" runat="server" CssClass="Std_Etiquette" Width="170px" Height="18px" Text="Lieu de naissance" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiPrenom" runat="server" CssClass="Std_Etiquette" Width="170px" Height="18px" Text="Prénom usuel" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiNom" runat="server" CssClass="Std_Etiquette" Width="170px" Height="18px" Text="Nom de famille" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiSexe" runat="server" CssClass="Std_Etiquette" Width="96px" Height="18px" Text="Sexe" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiCharge" runat="server" CssClass="Std_Etiquette" Width="96px" Height="18px" Text="à Charge" >
                        </asp:Label>
                    </asp:TableCell>

                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>


 
