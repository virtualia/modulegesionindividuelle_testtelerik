﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PerFormation.ascx.vb" Inherits="Virtualia.Net.PerFormation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Standards/VFiltreListe.ascx" tagname="VFiltreListe" tagprefix="Virtualia" %>

<style type="text/css">
    .Gen_Titre
    {  
        background-color:#216B68;
        color:#D7FAF3;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:initial;
        height:25px;
        width:746px;
    }
     .Gen_TitreChapitre
    {  
        background-color:#CAEBE4;
        color:#124545;
        border-style:none;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:normal;
        text-indent:0px;
        text-align:center;
        margin-top:5px;
        margin-left:0px;
        margin-bottom:0px;
        word-wrap:normal;
        height:28px;
        width:746px;
    }
      .Gen_EtiInfo
    {  
        background-color:#E2FDF7;
        color:#124545;
        border-style:unset;
        border-width:1px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:5px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:5px;
        word-wrap:normal;
        height:18px;
        width:710px;
    }
</style>
<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
      <asp:TableCell>
        <asp:Table ID="CadreTitreGeneral" runat="server" Height="50px" CellPadding="0" Width="750px" 
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#124545">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="EtiTitre" runat="server" Text="PARCOURS DE FORMATION" Height="25px" Width="746px" Cssclass="Gen_Titre">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
        </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreParcours" runat="server" BackColor="#E2F5F1" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow> 
                     <asp:TableCell HorizontalAlign="Center"> 
                       <Virtualia:VFiltreListe ID="ListeGrille" runat="server" CadreWidth="1000px" SiColonneSelect="false" V_NumColonne_Filtre="3" 
                            EtiTextFiltre="Suite donnée" EtiTextWidth="200px"/>
                     </asp:TableCell>
               </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
