﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Tableau.ascx.vb" Inherits="Virtualia.Net.PerEnfants" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>

<style type="text/css">
    .Std_Etiquette
    {  
        background-color:#B0E0D7;
        color:#142425;
        border-color:#B0E0D7;
        border-style:outset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        text-indent:5px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        height:20px;
    }
     .Std_Donnee
    {  
        background-color:white;
        color:black;
        border-color:#B0E0D7;
        border-style:inset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        text-indent:1px;
        text-align:center;
        margin-top:1px;
        margin-left:0px;
        margin-bottom:0px;
        height:16px;
    }
</style>
<asp:Table ID="CadrePerEnfant" runat="server" BorderStyle="Solid" BorderWidth="2px" BorderColor="#FFFFFF" Width="700px">
    <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="Label33" runat="server" Cssclass="Gen_EtiInfo"
                           Text="Nombre d'agents par mois ayant saisi et taux de saisie effectif par mois" Tooltip="" >
                        </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitre" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label" runat="server"  Width="103px" Height="0px">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label0" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Janvier" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label1" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Février" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label2" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Mars" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label3" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Avril" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label4" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Mai" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label5" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Juin" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label6" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Juillet" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label7" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Août" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label8" runat="server" CssClass="Std_Etiquette" Width="68px" Height="18px" Text="Septembre" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label9" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Octobre" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label10" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Novembre" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label11" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Décembre" >
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Ligne_01" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label12" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" Text="Personnel saisis" >
                        </asp:Label>  
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="lb1" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox1" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox2" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox3" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox4" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox5" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox6" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox7" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox8" runat="server" CssClass="Std_Donnee" Width="68px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox9" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox10" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox11" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>             
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table1" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label13" runat="server" Width="104px"  >
                        </asp:Label>  
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox12" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox13" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox14" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox15" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox16" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox17" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox18" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox19" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox20" runat="server" CssClass="Std_Donnee" Width="68px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox21" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox22" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="TextBox23" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>             
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:Table ID="Table2" runat="server"  Width="700px" padding_top="50px">
    <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="Label14" runat="server" Cssclass="Gen_EtiInfo"
                           Text="Nombre d'agents ayant saisi par mois par Niveau 1" Tooltip="" >
                        </asp:Label>  
                    </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table3" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label15" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" Text="Niveau 1">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label16" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Janvier" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label17" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Février" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label18" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Mars" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label19" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Avril" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label20" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Mai" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label21" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Juin" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label22" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Juillet" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label23" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Août" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label24" runat="server" CssClass="Std_Etiquette" Width="68px" Height="18px" Text="Septembre" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label25" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Octobre" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label26" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Novembre" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label27" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Décembre" >
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table4" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label28" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" >
                        </asp:Label>  
                    </asp:TableCell>            
                    <asp:TableCell>
                        <asp:Label ID="Label29" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label30" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label31" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label32" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label191" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label192" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label193" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label194" runat="server" CssClass="Std_Donnee" Width="68px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label195" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label196" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label197" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>  
                    <asp:TableCell>
                        <asp:Label ID="Label198" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table5" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label199" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" >
                        </asp:Label>  
                    </asp:TableCell>            
                    <asp:TableCell>
                        <asp:Label ID="Label200" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label201" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label202" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label203" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label204" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label205" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label206" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label207" runat="server" CssClass="Std_Donnee" Width="68px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label208" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label209" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label210" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>   
                    <asp:TableCell>
                        <asp:Label ID="Label211" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table6" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label212" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" >
                        </asp:Label>  
                    </asp:TableCell>            
                    <asp:TableCell>
                        <asp:Label ID="Label213" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label214" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label215" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label216" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label217" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label218" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label219" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label220" runat="server" CssClass="Std_Donnee" Width="68px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label221" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label222" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label223" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>   
                    <asp:TableCell>
                        <asp:Label ID="Label224" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table7" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label225" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" >
                        </asp:Label>  
                    </asp:TableCell>            
                    <asp:TableCell>
                        <asp:Label ID="Label226" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label227" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label228" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label229" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label230" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label231" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label232" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label233" runat="server" CssClass="Std_Donnee" Width="68px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label234" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label235" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label236" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label237" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table8" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label238" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" >
                        </asp:Label>  
                    </asp:TableCell>            
                    <asp:TableCell>
                        <asp:Label ID="Label239" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label240" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label241" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label242" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label243" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label244" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label245" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label246" runat="server" CssClass="Std_Donnee" Width="68px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label247" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label248" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label249" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label250" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:Table ID="Table10" runat="server"  padding_top="50px">
    <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="Label34" runat="server" Cssclass="Gen_EtiInfo"
                           Text="Nombre d'agents ayant saisi par mois par Niveau 2" Tooltip="" >
                        </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table11" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label35" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" Text="Niveau 2">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label36" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Janvier" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label37" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Février" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label38" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Mars" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label39" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Avril" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label40" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Mai" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label41" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Juin" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label42" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Juillet" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label43" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Août" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label44" runat="server" CssClass="Std_Etiquette" Width="68px" Height="18px" Text="Septembre" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label45" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Octobre" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label46" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Novembre" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label47" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Décembre" >
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table12" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label48" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" >
                        </asp:Label>  
                    </asp:TableCell>            
                    <asp:TableCell>
                        <asp:Label ID="Label49" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label50" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label51" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label52" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label53" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label132" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label133" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label134" runat="server" CssClass="Std_Donnee" Width="68px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label135" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label136" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label137" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>  
                    <asp:TableCell>
                        <asp:Label ID="Label138" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table13" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label139" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" >
                        </asp:Label>  
                    </asp:TableCell>            
                    <asp:TableCell>
                        <asp:Label ID="Label140" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label141" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label142" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label143" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label144" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label145" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label146" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label147" runat="server" CssClass="Std_Donnee" Width="68px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label148" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label149" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label150" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>   
                    <asp:TableCell>
                        <asp:Label ID="Label151" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table14" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label152" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" >
                        </asp:Label>  
                    </asp:TableCell>            
                    <asp:TableCell>
                        <asp:Label ID="Label153" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label154" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label155" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label156" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label157" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label158" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label159" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label160" runat="server" CssClass="Std_Donnee" Width="68px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label161" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label162" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label163" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>   
                    <asp:TableCell>
                        <asp:Label ID="Label164" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table15" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label165" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" >
                        </asp:Label>  
                    </asp:TableCell>            
                    <asp:TableCell>
                        <asp:Label ID="Label166" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label167" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label168" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label169" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label170" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label171" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label172" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label173" runat="server" CssClass="Std_Donnee" Width="68px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label174" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label175" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label176" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label177" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table16" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label178" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" >
                        </asp:Label>  
                    </asp:TableCell>            
                    <asp:TableCell>
                        <asp:Label ID="Label179" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label180" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label181" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label182" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label183" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label184" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label185" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label186" runat="server" CssClass="Std_Donnee" Width="68px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label187" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label188" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label189" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label190" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<asp:Table ID="Table18" runat="server" padding_top="50px">
    <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="Label54" runat="server" Cssclass="Gen_EtiInfo"
                           Text="Nombre d'agents ayant saisi par mois par Niveau 3" Tooltip="" >
                        </asp:Label>  
                    </asp:TableCell>
                </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table19" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label55" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" Text="Niveau 3">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label56" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Janvier" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label57" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Février" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label58" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Mars" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label59" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Avril" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label60" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Mai" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label61" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Juin" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label62" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Juillet" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label63" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Août" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label64" runat="server" CssClass="Std_Etiquette" Width="68px" Height="18px" Text="Septembre" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label65" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Octobre" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label66" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Novembre" >
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label67" runat="server" CssClass="Std_Etiquette" Width="65px" Height="18px" Text="Décembre" >
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table21" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label69" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" >
                        </asp:Label>  
                    </asp:TableCell>            
                    <asp:TableCell>
                        <asp:Label ID="Label117" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label118" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label119" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label120" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label121" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label122" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label123" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label124" runat="server" CssClass="Std_Donnee" Width="68px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label125" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label126" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label127" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>  
                    <asp:TableCell>
                        <asp:Label ID="Label68" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table22" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label70" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" >
                        </asp:Label>  
                    </asp:TableCell>            
                    <asp:TableCell>
                        <asp:Label ID="Label106" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label107" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label108" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label109" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label110" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label111" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label112" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label113" runat="server" CssClass="Std_Donnee" Width="68px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label114" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label115" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label116" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>   
                    <asp:TableCell>
                        <asp:Label ID="Label131" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table23" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label71" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" >
                        </asp:Label>  
                    </asp:TableCell>            
                    <asp:TableCell>
                        <asp:Label ID="Label95" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label96" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label97" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label98" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label99" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label100" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label101" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label102" runat="server" CssClass="Std_Donnee" Width="68px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label103" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label104" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label105" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>   
                    <asp:TableCell>
                        <asp:Label ID="Label130" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table24" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label72" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" >
                        </asp:Label>  
                    </asp:TableCell>            
                    <asp:TableCell>
                        <asp:Label ID="Label84" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label85" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label86" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label87" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label88" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label89" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label90" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label91" runat="server" CssClass="Std_Donnee" Width="68px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label92" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label93" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label94" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label129" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="Table25" runat="server" CellPadding="1" CellSpacing="0">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label73" runat="server" CssClass="Std_Etiquette" Width="100px" Height="18px" >
                        </asp:Label>  
                    </asp:TableCell>            
                    <asp:TableCell>
                        <asp:Label ID="Label83" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label82" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label81" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label80" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label79" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label78" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label77" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label76" runat="server" CssClass="Std_Donnee" Width="68px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label75" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label74" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Label676" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                    <asp:TableCell>
                        <asp:Label ID="Label128" runat="server" CssClass="Std_Donnee" Width="65px" TextMode="SingleLine" TabIndex="22"> 
                        </asp:Label>
                    </asp:TableCell> 
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
 
