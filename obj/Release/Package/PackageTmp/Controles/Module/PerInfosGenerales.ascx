﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PerInfosGenerales.ascx.vb" Inherits="Virtualia.Net.PerInfosGenerales" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controles/Standards/VMessage.ascx" TagName="VMessage" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Standards/VReferentiel.ascx" TagName="VReference" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDate.ascx" TagName="VCoupleEtiDate" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCocheSimple.ascx" TagName="VCocheSimple" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" TagName="VTrioHorizontalRadio" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VSixBoutonRadio.ascx" TagName="VSixBoutonRadio" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VNumTelephone.ascx" TagName="VNumTelephone" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Module/PerEnfants.ascx" TagName="VPerEnfant" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Module/PerListeEnfants.ascx" TagName="PerListeEnfants" TagPrefix="Virtualia" %>

<style type="text/css">
    .popup {
        background-color: #E2F5F1;
        border-width: 3px;
        border-color: black;
        border-style: solid;
        padding-top: 10px;
        padding-left: 10px;
        width: 500px;
        height: 400px;
    }

    .Gen_Titre {
        background-color: #216B68;
        color: #D7FAF3;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: bold;
        text-indent: 0px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: initial;
        height: 25px;
        width: 746px;
    }

    .Gen_TitreChapitre {
        background-color: #CAEBE4;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: normal;
        text-indent: 0px;
        text-align: center;
        margin-top: 5px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .Gen_EtiInfo {
        background-color: #E2FDF7;
        color: #124545;
        border-style: unset;
        border-width: 1px;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: italic;
        font-weight: normal;
        text-indent: 5px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 5px;
        word-wrap: normal;
        height: 18px;
        width: 710px;
    }
</style>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .roundedcorner {
            font-size: 11pt;
            margin-left: auto;
            margin-right: auto;
            margin-top: 1px;
            margin-bottom: 1px;
            padding: 3px;
            border-top: 1px solid;
            border-left: 1px solid;
            border-right: 1px solid;
            border-bottom: 1px solid;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
        }
        .titre {
            font-size: 25px;
            margin-top: 1px;
            margin-bottom: 1px;
            margin-left: 100px;
            padding: 3px;
            text-align: center;
        }

        .background {
            background-color: black;
            filter: alpha(opacity=90);
            opacity: 0.8;
        }

        .btncancel {
            margin-right: 150px;
        }

        .popup {
            background-color: #E2F5F1;
            border-width: 3px;
            border-color: black;
            border-style: solid;
            padding-top: 10px;
            padding-left: 10px;
            width: 500px;
            height: 400px;
            text-align: center;
        }
    </style>
</head>





<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="850px" HorizontalAlign="Center" Style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreGeneral" runat="server" Height="50px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiTitre" runat="server" Text="DONNEES PERSONNELLES" Height="25px" Width="746px" CssClass="Gen_Titre">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0"
                CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="true"
                BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
                Width="70px" HorizontalAlign="Right" Style="margin-top: 3px; margin-right: 3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                            BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                            BorderStyle="None" Style="margin-left: 6px; text-align: right;"></asp:Button>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreEtatCivil" runat="server" BackColor="#E2F5F1" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiIdentite" runat="server" CssClass="Gen_EtiInfo"
                            Text="ETAT-CIVIL" ToolTip="">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreEtat" runat="server" Width="500px" CellPadding="0" CellSpacing="0" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="CadreQualite" runat="server" Height="25px" Width="720px" CellPadding="0" CellSpacing="0">
                                        <asp:TableRow>
                                            <asp:TableCell Width="510px" HorizontalAlign="Left">
                                                <Virtualia:VTrioHorizontalRadio ID="RadioHA01" runat="server" V_PointdeVue="1" V_Objet="1" V_Information="1" V_SiDonneeDico="true"
                                                    V_SiAutoPostBack="true" V_Groupe="Qualite" V_TabIndex="1"
                                                    RadioGaucheWidth="130px" RadioCentreWidth="130px" RadioDroiteVisible="false"
                                                    RadioGaucheText="Monsieur" RadioCentreText="Madame" RadioDroiteText=""
                                                    RadioGaucheStyle="margin-left: 5px;" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="CadreNomPrenom" runat="server" Height="50px" Width="720px" CellPadding="0" CellSpacing="0">
                                        <asp:TableRow>
                                            <asp:TableCell Width="710px" HorizontalAlign="Left" ColumnSpan="2">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHA02" runat="server" EtiText="Nom d'usage ou nom usuel"
                                                    EtiTooltip="Il s'agit du nom d'usage. Le plus souvent il s'agit du nom de naissance ou du nom du conjoint pour les personnes mariées"
                                                    V_PointdeVue="1" V_Objet="1" V_Information="2" V_SiDonneeDico="true"
                                                    EtiWidth="320px" DonWidth="280px" DonTabIndex="2" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Width="710px" HorizontalAlign="Left" ColumnSpan="2">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHA13" runat="server" EtiText="Nom de naissance si différent du nom d'usage"
                                                    EtiTooltip="Encore appelé nom patronymique ou nom de jeune fille"
                                                    V_PointdeVue="1" V_Objet="1" V_Information="13" V_SiDonneeDico="true"
                                                    EtiWidth="320px" DonWidth="280px" DonTabIndex="3" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Width="330px" HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHA03" runat="server" EtiText="Prénom d'usage"
                                                    V_PointdeVue="1" V_Objet="1" V_Information="3" V_SiDonneeDico="true"
                                                    EtiWidth="120px" DonWidth="200px" DonTabIndex="4" />
                                            </asp:TableCell>
                                            <asp:TableCell Width="380px" HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHA14" runat="server" EtiText="Autres prénoms"
                                                    V_PointdeVue="1" V_Objet="1" V_Information="14" V_SiDonneeDico="true"
                                                    EtiWidth="120px" DonWidth="250px" DonTabIndex="5" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="CadreNaissance" runat="server" Height="40px" Width="720px" CellPadding="0" CellSpacing="0">
                                        <asp:TableRow VerticalAlign="Middle">
                                            <asp:TableCell Width="200px" HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDate ID="InfoDA04" runat="server" TypeCalendrier="Standard" EtiWidth="100px"
                                                    V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="1" V_Information="4" DonTabIndex="6" />
                                            </asp:TableCell>
                                            <asp:TableCell Width="200px" HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHA05" runat="server"
                                                    V_PointdeVue="1" V_Objet="1" V_Information="5" V_SiDonneeDico="true"
                                                    EtiWidth="20px" DonWidth="150px" EtiText="à" DonTabIndex="7"
                                                    EtiStyle="margin-left: 1px;" />
                                            </asp:TableCell>
                                            <asp:TableCell Width="300px" HorizontalAlign="Left">
                                                <Virtualia:VDuoEtiquetteCommande ID="DontabA06" runat="server"
                                                    V_PointdeVue="1" V_Objet="1" V_Information="6" V_SiDonneeDico="true"
                                                    EtiWidth="220px" DonWidth="80px" EtiText="Département ou Pays de Naissance" DonTabIndex="8"
                                                    EtiStyle="margin-left: 1px;" />
                                            </asp:TableCell>
                                            <asp:TableCell Width="20px"></asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="CadreNIR" runat="server" Height="30px" Width="720px" CellPadding="0" CellSpacing="0">
                                        <asp:TableRow VerticalAlign="Middle">
                                            <asp:TableCell Width="372px" HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHA11" runat="server"
                                                    V_PointdeVue="1" V_Objet="1" V_Information="11" V_SiDonneeDico="true"
                                                    EtiWidth="250px" DonWidth="120px" DonTabIndex="9" />
                                            </asp:TableCell>
                                            <asp:TableCell Width="350px" HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHA12" runat="server"
                                                    V_PointdeVue="1" V_Objet="1" V_Information="12" V_SiDonneeDico="true"
                                                    EtiWidth="30px" EtiText="Clé" DonWidth="20px" DonTabIndex="10" />
                                            </asp:TableCell>
                                            <asp:TableCell Width="350px" HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDate ID="InfoD17" runat="server"
                                                    V_PointdeVue="1" V_Objet="1" V_Information="17" V_SiDonneeDico="true"
                                                    EtiWidth="120px" DonWidth="185px" DonTabIndex="11" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" Height="25px">
                                    <asp:Label ID="EtiUploadEtatCiv" runat="server" CssClass="Gen_EtiInfo"
                                        Text="Pièces justificatives : Copie de la carte d'identité et copie de la carte Vitale"
                                        ToolTip="Vous pouvez déposer ici les documents justifiant de l'état-civil.">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" BackColor="WhiteSmoke">
                                    <ajaxToolkit:AjaxFileUpload ID="AjaxUploadEtatCiv" ThrobberID="myThrobber" ContextKeys="Virtualia"
                                        AllowedFileTypes=""
                                        MaximumNumberOfFiles="3" runat="server" Width="730px" BorderWidth="2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>


                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="25px">
                        <asp:Label ID="EtiTitreSitFam" runat="server" CssClass="Gen_EtiInfo"
                            Text="SITUATION DE FAMILLE" ToolTip="">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="Table2" runat="server" Width="500px" CellPadding="0" CellSpacing="0" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" Height="25px">
                                    <asp:Label ID="EtiSitFam" runat="server" CssClass="Gen_EtiInfo" Style="margin-left: 5px"
                                        Text="Vous êtes" ToolTip="">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="665px" Height="30px" HorizontalAlign="Center" VerticalAlign="Middle">
                                    <Virtualia:VSixBoutonRadio ID="RadioXA09" runat="server" V_PointdeVue="1" V_Objet="1" V_Information="9" V_SiDonneeDico="true"
                                        V_SiAutoPostBack="true" V_Groupe="SitFam" V_TabIndex="11"
                                        VRadioN1Width="110px" VRadioN2Width="110px" VRadioN3Width="110px" VRadioN4Width="110px" VRadioN5Width="110px" VRadioN6Width="110px"
                                        VRadioN1Text="Célibataire" VRadioN2Text="Marié(e)" VRadioN3Text="Pacsé(e)" VRadioN4Text="Vie maritale" VRadioN5Text="Veuf(ve)" VRadioN6Text="Divorcé(e)"
                                        VRadioN1Style="margin-left:1px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" Height="25px">
                                    <asp:Label ID="EtiConjoint" runat="server" CssClass="Gen_EtiInfo" Style="margin-left: 5px"
                                        Text="Eventuellement, situation du conjoint" ToolTip="Si le conjoint est fonctionnaire, ces informations doivent être renseignées">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="CadreConjoint" runat="server" Width="710px" BorderStyle="Solid" BorderWidth="2px" BorderColor="#E2F5F1" CellPadding="0" CellSpacing="0">
                                        <asp:TableRow>
                                            <asp:TableCell Width="400px" HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHB01" runat="server" EtiText="Nom du conjoint"
                                                    EtiTooltip=""
                                                    V_PointdeVue="1" V_Objet="2" V_Information="1" V_SiDonneeDico="true"
                                                    EtiWidth="150px" DonWidth="240px" DonTabIndex="12" />
                                            </asp:TableCell>
                                            <asp:TableCell Width="330px" HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHB02" runat="server" EtiText="Prénom"
                                                    V_PointdeVue="1" V_Objet="2" V_Information="2" V_SiDonneeDico="true"
                                                    EtiWidth="120px" DonWidth="185px" DonTabIndex="13" />
                                            </asp:TableCell>
                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell Width="400px" HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHB05" runat="server" EtiText="Lieu de naissance"
                                                    EtiTooltip=""
                                                    V_PointdeVue="1" V_Objet="2" V_Information="5" V_SiDonneeDico="true"
                                                    EtiWidth="150px" DonWidth="240px" DonTabIndex="14" />
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDate ID="InfoD10" runat="server" EtiText="Date situation de famille"
                                                    V_PointdeVue="1" V_Objet="1" V_Information="10" V_SiDonneeDico="true"
                                                    EtiWidth="170px" DonWidth="215px" DonTabIndex="15" />
                                            </asp:TableCell>

                                        </asp:TableRow>

                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHB04" runat="server" EtiText="Employeur"
                                                    EtiTooltip=""
                                                    V_PointdeVue="1" V_Objet="2" V_Information="4" V_SiDonneeDico="true"
                                                    EtiWidth="150px" DonWidth="240px" DonTabIndex="16" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VDuoEtiquetteCommande ID="DontabB03" runat="server" EtiText="Profession"
                                                    V_PointdeVue="1" V_Objet="2" V_Information="3" V_SiDonneeDico="true"
                                                    EtiWidth="120px" DonWidth="190px" DonTabIndex="17" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCocheSimple ID="CocheB07" runat="server"
                                                    EtiTooltip=""
                                                    V_PointdeVue="1" V_Objet="2" V_Information="7" V_SiDonneeDico="true"
                                                    V_Width="200px" V_Style="margin-left:5px" V_Text="Est fonctionnaire" DonTabIndex="18" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="5px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VPerEnfant ID="CadreEnfant" runat="server"></Virtualia:VPerEnfant>
                                </asp:TableCell>
                            </asp:TableRow>

                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiDomicile" runat="server" CssClass="Gen_EtiInfo"
                            Text="ADRESSE DU DOMICILE" ToolTip="">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreDomicile" runat="server" Width="500px" CellPadding="0" CellSpacing="0" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHD01" runat="server"
                                        V_PointdeVue="1" V_Objet="4" V_Information="1" V_SiDonneeDico="true"
                                        DonWidth="300px" DonTabIndex="50" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHD02" runat="server"
                                        V_PointdeVue="1" V_Objet="4" V_Information="2" V_SiDonneeDico="true"
                                        DonWidth="300px" DonTabIndex="51" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHD03" runat="server"
                                        V_PointdeVue="1" V_Objet="4" V_Information="3" V_SiDonneeDico="true"
                                        DonWidth="300px" DonTabIndex="52" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="CP" runat="server" Height="22px" Width="495px" CellPadding="0" CellSpacing="0">
                                        <asp:TableRow>
                                            <asp:TableCell Width="145px" HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHD04" runat="server"
                                                    V_PointdeVue="1" V_Objet="4" V_Information="4" V_SiDonneeDico="true"
                                                    DonWidth="40px" EtiText="Code postal et ville" DonTabIndex="53" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHD05" runat="server"
                                                    V_PointdeVue="1" V_Objet="4" V_Information="5" V_SiDonneeDico="true"
                                                    EtiWidth="0px" EtiVisible="false" DonWidth="250px" DonTabIndex="54" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiRIB" runat="server" CssClass="Gen_EtiInfo"
                            Text="RELEVE D'IDENTITE BANCAIRE - IBAN" ToolTip="">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle">
                        <asp:Table ID="CadreIban" runat="server" CellPadding="0" CellSpacing="0" Width="500px" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHE07" runat="server" EtiText="IBAN"
                                        V_PointdeVue="1" V_Objet="5" V_Information="7" V_SiDonneeDico="true"
                                        EtiWidth="130px" DonWidth="350px" DonHeight="20px" DonTabIndex="55"
                                        EtiStyle="margin-top:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHE08" runat="server"
                                        V_PointdeVue="1" V_Objet="5" V_Information="8" V_SiDonneeDico="true"
                                        EtiWidth="130px" DonWidth="200px" DonTabIndex="56"
                                        EtiStyle="margin-left: 2px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHE05" runat="server"
                                        V_PointdeVue="1" V_Objet="5" V_Information="5" V_SiDonneeDico="true"
                                        EtiWidth="220px" DonWidth="250px" DonTabIndex="57"
                                        EtiStyle="margin-left: 2px;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="25px">
                        <asp:Label ID="EtiJustificatifRIB" runat="server" CssClass="Gen_EtiInfo"
                            Text="Pièces justificatives : RIB - IBAN"
                            ToolTip="Vous pouvez déposer ici les documents justifiant du compte bancaire.">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" BackColor="WhiteSmoke">
                        <ajaxToolkit:AjaxFileUpload ID="AjaxFileUploadIBAN" ThrobberID="myThrobber" ContextKeys="Virtualia"
                            AllowedFileTypes=""
                            MaximumNumberOfFiles="1" runat="server" Width="730px" BorderWidth="2px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiCoordonnees" runat="server" CssClass="Gen_EtiInfo"
                            Text="COORDONNEES PERSONNELLES" ToolTip="">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreCoordonnee" runat="server" CellPadding="0" CellSpacing="0" Width="650px" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VNumTelephone ID="InfoTD06" runat="server"
                                        V_PointdeVue="1" V_Objet="4" V_Information="6" V_SiDonneeDico="true"
                                        DonWidth="230px" DonTabIndex="58" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VNumTelephone ID="InfoTD11" runat="server"
                                        V_PointdeVue="1" V_Objet="4" V_Information="11" V_SiDonneeDico="true"
                                        DonWidth="230px" DonTabIndex="59" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHD09" runat="server"
                                        V_PointdeVue="1" V_Objet="4" V_Information="9" V_SiDonneeDico="true"
                                        DonWidth="230px" DonTabIndex="60" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHD10" runat="server"
                                        V_PointdeVue="1" V_Objet="4" V_Information="10" V_SiDonneeDico="true"
                                        DonWidth="350px" DonTabIndex="61" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="Label1" runat="server" CssClass="Gen_EtiInfo"
                            Text="COORDONNEES réseaux sociaux " ToolTip="">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreReseaux" runat="server" CellPadding="0" CellSpacing="0" Width="550px" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHF18" runat="server"
                                        V_PointdeVue="1" V_Objet="4" V_Information="18" V_SiDonneeDico="true"
                                        DonWidth="350px" DonTabIndex="62" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHF19" runat="server"
                                        V_PointdeVue="1" V_Objet="4" V_Information="19" V_SiDonneeDico="true"
                                        DonWidth="350px" DonTabIndex="63" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHF20" runat="server"
                                        V_PointdeVue="1" V_Objet="4" V_Information="20" V_SiDonneeDico="true"
                                        DonWidth="350px" DonTabIndex="64" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHF21" runat="server"
                                        V_PointdeVue="1" V_Objet="4" V_Information="21" V_SiDonneeDico="true"
                                        DonWidth="350px" DonTabIndex="65" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHF22" runat="server"
                                        V_PointdeVue="1" EtiHeight="30px" V_Objet="4" V_Information="22" V_SiDonneeDico="true"
                                        DonWidth="350px" DonTabIndex="66" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>


                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiAprevenir" runat="server" CssClass="Gen_EtiInfo"
                            Text="PERSONNE A PREVENIR" ToolTip="Personne à prévenir en cas d'accident">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreAprevenir" runat="server" CellPadding="0" CellSpacing="0" Width="550px" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP01" runat="server" V_PointdeVue="1" V_Objet="30" V_Information="1" V_SiDonneeDico="true" DonWidth="300px" DonTabIndex="67" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VNumTelephone ID="InfoT06" runat="server" V_PointdeVue="1" V_Objet="30" V_Information="6" V_SiDonneeDico="true" DonWidth="150px" DonTabIndex="68" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP10" runat="server" V_PointdeVue="1" V_Objet="30" V_Information="10" V_SiDonneeDico="true" DonWidth="350px" DonTabIndex="69" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP09" runat="server" V_PointdeVue="1" V_Objet="30" V_Information="9" V_SiDonneeDico="true" DonWidth="350px" DonTabIndex="70" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" Height="25px">
                                    <asp:Label ID="EtiAdrAPrevenir" runat="server" CssClass="Gen_EtiInfo"
                                        Text="Eventuellement indiquer une adresse" ToolTip="">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP02" runat="server" V_PointdeVue="1" V_Objet="30" V_Information="2" V_SiDonneeDico="true" DonWidth="300px" DonTabIndex="71" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP03" runat="server" V_PointdeVue="1" V_Objet="30" V_Information="3" V_SiDonneeDico="true" DonWidth="300px" DonTabIndex="72" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="CPAPrevenir" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHP04" runat="server" V_PointdeVue="1" V_Objet="30" V_Information="4" V_SiDonneeDico="true" DonWidth="40px" EtiText="Code postal et ville" DonTabIndex="73" />
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHP05" runat="server" V_PointdeVue="1" V_Objet="30" V_Information="5" V_SiDonneeDico="true" EtiWidth="0px" EtiVisible="false" DonWidth="250px" DonTabIndex="74" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="Label2" runat="server" CssClass="Gen_EtiInfo"
                            Text="Informations sur le transport" ToolTip="">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="Table1" runat="server" CellPadding="0" CellSpacing="0" Width="550px" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabA12" runat="server"
                                        V_PointdeVue="1" V_Objet="4" V_Information="12" V_SiDonneeDico="true"
                                        DonWidth="350px" DonTabIndex="75" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH23" runat="server"
                                        V_PointdeVue="1" V_Objet="4" V_Information="23" V_SiDonneeDico="true" EtiHeight="30px"
                                        DonWidth="350px" DonTabIndex="76" DonStyle="text-align:left;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ID="CellMessage" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupMsg" runat="server" TargetControlID="HPopupMsg" PopupControlID="PanelMsgPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelMsgPopup" runat="server">
                <Virtualia:VMessage ID="MsgVirtualia" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupMsg" runat="server" Value="0" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ID="CellReference" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupReferentiel" runat="server" TargetControlID="HPopupRef" PopupControlID="PanelRefPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelRefPopup" runat="server">
                <Virtualia:VReference ID="RefVirtualia" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupRef" runat="server" Value="0" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
    <asp:TableCell>
        <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
    </asp:TableCell>
</asp:TableRow>
</asp:Table>
