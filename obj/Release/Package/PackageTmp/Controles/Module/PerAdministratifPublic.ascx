﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PerAdministratifPublic.ascx.vb" Inherits="Virtualia.Net.PerAdministratifPublic" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controles/Standards/VReferentiel.ascx" TagName="VReference" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/REF/Grade/VReferentielGrades.ascx" TagName="VRefGrade" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDate.ascx" TagName="VCoupleEtiDate" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" TagName="VDuoEtiquetteCommande" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VReliquatAnciennete.ascx" TagName="VReliquatAnciennete" TagPrefix="Virtualia" %>


<style type="text/css">
    .Gen_Titre {
        background-color: #216B68;
        color: #D7FAF3;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: bold;
        text-indent: 0px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: initial;
        height: 25px;
        width: 746px;
    }

    .Gen_TitreChapitre {
        background-color: #CAEBE4;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: normal;
        text-indent: 0px;
        text-align: center;
        margin-top: 5px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .Gen_EtiInfo {
        background-color: #E2FDF7;
        color: #124545;
        border-style: unset;
        border-width: 1px;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: italic;
        font-weight: normal;
        text-indent: 5px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 5px;
        word-wrap: normal;
        height: 18px;
        width: 710px;
    }
</style>
<asp:Table ID="CadreGeneralAdm" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" Style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreGeneral" runat="server" Height="50px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiTitre" runat="server" Text="SITUATION ADMINISTRATIVE" Height="25px" Width="746px" CssClass="Gen_Titre">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfosAdm" runat="server" BackColor="#E2F5F1" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiPerStatut" runat="server" CssClass="Gen_EtiInfo"
                            Text="STATUT - CONTRAT DE TRAVAIL" ToolTip="">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreStatut" runat="server" Width="500px" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545" CellPadding="0" CellSpacing="0">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDate ID="InfoDS00" runat="server" TypeCalendrier="Standard" EtiWidth="170px"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="12" V_Information="0" DonTabIndex="1" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDate ID="InfoDS08" runat="server" TypeCalendrier="Standard" EtiWidth="170px"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="12" V_Information="8" DonTabIndex="1" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2" Width="400px">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabS01" runat="server" V_PointdeVue="1" V_Objet="12" V_Information="1" V_SiDonneeDico="true" DonTabIndex="2" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabS02" runat="server" V_PointdeVue="1" V_Objet="12" V_Information="2" V_SiDonneeDico="true" DonTabIndex="3" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <asp:Table ID="CadreArrete" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDate ID="InfoDS03" runat="server" TypeCalendrier="Standard" EtiWidth="200px"
                                                    V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="12" V_Information="3" DonTabIndex="4" />
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHS04" runat="server" EtiVisible="false" DonWidth="140px"
                                                    V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="12" V_Information="4" DonTabIndex="5" DonStyle="margin-left:2px;margin-top:0px" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="25px">
                        <asp:Label ID="EtiUploadContrat" runat="server" CssClass="Gen_EtiInfo"
                            Text="Pièces justificatives : Copie du contrat de travail ou de l'arrété de nomination"
                            ToolTip="Vous pouvez déposer ici les documents justifiant la situation juridique.">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" BackColor="WhiteSmoke">
                        <ajaxToolkit:AjaxFileUpload ID="AjaxUploadContrat" ThrobberID="myThrobber" ContextKeys="Virtualia"
                            AllowedFileTypes=""
                            MaximumNumberOfFiles="3" runat="server" Width="730px" BorderWidth="2px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiPerPosition" runat="server" CssClass="Gen_EtiInfo"
                            Text="POSITION ADMINISTRATIVE" ToolTip="">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadrePosition" runat="server" Width="520px" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545" CellPadding="0" CellSpacing="0">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" Width="250px">
                                    <Virtualia:VCoupleEtiDate ID="InfoDP00" runat="server" TypeCalendrier="Standard"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="13" V_Information="0" DonTabIndex="1" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" Width="250px">
                                    <Virtualia:VCoupleEtiDate ID="InfoDP07" runat="server" TypeCalendrier="Standard" SiDateFin="true"
                                        EtiWidth="80px" EtiStyle="margin-left:2px" EtiText="Jusqu'au"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="13" V_Information="7" DonTabIndex="2" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabP01" runat="server"
                                        V_PointdeVue="1" V_Objet="13" V_Information="1" V_SiDonneeDico="true"
                                        DonTabIndex="3" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabP05" runat="server"
                                        V_PointdeVue="1" V_Objet="13" V_Information="5" V_SiDonneeDico="true"
                                        DonTabIndex="6" EtiStyle="text-align:left" EtiText="Administration d’origine " />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabP11" runat="server"
                                        V_PointdeVue="1" V_Objet="13" V_Information="11" V_SiDonneeDico="true"
                                        DonTabIndex="5" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP02" runat="server" EtiWidth="300px"
                                        V_PointdeVue="1" V_Objet="13" V_Information="2" V_SiDonneeDico="true"
                                        DonWidth="80px" DonTabIndex="6" EtiStyle="text-align:center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP12" runat="server" EtiWidth="300px"
                                        V_PointdeVue="1" V_Objet="13" V_Information="12" V_SiDonneeDico="true"
                                        DonWidth="80px" DonTabIndex="7" EtiStyle="text-align:center" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabP08" runat="server"
                                        V_PointdeVue="1" V_Objet="13" V_Information="8" V_SiDonneeDico="true"
                                        DonTabIndex="8" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow Height="25px">
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiArrete" runat="server" Height="20px" Width="300px"
                                        Text="Date et référence de l'arrété"
                                        BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <Virtualia:VCoupleEtiDate ID="InfoDP03" runat="server" TypeCalendrier="Standard" EtiVisible="false"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="13" V_Information="3" DonTabIndex="9" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHP04" runat="server"
                                        V_PointdeVue="1" V_Objet="13" V_Information="4" V_SiDonneeDico="true"
                                        DonWidth="200px" DonTabIndex="10" EtiVisible="false" DonStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiDecret" runat="server" Height="20px" Width="350px"
                                        Text="Date du décret et date de parution au JO du décret"
                                        BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Table ID="CadreJO" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <Virtualia:VCoupleEtiDate ID="InfoDP09" runat="server" TypeCalendrier="Standard" EtiWidth="120px"
                                                    V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="13" V_Information="9" DonTabIndex="11" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCoupleEtiDate ID="InfoDP10" runat="server" TypeCalendrier="Standard" EtiWidth="120px" EtiText="Date de parution"
                                                    V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="13" V_Information="10" DonTabIndex="12" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="25px">
                        <asp:Label ID="EtiUploadPosition" runat="server" CssClass="Gen_EtiInfo"
                            Text="Pièces justificatives : Copie de l'arrété ou du décret"
                            ToolTip="Vous pouvez déposer ici les documents justifiant la position administrative.">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" BackColor="WhiteSmoke">
                        <ajaxToolkit:AjaxFileUpload ID="AjaxUploadPosition" ThrobberID="myThrobber" ContextKeys="Virtualia"
                            AllowedFileTypes=""
                            MaximumNumberOfFiles="3" runat="server" Width="730px" BorderWidth="2px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" Height="30px">
                        <asp:Label ID="EtiPerGrade" runat="server" CssClass="Gen_EtiInfo"
                            Text="GRADE" ToolTip="">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Table ID="CadreGrade" runat="server" Width="600px" BorderStyle="Solid" BorderWidth="2px" BorderColor="#124545" CellPadding="0" CellSpacing="0">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDate ID="InfoDG00" runat="server" TypeCalendrier="Standard"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="14" V_Information="0" DonTabIndex="1" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabG01" runat="server"
                                        V_PointdeVue="1" V_Objet="14" V_Information="1" V_SiDonneeDico="true"
                                        DonTabIndex="2" EtiWidth="100px" DonWidth="450px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabG02" runat="server"
                                        V_PointdeVue="1" V_Objet="14" V_Information="2" V_SiDonneeDico="true"
                                        DonTabIndex="3" EtiWidth="100px" DonWidth="100px" DonStyle="text-align:center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabG11" runat="server"
                                        V_PointdeVue="1" V_Objet="14" V_Information="11" V_SiDonneeDico="true"
                                        DonTabIndex="4" EtiWidth="100px" DonWidth="450px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <asp:Table ID="CadreIndices" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHG03" runat="server"
                                                    V_PointdeVue="1" V_Objet="14" V_Information="3" V_SiDonneeDico="true"
                                                    EtiWidth="100px" DonWidth="60px" DonTabIndex="5" DonStyle="text-align:center;" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHG04" runat="server"
                                                    V_PointdeVue="1" V_Objet="14" V_Information="4" V_SiDonneeDico="true"
                                                    EtiWidth="100px" DonWidth="60px" DonTabIndex="6" DonStyle="text-align:center;" />
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <Virtualia:VCoupleEtiDonnee ID="InfoHG23" runat="server"
                                                    V_PointdeVue="1" V_Objet="14" V_Information="23" V_SiDonneeDico="true"
                                                    EtiWidth="100px" DonWidth="60px" DonTabIndex="6" DonStyle="text-align:center;" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabG05" runat="server"
                                        V_PointdeVue="1" V_Objet="14" V_Information="5" V_SiDonneeDico="true"
                                        DonTabIndex="8" EtiWidth="100px" DonWidth="100px" DonStyle="text-align:center;" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabG13" runat="server"
                                        V_PointdeVue="1" V_Objet="14" V_Information="13" V_SiDonneeDico="true"
                                        DonTabIndex="9" EtiWidth="90px" DonWidth="100px" DonStyle="text-align:center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabG12" runat="server"
                                        V_PointdeVue="1" V_Objet="14" V_Information="12" V_SiDonneeDico="true"
                                        DonTabIndex="10" EtiWidth="100px" DonWidth="450px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabG06" runat="server"
                                        V_PointdeVue="1" V_Objet="14" V_Information="6" V_SiDonneeDico="true"
                                        DonTabIndex="11" EtiWidth="100px" DonWidth="450px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="DontabG07" runat="server"
                                        V_PointdeVue="1" V_Objet="14" V_Information="7" V_SiDonneeDico="true"
                                        DonTabIndex="11" EtiWidth="100px" EtiHeight="50px" DonWidth="450px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VReliquatAnciennete ID="Reliquat08" runat="server"
                                        V_PointdeVue="1" V_Objet="14" V_Information="8" V_SiDonneeDico="true"
                                        EtiWidth="250px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiArreteGrade" runat="server" Height="20px" Width="370px"
                                        Text="Date et référence de l'arrété"
                                        BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <Virtualia:VCoupleEtiDate ID="InfoDG09" runat="server" TypeCalendrier="Standard" EtiVisible="false"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="14" V_Information="9" DonTabIndex="13" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoHG10" runat="server"
                                        V_PointdeVue="1" V_Objet="14" V_Information="10" V_SiDonneeDico="true"
                                        DonWidth="200px" DonTabIndex="14" EtiVisible="false" DonStyle="margin-left:2px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiDecretGrade" runat="server" Height="20px" Width="370px" Text="Date de parution au JO du décret"
                                        BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <Virtualia:VCoupleEtiDate ID="InfoDG22" runat="server" TypeCalendrier="Standard"
                                        V_SiDonneeDico="true" EtiWidth="175px" V_PointdeVue="1" V_Objet="14" V_Information="22" DonTabIndex="15" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" Height="25px">
                        <asp:Label ID="EtiUploadGrade" runat="server" CssClass="Gen_EtiInfo"
                            Text="Pièces justificatives : Copie de l'arrété ou du décret"
                            ToolTip="Vous pouvez déposer ici les documents justifiant le grade et l'échelon.">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" BackColor="WhiteSmoke">
                        <ajaxToolkit:AjaxFileUpload ID="AjaxUploadGrade" ThrobberID="myThrobber" ContextKeys="Virtualia"
                            AllowedFileTypes=""
                            MaximumNumberOfFiles="3" runat="server" Width="730px" BorderWidth="2px" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ID="CellReference" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupReferentiel" runat="server" TargetControlID="HPopupRef" PopupControlID="PanelRefPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelRefPopup" runat="server">
                <Virtualia:VReference ID="RefVirtualia" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupRef" runat="server" Value="0" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ID="CellRefGrade" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupRefGrade" runat="server" TargetControlID="HPopupRefGrade" PopupControlID="PanelRefGradePopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelRefGradePopup" runat="server">
                <Virtualia:VRefGrade ID="RefGradeGrilles" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupRefGrade" runat="server" Value="0" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
