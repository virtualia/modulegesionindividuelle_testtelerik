﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VMenuCommun.ascx.vb" Inherits="Virtualia.Net.VMenuCommun" %>

<asp:Table ID="CadreMenu" runat ="server" BorderColor="#B0E0D7" BorderWidth="2px" BorderStyle="Ridge" BackColor="White" style="margin-top: 5px; width: 350px; height:800px; background-attachment: inherit; display: table-cell;" >
    <asp:TableRow>
        <asp:TableCell>
            <div style=" margin-top: 0px; width: 350px; height:800px; vertical-align:top; overflow: auto; background-color: #B0E0D7; text-align: left">   
                <asp:TreeView ID="TreeListeMenu"  runat="server" MaxDataBindDepth="2" BorderStyle="None" BorderWidth="2px" BorderColor="#B0E0D7" ForeColor="#142425" ShowCheckBoxes="None" Width="320px" Height="750px" Font-Bold="True" NodeIndent="10" BackColor="#B0E0D7" LeafNodeStyle-HorizontalPadding="8px" RootNodeStyle-Font-Bold="True" RootNodeStyle-ImageUrl="~/Images/Armoire/FicheBleue.bmp" RootNodeStyle-Font-Italic="False" >
                    <SelectedNodeStyle BackColor="#6C9690" BorderColor="#E3924F"  BorderStyle="Solid" BorderWidth="1px" ForeColor="White" />
                </asp:TreeView>
            </div>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
