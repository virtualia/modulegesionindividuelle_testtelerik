﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VConfirmation_Dossier.ascx.vb" Inherits="Virtualia.Net.VConfirmation_Dossier" %>

<style type="text/css">
    .LaLettre
    {  
        background-color:#124545;
        color:#D7FAF3;
        border-color:#B0E0D7;
        border-style:inset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        padding-left:2px;
        vertical-align:middle;
        word-wrap:normal;
        height:24px;
        width:24px;
    }
    .Loption
    {  
        background-color:#7D9F99;
        color:#E9FDF9;
        border-color:#B0E0D7;
        border-style:inset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:oblique;
        font-weight:bold;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        padding-left:2px;
        text-align:left;
        text-indent:5px;
        vertical-align:middle;
        word-wrap:normal;
        height:20px;
        width:220px;
    }
</style>

        <br /><br />
        <div style="text-align:left;" class="divlabel">
          <asp:Label ID="Label2"  HorizontalAlign="center" CssClass="LabelErreur"  runat="server"></asp:Label>
        </div>
        <br /><br /><br />
        <table>
            <tr>
                 <asp:Table ID="cadrebutton" runat="server" Height="22px" CellPadding="0"
                        CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="true"
                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
                        Width="70px" HorizontalAlign="center" Style="margin-top: 3px; margin-right: 3px">
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Bottom">
                                <asp:Button ID="Button2" runat="server" Text="Confirmer" Width="65px" Height="20px"
                                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                    BorderStyle="None" Style="margin-left: 6px; text-align: Right;"></asp:Button>
                                    
                            </asp:TableCell>

                        </asp:TableRow>
                    </asp:Table>
            </tr>
            </table>
   

   