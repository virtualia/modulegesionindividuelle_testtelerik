﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VLegendeCouleurs" Codebehind="VLegendeCouleurs.ascx.vb" %>

<asp:Table ID="VLegende" runat="server" Width="800px" CellPadding="0" CellSpacing="5" 
                        BorderStyle="None" HorizontalAlign="Center" >
    <asp:TableRow Height="20" HorizontalAlign="Center" VerticalAlign="Middle">
        <asp:TableCell>
            <asp:Label ID="EtiCouleur00" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiLegende00" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiCouleur01" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiLegende01" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiCouleur02" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiLegende02" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiCouleur03" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiLegende03" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow Height="20" HorizontalAlign="Center" VerticalAlign="Middle">
        <asp:TableCell>
            <asp:Label ID="EtiCouleur04" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiLegende04" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiCouleur05" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiLegende05" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiCouleur06" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiLegende06" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiCouleur07" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiLegende07" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow Height="20" HorizontalAlign="Center" VerticalAlign="Middle">
        <asp:TableCell>
            <asp:Label ID="EtiCouleur08" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiLegende08" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiCouleur09" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiLegende09" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiCouleur10" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiLegende10" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiCouleur11" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiLegende11" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow Height="20" HorizontalAlign="Center" VerticalAlign="Middle">
        <asp:TableCell>
            <asp:Label ID="EtiCouleur12" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiLegende12" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiCouleur13" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiLegende13" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiCouleur14" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiLegende14" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiCouleur15" runat="server" Height="15px" Width="30px" BackColor="LightGray" 
                    BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text=""
                    style="margin-top: 1px; text-indent: 1px; text-align: center; vertical-align: middle" />
        </asp:TableCell>
        <asp:TableCell>
            <asp:Label ID="EtiLegende15" runat="server" Height="15px" Width="160px" BackColor="Transparent"
                    ForeColor="#064F4D" BorderStyle="None" Text="Legende"
                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="true"
                    style="margin-top: 1px; text-indent: 1px; text-align: left;" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
