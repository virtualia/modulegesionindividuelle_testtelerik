﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VArmoirePER.ascx.vb" Inherits="Virtualia.Net.VArmoirePER" %>

<asp:Panel ID="PanelArmoire" runat="server" BackColor="#1C5151" HorizontalAlign="Center" Width="1000px" Height="980px" BorderStyle="Solid" BorderWidth="1px" BorderColor="#B0E0D7" Font-Names="Trebuchet MS" Font-Italic="true" Style="margin-top: 5px; margin-bottom: 5px">
    <asp:Table ID="CadreArmoire" runat="server">
        <asp:TableRow>
            <asp:TableCell ColumnSpan="2">
                <asp:Table ID="TableOnglets" runat="server" Height="35px" Width="640px" HorizontalAlign="Left" BackColor="Transparent" Style="margin-top: 2px;margin-left: 10px">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <asp:Menu ID="MenuChoix" runat="server" Width="640px" DynamicHorizontalOffset="10" Font-Names="Trebuchet MS" Font-Size="Small" 
                                BackColor="Transparent" Font-Italic="false" ForeColor="White" Height="32px" Orientation="Horizontal" 
                                StaticSubMenuIndent="20px" Font-Bold="True" BorderWidth="2px" Style="margin-left: 1px;" 
                                StaticEnableDefaultPopOutImage="False" BorderColor="#B0E0D7" BorderStyle="None">
                                <StaticSelectedStyle BackColor="#B0E0D7" BorderStyle="Solid" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#000066" />
                                <StaticMenuItemStyle VerticalPadding="2px" HorizontalPadding="10px" Height="24px" BackColor="#0E5F5C" BorderColor="#B0E0D7" BorderStyle="Solid" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="White" />
                                <StaticHoverStyle BackColor="#7EC8BE" ForeColor="White" BorderColor="#B0E0D7" BorderStyle="Solid" />
                                <Items>
                                    <asp:MenuItem Text="Plannings collectifs" Value="PLANNING" ToolTip="Les plannings de l'entité sélectionnée" />
                                    <asp:MenuItem Text="Suivi du temps de travail" Value="SUIVI" ToolTip="Suivi des badgeages, des présences et des absences" />
                                </Items>
                            </asp:Menu>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="CadreListe" runat="server" Style="margin-top: 10px; width: 640px; height: 845px; background-attachment: inherit; display: table-cell;">
                    <asp:TableRow>
                        <asp:TableCell Width="640px" HorizontalAlign="Center" VerticalAlign="Top" BackColor="#6D9092">
                            <asp:Table ID="TableTypeArmoire" runat="server" CellPadding="0" CellSpacing="0" HorizontalAlign="Center" Height="40px" Width="440px">
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:Label ID="EtiTypeArmoire" runat="server" Height="22px" Width="180px" BackColor="Transparent" ForeColor="White" BorderStyle="Solid" BorderColor="#5E9598" BorderWidth="1px" Text="Choix du type d'armoire" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Style="margin-top: 1px; text-align: center" />
                                    </asp:TableCell>
                                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                        <asp:DropDownList ID="DropDownArmoire" runat="server" AutoPostBack="True" Height="22px" Width="300px" BackColor="#A8BBB8" ForeColor="#124545" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" Style="margin-top: 0px; border-spacing: 2px; text-indent: 5px; text-align: left" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ID="CelluleLettre" runat="server" Width="650px" Visible="false">
                            <asp:Table ID="CadreLettre" runat="server" CellPadding="0" CellSpacing="0" HorizontalAlign="Left" Width="640px">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonA" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/A_Sel.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonB" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/B.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonC" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/C.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonD" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/D.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonE" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/E.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonF" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/F.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonG" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/G.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonH" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/H.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonI" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/I.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonJ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/J.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonK" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/K.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonL" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/L.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonM" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/M.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonN" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/N.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonO" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/O.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonP" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/P.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonQ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Q.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonR" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/R.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonS" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/S.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonT" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/T.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonU" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/U.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonV" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/V.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonW" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/W.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonX" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/X.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonY" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Y.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonZ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Z.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonAll" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/arobase.bmp" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="19">
                                        <asp:Label ID="EtiRecherche" runat="server" Text="Recherche par nom (Commençant par)" Height="16px" Width="420px" BackColor="Transparent" ForeColor="#D7FAF3" BorderStyle="None" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-right: 5px; font-style: oblique; text-indent: 5px; text-align: right;">
                                        </asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="8">
                                        <asp:TextBox ID="DonRecherche" runat="server" Text="" Visible="true" AutoPostBack="true" BackColor="White" BorderColor="#B0E0D7" BorderStyle="InSet" BorderWidth="2px" ForeColor="#124545" Height="16px" Width="220px" MaxLength="35" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 2px; font-style: normal; text-indent: 1px; text-align: left" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Panel ID="PanelTree" runat="server" ScrollBars="Vertical" Width="685px" Height="850px" BackColor="#B0E0D7" Wrap="true" Style="margin-top: 0px; vertical-align: top; overflow: hidden; text-align: left">
                                <asp:TreeView ID="TreeListeDossier" runat="server" MaxDataBindDepth="2" BorderStyle="None" BorderWidth="2px" BorderColor="Snow" ForeColor="#142425" ShowCheckBoxes="All" Width="685px" Height="850px" Font-Bold="True" NodeIndent="10" BackColor="#B0E0D7" LeafNodeStyle-HorizontalPadding="8px" RootNodeStyle-Font-Bold="True" RootNodeStyle-ImageUrl="~/Images/Armoire/FicheBleue.bmp" RootNodeStyle-Font-Italic="False">
                                    <SelectedNodeStyle BackColor="#6C9690" BorderColor="#E3924F" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" />
                                </asp:TreeView>
                            </asp:Panel>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="620px" HorizontalAlign="Center">
                            <asp:Label ID="EtiStatus1" runat="server" Height="15px" Width="150px" BackColor="#6D9092" Visible="true" BorderColor="#CCFFFF" BorderStyle="None" BorderWidth="2px" ForeColor="White" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-left: 0px; font-style: oblique; text-align: center" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell>
                <asp:Table ID="CadrePER" runat="server" Width="300px" HorizontalAlign="Left">
                    <asp:TableRow>
                        <asp:TableCell Width="298px" HorizontalAlign="Center" Height="42px" BackColor="#6D9092">
                            <asp:Label ID="EtiSelection" runat="server" Height="22px" Width="250px" BackColor="Transparent" Visible="true" BorderColor="#CCFFFF" BorderStyle="None" BorderWidth="2px" ForeColor="White" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-left: 0px; font-style: oblique; text-align: center" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:ListBox ID="VListePer" runat="server" Height="850px" Width="300px" AutoPostBack="true" BackColor="Snow" 
                                ForeColor="#142425" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" 
                                Style="border-color: #CCFFFF; border-width: 2px; border-style: inset; display: table-cell; font-style: normal;" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="300px" HorizontalAlign="Center">
                            <asp:Label ID="EtiStatus2" runat="server" Height="15px" Width="150px" BackColor="#6D9092" Visible="true" BorderColor="#CCFFFF" BorderStyle="None" BorderWidth="2px" ForeColor="White" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-left: 0px; font-style: oblique; text-align: center" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="2" HorizontalAlign="Center" Height="20px">
                <asp:Table ID="CadreBas" runat="server" HorizontalAlign="Left" Style="margin-top: 3px;" Width="300px" Visible="false">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Panel ID="PanelCmdBas" runat="server" Style="height: 25px; width: 200px">
                                <asp:Table ID="CadreCmdCsv" runat="server" Height="20px" CellPadding="0" CellSpacing="0" BackImageUrl="~/Images/General/Cmd_Std.bmp" BorderWidth="2px" BorderStyle="Outset" BorderColor="#B6C7E2" ForeColor="#D7FAF3" Width="150px" HorizontalAlign="Left" Style="margin-top: 3px;">
                                    <asp:TableRow VerticalAlign="Top">
                                        <asp:TableCell>
                                            <asp:Button ID="CommandeCsv" runat="server" Text="Fichier Csv" Width="125px" Height="20px" BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" BorderStyle="None" Style="margin-left: 15px; text-align: left;" ToolTip="" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:HiddenField ID="HSelLettre" runat="server" Value="" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>


