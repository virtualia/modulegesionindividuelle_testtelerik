﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VCoupleIconeEti.ascx.vb" ClassName="VCoupleIconeEti" EnableViewState="true" Inherits="Virtualia.Net.Controles_VCoupleIconeEti" %>

<asp:Table ID="VIcone" runat="server" CellPadding="1" CellSpacing="0" Width="310px" Height="150px"  style="margin-top:2px" BackColor="Window">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top" Width ="310px" BorderStyle ="None">
            <asp:ImageButton ID="CmdChoix" runat="server"  Width="96px" Height="96px" BackColor="Transparent" BorderStyle="None" style="margin-top: 10px" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell VerticalAlign="Bottom" Width ="310px" BorderStyle ="None">
            <asp:Label ID="Etiquette" runat="server" Height="28px" Width="300px" BackColor="Transparent" BorderColor="#B0E0D7"  BorderStyle="None" ForeColor="White" Font-Italic="True" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" style="margin-left: 10px;margin-top: 10px;text-align: left" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
