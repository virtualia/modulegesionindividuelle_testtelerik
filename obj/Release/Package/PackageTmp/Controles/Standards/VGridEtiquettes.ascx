﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VGridEtiquettes.ascx.vb" Inherits="Virtualia.Net.VGridEtiquettes" %>

<%@ Register src="~/Controles/Standards/VListeCombo.ascx" tagname="VListeCombo" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>

<style type="text/css">
    .LigneTable
    {
        border-style:none;
        margin-top:0px;
        position:static;
        z-index:99;
    }

    .CellEtiquette
    {
        background-color:#225C59;
        border-color:#124545;
        border-style:solid;
        border-width:1px;
    }
    .Etiquette
    {
        background-color:transparent;
        border-style:none;
        color:white;
        height:20px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:bold;
        margin-top:1px;
        padding-top: 2px;
        text-indent:2px;
        text-align:center;
        word-wrap:normal;
    }
     .CellDonnee
    {
        background-color:white;
        border-color:#124545;
        border-bottom-style:solid;
        border-left-style:solid;
        border-right-style:solid;
        border-top-style:none;
        border-width:1px;
        height:20px;
        margin-bottom:0px;
        margin-top:0px;
        vertical-align:top;
    }
    .Donnee
    {
        background-color:transparent;
        border-style:none;
        color:black;
        height:16px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        padding-top:2px;
        text-indent:2px;
        text-align:left;
    }
</style>
<asp:Panel ID="PanelGrille" runat="server" Width="1050px" Height="760px" ScrollBars="Auto" HorizontalAlign="Center" BackColor="#B0E0D7">
    <asp:Table ID="CadreGrille" runat="server" HorizontalAlign="Center" CellPadding="0" CellSpacing="0">
        <asp:TableRow>
            <asp:TableCell ID="CellTitre" HorizontalAlign="Left">
                 <asp:Label ID="EtiTitre" runat="server" Height="25px" Width="800px"
                    BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Groove"
                    BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                    style="margin-top: 5px; text-indent: 5px; text-align: center;" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ID="CellFiltre" Visible="false">
                <asp:Table ID="CadreFiltre" runat="server">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" Width="740px">
                            <Virtualia:VListeCombo ID="LstFiltres" runat="server" EtiVisible="true" EtiText=" nature " EtiWidth="60px"
                                 V_NomTable="Mois" LstWidth="250px" LstBorderColor="#B0E0D7" LstBackColor="#EEECFD" 
                                  EtiStyle="text-align:center;margin-left:0px;" EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" SiLigneBlanche="true" />
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            <Virtualia:VCoupleEtiDate ID="DDateDebut" runat="server" TypeCalendrier="Standard" EtiWidth="40px" 
                                                    V_SiDonneeDico="false" EtiText=" du " EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9"/>  
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VCoupleEtiDate ID="DDateFin" runat="server" TypeCalendrier="Standard" EtiWidth="40px" SiDateFin="true"
                                                    V_SiDonneeDico="false" EtiText=" au " EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellCadrageFiltre" Width="25px" Height="20px" HorizontalAlign="Center">
                                 <asp:Label ID="EtiCadrageFiltre" runat="server" Width="25px" Text="" />
                            </asp:TableCell>     
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ID="CellPagination">
                <asp:Table ID="CadreCommande" runat="server" HorizontalAlign="Left">
                    <asp:TableRow>
                        <asp:TableCell BackColor="#A8BBB8">
                                <asp:ImageButton ID="CmdPrecedent" runat="server" Height="18px" Width="56px" 
                                    ImageUrl="~/Images/Boutons/PagePrecedente.bmp" ToolTip="Page précédente"/> 
                            </asp:TableCell>
                            <asp:TableCell BackColor="#A8BBB8"> 
                                <asp:Label ID="LabelPage" runat="server" Height="18px" Width="55px"
                                    BackColor="#A8BBB8" BorderColor="LightGray" BorderStyle="Solid" Text="1 / 1"
                                    BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 0px; text-align:  center; padding-top: 2px;">
                                </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell  BackColor="#A8BBB8"> 
                                <asp:ImageButton ID="CmdSuivant" runat="server" Height="16px" Width="56px"
                                    ImageUrl="~/Images/Boutons/PageSuivante.bmp" ToolTip="Page suivante"/>      
                            </asp:TableCell>                 
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell> 
                <asp:Label ID="EtiNbAffiches" runat="server" Height="16px" Width="400px"
                    BackColor="Transparent" BorderStyle="none" Text=""
                    ForeColor="black" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller" 
                    style="text-indent: 0px; text-align:  center; padding-top: 2px;">
                </asp:Label>          
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
           <asp:TableCell>
               <asp:Table ID="CadreEtiquettes" runat="server" HorizontalAlign="Center" CellPadding="0" CellSpacing="0">
                   <asp:TableRow>
                        <asp:TableCell ID="CellEti1" Width="150px" CssClass="CellEtiquette">
                            <asp:Label ID="EtiCol1" runat="server" CssClass="Etiquette" Width="140px" Text="Virtualia" />
                        </asp:TableCell>
                       <asp:TableCell ID="CellEti2" Width="150px" CssClass="CellEtiquette">
                            <asp:Label ID="EtiCol2" runat="server" CssClass="Etiquette" Width="140px" Text="Virtualia" />
                        </asp:TableCell>
                       <asp:TableCell ID="CellEti3" Width="150px" CssClass="CellEtiquette">
                            <asp:Label ID="EtiCol3" runat="server" CssClass="Etiquette" Width="140px" Text="Virtualia" />
                        </asp:TableCell>
                       <asp:TableCell ID="CellEti4" Width="150px" CssClass="CellEtiquette">
                            <asp:Label ID="EtiCol4" runat="server" CssClass="Etiquette" Width="140px" Text="Virtualia" />
                        </asp:TableCell>
                       <asp:TableCell ID="CellEti5" Width="150px" CssClass="CellEtiquette">
                            <asp:Label ID="EtiCol5" runat="server" CssClass="Etiquette" Width="140px" Text="Virtualia" />
                        </asp:TableCell>
                       <asp:TableCell ID="CellEti6" Width="150px" CssClass="CellEtiquette">
                            <asp:Label ID="EtiCol6" runat="server" CssClass="Etiquette" Width="140px" Text="Virtualia" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellEti7" Width="150px" CssClass="CellEtiquette">
                            <asp:Label ID="EtiCol7" runat="server" CssClass="Etiquette" Width="140px" Text="Virtualia" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellNeutre" Width="25px" Height="20px" HorizontalAlign="Center">
                            <asp:Label ID="EtiNeutre" runat="server" Width="25px" Text="" />
                        </asp:TableCell>
                   </asp:TableRow>
               </asp:Table>
           </asp:TableCell>
       </asp:TableRow>
       <asp:TableRow>
           <asp:TableCell>
               <asp:Table ID="Ligne01" runat="server" CssClass="LigneTable" HorizontalAlign="Center" CellPadding="0" CellSpacing="0">
                   <asp:TableRow VerticalAlign="Top">
                        <asp:TableCell ID="CellDon_L01_1" Width="150px" CssClass="CellDonnee">
                            <asp:TextBox ID="DonCol_L01_1" runat="server" CssClass="Donnee" Width="140px" Text="Affichage" ToolTip=""
                                ReadOnly="true" AutoPostBack="false" TextMode="SingleLine" > 
                            </asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell ID="CellDon_L01_2" Width="150px" CssClass="CellDonnee">
                            <asp:TextBox ID="DonCol_L01_2" runat="server" CssClass="Donnee" Width="140px" Text="Affichage" ToolTip=""
                                ReadOnly="true" AutoPostBack="false" TextMode="SingleLine" >  
                            </asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell ID="CellDon_L01_3" Width="150px" CssClass="CellDonnee">
                            <asp:TextBox ID="DonCol_L01_3" runat="server" CssClass="Donnee" Width="140px" Text="Affichage" ToolTip=""
                                ReadOnly="true" AutoPostBack="false" TextMode="SingleLine" > 
                            </asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell ID="CellDon_L01_4" Width="150px" CssClass="CellDonnee">
                            <asp:TextBox ID="DonCol_L01_4" runat="server" CssClass="Donnee" Width="140px" Text="Affichage" ToolTip=""
                                ReadOnly="true" AutoPostBack="false" TextMode="SingleLine" >  
                            </asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell ID="CellDon_L01_5" Width="150px" CssClass="CellDonnee">
                            <asp:TextBox ID="DonCol_L01_5" runat="server" CssClass="Donnee" Width="140px" Text="Affichage" ToolTip=""
                                ReadOnly="true" AutoPostBack="false" TextMode="SingleLine" > 
                            </asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell ID="CellDon_L01_6" Width="150px" CssClass="CellDonnee">
                            <asp:TextBox ID="DonCol_L01_6" runat="server" CssClass="Donnee" Width="140px" Text="Affichage" ToolTip=""
                                ReadOnly="true" AutoPostBack="false" TextMode="SingleLine" > 
                            </asp:TextBox>
                        </asp:TableCell>
                       <asp:TableCell ID="CellDon_L01_7" Width="150px" CssClass="CellDonnee">
                            <asp:TextBox ID="DonCol_L01_7" runat="server" CssClass="Donnee" Width="140px" Text="Affichage" ToolTip=""
                                ReadOnly="true" AutoPostBack="false" TextMode="SingleLine" > 
                            </asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell ID="CellCmd_L01" Width="25px" CssClass="CellDonnee">
                            <asp:ImageButton ID="CmdCol_L01" runat="server" Width="25px" Height="19px" 
                                 ImageUrl="~/Images/Boutons/Modifier.bmp" Tooltip="Supprimer la fiche" />
                        </asp:TableCell>
                    </asp:TableRow>
               </asp:Table>
           </asp:TableCell>
       </asp:TableRow>
    </asp:Table>
</asp:Panel>
