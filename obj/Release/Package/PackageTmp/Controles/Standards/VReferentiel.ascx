﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VReferentiel.ascx.vb" Inherits="Virtualia.Net.VReferentiel" %>

<style type="text/css">
    .LaLettre
    {  
        background-color:#124545;
        color:#D7FAF3;
        border-color:#B0E0D7;
        border-style:inset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:bold;
        text-indent:0px;
        text-align:center;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        padding-left:2px;
        vertical-align:middle;
        word-wrap:normal;
        height:24px;
        width:24px;
    }
    .Loption
    {  
        background-color:#7D9F99;
        color:#E9FDF9;
        border-color:#B0E0D7;
        border-style:inset;
        border-width:2px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:oblique;
        font-weight:bold;
        margin-top:0px;
        margin-left:0px;
        margin-bottom:0px;
        padding-left:2px;
        text-align:left;
        text-indent:5px;
        vertical-align:middle;
        word-wrap:normal;
        height:20px;
        width:220px;
    }
</style>

<asp:Table ID="CadreReferentiel" runat="server" BackColor="#5E9598" BorderColor="#B0E0D7" BorderStyle="Ridge" BorderWidth="4px" Width="630px">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreHaut" runat="server">
                <asp:TableRow>
                    <asp:TableCell ID="CellTitre">
                        <asp:Label ID="EtiTitre" runat="server" Height="20px" Width="600px" BackColor="#8DA8A3" Visible="true" BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="2px" ForeColor="#E9FDF9" 
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Text="Référentiel" Style="margin-top:10px;margin-bottom:3px;font-style:oblique; text-align:center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="CommandeRetour" runat="server" Height="20px" Width="20px" BackColor="Transparent" ForeColor="Black"
                            BorderStyle="None" BorderColor="#216B68" BorderWidth="1px" Text=" x" ToolTip="Fermer"
                            Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Medium" Font-Italic="false"
                            style="margin-top:0px;text-indent:0px;text-align:center;vertical-align: middle" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreLettre" runat="server" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="ButtonA" runat="server" Text="A" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonB" runat="server" Text="B" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonC" runat="server" Text="C" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonD" runat="server" Text="D" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonE" runat="server" Text="E" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonF" runat="server" Text="F" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonG" runat="server" Text="G" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonH" runat="server" Text="H" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonI" runat="server" Text="I" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonJ" runat="server" Text="J" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonK" runat="server" Text="K" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonL" runat="server" Text="L" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonM" runat="server" Text="M" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonN" runat="server" Text="N" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonO" runat="server" Text="O" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonP" runat="server" Text="P" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonQ" runat="server" Text="Q" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonR" runat="server" Text="R" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonS" runat="server" Text="S" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonT" runat="server" Text="T" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonU" runat="server" Text="U" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonV" runat="server" Text="V" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonW" runat="server" Text="W" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonX" runat="server" Text="X" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonY" runat="server" Text="Y" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonZ" runat="server" Text="Z" CssClass="LaLettre" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ButtonTout" runat="server" Text="@" CssClass="LaLettre" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreRecherche" runat="server" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiRecherche" runat="server" Height="20px" Width="80px" Text="Aller à" BackColor="#8DA8A3" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#E9FDF9" 
                            Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top:0px; text-indent:5px; text-align:center" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:TextBox ID="VRecherche" runat="server" BackColor="Snow" BorderColor="#B0E0D7" BorderStyle="Inset" BorderWidth="1px" Height="16px" Width="290px" Font-Bold="false" ForeColor="#124545" 
                            Style="margin-left:2px;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:ImageButton ID="CmdRecherche" runat="server" Height="17px" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ImageAlign="Middle" ImageUrl="~/Images/Boutons/Executer_Gris.bmp" ToolTip="Recherche d'une valeur"
                                Style="margin-right:10px;margin-bottom:0px" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="CommandeBlanc" runat="server" Text="Remettre à blanc" Width="120px" Height="24px" BackColor="#8DA8A3" BorderColor="#B0E0D7" ForeColor="#E9FDF9" Visible="true" 
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" BorderStyle="Outset" Style="margin-left:2px; text-align: center;" ToolTip="Remettre à blanc la donnée sélectionnée" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreControle" runat="server" HorizontalAlign="Center" Style="margin-top:5px;margin-top:1px;background-attachment: inherit;display:table-cell;">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table ID="CadreRadio" runat="server" HorizontalAlign="Center">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:RadioButton ID="RadioV0" runat="server" Text="Liste alphabétique" AutoPostBack="true" GroupName="OptionV" Checked="true" CssClass="Loption" Width="200px"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:RadioButton ID="RadioV1" runat="server" Text="Liste organisée" AutoPostBack="true" GroupName="OptionV" CssClass="Loption" Width="200px"/>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:RadioButton ID="RadioV2" runat="server" Text="Liste par référence" AutoPostBack="true" GroupName="OptionV" CssClass="Loption" Width="200px"/>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ID="CellCommandes" Visible="false">
                            <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0" BackImageUrl="~/Images/Boutons/NewSuppOK_Std.bmp" 
                                BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#FFF2DB" HorizontalAlign="Right" Style="margin-top:3px; margin-right:0px" Visible="true">
                                <asp:TableRow VerticalAlign="Top">
                                    <asp:TableCell HorizontalAlign="Right">
                                        <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px" BackColor="Transparent" BorderStyle="None"  
                                            BorderColor="#B0E0D7" ForeColor="#E9FDF9" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" 
                                            Style="margin-left:10px;text-align:center;border-right-style:outset;border-right-width:3px" ToolTip="Nouvelle fiche" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right">
                                        <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px" BackColor="Transparent" 
                                            BorderColor="#B0E0D7" ForeColor="#E9FDF9" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" BorderStyle="None" 
                                            Style="margin-left:15px;text-align:center;" ToolTip="Supprimer la fiche" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell BackColor="Snow">
                            <asp:Panel ID="PanelListe" runat="server" Height="500px" ScrollBars="Auto" style="text-align:left;">
                                <asp:TreeView ID="TreeListeRef" runat="server" MaxDataBindDepth="2" BorderStyle="None" BorderWidth="2px" BorderColor="#CCFFCC" 
                                    ForeColor="#124545" Font-Names="Trebuchet MS" Width="590px" Height="480px" Font-Bold="False" NodeIndent="10" 
                                    BackColor="Snow" ImageSet="XPFileExplorer" Font-Size="Small" Font-Italic="true" RootNodeStyle-HorizontalPadding="10px" LeafNodeStyle-HorizontalPadding="10px" 
                                    Style="margin-bottom: 1px; margin-left: 30px; display: table-cell">
                                    <SelectedNodeStyle BackColor="#6C9690" BorderColor="#BFB8AB" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" />
                                    <Nodes>
                                        <asp:TreeNode PopulateOnDemand="False" Text="Système de référence" Value="SystemeReference" ImageUrl="~/Images/Armoire/VertFonceOuvert16.bmp" />
                                    </Nodes>
                                </asp:TreeView>
                            </asp:Panel>
                        </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                            <asp:Label ID="EtiStatus1" runat="server" Height="16px" Width="635px" BackColor="#B0E0D7" Visible="true" BorderColor="#B0E0D7" BorderStyle="None" ForeColor="#124545" 
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true" Style="text-align:center" />
                        </asp:TableCell>
                        </asp:TableRow>
                </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
   

   