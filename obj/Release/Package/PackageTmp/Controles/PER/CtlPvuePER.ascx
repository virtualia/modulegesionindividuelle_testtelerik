﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlPvuePER.ascx.vb" Inherits="Virtualia.Net.CtlPvuePER" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Standards/VMenuCommun.ascx" tagname="VMenu" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VReferentiel.ascx" tagname="VReference" tagprefix="Virtualia" %>
<%@ Register Src="~/Controles/REF/Grade/VReferentielGrades.ascx" TagName="VRefGrade" TagPrefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>
<%@ Register src="PER_ETATCIVIL.ascx" tagname="PER_ETATCIVIL" tagprefix="Virtualia" %>
<%@ Register src="PER_PIECE_IDENTITE.ascx" tagname="PER_PIECE_IDENTITE" tagprefix="Virtualia" %>
<%@ Register src="PER_DOMICILE.ascx" tagname="PER_DOMICILE" tagprefix="Virtualia" %>
<%@ Register src="PER_DOMICILE2.ascx" tagname="PER_DOMICILE2" tagprefix="Virtualia" %>
<%@ Register src="PER_RESIDENCE_CONGE.ascx" tagname="PER_RESIDENCE_CONGE" tagprefix="Virtualia" %>
<%@ Register src="PER_ADRESSE_ETRANGER.ascx" tagname="PER_ADRESSE_ETRANGER" tagprefix="Virtualia" %>
<%@ Register src="PER_BANQUE.ascx" tagname="PER_BANQUE" tagprefix="Virtualia" %>
<%@ Register src="PER_ENFANT.ascx" tagname="PER_ENFANT" tagprefix="Virtualia" %>
<%@ Register src="PER_CONJOINT.ascx" tagname="PER_CONJOINT" tagprefix="Virtualia" %>
<%@ Register src="PER_APREVENIR.ascx" tagname="PER_APREVENIR" tagprefix="Virtualia" %>
<%@ Register src="PER_DECORATION.ascx" tagname="PER_DECORATION" tagprefix="Virtualia" %>
<%@ Register src="PER_DIPLOME.ascx" tagname="PER_DIPLOME" tagprefix="Virtualia" %>
<%@ Register src="PER_QUALIFICATION.ascx" tagname="PER_QUALIFICATION" tagprefix="Virtualia" %>
<%@ Register src="PER_SPECIALITE.ascx" tagname="PER_SPECIALITE" tagprefix="Virtualia" %>
<%@ Register src="PER_THESE.ascx" tagname="PER_THESE" tagprefix="Virtualia" %>
<%@ Register src="PER_EXPERIENCE.ascx" tagname="PER_EXPERIENCE" tagprefix="Virtualia" %>
<%@ Register src="PER_LANG_ETRANGER.ascx" tagname="PER_LANG_ETRANGER" tagprefix="Virtualia" %>
<%@ Register src="PER_ACQUIS.ascx" tagname="PER_ACQUIS" tagprefix="Virtualia" %>
<%@ Register src="PER_STAGE.ascx" tagname="PER_STAGE" tagprefix="Virtualia" %>
<%@ Register src="PER_LOISIR.ascx" tagname="PER_LOISIR" tagprefix="Virtualia" %>
<%@ Register src="PER_COLLECTIVITE.ascx" tagname="PER_COLLECTIVITE" tagprefix="Virtualia" %>
<%@ Register src="PER_IDENTIFICATION.ascx" tagname="PER_IDENTIFICATION" tagprefix="Virtualia" %>
<%@ Register src="PER_ADRESSEPRO.ascx" tagname="PER_ADRESSEPRO" tagprefix="Virtualia" %>
<%@ Register src="PER_AFFECTATION.ascx" tagname="PER_AFFECTATION" tagprefix="Virtualia" %>
<%@ Register src="PER_AFFECTATION2.ascx" tagname="PER_AFFECTATION2" tagprefix="Virtualia" %>
<%@ Register src="PER_AFFECTATION3.ascx" tagname="PER_AFFECTATION3" tagprefix="Virtualia" %>
<%@ Register src="PER_AFFECTATION4.ascx" tagname="PER_AFFECTATION4" tagprefix="Virtualia" %>
<%@ Register src="PER_AFFECTATION5.ascx" tagname="PER_AFFECTATION5" tagprefix="Virtualia" %>
<%@ Register src="PER_AFFECTATION6.ascx" tagname="PER_AFFECTATION6" tagprefix="Virtualia" %>
<%@ Register src="PER_LOLF.ascx" tagname="PER_LOLF" tagprefix="Virtualia" %>
<%@ Register src="PER_EMPLOIBUD.ascx" tagname="PER_EMPLOIBUD" tagprefix="Virtualia" %>
<%@ Register src="PER_ACTIANNEXE.ascx" tagname="PER_ACTIANNEXE" tagprefix="Virtualia" %>
<%@ Register src="PER_ACTI_INTERNE.ascx" tagname="PER_ACTI_INTERNE" tagprefix="Virtualia" %>
<%@ Register src="PER_STATUT.ascx" tagname="PER_STATUT" tagprefix="Virtualia" %>
<%@ Register src="PER_POSITION.ascx" tagname="PER_POSITION" tagprefix="Virtualia" %>
<%@ Register src="PER_GRADE.ascx" tagname="PER_GRADE" tagprefix="Virtualia" %>
<%@ Register src="PER_INTEGRATION.ascx" tagname="PER_INTEGRATION" tagprefix="Virtualia" %>
<%@ Register src="PER_BONIFICATION.ascx" tagname="PER_BONIFICATION" tagprefix="Virtualia" %>
<%@ Register src="PER_SANCTION.ascx" tagname="PER_SANCTION" tagprefix="Virtualia" %>
<%@ Register src="PER_STATUT_DETACHE.ascx" tagname="PER_STATUT_DETACHE" tagprefix="Virtualia" %>
<%@ Register src="PER_POSITION_DETACHE.ascx" tagname="PER_POSITION_DETACHE" tagprefix="Virtualia" %>
<%@ Register src="PER_GRADE_DETACHE.ascx" tagname="PER_GRADE_DETACHE" tagprefix="Virtualia" %>
<%@ Register src="PER_SERVICECIVIL.ascx" tagname="PER_SERVICECIVIL" tagprefix="Virtualia" %>
<%@ Register src="PER_SERVICEMILITAIRE.ascx" tagname="PER_SERVICEMILITAIRE" tagprefix="Virtualia" %>
<%@ Register src="PER_CUMULREM.ascx" tagname="PER_CUMULREM" tagprefix="Virtualia" %>
<%@ Register src="PER_VISITE_MEDICAL.ascx" tagname="PER_VISITE_MEDICAL" tagprefix="Virtualia" %>
<%@ Register src="PER_HANDICAP.ascx" tagname="PER_HANDICAP" tagprefix="Virtualia" %>
<%@ Register src="PER_VACCINATION.ascx" tagname="PER_VACCINATION" tagprefix="Virtualia" %>
<%@ Register src="PER_PREVENTION.ascx" tagname="PER_PREVENTION" tagprefix="Virtualia" %>
<%@ Register src="PER_ABSENCE.ascx" tagname="PER_ABSENCE" tagprefix="Virtualia" %>
<%@ Register src="PER_AGENDA.ascx" tagname="PER_AGENDA" tagprefix="Virtualia" %>
<%@ Register src="PER_MANDAT.ascx" tagname="PER_MANDAT" tagprefix="Virtualia" %>
<%@ Register src="PER_AVANTAGE.ascx" tagname="PER_AVANTAGE" tagprefix="Virtualia" %>
<%@ Register src="PER_AFFILIATION.ascx" tagname="PER_AFFILIATION" tagprefix="Virtualia" %>
<%@ Register src="PER_MUTUELLE.ascx" tagname="PER_MUTUELLE" tagprefix="Virtualia" %>
<%@ Register src="PER_MATERIEL.ascx" tagname="PER_MATERIEL" tagprefix="Virtualia" %>
<%@ Register src="PER_ANNOTATION.ascx" tagname="PER_ANNOTATION" tagprefix="Virtualia" %>
<%@ Register src="PER_DOCUMENT.ascx" tagname="PER_DOCUMENT" tagprefix="Virtualia" %>

<asp:Table ID="CadreGlobal" runat="server" HorizontalAlign="Center" Width="1140px">
        <asp:TableRow>
        <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
            <asp:Label ID="EtiTitre" runat="server" Text="SITUATION EN GESTION" Height="22px" Width="700px"
                        BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" ForeColor="#D7FAF3"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                        style="font-style: normal; text-indent: 5px; text-align: center;">
                </asp:Label>     
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
            <Virtualia:VMenu ID="PER_MENU" runat="server" V_Appelant="PER"></Virtualia:VMenu>
        </asp:TableCell>
        <asp:TableCell ID="CellulePER" runat="server" Width="800px" VerticalAlign="Top">
            <asp:MultiView ID="MultiPER" runat="server" ActiveViewIndex="0">
                <asp:View ID="VueEtatCivil" runat="server">
                    <Virtualia:PER_ETATCIVIL ID="PER_ETATCIVIL_1" runat="server"></Virtualia:PER_ETATCIVIL>
                </asp:View>
                <asp:View ID="VuePieceIdentite" runat="server">
                    <Virtualia:PER_PIECE_IDENTITE ID="PER_PIECE_IDENTITE1" runat="server"></Virtualia:PER_PIECE_IDENTITE>
                </asp:View>
                <asp:View ID="VueAdresse" runat="server">
                    <Virtualia:PER_DOMICILE ID="PER_DOMICILE_1" runat="server"></Virtualia:PER_DOMICILE>
                </asp:View>
                <asp:View ID="VueAdresse2" runat="server">
                    <Virtualia:PER_DOMICILE2 ID="PER_DOMICILE_2" runat="server"></Virtualia:PER_DOMICILE2>
                </asp:View>
                <asp:View ID="VueResidenceConge" runat="server">
                    <Virtualia:PER_RESIDENCE_CONGE ID="PER_RESIDENCE_CONGE" runat="server"></Virtualia:PER_RESIDENCE_CONGE>
                </asp:View>
                <asp:View ID="VueAdresseEtranger" runat="server">
                    <Virtualia:PER_ADRESSE_ETRANGER ID="PER_ADRESSE_ETRANGER" runat="server"></Virtualia:PER_ADRESSE_ETRANGER>
                </asp:View>
                <asp:View ID="VueBanque" runat="server">
                    <Virtualia:PER_BANQUE ID="PER_BANQUE_5" runat="server"></Virtualia:PER_BANQUE>
                </asp:View>
                <asp:View ID="VueEnfant" runat="server">
                    <Virtualia:PER_ENFANT ID="PER_ENFANT_3" runat="server"></Virtualia:PER_ENFANT>
                </asp:View>
                <asp:View ID="VueConjoint" runat="server">
                    <Virtualia:PER_CONJOINT ID="PER_CONJOINT1" runat="server"></Virtualia:PER_CONJOINT>
                </asp:View>
                <asp:View ID="VuePrevenir" runat="server">
                    <Virtualia:PER_APREVENIR ID="PER_APREVENIR2" runat="server"></Virtualia:PER_APREVENIR>
                </asp:View>
                <asp:View ID="VueDecoration" runat="server">
                    <Virtualia:PER_DECORATION ID="PER_DECORATION" runat="server"></Virtualia:PER_DECORATION>
                </asp:View>
                <asp:View ID="VueDiplome" runat="server">
                    <Virtualia:PER_DIPLOME ID="PER_DIPLOME" runat="server"></Virtualia:PER_DIPLOME>
                </asp:View>
                <asp:View ID="VueQualif" runat="server">
                    <Virtualia:PER_QUALIFICATION ID="PER_QUALIFICATION" runat="server"></Virtualia:PER_QUALIFICATION>
                </asp:View>
                <asp:View ID="VueSpecialite" runat="server">
                    <Virtualia:PER_SPECIALITE ID="PER_SPECIALITE" runat="server"></Virtualia:PER_SPECIALITE>
                </asp:View>
                <asp:View ID="Vuethese" runat="server">
                    <Virtualia:PER_THESE ID="PER_THESE" runat="server"></Virtualia:PER_THESE>
                </asp:View>
                <asp:View ID="VueExperience" runat="server">
                    <Virtualia:PER_EXPERIENCE ID="PER_EXPERIENCE" runat="server"></Virtualia:PER_EXPERIENCE>
                </asp:View>
                <asp:View ID="VueLangueEtrangeres" runat="server">
                    <Virtualia:PER_LANG_ETRANGER ID="PER_LANG_ETRANGER" runat="server"></Virtualia:PER_LANG_ETRANGER>
                </asp:View>
                <asp:View ID="VueAcquis" runat="server">
                    <Virtualia:PER_ACQUIS ID="PER_ACQUIS" runat="server"></Virtualia:PER_ACQUIS>
                </asp:View>
                <asp:View ID="VueStage" runat="server">
                    <Virtualia:PER_STAGE ID="PER_STAGE" runat="server"></Virtualia:PER_STAGE>
                </asp:View>
                <asp:View ID="VueLoisir" runat="server">
                    <Virtualia:PER_LOISIR ID="PER_LOISIR" runat="server"></Virtualia:PER_LOISIR>
                </asp:View>
                <asp:View ID="VueAdministration" runat="server">
                    <Virtualia:PER_COLLECTIVITE ID="PER_COLLECTIVITE_26" runat="server"></Virtualia:PER_COLLECTIVITE>
                </asp:View>
                <asp:View ID="VueIdentification" runat="server">
                    <Virtualia:PER_IDENTIFICATION ID="PER_IDENTIFICATION" runat="server"></Virtualia:PER_IDENTIFICATION>
                </asp:View>
                <asp:View ID="VueAdressePro" runat="server">
                    <Virtualia:PER_ADRESSEPRO ID="PER_ADRESSEPRO_24" runat="server"></Virtualia:PER_ADRESSEPRO>
                </asp:View>
                <asp:View ID="VueAffectation" runat="server">
                    <Virtualia:PER_AFFECTATION ID="PER_AFFECTATION_17" runat="server"></Virtualia:PER_AFFECTATION>
                </asp:View>
                <asp:View ID="VueAffectation2" runat="server">
                    <Virtualia:PER_AFFECTATION2 ID="PER_AFFECTATION2" runat="server"></Virtualia:PER_AFFECTATION2>
                </asp:View>
                <asp:View ID="VueAffectation3" runat="server">
                    <Virtualia:PER_AFFECTATION3 ID="PER_AFFECTATION3" runat="server"></Virtualia:PER_AFFECTATION3>
                </asp:View>
                <asp:View ID="VueAffectation4" runat="server">
                    <Virtualia:PER_AFFECTATION4 ID="PER_AFFECTATION4" runat="server"></Virtualia:PER_AFFECTATION4>
                </asp:View>
                <asp:View ID="VueAffectation5" runat="server">
                    <Virtualia:PER_AFFECTATION5 ID="PER_AFFECTATION5" runat="server"></Virtualia:PER_AFFECTATION5>
                </asp:View>
                <asp:View ID="VueAffectation6" runat="server">
                    <Virtualia:PER_AFFECTATION6 ID="PER_AFFECTATION6" runat="server"></Virtualia:PER_AFFECTATION6>
                </asp:View>
                <asp:View ID="VueLOLF" runat="server">
                    <Virtualia:PER_LOLF ID="PER_LOLF_92" runat="server"></Virtualia:PER_LOLF>
                </asp:View>
                <asp:View ID="VueEmploiBud" runat="server">
                    <Virtualia:PER_EMPLOIBUD ID="PER_EMPLOIBUD" runat="server"></Virtualia:PER_EMPLOIBUD>
                </asp:View>
                <asp:View ID="VueActiAnnexe" runat="server">
                    <Virtualia:PER_ACTIANNEXE ID="PER_ACTIANNEXE" runat="server"></Virtualia:PER_ACTIANNEXE>
                </asp:View>
                <asp:View ID="VueActiInterne" runat="server">
                    <Virtualia:PER_ACTI_INTERNE ID="PER_ACTI_INTERNE" runat="server"></Virtualia:PER_ACTI_INTERNE>
                </asp:View>
                <asp:View ID="VueStatut" runat="server">
                    <Virtualia:PER_STATUT ID="PER_STATUT_12" runat="server"></Virtualia:PER_STATUT>
                </asp:View>
                <asp:View ID="VuePosition" runat="server">
                    <Virtualia:PER_POSITION ID="PER_POSITION_13" runat="server"></Virtualia:PER_POSITION>
                </asp:View>
                <asp:View ID="VueGrade" runat="server">
                    <Virtualia:PER_GRADE ID="PER_GRADE_14" runat="server"></Virtualia:PER_GRADE>
                </asp:View>
                <asp:View ID="VueIntegration" runat="server">
                    <Virtualia:PER_INTEGRATION ID="PER_INTEGRATION" runat="server"></Virtualia:PER_INTEGRATION>
                </asp:View>
                <asp:View ID="VueBonification" runat="server">
                    <Virtualia:PER_BONIFICATION ID="PER_BONIFICATION" runat="server"></Virtualia:PER_BONIFICATION>
                </asp:View>
                <asp:View ID="VueSanction" runat="server">
                    <Virtualia:PER_SANCTION ID="PER_SANCTION" runat="server"></Virtualia:PER_SANCTION>
                </asp:View>
                <asp:View ID="VueStatutDetache" runat="server">
                    <Virtualia:PER_STATUT_DETACHE ID="PER_STATUT_54" runat="server"></Virtualia:PER_STATUT_DETACHE>
                </asp:View>
                <asp:View ID="VuePositionDetache" runat="server">
                    <Virtualia:PER_POSITION_DETACHE ID="PER_POSITION_55" runat="server"></Virtualia:PER_POSITION_DETACHE>
                </asp:View>
                <asp:View ID="VueGradeDetache" runat="server">
                    <Virtualia:PER_GRADE_DETACHE ID="PER_GRADE_56" runat="server"></Virtualia:PER_GRADE_DETACHE>
                </asp:View>
                <asp:View ID="VueServiceCivil" runat="server">
                    <Virtualia:PER_SERVICECIVIL ID="PER_SERVICECIVIL" runat="server"></Virtualia:PER_SERVICECIVIL>
                </asp:View>
                <asp:View ID="VueArmee" runat="server">
                    <Virtualia:PER_SERVICEMILITAIRE ID="PER_SERVICEMILITAIRE" runat="server"></Virtualia:PER_SERVICEMILITAIRE>
                </asp:View>
                <asp:View ID="VueCumulRem" runat="server">
                    <Virtualia:PER_CUMULREM ID="PER_CUMULREM" runat="server"></Virtualia:PER_CUMULREM>
                </asp:View>
                <asp:View ID="VueMedical" runat="server">
                    <Virtualia:PER_VISITE_MEDICAL ID="PER_VISITE_MEDICAL" runat="server"></Virtualia:PER_VISITE_MEDICAL>
                </asp:View>
                <asp:View ID="VueHandicap" runat="server">
                    <Virtualia:PER_HANDICAP ID="PER_HANDICAP" runat="server"></Virtualia:PER_HANDICAP>
                </asp:View>
                <asp:View ID="VueVaccination" runat="server">
                    <Virtualia:PER_VACCINATION ID="PER_VACCINATION" runat="server"></Virtualia:PER_VACCINATION>
                </asp:View>
                <asp:View ID="VuePrevention" runat="server">
                    <Virtualia:PER_PREVENTION ID="PER_PREVENTION" runat="server"></Virtualia:PER_PREVENTION>
                </asp:View>
                <asp:View ID="VueAbsence" runat="server">
                    <Virtualia:PER_ABSENCE ID="PER_ABSENCE_15" runat="server"></Virtualia:PER_ABSENCE>
                </asp:View>
                <asp:View ID="VueAgenda" runat="server">
                    <Virtualia:PER_AGENDA ID="PER_AGENDA" runat="server"></Virtualia:PER_AGENDA>
                </asp:View>
                <asp:View ID="VueMandat" runat="server">
                    <Virtualia:PER_MANDAT ID="PER_MANDAT" runat="server"></Virtualia:PER_MANDAT>
                </asp:View>
                <asp:View ID="VueAvantage" runat="server">
                    <Virtualia:PER_AVANTAGE ID="PER_AVANTAGE" runat="server"></Virtualia:PER_AVANTAGE>
                </asp:View>
                <asp:View ID="VueAffiliation" runat="server">
                    <Virtualia:PER_AFFILIATION ID="PER_AFFILIATION" runat="server"></Virtualia:PER_AFFILIATION>
                </asp:View>
                <asp:View ID="VueMutuelle" runat="server">
                    <Virtualia:PER_MUTUELLE ID="PER_MUTUELLE" runat="server"></Virtualia:PER_MUTUELLE>
                </asp:View>
                <asp:View ID="VueMateriel" runat="server">
                    <Virtualia:PER_MATERIEL ID="PER_MATERIEL" runat="server"></Virtualia:PER_MATERIEL>
                </asp:View>
                <asp:View ID="VueAnnotation" runat="server">
                    <Virtualia:PER_ANNOTATION ID="PER_ANNOTATION" runat="server"></Virtualia:PER_ANNOTATION>
                </asp:View>
                <asp:View ID="VueDocument" runat="server">
                    <Virtualia:PER_DOCUMENT ID="PER_DOCUMENT" runat="server"></Virtualia:PER_DOCUMENT>
                </asp:View>       
            </asp:MultiView>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ID="CellMessage" Visible="false">
            <ajaxToolkit:ModalPopupExtender ID="PopupMsg" runat="server" TargetControlID="HPopupMsg" PopupControlID="PanelMsgPopup" BackgroundCssClass="modalBackground" />
            <asp:Panel ID="PanelMsgPopup" runat="server">
                <Virtualia:VMessage id="MsgVirtualia" runat="server" />
            </asp:Panel>
            <asp:HiddenField ID="HPopupMsg" runat="server" Value="0" />
        </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ID="CellReference" Visible="false">
                <ajaxToolkit:ModalPopupExtender ID="PopupReferentiel" runat="server" TargetControlID="HPopupRef" PopupControlID="PanelRefPopup" BackgroundCssClass="modalBackground" />
                <asp:Panel ID="PanelRefPopup" runat="server">
                    <Virtualia:VReference ID="RefVirtualia" runat="server" />
                </asp:Panel>
                <asp:HiddenField ID="HPopupRef" runat="server" Value="0" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
             <asp:TableCell ID="CellRefGrade" Visible="false">
                 <ajaxToolkit:ModalPopupExtender ID="PopupRefGrade" runat="server" TargetControlID="HPopupRefGrade" PopupControlID="PanelRefGradePopup" BackgroundCssClass="modalBackground" />
                 <asp:Panel ID="PanelRefGradePopup" runat="server">
                    <Virtualia:VRefGrade ID="RefGradeGrilles" runat="server"/>
                </asp:Panel>
                <asp:HiddenField ID="HPopupRefGrade" runat="server" Value="0" />
             </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
            </asp:TableCell>
    </asp:TableRow>
</asp:Table>
   
