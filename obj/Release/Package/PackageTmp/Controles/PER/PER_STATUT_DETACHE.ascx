﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_STATUT_DETACHE" Codebehind="PER_STATUT_DETACHE.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

 <asp:Table ID="CadreControle" runat="server" Height="30px" Width="600px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
    <asp:TableRow>
        <asp:TableCell Height="20px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow> 
        <asp:TableCell> 
            <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="530px" V_NiveauCharte="0" SiColonneSelect="true" SiCaseAcocher="false"/>
        </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BackColor="#CBF2BE"
                BorderColor="#B0E0D7" Height="350px" Width="520px" HorizontalAlign="Center" style="margin-top: 3px;">
                <asp:TableRow>
                    <asp:TableCell>
                      <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                        Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Bottom">
                                <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                                    Width="160px" CellPadding="0" CellSpacing="0"
                                    BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                                BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                                Tooltip="Nouvelle fiche">
                                            </asp:Button>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                                BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                                Tooltip="Supprimer la fiche">
                                            </asp:Button>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>    
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Bottom">
                                <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                                    BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                    CellPadding="0" CellSpacing="0" Visible="false">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                                BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                                </asp:Button>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>  
                            </asp:TableCell>
                        </asp:TableRow>
                      </asp:Table>    
                    </asp:TableCell>
                </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="550px" 
                            CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="Etiquette" runat="server" Height="20px" Width="400px"
                                        Text="Emploi fonctionnel - Statut d'emploi - Détachement"
                                        BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                                        BorderWidth="2px" ForeColor="#D7FAF3"
                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px;
                                        font-style: oblique; text-indent: 5px; text-align: center;">
                                    </asp:Label>          
                                </asp:TableCell>      
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                 </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="550px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDate ID="InfoD00" runat="server" TypeCalendrier="Standard" EtiWidth="170px" EtiBackColor="#CBF2BE"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="54" V_Information="0" DonTabIndex="101"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server" EtiBackColor="#CBF2BE"
                                       V_PointdeVue="1" V_Objet="54" V_Information="1" V_SiDonneeDico="true"
                                       DonTabIndex="102" V_SiFenetrePer_Bis="true"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server" EtiBackColor="#CBF2BE"
                                       V_PointdeVue="1" V_Objet="54" V_Information="2" V_SiDonneeDico="true"
                                       DonTabIndex="103" V_SiFenetrePer_Bis="true"/> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <asp:Table ID="CadreArrete" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDate ID="InfoD03" runat="server" TypeCalendrier="Standard" EtiWidth="200px" EtiBackColor="#CBF2BE"
                                                       V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="54" V_Information="3" DonTabIndex="104"/>
                                            </asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" EtiVisible="false" EtiBackColor="#CBF2BE" DonWidth="140px"
                                                       V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="54" V_Information="4" DonTabIndex="105" DonStyle="margin-left:2px;margin-top:0px"/>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiContrat" runat="server" Height="20px" Width="250px" Text="Si non titulaire et/ou contractuel"
                                       BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                       BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 8px; margin-bottom: 8px; text-indent: 5px; text-align: center">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDate ID="InfoD08" runat="server" TypeCalendrier="Standard" EtiWidth="170px" SiDateFin="true" EtiText="Date de fin de contrat"
                                          V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="54" V_Information="8" DonTabIndex="106" EtiBackColor="#CBF2BE" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" V_PointdeVue="1" V_Objet="54" V_Information="7" V_SiDonneeDico="true"
                                       DonWidth="20px" DonTabIndex="107" EtiBackColor="#CBF2BE" V_SiFenetrePer_Bis="true"/>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                    <asp:Label ID="EtiRemu" runat="server" Height="20px" Width="250px" Text="Rémunération"
                                       BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                       BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true"
                                       Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                       style="margin-top: 8px; margin-bottom: 8px; text-indent: 5px; text-align: center">
                                    </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                     <Virtualia:VDuoEtiquetteCommande ID="Dontab05" runat="server"
                                       V_PointdeVue="1" V_Objet="54" V_Information="5" V_SiDonneeDico="true"
                                       DonTabIndex="108" EtiBackColor="#CBF2BE" V_SiFenetrePer_Bis="true"/> 
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <Virtualia:VDuoEtiquetteCommande ID="Dontab06" runat="server"
                                       V_PointdeVue="1" V_Objet="54" V_Information="6" V_SiDonneeDico="true"
                                       DonTabIndex="109" EtiBackColor="#CBF2BE" V_SiFenetrePer_Bis="true"/>  
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="12px" Columnspan="2"></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>      
                    </asp:TableCell>
                </asp:TableRow>
             </asp:Table>
         </asp:TableCell>
     </asp:TableRow>
 </asp:Table>
