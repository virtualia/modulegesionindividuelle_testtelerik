﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_COLLECTIVITE" Codebehind="PER_COLLECTIVITE.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" tagname="VCoupleEtiquetteExperte" tagprefix="Virtualia" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="600px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
  <asp:TableRow> 
    <asp:TableCell> 
      <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="530px" SiColonneSelect="true"
                                        SiCaseAcocher="false"/>
    </asp:TableCell>
  </asp:TableRow>
  <asp:TableRow>
    <asp:TableCell>
      <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
        BorderColor="#B0E0D7" Height="350px" Width="520px" HorizontalAlign="Center" style="margin-top: 3px;">
        <asp:TableRow>
          <asp:TableCell>
            <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                            Width="160px" CellPadding="0" CellSpacing="0"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Nouvelle fiche">
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Supprimer la fiche">
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="false">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="550px" 
                CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="Administration / Collectivité" Height="20px" Width="300px"
                            BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px;
                            font-style: oblique; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="550px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="250px">
                        <Virtualia:VCoupleEtiDate ID="InfoD00" runat="server" TypeCalendrier="Standard" EtiWidth="120px"
                               V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="26" V_Information="0" DonTabIndex="1"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server"
                           V_PointdeVue="1" V_Objet="26" V_Information="1" V_SiDonneeDico="true"
                           DonTabIndex="2"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab10" runat="server"
                           V_PointdeVue="1" V_Objet="26" V_Information="10" V_SiDonneeDico="true"
                           DonTabIndex="3"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDate ID="InfoD12" runat="server" TypeCalendrier="Standard" SiDateFin="true" EtiWidth="170px"
                               V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="26" V_Information="12" DonTabIndex="4"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab11" runat="server" V_PointdeVue="1" V_Objet="26" V_Information="11" V_SiDonneeDico="true"
                           DonTabIndex="5"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center"> 
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV13" runat="server"
                           V_PointdeVue="1" V_Objet="26" V_Information="13" V_SiDonneeDico="true"
                           EtiWidth="402px" EtiHeight="35px" DonWidth="400px" DonTabIndex="6"
                           DonTextMode="true" DonHeight="70px" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px"></asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Panel ID="CadreExpert" runat="server" BorderStyle="None" BorderWidth="1px"
                BorderColor="#B0E0D7" Height="40px" Width="520px" HorizontalAlign="Left"
                style="margin-top: 5px;  vertical-align: middle">
                <Virtualia:VCoupleEtiquetteExperte ID="ExprH01" runat="server"
                   V_PointdeVue="1" V_Objet="26" V_InfoExperte="619" V_SiDonneeDico="true"
                   EtiHeight="20px" EtiWidth="250px" DonWidth="160px" DonHeight="20px"
                   EtiStyle="margin-left: 40px; margin-top: 5px" DonStyle="margin-top: 9px; text-align: center"/>
              </asp:Panel> 
          </asp:TableCell>
        </asp:TableRow>      
      </asp:Table>
    </asp:TableCell>
  </asp:TableRow>
</asp:Table>