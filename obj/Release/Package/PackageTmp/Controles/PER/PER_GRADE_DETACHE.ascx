﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_GRADE_DETACHE" Codebehind="PER_GRADE_DETACHE.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Standards/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VReliquatAnciennete.ascx" tagname="VReliquatAnciennete" tagprefix="Virtualia" %>

 <asp:Table ID="CadreControle" runat="server" Height="30px" Width="600px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
   <asp:TableRow>
     <asp:TableCell Height="20px"></asp:TableCell>
   </asp:TableRow>
   <asp:TableRow> 
     <asp:TableCell> 
       <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="530px" SiColonneSelect="true"
                                        SiCaseAcocher="false"/>
     </asp:TableCell>
   </asp:TableRow>
   <asp:TableRow>
     <asp:TableCell>
       <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BackColor="#CBF2BE"
        BorderColor="#B0E0D7" Height="470px" Width="600px" HorizontalAlign="Center" style="margin-top: 3px;">
        <asp:TableRow>
          <asp:TableCell>
            <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                            Width="160px" CellPadding="0" CellSpacing="0"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Nouvelle fiche">
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Supprimer la fiche">
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="false">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" Width="600px" 
                CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Height="20px" Width="500px"
                            Text="Emploi fonctionnel - Statut d'emploi - Détachement"
                            BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px;
                            font-style: oblique; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
              <asp:Table ID="CadreDon" runat="server" CellPadding="0" CellSpacing="0" Width="550px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VCoupleEtiDate ID="InfoD00" runat="server" TypeCalendrier="Standard" EtiBackColor="#CBF2BE"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="56" V_Information="0" DonTabIndex="1"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab01" runat="server"
                           V_PointdeVue="1" V_Objet="56" V_Information="1" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true"
                           DonTabIndex="2" EtiWidth="100px" DonWidth="450px" EtiBackColor="#CBF2BE"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab02" runat="server"
                           V_PointdeVue="1" V_Objet="56" V_Information="2" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true"
                           DonTabIndex="3" EtiWidth="100px" DonWidth="100px" EtiBackColor="#CBF2BE"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab11" runat="server"
                           V_PointdeVue="1" V_Objet="56" V_Information="11" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true"
                           DonTabIndex="4" EtiWidth="100px" DonWidth="450px" EtiBackColor="#CBF2BE"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Table ID="CadreIndices" runat="server">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                     <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" EtiBackColor="#CBF2BE"
                                           V_PointdeVue="1" V_Objet="56" V_Information="3" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true"
                                           EtiWidth="100px" DonWidth="60px" DonTabIndex="5" DonStyle="text-align:center;"/>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" EtiBackColor="#CBF2BE"
                                        V_PointdeVue="1" V_Objet="56" V_Information="4" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true"
                                        EtiWidth="100px" DonWidth="60px" DonTabIndex="6" DonStyle="text-align:center;"/>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH23" runat="server" EtiBackColor="#CBF2BE"
                                        V_PointdeVue="1" V_Objet="56" V_Information="23" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true"
                                        EtiWidth="90px" DonWidth="60px" DonTabIndex="7" DonStyle="text-align:center;"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab05" runat="server" EtiBackColor="#CBF2BE"
                           V_PointdeVue="1" V_Objet="56" V_Information="5" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true"
                           DonTabIndex="8" EtiWidth="100px" DonWidth="60px"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right" ColumnSpan="2">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab13" runat="server" EtiBackColor="#CBF2BE"
                           V_PointdeVue="1" V_Objet="56" V_Information="13" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true"
                           DonTabIndex="9" EtiWidth="90px" DonWidth="100px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab12" runat="server" EtiBackColor="#CBF2BE"
                           V_PointdeVue="1" V_Objet="56" V_Information="12" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true"
                           DonTabIndex="10" EtiWidth="100px" DonWidth="450px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab06" runat="server" EtiBackColor="#CBF2BE"
                           V_PointdeVue="1" V_Objet="56" V_Information="6" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true"
                           DonTabIndex="11" EtiWidth="100px" DonWidth="450px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VDuoEtiquetteCommande ID="Dontab07" runat="server" EtiBackColor="#CBF2BE"
                           V_PointdeVue="1" V_Objet="56" V_Information="7" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true"
                           DonTabIndex="12" EtiWidth="250px" DonWidth="300px"/> 
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <Virtualia:VReliquatAnciennete ID="Reliquat08" runat="server" EtiBackColor="#CBF2BE"
                           V_PointdeVue="1" V_Objet="56" V_Information="8" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true"
                           EtiWidth="250px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="EtiArrete" runat="server" Height="20px" Width="350px" Text="Date et référence de l'arrété" 
                            BackColor="#CBF2BE" BorderColor="#B0E0D7"  BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <Virtualia:VCoupleEtiDate ID="InfoD09" runat="server" TypeCalendrier="Standard" EtiVisible="false" V_SiFenetrePer_Bis="true"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="56" V_Information="9" DonTabIndex="13"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server" EtiBackColor="#CBF2BE"
                           V_PointdeVue="1" V_Objet="56" V_Information="10" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true"
                           DonWidth="200px" DonTabIndex="14"  EtiVisible="false" DonStyle="margin-left:2px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="EtiDecret" runat="server" Height="20px" Width="350px" Text="Date et référence de parution au JO du décret"
                           BackColor="#CBF2BE" BorderColor="#B0E0D7"  BorderStyle="Outset"
                           BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                           Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <Virtualia:VCoupleEtiDate ID="InfoD21" runat="server" TypeCalendrier="Standard" EtiBackColor="#CBF2BE" V_SiFenetrePer_Bis="true"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="56" V_Information="21" DonTabIndex="15"/>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH22" runat="server" EtiBackColor="#CBF2BE"
                           V_PointdeVue="1" V_Objet="56" V_Information="22" V_SiDonneeDico="true" V_SiFenetrePer_Bis="true"
                           DonWidth="200px" DonTabIndex="16"  EtiVisible="false" DonStyle="margin-left:2px"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="12px" Columnspan="2"></asp:TableCell>
                </asp:TableRow>
            </asp:Table>
          </asp:TableCell>
        </asp:TableRow>      
      </asp:Table>
     </asp:TableCell>
   </asp:TableRow>
 </asp:Table>