﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_PER_APREVENIR" Codebehind="PER_APREVENIR.ascx.vb" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiquetteExperte.ascx" tagname="VCoupleEtiquetteExperte" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VDuoEtiquetteCommande.ascx" tagname="VDuoEtiquetteCommande" tagprefix="Virtualia" %>
 
 <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px"
    BorderColor="#B0E0D7" Height="300px" Width="800px" HorizontalAlign="Center">
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0" 
            CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="false"
            BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
            Width="70px" HorizontalAlign="Right" style="margin-top: 3px; margin-right:3px">
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Bottom">
                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                    BorderStyle="None" style=" margin-left: 6px; text-align: center;">
                    </asp:Button>
                </asp:TableCell>
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" 
            CellSpacing="0" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="Etiquette" runat="server" Text="Personne à prévenir" Height="20px" Width="300px"
                        BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#D7FAF3"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px;
                        font-style: oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>          
                </asp:TableCell>      
            </asp:TableRow>
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Table ID="Adresse" runat="server" Height="22px" Width="495px" CellPadding="0" CellSpacing="0">
            <asp:TableRow>
             <asp:TableCell>
                 <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" 
                    V_PointdeVue="1" V_Objet="30" V_Information="1" V_SiDonneeDico="true"
                    DonWidth="300px" DonTabIndex="1"/>
             </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
             <asp:TableCell>
                 <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" 
                    V_PointdeVue="1" V_Objet="30" V_Information="2" V_SiDonneeDico="true"
                    DonWidth="300px" DonTabIndex="2"/>
             </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
             <asp:TableCell>
                 <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server"
                    V_PointdeVue="1" V_Objet="30" V_Information="3" V_SiDonneeDico="true"
                    DonWidth="300px" DonTabIndex="3"/>
             </asp:TableCell>
            </asp:TableRow>
          </asp:Table>
          <asp:Table ID="CP" runat="server" Height="22px" Width="495px" CellPadding="0" CellSpacing="0">
            <asp:TableRow> 
             <asp:TableCell Width="145px" HorizontalAlign="Left">  
                 <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server"
                    V_PointdeVue="1" V_Objet="30" V_Information="4" V_SiDonneeDico="true"
                    DonWidth="40px" EtiText="Code postal et ville" DonTabIndex="7"/>
             </asp:TableCell>
             <asp:TableCell>         
                 <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" 
                    V_PointdeVue="1" V_Objet="30" V_Information="5" V_SiDonneeDico="true"
                    EtiWidth="0px" EtiVisible="false" DonWidth="250px" DonTabIndex="8"/>
             </asp:TableCell>
            </asp:TableRow> 
            <asp:TableRow> 
             <asp:TableCell Width="145px" HorizontalAlign="Left">  
                 <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server"
                    V_PointdeVue="1" V_Objet="30" V_Information="6" V_SiDonneeDico="true"
                    DonWidth="120px" DonTabIndex="7"/>
             </asp:TableCell>
            </asp:TableRow> 
            <asp:TableRow> 
             <asp:TableCell Width="145px" HorizontalAlign="Left"> 
                 <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server"
                    V_PointdeVue="1" V_Objet="30" V_Information="7" V_SiDonneeDico="true"
                    DonWidth="30px" EtiText="Code pays et pays" DonTabIndex="9"/>
             </asp:TableCell>
             <asp:TableCell>  
                 <Virtualia:VDuoEtiquetteCommande ID="Dontab08" runat="server" 
                    V_PointdeVue="1" V_Objet="30" V_Information="8" V_SiDonneeDico="true"
                    EtiWidth="0px" EtiVisible="false" DonWidth="200px" DonTabIndex="10"/>        
             </asp:TableCell>
            </asp:TableRow>        
          </asp:Table>
      </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
      <asp:TableCell>
          <asp:Panel ID="CadreExpert" runat="server" BorderStyle="None" BorderWidth="1px"
            BorderColor="#B0E0D7" Height="44px" Width="500px" HorizontalAlign="Center"
            style="margin-top: 5px; vertical-align: middle">
            <Virtualia:VCoupleEtiquetteExperte ID="ExprH01" runat="server"
               V_PointdeVue="1" V_Objet="30" V_InfoExperte="508" V_SiDonneeDico="true"
               EtiHeight="20px" EtiWidth="170px" DonWidth="600px" DonHeight="35px"/>
          </asp:Panel>
      </asp:TableCell>
    </asp:TableRow>        
 </asp:Table>


