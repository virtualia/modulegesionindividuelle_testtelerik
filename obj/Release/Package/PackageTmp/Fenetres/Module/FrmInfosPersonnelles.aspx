﻿<%@ Page Language="vb" MasterPageFile="~/PagesMaitre/VirtualiaMain.master" AutoEventWireup="false" Culture="auto:fr-FR" UICulture="auto:fr-FR"
    CodeBehind="FrmInfosPersonnelles.aspx.vb" Inherits="Virtualia.Net.FrmInfosPersonnelles" MaintainScrollPositionOnPostback="True" %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controles/Standards/VReferentiel.ascx" TagName="VReference" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Standards/VMessage.ascx" TagName="VMessage" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Module/PerInfosGenerales.ascx" TagName="VInfoGen" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Module/PerInfosCV.ascx" TagName="VInfoCV" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Module/PerAdministratifPublic.ascx" TagName="VAdministratif" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Module/PerAffectationPublic.ascx" TagName="VAffectation" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Module/PerFormation.ascx" TagName="VFormation" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Standards/VArmoirePER.ascx" TagName="VArmoire" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/PER/CtlPvuePER.ascx" TagName="VPER" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Module/CtlPvueDossier.ascx" TagName="VInfoDoss" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Module/PerDossierAcceuil.ascx" TagName="VProfile" TagPrefix="Virtualia" %>

<asp:Content ID="Corps" ContentPlaceHolderID="Principal" runat="server">
    <style type="text/css">
        .ajax__fileupload_fileItemInfo div.removeButton {
            width: 170px;
            background-color: #9B171A;
        }

        div.ajax__fileupload_uploadbutton {
            width: 170px;
            background-color: #3A5892;
        }

        .ajax__fileupload_selectFileContainer {
            width: 140px;
            background-color: white;
        }

        .ajax__fileupload_selectFileButton {
            width: 140px;
            background-color: #B0E0D7;
            color: #124545;
            text-align: center;
            font-family: 'Trebuchet MS';
            font-style: italic;
            font-weight: bold;
            border-style: solid;
            border-width: 1px
        }

        .BtnOnglet {
            background-color: transparent;
            border-style: none;
            color: #142425;
            font-family: 'Trebuchet MS';
            font-size: small;
            font-style: italic;
            font-weight: normal;
            padding-left: 1px;
            text-indent: 0px;
            text-align: center;
            margin-left: 1px;
            margin-bottom: 2px;
            word-wrap: normal;
            height: 33px;
            width: 98px;
        }

        .BtnOngletSel {
            background-color: #7D9F99;
            border-style: none;
            color: white;
            font-family: 'Trebuchet MS';
            font-size: small;
            font-style: italic;
            font-weight: bold;
            padding-left: 1px;
            text-indent: 0px;
            text-align: center;
            margin-left: 1px;
            margin-bottom: 2px;
            word-wrap: normal;
            height: 33px;
            width: 85px;
        }
    </style>
    <script type="text/javascript">
        function AjaxFileUpload_change_text() {
            Sys.Extended.UI.Resources.AjaxFileUpload_SelectFile = "Sélection du fichier";
            Sys.Extended.UI.Resources.AjaxFileUpload_DropFiles = "Glisser/Déplacer des fichiers ici";
            Sys.Extended.UI.Resources.AjaxFileUpload_Pending = "en attente";
            Sys.Extended.UI.Resources.AjaxFileUpload_Remove = "Supprimer";
            Sys.Extended.UI.Resources.AjaxFileUpload_Upload = "Télécharger";
            Sys.Extended.UI.Resources.AjaxFileUpload_Uploaded = "Téléchargé";
            Sys.Extended.UI.Resources.AjaxFileUpload_UploadedPercentage = "téléchargement {0} %";
            Sys.Extended.UI.Resources.AjaxFileUpload_Uploading = "Téléchargement en cours";
            Sys.Extended.UI.Resources.AjaxFileUpload_FileInQueue = "{0} fichier(s) en file d'attente.";
            Sys.Extended.UI.Resources.AjaxFileUpload_AllFilesUploaded = "Tous les fichiers ont été téléchargés.";
            Sys.Extended.UI.Resources.AjaxFileUpload_FileList = "Liste des fichiers téléchargés:";
            Sys.Extended.UI.Resources.AjaxFileUpload_SelectFileToUpload = "Glisser/déplacer ou bien sélectionner les fichiers à télécharger.";
            Sys.Extended.UI.Resources.AjaxFileUpload_Cancelling = "Annulation en cours...";
            Sys.Extended.UI.Resources.AjaxFileUpload_UploadError = "Une erreur est survenue pendant le téléchargement.";
            Sys.Extended.UI.Resources.AjaxFileUpload_CancellingUpload = "Annulation upload...";
            Sys.Extended.UI.Resources.AjaxFileUpload_UploadingInputFile = "Téléchargement fichier: {0}.";
            Sys.Extended.UI.Resources.AjaxFileUpload_Cancel = "Annuler";
            Sys.Extended.UI.Resources.AjaxFileUpload_Canceled = "annulé";
            Sys.Extended.UI.Resources.AjaxFileUpload_UploadCanceled = "téléchargement annulé";
            Sys.Extended.UI.Resources.AjaxFileUpload_DefaultError = "erreur lors du téléchargement";
            Sys.Extended.UI.Resources.AjaxFileUpload_UploadingHtml5File = "Uploading file: {0} of size {1} bytes.";
            Sys.Extended.UI.Resources.AjaxFileUpload_error = "erreur";
        }
    </script>
    <asp:Table ID="CadreGeneral" runat="server" HorizontalAlign="Center" Width="1050px">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                <asp:MultiView ID="MultiVues" runat="server" ActiveViewIndex="0">
                    <asp:View ID="VueGestionIndividuelle" runat="server">

                        <asp:Table ID="TableOnglets" runat="server" Height="35px" Width="950px" HorizontalAlign="Center" Style="margin-top: 0px">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="CadreOnglets" runat="server" Height="35px" Width="1100px" HorizontalAlign="Left" CellPadding="0" CellSpacing="0" BackColor="Transparent" Style="margin-top: 2px">
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Center">
                                                <asp:Menu ID="MenuChoix" runat="server" Width="1100px" DynamicHorizontalOffset="10" Font-Names="Trebuchet MS" Font-Size="Small" BackColor="Transparent" Font-Italic="false" ForeColor="#124545" Height="22px" Orientation="Horizontal" StaticSubMenuIndent="20px" Font-Bold="True" BorderWidth="" Style="margin-left: 1px;" StaticEnableDefaultPopOutImage="False" BorderColor="Transparent" BorderStyle="Solid">
                                                    <StaticSelectedStyle BackColor="#124545" BorderStyle="Solid" BorderColor="#124545" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#B0E0D7" />
                                                    <StaticMenuItemStyle VerticalPadding="2px" HorizontalPadding="10px" Height="27px" BackColor="#B0E0D7" BorderColor="#124545" BorderStyle="Solid" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#124545" />
                                                    <StaticHoverStyle BackColor="#124545" ForeColor="White" BorderColor="#124545" BorderStyle="Solid" />
                                                    <Items>
                                                        <asp:MenuItem Selected="true" Text="DONNEES PERSONNELLES" Value="DONPER" ToolTip="" />
                                                        <asp:MenuItem Text="DIPLOMES ET QUALIFICATIONS" Value="DIPLOME" ToolTip="" />
                                                        <asp:MenuItem Text="SITUATION ADMINISTRATIVE" Value="SITADM" ToolTip="" />
                                                        <asp:MenuItem Text="AFFECTATIONS" Value="AFFECTATION" ToolTip="" />
                                                    </Items>
                                                </asp:Menu>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>

                        <asp:Table ID="CadreVues" runat="server" Height="800px" Width="1150px" HorizontalAlign="Center" BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" CellSpacing="2" Style="margin-top: 1px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Table ID="CadreCmdOK" runat="server" Height="22px" CellPadding="0"
                                        CellSpacing="0" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Visible="true"
                                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#FFEBC8" ForeColor="#FFF2DB"
                                        Width="80px" HorizontalAlign="right" Style="margin-top: 3px; margin-right: 3px">
                                        <asp:TableRow>
                                            <asp:TableCell VerticalAlign="Bottom">
                                                <asp:Button ID="CommandeOK" runat="server" Text="Enregistrer" Width="75px" Height="20px"
                                                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                    BorderStyle="None" Style="margin-left: 6px; text-align: right;"></asp:Button>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ID="ConteneurVues" Width="1150px" VerticalAlign="Top" HorizontalAlign="Center">
                                    <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                                        <asp:View ID="VueGenerale" runat="server">
                                            <Virtualia:VInfoGen ID="Per_InfoGen" runat="server" CadreStyle="margin-bottom: 20px" />
                                        </asp:View>
                                        <asp:View ID="VueDiplome" runat="server">
                                            <Virtualia:VInfoCV ID="Per_InfoCV" runat="server" CadreStyle="margin-bottom: 20px" />
                                        </asp:View>
                                        <asp:View ID="VueSitAdm" runat="server">
                                            <Virtualia:VAdministratif ID="Per_InfoAdm" runat="server" CadreStyle="margin-bottom: 20px" />
                                        </asp:View>
                                        <asp:View ID="VueAffectation" runat="server">
                                            <Virtualia:VAffectation ID="Per_Affectation" runat="server" CadreStyle="margin-bottom: 20px" />
                                        </asp:View>
                                        <asp:View ID="VueFormation" runat="server">
                                            <Virtualia:VFormation ID="Per_Formation" runat="server" V_SiEnLectureSeule="true" CadreStyle="margin-bottom: 20px" />
                                        </asp:View>
                                    </asp:MultiView>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ID="CellMessage" Visible="false">
                                    <ajaxToolkit:ModalPopupExtender ID="PopupMsg" runat="server" TargetControlID="HPopupMsg" PopupControlID="PanelMsgPopup" BackgroundCssClass="modalBackground" />
                                    <asp:Panel ID="PanelMsgPopup" runat="server">
                                        <Virtualia:VMessage ID="MsgVirtualia" runat="server" />
                                    </asp:Panel>
                                    <asp:HiddenField ID="HPopupMsg" runat="server" Value="0" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ID="CellReference" Visible="false">
                                    <ajaxToolkit:ModalPopupExtender  ID="PopupReferentiel" runat="server" TargetControlID="HPopupRef" PopupControlID="PanelRefPopup" BackgroundCssClass="modalBackground" />
                                    <asp:Panel ID="PanelRefPopup" runat="server">
                                        <Virtualia:VReference ID="RefVirtualia" runat="server" />
                                    </asp:Panel>
                                    <asp:HiddenField ID="HPopupRef" runat="server" Value="0" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:View>
                    <asp:View ID="VueArmoire" runat="server">
                        <Virtualia:VArmoire ID="ArmoirePER" runat="server" V_Appelant="HSUPP" />
                    </asp:View>
                    <asp:View ID="VuePER" runat="server">
                        <Virtualia:VPER ID="VFenetrePER" runat="server" />
                    </asp:View>
                    <asp:View ID="VueDoss" runat="server">
                        <Virtualia:VInfoDoss ID="VInfoDossier" runat="server" />
                    </asp:View>
                    <asp:View ID="VueProfile" runat="server">
                        <Virtualia:VProfile ID="VProfile" runat="server" />
                    </asp:View>
                </asp:MultiView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
