﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class VirtualiaMain
    Inherits System.Web.UI.MasterPage
    Private WsDossierPer As Virtualia.Net.Individuel.DossierIndividu
    Private WsParent As Individuel.EnsembleDossiers
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private Cretour As Boolean
    Private wsfrm As Virtualia.Net.FrmInfosPersonnelles


    Public Function GetApplication() As HttpApplicationState
        Return Me.Application
    End Function

    Public Function GetSession() As HttpSessionState
        Return Me.Session
    End Function

    Public Property V_SelDate As String
        Get
            If Session.Item("SelDate") IsNot Nothing Then
                Return Session.Item("SelDate").ToString
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            If Session.Item("SelDate") IsNot Nothing Then
                Session.Remove("SelDate")
            End If
            Session.Add("SelDate", value)
        End Set
    End Property

    Public Property V_SelEtablissement As String
        Get
            If Session.Item("SelEta") IsNot Nothing Then
                Return Session.Item("SelEta").ToString
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            If Session.Item("SelEta") IsNot Nothing Then
                Session.Remove("SelEta")
            End If
            Session.Add("SelEta", value)
        End Set
    End Property

    Public Property V_SelNiveau1 As String
        Get
            If Session.Item("SelN1") IsNot Nothing Then
                Return Session.Item("SelN1").ToString
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            If Session.Item("SelN1") IsNot Nothing Then
                Session.Remove("SelN1")
            End If
            Session.Add("SelN1", value)
        End Set
    End Property

    Public WriteOnly Property V_SiCadreCommandeVisible As Boolean
        Set(value As Boolean)
            CadreCommandes.Visible = value
        End Set
    End Property

    Public WriteOnly Property V_Info1 As String
        Set(value As String)
            EtiInfoN1.Text = value
        End Set
    End Property

    Public WriteOnly Property V_Info2 As String
        Set(value As String)
            If value = "(Tous)" Then
                EtiInfoN2.Text = ""
            Else
                EtiInfoN2.Text = value
            End If
        End Set
    End Property

    Public WriteOnly Property V_Info3 As String
        Set(value As String)
            If value = "(Tous)" Then
                EtiInfoN3.Text = ""
            Else
                EtiInfoN3.Text = value
            End If
        End Set
    End Property
    Private ReadOnly Property V_Contexte As Virtualia.Net.Individuel.LocalNavigation
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
        End Get
    End Property
    Private ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 2)
            End If
            Return WebFct
        End Get
    End Property
    Private ReadOnly Property V_ListeDossiers As Virtualia.Net.Individuel.EnsembleDossiers
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).EnsemblePER
        End Get
    End Property

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim WebFct As Virtualia.Net.Controles.WebFonctions = New Virtualia.Net.Controles.WebFonctions(Me, 2)
        Dim Chaine As String
        Dim DossierPER As Virtualia.Ressources.Datas.ObjetDossierPER = Nothing


        EtiDateJour.Text = WebFct.ViRhDates.ClairDate(WebFct.ViRhDates.DateduJour, True)
        EtiHeure.Text = Format(TimeOfDay, "t")
        If WebFct.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If
        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer IsNot Nothing Then
            If V_ListeDossiers.Item(0).creation_annuler = True Then
                HPopupNvDoss.Value = "0"
                PopupNvDossier.Hide()
                CellNvDossier.Visible = False
                V_ListeDossiers.Item(0).creation_annuler = False
            End If
            If WsDossierPer.Count > 0 Then
                If WsDossierPer.Success_NvDossier = 1 Then
                    HPopupRef2.Value = "1"
                    ModalPopupExtender2.Show()
                    CellReference2.Visible = True
                    Label2.Text = "Le dossier de " & WsDossierPer.FicheEtatCivil.Nom & " " & WsDossierPer.FicheEtatCivil.Prenom & " a été créé avec succés."
                ElseIf WsDossierPer.Success_NvDossier = 2 Or WsDossierPer.Success_NvDossier = 3 Then
                    HPopupNvDoss.Value = "1"
                    PopupNvDossier.Show()
                    CellNvDossier.Visible = True
                Else

                End If
            End If
        End If
        If WsDossierPer Is Nothing Then
            If V_Contexte IsNot Nothing Then
                V_ListeDossiers.Identifiant(False) = 0
                If V_ListeDossiers.ItemDossier(0).V_NvDossier = True Then
                    V_Contexte.DossierPER = V_ListeDossiers.ItemDossier(0)
                    V_Contexte.Identifiant_Courant = 0
                    WsDossierPer = V_Contexte.DossierPER
                Else
                    WsDossierPer = V_Contexte.DossierPER
                End If
            End If
        End If
        EtiTitre.Text = "Module de gestion individuelle" 'WebFct.PointeurUtilisateur.PointeurContexte.TitreModuleActif
        EtiUtilisateur.Text = WebFct.PointeurGlobal.NomPrenomUtilisateur(Session.SessionID)
        CmdAccueil.PostBackUrl = WebFct.PointeurGlobal.UrlDestination(Session.SessionID, 0, "") 'Retour à l'accueil Virtualia.Net
        Chaine = "~/Fenetres/Commun/FrmAttente.aspx"
        Cmd01.PostBackUrl = Chaine & "?Index=1&Lien=~/Fenetres/Module/FrmInfosPersonnelles.aspx&Param=Armoire"



    End Sub

    Protected Sub Cmd02_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cmd02.Click
        'Dim Objetglobal = New Session.ObjetGlobal
        'Dim ensembledossier = New Individuel.EnsembleDossiers(Objetglobal)
        'Dim Dossiervide = New Individuel.DossierIndividu(ensembledossier)
        'V_ListeDossiers.Add(Dossiervide)
        HPopupNvDoss.Value = "1"
        PopupNvDossier.Show()
        CellNvDossier.Visible = True
        If V_ListeDossiers.Count > 0 Then
            V_ListeDossiers.Item(0).ControleNvDossier = True
        End If
    End Sub
    Private Sub HorlogeVirtuelle_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles HorlogeVirtuelle.Tick
        Dim WebFct As Virtualia.Net.Controles.WebFonctions = New Virtualia.Net.Controles.WebFonctions(Me, 2)
        EtiDateJour.Text = WebFct.ViRhDates.ClairDate(WebFct.ViRhDates.DateduJour, True)
        EtiHeure.Text = Format(TimeOfDay, "t")
    End Sub


    Public WriteOnly Property Titre As String
        Set(value As String)

        End Set
    End Property



    Public ReadOnly Property WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            Dim WebFct As Virtualia.Net.Controles.WebFonctions = New Virtualia.Net.Controles.WebFonctions(Me, 2)
            Return WebFct
        End Get
    End Property


    Protected Sub BtnConfirmer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click

        'V_Contexte.DossierPER = V_ListeDossiers.Item(0)
        'V_Contexte.Identifiant_Courant = 0
        For Each dossier In V_ListeDossiers
            If V_ListeDossiers.Item(0).Nom_NV = dossier.FicheEtatCivil.Nom And V_ListeDossiers.Item(0).Prenom_NV = dossier.FicheEtatCivil.Prenom And V_ListeDossiers.Item(0).DN_NV = dossier.FicheEtatCivil.Date_de_naissance Then
                V_Contexte.DossierPER = dossier
            End If
        Next
        WsDossierPer = V_Contexte.DossierPER
        WsDossierPer.V_NvDossier = False
        WsDossierPer.V_NvDossier_Clicked = True
        WsDossierPer.Success_NvDossier = 0
        WsDossierPer.EnCours_Creation = False
        V_ListeDossiers.Item(0).ControleNvDossier = False
        Response.Redirect("~/Fenetres/Commun/FrmAttente.aspx" & "?Index=2&Lien=~/Fenetres/Module/FrmInfosPersonnelles.aspx&Param=GestionIndividuelle")
    End Sub


End Class