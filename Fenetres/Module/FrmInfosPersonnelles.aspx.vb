﻿Imports VI = Virtualia.Systeme.Constantes
Public Class FrmInfosPersonnelles
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsParam As String = ""
    Private WsDossierPer As Virtualia.Net.Individuel.DossierIndividu
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
    Private WsNumObjet As Integer = VI.ObjetPer.ObaCivil
    Private WsSicreationOK As Boolean = False
    Private WsVue As Integer


    Private Enum Ivue As Integer
        IGestionIndividuelle = 0
        IArmoire = 1
        IVPER = 2
        IVDoss = 3
        IVProfile = 4
    End Enum
    Private ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
            End If
            Return WebFct
        End Get
    End Property

    Private ReadOnly Property V_Contexte As Virtualia.Net.Individuel.LocalNavigation
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ContexteSession(Session.SessionID)
        End Get
    End Property

    Private ReadOnly Property V_ListeDossiers As Virtualia.Net.Individuel.EnsembleDossiers
        Get
            Return CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).EnsemblePER
        End Get
    End Property

    Private ReadOnly Property V_DossierPer As Virtualia.Net.Individuel.DossierIndividu
        Get
            If V_Contexte Is Nothing Then
                Return Nothing
            End If
            Return V_Contexte.DossierPER
        End Get
    End Property
    Public Property V_Identifiant() As Integer
        Get
            Return CInt(HSelIde.Value)
        End Get
        Set(ByVal value As Integer)
            If value = CInt(HSelIde.Value) Then
                Exit Property
            End If
            Dim Chaine As String = ""
            HSelIde.Value = value.ToString
            If V_Contexte IsNot Nothing Then
                V_ListeDossiers.Identifiant(False) = value
                V_Contexte.DossierPER = V_ListeDossiers.ItemDossier(value)
                V_Contexte.Identifiant_Courant = value
                Chaine = V_Contexte.DossierPER.FicheEtatCivil.Qualite
                Chaine &= Strings.Space(1) & V_Contexte.DossierPER.FicheEtatCivil.Nom
                Chaine &= Strings.Space(1) & V_Contexte.DossierPER.FicheEtatCivil.Prenom
                Chaine &= " (Ide V : " & value & ")"
            End If
        End Set
    End Property
    Private Sub FrmInfosPersonnelles_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Dim DossierPER As Virtualia.Ressources.Datas.ObjetDossierPER = Nothing
        Dim Cretour As Boolean
        Dim TabMsg As New List(Of String)

        If V_Contexte IsNot Nothing Then
            WsDossierPer = V_Contexte.DossierPER
        End If


        If V_Identifiant = 0 Then
            If WsParam = "" Then
                WsParam = Server.HtmlDecode(Request.QueryString("Param"))
                'V_WebFonction.PointeurUtilisateur.V_PointeurContexte.ModuleActif = WsParam
                Select Case WsParam
                    Case "Armoire"
                        MultiVues.ActiveViewIndex = Ivue.IArmoire
                    Case "GestionIndividuelle"
                        V_ListeDossiers.Identifiant(False) = 0
                        'V_Contexte.Identifiant_Courant = 0
                        'V_Contexte.DossierPER = V_ListeDossiers.ItemDossier(V_Identifiant)
                        MultiVues.ActiveViewIndex = Ivue.IVPER
                        MultiVues.ActiveViewIndex = Ivue.IVDoss
                        MultiVues.ActiveViewIndex = Ivue.IGestionIndividuelle
                        'V_Contexte.DossierPER = V_ListeDossiers.ItemDossier(V_Identifiant)
                        If V_ListeDossiers.Count > 0 Then
                            For Each dossier In V_ListeDossiers
                                If dossier.FicheEtatCivil IsNot Nothing Then
                                    If V_ListeDossiers.Item(0).Nom_NV = dossier.FicheEtatCivil.Nom And V_ListeDossiers.Item(0).Prenom_NV = dossier.FicheEtatCivil.Prenom And V_ListeDossiers.Item(0).DN_NV = dossier.FicheEtatCivil.Date_de_naissance Then
                                        V_Contexte.DossierPER = dossier
                                        WsDossierPer = V_Contexte.DossierPER
                                        V_Identifiant = dossier.Identifiant
                                    End If
                                End If
                            Next
                        End If
                        If V_Contexte.DossierPER.V_NvDossier_Clicked = True Then
                            MultiVues.ActiveViewIndex = Ivue.IGestionIndividuelle
                            Per_InfoGen.IdDossier = V_Identifiant

                        Else
                            MultiVues.ActiveViewIndex = Ivue.IVProfile
                        End If

                    Case Else
                        MultiVues.ActiveViewIndex = Ivue.IArmoire
                End Select
            End If
        ElseIf WsDossierPer IsNot Nothing AndAlso WsDossierPer.Dossier_entier_clicked = True Then
            MultiVues.ActiveViewIndex = Ivue.IVPER
            WsDossierPer.Dossier_entier_clicked = False
        End If


        If V_Contexte IsNot Nothing Then
            WsDossierPer = V_Contexte.DossierPER
            If WsDossierPer IsNot Nothing Then
                If WsDossierPer.V_NvDossier = True Then
                    If WsDossierPer.TableErreur = False Then
                        DossierPER = V_WebFonction.PointeurGlobal.NouveauDossier(V_Contexte.DossierPER.FicheEtatCivil.Nom, V_Contexte.DossierPER.FicheEtatCivil.Prenom, V_Contexte.DossierPER.FicheEtatCivil.Date_de_naissance)
                        Cretour = WsDossierPer.MettreAJour(DossierPER.V_Identifiant, "C")
                        If Cretour = True Then
                            WsDossierPer.Identifiant = DossierPER.V_Identifiant
                            V_Identifiant = DossierPER.V_Identifiant
                            WsDossierPer.Success_NvDossier = 1
                            V_ListeDossiers.Item(0).Nom_NV = WsDossierPer.FicheEtatCivil.Nom
                            V_ListeDossiers.Item(0).Prenom_NV = WsDossierPer.FicheEtatCivil.Prenom
                            V_ListeDossiers.Item(0).DN_NV = WsDossierPer.FicheEtatCivil.Date_de_naissance
                            'V_Contexte.DossierPER = WsDossierPer
                        Else
                            WsDossierPer.Success_NvDossier = 2
                        End If
                    Else
                        WsDossierPer.Success_NvDossier = 3
                    End If
                End If
            End If

        End If

    End Sub

    Private Sub Armoire_Dossier_Click(ByVal sender As Object, ByVal e As Systeme.Evenements.DossierClickEventArgs) Handles ArmoirePER.Dossier_Click
        If e.Identifiant = 0 Then
            Exit Sub
        End If
        V_Identifiant = e.Identifiant
        VFenetrePER.V_Identifiant = V_Identifiant
        VProfile.V_Identifiant = V_Identifiant
        MultiVues.ActiveViewIndex = Ivue.IVProfile

    End Sub
    Private Sub MenuChoix_MenuItemClick(sender As Object, e As MenuEventArgs) Handles MenuChoix.MenuItemClick
        Select Case e.Item.Value
            Case "DONPER"
                MultiOnglets.SetActiveView(VueGenerale)
            Case "DIPLOME"
                MultiOnglets.SetActiveView(VueDiplome)
            Case "SITADM"
                MultiOnglets.SetActiveView(VueSitAdm)
            Case "AFFECTATION"
                MultiOnglets.SetActiveView(VueAffectation)
            Case "FORMATION"
                MultiOnglets.SetActiveView(VueFormation)
        End Select
    End Sub


    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click

        Dim TableauErreur As List(Of String) = SiErreurSpecifique()
        Dim TabMsg As New List(Of String)
        Dim Cretour As Boolean
        Dim TitreMsg As String
        WsDossierPer = V_Contexte.DossierPER
        If WsDossierPer.Count = 0 Then
            TitreMsg = "Contrôle préalable à l'enregistrement du dossier :"
            Call MessageDialogue(V_WebFonction.Evt_MessageInformatif(WsNumObjet, TitreMsg, TableauErreur))
            Exit Sub
        End If

        If WsDossierPer Is Nothing Then
            Exit Sub
        End If
        If TableauErreur IsNot Nothing Then
            TitreMsg = "Contrôle préalable à l'enregistrement du dossier :"
            Call MessageDialogue(V_WebFonction.Evt_MessageInformatif(WsNumObjet, TitreMsg, TableauErreur))
            Exit Sub
        End If

        Cretour = WsDossierPer.MettreAJour(V_Identifiant, "M")
        If Cretour = True Then
            TabMsg.Add("Mis à jour effectué avec succés.")
            TitreMsg = "Contrôle préalable à l'enregistrement du dossier de " & WsDossierPer.FicheEtatCivil.Nom & Strings.Space(1) & WsDossierPer.FicheEtatCivil.Prenom
            Call MessageDialogue(V_WebFonction.Evt_MessageInformatif(WsNumObjet, TitreMsg, TabMsg))
        Else
            TabMsg.Add("Une erreur est survenue lors du mise à jour du dossier.")
            TitreMsg = "Contrôle préalable à l'enregistrement du dossier de " & WsDossierPer.FicheEtatCivil.Nom & Strings.Space(1) & WsDossierPer.FicheEtatCivil.Prenom
            Call MessageDialogue(V_WebFonction.Evt_MessageInformatif(WsNumObjet, TitreMsg, TabMsg))
        End If

    End Sub

    Private Function SiErreurSpecifique() As List(Of String)
        Dim Ide_Dossier As Integer = V_Identifiant
        Dim TabErreurs As New List(Of String)

        WsDossierPer = V_Contexte.DossierPER


        Dim Dossier As Virtualia.Ressources.Datas.ObjetDossierPER
        If Ide_Dossier > 0 Then
            Dossier = V_WebFonction.PointeurDossier(Ide_Dossier)
        Else
            Dossier = Nothing
        End If

        If WsDossierPer.FicheDomicile IsNot Nothing Then
            If WsDossierPer.FicheDomicile.Numero_et_nom_de_la_rue <> "" And WsDossierPer.FicheDomicile.Code_postal = "" And WsDossierPer.FicheDomicile.Ville = "" Then
                TabErreurs.Add("Domicile : Veuillez remplir les champ 'code postal' et 'ville'")
            ElseIf WsDossierPer.FicheDomicile.Numero_et_nom_de_la_rue <> "" And WsDossierPer.FicheDomicile.Code_postal <> "" And WsDossierPer.FicheDomicile.Ville = "" Then
                TabErreurs.Add("Domicile : Veuillez remplir le champ 'ville'")
            ElseIf WsDossierPer.FicheDomicile.Numero_et_nom_de_la_rue <> "" And WsDossierPer.FicheDomicile.Code_postal = "" And WsDossierPer.FicheDomicile.Ville <> "" Then
                TabErreurs.Add("Domicile : Veuillez remplir le champ 'code postal'")
            End If
        End If

        If WsDossierPer.FicheBanque IsNot Nothing Then
            If WsDossierPer.FicheBanque.IBAN <> "" And WsDossierPer.FicheBanque.BIC = "" Then
                TabErreurs.Add("Banque : Veuillez remplir le champ 'BIC'")
            ElseIf WsDossierPer.FicheBanque.BIC <> "" And WsDossierPer.FicheBanque.IBAN = "" Then
                TabErreurs.Add("Banque : Veuillez remplir le champ 'IBAN'")
            ElseIf (WsDossierPer.FicheBanque.BIC = "" And WsDossierPer.FicheBanque.IBAN = "") And WsDossierPer.FicheBanque.Domiciliation <> "" Then
                TabErreurs.Add("Banque : Veuillez remplir les champ 'IBAN' et 'BIC'")
            End If
        End If


        If WsDossierPer.FicheStatut IsNot Nothing Then
            If (WsDossierPer.FicheStatut.Situation <> "" Or WsDossierPer.FicheStatut.Date_de_l_arrete <> "" Or WsDossierPer.FicheStatut.Reference_de_l_arrete <> "") And WsDossierPer.FicheStatut.Statut = "" And WsDossierPer.FicheStatut.Date_de_Valeur = "" Then
                TabErreurs.Add("Statut : Veuillez remplir les champs 'Statut' et 'Depuis le'")
            ElseIf (WsDossierPer.FicheStatut.Date_de_Valeur <> "" Or WsDossierPer.FicheStatut.Situation <> "" Or WsDossierPer.FicheStatut.Date_de_l_arrete <> "" Or WsDossierPer.FicheStatut.Reference_de_l_arrete <> "") And WsDossierPer.FicheStatut.Statut = "" Then
                TabErreurs.Add("Statut : Veuillez remplir le champ 'Statut'")
            ElseIf (WsDossierPer.FicheStatut.Statut <> "" Or WsDossierPer.FicheStatut.Situation <> "" Or WsDossierPer.FicheStatut.Date_de_l_arrete <> "" Or WsDossierPer.FicheStatut.Reference_de_l_arrete <> "") And WsDossierPer.FicheStatut.Date_de_Valeur = "" Then
                TabErreurs.Add("Statut : Veuillez remplir le champ 'Depuis le'")

            End If
        End If

        If WsDossierPer.FichePosition IsNot Nothing Then
            If (WsDossierPer.FichePosition.Detachement <> "" Or WsDossierPer.FichePosition.Modaliteservice <> "" Or WsDossierPer.FichePosition.Taux_d_activite <> "" Or WsDossierPer.FichePosition.Tauxremuneration <> "" Or WsDossierPer.FichePosition.Motif <> "" Or WsDossierPer.FichePosition.Date_de_l_arrete <> "" Or WsDossierPer.FichePosition.Reference_de_l_arrete <> "" Or WsDossierPer.FichePosition.Date_du_decret <> "" Or WsDossierPer.FichePosition.Date_de_parution <> "") And WsDossierPer.FichePosition.Position = "" And WsDossierPer.FichePosition.Date_de_Valeur = "" Then
                TabErreurs.Add("Position : Veuillez remplir les champs 'Position' et 'Depuis le'")
            ElseIf (WsDossierPer.FichePosition.Date_de_Valeur <> "" Or WsDossierPer.FichePosition.Detachement <> "" Or WsDossierPer.FichePosition.Modaliteservice <> "" Or WsDossierPer.FichePosition.Taux_d_activite <> "" Or WsDossierPer.FichePosition.Tauxremuneration <> "" Or WsDossierPer.FichePosition.Motif <> "" Or WsDossierPer.FichePosition.Date_de_l_arrete <> "" Or WsDossierPer.FichePosition.Reference_de_l_arrete <> "" Or WsDossierPer.FichePosition.Date_du_decret <> "" Or WsDossierPer.FichePosition.Date_de_parution <> "") And WsDossierPer.FichePosition.Position = "" Then
                TabErreurs.Add("Position : Veuillez remplir le champ 'Position'")
            ElseIf (WsDossierPer.FichePosition.Position <> "" Or WsDossierPer.FichePosition.Detachement <> "" Or WsDossierPer.FichePosition.Modaliteservice <> "" Or WsDossierPer.FichePosition.Taux_d_activite <> "" Or WsDossierPer.FichePosition.Tauxremuneration <> "" Or WsDossierPer.FichePosition.Motif <> "" Or WsDossierPer.FichePosition.Date_de_l_arrete <> "" Or WsDossierPer.FichePosition.Reference_de_l_arrete <> "" Or WsDossierPer.FichePosition.Date_du_decret <> "" Or WsDossierPer.FichePosition.Date_de_parution <> "") And WsDossierPer.FichePosition.Date_de_Valeur = "" Then
                TabErreurs.Add("Position : Veuillez remplir le champ 'Depuis le'")
            ElseIf WsDossierPer.FichePosition.Position <> "" And WsDossierPer.FichePosition.Date_de_Valeur <> "" Then
                If WsDossierPer.FichePosition.Modaliteservice <> "" And WsDossierPer.FichePosition.Taux_d_activite = "0,00" Then
                    TabErreurs.Add("Position : Veuillez remplir le champ 'Taux d'activité' ")
                ElseIf WsDossierPer.FichePosition.Taux_d_activite <> "0,00" And WsDossierPer.FichePosition.Modaliteservice = "" Then
                    TabErreurs.Add("Position : Veuillez remplir le champ 'Modalité de service'")
                End If

            End If
        End If


        If WsDossierPer.FicheGrade IsNot Nothing Then
            If (WsDossierPer.FicheGrade.Echelon <> "" Or WsDossierPer.FicheGrade.Mode_d_acces <> "" Or WsDossierPer.FicheGrade.Indice_brut <> "" Or WsDossierPer.FicheGrade.Indice_majore <> "" Or WsDossierPer.FicheGrade.Impersonnel <> "" Or WsDossierPer.FicheGrade.Categorie <> "" Or WsDossierPer.FicheGrade.Groupe_hierarchique <> "" Or WsDossierPer.FicheGrade.Filiere <> "" Or WsDossierPer.FicheGrade.Corps <> "" Or WsDossierPer.FicheGrade.Echelle_de_remuneration <> "" Or WsDossierPer.FicheGrade.Reliquat_valorise.ToString <> "" Or WsDossierPer.FicheGrade.Date_de_l_arrete <> "" Or WsDossierPer.FicheGrade.Reference_de_l_arrete <> "" Or WsDossierPer.FicheGrade.Date_de_parution <> "") And WsDossierPer.FicheGrade.Grade = "" And WsDossierPer.FicheGrade.Date_de_Valeur = "" Then
                TabErreurs.Add("Grade : Veuillez remplir les champs 'Grade' et 'Depuis le'")
            ElseIf (WsDossierPer.FicheGrade.Date_de_Valeur <> "" Or WsDossierPer.FicheGrade.Echelon <> "" Or WsDossierPer.FicheGrade.Mode_d_acces <> "" Or WsDossierPer.FicheGrade.Indice_brut <> "" Or WsDossierPer.FicheGrade.Indice_majore <> "" Or WsDossierPer.FicheGrade.Impersonnel <> "" Or WsDossierPer.FicheGrade.Categorie <> "" Or WsDossierPer.FicheGrade.Groupe_hierarchique <> "" Or WsDossierPer.FicheGrade.Filiere <> "" Or WsDossierPer.FicheGrade.Corps <> "" Or WsDossierPer.FicheGrade.Echelle_de_remuneration <> "" Or WsDossierPer.FicheGrade.Reliquat_valorise.ToString <> "" Or WsDossierPer.FicheGrade.Date_de_l_arrete <> "" Or WsDossierPer.FicheGrade.Reference_de_l_arrete <> "" Or WsDossierPer.FicheGrade.Date_de_parution <> "") And WsDossierPer.FicheGrade.Grade = "" Then
                TabErreurs.Add("Grade : Veuillez remplir le champ 'Grade'")
            ElseIf (WsDossierPer.FicheGrade.Grade <> "" Or WsDossierPer.FicheGrade.Echelon <> "" Or WsDossierPer.FicheGrade.Mode_d_acces <> "" Or WsDossierPer.FicheGrade.Indice_brut <> "" Or WsDossierPer.FicheGrade.Indice_majore <> "" Or WsDossierPer.FicheGrade.Impersonnel <> "" Or WsDossierPer.FicheGrade.Categorie <> "" Or WsDossierPer.FicheGrade.Groupe_hierarchique <> "" Or WsDossierPer.FicheGrade.Filiere <> "" Or WsDossierPer.FicheGrade.Corps <> "" Or WsDossierPer.FicheGrade.Echelle_de_remuneration <> "" Or WsDossierPer.FicheGrade.Reliquat_valorise.ToString <> "" Or WsDossierPer.FicheGrade.Date_de_l_arrete <> "" Or WsDossierPer.FicheGrade.Reference_de_l_arrete <> "" Or WsDossierPer.FicheGrade.Date_de_parution <> "") And WsDossierPer.FicheGrade.Date_de_Valeur = "" Then
                TabErreurs.Add("Grade : Veuillez remplir le champ 'Depuis le'")
            ElseIf (WsDossierPer.FicheGrade.Impersonnel <> "") Then
                If Double.Parse(WsDossierPer.FicheGrade.Impersonnel) > 2000 Then
                    TabErreurs.Add("Grade : Le champ 'ImPersonnel ne peut pas dépasser 2000'")
                End If
            End If
        End If

        If WsDossierPer.FicheEtablissement IsNot Nothing Then
            If (WsDossierPer.FicheEtablissement.Mode_de_recrutement <> "" Or WsDossierPer.FicheEtablissement.Date_de_depart <> "" Or WsDossierPer.FicheEtablissement.Motif_de_depart <> "" Or WsDossierPer.FicheEtablissement.Observations <> "") And WsDossierPer.FicheEtablissement.Administration = "" And WsDossierPer.FicheEtablissement.Date_de_Valeur = "" Then
                TabErreurs.Add("Etablissement : Veuillez remplir les champ 'Depuis le' et 'Administration'")
            ElseIf (WsDossierPer.FicheEtablissement.Administration <> "" Or WsDossierPer.FicheEtablissement.Mode_de_recrutement <> "" Or WsDossierPer.FicheEtablissement.Date_de_depart <> "" Or WsDossierPer.FicheEtablissement.Motif_de_depart <> "" Or WsDossierPer.FicheEtablissement.Observations <> "") And WsDossierPer.FicheEtablissement.Date_de_Valeur = "" Then
                TabErreurs.Add("Etablissement : Veuillez remplir le champ 'Depuis le'")
            ElseIf (WsDossierPer.FicheEtablissement.Date_de_Valeur <> "" Or WsDossierPer.FicheEtablissement.Mode_de_recrutement <> "" Or WsDossierPer.FicheEtablissement.Date_de_depart <> "" Or WsDossierPer.FicheEtablissement.Motif_de_depart <> "" Or WsDossierPer.FicheEtablissement.Observations <> "") And WsDossierPer.FicheEtablissement.Administration = "" Then
                TabErreurs.Add("Etablissement : Veuillez remplir le champ 'Administration'")
            End If
        End If

        If WsDossierPer.FicheAffectation IsNot Nothing Then
            If (WsDossierPer.FicheAffectation.Structure_de_2e_niveau <> "" Or WsDossierPer.FicheAffectation.Structure_de_3e_niveau <> "" Or WsDossierPer.FicheAffectation.Structure_de_4e_niveau <> "" Or WsDossierPer.FicheAffectation.Structure_de_5e_niveau <> "" Or WsDossierPer.FicheAffectation.Structure_de_6e_niveau = "" Or WsDossierPer.FicheAffectation.Bonification_indiciaire = "" Or WsDossierPer.FicheAffectation.Secteureconomique = "" Or WsDossierPer.FicheAffectation.Annuaire = "") And WsDossierPer.FicheAffectation.Date_de_Valeur = "" And WsDossierPer.FicheAffectation.Structure_de_1er_niveau = "" Then
                TabErreurs.Add("Affectation : Veuillez remplir les champs 'Depuis le' et 'Niveau 1'")
            ElseIf (WsDossierPer.FicheAffectation.Structure_de_1er_niveau <> "" Or WsDossierPer.FicheAffectation.Structure_de_2e_niveau <> "" Or WsDossierPer.FicheAffectation.Structure_de_3e_niveau <> "" Or WsDossierPer.FicheAffectation.Structure_de_4e_niveau <> "" Or WsDossierPer.FicheAffectation.Structure_de_5e_niveau <> "" Or WsDossierPer.FicheAffectation.Structure_de_6e_niveau = "" Or WsDossierPer.FicheAffectation.Bonification_indiciaire = "" Or WsDossierPer.FicheAffectation.Secteureconomique = "" Or WsDossierPer.FicheAffectation.Annuaire = "") And WsDossierPer.FicheAffectation.Date_de_Valeur = "" Then
                TabErreurs.Add("Affectation : Veuillez remplir le champ 'Depuis le'")
            ElseIf (WsDossierPer.FicheAffectation.Date_de_Valeur <> "" Or WsDossierPer.FicheAffectation.Structure_de_2e_niveau <> "" Or WsDossierPer.FicheAffectation.Structure_de_3e_niveau <> "" Or WsDossierPer.FicheAffectation.Structure_de_4e_niveau <> "" Or WsDossierPer.FicheAffectation.Structure_de_5e_niveau <> "" Or WsDossierPer.FicheAffectation.Structure_de_6e_niveau = "" Or WsDossierPer.FicheAffectation.Bonification_indiciaire = "" Or WsDossierPer.FicheAffectation.Secteureconomique = "" Or WsDossierPer.FicheAffectation.Annuaire = "") And WsDossierPer.FicheAffectation.Structure_de_1er_niveau = "" Then
                TabErreurs.Add("Affectation : Veuillez remplir le champ 'Niveau 1'")
            End If
        End If


        Dim LstFiches = WsDossierPer.Liste_Fiches("PER_ENFANT")

        If LstFiches IsNot Nothing Then
            For Each fiche In LstFiches
                Dim ficheenfant = CType(fiche, Virtualia.TablesObjet.ShemaPER.PER_ENFANT)
                If (ficheenfant.Lieudenaissance <> "" Or ficheenfant.Sexe <> "" Or ficheenfant.A_charge <> "") And ficheenfant.Date_de_naissance = "" And ficheenfant.Prenom = "" And ficheenfant.Nom = "" Then
                    TabErreurs.Add("Enfant : Veuillez remplir les champs 'Nom' , 'Prenom' et 'Date de naissance'")
                ElseIf (ficheenfant.Nom <> "" Or ficheenfant.Lieudenaissance <> "" Or ficheenfant.Sexe <> "" Or ficheenfant.A_charge <> "") And ficheenfant.Date_de_naissance = "" And ficheenfant.Prenom = "" Then
                    TabErreurs.Add("Enfant : Veuillez remplir les champs 'Prenom' et 'Date de naissance'")
                ElseIf (ficheenfant.Prenom <> "" Or ficheenfant.Lieudenaissance <> "" Or ficheenfant.Sexe <> "" Or ficheenfant.A_charge <> "") And ficheenfant.Date_de_naissance = "" And ficheenfant.Nom = "" Then
                    TabErreurs.Add("Enfant : Veuillez remplir les champs 'Nom' et 'Date de naissance'")
                ElseIf (ficheenfant.Date_de_naissance <> "" Or ficheenfant.Lieudenaissance <> "" Or ficheenfant.Sexe <> "" Or ficheenfant.A_charge <> "") And ficheenfant.Prenom = "" And ficheenfant.Nom = "" Then
                    TabErreurs.Add("Enfant : Veuillez remplir les champs 'Nom' , 'Prenom'")
                ElseIf (ficheenfant.Date_de_naissance <> "" Or ficheenfant.Nom <> "" Or ficheenfant.Lieudenaissance <> "" Or ficheenfant.Sexe <> "" Or ficheenfant.A_charge <> "") And ficheenfant.Prenom = "" Then
                    TabErreurs.Add("Enfant : Veuillez remplir le champ 'Prenom'")
                ElseIf (ficheenfant.Date_de_naissance <> "" Or ficheenfant.Prenom <> "" Or ficheenfant.Lieudenaissance <> "" Or ficheenfant.Sexe <> "" Or ficheenfant.A_charge <> "") And ficheenfant.Nom = "" Then
                    TabErreurs.Add("Enfant : Veuillez remplir le champ 'Nom'")
                ElseIf (ficheenfant.Prenom <> "" Or ficheenfant.Nom <> "" Or ficheenfant.Lieudenaissance <> "" Or ficheenfant.Sexe <> "" Or ficheenfant.A_charge <> "") And ficheenfant.Date_de_naissance = "" Then
                    TabErreurs.Add("Enfant : Veuillez remplir le champ 'date de naissance'")


                End If
            Next
        End If



        'If V_IndexFiche <> -1 Then
        If Ide_Dossier > 0 Then
            If WsDossierPer Is Nothing Then
                Return Nothing
            End If
            'WsDossierPer = V_ListeDossiers.Item(0)
            If V_WebFonction.PointeurConfiguration IsNot Nothing Then
                If WsDossierPer.FicheEtatCivil.Numero_d_identification_nation <> "" Then
                    Dim totalmodulo = Convert.ToDouble(WsDossierPer.FicheEtatCivil.Numero_d_identification_nation) Mod 97
                    Dim numNir = 97 - totalmodulo
                    If Convert.ToString(numNir) <> "" Then

                        If V_WebFonction.PointeurConfiguration.Parametres_Saisie.Si_Controle_NIR = True Then
                            If WsDossierPer.FicheEtatCivil.SiOK_NIR(WsDossierPer.FicheEtatCivil.Numero_d_identification_nation, Convert.ToString(numNir)) = False Then
                                TabErreurs.Add("Numéro d'Identification NIR erronée ou Clef NIR erronée.")
                            End If
                        End If
                    End If
                End If
            Else
                Dim totalmodulo = Convert.ToDouble(WsDossierPer.FicheEtatCivil.Numero_d_identification_nation) Mod 97
                Dim numNir = 97 - totalmodulo
                If WsFiche.SiOK_NIR(WsDossierPer.FicheEtatCivil.Numero_d_identification_nation, Convert.ToString(numNir)) = False Then
                    TabErreurs.Add("Numéro d'Identification NIR erronée ou Clef NIR erronée.")
                End If
            End If


            If WsDossierPer.FicheEtatCivil.Numero_d_identification_nation <> "" Then
                Try
                    Dim totalmodulo = Convert.ToDouble(WsDossierPer.FicheEtatCivil.Numero_d_identification_nation) Mod 97
                    Dim numNir = 97 - totalmodulo
                    WsDossierPer.TableauMaj(1, 12) = Convert.ToString(numNir)
                Catch ex As Exception
                    TabErreurs.Add("Le numéro de sécurité sociale ne peut pas contenir des lettres.")
                    Return TabErreurs
                End Try

                If WsDossierPer.FicheEtatCivil.Numero_d_identification_nation.Length <> 13 Then
                    TabErreurs.Add("Le numéro de sécurité sociale doit contenir 13 chiffres.")
                    Return TabErreurs
                End If

                Dim nir_annee As String = WsDossierPer.FicheEtatCivil.Numero_d_identification_nation.Substring(1, 2)
                Dim date_annee As String = Right(WsDossierPer.FicheEtatCivil.Date_de_naissance, 2)
                If nir_annee <> date_annee Then
                    TabErreurs.Add("Votre numéro de sécurité sociale ne correspond pas avec votre date de naissance.")
                    Return TabErreurs
                End If
                If WsDossierPer.FicheEtatCivil.Numero_d_identification_nation.Substring(3, 2) <> WsDossierPer.FicheEtatCivil.Date_de_naissance.Substring(3, 2) Then
                    TabErreurs.Add("Votre numéro de sécurité sociale ne correspond pas avec votre date de naissance.")
                    Return TabErreurs
                End If
            End If

            If WsDossierPer.FicheEtatCivil.Numero_d_identification_nation <> "" And WsDossierPer.FicheEtatCivil.Sexe <> "" And WsDossierPer.FicheEtatCivil.Departement_ou_pays_de_naissan <> "" Then
                Try
                    Dim totalmodulo = Convert.ToDouble(WsDossierPer.FicheEtatCivil.Numero_d_identification_nation) Mod 97
                    Dim numNir = 97 - totalmodulo
                    WsDossierPer.TableauMaj(1, 12) = Convert.ToString(numNir)
                Catch ex As Exception
                    TabErreurs.Add("Le numéro de sécurité sociale ne peut pas contenir des lettres.")
                    Return TabErreurs
                End Try

                If WsDossierPer.FicheEtatCivil.Numero_d_identification_nation.Length <> 13 Then
                    TabErreurs.Add("Le numéro de sécurité sociale doit contenir 13 chiffres.")
                    Return TabErreurs
                End If

                If WsDossierPer.FicheEtatCivil.SiOK_NumNIR(WsDossierPer.FicheEtatCivil.Numero_d_identification_nation, WsDossierPer.FicheEtatCivil.Date_de_naissance, WsDossierPer.FicheEtatCivil.Sexe, WsDossierPer.FicheEtatCivil.Departement_ou_pays_de_naissan) = False Then
                    TabErreurs.Add("Numéro d'Identification NIR erronée ou Clef NIR erronée.")
                End If

            End If
        End If
        If TabErreurs.Count > 0 Then
            Return TabErreurs
        Else
            Return Nothing
        End If
        'Else
        'Return Nothing
        'End If
    End Function

    '***  AKR   
    Protected Sub MessageDialogue(ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
        HPopupMsg.Value = "1"
        CellMessage.Visible = True
        MsgVirtualia.AfficherMessage = e
        PopupMsg.Show()
    End Sub

    Public Property SaisiVue() As Integer
        Get
            Return WsVue
        End Get
        Set(value As Integer)
            WsVue = value
        End Set
    End Property


    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
    ''*** FIN AKR
End Class