﻿<%@ Page Language="vb" MasterPageFile="~/PagesMaitre/VirtualiaAttente.master" AutoEventWireup="false" CodeBehind="FrmAttente.aspx.vb" 
    Inherits="Virtualia.Net.FrmAttente" MaintainScrollPositionOnPostback="True" %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaAttente.master"  %>

<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="CorpsMaster">
    <asp:UpdatePanel ID="UpdatePanelAttente" runat="server">
        <ContentTemplate>
            <asp:Timer ID="TimerAttente" runat="server" Interval="1000">
            </asp:Timer>
            <asp:Table ID="CadreAttente" runat="server" BackColor="Transparent" HorizontalAlign="Center" Width="1150px" Height="800px"
                       Style="margin-top: 1px; border-width: 2px; border-style: Solid; border-color: #B0E0D7">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ForeColor="White" Font-Bold="false" Height="60px">
                        <asp:UpdateProgress ID="UpdateAttente" runat="server">
                            <ProgressTemplate>
                                <asp:Table ID="Cadre_Attente" runat="server" BackColor="Transparent">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Image ID="ImgAttente" runat="server" Height="30px" Width="30px" ImageUrl="~/Images/General/Loading.gif" />
                                        </asp:TableCell>
                                        <asp:TableCell Width="10px" Height="10px" />
                                        <asp:TableCell>
                                            <asp:Label ID="EtiAttente" runat="server" Width="280px" Text="... Veuillez patienter ..." ForeColor="White" BorderStyle="None" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </ProgressTemplate>
                       </asp:UpdateProgress>
                    </asp:TableCell>
                 </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
