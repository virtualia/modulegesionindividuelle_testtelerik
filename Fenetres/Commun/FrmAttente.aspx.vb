﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class FrmAttente
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomState As String = "VAttente"

    Public Property V_Index As Integer
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                If IsNumeric(VCache(0)) Then
                    Return CInt(VCache(0))
                End If
            End If
            Return 0
        End Get
        Set(ByVal value As Integer)
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) Is Nothing Then
                VCache = New ArrayList
                VCache.Add(value)
                VCache.Add("")
                VCache.Add("")
            Else
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                If IsNumeric(VCache(0)) Then
                    If CInt(VCache(0)) = value Then
                        Exit Property
                    End If
                End If
                VCache(0) = value
                Me.ViewState.Remove(WsNomState)
            End If
            Me.ViewState.Add(WsNomState, VCache)
        End Set
    End Property

    Public Property V_UrlCible As String
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                Return VCache(1).ToString
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            If value = "" Then
                Exit Property
            End If
            Dim VCache As ArrayList
            VCache = CType(Me.ViewState(WsNomState), ArrayList)
            If VCache Is Nothing Then
                VCache = New ArrayList
                VCache.Add("")
                VCache.Add("")
                VCache.Add("")
            Else
                Me.ViewState.Remove(WsNomState)
            End If
            VCache(1) = value
            Me.ViewState.Add(WsNomState, VCache)
        End Set
    End Property

    Public Property V_Parametre As String
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                Return VCache(2).ToString
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            If value = "" Then
                Exit Property
            End If
            Dim VCache As ArrayList
            VCache = CType(Me.ViewState(WsNomState), ArrayList)
            If VCache Is Nothing Then
                VCache = New ArrayList
                VCache.Add("")
                VCache.Add("")
                VCache.Add("")
            Else
                Me.ViewState.Remove(WsNomState)
            End If
            Me.ViewState.Remove(WsNomState)
            VCache(2) = value
            Me.ViewState.Add(WsNomState, VCache)
        End Set
    End Property

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Dim NumLogo As String = Server.HtmlDecode(Request.QueryString("Index"))
        Dim LienCible As String = Server.HtmlDecode(Request.QueryString("Lien"))
        Dim Param As String = Server.HtmlDecode(Request.QueryString("Param"))

        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        If LienCible = "" Then
            Response.Redirect(WebFct.PointeurGlobal.UrlDestination("", 0, "")) 'Retour à l'accueil Virtualia.Net
        End If
        If IsNumeric(NumLogo) Then
            V_Index = CInt(NumLogo)
        Else
            NumLogo = "0"
        End If
        Select Case Strings.Left(LienCible, 4)
            Case "http"
                V_UrlCible = LienCible
            Case Else
                If Param = "" Then
                    V_UrlCible = LienCible
                Else
                    V_UrlCible = LienCible & "?Param=" & Param
                End If
        End Select

        Dim Charte As New Virtualia.Systeme.Fonctions.CharteGraphique
        CadreAttente.BackColor = Charte.CouleurTuile(CInt(NumLogo))
    End Sub

    Private Sub TimerAttente_Tick(sender As Object, e As EventArgs) Handles TimerAttente.Tick
        Dim ChaineHttp As String = V_UrlCible
        If ChaineHttp = "" Then
            Exit Sub
        End If
        V_UrlCible = ""
        Response.Redirect(ChaineHttp)
    End Sub

End Class