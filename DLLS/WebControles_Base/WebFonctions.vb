﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports System.Web.UI

Namespace Controles
    Public Class WebFonctions
        Private WsRhFonction As Virtualia.Systeme.Fonctions.Generales
        Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates
        Private WsCharte As Virtualia.Systeme.Fonctions.CharteGraphique
        Private WsTypeControle As Integer = 0 'UserControl, 1 Page, 2 MasterPage

        Private WsAppObjetGlobal As Virtualia.Net.Session.ObjetGlobalBase
        Private WsAppUtiSession As Virtualia.Net.Session.ObjetSession
        Private WsNosession As String

        Private WsApplication As Web.HttpApplicationState
        Private WsSession As Web.SessionState.HttpSessionState

        Private Enum TypeControle As Integer
            TypeUserControl = 0
            TypePage = 1
            TypeMasterPage = 2
        End Enum

        Public ReadOnly Property PointeurGlobal As Virtualia.Net.Session.ObjetGlobalBase
            Get
                Return WsAppObjetGlobal
            End Get
        End Property

        Public ReadOnly Property PointeurUtilisateur As Virtualia.Net.Session.ObjetSession
            Get
                Return WsAppUtiSession
            End Get
        End Property

        Public ReadOnly Property PointeurReferentiel As Virtualia.Ressources.Datas.EnsembleReferences
            Get
                Return WsAppObjetGlobal.VirInstanceReferentiel
            End Get
        End Property

        Public ReadOnly Property PointeurParametres As Virtualia.Net.Session.ParametresGlobaux
            Get
                Return WsAppObjetGlobal.PointeurParametresGlobaux
            End Get
        End Property

        Public ReadOnly Property PointeurConfiguration As Virtualia.Systeme.Configuration.FichierConfig
            Get
                Return WsAppObjetGlobal.VirConfiguration
            End Get
        End Property

        Public ReadOnly Property PointeurContexte As Virtualia.Net.Session.ObjetNavigation
            Get
                If WsAppUtiSession Is Nothing Then
                    Return Nothing
                End If
                Return WsAppUtiSession.V_PointeurContexte
            End Get
        End Property

        Public ReadOnly Property PointeurDossier(ByVal Ide As Integer) As Virtualia.Ressources.Datas.ObjetDossierPER
            Get
                If Ide = 0 Then
                    Return Nothing
                End If
                If WsAppUtiSession Is Nothing Then
                    Return Nothing
                End If
                Return WsAppObjetGlobal.GEN_DossierPER(Ide)
            End Get
        End Property

        Public ReadOnly Property PointeurIdeSysRef(ByVal PtdeVue As Integer, ByVal Ide As Integer) As Virtualia.Ressources.Datas.ObjetDossierREF
            Get
                If Ide = 0 Then
                    Return Nothing
                End If
                If WsAppUtiSession Is Nothing Then
                    Return Nothing
                End If
                Return PointeurContexte.SysRef_PointeurDossier(PtdeVue, Ide)
            End Get
        End Property

        Public ReadOnly Property ViRhDates As Virtualia.Systeme.Fonctions.CalculDates
            Get
                Return WsRhDates
            End Get
        End Property

        Public ReadOnly Property ViRhFonction As Virtualia.Systeme.Fonctions.Generales
            Get
                Return WsRhFonction
            End Get
        End Property

        Public Function MajFiche(ByVal PtdeVue As Integer, ByVal NumObjet As Integer, ByVal Ide_Dossier As Integer,
                                 ByVal CacheMaj As List(Of String), ByVal Chainelue As String, ByVal DatasSignificatives As String, ByVal NumInfoCer As Integer) As String
            If WsAppUtiSession Is Nothing Then
                Return ""
            End If
            Dim IndiceI As Integer
            Dim SiOK As Boolean
            Dim ChaineMaj As String

            Dim Tableaudata(PointeurDicoObjet(PtdeVue, NumObjet).LimiteMaximum) As String
            For IndiceI = 0 To Tableaudata.Count - 1
                If CacheMaj.Item(IndiceI) Is Nothing Then
                    Tableaudata(IndiceI) = ""
                Else
                    Tableaudata(IndiceI) = CacheMaj.Item(IndiceI)
                End If
            Next IndiceI
            '*****************
            If PointeurDicoObjet(PtdeVue, NumObjet).SiCertification = 1 And Chainelue <> Strings.Join(Tableaudata, VI.Tild) And DatasSignificatives <> "" And NumInfoCer > 0 Then
                Dim ObjetCertification As Virtualia.Net.Session.ObjetTransaction
                Dim ChaineCryptee As String
                Dim TableauLu(0) As String
                Dim ChainagePrecedent As String = ""
                ObjetCertification = New Virtualia.Net.Session.ObjetTransaction(WsAppObjetGlobal)
                If Chainelue <> "" Then
                    TableauLu = Strings.Split(Chainelue, VI.Tild, -1)
                    ChainagePrecedent = TableauLu(NumInfoCer)
                End If
                ObjetCertification.ChainageOrigine(WsNosession, "Controles.WebFonctions") = ChainagePrecedent
                ChaineCryptee = ObjetCertification.CreerTransactionCertifiee(NumObjet, Ide_Dossier, DatasSignificatives)
                Tableaudata(NumInfoCer) = ChaineCryptee
                CacheMaj.Item(NumInfoCer) = ChaineCryptee
            End If
            '******************
            Select Case PtdeVue
                Case VI.PointdeVue.PVueApplicatif
                    SiOK = PointeurDossier(Ide_Dossier).V_MettreAJourFiche(PointeurUtilisateur.V_NomdUtilisateurSgbd,
                                            PointeurUtilisateur.V_NomdeConnexion, NumObjet, Chainelue, Strings.Join(Tableaudata, VI.Tild))
                Case Else
                    SiOK = PointeurIdeSysRef(PtdeVue, Ide_Dossier).V_MettreAJourFiche(PointeurUtilisateur.V_NomdUtilisateurSgbd,
                                            PointeurUtilisateur.V_NomdeConnexion, NumObjet, Chainelue, Strings.Join(Tableaudata, VI.Tild))
            End Select

            ChaineMaj = ""
            If SiOK Then
                Select Case PointeurDicoObjet(PtdeVue, NumObjet).VNature
                    Case VI.TypeObjet.ObjetSimple, VI.TypeObjet.ObjetTableau, VI.TypeObjet.ObjetMemo
                        For IndiceI = 1 To Tableaudata.Count - 1
                            ChaineMaj &= Tableaudata(IndiceI) & VI.Tild
                        Next IndiceI
                    Case Else
                        For IndiceI = 0 To Tableaudata.Count - 1
                            ChaineMaj &= Tableaudata(IndiceI) & VI.Tild
                        Next IndiceI
                End Select
                Return Ide_Dossier.ToString & VI.Tild & ChaineMaj
            Else
                Return ""
            End If

        End Function

        Public ReadOnly Property ConfirmerSuppressionFiche(ByVal NumObjet As Integer, ByVal Occurence As String) As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
            Get
                Dim TitreMsg As String = "Suppression de la fiche " & Occurence
                Dim Msg As String = "Confirmez-vous la suppression de cette fiche ?"
                Dim NatureCmd As String = "Oui;Non"

                Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
                Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("SuppFiche", NumObjet, Occurence, NatureCmd, TitreMsg, Msg)
                Return Evenement
            End Get
        End Property

        Public ReadOnly Property SiFicheASupprimer(ByVal CacheMaj As List(Of String)) As Boolean
            Get
                If CacheMaj Is Nothing Then
                    Return True
                End If
                For Each Element In CacheMaj
                    If Element IsNot Nothing AndAlso Element <> "" Then
                        Return False
                    End If
                Next
                Return True
            End Get
        End Property

        Public ReadOnly Property Evt_MessageInformatif(ByVal NumObjet As Integer, ByVal TitreMsg As String, ByVal MsgInfos As List(Of String), Optional ByVal Ide As Integer = 0) As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
            Get
                If MsgInfos Is Nothing Then
                    Return Nothing
                End If
                Dim Msg As String = ""
                Dim I As Integer
                For I = 0 To MsgInfos.Count - 1
                    Msg &= MsgInfos.Item(I) & vbCrLf
                Next I
                Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
                Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("MajDossier", NumObjet, Ide.ToString, "OK", TitreMsg, Msg)
                Return Evenement
            End Get
        End Property


        Public ReadOnly Property Evt_MessageBloquant(ByVal NumObjet As Integer, ByVal TitreMsg As String, ByVal MsgErreurs As List(Of String)) As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
            Get
                If MsgErreurs Is Nothing Then
                    Return Nothing
                End If
                Dim Msg As String = "ANOMALIES BLOQUANTES" & vbCrLf
                Dim I As Integer
                For I = 0 To MsgErreurs.Count - 1
                    Msg &= MsgErreurs.Item(I) & vbCrLf
                Next I
                Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
                Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("MajDossier", NumObjet, "", "KO", TitreMsg, Msg)
                Return Evenement
            End Get
        End Property

        Public ReadOnly Property Evt_MessageConfirmation(ByVal NumObjet As Integer, ByVal TitreMsg As String, ByVal MsgInfos As List(Of String), Optional ByVal Ide As Integer = 0) As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
            Get
                If MsgInfos Is Nothing Then
                    Return Nothing
                End If
                Dim Msg As String = ""
                Dim I As Integer
                For I = 0 To MsgInfos.Count - 1
                    Msg &= MsgInfos.Item(I) & vbCrLf
                Next I
                Msg &= "Confirmez-vous cette opération ?"
                Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
                Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("MajDossier", NumObjet, Ide.ToString, "Oui;Non", TitreMsg, Msg)
                Return Evenement
            End Get
        End Property

        Public Function VerifierCookie(ByVal CtlPage As System.Web.UI.Page, ByVal NoSession As String) As Boolean
            Dim CacheIdent As VCaches.CacheIdentification = LireCookie(CtlPage)
            If CacheIdent Is Nothing Then
                Return False
            End If
            If CacheIdent.NumeroSession = NoSession Then
                Return True
            End If
            Return False
        End Function

        Public Sub EcrireCookie(ByVal CtlPage As System.Web.UI.Page, ByVal Valeur As VCaches.CacheIdentification)
            Dim Vcookie As System.Web.HttpCookie

            Vcookie = CtlPage.Request.Cookies.Get("MyVirtualia")
            If Vcookie Is Nothing Then
                Vcookie = New System.Web.HttpCookie("MyVirtualia")
            End If
            Vcookie.Value = EncodageCookie(Valeur.Chaine_Cookie)
            Vcookie.Expires = DateTime.Now.AddMonths(3)
            CtlPage.Response.Cookies.Add(Vcookie)
        End Sub

        Public Function LireCookie(ByVal CtlPage As System.Web.UI.Page) As VCaches.CacheIdentification
            Dim Vcookie As System.Web.HttpCookie
            Dim CacheIdent As New VCaches.CacheIdentification

            Vcookie = CtlPage.Request.Cookies.Get("MyVirtualia")
            If Vcookie Is Nothing OrElse Vcookie.Value Is Nothing Then
                Return Nothing
            End If
            CacheIdent.Chaine_Cookie = DecodageCookie(Vcookie.Value)
            Return CacheIdent
        End Function

        Public ReadOnly Property EncodageCookie(ByVal Valeur As String) As String
            Get
                Dim TableauByte() As Byte
                TableauByte = System.Text.Encoding.UTF8.GetBytes(Valeur)
                Return BitConverter.ToString(TableauByte)
            End Get
        End Property

        Public ReadOnly Property DecodageCookie(ByVal Valeur As String) As String
            Get
                Dim I As Integer
                Dim Tableaudata() As String = Strings.Split(Valeur, "-")
                Dim TableauByte() As Byte
                ReDim TableauByte(Tableaudata.Length - 1)
                Try
                    For I = 0 To TableauByte.Count - 1
                        TableauByte(I) = Convert.ToByte(Tableaudata(I), 16)
                    Next I
                    Return System.Text.Encoding.UTF8.GetString(TableauByte)
                Catch ex As Exception
                    Return Valeur
                End Try
            End Get
        End Property

        Public Function LostFocusStd(ByVal NatureDonnee As String, ByVal FormatDonnee As String, ByVal Longueur As Integer, ByVal Valeur As String) As String
            Dim Chaine As String = Valeur

            If Valeur.Length > 0 Then
                If Strings.Left(Valeur, 1) = "Z" And NatureDonnee <> "Alpha" Then
                    Return Valeur
                End If
            End If
            Select Case NatureDonnee
                Case "Alpha"
                    If FormatDonnee = "TelleQue" Then
                        Chaine = Valeur
                    Else
                        Chaine = VIR_FICHE.F_FormatAlpha(Valeur.ToUpper, Longueur)
                        Chaine = WsRhFonction.ChaineSansAccent(Chaine, True)
                    End If
                Case "Date"
                    Chaine = WsRhDates.DateSaisieVerifiee(Valeur)
                Case "Numerique"
                    Dim NbDecimales As Integer = 0
                    Select Case FormatDonnee
                        Case "Decimal1"
                            NbDecimales = 1
                        Case "Decimal2"
                            NbDecimales = 2
                    End Select
                    Chaine = VIR_FICHE.F_FormatNumerique(Valeur, NbDecimales)
                Case "Table"
                    If Valeur = "Blanc" Then
                        Return ""
                    End If
            End Select
            Return Chaine
        End Function

        Public Function InverseGEST(ByVal NatureDonnee As String, ByVal FormatDonnee As String, ByVal Longueur As Integer, ByVal Valeur As String) As String
            Dim Chaine As String = Valeur
            Dim ZCalc As Double

            If Valeur.Trim = "" Then
                Return ""
            End If
            If Strings.Left(Valeur, 1) = "Z" And NatureDonnee <> "Alpha" Then
                Return Valeur
            End If
            Select Case FormatDonnee
                Case "EntierOuBL"
                    If Valeur.Trim <> "" Then
                        ZCalc = Val(Valeur)
                        Chaine = VIR_FICHE.F_FormatNumerique(ZCalc.ToString)
                    Else
                        Chaine = VIR_FICHE.F_FormatAlpha(Valeur.ToUpper, Longueur)
                    End If
                    Return Chaine
            End Select

            Select Case NatureDonnee
                Case "Alpha"
                    Chaine = VIR_FICHE.F_FormatAlpha(Valeur.ToUpper, Longueur)
                    Chaine = WsRhFonction.ChaineSansAccent(Chaine, True)
                Case "Date"
                    Select Case FormatDonnee
                        Case "AAAA"
                            Chaine = Valeur
                        Case "AAAAMM"
                            Chaine = WsRhDates.DateSaisieVerifiee("01" & Strings.Right(Valeur, 2) & Strings.Left(Valeur, 4))
                        Case "MMAAAA"
                            Chaine = WsRhDates.DateSaisieVerifiee("01" & Valeur)
                        Case "JJMMAAAA"
                            Chaine = WsRhDates.DateSaisieVerifiee(Valeur)
                        Case Else
                            Chaine = WsRhDates.DateSaisieVerifiee(Valeur)
                    End Select
                Case "Numerique"
                    Dim NbDecimales As Integer = 0
                    Select Case FormatDonnee
                        Case "Decimal1"
                            NbDecimales = 1
                            ZCalc = Val(Valeur) / 10
                        Case "Decimal2"
                            NbDecimales = 2
                            ZCalc = Val(Valeur) / 100
                        Case "EntierBL"
                            ZCalc = Val(Valeur)
                            If ZCalc = 0 Then
                                Return ""
                            End If
                        Case Else
                            ZCalc = Val(Valeur)
                    End Select
                    Chaine = VIR_FICHE.F_FormatNumerique(ZCalc.ToString, NbDecimales)
            End Select
            Return Chaine
        End Function

        Public Function InverseBASEPAY(ByVal NatureDonnee As String, ByVal Valeur As String) As String
            Dim Chaine As String = Valeur
            Select Case NatureDonnee
                Case "Date"
                    If Valeur.Length = 8 Then
                        Chaine = Strings.Right(Valeur, 2) & "/" & Strings.Mid(Valeur, 5, 2) & "/" & Strings.Left(Valeur, 4)
                    ElseIf Valeur.Length = 6 Then
                        Chaine = Strings.Right(Valeur, 2) & "/" & Strings.Left(Valeur, 4)
                    Else
                        Chaine = ""
                    End If
                Case "Numerique"
                    If Strings.InStr(Valeur, ",") > 0 Or Strings.InStr(Valeur, ".") > 0 Then
                        If WsRhFonction.ConversionDouble(Valeur) = 0 Then
                            Chaine = ""
                        Else
                            Chaine = Strings.Format(WsRhFonction.ConversionDouble(Valeur), "#0.00")
                        End If
                    Else
                        If Val(Valeur) = 0 Then
                            Chaine = ""
                        Else
                            Chaine = Val(Valeur).ToString
                        End If
                    End If
            End Select
            Return Chaine
        End Function

        Public ReadOnly Property PointeurDicoObjet(ByVal PointdeVue As Integer, ByVal Numobjet As Integer) As Virtualia.Systeme.MetaModele.Donnees.Objet
            Get
                If PointdeVue = 0 Or Numobjet = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Dim Predicat As New Virtualia.Systeme.MetaModele.Predicats.PredicateDictionnaire(PointdeVue, Numobjet)
                Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheObjetDictionnaire)
                Dico = WsAppObjetGlobal.VirListeObjetsDico.FindAll(AddressOf Predicat.InformationParIdeObjet)
                If Dico.Count = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Return Dico.Item(0).PointeurModele
            End Get
        End Property

        Public ReadOnly Property PointeurDicoInfo(ByVal PointdeVue As Integer, ByVal Numobjet As Integer, ByVal NumInfo As Integer) As Virtualia.Systeme.MetaModele.Donnees.Information
            Get
                If PointdeVue = 0 Or Numobjet = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Dim Predicat As New Virtualia.Systeme.MetaModele.Predicats.PredicateDictionnaire(PointdeVue, Numobjet, NumInfo)
                Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoDictionnaire)
                Dico = WsAppObjetGlobal.VirListeInfosDico.FindAll(AddressOf Predicat.InformationParIdeInfo)
                If Dico.Count = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Return Dico.Item(0).PointeurModele
            End Get
        End Property

        Public ReadOnly Property PointeurDicoExperte(ByVal PointdeVue As Integer, ByVal Numobjet As Integer, ByVal NumInfo As Integer) As Virtualia.Systeme.MetaModele.Expertes.InformationExperte
            Get
                If PointdeVue = 0 Or Numobjet = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Dim Predicat As New Virtualia.Systeme.MetaModele.Predicats.PredicateDictionnaire(PointdeVue, Numobjet, NumInfo)
                Dim Dico As List(Of Virtualia.Systeme.MetaModele.Outils.FicheInfoExperte)
                Dico = WsAppObjetGlobal.VirListeExpertesDico.FindAll(AddressOf Predicat.InformationParIdeExperte)
                If Dico.Count = 0 Then
                    Return Nothing
                    Exit Property
                End If
                Return Dico.Item(0).PointeurModele
            End Get
        End Property

        Public ReadOnly Property InfoExperte(ByVal Ptdevue As Integer, ByVal NumObjet As Integer, ByVal IdeExperte As Integer, ByVal Ide As Integer) As String
            Get
                Dim Result As String

                WsAppObjetGlobal.PointeurDllExpert.ClasseExperte(Ptdevue, NumObjet) = VI.OptionInfo.DicoRecherche
                Try
                    Result = WsAppObjetGlobal.PointeurDllExpert.Donnee(Ptdevue, NumObjet, IdeExperte, Ide, "01/01/1950", WsRhDates.DateduJour)
                Catch Ex As Exception
                    Result = ""
                End Try
                Return Result
            End Get
        End Property

        Public Function Controle_KeyPressStd(ByVal PtdeVue As Integer, ByVal NumObjet As Integer, ByVal NumInfo As Integer, ByVal KeyAscii As String) As String
            Select Case KeyAscii
                Case "8"
                    Return KeyAscii
                    Exit Function
            End Select
            Select Case KeyAscii
                Case "ToucheSuppression"
                    Return "0"
                    Exit Function
            End Select
            Select Case KeyAscii
                Case Is = VI.Tild
                    Return "0"
                    Exit Function
            End Select
            Select Case KeyAscii
                Case Is = VI.SigneBarre
                    Return "0"
                    Exit Function
            End Select
            Select Case PointeurDicoInfo(PtdeVue, NumObjet, NumInfo).VNature
                Case VI.NatureDonnee.DonneeNumerique
                    Select Case PointeurDicoInfo(PtdeVue, NumObjet, NumInfo).Format
                        Case VI.FormatDonnee.Monetaire, VI.FormatDonnee.Pourcentage, VI.FormatDonnee.Standard
                            Select Case KeyAscii
                                Case "."
                                    Return KeyAscii
                                    Exit Function
                                Case ","
                                    Return Strings.Chr(46)
                                    Exit Function
                            End Select
                        Case VI.FormatDonnee.Decimal1 To VI.FormatDonnee.Decimal6
                            Select Case KeyAscii
                                Case "."
                                    Return KeyAscii
                                    Exit Function
                                Case ","
                                    Return Strings.Chr(46)
                                    Exit Function
                            End Select
                        Case VI.FormatDonnee.Fixe, VI.FormatDonnee.Duree
                            If KeyAscii = "." Or KeyAscii = "," Then
                                Return "0"
                                Exit Function
                            End If
                    End Select
                    If KeyAscii < "0" Or KeyAscii > "9" Then Return "0"
                Case VI.NatureDonnee.DonneeHeure
                    Select Case PointeurDicoInfo(PtdeVue, NumObjet, NumInfo).Format
                        Case VI.FormatDonnee.DureeHeure
                            Select Case KeyAscii
                                Case "h"
                                    Return KeyAscii
                                    Exit Function
                            End Select
                        Case Else
                            Select Case KeyAscii
                                Case ":"
                                    Return KeyAscii
                                    Exit Function
                            End Select
                    End Select
                    If KeyAscii < "0" Or KeyAscii > "9" Then Return "0"
                Case VI.NatureDonnee.DonneeTable, VI.NatureDonnee.DonneeTableMemo
                    Return "0"
            End Select
            Return KeyAscii
        End Function

        Public Function Controle_LostFocusStd(ByVal NatureDonnee As Integer, ByVal FormatDonnee As Integer, ByVal Valeur As String) As String
            Dim Chaine As String = Valeur
            Select Case NatureDonnee
                Case VI.NatureDonnee.DonneeExperte
                    Return Valeur
                    Exit Function
                Case VI.NatureDonnee.DonneeTexte
                    Select Case FormatDonnee
                        Case VI.FormatDonnee.Majuscule
                            Chaine = Valeur.ToUpper
                        Case VI.FormatDonnee.Minuscule
                            Chaine = WsRhFonction.Lettre1Capi(Valeur, 0)
                        Case VI.FormatDonnee.L1Capi
                            Chaine = WsRhFonction.Lettre1Capi(Valeur, 1)
                    End Select
                Case VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeDateTime
                    Select Case FormatDonnee
                        Case VI.FormatDonnee.Annee, VI.FormatDonnee.FinAnnee
                            Select Case Valeur
                                Case Is <> ""
                                    Select Case Len(Valeur)
                                        Case Is > 4
                                            Chaine = Right(Valeur, 4)
                                        Case Is = 4
                                            Chaine = Valeur
                                    End Select
                            End Select
                            Select Case Chaine
                                Case Is <> ""
                                    Select Case FormatDonnee
                                        Case VI.FormatDonnee.Annee
                                            Chaine = "01/01/" & Chaine
                                        Case VI.FormatDonnee.FinAnnee
                                            Chaine = "31/12/" & Chaine
                                    End Select
                                    Chaine = WsRhDates.DateSaisieVerifiee(Chaine)
                                    Chaine = Strings.Right(Chaine, 4)
                            End Select
                        Case Else
                            Chaine = WsRhDates.DateSaisieVerifiee(Valeur)
                    End Select
                Case VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeCalculee
                    Select Case Valeur
                        Case Is <> ""
                            Select Case FormatDonnee
                                Case VI.FormatDonnee.Standard
                                    If Strings.InStr(Valeur, ".") > 0 Or InStr(Valeur, ",") > 0 Then
                                        Chaine = Strings.Format(WsRhFonction.ConversionDouble(Valeur), "###0.0")
                                    Else
                                        Chaine = Format(Val(Valeur), "###0")
                                    End If
                                Case VI.FormatDonnee.Fixe
                                    Chaine = Strings.Format(Val(Valeur), "###0")
                                Case VI.FormatDonnee.Pourcentage
                                    Chaine = Strings.Format(WsRhFonction.ConversionDouble(Valeur), "##0.00#")
                                Case VI.FormatDonnee.Monetaire, VI.FormatDonnee.Decimal2
                                    Chaine = Strings.Format(WsRhFonction.ConversionDouble(Valeur), "#,##0.00")
                                Case VI.FormatDonnee.Decimal1
                                    Select Case Right(Valeur, 2)
                                        Case ".0", ",0"
                                            Chaine = Strings.Format(WsRhFonction.ConversionDouble(Valeur), "#,##0")
                                        Case Else
                                            Chaine = Strings.Format(WsRhFonction.ConversionDouble(Valeur), "#,##0.0")
                                    End Select
                                Case VI.FormatDonnee.Decimal3
                                    Chaine = Strings.Format(WsRhFonction.ConversionDouble(Valeur), "#,##0.000")
                                Case VI.FormatDonnee.Decimal4
                                    Chaine = Strings.Format(WsRhFonction.ConversionDouble(Valeur), "#,##0.0000")
                                Case VI.FormatDonnee.Decimal5
                                    Chaine = Strings.Format(WsRhFonction.ConversionDouble(Valeur), "#,##0.00000")
                                Case VI.FormatDonnee.Duree
                                    Chaine = Strings.Mid(Valeur, 3, 2) & " A. " & Strings.Mid(Valeur, 5, 2) & " M. " & Strings.Mid(Valeur, 7, 2) & " J. "
                                Case VI.FormatDonnee.DureeHeure
                                    Chaine = WsRhDates.VerifHeure(Valeur, False)
                                Case Else
                                    Chaine = Valeur.ToUpper
                            End Select
                        Case Else
                            Chaine = Valeur
                    End Select
                Case VI.NatureDonnee.DonneeHeure
                    Select Case Valeur
                        Case Is <> ""
                            Select Case FormatDonnee
                                Case VI.FormatDonnee.DureeHeure
                                    Chaine = WsRhDates.VerifHeure(Valeur, False)
                                Case Else
                                    Chaine = WsRhDates.VerifHeure(Valeur, True)
                            End Select
                    End Select
            End Select
            Return Chaine
        End Function

        Private Sub InitialiserSession()
            Dim NoBd As Integer = CInt(Configuration.ConfigurationManager.AppSettings("NumeroDatabase"))
            WsAppObjetGlobal = DirectCast(WsApplication.Item("VirGlobales"), Virtualia.Net.Session.ObjetGlobalBase)

            If WsSession.Item("IDVirtualia") IsNot Nothing Then
                WsNosession = WsSession.Item("IDVirtualia").ToString
            End If
            If WsAppObjetGlobal Is Nothing Then
                Dim VirObjetGlobal As Virtualia.Net.Session.ObjetGlobalBase
                Try
                    VirObjetGlobal = New Virtualia.Net.Session.ObjetGlobalBase(Configuration.ConfigurationManager.AppSettings("NomUtilisateur"), NoBd)
                    WsApplication.Add("VirGlobales", VirObjetGlobal)

                Catch ex As Exception
                    Exit Sub
                End Try
            End If
            If WsNosession <> "" Then
                WsAppUtiSession = WsAppObjetGlobal.ItemSessionModule(WsNosession)
            Else
                WsAppUtiSession = Nothing
            End If
        End Sub

        Public Function VirSourceHtml(ByVal Ctrl As System.Web.UI.Control) As String
            Dim ChaineWriter As System.IO.StringWriter = New System.IO.StringWriter
            Dim RenduHtml As System.Web.UI.HtmlTextWriter
            RenduHtml = New System.Web.UI.HtmlTextWriter(ChaineWriter)
            Try
                Ctrl.RenderControl(RenduHtml)
            Catch ex As Exception
                Return ""
            End Try
            Return RenduHtml.InnerWriter.ToString
        End Function

        Public ReadOnly Property VirWebControle(ByVal CadreInfo As Control, ByVal Prefixe As String, ByVal Index As Integer) As Control
            Get
                Dim Cpt As Integer = -1
                Dim IndiceI As Integer
                Dim IndiceN1 As Integer
                Dim IndiceN2 As Integer
                Dim IndiceN3 As Integer
                Dim IndiceN4 As Integer
                Dim IndiceN5 As Integer
                Dim IndiceN6 As Integer
                Dim IndiceN7 As Integer
                Dim IndiceN8 As Integer
                Dim N0 As Control
                Dim N1 As Control
                Dim N2 As Control
                Dim N3 As Control
                Dim N4 As Control
                Dim N5 As Control
                Dim N6 As Control
                Dim N7 As Control
                Dim N8 As Control
                Dim Lg As Integer = Prefixe.Length


                For IndiceI = 0 To CadreInfo.Controls.Count - 1
                        N0 = CadreInfo.Controls.Item(IndiceI)
                        Select Case Strings.Left(N0.ID, Lg)
                            Case Is = Prefixe
                                Cpt += 1
                                If Cpt = Index Then
                                    Return N0
                                End If
                            Case Else
                                For IndiceN1 = 0 To N0.Controls.Count - 1
                                    N1 = N0.Controls.Item(IndiceN1)
                                    Select Case Strings.Left(N1.ID, Lg)
                                        Case Is = Prefixe
                                            Cpt += 1
                                            If Cpt = Index Then
                                                Return N1
                                            End If
                                        Case Else
                                            For IndiceN2 = 0 To N1.Controls.Count - 1
                                                N2 = N1.Controls.Item(IndiceN2)
                                                Select Case Strings.Left(N2.ID, Lg)
                                                    Case Is = Prefixe
                                                        Cpt += 1
                                                        If Cpt = Index Then
                                                            Return N2
                                                        End If
                                                    Case Else
                                                        For IndiceN3 = 0 To N2.Controls.Count - 1
                                                            N3 = N2.Controls.Item(IndiceN3)
                                                            Select Case Strings.Left(N3.ID, Lg)
                                                                Case Is = Prefixe
                                                                    Cpt += 1
                                                                    If Cpt = Index Then
                                                                        Return N3
                                                                    End If
                                                                Case Else
                                                                    For IndiceN4 = 0 To N3.Controls.Count - 1
                                                                        N4 = N3.Controls.Item(IndiceN4)
                                                                        Select Case Strings.Left(N4.ID, Lg)
                                                                            Case Is = Prefixe
                                                                                Cpt += 1
                                                                                If Cpt = Index Then
                                                                                    Return N4
                                                                                End If
                                                                            Case Else
                                                                                For IndiceN5 = 0 To N4.Controls.Count - 1
                                                                                    N5 = N4.Controls.Item(IndiceN5)
                                                                                    Select Case Strings.Left(N5.ID, Lg)
                                                                                        Case Is = Prefixe
                                                                                            Cpt += 1
                                                                                            If Cpt = Index Then
                                                                                                Return N5
                                                                                            End If
                                                                                        Case Else
                                                                                            For IndiceN6 = 0 To N5.Controls.Count - 1
                                                                                                N6 = N5.Controls.Item(IndiceN6)
                                                                                                Select Case Strings.Left(N6.ID, Lg)
                                                                                                    Case Is = Prefixe
                                                                                                        Cpt += 1
                                                                                                        If Cpt = Index Then
                                                                                                            Return N6
                                                                                                        End If
                                                                                                    Case Else
                                                                                                        For IndiceN7 = 0 To N6.Controls.Count - 1
                                                                                                            N7 = N6.Controls.Item(IndiceN7)
                                                                                                            Select Case Strings.Left(N7.ID, Lg)
                                                                                                                Case Is = Prefixe
                                                                                                                    Cpt += 1
                                                                                                                    If Cpt = Index Then
                                                                                                                        Return N7
                                                                                                                    End If
                                                                                                                Case Else
                                                                                                                    For IndiceN8 = 0 To N7.Controls.Count - 1
                                                                                                                        N8 = N7.Controls.Item(IndiceN8)
                                                                                                                        Select Case Strings.Left(N8.ID, Lg)
                                                                                                                            Case Is = Prefixe
                                                                                                                                Cpt += 1
                                                                                                                                If Cpt = Index Then
                                                                                                                                    Return N8
                                                                                                                                End If
                                                                                                                        End Select
                                                                                                                    Next IndiceN8
                                                                                                            End Select
                                                                                                        Next IndiceN7
                                                                                                End Select
                                                                                            Next IndiceN6
                                                                                    End Select
                                                                                Next IndiceN5
                                                                        End Select
                                                                    Next IndiceN4
                                                            End Select
                                                        Next IndiceN3
                                                End Select
                                            Next IndiceN2
                                    End Select
                                Next IndiceN1
                        End Select
                    Next IndiceI

                Return Nothing
            End Get
        End Property

        Public ReadOnly Property CouleurMaj() As System.Drawing.Color
            Get
                Return ConvertCouleur("DEFAD7")
            End Get
        End Property

        Public ReadOnly Property CouleurCharte(ByVal PointdeVue As Integer, ByVal NomCouleur As String) As System.Drawing.Color
            Get
                Dim NumCharte As Integer
                If WsCharte Is Nothing Then
                    WsCharte = New Virtualia.Systeme.Fonctions.CharteGraphique
                End If
                Select Case PointdeVue
                    Case 0, VI.PointdeVue.PVueApplicatif
                        NumCharte = 0
                    Case Else
                        NumCharte = 1
                End Select
                Return WsCharte.CouleurParSonNom(NumCharte, NomCouleur)
            End Get
        End Property

        Public Function ConvertCouleur(ByVal Valeur As String) As System.Drawing.Color
            Dim R As Integer
            Dim G As Integer
            Dim B As Integer
            Select Case Valeur.Length
                Case Is = 6
                    R = CInt(Val("&H" & Strings.Left(Valeur, 2) & "&"))
                    G = CInt(Val("&H" & Strings.Mid(Valeur, 3, 2) & "&"))
                    B = CInt(Val("&H" & Strings.Right(Valeur, 2) & "&"))
                    Return System.Drawing.Color.FromArgb(R, G, B)
                Case Is = 7
                    R = CInt(Val("&H" & Strings.Mid(Valeur, 2, 2) & "&"))
                    G = CInt(Val("&H" & Strings.Mid(Valeur, 4, 2) & "&"))
                    B = CInt(Val("&H" & Strings.Right(Valeur, 2) & "&"))
                    Return System.Drawing.Color.FromArgb(R, G, B)
                Case Else
                    Return Drawing.Color.White
            End Select
        End Function

        Public Function ConvertCouleur_Num(ByVal Valeur As System.Drawing.Color) As Integer
            Dim R As Integer = Valeur.R
            Dim G As Integer = Valeur.G
            Dim B As Integer = Valeur.B
            Return RGB(R, G, B)
        End Function

        Public Sub New(ByVal Host As Object, ByVal ControleSource As Integer)
            WsTypeControle = ControleSource

            Select Case WsTypeControle
                Case TypeControle.TypeUserControl
                    WsApplication = CType(Host, System.Web.UI.UserControl).Application
                    WsSession = CType(Host, System.Web.UI.UserControl).Session
                Case TypeControle.TypePage
                    WsApplication = CType(Host, System.Web.UI.Page).Application
                    WsSession = CType(Host, System.Web.UI.Page).Session
                Case TypeControle.TypeMasterPage
                    WsApplication = CType(Host, System.Web.UI.MasterPage).Application
                    WsSession = CType(Host, System.Web.UI.MasterPage).Session
            End Select

            Call InitialiserSession()
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
            WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates

        End Sub
    End Class
End Namespace