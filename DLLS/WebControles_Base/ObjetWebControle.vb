﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Controles
    Public Class ObjetWebControle
        Inherits System.Web.UI.UserControl
        Public Delegate Sub AppelTableEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs)
        Public Event AppelTable As AppelTableEventHandler
        Public Delegate Sub MessageEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
        Public Event MessageSaisie As MessageEventHandler
        Public Delegate Sub MsgDialog_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
        Public Event MessageDialogue As MsgDialog_MsgEventHandler
        '
        Private WebFct As Virtualia.Net.Controles.WebFonctions
        Private WsNomStateCache As String = "VControle"
        Private WsCtl_Cache As Virtualia.Net.VCaches.CacheWebControle
        '
        Private WsPvue As Integer = VI.PointdeVue.PVueApplicatif
        Private WsNumObjet As Integer = 0
        Private WsNomTable As String = ""
        Private WsSiReadonly As Boolean = False

        Private WsFiche As Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsSiListe As TypeListe = TypeListe.SiSimple
        Private WsLibelListe As List(Of String) = Nothing
        Private WsLibelColonne As List(Of String) = Nothing

        Public Enum TypeListe As Integer
            SiSimple = 0
            SiListeGrid = 1
            SiListeHisto = 2
        End Enum

        Protected Overridable Sub V_MessageSaisie(ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
            RaiseEvent MessageSaisie(Me, e)
        End Sub

        Public Sub V_MessageDialog(ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
            RaiseEvent MessageDialogue(Me, e)
        End Sub

        Protected Overridable Sub V_AppelTable(ByVal e As Virtualia.Systeme.Evenements.AppelTableEventArgs)
            RaiseEvent AppelTable(Me, e)
        End Sub

        Protected Overridable Sub V_DonTab_AppelTable(ByVal NumInfo As Integer, ByVal IDAppelant As String, ByVal ObjetAppelant As Integer, ByVal PvueInverse As Integer, ByVal Nomdelatable As String)
            Dim Evenement As Virtualia.Systeme.Evenements.AppelTableEventArgs
            Evenement = New Virtualia.Systeme.Evenements.AppelTableEventArgs(IDAppelant, ObjetAppelant, PvueInverse, Nomdelatable)
            V_AppelTable(Evenement)
        End Sub

        Public Property CacheVirControle As Virtualia.Net.VCaches.CacheWebControle
            Get
                If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                    Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.VCaches.CacheWebControle)
                End If
                Dim NewCache As Virtualia.Net.VCaches.CacheWebControle
                NewCache = New Virtualia.Net.VCaches.CacheWebControle
                Return NewCache
            End Get
            Set(value As Virtualia.Net.VCaches.CacheWebControle)
                If value Is Nothing Then
                    Exit Property
                End If
                If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                    Me.ViewState.Remove(WsNomStateCache)
                End If
                Me.ViewState.Add(WsNomStateCache, value)
            End Set
        End Property

        Public Property V_CacheMaj() As List(Of String)
            Get
                WsCtl_Cache = CacheVirControle
                If WsCtl_Cache.Liste_Datas Is Nothing And WsNumObjet > 0 Then
                    Dim Tableau As New List(Of String)
                    Dim I As Integer
                    For I = 0 To V_WebFonction.PointeurDicoObjet(WsPvue, WsNumObjet).LimiteMaximum
                        Tableau.Add("")
                    Next I
                    WsCtl_Cache.Liste_Datas = Tableau
                    CacheVirControle = WsCtl_Cache
                End If
                Return WsCtl_Cache.Liste_Datas
            End Get
            Set(ByVal value As List(Of String))
                WsCtl_Cache = CacheVirControle
                WsCtl_Cache.Liste_Datas = value
                If value Is Nothing And WsNumObjet > 0 Then
                    Dim Tableau As New List(Of String)
                    Dim I As Integer
                    For I = 0 To V_WebFonction.PointeurDicoObjet(WsPvue, WsNumObjet).LimiteMaximum
                        Tableau.Add("")
                    Next I
                    WsCtl_Cache.Liste_Datas = Tableau
                End If
                CacheVirControle = WsCtl_Cache
            End Set
        End Property

        Public Property V_CacheColHisto() As List(Of Integer)
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.TabColonnes
            End Get
            Set(ByVal value As List(Of Integer))
                WsCtl_Cache = CacheVirControle
                WsCtl_Cache.TabColonnes = value
                CacheVirControle = WsCtl_Cache
            End Set
        End Property

        Public Property V_CacheParticulier() As List(Of String)
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.Liste_Valeurs_Particulieres
            End Get
            Set(ByVal value As List(Of String))
                WsCtl_Cache = CacheVirControle
                WsCtl_Cache.Liste_Valeurs_Particulieres = value
                CacheVirControle = WsCtl_Cache
            End Set
        End Property

        Public ReadOnly Property V_ListeFiches(ByVal ArgumentDate As String, ByVal Rang As String,
                                               Optional ByVal SiDistinctDate As Boolean = False) As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Get
                WsCtl_Cache = CacheVirControle
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE) = ListeDesFiches()
                If LstFiches Is Nothing Then
                    Return Nothing
                End If
                If ArgumentDate = "" And Rang = "" Then
                    If SiDistinctDate = False Then
                        Return LstFiches
                    End If
                End If
                If ArgumentDate <> "" And Rang = "" Then
                    Return (From Fiche In LstFiches Where Fiche.Date_Valeur_ToDate = CDate(ArgumentDate) Order By Fiche.Clef Ascending).ToList
                End If
                If ArgumentDate <> "" And Rang <> "" Then
                    Return (From Fiche In LstFiches Where Fiche.Date_Valeur_ToDate = CDate(ArgumentDate) And Fiche.Clef = Rang).ToList
                End If
                '************ SiDistinctDate = true *************************
                If ArgumentDate = "" And Rang = "" Then
                    Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                    Dim LstTrie As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                    Dim IndiceF As Integer
                    LstTrie = (From Fiche In LstFiches Order By Fiche.Date_Valeur_ToDate Ascending, Fiche.Clef Ascending).ToList
                    LstRes = New List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                    LstRes.Add(LstTrie.Item(0))
                    For IndiceF = 0 To LstTrie.Count - 1
                        If LstTrie.Item(IndiceF).Date_de_Valeur <> LstRes.Item(LstRes.Count - 1).Date_de_Valeur Then
                            LstRes.Add(LstTrie.Item(IndiceF))
                        End If
                    Next IndiceF
                    Return LstRes
                End If
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property V_ListeFiches() As List(Of String)
            Get
                If WsNumObjet = 0 Then
                    Return Nothing
                End If
                WsCtl_Cache = CacheVirControle
                If V_CacheColHisto Is Nothing Then
                    Return Nothing
                End If
                If WsCtl_Cache.Index_Fiche = -1 Then
                    Return Nothing
                End If
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE) = ListeDesFiches()
                If LstFiches Is Nothing Then
                    Return Nothing
                End If

                Dim LstHisto As New List(Of String)
                Dim Tableaudata(0) As String
                Dim Separateur As String = VI.Tild
                Dim Chaine As String
                Dim IndiceK As Integer
                Dim IndexF As Integer = 0

                For Each Fiche In LstFiches
                    Tableaudata = Strings.Split(Fiche.ContenuTable, VI.Tild, -1)
                    If WsSiListe = TypeListe.SiListeHisto Then
                        Separateur = " --> "
                    End If
                    If WsCtl_Cache.TabColonnes(0) = 0 Then
                        Select Case V_WebFonction.PointeurDicoInfo(WsPvue, WsNumObjet, 0).Format
                            Case Is = VI.FormatDonnee.Annee, VI.FormatDonnee.FinAnnee
                                Chaine = Fiche.Date_Valeur_ToDate.Year & Separateur
                            Case Else
                                Chaine = Fiche.Date_de_Valeur & Separateur
                        End Select
                    Else
                        Chaine = Tableaudata(WsCtl_Cache.TabColonnes(0)) & Separateur
                    End If
                    If WsSiListe = TypeListe.SiListeHisto Then
                        Separateur = Strings.Space(2)
                    End If
                    For IndiceK = 1 To WsCtl_Cache.TabColonnes.Count - 1
                        If WsCtl_Cache.TabColonnes(IndiceK) = -1 Then
                            Exit For
                        End If
                        If WsCtl_Cache.TabColonnes(IndiceK) = 99 Then
                            Chaine &= Fiche.Date_de_Fin & Separateur
                        Else
                            Chaine &= Tableaudata(WsCtl_Cache.TabColonnes(IndiceK)) & Separateur
                        End If
                    Next IndiceK
                    If WsSiListe = TypeListe.SiListeGrid Then
                        If WsCtl_Cache.TabColonnes(0) = 0 Then
                            Select Case V_TypeObjet
                                Case VI.TypeObjet.ObjetDate
                                    If WsPvue = VI.PointdeVue.PVueFormation And WsNumObjet = 5 Then
                                        Chaine &= Fiche.Date_de_Valeur & Tableaudata(28).ToString 'Spécifique FOR_SESSION
                                    Else
                                        Chaine &= Fiche.Date_de_Valeur
                                    End If
                                Case Else
                                    Chaine &= Fiche.Date_de_Valeur & Tableaudata(1).ToString
                            End Select
                        Else
                            Chaine &= Tableaudata(WsCtl_Cache.TabColonnes(0))
                        End If
                    Else
                        Chaine &= IndexF.ToString
                    End If
                    LstHisto.Add(Chaine)
                    IndexF += 1
                Next

                Return LstHisto
            End Get
        End Property

        Public ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
            Get
                If WebFct Is Nothing Then
                    WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
                End If
                Return WebFct
            End Get
        End Property

        Public Property V_PointdeVue() As Integer
            Get
                Return WsPvue
            End Get
            Set(ByVal value As Integer)
                WsPvue = value
            End Set
        End Property

        Public Property V_Objet(ByVal SiListe As TypeListe) As Integer
            Get
                Return WsNumObjet
            End Get
            Set(ByVal value As Integer)
                WsNumObjet = value
                WsSiListe = SiListe
            End Set
        End Property

        Public Property V_NomTableSgbd() As String
            Get
                Return WsNomTable
            End Get
            Set(ByVal value As String)
                WsNomTable = value
            End Set
        End Property

        Public Property V_SiEnLectureSeule As Boolean
            Get
                Return WsSiReadonly
            End Get
            Set(value As Boolean)
                WsSiReadonly = value
            End Set
        End Property

        Public Property V_Fiche() As Virtualia.Systeme.MetaModele.VIR_FICHE
            Get
                WsCtl_Cache = CacheVirControle
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE) = ListeDesFiches()
                If LstFiches Is Nothing Then
                    Return Nothing
                End If
                Dim IFiche As Integer = WsCtl_Cache.Index_Fiche
                If IFiche >= 0 Then
                    WsFiche = LstFiches.Item(IFiche)
                Else
                    WsFiche = Nothing
                End If
                Return WsFiche
            End Get
            Set(ByVal value As Virtualia.Systeme.MetaModele.VIR_FICHE)
                WsFiche = value
                If value Is Nothing Then
                    Exit Property
                End If
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE) = ListeDesFiches()
                If LstFiches Is Nothing Then
                    Exit Property
                End If
                Try
                    V_IndexFiche = LstFiches.IndexOf(value)
                Catch ex As Exception
                    Exit Property
                End Try
            End Set
        End Property

        Public Property V_IndexFiche() As Integer
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.Index_Fiche
            End Get
            Set(value As Integer)
                WsCtl_Cache = CacheVirControle
                WsCtl_Cache.Index_Fiche = value
                CacheVirControle = WsCtl_Cache
            End Set
        End Property

        Public ReadOnly Property V_SiCreation() As Boolean
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.SiCreation
            End Get
        End Property

        Public Property V_Identifiant() As Integer
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.Ide_Dossier
            End Get
            Set(ByVal value As Integer)
                WsCtl_Cache = CacheVirControle
                If value = 0 Then
                    V_CacheMaj = Nothing
                    WsCtl_Cache.Ide_Dossier = 0
                    Exit Property
                End If
                If value = WsCtl_Cache.Ide_Dossier Then
                    Exit Property
                End If
                WsCtl_Cache.Ide_Dossier = value

                Dim DossierPER As Virtualia.Ressources.Datas.ObjetDossierPER = Nothing
                Dim DossierREF As Virtualia.Ressources.Datas.ObjetDossierREF = Nothing
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

                Select Case WsPvue
                    Case VI.PointdeVue.PVueApplicatif
                        DossierPER = V_WebFonction.PointeurDossier(WsCtl_Cache.Ide_Dossier)
                        If DossierPER Is Nothing Then
                            Exit Property
                        End If
                        WsCtl_Cache.Index_Fiche = DossierPER.V_UnObjetLu(V_WebFonction.PointeurUtilisateur.V_NomdUtilisateurSgbd, WsNomTable, WsNumObjet)
                    Case Else
                        DossierREF = V_WebFonction.PointeurIdeSysRef(WsPvue, WsCtl_Cache.Ide_Dossier)
                        If DossierREF Is Nothing Then
                            Exit Property
                        End If
                        WsCtl_Cache.Index_Fiche = DossierREF.V_UnObjetLu(V_WebFonction.PointeurUtilisateur.V_NomdUtilisateurSgbd, WsNomTable, WsNumObjet)
                End Select

                '** MettreEnCache le dossier et la Fiche lue **
                WsCtl_Cache.Point_de_Vue = WsPvue
                WsCtl_Cache.Numero_Objet = WsNumObjet
                WsCtl_Cache.SiCreation = False
                CacheVirControle = WsCtl_Cache

                Dim Tableaudata(0) As String

                If WsCtl_Cache.Index_Fiche = -1 Then
                    V_CacheMaj = Nothing
                    Exit Property
                End If
                WsCtl_Cache.Index_Fiche = 0
                CacheVirControle = WsCtl_Cache
                Select Case WsPvue
                    Case VI.PointdeVue.PVueApplicatif
                        LstFiches = DossierPER.V_ListeDesFiches(WsNumObjet)
                    Case Else
                        LstFiches = DossierREF.V_ListeDesFiches(WsNumObjet)
                End Select
                If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                    Exit Property
                End If
                If WsSiListe = TypeListe.SiSimple Then
                    V_CacheMaj = V_WebFonction.ViRhFonction.TransformeEnListe(LstFiches.Item(WsCtl_Cache.Index_Fiche))
                    Exit Property
                End If

                '*** SiListe = 1,2 ***********************************************************
                Dim IndexF As Integer = 0
                WsFiche = LstFiches.Item(IndexF)
                If WsFiche Is Nothing Then
                    Exit Property
                End If
                If V_CacheColHisto Is Nothing Then
                    Exit Property
                End If
                If WsSiListe = TypeListe.SiListeGrid Then
                    Tableaudata = Strings.Split(WsFiche.ContenuTable, VI.Tild, -1)
                    If WsCtl_Cache.TabColonnes(0) = 0 Then
                        Select Case V_TypeObjet
                            Case VI.TypeObjet.ObjetDate
                                If WsPvue = VI.PointdeVue.PVueFormation And WsNumObjet = 5 Then
                                    V_Occurence = WsFiche.Date_de_Valeur & Tableaudata(28) 'Spécifique FOR_SESSION
                                Else
                                    V_Occurence = WsFiche.Date_de_Valeur
                                End If
                            Case Else
                                V_Occurence = WsFiche.Date_de_Valeur & Tableaudata(1).ToString
                        End Select
                    Else
                        V_Occurence = Tableaudata(WsCtl_Cache.TabColonnes(0))
                    End If
                Else
                    V_Occurence = IndexF.ToString
                End If
            End Set
        End Property

        Public WriteOnly Property V_Occurence() As String
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                WsCtl_Cache = CacheVirControle
                If WsCtl_Cache.TabColonnes Is Nothing Then
                    Exit Property
                End If
                If WsCtl_Cache.Index_Fiche = -1 Then
                    Exit Property
                End If
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE) = ListeDesFiches()
                If LstFiches Is Nothing Then
                    Exit Property
                End If

                Dim IndexF As Integer = 0
                Dim Clef As String
                Dim Tableaudata(0) As String

                WsCtl_Cache.Index_Fiche = -1

                For Each Fiche In LstFiches
                    Tableaudata = Strings.Split(Fiche.ContenuTable, VI.Tild, -1)
                    If WsSiListe = TypeListe.SiListeGrid Then
                        If WsCtl_Cache.TabColonnes(0) = 0 Then
                            Select Case V_TypeObjet
                                Case VI.TypeObjet.ObjetDate
                                    If WsPvue = VI.PointdeVue.PVueFormation And WsNumObjet = 5 Then
                                        Clef = Fiche.Date_de_Valeur & Tableaudata(28).ToString 'Spécifique FOR_SESSION
                                    Else
                                        Clef = Fiche.Date_de_Valeur
                                    End If
                                Case Else
                                    Clef = Fiche.Date_de_Valeur & Tableaudata(1)
                            End Select
                        Else
                            Clef = Tableaudata(WsCtl_Cache.TabColonnes(0))
                        End If
                    Else
                        Clef = IndexF.ToString
                    End If
                    If Clef = value Then
                        WsCtl_Cache.Index_Fiche = IndexF
                        WsCtl_Cache.Liste_Datas = V_WebFonction.ViRhFonction.TransformeEnListe(Fiche)
                        Exit For
                    End If
                    IndexF += 1
                Next
                CacheVirControle = WsCtl_Cache
            End Set
        End Property

        Public Property V_LibelListe() As List(Of String)
            Get
                Return WsLibelListe
            End Get
            Set(ByVal value As List(Of String))
                WsLibelListe = value
            End Set
        End Property

        Public Property V_LibelColonne() As List(Of String)
            Get
                Return WsLibelColonne
            End Get
            Set(ByVal value As List(Of String))
                WsLibelColonne = value
            End Set
        End Property

        Public Overridable Sub V_MajFiche()
            If WsSiReadonly = True Then
                Exit Sub
            End If
            Dim ChaineLue As String = ""
            Dim IFiche As Integer

            WsCtl_Cache = CacheVirControle
            IFiche = WsCtl_Cache.Index_Fiche
            Select Case WsSiListe
                Case Is <> TypeListe.SiSimple
                    If V_CacheColHisto Is Nothing Then
                        Exit Sub
                    End If
            End Select

            Dim DossierPER As Virtualia.Ressources.Datas.ObjetDossierPER = Nothing
            Dim DossierREF As Virtualia.Ressources.Datas.ObjetDossierREF = Nothing
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

            Select Case WsPvue
                Case VI.PointdeVue.PVueApplicatif
                    If WsCtl_Cache.Ide_Dossier = 0 And WsNumObjet = 1 Then
                        Try
                            DossierPER = V_WebFonction.PointeurGlobal.NouveauDossier(WsCtl_Cache.Liste_Datas(2), WsCtl_Cache.Liste_Datas(3), WsCtl_Cache.Liste_Datas(4))
                            WsCtl_Cache.Ide_Dossier = DossierPER.V_Identifiant
                            WsCtl_Cache.SiCreation = True
                        Catch ex As Exception
                            Exit Sub
                        End Try
                        IFiche = -1
                    Else
                        DossierPER = V_WebFonction.PointeurDossier(WsCtl_Cache.Ide_Dossier)
                    End If
                Case Else
                    If WsCtl_Cache.Ide_Dossier = 0 And WsNumObjet = 1 Then
                        Try
                            WsCtl_Cache.Ide_Dossier = V_WebFonction.PointeurGlobal.NouvelIdentifiant(V_NomTableSgbd)
                            DossierREF = V_WebFonction.PointeurIdeSysRef(WsPvue, WsCtl_Cache.Ide_Dossier)
                            WsCtl_Cache.SiCreation = True
                        Catch ex As Exception
                            Exit Sub
                        End Try
                        IFiche = -1
                    Else
                        DossierREF = V_WebFonction.PointeurIdeSysRef(WsPvue, WsCtl_Cache.Ide_Dossier)
                    End If
            End Select

            If IFiche <> -1 Then
                Select Case WsPvue
                    Case VI.PointdeVue.PVueApplicatif
                        LstFiches = DossierPER.V_ListeDesFiches(WsNumObjet)
                    Case Else
                        LstFiches = DossierREF.V_ListeDesFiches(WsNumObjet)
                End Select
                If LstFiches IsNot Nothing AndAlso IFiche < LstFiches.Count Then
                    WsFiche = LstFiches.Item(IFiche)
                End If
                If WsFiche Is Nothing Then
                    Exit Sub
                End If
                ChaineLue = ""
                Select Case WsSiListe
                    Case TypeListe.SiListeGrid, TypeListe.SiListeHisto
                        If WsCtl_Cache.TabColonnes(0) = 0 Then
                            If WsFiche.Date_de_Valeur = "" Then
                                ChaineLue = ""
                            Else
                                ChaineLue = WsFiche.FicheLue
                            End If
                        ElseIf WsFiche.FicheLue <> "" Then
                            ChaineLue = WsFiche.FicheLue
                        End If
                    Case TypeListe.SiSimple
                        If WsFiche.FicheLue <> "" Then
                            ChaineLue = WsFiche.FicheLue
                        End If
                End Select
            Else
                Select Case WsPvue
                    Case VI.PointdeVue.PVueApplicatif
                        WsFiche = CreerFiche()
                        DossierPER.V_ListeDesFiches.Add(WsFiche)
                        WsCtl_Cache.Index_Fiche = DossierPER.V_NombredeFiches - 1
                    Case Else
                        WsFiche = CreerFiche()
                        DossierREF.V_ListeDesFiches.Add(WsFiche)
                        WsCtl_Cache.Index_Fiche = DossierREF.V_NombredeFiches - 1
                End Select
                ChaineLue = ""
            End If
            CacheVirControle = WsCtl_Cache
            V_RetourDialogueMaj("Oui") = ChaineLue
        End Sub

        Public WriteOnly Property V_RetourDialogueMaj(ByVal Cmd As String) As String
            Set(ByVal value As String)
                If WsSiReadonly = True Then
                    Exit Property
                End If
                If Cmd = "OK" Then               '** Retour OK de MajDossier
                    Exit Property
                End If
                If Cmd = "KO" And value = "" Then '** Retour Message bloquant Spécifique
                    Exit Property
                End If
                WsCtl_Cache = CacheVirControle
                If WsCtl_Cache.Liste_Datas Is Nothing Then
                    Exit Property
                End If

                Dim IFiche As Integer
                Dim MajOK As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
                Dim DossierPER As Virtualia.Ressources.Datas.ObjetDossierPER = Nothing
                Dim DossierREF As Virtualia.Ressources.Datas.ObjetDossierREF = Nothing
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim IInfoCer As Integer
                Dim DatasSignificatives As String = ""

                Select Case WsPvue
                    Case VI.PointdeVue.PVueApplicatif
                        DossierPER = V_WebFonction.PointeurDossier(WsCtl_Cache.Ide_Dossier)
                        If DossierPER Is Nothing Then
                            MajOK = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs(Me.ID, "KO", "Incident - La mise à jour n'a pas pu être effectuée")
                            V_MessageSaisie(MajOK)
                            Exit Property
                        End If
                    Case Else
                        DossierREF = V_WebFonction.PointeurIdeSysRef(WsPvue, WsCtl_Cache.Ide_Dossier)
                        If DossierREF Is Nothing Then
                            MajOK = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs(Me.ID, "KO", "Incident - La mise à jour n'a pas pu être effectuée")
                            V_MessageSaisie(MajOK)
                            Exit Property
                        End If
                End Select

                If WsCtl_Cache.SiCreation = True And WsNumObjet = 1 Then 'Cas de la création d'un nouveau dossier
                    'vérification de la validité du N° Ide attribué
                    If V_WebFonction.PointeurGlobal.NouvelIdentifiant(V_NomTableSgbd) <> WsCtl_Cache.Ide_Dossier Then
                        WsCtl_Cache.Ide_Dossier = 0
                        CacheVirControle = WsCtl_Cache
                        Call V_MajFiche()
                        Exit Property
                    End If
                End If

                Select Case WsSiListe
                    Case TypeListe.SiListeGrid, TypeListe.SiListeHisto
                        IFiche = WsCtl_Cache.Index_Fiche
                        If WsCtl_Cache.TabColonnes Is Nothing Then
                            Exit Property
                        End If
                    Case TypeListe.SiSimple
                        IFiche = WsCtl_Cache.Index_Fiche
                End Select
                If IFiche = -1 Then
                    Exit Property
                End If

                Select Case WsPvue
                    Case VI.PointdeVue.PVueApplicatif
                        LstFiches = DossierPER.V_ListeDesFiches(WsNumObjet)
                    Case Else
                        LstFiches = DossierREF.V_ListeDesFiches(WsNumObjet)
                End Select
                If LstFiches IsNot Nothing AndAlso IFiche < LstFiches.Count Then
                    WsFiche = LstFiches.Item(IFiche)
                End If
                If WsFiche Is Nothing Then
                    Exit Property
                End If
                IInfoCer = WsFiche.NumeroInfoCertification
                DatasSignificatives = WsFiche.V_Donnees_Significatives
                If value = "" Then
                    If Cmd = "Annuler" Then
                        Select Case WsPvue
                            Case VI.PointdeVue.PVueApplicatif
                                DossierPER.V_RemoveFiche(WsFiche)
                            Case Else
                                DossierREF.V_RemoveFiche(WsFiche)
                        End Select
                        If WsSiListe = TypeListe.SiListeGrid Then
                            If WsCtl_Cache.TabColonnes(0) = 0 Then
                                Select Case V_TypeObjet
                                    Case VI.TypeObjet.ObjetDate
                                        If WsPvue = VI.PointdeVue.PVueFormation And WsNumObjet = 5 Then
                                            V_Occurence = WsFiche.Date_de_Valeur & WsCtl_Cache.Liste_Datas(28) 'Spécifique FOR_SESSION
                                        Else
                                            V_Occurence = WsFiche.Date_de_Valeur
                                        End If
                                    Case Else
                                        V_Occurence = WsFiche.Date_de_Valeur & WsCtl_Cache.Liste_Datas(1)
                                End Select
                            Else
                                V_Occurence = WsCtl_Cache.Liste_Datas(WsCtl_Cache.TabColonnes(0))
                            End If
                        Else
                            V_Occurence = IFiche.ToString
                        End If
                        Exit Property
                    End If
                    If Cmd = "Non" Then
                        Exit Property
                    End If
                ElseIf Cmd = "Annuler" Then
                    WsCtl_Cache.Ide_Dossier = 0
                    If WsSiListe = TypeListe.SiListeGrid Then
                        If WsCtl_Cache.TabColonnes(0) = 0 Then
                            Select Case V_TypeObjet
                                Case VI.TypeObjet.ObjetDate
                                    If WsPvue = VI.PointdeVue.PVueFormation And WsNumObjet = 5 Then
                                        V_Occurence = WsFiche.Date_de_Valeur & WsCtl_Cache.Liste_Datas(28) 'Spécifique FOR_SESSION
                                    Else
                                        V_Occurence = WsFiche.Date_de_Valeur
                                    End If
                                Case Else
                                    V_Occurence = WsFiche.Date_de_Valeur & WsCtl_Cache.Liste_Datas(1)
                            End Select
                        Else
                            V_Occurence = WsCtl_Cache.Liste_Datas(WsCtl_Cache.TabColonnes(0))
                        End If
                    Else
                        V_Occurence = IFiche.ToString
                    End If
                    Exit Property
                End If

                Dim ChaineMaj As String = V_WebFonction.MajFiche(WsPvue, WsNumObjet, WsCtl_Cache.Ide_Dossier, WsCtl_Cache.Liste_Datas, value, DatasSignificatives, IInfoCer)

                If ChaineMaj = "" Then
                    MajOK = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs(Me.ID, "KO", "Incident - La mise à jour n'a pas été effectuée")
                    V_MessageSaisie(MajOK)
                    Exit Property
                End If

                WsFiche.ContenuTable = ChaineMaj
                If WsCtl_Cache.SiCreation = True And WsPvue = VI.PointdeVue.PVueApplicatif Then
                    V_WebFonction.PointeurUtilisateur.PointeurArmoirePER = Nothing
                End If

                Select Case WsSiListe
                    Case TypeListe.SiListeGrid, TypeListe.SiListeHisto
                        If value = "" Then
                            Select Case WsPvue
                                Case VI.PointdeVue.PVueApplicatif
                                    DossierPER.V_ActualiserUnObjet(V_WebFonction.PointeurUtilisateur.V_NomdUtilisateurSgbd, WsNomTable) = WsNumObjet
                                Case Else
                                    DossierREF.V_ActualiserUnObjet(V_WebFonction.PointeurUtilisateur.V_NomdUtilisateurSgbd, WsNomTable) = WsNumObjet
                            End Select
                        End If
                        If WsSiListe = TypeListe.SiListeGrid Then
                            If WsCtl_Cache.TabColonnes(0) = 0 Then
                                Select Case V_TypeObjet
                                    Case VI.TypeObjet.ObjetDate
                                        If WsPvue = VI.PointdeVue.PVueFormation And WsNumObjet = 5 Then
                                            V_Occurence = WsFiche.Date_de_Valeur & WsCtl_Cache.Liste_Datas(28) 'Spécifique FOR_SESSION
                                        Else
                                            V_Occurence = WsFiche.Date_de_Valeur
                                        End If
                                    Case Else
                                        V_Occurence = WsFiche.Date_de_Valeur & WsCtl_Cache.Liste_Datas(1)
                                End Select
                            Else
                                V_Occurence = WsCtl_Cache.Liste_Datas(WsCtl_Cache.TabColonnes(0))
                            End If
                        Else
                            V_Occurence = IFiche.ToString
                        End If
                End Select

                MajOK = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs(Me.ID, WsNumObjet,
                                             WsCtl_Cache.Ide_Dossier.ToString, "OK", "Mise à jour effectuée", "")

                Call V_WebFonction.PointeurUtilisateur.Actualiser(WsPvue, WsNumObjet, WsCtl_Cache.Ide_Dossier)

                V_MessageSaisie(MajOK)
            End Set
        End Property

        Public WriteOnly Property V_RetourDialogueSupp(ByVal Cmd As String) As String
            Set(ByVal value As String)
                If WsSiReadonly = True Then
                    Exit Property
                End If
                If Cmd = "Non" Then
                    Exit Property
                End If
                WsCtl_Cache = CacheVirControle
                If WsCtl_Cache.Liste_Datas Is Nothing Then
                    Exit Property
                End If

                Dim ChaineLue As String
                Dim DossierPER As Virtualia.Ressources.Datas.ObjetDossierPER = Nothing
                Dim DossierREF As Virtualia.Ressources.Datas.ObjetDossierREF = Nothing
                Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

                Select Case WsPvue
                    Case VI.PointdeVue.PVueApplicatif
                        DossierPER = V_WebFonction.PointeurDossier(WsCtl_Cache.Ide_Dossier)
                    Case Else
                        DossierREF = V_WebFonction.PointeurIdeSysRef(WsPvue, WsCtl_Cache.Ide_Dossier)
                End Select
                Dim IFiche As Integer = WsCtl_Cache.Index_Fiche
                If IFiche <> -1 Then
                    Select Case WsPvue
                        Case VI.PointdeVue.PVueApplicatif
                            LstFiches = DossierPER.V_ListeDesFiches(WsNumObjet)
                        Case Else
                            LstFiches = DossierREF.V_ListeDesFiches(WsNumObjet)
                    End Select
                    If LstFiches IsNot Nothing AndAlso IFiche < LstFiches.Count Then
                        WsFiche = LstFiches.Item(IFiche)
                        ChaineLue = WsFiche.FicheLue
                    Else
                        Exit Property
                    End If
                    Select Case WsPvue
                        Case VI.PointdeVue.PVueApplicatif
                            If DossierPER.V_SupprimerFiche(V_WebFonction.PointeurUtilisateur.V_NomdUtilisateurSgbd,
                                                           V_WebFonction.PointeurUtilisateur.V_NomdeConnexion,
                                                           WsNumObjet, ChaineLue) = True Then
                                DossierPER.V_RemoveFiche(WsFiche)

                                V_CacheMaj = Nothing

                                Dim SuppOK As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
                                SuppOK = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs(Me.ID, "OK", "Suppression effectuée")
                                V_MessageSaisie(SuppOK)
                            End If
                        Case Else
                            If DossierREF.V_SupprimerFiche(V_WebFonction.PointeurUtilisateur.V_NomdUtilisateurSgbd,
                                                           V_WebFonction.PointeurUtilisateur.V_NomdeConnexion,
                                                           WsNumObjet, ChaineLue) = True Then
                                DossierREF.V_RemoveFiche(WsFiche)

                                V_CacheMaj = Nothing

                                Dim SuppOK As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
                                SuppOK = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs(Me.ID, "OK", "Suppression effectuée")
                                V_MessageSaisie(SuppOK)
                            End If
                    End Select
                    Call V_WebFonction.PointeurUtilisateur.Actualiser(WsPvue, WsNumObjet, WsCtl_Cache.Ide_Dossier)
                End If
            End Set
        End Property

        Public Sub V_CommandeNewDossier(ByVal PtdeVue As Integer)
            If WsSiReadonly = True Then
                Exit Sub
            End If
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Point_de_Vue = PtdeVue
            WsCtl_Cache.Numero_Objet = 1
            WsCtl_Cache.Ide_Dossier = 0
            WsCtl_Cache.Index_Fiche = -1
            ''**** AKR
            'WsCtl_Cache.Liste_Datas = Nothing
            CacheVirControle = WsCtl_Cache
        End Sub

        Public Sub V_CommandeNewFiche()
            If WsSiReadonly = True Then
                Exit Sub
            End If
            Dim DossierPER As Virtualia.Ressources.Datas.ObjetDossierPER = Nothing
            Dim DossierREF As Virtualia.Ressources.Datas.ObjetDossierREF = Nothing

            WsCtl_Cache = CacheVirControle
            Select Case WsPvue
                Case VI.PointdeVue.PVueApplicatif
                    Try
                        DossierPER = V_WebFonction.PointeurDossier(WsCtl_Cache.Ide_Dossier)
                    Catch ex As Exception
                        Exit Sub
                    End Try
                    If DossierPER Is Nothing Then
                        Exit Sub
                    End If
                    WsFiche = CreerFiche()
                    DossierPER.V_ListeDesFiches.Add(WsFiche)
                    V_CacheMaj = Nothing
                    WsCtl_Cache.Index_Fiche = DossierPER.V_ListeDesFiches(WsNumObjet).Count - 1
                Case Else
                    Try
                        DossierREF = V_WebFonction.PointeurIdeSysRef(WsPvue, WsCtl_Cache.Ide_Dossier)
                    Catch ex As Exception
                        Exit Sub
                    End Try
                    If DossierREF Is Nothing Then
                        Exit Sub
                    End If
                    WsFiche = CreerFiche()
                    DossierREF.V_ListeDesFiches.Add(WsFiche)
                    V_CacheMaj = Nothing
                    WsCtl_Cache.Index_Fiche = DossierREF.V_ListeDesFiches(WsNumObjet).Count - 1
            End Select
            CacheVirControle = WsCtl_Cache
        End Sub

        Public Sub V_CommandeSuppFiche()
            If WsSiReadonly = True Then
                Exit Sub
            End If
            If V_CacheColHisto Is Nothing Then
                Exit Sub
            End If
            WsCtl_Cache = CacheVirControle
            Dim Tableaudata(0) As String
            Dim ChaineLibel As String
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE) = ListeDesFiches()
            If LstFiches IsNot Nothing AndAlso WsCtl_Cache.Index_Fiche < LstFiches.Count Then
                WsFiche = LstFiches.Item(WsCtl_Cache.Index_Fiche)
            Else
                Exit Sub
            End If
            Tableaudata = Strings.Split(WsFiche.ContenuTable, VI.Tild, -1)
            If WsCtl_Cache.TabColonnes(0) = 0 Then
                ChaineLibel = Tableaudata(1) & " du " & WsFiche.Date_de_Valeur
                If WsFiche.Date_de_Fin <> "" Then
                    ChaineLibel &= " au " & WsFiche.Date_de_Fin
                End If
            Else
                ChaineLibel = Tableaudata(1)
            End If

            Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs = V_WebFonction.ConfirmerSuppressionFiche(WsNumObjet, ChaineLibel)
            V_MessageDialog(Evenement)
        End Sub

        Public Sub V_ValeurChange(ByVal NumInfo As Integer, ByVal Valeur As String, ByVal ITabul As Integer)
            If WsSiReadonly = True Then
                Exit Sub
            End If
            WsCtl_Cache = CacheVirControle
            If WsCtl_Cache.Liste_Datas Is Nothing Then
                Exit Sub
            End If
            'If WsCtl_Cache.Liste_Datas(NumInfo) IsNot Nothing Then
            'WsCtl_Cache.Liste_Datas(NumInfo) = Valeur.Trim
            'CacheVirControle = WsCtl_Cache
            'End If
        End Sub

        Public Sub V_ValeurChange(ByVal NumInfo As Integer, ByVal Valeur As String)
            If WsSiReadonly = True Then
                Exit Sub
            End If
            WsCtl_Cache = CacheVirControle
            If WsCtl_Cache.Liste_Datas Is Nothing Then
                Exit Sub
            End If
            If WsCtl_Cache.Liste_Datas(NumInfo) IsNot Nothing Then
                WsCtl_Cache.Liste_Datas(NumInfo) = Valeur.Trim
                CacheVirControle = WsCtl_Cache
            End If
        End Sub

        Public Sub V_ActualiserObjet(ByVal Ide As Integer)
            Dim DossierPER As Virtualia.Ressources.Datas.ObjetDossierPER = Nothing
            Dim DossierREF As Virtualia.Ressources.Datas.ObjetDossierREF = Nothing
            Select Case WsPvue
                Case VI.PointdeVue.PVueApplicatif
                    DossierPER = V_WebFonction.PointeurDossier(Ide)
                    If DossierPER Is Nothing Then
                        Exit Sub
                    End If
                Case Else
                    DossierREF = V_WebFonction.PointeurIdeSysRef(WsPvue, Ide)
                    If DossierREF Is Nothing Then
                        Exit Sub
                    End If
            End Select
            Select Case WsPvue
                Case VI.PointdeVue.PVueApplicatif
                    DossierPER.V_ActualiserUnObjet(V_WebFonction.PointeurUtilisateur.V_NomdUtilisateurSgbd, WsNomTable) = WsNumObjet
                Case Else
                    DossierREF.V_ActualiserUnObjet(V_WebFonction.PointeurUtilisateur.V_NomdUtilisateurSgbd, WsNomTable) = WsNumObjet
            End Select
        End Sub

        Public ReadOnly Property V_TypeObjet As Integer
            Get
                If V_WebFonction.PointeurDicoObjet(WsPvue, WsNumObjet) IsNot Nothing Then
                    Return V_WebFonction.PointeurDicoObjet(WsPvue, WsNumObjet).VNature
                End If
                Return VI.TypeObjet.ObjetDate
            End Get
        End Property
        Private Function ListeDesFiches() As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim DossierPER As Virtualia.Ressources.Datas.ObjetDossierPER = Nothing
            Dim DossierREF As Virtualia.Ressources.Datas.ObjetDossierREF = Nothing
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

            Select Case WsPvue
                Case VI.PointdeVue.PVueApplicatif
                    DossierPER = V_WebFonction.PointeurDossier(WsCtl_Cache.Ide_Dossier)
                    If DossierPER Is Nothing Then
                        Return Nothing
                    End If
                    LstFiches = DossierPER.V_ListeDesFiches(WsNumObjet)
                Case Else
                    DossierREF = V_WebFonction.PointeurIdeSysRef(WsPvue, WsCtl_Cache.Ide_Dossier)
                    If DossierREF Is Nothing Then
                        Return Nothing
                    End If
                    LstFiches = DossierREF.V_ListeDesFiches(WsNumObjet)
            End Select
            If LstFiches Is Nothing Then
                Return Nothing
            End If
            If LstFiches.Count = 0 Then
                Return Nothing
            End If
            Return LstFiches
        End Function

        Private Function CreerFiche() As Virtualia.Systeme.MetaModele.VIR_FICHE
            Dim IConstructeur As Virtualia.Systeme.IVConstructeur
            Select Case WsPvue
                Case VI.PointdeVue.PVueApplicatif
                    IConstructeur = New Virtualia.TablesObjet.ShemaPER.VConstructeur(V_WebFonction.PointeurGlobal.VirModele.InstanceProduit.ClefModele)
                Case Else
                    IConstructeur = New Virtualia.TablesObjet.ShemaREF.VConstructeur(V_WebFonction.PointeurGlobal.VirModele.InstanceProduit.ClefModele, WsPvue)
            End Select
            Return IConstructeur.V_NouvelleFiche(WsNumObjet, WsCtl_Cache.Ide_Dossier)
        End Function

        Protected Overridable Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            WsLibelListe = Nothing
        End Sub

        Protected Overridable Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
            WsLibelListe = Nothing
        End Sub

    End Class
End Namespace