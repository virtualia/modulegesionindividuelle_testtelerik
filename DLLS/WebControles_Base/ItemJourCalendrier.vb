﻿Option Explicit On
Option Strict Off
Option Compare Text
Namespace Controles
    Public Class ItemJourCalendrier
        Private WsDateValeur As String
        Private WsIntitule As String
        Private WsComplement As String
        Private WsCaracteristique As Integer 'Cf VI.NumeroPlage
        Private WsObjetOrigine As Integer
        Public Property Date_Valeur As String
            Get
                Return WsDateValeur
            End Get
            Set(value As String)
                WsDateValeur = value
            End Set
        End Property

        Public ReadOnly Property Date_Valeur_ToDate As Date
            Get
                Return CDate(WsDateValeur)
            End Get
        End Property

        Public Property Intitule As String
            Get
                Return WsIntitule
            End Get
            Set(value As String)
                WsIntitule = value
            End Set
        End Property

        Public Property LibelleComplementaire As String
            Get
                Return WsComplement
            End Get
            Set(value As String)
                WsComplement = value
            End Set
        End Property

        Public Property Caracteristique As Integer
            Get
                Return WsCaracteristique
            End Get
            Set(value As Integer)
                WsCaracteristique = value
            End Set
        End Property

        Public Property ObjetOrigine As Integer
            Get
                Return WsObjetOrigine
            End Get
            Set(value As Integer)
                WsObjetOrigine = value
            End Set
        End Property

        Public Sub New(ByVal DateEffet As String, ByVal Nature As String, ByVal Complement As String, ByVal TypeJour As Integer, ByVal NumObjet As Integer)
            Date_Valeur = DateEffet
            Intitule = Nature
            LibelleComplementaire = Complement
            Caracteristique = TypeJour
            ObjetOrigine = NumObjet
        End Sub
    End Class
End Namespace