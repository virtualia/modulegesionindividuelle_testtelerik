﻿Option Explicit On
Option Strict Off
Option Compare Text
Namespace Controles
    Public Class ItemMenuCommun
        Private WsGroupeMenu As String = ""
        Private WsIntituleMenu As String = ""
        Private WsPtdeVue As Integer
        Private WsNumeroObjet As Integer
        Private WsIndexVue As Integer
        Private WsIDVue As String = ""

        Public Property Groupe As String
            Get
                Return WsGroupeMenu
            End Get
            Set(value As String)
                WsGroupeMenu = value
            End Set
        End Property

        Public Property Intitule As String
            Get
                Return WsIntituleMenu
            End Get
            Set(value As String)
                WsIntituleMenu = value
            End Set
        End Property

        Public Property Point_de_Vue As Integer
            Get
                Return WsPtdeVue
            End Get
            Set(value As Integer)
                WsPtdeVue = value
            End Set
        End Property

        Public Property Numero_Objet As Integer
            Get
                Return WsNumeroObjet
            End Get
            Set(value As Integer)
                WsNumeroObjet = value
            End Set
        End Property

        Public Property Index_Vue As Integer
            Get
                Return WsIndexVue
            End Get
            Set(value As Integer)
                WsIndexVue = value
            End Set
        End Property

        Public Property ID_Vue As String
            Get
                Return WsIDVue
            End Get
            Set(value As String)
                WsIDVue = value
            End Set
        End Property

    End Class
End Namespace