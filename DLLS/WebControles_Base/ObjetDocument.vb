﻿Option Explicit On
Option Strict Off
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Session
    Public Class ObjetDocument
        Private WsParent As ObjetGlobalBase
        Private WsModuleOrigine As String
        Private WsIde As Integer
        Private WsObjetOrigine As Integer
        Private WsListeDocuments As List(Of Virtualia.TablesObjet.ShemaPER.PER_DOCUMENTS) = Nothing
        Private WsNomRepertoire As String = ""
        Private WsPatronyme As String = ""

        Public ReadOnly Property NomCoffreVirtuel As String
            Get
                Return WsNomRepertoire
            End Get
        End Property

        Public Function StockerNewDocument(ByVal Octets As Byte(), ByVal Suffixe As String, ByVal Commentaires As String, ByVal CerTransaction As String) As Boolean
            Dim ChronoBase As String
            Dim NomFichierStocke As String
            Dim SiOK As Boolean
            Dim FicheDOC As Virtualia.TablesObjet.ShemaPER.PER_DOCUMENTS
            Dim RandomizerAlea As System.Random

            If WsNomRepertoire = "" Then
                Return False
            End If
            If Suffixe = "" Then
                Suffixe = "unk"
            End If
            RandomizerAlea = New System.Random
            ChronoBase = "V" & String.Format(WsObjetOrigine, "000") & "_" & RandomizerAlea.Next(1000000)
            Do
                Try
                    NomFichierStocke = WsParent.VirRhFonction.ChaineCryptee(ChronoBase & "_" & CStr(WsIde), WsParent.ClefModeleCryptage) & VI.PointFinal & Suffixe
                Catch ex As Exception
                    Return False
                End Try
                If My.Computer.FileSystem.FileExists(WsNomRepertoire & "\" & NomFichierStocke) = False Then
                    Exit Do
                End If
                ChronoBase = "V" & String.Format(WsObjetOrigine, "000") & "_" & RandomizerAlea.Next(1000000)
            Loop
            Try
                My.Computer.FileSystem.WriteAllBytes(WsNomRepertoire & "\" & NomFichierStocke, Octets, False)
            Catch ex As Exception
                Return False
            End Try
            FicheDOC = New Virtualia.TablesObjet.ShemaPER.PER_DOCUMENTS
            FicheDOC.Ide_Dossier = WsIde
            FicheDOC.Chrono = ChronoBase
            FicheDOC.SiVisibleParIntranet = True
            FicheDOC.Datation = WsParent.VirRhDates.DateduJour
            '** NomFichier crypté = (FicheDOC.Chrono & "_" & CStr(WsIde)).Suffixe
            '** NomFichier non crypté = ChronoBase & "_" & WsPatronyme & "." & Suffixe pour rendu public 
            FicheDOC.NomFichier = ChronoBase & "_" & WsPatronyme & VI.PointFinal & Suffixe 'Equivalent à Reference
            '**
            FicheDOC.Classement = WsModuleOrigine
            FicheDOC.Observations = Commentaires
            FicheDOC.SiVisibleParIntranet = True
            FicheDOC.Certification = CerTransaction

            SiOK = WsParent.VirServiceServeur.MiseAjour_Fiche(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaDocuments, WsIde,
                                                              "C", "", FicheDOC.ContenuTable)

            Return SiOK
        End Function
        Public Function MiseAjourDocument(ByVal Octets As Byte(), ByVal RangChrono As String, ByVal Commentaires As String, ByVal CerTransaction As String) As Boolean
            Dim NomFichierStocke As String
            Dim Suffixe As String = ""
            Dim SiOK As Boolean
            Dim FicheDOC As Virtualia.TablesObjet.ShemaPER.PER_DOCUMENTS
            Dim TableauData(0) As String

            If WsNomRepertoire = "" Then
                Return False
            End If
            FicheDOC = FicheDocument(RangChrono)
            If FicheDOC Is Nothing Then
                Return False
            End If
            TableauData = Strings.Split(FicheDOC.NomFichier, VI.PointFinal, -1)
            If TableauData.Count < 2 Then
                Return False
            End If
            Suffixe = TableauData(TableauData.Count - 1)
            NomFichierStocke = WsParent.VirRhFonction.ChaineCryptee(RangChrono & "_" & CStr(WsIde), WsParent.ClefModeleCryptage) & VI.PointFinal & Suffixe
            Try
                My.Computer.FileSystem.WriteAllBytes(WsNomRepertoire & "\" & NomFichierStocke, Octets, False)
            Catch ex As Exception
                Return False
            End Try
            FicheDOC.Datation = WsParent.VirRhDates.DateduJour
            '** NomFichier crypté = (FicheDOC.Chrono & "_" & CStr(WsIde)).Suffixe
            '** NomFichier non crypté = ChronoBase & "_" & WsPatronyme & "." & Suffixe pour rendu public 
            '**
            FicheDOC.Classement = WsModuleOrigine
            FicheDOC.Observations = Commentaires

            SiOK = WsParent.VirServiceServeur.MiseAjour_Fiche(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaDocuments, WsIde,
                                                              "M", FicheDOC.FicheLue, FicheDOC.ContenuTable)

            Return SiOK
        End Function

        Public Function LireDocument(ByVal RangChrono As String) As Byte()
            Dim NomFichierStocke As String
            Dim Suffixe As String = ""
            Dim FicheDOC As Virtualia.TablesObjet.ShemaPER.PER_DOCUMENTS
            Dim TableauData(0) As String

            If WsNomRepertoire = "" Then
                Return Nothing
            End If
            FicheDOC = FicheDocument(RangChrono)
            If FicheDOC Is Nothing Then
                Return Nothing
            End If
            TableauData = Strings.Split(FicheDOC.NomFichier, VI.PointFinal, -1)
            If TableauData.Count < 2 Then
                Return Nothing
            End If
            Suffixe = TableauData(TableauData.Count - 1)
            NomFichierStocke = WsParent.VirRhFonction.ChaineCryptee(RangChrono & "_" & CStr(WsIde), WsParent.ClefModeleCryptage) & VI.PointFinal & Suffixe
            If My.Computer.FileSystem.FileExists(WsNomRepertoire & "\" & NomFichierStocke) = False Then
                Return Nothing
            End If
            Return My.Computer.FileSystem.ReadAllBytes(WsNomRepertoire & "\" & NomFichierStocke)
        End Function

        Public ReadOnly Property FicheDocument(ByVal ChronoFichier As String) As Virtualia.TablesObjet.ShemaPER.PER_DOCUMENTS
            Get
                Dim LstFichesDOC As List(Of Virtualia.TablesObjet.ShemaPER.PER_DOCUMENTS) = ListeDocuments
                If LstFichesDOC Is Nothing OrElse LstFichesDOC.Count = 0 Then
                    Return Nothing
                End If
                For Each PerDoc In LstFichesDOC
                    If PerDoc.Chrono = ChronoFichier Then
                        Return PerDoc
                    End If
                Next
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property ListeDocuments() As List(Of Virtualia.TablesObjet.ShemaPER.PER_DOCUMENTS)
            Get
                If WsListeDocuments Is Nothing Then
                    Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                    LstFiches = WsParent.VirServiceServeur.LectureObjet_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaDocuments, WsIde, False)
                    If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                        Return Nothing
                    End If
                    WsListeDocuments = WsParent.VirRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_DOCUMENTS)(LstFiches)
                End If
                Try
                    Return (From Element In WsListeDocuments Order By Element.Datation).ToList
                Catch ex As Exception
                    Return Nothing
                End Try
            End Get
        End Property

        Public Sub New(ByVal Host As ObjetGlobalBase, ByVal ModuleOrigine As String, ByVal Ide As Integer, ByVal ObjetOrigine As Integer)
            Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim NomDossier As String = CStr(Ide)

            WsParent = Host
            WsModuleOrigine = ModuleOrigine
            WsIde = Ide
            WsObjetOrigine = ObjetOrigine

            WsNomRepertoire = System.Configuration.ConfigurationManager.AppSettings("CoffreDocuments")
            If WsNomRepertoire = "" Then
                WsNomRepertoire = WsParent.VirRepertoire & "\CoffreVirtualia"
            End If
            If My.Computer.FileSystem.DirectoryExists(WsNomRepertoire) = False Then
                My.Computer.FileSystem.CreateDirectory(WsNomRepertoire)
            End If

            LstRes = WsParent.VirServiceServeur.LectureObjet_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaCivil, Ide, False)
            If LstRes IsNot Nothing AndAlso LstRes.Count > 0 Then
                WsPatronyme = CType(LstRes.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL).V_Patronyme
                NomDossier = WsPatronyme & "_" & CStr(Ide)
            End If
            Try
                WsNomRepertoire &= "\" & WsParent.VirRhFonction.ChaineCryptee(NomDossier, WsParent.ClefModeleCryptage)
                If My.Computer.FileSystem.DirectoryExists(WsNomRepertoire) = False Then
                    My.Computer.FileSystem.CreateDirectory(WsNomRepertoire)
                End If
            Catch ex As Exception
                WsNomRepertoire = ""
            End Try
        End Sub
    End Class
End Namespace
