﻿Option Explicit On
Option Strict Off
Option Compare Text
Imports Microsoft.VisualBasic
Namespace VCaches
    <Serializable>
    Public Class CacheSaisieAbsence
        Private WsIdentifiant As Integer
        Private WsDateDebut As String = ""
        Private WsDateFin As String = ""
        Private WsCocheDebut As String = ""
        Private WsCocheFin As String = ""
        Private WsSelRadio As Integer = 0 '0 = Sur plusieurs Jours, 1 = Sur Un seul jour
        Private WsSelAbs As String = "Congés annuels"
        Private WsClef As String = ""

        Public Property Ide_Dossier As Integer
            Get
                Return WsIdentifiant
            End Get
            Set(value As Integer)
                WsIdentifiant = value
            End Set
        End Property

        Public Property Date_Debut As String
            Get
                Return WsDateDebut
            End Get
            Set(value As String)
                WsDateDebut = value
            End Set
        End Property

        Public Property Date_Fin As String
            Get
                Return WsDateFin
            End Get
            Set(value As String)
                WsDateFin = value
            End Set
        End Property

        Public Property Coche_Debut As String
            Get
                Return WsCocheDebut
            End Get
            Set(value As String)
                WsCocheDebut = value
            End Set
        End Property

        Public Property Coche_Fin As String
            Get
                Return WsCocheFin
            End Get
            Set(value As String)
                WsCocheFin = value
            End Set
        End Property

        Public Property SiSurUnJour As Boolean
            Get
                If WsSelRadio = 1 Then
                    Return True
                Else
                    Return False
                End If
            End Get
            Set(value As Boolean)
                If value = True Then
                    WsSelRadio = 1
                Else
                    WsSelRadio = 0
                End If
            End Set
        End Property

        Public Property NatureAbsence As String
            Get
                Return WsSelAbs
            End Get
            Set(value As String)
                WsSelAbs = value
            End Set
        End Property

        Public Property Clef As String
            Get
                Return WsClef
            End Get
            Set(value As String)
                WsClef = value
            End Set
        End Property

        Public Sub Initialiser()
            WsDateDebut = ""
            WsDateFin = ""
            WsCocheDebut = ""
            WsCocheFin = ""
        End Sub
    End Class
End Namespace
