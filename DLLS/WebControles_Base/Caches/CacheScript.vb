﻿Option Explicit On
Option Strict Off
Option Compare Text
Namespace VCaches
    <Serializable>
    Public Class CacheScript
        Private WsPointdeVue As Integer
        Private WsTypeOutil As Integer
        Private WsIdentifiant As Integer
        Private WsIntitule As String = ""
        Private WsOpeLogique As Virtualia.Systeme.Constantes.Operateurs = Virtualia.Systeme.Constantes.Operateurs.ET
        Private WsExeDateDebut As String = ""
        Private WsExeDateFin As String = ""
        Private WsSiHisto As Boolean = False
        Private WsSiScriptSql As Boolean
        Private WsListeLignes As List(Of CacheLigneScript) = Nothing

        Public Property PtdeVue As Integer
            Get
                Return WsPointdeVue
            End Get
            Set(value As Integer)
                WsPointdeVue = value
            End Set
        End Property

        Public Property Ide_Dossier As Integer
            Get
                Return WsIdentifiant
            End Get
            Set(value As Integer)
                WsIdentifiant = value
            End Set
        End Property

        Public Property TypeOutil As Integer
            Get
                Return WsTypeOutil
            End Get
            Set(value As Integer)
                WsTypeOutil = value
            End Set
        End Property

        Public Property Intitule As String
            Get
                Return WsIntitule
            End Get
            Set(value As String)
                WsIntitule = value
            End Set
        End Property

        Public Property OperateurLogique As Virtualia.Systeme.Constantes.Operateurs
            Get
                Return WsOpeLogique
            End Get
            Set(value As Virtualia.Systeme.Constantes.Operateurs)
                WsOpeLogique = value
            End Set
        End Property

        Public Property ExeDate_Debut As String
            Get
                Return WsExeDateDebut
            End Get
            Set(value As String)
                WsExeDateDebut = value
            End Set
        End Property

        Public Property ExeDate_Fin As String
            Get
                Return WsExeDateFin
            End Get
            Set(value As String)
                WsExeDateFin = value
            End Set
        End Property

        Public Property SiHistorique As Boolean
            Get
                Return WsSiHisto
            End Get
            Set(value As Boolean)
                WsSiHisto = value
            End Set
        End Property

        Public ReadOnly Property ListeLignes As List(Of CacheLigneScript)
            Get
                If WsListeLignes Is Nothing OrElse WsListeLignes.Count = 0 Then
                    Return Nothing
                End If
                Dim IndiceI As Integer = 0
                Dim LstTrie As List(Of CacheLigneScript) = (From Ligne In WsListeLignes Order By Ligne.NumeroLigne).ToList
                WsListeLignes = LstTrie
                For Each Ligne In WsListeLignes
                    IndiceI += 1
                    Ligne.NumeroLigne = IndiceI
                Next
                Return WsListeLignes
            End Get
        End Property

        Public Sub AjouterUneligne(ByVal Ligne As CacheLigneScript)
            If WsListeLignes Is Nothing Then
                WsListeLignes = New List(Of CacheLigneScript)
            End If
            WsListeLignes.Add(Ligne)
        End Sub

        Public Sub SupprimerUneligne(ByVal Ligne As CacheLigneScript)
            If WsListeLignes Is Nothing Then
                Exit Sub
            End If
            WsListeLignes.Remove(Ligne)
        End Sub
    End Class
End Namespace
