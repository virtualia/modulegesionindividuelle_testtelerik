﻿Option Explicit On
Option Strict Off
Option Compare Text
Namespace VCaches
    <Serializable>
    Public Class CacheImport
        Private WsTypeImport As String = ""
        Private WsDateValeur As String = ""
        Private WsListeFichiers As List(Of String)

        Public Property TypeImport As String
            Get
                Return WsTypeImport
            End Get
            Set(value As String)
                WsTypeImport = value
            End Set
        End Property

        Public Property Date_Valeur As String
            Get
                Return WsDateValeur
            End Get
            Set(value As String)
                WsDateValeur = value
            End Set
        End Property

        Public Property ListeFichiers As List(Of String)
            Get
                Return WsListeFichiers
            End Get
            Set(value As List(Of String))
                WsListeFichiers = value
            End Set
        End Property

        Public Property Fichier(ByVal Index As Integer) As String
            Get
                If WsListeFichiers Is Nothing OrElse Index >= WsListeFichiers.Count Then
                    Return ""
                End If
                Return WsListeFichiers.Item(Index)
            End Get
            Set(value As String)
                If WsListeFichiers Is Nothing Then
                    WsListeFichiers = New List(Of String)
                End If
                If Index >= WsListeFichiers.Count Then
                    WsListeFichiers.Add(value)
                Else
                    WsListeFichiers.Item(Index) = value
                End If
            End Set
        End Property
    End Class
End Namespace