﻿Option Explicit On
Option Strict Off
Option Compare Text
Namespace VCaches
    <Serializable>
    Public Class CacheListeMensuelle
        Private WsIdentifiant As Integer
        Private WsLibelle As String
        Private WsFiltre As String
        Private WsBulle As String
        Private WsLstDates As List(Of CacheJournees) = New List(Of CacheJournees)
        Public Property Identifiant As Integer
            Get
                Return WsIdentifiant
            End Get
            Set(value As Integer)
                WsIdentifiant = value
            End Set
        End Property
        Public Property Libelle As String
            Get
                Return WsLibelle
            End Get
            Set(value As String)
                WsLibelle = value
            End Set
        End Property
        Public Property Filtre As String
            Get
                Return WsFiltre
            End Get
            Set(value As String)
                WsFiltre = value
            End Set
        End Property
        Public Property Bulle As String
            Get
                Return WsBulle
            End Get
            Set(value As String)
                WsBulle = value
            End Set
        End Property
        Public Property ListeJournees As List(Of CacheJournees)
            Get
                Return WsLstDates
            End Get
            Set(value As List(Of CacheJournees))
                WsLstDates = value
            End Set
        End Property
    End Class
    <Serializable>
    Public Class CacheJournees
        Private WsDateDebut As String
        Private WsDateFin As String
        Private WsIntitule As String
        Private WsComplement As String
        Private WsHeureDebut_AM As String
        Private WsHeureFin_AM As String
        Private WsHeureDebut_PM As String
        Private WsHeureFin_PM As String

        Public Property DateDebut As String
            Get
                Return WsDateDebut
            End Get
            Set(value As String)
                WsDateDebut = value
            End Set
        End Property
        Public ReadOnly Property DateDebut_ToDate As Date
            Get
                If WsDateDebut Is Nothing OrElse WsDateDebut = "" Then
                    Return System.DateTime.MinValue
                End If
                Try
                    Return CDate(WsDateDebut)
                Catch ex As Exception
                    Return System.DateTime.MinValue
                End Try
            End Get
        End Property
        Public Property DateFin As String
            Get
                Return WsDateFin
            End Get
            Set(value As String)
                WsDateFin = value
            End Set
        End Property
        Public ReadOnly Property DateFin_ToDate As Date
            Get
                If WsDateFin Is Nothing OrElse WsDateFin = "" Then
                    Return System.DateTime.MaxValue
                End If
                Try
                    Return CDate(WsDateFin)
                Catch ex As Exception
                    Return System.DateTime.MaxValue
                End Try
            End Get
        End Property
        Public Property Intitule As String
            Get
                Return WsIntitule
            End Get
            Set(value As String)
                WsIntitule = value
            End Set
        End Property
        Public Property Complement As String
            Get
                Return WsComplement
            End Get
            Set(value As String)
                WsComplement = value
            End Set
        End Property
        Public Property HeureDebut_AM As String
            Get
                Return WsHeureDebut_AM
            End Get
            Set(value As String)
                WsHeureDebut_AM = value
            End Set
        End Property
        Public Property HeureFin_AM As String
            Get
                Return WsHeureFin_AM
            End Get
            Set(value As String)
                WsHeureFin_AM = value
            End Set
        End Property
        Public Property HeureDebut_PM As String
            Get
                Return WsHeureDebut_PM
            End Get
            Set(value As String)
                WsHeureDebut_PM = value
            End Set
        End Property
        Public Property HeureFin_PM As String
            Get
                Return WsHeureFin_PM
            End Get
            Set(value As String)
                WsHeureFin_PM = value
            End Set
        End Property
    End Class
End Namespace