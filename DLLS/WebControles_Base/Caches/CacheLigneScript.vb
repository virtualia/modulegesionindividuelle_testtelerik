﻿Option Explicit On
Option Strict Off
Option Compare Text
Namespace VCaches
    <Serializable>
    Public Class CacheLigneScript
        Private WsNumLigne As Integer
        Private WsNumObjet As Integer
        Private WsNumInfo As Integer
        Private WsOpeComparaison As Virtualia.Systeme.Constantes.Operateurs = Virtualia.Systeme.Constantes.Operateurs.Egalite
        Private WsIntitule As String = ""
        Private WsLongueurDonnee As Integer
        Private WsNbColonnes As Integer
        Private WsFormatDonnee As Virtualia.Systeme.Constantes.FormatDonnee = Virtualia.Systeme.Constantes.FormatDonnee.Standard
        Private WsListeValeurFiltre As List(Of KeyValuePair(Of Integer, String)) = Nothing
        Private WsPtdeVue As Integer

        Public Property NumeroLigne As Integer
            Get
                Return WsNumLigne
            End Get
            Set(value As Integer)
                WsNumLigne = value
            End Set
        End Property

        Public Property NumeroObjet As Integer
            Get
                Return WsNumObjet
            End Get
            Set(value As Integer)
                WsNumObjet = value
            End Set
        End Property

        Public Property NumeroInfo As Integer
            Get
                Return WsNumInfo
            End Get
            Set(value As Integer)
                WsNumInfo = value
            End Set
        End Property

        Public Property OperateurComparaison As Virtualia.Systeme.Constantes.Operateurs
            Get
                Return WsOpeComparaison
            End Get
            Set(value As Virtualia.Systeme.Constantes.Operateurs)
                WsOpeComparaison = value
            End Set
        End Property

        Public Property Intitule As String
            Get
                Return WsIntitule
            End Get
            Set(value As String)
                WsIntitule = value
            End Set
        End Property

        Public Property LongueurDonnee As Integer
            Get
                Return WsLongueurDonnee
            End Get
            Set(value As Integer)
                WsLongueurDonnee = value
            End Set
        End Property

        Public Property NombreColonnes As Integer
            Get
                Return WsNbColonnes
            End Get
            Set(value As Integer)
                WsNbColonnes = value
            End Set
        End Property

        Public Property FormatDonnee As Virtualia.Systeme.Constantes.FormatDonnee
            Get
                Return WsFormatDonnee
            End Get
            Set(value As Virtualia.Systeme.Constantes.FormatDonnee)
                WsFormatDonnee = value
            End Set
        End Property

        Public ReadOnly Property ListeValeurFiltre As List(Of KeyValuePair(Of Integer, String))
            Get
                Return WsListeValeurFiltre
            End Get
        End Property

        Public Sub AjouterUneValeur(ByVal NumInfo As Integer, ByVal Valeur As String)
            If WsListeValeurFiltre Is Nothing Then
                WsListeValeurFiltre = New List(Of KeyValuePair(Of Integer, String))
            End If
            WsListeValeurFiltre.Add(New KeyValuePair(Of Integer, String)(NumInfo, Valeur))
        End Sub

        Public Sub SupprimerUneValeur(ByVal Valeur As String)
            If WsListeValeurFiltre Is Nothing Then
                Exit Sub
            End If
            For Each Paire In WsListeValeurFiltre
                If Paire.Value = Valeur Then
                    WsListeValeurFiltre.Remove(Paire)
                    Exit For
                End If
            Next
        End Sub

        Public ReadOnly Property PointdeVue As Integer
            Get
                Return WsPtdeVue
            End Get
        End Property

        Public Sub New(ByVal PtdeVue As Integer)
            WsPtdeVue = PtdeVue
        End Sub
    End Class
End Namespace

