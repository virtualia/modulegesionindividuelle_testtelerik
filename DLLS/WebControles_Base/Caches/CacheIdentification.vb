﻿Option Explicit On
Option Strict Off
Option Compare Text
Namespace VCaches
    <Serializable>
    Public Class CacheIdentification
        Private WsTild As String = "~"
        Private WsNomIdentification As String = ""
        Private WsPrenom As String = ""
        Private WsMotdePasseCrypte As String = ""
        Private WsCocheIdent As Boolean = False
        Private WsCochePW As Boolean = False
        Private WsModeCnx As String = ""
        Private WsSessionID As String = ""

        Public Property Nom_Identification As String
            Get
                Return WsNomIdentification
            End Get
            Set(value As String)
                WsNomIdentification = value
            End Set
        End Property

        Public Property Prenom As String
            Get
                Return WsPrenom
            End Get
            Set(value As String)
                WsPrenom = value
            End Set
        End Property

        Public Property MotdePasseCrypte As String
            Get
                Return WsMotdePasseCrypte
            End Get
            Set(value As String)
                WsMotdePasseCrypte = value
            End Set
        End Property

        Public Property SiSeSouvenir_Identification As Boolean
            Get
                Return WsCocheIdent
            End Get
            Set(value As Boolean)
                WsCocheIdent = value
            End Set
        End Property

        Public Property SiSeSouvenir_Password As Boolean
            Get
                Return WsCochePW
            End Get
            Set(value As Boolean)
                WsCochePW = value
            End Set
        End Property

        Public Property ModedeConnexion As String
            Get
                Return WsModeCnx
            End Get
            Set(value As String)
                WsModeCnx = value
            End Set
        End Property

        Public Property NumeroSession As String
            Get
                Return WsSessionID
            End Get
            Set(value As String)
                WsSessionID = value
            End Set
        End Property

        Public Property Chaine_Cookie As String
            Get
                Dim Chaine As New System.Text.StringBuilder
                If SiSeSouvenir_Identification = True Then
                    Chaine.Append(Nom_Identification & WsTild & Prenom & WsTild)
                    If SiSeSouvenir_Password = True Then
                        Chaine.Append(MotdePasseCrypte & WsTild)
                    Else
                        Chaine.Append(WsTild)
                    End If
                    Chaine.Append("1" & WsTild) 'SiSeSouvenir_Identification
                    If SiSeSouvenir_Password = True Then
                        Chaine.Append("1" & WsTild)
                    Else
                        Chaine.Append("0" & WsTild)
                    End If
                Else
                    Chaine.Append(Strings.StrDup(3, WsTild) & "0" & WsTild & "0" & WsTild)
                End If
                Chaine.Append(ModedeConnexion & WsTild)
                Chaine.Append(NumeroSession)
                Return Chaine.ToString
            End Get
            Set(value As String)
                Dim TableauData(0) As String
                TableauData = Strings.Split(value, WsTild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If
                Nom_Identification = TableauData(0)
                Prenom = TableauData(1)
                MotdePasseCrypte = TableauData(2)
                If TableauData(3) = "1" Then
                    SiSeSouvenir_Identification = True
                End If
                If TableauData(4) = "1" Then
                    SiSeSouvenir_Password = True
                End If
                ModedeConnexion = TableauData(5)
                Select Case ModedeConnexion
                    Case "AD", "V3", "V4"
                        Prenom = ""
                End Select
                NumeroSession = TableauData(6)
            End Set
        End Property
    End Class
End Namespace