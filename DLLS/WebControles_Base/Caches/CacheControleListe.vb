﻿Option Explicit On
Option Strict Off
Option Compare Text
Namespace VCaches
    <Serializable>
    Public Class CacheControleListe
        Private WsLibellesCaption As List(Of String) = Nothing
        Private WsLibellesColonne As List(Of String) = Nothing
        Private WsListeValeurs As List(Of String) = Nothing
        Private WsListeToolTip As List(Of String) = Nothing

        Private WsClefSelectionnee As String = ""
        Private WsItemSelectionne As String = ""
        Private WsNoPage As Integer = 1
        Private WsTotalPages As Integer = 1

        Private WsPointdeVue As Integer
        Private WsNumObjet As Integer
        Private WsNomTable As String = ""
        Private WsCritere As String = ""

        Private WsColonneFiltre As Integer = 1
        Private WsValeurFiltre As String = ""
        Private WsDateDebutFiltre As String = ""
        Private WsDateFinFiltre As String = ""
        Private WsValeurNulle As String = ""

        Public Property Libelles_Caption As List(Of String)
            Get
                If WsLibellesCaption Is Nothing Then
                    WsLibellesCaption = New List(Of String)
                    WsLibellesCaption.Add("aucune situation")
                    WsLibellesCaption.Add("une situation")
                    WsLibellesCaption.Add("situations")
                End If
                Return WsLibellesCaption
            End Get
            Set(value As List(Of String))
                WsLibellesCaption = value
            End Set
        End Property

        Public Property Libelles_Colonne As List(Of String)
            Get
                Return WsLibellesColonne
            End Get
            Set(value As List(Of String))
                WsLibellesColonne = value
            End Set
        End Property

        Public Property Liste_Valeurs As List(Of String)
            Get
                Return WsListeValeurs
            End Get
            Set(value As List(Of String))
                WsListeValeurs = value
            End Set
        End Property

        Public Property Liste_ToolTip As List(Of String)
            Get
                Return WsListeToolTip
            End Get
            Set(value As List(Of String))
                WsListeToolTip = value
            End Set
        End Property

        Public Property Clef_ItemSelectionne As String
            Get
                Return WsClefSelectionnee
            End Get
            Set(value As String)
                WsClefSelectionnee = value
            End Set
        End Property

        Public Property Valeur_ItemSelectionne As String
            Get
                Return WsItemSelectionne
            End Get
            Set(value As String)
                WsItemSelectionne = value
            End Set
        End Property

        Public Property Total_Pages As Integer
            Get
                Return WsTotalPages
            End Get
            Set(value As Integer)
                WsTotalPages = value
            End Set
        End Property

        Public Property Numero_Page As Integer
            Get
                Return WsNoPage
            End Get
            Set(value As Integer)
                If value < 1 Or value > WsTotalPages Then
                    Exit Property
                End If
                WsNoPage = value
            End Set
        End Property

        Public Property Point_de_Vue As Integer
            Get
                Return WsPointdeVue
            End Get
            Set(value As Integer)
                WsPointdeVue = value
            End Set
        End Property

        Public Property Numero_Objet As Integer
            Get
                Return WsNumObjet
            End Get
            Set(value As Integer)
                WsNumObjet = value
            End Set
        End Property

        Public Property NomTableGenerale As String
            Get
                Return WsNomTable
            End Get
            Set(value As String)
                WsNomTable = value
            End Set
        End Property

        Public Property Critere As String
            Get
                Return WsCritere
            End Get
            Set(value As String)
                WsCritere = value
            End Set
        End Property

        Public Property Colonne_Filtre As Integer
            Get
                Return WsColonneFiltre
            End Get
            Set(value As Integer)
                WsColonneFiltre = value
            End Set
        End Property

        Public Property Valeur_Filtre As String
            Get
                Return WsValeurFiltre
            End Get
            Set(value As String)
                WsValeurFiltre = value
            End Set
        End Property

        Public Property Date_Debut_Filtre As String
            Get
                Return WsDateDebutFiltre
            End Get
            Set(value As String)
                WsDateDebutFiltre = value
            End Set
        End Property

        Public Property Date_Fin_Filtre As String
            Get
                Return WsDateFinFiltre
            End Get
            Set(value As String)
                WsDateFinFiltre = value
            End Set
        End Property

        Public Property Valeur_Nulle As String
            Get
                Return WsValeurNulle
            End Get
            Set(value As String)
                WsValeurNulle = value
            End Set
        End Property
    End Class
End Namespace
