﻿Option Explicit On
Option Strict Off
Option Compare Text
Namespace VCaches
    <Serializable>
    Public Class CacheControleMessage
        Private WsEmetteur As String = ""
        Private WsChaineDatas As String = ""
        Private WsNumeroObjet As Integer

        Public Property Emetteur As String
            Get
                Return WsEmetteur
            End Get
            Set(value As String)
                WsEmetteur = value
            End Set
        End Property

        Public Property ChaineDatas As String
            Get
                Return WsChaineDatas
            End Get
            Set(value As String)
                WsChaineDatas = value
            End Set
        End Property

        Public Property NumeroObjet As Integer
            Get
                Return WsNumeroObjet
            End Get
            Set(value As Integer)
                WsNumeroObjet = value
            End Set
        End Property
    End Class
End Namespace