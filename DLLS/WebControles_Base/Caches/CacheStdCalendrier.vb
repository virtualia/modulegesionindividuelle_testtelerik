﻿Option Strict On
Option Explicit On
Option Compare Text
Namespace VCaches
    <Serializable>
    Public Class CacheStdCalendrier
        Private WsAnnee As Integer = 0
        Private WsMois As Integer = 0
        Private WsFiltreN1 As String = ""
        Private WsFiltreN2 As String = ""
        Private WsLstSujets As List(Of Virtualia.Net.VCaches.CacheListeMensuelle) = New List(Of Virtualia.Net.VCaches.CacheListeMensuelle)
        Public Property Annee As Integer
            Get
                Return WsAnnee
            End Get
            Set(value As Integer)
                WsAnnee = value
            End Set
        End Property
        Public Property Mois As Integer
            Get
                Return WsMois
            End Get
            Set(value As Integer)
                WsMois = value
            End Set
        End Property
        Public Property Filtre_N1 As String
            Get
                Return WsFiltreN1
            End Get
            Set(value As String)
                WsFiltreN1 = value
            End Set
        End Property
        Public Property Filtre_N2 As String
            Get
                Return WsFiltreN2
            End Get
            Set(value As String)
                WsFiltreN2 = value
            End Set
        End Property
        Public Property ListeSujets As List(Of Virtualia.Net.VCaches.CacheListeMensuelle)
            Get
                Return WsLstSujets
            End Get
            Set(value As List(Of Virtualia.Net.VCaches.CacheListeMensuelle))
                WsLstSujets = value
            End Set
        End Property
    End Class
End Namespace