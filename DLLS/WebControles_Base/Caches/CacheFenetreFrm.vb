﻿Option Explicit On
Option Strict Off
Option Compare Text
Namespace VCaches
    <Serializable>
    Public Class CacheFenetreFrm
        Private WsIdentifiant As Integer
        Private WsDateValeur As String
        Private WsSelRegime As String
        Private WsSelTri As String
        Private WsSelCritere As String
        Private WsSelValeur As String
        Private WsSelIndex As Integer
        Private WsVueActive As Integer
        Private WsSelAnnee As Integer

        Public Property Ide_Dossier As Integer
            Get
                Return WsIdentifiant
            End Get
            Set(value As Integer)
                WsIdentifiant = value
            End Set
        End Property

        Public Property Date_Valeur As String
            Get
                Return WsDateValeur
            End Get
            Set(value As String)
                WsDateValeur = value
            End Set
        End Property

        Public Property Regime_Selection As String
            Get
                Return WsSelRegime
            End Get
            Set(value As String)
                WsSelRegime = value
            End Set
        End Property

        Public Property Tri_Selection As String
            Get
                Return WsSelTri
            End Get
            Set(value As String)
                WsSelTri = value
            End Set
        End Property

        Public Property Critere_Selection As String
            Get
                Return WsSelCritere
            End Get
            Set(value As String)
                WsSelCritere = value
            End Set
        End Property

        Public Property Valeur_Selection As String
            Get
                Return WsSelValeur
            End Get
            Set(value As String)
                WsSelValeur = value
            End Set
        End Property

        Public Property Index_Selection As Integer
            Get
                Return WsSelIndex
            End Get
            Set(value As Integer)
                WsSelIndex = value
            End Set
        End Property

        Public Property Index_VueActive As Integer
            Get
                Return WsVueActive
            End Get
            Set(value As Integer)
                WsVueActive = value
            End Set
        End Property

        Public Property Annee_Selection As Integer
            Get
                Return WsSelAnnee
            End Get
            Set(value As Integer)
                WsSelAnnee = value
            End Set
        End Property
    End Class
End Namespace
