﻿Option Strict On
Option Explicit On
Option Compare Text
Imports System.Web.UI.WebControls
Namespace Controles
    Public Class ObjetWebControlSaisie
        Inherits System.Web.UI.UserControl
        Private WsCharte As Virtualia.Systeme.Fonctions.CharteGraphique
        '
        Private WebFct As Virtualia.Net.Controles.WebFonctions
        Private WsNomState As String = "VSaisie"

        Private WsNumCharte As Integer = 0
        Private WsPolice As System.Drawing.Font
        Private WsPoliceNom As String = "Trebuchet MS"
        Private WsPoliceBold As Boolean = False
        Private WsPoliceItalic As Boolean = True
        Private WsPoliceTaille As System.Web.UI.WebControls.FontUnit = FontUnit.Small

        Private WsSiDonneeDico As Boolean = True
        Private WsSiFenetrePer_Bis As Boolean = False
        Private WsPointdeVue As Integer = 1
        Private WsObjet As Integer = 1
        Private WsInformation As Integer = 1
        Private WsNatureDonnee As Integer = 0
        Private WsFormatDonnee As Integer = 0
        Private WsTabIndex As Integer = 0

        Public Property V_SiDonneeDico() As Boolean
            Get
                Return WsSiDonneeDico
            End Get
            Set(ByVal value As Boolean)
                WsSiDonneeDico = value
            End Set
        End Property

        Public Property V_SiFenetrePer_Bis() As Boolean
            Get
                Return WsSiFenetrePer_Bis
            End Get
            Set(ByVal value As Boolean)
                WsSiFenetrePer_Bis = value
            End Set
        End Property

        Public Property V_PointdeVue() As Integer
            Get
                Return WsPointdeVue
            End Get
            Set(ByVal value As Integer)
                WsPointdeVue = value
            End Set
        End Property

        Public Property V_Objet() As Integer
            Get
                Return WsObjet
            End Get
            Set(ByVal value As Integer)
                WsObjet = value
            End Set
        End Property

        Public Property V_Information() As Integer
            Get
                Return WsInformation
            End Get
            Set(ByVal value As Integer)
                WsInformation = value
            End Set
        End Property

        Public Property V_Nature() As Integer
            Get
                Return WsNatureDonnee
            End Get
            Set(ByVal value As Integer)
                WsNatureDonnee = value
            End Set
        End Property

        Public Property V_Format() As Integer
            Get
                Return WsFormatDonnee
            End Get
            Set(ByVal value As Integer)
                WsFormatDonnee = value
            End Set
        End Property

        Public Property V_TabIndex() As Integer
            Get
                Return WsTabIndex
            End Get
            Set(ByVal value As Integer)
                WsTabIndex = value
            End Set
        End Property

        Public ReadOnly Property V_CharteGraphique As Virtualia.Systeme.Fonctions.CharteGraphique
            Get
                If WsCharte Is Nothing Then
                    WsCharte = New Virtualia.Systeme.Fonctions.CharteGraphique
                End If
                Return WsCharte
            End Get
        End Property

        Public ReadOnly Property V_Numero_Charte() As Integer
            Get
                Return WsNumCharte
            End Get
        End Property

        Public ReadOnly Property V_Base_BackColor() As System.Drawing.Color
            Get
                Return V_CharteGraphique.Base_BackColor(WsNumCharte)
            End Get
        End Property

        Public ReadOnly Property V_Titre_BackColor() As System.Drawing.Color
            Get
                Return V_CharteGraphique.Titre_BackColor(WsNumCharte)
            End Get
        End Property

        Public ReadOnly Property V_SousTitre_BackColor() As System.Drawing.Color
            Get
                Return V_CharteGraphique.SousTitre_BackColor(WsNumCharte)
            End Get
        End Property

        Public ReadOnly Property V_Selection_BackColor() As System.Drawing.Color
            Get
                Return V_CharteGraphique.Selection_BackColor(WsNumCharte)
            End Get
        End Property

        Public ReadOnly Property V_PoliceClaire() As System.Drawing.Color
            Get
                Return V_CharteGraphique.Police_Claire(WsNumCharte)
            End Get
        End Property

        Public ReadOnly Property V_PoliceFonce() As System.Drawing.Color
            Get
                Return V_CharteGraphique.Police_Fonce(WsNumCharte)
            End Get
        End Property

        Public ReadOnly Property V_DonBordercolor() As System.Drawing.Color
            Get
                Return V_CharteGraphique.Donnee_Bordercolor(WsNumCharte)
            End Get
        End Property

        Public ReadOnly Property V_EtiBackcolor() As System.Drawing.Color
            Get
                Return V_CharteGraphique.Etiquette_Backcolor(WsNumCharte)
            End Get
        End Property

        Public ReadOnly Property V_InverseBackColor() As System.Drawing.Color
            Get
                Return V_CharteGraphique.SysRef_ForeColor(WsNumCharte)
            End Get
        End Property

        Public ReadOnly Property V_EtiForecolor() As System.Drawing.Color
            Get
                Return V_CharteGraphique.Etiquette_Forecolor(WsNumCharte)
            End Get
        End Property

        Public ReadOnly Property V_EtiBordercolor() As System.Drawing.Color
            Get
                Return V_CharteGraphique.Etiquette_Bordercolor(WsNumCharte)
            End Get
        End Property

        Public ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
            Get
                If WebFct Is Nothing Then
                    WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
                End If
                Return WebFct
            End Get
        End Property

        Public ReadOnly Property V_FontName() As String
            Get
                Return WsPoliceNom
            End Get
        End Property

        Public ReadOnly Property V_FontTaille() As System.Web.UI.WebControls.FontUnit
            Get
                Return WsPoliceTaille
            End Get
        End Property

        Public ReadOnly Property V_FontBold() As Boolean
            Get
                Return WsPoliceBold
            End Get
        End Property

        Public ReadOnly Property V_FontItalic() As Boolean
            Get
                Return WsPoliceItalic
            End Get
        End Property

    End Class
End Namespace