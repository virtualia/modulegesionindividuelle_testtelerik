﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Session
    Public Class ObjetSession
        Private WsAppGlobal As Virtualia.Net.Session.ObjetGlobalBase
        'Utilisateur
        Private WsTypeUti As String '** I = Interne Virtualia, E = Externe Via Version 3 - Intranet - Nouveaux Modules
        Private WsNomConnexion As String = ""
        Private WsPrenomConnexion As String = ""
        Private WsInitialesConnexion As String = ""
        Private WsIdeVirtualia As Integer
        Private WsIDSession As String 'Attribué par Virtualia au moment de la connexion = 1ere SessionID IIS
        Private WsSgbdUtilisateur As Integer = 0
        Private WsWebServeurUtilisateur As String = ""
        Private WsID_Machine As String = ""
        Private WsID_AdresseIP As String = ""
        Private WsID_LogonIdentity As String = ""
        '
        Private WsRhFonction As Virtualia.Systeme.Fonctions.Generales
        Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates

        Private WsSessionModule As Object
        Private WsObjetArmoirePER As Virtualia.Ressources.Datas.ObjetArmoirePER
        Private WsCouranteArmoirePER As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
        Private WsSousEnsembleArmoirePer As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
        Private WsObjetCalendrier As Virtualia.Net.Session.ObjetCalendrier = Nothing
        '**
        Private WsFiltreV3 As String = ""
        Private WsFiltreEtablissement As String = ""
        Private WsListeNatureAbsence_Self As List(Of String) = Nothing

        'TS - Objet Navigation pour le contexte
        Private WsParamModule As String = ""
        Private WsContexte As Virtualia.Net.Session.ObjetNavigation
        '** Execution Sous-Sélection de dossiers **
        Private WsSousListeIde As List(Of Integer)
        '** Exécution
        Private WsSiActualiser As Boolean = False
        Private TsSiEnExecution As Boolean = False
        Private TsIndexExecution As Integer = 0
        Private TsParam_Annee As String = ""
        Private TsParam_Etablissement As String = ""
        Private TsParam_Niveau1 As String = ""
        Private TsParam_Repartition As String = ""
        Private TsParam_ListeRupture As List(Of String)
        Private TsParam_TypedeCritere As String = ""
        Private TsParam_Critere As String = ""
        '** Navigation
        Private WsEcranCourant As String
        Private WsVueActive As Integer = 0
        Private WsSiValideur As Boolean = False
        Private WsSiEnLecture As Boolean = False

        Public ReadOnly Property V_PointeurGlobal As Virtualia.Net.Session.ObjetGlobalBase
            Get
                Return WsAppGlobal
            End Get
        End Property

        Public Property V_PointeurSessionModule As Object
            Get
                Return WsSessionModule
            End Get
            Set(value As Object)
                WsSessionModule = value
            End Set
        End Property

        Public Property PointeurArmoirePER As Virtualia.Ressources.Datas.ObjetArmoirePER
            Get
                If WsObjetArmoirePER Is Nothing Then
                    WsObjetArmoirePER = New Virtualia.Ressources.Datas.ObjetArmoirePER(V_NomdUtilisateurSgbd, WsAppGlobal.VirModele, _
                                            WsAppGlobal.VirInstanceBd, WsFiltreEtablissement, WsFiltreV3)

                End If
                Return WsObjetArmoirePER
            End Get
            Set(value As Virtualia.Ressources.Datas.ObjetArmoirePER)
                WsObjetArmoirePER = value
            End Set
        End Property

        Public Property PointeurListeCourantePER As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Get
                Return WsCouranteArmoirePER
            End Get
            Set(value As List(Of Virtualia.Net.ServiceServeur.VirRequeteType))
                WsCouranteArmoirePER = value
            End Set
        End Property

        Public Property PointeurSousEnsemble As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Get
                Return WsSousEnsembleArmoirePer
            End Get
            Set(value As List(Of Virtualia.Net.ServiceServeur.VirRequeteType))
                WsSousEnsembleArmoirePer = value
            End Set
        End Property

        Public Property V_SiEnExecution As Boolean
            Get
                Return TsSiEnExecution
            End Get
            Set(ByVal value As Boolean)
                TsSiEnExecution = value
            End Set
        End Property

        Public Property V_IndexExecution As Integer
            Get
                Return TsIndexExecution
            End Get
            Set(ByVal value As Integer)
                TsIndexExecution = value
            End Set
        End Property

        Public Property VueActive As Integer
            Get
                Return WsVueActive
            End Get
            Set(ByVal value As Integer)
                WsVueActive = value
            End Set
        End Property

        Public Property Parametre_Module As String
            Get
                Return WsParamModule
            End Get
            Set(ByVal value As String)
                WsParamModule = value
            End Set
        End Property

        Public ReadOnly Property ListeNatureAbsence_Self As List(Of String)
            Get
                If WsListeNatureAbsence_Self Is Nothing Then
                    Dim LstTabGenerale As List(Of Virtualia.Net.ServiceServeur.VirRequeteType) = Nothing
                    Dim IndiceI As Integer
                    LstTabGenerale = WsAppGlobal.LireTableGeneraleSimple("Intranet - Type de congés")
                    WsListeNatureAbsence_Self = New List(Of String)
                    If LstTabGenerale Is Nothing Then
                        WsListeNatureAbsence_Self.Add("Congés annuels")
                        WsListeNatureAbsence_Self.Add("Journées ARTT")
                    Else
                        For IndiceI = 0 To LstTabGenerale.Count - 1
                            WsListeNatureAbsence_Self.Add(LstTabGenerale.Item(IndiceI).Valeurs.Item(0))
                        Next IndiceI
                    End If
                End If
                Return WsListeNatureAbsence_Self
            End Get
        End Property

        Public Property V_SousListeIde As List(Of Integer)
            Get
                Return WsSousListeIde
            End Get
            Set(ByVal value As List(Of Integer))
                WsSousListeIde = value
            End Set
        End Property

        Public Property EcranCourant As String
            Get
                If WsEcranCourant = "" Then
                    WsEcranCourant = "HISTO"
                End If
                Return WsEcranCourant
            End Get
            Set(ByVal value As String)
                WsEcranCourant = value
            End Set
        End Property

        Public Property Param_Annee As String
            Get
                If TsParam_Annee = "" Then
                    TsParam_Annee = Year(Now).ToString
                End If
                Return TsParam_Annee
            End Get
            Set(ByVal value As String)
                TsParam_Annee = value
            End Set
        End Property

        Public Property Param_Etablissement As String
            Get
                If TsParam_Etablissement = "" Then
                    TsParam_Etablissement = WsFiltreEtablissement
                End If
                Return TsParam_Etablissement
            End Get
            Set(ByVal value As String)
                TsParam_Etablissement = value
            End Set
        End Property

        Public Property Param_Niveau1 As String
            Get
                Return TsParam_Niveau1
            End Get
            Set(ByVal value As String)
                TsParam_Niveau1 = value
            End Set
        End Property

        Public Property Param_Repartition As String
            Get
                Return TsParam_Repartition
            End Get
            Set(ByVal value As String)
                TsParam_Repartition = value
            End Set
        End Property

        Public Property Param_ListeRupture As List(Of String)
            Get
                Return TsParam_ListeRupture
            End Get
            Set(ByVal value As List(Of String))
                TsParam_ListeRupture = value
            End Set
        End Property

        Public Property Param_TypedeCritere As String
            Get
                Return TsParam_TypedeCritere
            End Get
            Set(ByVal value As String)
                TsParam_TypedeCritere = value
            End Set
        End Property

        Public Property Param_Critere As String
            Get
                Return TsParam_Critere
            End Get
            Set(ByVal value As String)
                TsParam_Critere = value
            End Set
        End Property

        Public Property SiActualiser As Boolean
            Get
                Return WsSiActualiser
            End Get
            Set(value As Boolean)
                WsSiActualiser = value
            End Set
        End Property

        Public ReadOnly Property Etablissement As String
            Get
                Return WsFiltreEtablissement
            End Get
        End Property

        Public ReadOnly Property FiltreV3 As String
            Get
                Return WsFiltreV3
            End Get
        End Property

        Public ReadOnly Property SiEnLectureV3 As Boolean
            Get
                Return WsSiEnLecture
            End Get
        End Property

        Public Property SiValideurV3 As Boolean
            Get
                Return WsSiValideur
            End Get
            Set(value As Boolean)
                WsSiValideur = value
            End Set
        End Property

        Public Overridable Sub Actualiser(ByVal PtdeVue As Integer, ByVal NoObjet As Integer, ByVal Ide As Integer)
            Call ReInitialiser()
        End Sub

        Public Sub ActualiserLaConnexion(ByVal NomUtilisateur As String, ByVal FiltreEtablissement As String, ByVal FiltreV3 As String)
            WsNomConnexion = NomUtilisateur
            WsFiltreEtablissement = FiltreEtablissement
            WsFiltreV3 = FiltreV3
        End Sub

        Public Sub ReInitialiser()
            Try
                GC.Collect()
                GC.WaitForPendingFinalizers()
                GC.Collect()
            Catch ex As Exception
                Exit Try
            End Try
        End Sub

        Public Property V_IDSession As String
            Get
                Return WsIDSession
            End Get
            Set(ByVal value As String)
                WsIDSession = value
            End Set
        End Property

        Public ReadOnly Property V_NomdeConnexion() As String
            Get
                Return WsNomConnexion
            End Get
        End Property

        Public Property V_PrenomdeConnexion As String
            Get
                Return WsPrenomConnexion
            End Get
            Set(ByVal value As String)
                WsPrenomConnexion = value
            End Set
        End Property

        Public Property V_InitialesdeConnexion As String
            Get
                Return WsInitialesConnexion
            End Get
            Set(ByVal value As String)
                WsInitialesConnexion = value
            End Set
        End Property

        Public Property V_Identifiant As Integer
            Get
                Return WsIdeVirtualia
            End Get
            Set(ByVal value As Integer)
                WsIdeVirtualia = value
            End Set
        End Property

        Public Property V_NomdUtilisateurSgbd() As String
            Get
                If WsWebServeurUtilisateur Is Nothing OrElse WsWebServeurUtilisateur = "" Then
                    WsWebServeurUtilisateur = WsAppGlobal.VirNomUtilisateur
                End If
                Return WsWebServeurUtilisateur
            End Get
            Set(value As String)
                WsWebServeurUtilisateur = value
            End Set
        End Property

        Public ReadOnly Property V_NumeroSgbdActif() As Integer
            Get
                Return WsSgbdUtilisateur
            End Get
        End Property

        Public Property ID_Machine As String
            Get
                Return WsID_Machine
            End Get
            Set(ByVal value As String)
                WsID_Machine = value
            End Set
        End Property

        Public Property ID_AdresseIP As String
            Get
                Return WsID_AdresseIP
            End Get
            Set(ByVal value As String)
                WsID_AdresseIP = value
            End Set
        End Property

        Public Property ID_LogonIdentite As String
            Get
                Return WsID_LogonIdentity
            End Get
            Set(ByVal value As String)
                WsID_LogonIdentity = value
            End Set
        End Property

        Public ReadOnly Property V_PointeurContexte() As Virtualia.Net.Session.ObjetNavigation
            Get
                If WsContexte Is Nothing Then
                    WsContexte = New Virtualia.Net.Session.ObjetNavigation(Me)
                End If
                Return WsContexte
            End Get
        End Property

        Public ReadOnly Property V_RhDates() As Virtualia.Systeme.Fonctions.CalculDates
            Get
                Return WsRhDates
            End Get
        End Property

        Public ReadOnly Property V_RhFonction() As Virtualia.Systeme.Fonctions.Generales
            Get
                Return WsRhFonction
            End Get
        End Property

        Public ReadOnly Property V_ObjetCalendrier(ByVal Ide As Integer, ByVal DateEffet As String) As List(Of Virtualia.Net.Controles.ItemJourCalendrier)
            Get
                If WsObjetCalendrier Is Nothing OrElse WsObjetCalendrier.Ide_Dossier <> Ide Then
                    WsObjetCalendrier = New Virtualia.Net.Session.ObjetCalendrier(WsAppGlobal, Ide)
                End If
                Return WsObjetCalendrier.Dossier_Calendrier(DateEffet)
            End Get
        End Property

        Public Function V_ChangerBaseCourante(ByVal NoBd As Integer) As Boolean

            Dim WcfWebServiceServeur = New Virtualia.Net.WebService.ServiceWcfServeur
            Dim CodeRetour As Boolean = WcfWebServiceServeur.ChangerBasedeDonneesCourante(V_NomdUtilisateurSgbd, NoBd)
            WcfWebServiceServeur = Nothing

            If CodeRetour = True Then
                WsSgbdUtilisateur = NoBd
                Initialiser()
            End If
            Return CodeRetour
        End Function

        Private Sub LireSessionUti()
            Dim WcfWebService As New Virtualia.Net.WebService.ServiceWcfServeur
            Dim Uti As Virtualia.Net.ServiceServeur.UtilisateurType
            Uti = WcfWebService.LireProfilUtilisateurVirtualia(V_NomdeConnexion, V_NomdeConnexion, V_NumeroSgbdActif)
            WcfWebService = Nothing

            WsIdeVirtualia = Uti.Identifiant
            WsPrenomConnexion = Uti.Prenom
            WsSgbdUtilisateur = Uti.InstanceBd
        End Sub

        Private Sub Initialiser()
            WsContexte = Nothing
        End Sub

        Public Sub New(ByVal PointeurGlobal As Virtualia.Net.Session.ObjetGlobalBase, ByVal NomUtilisateur As String,
                       ByVal Ide As Integer, ByVal FiltreEtablissement As String, ByVal FiltreV3 As String)

            WsTypeUti = "I"
            WsNomConnexion = NomUtilisateur
            WsSgbdUtilisateur = PointeurGlobal.VirNoBd
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
            WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates
            WsIdeVirtualia = Ide
            WsAppGlobal = PointeurGlobal
            WsFiltreEtablissement = FiltreEtablissement
            WsFiltreV3 = FiltreV3

        End Sub
    End Class
End Namespace
