﻿Option Explicit On
Option Strict On
Option Compare Text
Namespace Session
    Public Class ObjetNavigation
        Private WsParent As Virtualia.Net.Session.ObjetSession
        Private WsModuleActif As String = "ETP"
        'Système de référence
        Private WsSysRefListe As List(Of Virtualia.Ressources.Datas.ObjetDossierREF)
        Private WsSysRefDossier As Virtualia.Ressources.Datas.ObjetDossierREF
        Private WsSysRefIndexVueRetour As Integer
        Private WsSysRefIDVueRetour As String = ""
        'Fenetres
        Private WsFenActiveVue(12) As Integer
        Private WsFenVuePrecedente(12) As Integer
        '*************************************************************************
        'Tampons
        Private WsTampon_ArrayList As ArrayList
        Private WsTampon_Chaine As String
        Private WsTampon_ListeString As List(Of String)

        Private TsGrid_Entete As String
        Private TsGrid_Tableau As String
        Private TsGrid_Titre As String
        '
        Private TsEtaPDF_Carte As String
        Private TsEtaPDF_Unite As String
        Private TsEtaPDF_Nom As String
        Private TsEtaPDF_Tri As Boolean = False

        Private TsMsgEmetteur As String
        Private TsMsgTitre As String
        Private TsMsgContenu As String

        Public ReadOnly Property VParent() As Virtualia.Net.Session.ObjetSession
            Get
                Return WsParent
            End Get
        End Property

        Public Property ModuleActif As String
            Get
                Return WsModuleActif
            End Get
            Set(value As String)
                WsModuleActif = value
            End Set
        End Property

        Public Property VEtaPDF_Carte As String
            Get
                Return TsEtaPDF_Carte
            End Get
            Set(value As String)
                TsEtaPDF_Carte = value
            End Set
        End Property

        Public Property VMsg_Emetteur As String
            Get
                Return TsMsgEmetteur
            End Get
            Set(value As String)
                TsMsgEmetteur = value
            End Set
        End Property

        Public Property VMsg_Titre As String
            Get
                Return TsMsgTitre
            End Get
            Set(value As String)
                TsMsgTitre = value
            End Set
        End Property

        Public Property VMsg_Contenu As String
            Get
                Return TsMsgContenu
            End Get
            Set(value As String)
                TsMsgContenu = value
            End Set
        End Property

        Public Property VEtaPDF_Unite As String
            Get
                Return TsEtaPDF_Unite
            End Get
            Set(value As String)
                TsEtaPDF_Unite = value
            End Set
        End Property

        Public Property VEtaPDF_Nom As String
            Get
                Return TsEtaPDF_Nom
            End Get
            Set(value As String)
                TsEtaPDF_Nom = value
            End Set
        End Property

        Public Property VEtaPDF_Tri As Boolean
            Get
                Return TsEtaPDF_Tri
            End Get
            Set(value As Boolean)
                TsEtaPDF_Tri = value
            End Set
        End Property

        Public Property VGrid_Entete As String
            Get
                Return TsGrid_Entete
            End Get
            Set(value As String)
                TsGrid_Entete = value
            End Set
        End Property

        Public Property VGrid_Tableau As String
            Get
                Return TsGrid_Tableau
            End Get
            Set(value As String)
                TsGrid_Tableau = value
            End Set
        End Property

        Public Property VGrid_Titre As String
            Get
                Return TsGrid_Titre
            End Get
            Set(value As String)
                TsGrid_Titre = value
            End Set
        End Property

        Public ReadOnly Property TitreModuleActif As String
            Get
                Select Case WsModuleActif
                    Case "ETP"
                        Return "Virtualia - Module de gestion des effectifs, ETP et dépenses de personnels"
                    Case "DGFIP", "GAPAIE"
                        Return "Virtualia - Module de préparation de la paie"
                    Case "Flux"
                        Return "Virtualia - Analyse des flux"
                    Case "Individu"
                        Return "Virtualia - Gestion des données individuelles"
                    Case "PAIE"
                        Return "Virtualia - Comptabilisation de la paie"
                    Case "Synthese"
                        Return "Virtualia - Synthèse des effectifs"
                    Case "Syntremu"
                        Return "Virtualia - Synthèse des rémunérations"
                    Case Else
                        Select Case WsParent.Parametre_Module
                            Case "Etp"
                                Return "Virtualia - Module de gestion des effectifs, ETP et dépenses de personnels"
                            Case Else
                                Return "Virtualia - Module de préparation de la paie"
                        End Select
                End Select
            End Get
        End Property

        Public ReadOnly Property SysRef_Listes() As List(Of Virtualia.Ressources.Datas.ObjetDossierREF)
            Get
                If WsSysRefListe Is Nothing OrElse WsSysRefListe.Count = 0 Then
                    WsSysRefListe = New List(Of Virtualia.Ressources.Datas.ObjetDossierREF)
                End If
                Return WsSysRefListe
            End Get
        End Property

        Public ReadOnly Property SysRef_PointeurDossier(ByVal Pvue As Integer, ByVal Ide As Integer) As Virtualia.Ressources.Datas.ObjetDossierREF
            Get
                If WsSysRefDossier Is Nothing Then
                    WsSysRefDossier = New Virtualia.Ressources.Datas.ObjetDossierREF(WsParent.V_PointeurGlobal.VirModele, Pvue, Ide)
                ElseIf WsSysRefDossier.V_PointdeVue <> Pvue Or WsSysRefDossier.V_Identifiant <> Ide Then
                    WsSysRefDossier = Nothing
                    WsSysRefDossier = New Virtualia.Ressources.Datas.ObjetDossierREF(WsParent.V_PointeurGlobal.VirModele, Pvue, Ide)
                End If
                Return WsSysRefDossier
            End Get
        End Property

        Public Property SysRef_IDVueRetour() As String
            Get
                Return WsSysRefIDVueRetour
            End Get
            Set(ByVal value As String)
                WsSysRefIDVueRetour = value
            End Set
        End Property

        Public Property SysRef_IndexVueRetour() As Integer
            Get
                Return WsSysRefIndexVueRetour
            End Get
            Set(ByVal value As Integer)
                WsSysRefIndexVueRetour = value
            End Set
        End Property

        Public Property TsTampon_ArrayList() As ArrayList
            Get
                Return WsTampon_ArrayList
            End Get
            Set(ByVal value As ArrayList)
                WsTampon_ArrayList = value
            End Set
        End Property

        Public Property TsTampon_Chaine() As String
            Get
                Return WsTampon_Chaine
            End Get
            Set(ByVal value As String)
                WsTampon_Chaine = value
            End Set
        End Property

        Public Property TsTampon_StringList() As List(Of String)
            Get
                Return WsTampon_ListeString
            End Get
            Set(ByVal value As List(Of String))
                WsTampon_ListeString = value
            End Set
        End Property

        Public Property Fenetre_VueActive(ByVal Index As Integer) As Integer
            Get
                If Index > WsFenActiveVue.Count - 1 Then
                    Return -1
                    Exit Property
                End If
                Return WsFenActiveVue(Index)
            End Get
            Set(ByVal value As Integer)
                If Index > WsFenActiveVue.Count - 1 Then
                    Exit Property
                End If
                WsFenVuePrecedente(Index) = WsFenActiveVue(Index)
                WsFenActiveVue(Index) = value
            End Set
        End Property

        Public ReadOnly Property Fenetre_VuePrecedente(ByVal Index As Integer) As Integer
            Get
                If Index > WsFenVuePrecedente.Count - 1 Then
                    Return -1
                    Exit Property
                End If
                Return WsFenVuePrecedente(Index)
            End Get
        End Property

        Public Sub New(ByVal Host As Virtualia.Net.Session.ObjetSession)
            WsParent = Host
        End Sub
    End Class
End Namespace
