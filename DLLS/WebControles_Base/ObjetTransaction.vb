﻿Option Explicit On
Option Strict Off
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Session
    Public Class ObjetTransaction
        Private WsParent As ObjetGlobalBase
        Private WsSessionUti As Virtualia.Net.Session.ObjetSession
        Private WsPointdeVue As Integer = 1
        Private WsModuleOrigine As String = ""
        Private WsProcedureOrigine As String = ""
        Private WsChainageOrigine As Integer = 1

        Public Function CreerTransactionCertifiee(ByVal NoObjet As Integer, ByVal Ide As Integer, ByVal DonneesSignifiantes As String) As String
            Dim FicheCER As Virtualia.TablesObjet.ShemaREF.CER_TRANSACTION
            Dim Empreinte As Virtualia.TablesObjet.ShemaREF.CER_EMPREINTE
            Dim RandomizerAlea As System.Random
            Dim IdeCLef As Integer
            Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

            Do
                RandomizerAlea = New System.Random
                IdeCLef = RandomizerAlea.Next(100000000)
                LstRes = WsParent.VirServiceServeur.LectureObjet_ToFiches(WsParent.VirNomUtilisateur, 41, 2, IdeCLef, False)
                If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                    Exit Do
                End If
            Loop

            FicheCER = New Virtualia.TablesObjet.ShemaREF.CER_TRANSACTION
            FicheCER.Ide_Dossier = IdeCLef
            FicheCER.Donnees_Significatives = DonneesSignifiantes

            Empreinte = New Virtualia.TablesObjet.ShemaREF.CER_EMPREINTE
            Empreinte.PointeurChainage = WsChainageOrigine
            Empreinte.Ide_Dossier = Ide
            Empreinte.PointdeVue = WsPointdeVue
            Empreinte.NumObjet = NoObjet
            Empreinte.DateTransaction = WsParent.VirRhDates.DateduJour
            Empreinte.HeureTransaction = Strings.Format(System.DateTime.Now, "HH:mm:ss.ff")
            Empreinte.ModuleOrigine = WsModuleOrigine
            Empreinte.ProcedureOrigine = WsProcedureOrigine
            If WsSessionUti Is Nothing Then
                Empreinte.NomEmetteur = WsParent.VirNomUtilisateur
                Dim SysOutils As Virtualia.Systeme.Outils.Ordinateur
                SysOutils = New Virtualia.Systeme.Outils.Ordinateur
                Empreinte.IPEmetteur = SysOutils.AdresseIPOrdinateur
                Empreinte.MachineEmetteur = SysOutils.NomDNSOrdinateur
            Else
                Empreinte.NomEmetteur = WsSessionUti.V_NomdeConnexion
                Empreinte.IPEmetteur = WsSessionUti.ID_AdresseIP
                Empreinte.MachineEmetteur = WsSessionUti.ID_Machine
            End If
            Empreinte.IPServeur = WsParent.VirServiceServeur.IPServeur
            Empreinte.MachineServeur = WsParent.VirServiceServeur.NomDNSServeur
            FicheCER.ObjetEmpreinte = Empreinte

            If FicheCER.Crypter(WsParent.ChaineModeleCryptage) = False Then
                Return ""
            End If
            If WsParent.VirServiceServeur.MiseAjour_Fiche(WsParent.VirNomUtilisateur, 41, 2, FicheCER.Ide_Dossier, "C", "", FicheCER.ContenuTable, False) = True Then
                Try
                    Return WsParent.VirRhFonction.ChaineCryptee(CStr(FicheCER.Ide_Dossier), WsParent.ClefModeleCryptage)
                Catch ex As Exception
                    Return ""
                End Try
            End If
            Return ""
        End Function

        Public Function LireTransactionCertifiee(ByVal NoObjet As Integer, ByVal NumeroTransaction As String) As Virtualia.TablesObjet.ShemaREF.CER_TRANSACTION
            Dim IdeClef As Integer
            Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim FicheCER As Virtualia.TablesObjet.ShemaREF.CER_TRANSACTION

            Try
                IdeClef = CInt(WsParent.VirRhFonction.ChaineDecryptee(NumeroTransaction, WsParent.ClefModeleCryptage))
            Catch ex As Exception
                Return Nothing
            End Try
            LstRes = WsParent.VirServiceServeur.LectureObjet_ToFiches(WsParent.VirNomUtilisateur, 41, 2, IdeClef, False)
            If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                Return Nothing
            End If
            FicheCER = CType(LstRes.Item(0), Virtualia.TablesObjet.ShemaREF.CER_TRANSACTION)
            If FicheCER.Decrypter(WsParent.ChaineModeleCryptage) = False Then
                Return Nothing
            End If
            If FicheCER.ObjetEmpreinte.SiValide = False Then
                Return Nothing
            End If
            If FicheCER.ObjetEmpreinte.NumObjet <> NoObjet Then
                Return Nothing
            End If
            Return FicheCER
        End Function

        Public Function LireTransactionCertifiee(ByVal IdeClef As Integer) As Virtualia.TablesObjet.ShemaREF.CER_TRANSACTION
            Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim FicheCER As Virtualia.TablesObjet.ShemaREF.CER_TRANSACTION

            LstRes = WsParent.VirServiceServeur.LectureObjet_ToFiches(WsParent.VirNomUtilisateur, 41, 2, IdeClef, False)
            If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                Return Nothing
            End If
            FicheCER = CType(LstRes.Item(0), Virtualia.TablesObjet.ShemaREF.CER_TRANSACTION)
            If FicheCER.Decrypter(WsParent.ChaineModeleCryptage) = False Then
                Return Nothing
            End If
            If FicheCER.ObjetEmpreinte.SiValide = False Then
                Return Nothing
            End If
            Return FicheCER
        End Function

        Public Function ListeTransactions(ByVal NumeroTransaction As String) As List(Of Virtualia.TablesObjet.ShemaREF.CER_TRANSACTION)
            Dim IdeDossier As Integer
            Dim FicheREF As Virtualia.TablesObjet.ShemaREF.CER_TRANSACTION
            Dim LstCER As List(Of Virtualia.TablesObjet.ShemaREF.CER_TRANSACTION)

            Try
                IdeDossier = CInt(WsParent.VirRhFonction.ChaineDecryptee(NumeroTransaction, WsParent.ClefModeleCryptage))
            Catch ex As Exception
                Return Nothing
            End Try
            LstCER = New List(Of Virtualia.TablesObjet.ShemaREF.CER_TRANSACTION)
            Do
                FicheREF = LireTransactionCertifiee(IdeDossier)
                If FicheREF Is Nothing Then
                    Exit Do
                End If
                LstCER.Add(FicheREF)
                If FicheREF.ObjetEmpreinte.PointeurChainage = 0 Or FicheREF.ObjetEmpreinte.PointeurChainage = FicheREF.Ide_Dossier Then
                    Exit Do
                End If
                IdeDossier = FicheREF.ObjetEmpreinte.PointeurChainage
            Loop
            Return LstCER
        End Function

        Public Property PointdeVue As Integer
            Get
                Return WsPointdeVue
            End Get
            Set(value As Integer)
                WsPointdeVue = value
            End Set
        End Property

        Public WriteOnly Property ChainageOrigine(ByVal NoSession As String, ByVal ProcedureOrigine As String) As String
            Set(value As String)
                WsSessionUti = WsParent.ItemSessionModule(NoSession)
                WsProcedureOrigine = ProcedureOrigine
                If WsProcedureOrigine = "" Then
                    WsProcedureOrigine = "?"
                End If
                WsSessionUti = WsParent.ItemSessionModule(NoSession)
                If value <> "" Then
                    Try
                        WsChainageOrigine = CInt(WsParent.VirRhFonction.ChaineDecryptee(value, WsParent.ClefModeleCryptage))
                    Catch ex As Exception
                        WsChainageOrigine = 0
                    End Try
                End If
            End Set
        End Property

        Public Sub New(ByVal Host As ObjetGlobalBase)
            WsParent = Host
            WsModuleOrigine = WsParent.VirNomModule
            If WsModuleOrigine = "" Then
                WsModuleOrigine = "Virtualia.Net"
            End If
        End Sub

    End Class
End Namespace