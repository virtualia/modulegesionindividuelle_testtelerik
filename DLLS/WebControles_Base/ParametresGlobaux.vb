﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Microsoft.VisualBasic
Imports Virtualia.Ressources.Datas
Namespace Session
    Public Class ParametresGlobaux
        Private WsParent As Virtualia.Net.Session.ObjetGlobalBase
        '** Niveaux affectations fonctionnelles
        Private WsTabLibelNiveaux As List(Of String) = Nothing
        '** Compteurs Congés
        Private WsTabLibelCompteurs As List(Of String) = Nothing
        '** Discriminants des menus du sytème de référence
        Private WsObjetDistingo As ObjetDiscriminants
        '** Liste de valeurs
        Private WsListeNatureAbsence_DRH As List(Of String) = Nothing

        Public ReadOnly Property Email_Etablissement(ByVal Ide As Integer) As String
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim Resultat As List(Of String)
                Dim IndiceA As Integer
                Dim TableauData(0) As String

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsParent.VirModele, WsParent.VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueEtablissement, "", "", VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.SiHistoriquedeSituation = False
                Constructeur.NoInfoSelection(0, 1) = 19
                Constructeur.InfoExtraite(0, 1, 0) = 1 'l'intitulé
                Constructeur.InfoExtraite(1, 1, 0) = 19 'l'Email

                Resultat = WsParent.VirServiceServeur.RequeteSql_ToListeChar(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueEtablissement, 1, Constructeur.OrdreSqlDynamique)

                Constructeur = Nothing

                If Resultat Is Nothing Then
                    Return ""
                End If
                For IndiceA = 0 To Resultat.Count - 1
                    TableauData = Strings.Split(Resultat(IndiceA), VI.Tild)
                    If IsNumeric(TableauData(0)) Then
                        If CInt(TableauData(0)) = Ide Then
                            Return TableauData(2)
                        End If
                    End If
                Next IndiceA
                Return ""
            End Get
        End Property

        '** Objet Discriminant
        Public ReadOnly Property PointeurDistingo As ObjetDiscriminants
            Get
                If WsObjetDistingo Is Nothing Then
                    Try
                        WsObjetDistingo = New ObjetDiscriminants(WsParent.VirNomUtilisateur)
                    Catch Ex As Exception
                        WsObjetDistingo = Nothing
                    End Try
                End If
                Return WsObjetDistingo
            End Get
        End Property

        Public ReadOnly Property ListeNatureAbsence_DRH As List(Of String)
            Get
                If WsListeNatureAbsence_DRH Is Nothing Then
                    Dim LstTabRessource As List(Of Virtualia.TablesObjet.ShemaREF.ABS_ABSENCE) = Nothing
                    LstTabRessource = WsParent.VirInstanceReferentiel.PointeurAbsences.ListeDesAbsences
                    WsListeNatureAbsence_DRH = New List(Of String)
                    If LstTabRessource IsNot Nothing Then
                        For IndiceI = 0 To LstTabRessource.Count - 1
                            If LstTabRessource.Item(IndiceI).Categorie <> "Maladie / Maternité" AndAlso _
                                Strings.InStr(LstTabRessource.Item(IndiceI).Intitule, "Accident") = 0 Then
                                WsListeNatureAbsence_DRH.Add(LstTabRessource.Item(IndiceI).Intitule)
                            End If
                        Next IndiceI
                    End If
                End If
                Return WsListeNatureAbsence_DRH
            End Get
        End Property

        Public ReadOnly Property NombreNiveaux_Organigramme() As Integer
            Get
                If WsParent.VirConfiguration Is Nothing Then
                    Return 2
                End If
                If WsParent.VirConfiguration.Parametres_Annuaire.LesLabels Is Nothing Then
                    Return 2
                End If
                Return WsParent.VirConfiguration.Parametres_Annuaire.LesLabels.Count
            End Get
        End Property

        Public ReadOnly Property LibelleNiveau_Organigramme(ByVal Index As Integer) As String
            Get
                If WsTabLibelNiveaux Is Nothing Then
                    WsTabLibelNiveaux = New List(Of String)
                    If WsParent.VirConfiguration Is Nothing Then
                        WsTabLibelNiveaux.Add("Direction")
                        WsTabLibelNiveaux.Add("Service")
                    Else
                        WsTabLibelNiveaux = WsParent.VirConfiguration.Parametres_Annuaire.LesLabels
                    End If
                End If
                Select Case Index
                    Case 0 To WsTabLibelNiveaux.Count - 1
                        Return WsTabLibelNiveaux(Index)
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property LibelleCompteur_Conge(ByVal Index As Integer) As String
            Get
                If WsParent.VirConfiguration Is Nothing Then
                    Return ""
                End If
                If WsTabLibelCompteurs Is Nothing Then
                    WsTabLibelCompteurs = New List(Of String)
                    WsTabLibelCompteurs.Add(WsParent.VirConfiguration.Parametres_TempsTravail.Compteur_CA)
                    WsTabLibelCompteurs.Add(WsParent.VirConfiguration.Parametres_TempsTravail.Compteur_RTT)
                    WsTabLibelCompteurs.Add(WsParent.VirConfiguration.Parametres_TempsTravail.Compteur_Additionnel(1))
                    WsTabLibelCompteurs.Add(WsParent.VirConfiguration.Parametres_TempsTravail.Compteur_Additionnel(2))
                    WsTabLibelCompteurs.Add(WsParent.VirConfiguration.Parametres_TempsTravail.Compteur_Additionnel(3))
                    WsTabLibelCompteurs.Add(WsParent.VirConfiguration.Parametres_TempsTravail.Compteur_Additionnel(4))
                    WsTabLibelCompteurs.Add(WsParent.VirConfiguration.Parametres_TempsTravail.Compteur_Additionnel(5))
                End If
                Select Case Index
                    Case 0 To WsTabLibelCompteurs.Count - 1
                        Return WsTabLibelCompteurs(Index)
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public Sub Actualiser()
            If WsObjetDistingo IsNot Nothing Then
                WsObjetDistingo = Nothing
            End If
            Dim CollVirtualia As Object
            CollVirtualia = PointeurDistingo
        End Sub

        Public Sub New(ByVal host As Virtualia.Net.Session.ObjetGlobalBase)
            WsParent = host
            Call Actualiser()
        End Sub
    End Class
End Namespace

