﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Namespace Session
    Public Class ObjetGlobalBase
        Private WsClientSessionWcf As Virtualia.Net.WebService.ServiceWcfSessions = Nothing
        Private WsClientServiceWcf As Virtualia.Net.WebService.ServiceWcfServeur = Nothing
        Private WsClientServiceWeb As Virtualia.Net.WebService.ServiceWebServeur = Nothing
        Private ModeFonctionnement As String = System.Configuration.ConfigurationManager.AppSettings("ModeVirtualia")

        Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates
        Private WsRhFct As Virtualia.Systeme.Fonctions.Generales
        Private WsRhModele As Donnees.ModeleRH
        Private WsRhShemaPER As Virtualia.TablesObjet.ShemaPER.VConstructeur
        Private WsRhShemaREF As Virtualia.TablesObjet.ShemaREF.VConstructeur
        Private WsInstanceSgbd As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbd
        Private WsRhPartition As Virtualia.Metier.Expertes.Partition = Nothing
        Private WsRhExpertes As Virtualia.Systeme.MetaModele.Expertes.ExpertesRH
        Private WsObjetCalcJour As Virtualia.Ressources.Calculs.ObjetCalculJour
        Private WsUrlImageArmoire As List(Of String)
        Private WsConfigVirtualia As Virtualia.Systeme.Configuration.FichierConfig
        Private WsCouleursPlanning As Virtualia.Systeme.Planning.CouleursJour

        Private WsModeConnexion As String = ""
        Private WsNomUtilisateurBd As String = ""
        Private WsNoBd As Integer
        Private WsRepertoireVirtualia As String = System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia")

        Private WsListeObjetsDico As List(Of Outils.FicheObjetDictionnaire) = Nothing
        Private WsListeInfosDico As List(Of Outils.FicheInfoDictionnaire) = Nothing
        Private WsListeExpertesDico As List(Of Outils.FicheInfoExperte) = Nothing

        Private WsUrlWebOutil As String = ""
        Private WsParametres As Virtualia.Net.Session.ParametresGlobaux

        '** Exécution
        Private WsNomModule As String = "Virtualia"
        Private WsIndexInitialisation As Integer
        Private WsListeSessions As List(Of Virtualia.Net.Session.ObjetSession)
        Private WsListeCompletePER As List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
        Private WsEnsembleREF As Virtualia.Ressources.Datas.EnsembleReferences

        Public ReadOnly Property VirClientSessionWcf As Virtualia.Net.WebService.ServiceWcfSessions
            Get
                If WsClientSessionWcf Is Nothing Then
                    Try
                        WsClientSessionWcf = New Virtualia.Net.WebService.ServiceWcfSessions
                    Catch ex As Exception
                        Return Nothing
                    End Try
                End If
                Return WsClientSessionWcf
            End Get
        End Property

        Public ReadOnly Property VirServiceServeur As Virtualia.Net.WebService.IServiceServeur
            Get
                Select Case ModeFonctionnement
                    Case "WCF"
                        If WsClientServiceWcf Is Nothing Then
                            If WsRhModele Is Nothing Then
                                WsClientServiceWcf = New Virtualia.Net.WebService.ServiceWcfServeur
                            Else
                                WsClientServiceWcf = New Virtualia.Net.WebService.ServiceWcfServeur(WsRhModele.InstanceProduit.ClefModele)
                            End If
                        End If
                        Return WsClientServiceWcf
                    Case Else
                        If WsClientServiceWeb Is Nothing Then
                            If WsRhModele Is Nothing Then
                                WsClientServiceWeb = New Virtualia.Net.WebService.ServiceWebServeur(System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaServeur"))
                            Else
                                WsClientServiceWeb = New Virtualia.Net.WebService.ServiceWebServeur(System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaServeur"), WsRhModele.InstanceProduit.ClefModele)
                            End If
                        End If
                        Return WsClientServiceWeb
                End Select
            End Get
        End Property

        Public ReadOnly Property PointeurDllExpert As Virtualia.Metier.Expertes.Partition
            Get
                If WsRhPartition Is Nothing Then
                    WsRhPartition = New Virtualia.Metier.Expertes.Partition(VirModele, VirInstanceBd)
                    WsRhPartition.NomUtilisateur = VirNomUtilisateur
                    WsRhPartition.PreselectiondeDossiers = Nothing
                End If
                Return WsRhPartition
            End Get
        End Property

        Public ReadOnly Property PointeurParametresGlobaux As Virtualia.Net.Session.ParametresGlobaux
            Get
                If WsParametres Is Nothing Then
                    WsParametres = New Virtualia.Net.Session.ParametresGlobaux(Me)
                End If
                Return WsParametres
            End Get
        End Property

        Public ReadOnly Property ObjetCalcJour As Virtualia.Ressources.Calculs.ObjetCalculJour
            Get
                If WsObjetCalcJour Is Nothing Then
                    WsObjetCalcJour = New Virtualia.Ressources.Calculs.ObjetCalculJour(WsNomUtilisateurBd, Nothing)
                End If
                Return WsObjetCalcJour
            End Get
        End Property

        Public ReadOnly Property InstanceCouleur As Virtualia.Systeme.Planning.CouleursJour
            Get
                If WsCouleursPlanning IsNot Nothing AndAlso WsCouleursPlanning.Count > 0 Then
                    Return WsCouleursPlanning
                End If
                Dim LstRef As List(Of Virtualia.TablesObjet.ShemaREF.PAI_COULEURS) = Nothing
                Dim ToutUnObjet As Virtualia.Ressources.Datas.EnsembleFiches
                ToutUnObjet = New Virtualia.Ressources.Datas.EnsembleFiches(VirNomUtilisateur, VI.PointdeVue.PVuePaie)
                ToutUnObjet.NumeroObjet(False) = 24
                If ToutUnObjet.ListedesFiches IsNot Nothing Then
                    LstRef = New List(Of Virtualia.TablesObjet.ShemaREF.PAI_COULEURS)
                    For IndiceI = 0 To ToutUnObjet.ListedesFiches.Count - 1
                        LstRef.Add(CType(ToutUnObjet.ListedesFiches.Item(IndiceI), Virtualia.TablesObjet.ShemaREF.PAI_COULEURS))
                    Next IndiceI
                End If
                WsCouleursPlanning = New Virtualia.Systeme.Planning.CouleursJour
                If LstRef Is Nothing OrElse LstRef.Count = 0 Then
                    Call WsCouleursPlanning.Initialiser_Defaut()
                Else
                    Dim HexaCouleur As String
                    Dim ColStocke As String
                    Dim IndiceI As Integer
                    Dim LstValeurs As List(Of String)

                    For IndiceI = 0 To LstRef.Count - 1
                        HexaCouleur = Hex(LstRef.Item(IndiceI).Couleur)
                        Select Case HexaCouleur.Length
                            Case Is = 6
                                ColStocke = "#" & Strings.Right(HexaCouleur, 2) & Strings.Mid(HexaCouleur, 3, 2) & Strings.Left(HexaCouleur, 2)
                            Case Is = 4
                                ColStocke = "#" & Strings.Right(HexaCouleur, 2) & Strings.Left(HexaCouleur, 2) & "00"
                            Case Else
                                ColStocke = "#" & HexaCouleur & "0000"
                        End Select
                        LstValeurs = New List(Of String)
                        If LstRef.Item(IndiceI).Valeur_N1 <> "" Then
                            LstValeurs.Add(LstRef.Item(IndiceI).Valeur_N1)
                        End If
                        If LstRef.Item(IndiceI).Valeur_N2 <> "" Then
                            LstValeurs.Add(LstRef.Item(IndiceI).Valeur_N2)
                        End If
                        If LstRef.Item(IndiceI).Valeur_N3 <> "" Then
                            LstValeurs.Add(LstRef.Item(IndiceI).Valeur_N3)
                        End If
                        If LstRef.Item(IndiceI).Valeur_N4 <> "" Then
                            LstValeurs.Add(LstRef.Item(IndiceI).Valeur_N4)
                        End If
                        If LstRef.Item(IndiceI).Valeur_N5 <> "" Then
                            LstValeurs.Add(LstRef.Item(IndiceI).Valeur_N5)
                        End If
                        If LstRef.Item(IndiceI).Valeur_N6 <> "" Then
                            LstValeurs.Add(LstRef.Item(IndiceI).Valeur_N6)
                        End If
                        If LstRef.Item(IndiceI).Valeur_N7 <> "" Then
                            LstValeurs.Add(LstRef.Item(IndiceI).Valeur_N7)
                        End If
                        WsCouleursPlanning.AjouterItem(LstRef.Item(IndiceI).Etiquette, ColStocke, LstValeurs)
                    Next IndiceI
                End If
                Return WsCouleursPlanning
            End Get
        End Property

        Public ReadOnly Property AjouterUneSessionModule(ByVal NoSession As String, ByVal NomUtilisateur As String, _
                                 ByVal FiltreEtablissement As String, ByVal FiltreSqlV3 As String) As Virtualia.Net.Session.ObjetSession
            Get
                Dim NouveauUser As Virtualia.Net.Session.ObjetSession

                If WsListeSessions Is Nothing Then
                    WsListeSessions = New List(Of Virtualia.Net.Session.ObjetSession)
                End If
                If ItemSessionModule(NoSession) IsNot Nothing Then
                    Return ItemSessionModule(NoSession)
                End If
                NouveauUser = New Virtualia.Net.Session.ObjetSession(Me, NomUtilisateur, 0, FiltreEtablissement, FiltreSqlV3)
                NouveauUser.V_IDSession = NoSession
                WsListeSessions.Add(NouveauUser)
                Return NouveauUser
            End Get
        End Property

        Public ReadOnly Property ItemSessionModule(ByVal NoSession As String) As Virtualia.Net.Session.ObjetSession
            Get
                If WsListeSessions Is Nothing Then
                    Return Nothing
                End If
                Return WsListeSessions.Find(Function(Recherche) Recherche.V_IDSession = NoSession)
            End Get
        End Property
        Public ReadOnly Property ItemSessionParLogin(ByVal LoginLdap As String) As Virtualia.Net.Session.ObjetSession
            Get
                If LoginLdap = "" Then
                    Return Nothing
                End If
                If WsListeSessions Is Nothing Then
                    Return Nothing
                End If
                Return WsListeSessions.Find(Function(Recherche) Recherche.V_NomdeConnexion = LoginLdap)
            End Get
        End Property
        Public Sub LibererSessionModule(ByVal NoSession As String)
            Dim Uti As Virtualia.Net.Session.ObjetSession = ItemSessionModule(NoSession)
            If Uti Is Nothing Then
                Exit Sub
            End If
            WsListeSessions.Remove(Uti)
        End Sub

        Public Property VirIndexInit As Integer
            Get
                Return WsIndexInitialisation
            End Get
            Set(value As Integer)
                WsIndexInitialisation = value
            End Set
        End Property

        Public ReadOnly Property UrlWebOutil As String
            Get
                If WsUrlWebOutil = "" Then
                    WsUrlWebOutil = System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaUtils")
                End If
                If WsUrlWebOutil = "" And VirClientSessionWcf IsNot Nothing Then
                    WsUrlWebOutil = VirClientSessionWcf.Url_WebService_Outil
                End If
                Return WsUrlWebOutil
            End Get
        End Property

        Public ReadOnly Property UrlDestination(ByVal NoSession As String, ByVal Index As Integer, ByVal Param As String) As String
            Get
                If VirClientSessionWcf Is Nothing Then
                    Return ""
                End If
                Return VirClientSessionWcf.Url_Applicative(NoSession, Index, Param)
            End Get
        End Property

        Public ReadOnly Property VirConfiguration As Virtualia.Systeme.Configuration.FichierConfig
            Get
                If WsConfigVirtualia Is Nothing Then
                    WsConfigVirtualia = VirServiceServeur.Instance_ConfigV4
                End If
                Return WsConfigVirtualia
            End Get
        End Property

        Public ReadOnly Property VirListeCompletePER As List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
            Get
                If WsListeCompletePER Is Nothing OrElse WsListeCompletePER.Count = 0 Then
                    Call InitialiserArmoireComplete(VirNomUtilisateur)
                End If
                Return WsListeCompletePER
            End Get
        End Property

        Public ReadOnly Property GEN_DossierPER(ByVal Ide As Integer, Optional ByVal UtiConnecte As String = "") As Virtualia.Ressources.Datas.ObjetDossierPER
            Get
                If Ide = 0 Then
                    Return Nothing
                End If
                If WsListeCompletePER Is Nothing OrElse WsListeCompletePER.Count = 0 Then
                    Call InitialiserArmoireComplete(UtiConnecte)
                End If
                If WsListeCompletePER Is Nothing Then
                    Return Nothing
                End If
                Return WsListeCompletePER.Find(Function(Recherche) Recherche.V_Identifiant = Ide)
            End Get
        End Property

        Public ReadOnly Property VirInstanceReferentiel As Virtualia.Ressources.Datas.EnsembleReferences
            Get
                If WsEnsembleREF Is Nothing Then
                    WsEnsembleREF = New Virtualia.Ressources.Datas.EnsembleReferences(WsNomUtilisateurBd)
                End If
                Return WsEnsembleREF
            End Get
        End Property

        Public Property VirModeConnexxion As String
            Get
                If WsModeConnexion = "" Then
                    WsModeConnexion = "V4"
                End If
                Return WsModeConnexion
            End Get
            Set(value As String)
                WsModeConnexion = value
            End Set
        End Property

        Public ReadOnly Property NouveauDossier(ByVal Nom As String, ByVal Prenom As String, ByVal DateNai As String) As Virtualia.Ressources.Datas.ObjetDossierPER
            Get
                Dim PerDossier As Virtualia.Ressources.Datas.ObjetDossierPER
                Dim Ide As Integer

                Try
                    Ide = NouvelIdentifiant("PER_ETATCIVIL")
                    PerDossier = New Virtualia.Ressources.Datas.ObjetDossierPER(VirModele, Ide)
                    PerDossier.Nom = Nom
                    PerDossier.Prenom = Prenom
                    PerDossier.Date_de_Naissance = DateNai

                    PerDossier.V_ActualiserUnObjet(VirNomUtilisateur, "PER_ETATCIVIL") = VI.ObjetPer.ObaCivil

                    WsListeCompletePER.Add(PerDossier)

                    Return PerDossier
                Catch ex As Exception
                    Return Nothing
                End Try
            End Get
        End Property

        Public ReadOnly Property NouvelIdentifiant(ByVal NomTableSgbd As String) As Integer
            Get
                Return VirServiceServeur.ObtenirUnCompteur(VirNomUtilisateur, NomTableSgbd, "Max") + 1
            End Get
        End Property

        Public ReadOnly Property VirListeObjetsDico() As List(Of Outils.FicheObjetDictionnaire)
            Get
                If WsListeObjetsDico Is Nothing Then
                    WsListeObjetsDico = New List(Of Outils.FicheObjetDictionnaire)
                    Dim FicheDico As Outils.FicheObjetDictionnaire
                    For Each PtdeVue In VirModele
                        For Each Objet In PtdeVue
                            FicheDico = New Outils.FicheObjetDictionnaire(Objet)
                            WsListeObjetsDico.Add(FicheDico)
                            FicheDico.VIndex = WsListeObjetsDico.Count
                        Next
                    Next
                End If
                Return WsListeObjetsDico
            End Get
        End Property

        Public ReadOnly Property VirRhShemaPER As Virtualia.TablesObjet.ShemaPER.VConstructeur
            Get
                If WsRhShemaPER Is Nothing Then
                    WsRhShemaPER = New Virtualia.TablesObjet.ShemaPER.VConstructeur(VirModele.InstanceProduit.ClefModele)
                    If IsNumeric(System.Configuration.ConfigurationManager.AppSettings("Particularite")) Then
                        WsRhShemaPER.V_Parametre = CInt(System.Configuration.ConfigurationManager.AppSettings("Particularite"))
                    End If
                End If
                Return WsRhShemaPER
            End Get
        End Property
        Public ReadOnly Property VirRhShemaREF(ByVal PtdeVue As Integer) As Virtualia.TablesObjet.ShemaREF.VConstructeur
            Get
                WsRhShemaREF = New Virtualia.TablesObjet.ShemaREF.VConstructeur(VirModele.InstanceProduit.ClefModele, PtdeVue)
                Return WsRhShemaREF
            End Get
        End Property

        Public ReadOnly Property VirListeInfosDico() As List(Of Outils.FicheInfoDictionnaire)
            Get
                If WsListeInfosDico Is Nothing Then
                    WsListeInfosDico = New List(Of Outils.FicheInfoDictionnaire)
                    Dim FicheDico As Outils.FicheInfoDictionnaire
                    For Each PtdeVue In VirModele
                        For Each Objet In PtdeVue
                            For Each Info In Objet
                                FicheDico = New Outils.FicheInfoDictionnaire(Info)
                                WsListeInfosDico.Add(FicheDico)
                                FicheDico.VIndex = WsListeInfosDico.Count
                            Next
                        Next
                    Next
                End If
                Return WsListeInfosDico
            End Get
        End Property

        Public ReadOnly Property VirListeExpertesDico() As List(Of Outils.FicheInfoExperte)
            Get
                If WsListeExpertesDico Is Nothing Then
                    WsListeExpertesDico = New List(Of Outils.FicheInfoExperte)
                    Dim FicheDico As Outils.FicheInfoExperte
                    For Each PtdeVue In VirExpertes
                        For Each Objet In PtdeVue
                            For Each Info In Objet
                                FicheDico = New Outils.FicheInfoExperte(Info)
                                WsListeExpertesDico.Add(FicheDico)
                                FicheDico.VIndex = WsListeExpertesDico.Count
                            Next
                        Next
                    Next
                End If
                Return WsListeExpertesDico
            End Get
        End Property

        Public ReadOnly Property VirExpertes() As Expertes.ExpertesRH
            Get
                Return WsRhExpertes
            End Get
        End Property

        Public ReadOnly Property VirNomUtilisateur As String
            Get
                Return WsNomUtilisateurBd
            End Get
        End Property

        Public ReadOnly Property VirModele As Donnees.ModeleRH
            Get
                Return WsRhModele
            End Get
        End Property

        Public ReadOnly Property ChaineModeleCryptage As String
            Get
                Return WsRhModele.InstanceProduit.ClefCryptage
            End Get
        End Property
        Public ReadOnly Property ClefModeleCryptage As Byte()
            Get
                Try
                    Return Convert.FromBase64String(WsRhModele.InstanceProduit.ClefCryptage)
                Catch ex As Exception
                    Return Nothing
                End Try
            End Get
        End Property

        Public ReadOnly Property VirRepertoire As String
            Get
                Return WsRepertoireVirtualia
            End Get
        End Property

        Public ReadOnly Property VirRepertoireTemporaire As String
            Get
                Dim RepTemp As String = System.Configuration.ConfigurationManager.AppSettings("RepertoireTemporaire")
                If RepTemp Is Nothing OrElse RepTemp = "" Then
                    RepTemp = WsRepertoireVirtualia & "\Temp\" & VirNomModule
                End If
                If My.Computer.FileSystem.DirectoryExists(RepTemp) = False Then
                    Try
                        My.Computer.FileSystem.CreateDirectory(RepTemp)
                    Catch ex As Exception
                        Exit Try
                    End Try
                End If
                Return RepTemp
            End Get
        End Property

        Public ReadOnly Property VirNoBd As Integer
            Get
                Return WsNoBd
            End Get
        End Property

        Public ReadOnly Property VirSgbd As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbd
            Get
                Return WsInstanceSgbd
            End Get
        End Property

        Public ReadOnly Property VirInstanceBd As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdDatabase
            Get
                Dim IndiceI As Integer

                For IndiceI = 0 To VirSgbd.NombredeDatabases - 1
                    Select Case VirSgbd.Item(IndiceI).Numero
                        Case Is = WsNoBd
                            Return VirSgbd.Item(IndiceI)
                    End Select
                Next IndiceI
                Return Nothing
            End Get
        End Property

        Public Property VirNomModule As String
            Get
                Return WsNomModule
            End Get
            Set(value As String)
                WsNomModule = value
            End Set
        End Property

        Public ReadOnly Property VirUrlImageArmoire(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To WsUrlImageArmoire.Count - 1
                        Return WsUrlImageArmoire(Index)
                End Select
                Return WsUrlImageArmoire(0)
            End Get
        End Property

        Public ReadOnly Property VirUrlImageArmoire(ByVal Valeur As String) As String
            Get
                Dim IndiceI As Integer
                For IndiceI = 0 To 15
                    Select Case WsUrlImageArmoire(IndiceI)
                        Case Is = Valeur
                            If WsUrlImageArmoire(IndiceI + 16) IsNot Nothing Then
                                Return WsUrlImageArmoire(IndiceI + 16)
                            End If
                    End Select
                Next IndiceI
                Return WsUrlImageArmoire(0)
            End Get
        End Property

        Public ReadOnly Property VirRhDates As Virtualia.Systeme.Fonctions.CalculDates
            Get
                Return WsRhDates
            End Get
        End Property

        Public ReadOnly Property VirRhFonction As Virtualia.Systeme.Fonctions.Generales
            Get
                Return WsRhFct
            End Get
        End Property

        Public ReadOnly Property SelectionDistincte(ByVal Pvue As Integer, ByVal NoObjet As Integer, ByVal NomTable As String, ByVal Champ As String) As List(Of String)
            Get
                Dim OrdreSql As String
                OrdreSql = "SELECT DISTINCT " & Champ & " FROM " & NomTable & " ORDER BY " & Champ
                Return VirServiceServeur.RequeteSql_ToListeChar(VirNomUtilisateur, Pvue, NoObjet, OrdreSql)
            End Get
        End Property

        Public ReadOnly Property SelectionDynamique(ByVal Pvue As Integer, ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal OpeLiaison As Integer, _
                                                    ByVal OpeComparaison As Integer, ByVal ConditionValeurs As String, _
                                                    ByVal DateDebut As String, ByVal DateFin As String, Optional ByVal UtiConnecte As String = "") As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModele, VirInstanceBd)
                Constructeur.NombredeRequetes(Pvue, DateDebut, DateFin, VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.NoInfoSelection(0, NoObjet) = NoInfo
                Select Case ConditionValeurs
                    Case Is <> ""
                        Constructeur.ValeuraComparer(0, OpeLiaison, OpeComparaison, False) = ConditionValeurs
                End Select
                Constructeur.InfoExtraite(0, NoObjet, 0) = NoInfo

                If UtiConnecte = "" Then
                    UtiConnecte = VirNomUtilisateur
                End If
                Return VirServiceServeur.RequeteSql_ToListeType(UtiConnecte, Pvue, NoObjet, Constructeur.OrdreSqlDynamique)

            End Get
        End Property

        Public ReadOnly Property SelectionObjetDate(ByVal LstIde As List(Of Integer), ByVal DateDebut As String, ByVal DateFin As String, ByVal NoObjet As Integer, Optional ByVal Filtre As String = "",
                                                    Optional ByVal LstFiltres As List(Of String) = Nothing, Optional ByVal UtiConnecte As String = "") As List(Of VIR_FICHE)
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim IndiceI As Integer
                Dim LimiteObjet As Integer

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModele, VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, DateDebut, DateFin, VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.SiHistoriquedeSituation = True
                Constructeur.IdentifiantDossier = 0
                If LstIde IsNot Nothing Then
                    If LstIde.Count = 1 Then
                        Constructeur.IdentifiantDossier = LstIde.Item(0)
                    Else
                        Constructeur.PreselectiondIdentifiants = LstIde
                    End If
                Else
                    Constructeur.PreselectiondIdentifiants = Nothing
                End If
                For Each VirObjet In VirListeObjetsDico
                    If VirObjet.PointdeVue = VI.PointdeVue.PVueApplicatif And VirObjet.Objet = NoObjet Then
                        LimiteObjet = VirObjet.PointeurModele.LimiteMaximum
                        If NoObjet = VI.ObjetPer.ObaGrade Then
                            LimiteObjet -= 2
                        End If
                        Exit For
                    End If
                Next
                If LimiteObjet = 0 Then
                    Return Nothing
                End If
                If Filtre = "" Then
                    Constructeur.NoInfoSelection(0, NoObjet) = 0
                    Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Inclu, False) = DateDebut & VI.PointVirgule & DateFin
                Else
                    Constructeur.NoInfoSelection(0, NoObjet) = 1
                    Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Filtre
                End If
                If LstFiltres IsNot Nothing Then
                    For IndiceI = 0 To LstFiltres.Count - 1
                        Constructeur.ClauseFiltreIN = LstFiltres(IndiceI)
                    Next IndiceI
                End If
                For IndiceI = 0 To LimiteObjet
                    Constructeur.InfoExtraite(IndiceI, NoObjet, 0) = IndiceI
                Next IndiceI
                If UtiConnecte = "" Then
                    UtiConnecte = VirNomUtilisateur
                End If

                Return VirServiceServeur.RequeteSql_ToFiches(UtiConnecte, VI.PointdeVue.PVueApplicatif, NoObjet, Constructeur.OrdreSqlDynamique)

            End Get

        End Property

        Public ReadOnly Property SelectionObjetDate(ByVal PtdeVue As Integer, ByVal NoObjet As Integer, ByVal DateDebut As String,
                                                       ByVal DateFin As String, ByVal LstIde As List(Of Integer)) As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim IndiceI As Integer
                Dim LimiteObjet As Integer

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModele, VirInstanceBd)
                Constructeur.NombredeRequetes(PtdeVue, DateDebut, DateFin, VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.SiHistoriquedeSituation = True
                Constructeur.IdentifiantDossier = 0
                If LstIde IsNot Nothing Then
                    If LstIde.Count = 1 Then
                        Constructeur.IdentifiantDossier = LstIde.Item(0)
                    Else
                        Constructeur.PreselectiondIdentifiants = LstIde
                    End If
                Else
                    Constructeur.PreselectiondIdentifiants = Nothing
                End If
                For Each VirObjet In VirListeObjetsDico
                    If VirObjet.PointdeVue = PtdeVue And VirObjet.Objet = NoObjet Then
                        LimiteObjet = VirObjet.PointeurModele.LimiteMaximum
                        If NoObjet = VI.ObjetPer.ObaGrade Then
                            LimiteObjet -= 2
                        End If
                        Exit For
                    End If
                Next
                If LimiteObjet = 0 Then
                    Return Nothing
                End If
                Constructeur.NoInfoSelection(0, NoObjet) = 0
                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Inclu, False) = DateDebut & VI.PointVirgule & DateFin
                For IndiceI = 0 To LimiteObjet
                    Constructeur.InfoExtraite(IndiceI, NoObjet, 0) = IndiceI
                Next IndiceI
                Return VirServiceServeur.RequeteSql_ToFiches(VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, NoObjet, Constructeur.OrdreSqlDynamique)

            End Get

        End Property

        Public ReadOnly Property SelectionObjetValable(ByVal Ide As Integer, ByVal DateValeur As String, ByVal NoObjet As Integer,
                                                          Optional ByVal ClauseFiltre As String = "") As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim ChaineSql As String
                Dim IndiceI As Integer
                Dim IDebut As Integer = 0
                Dim LimiteObjet As Integer

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsRhModele, VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", DateValeur, VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.SiHistoriquedeSituation = False
                Constructeur.IdentifiantDossier = Ide
                For Each VirObjet In VirListeObjetsDico
                    If VirObjet.PointdeVue = VI.PointdeVue.PVueApplicatif And VirObjet.Objet = NoObjet Then
                        LimiteObjet = VirObjet.PointeurModele.LimiteMaximum
                        Select Case VirObjet.PointeurModele.VNature
                            Case VI.TypeObjet.ObjetSimple, VI.TypeObjet.ObjetTableau, VI.TypeObjet.ObjetMemo
                                IDebut = 1
                        End Select
                        '' AKR 29/01/2018 : ONISEP Probleme coloumn non existant dans la table PER_GRADE
                        'If NoObjet = VI.ObjetPer.ObaGrade Then
                        '    LimiteObjet -= 2
                        'End If
                        Exit For
                    End If
                Next
                If LimiteObjet = 0 Then
                    Return Nothing
                End If
                Select Case NoObjet
                    Case VI.ObjetPer.ObaCivil
                        Constructeur.NoInfoSelection(0, NoObjet) = 2
                    Case VI.ObjetPer.ObaPresence
                        Constructeur.NoInfoSelection(0, NoObjet) = 15
                    Case Else
                        Constructeur.NoInfoSelection(0, NoObjet) = 1
                End Select
                For IndiceI = IDebut To LimiteObjet
                    Constructeur.InfoExtraite(IndiceI, NoObjet, 0) = IndiceI
                Next IndiceI
                If ClauseFiltre <> "" Then
                    Constructeur.ClauseFiltreIN = ClauseFiltre
                End If
                ChaineSql = Constructeur.OrdreSqlDynamique
                Constructeur = Nothing

                Return VirServiceServeur.RequeteSql_ToFiches(VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, NoObjet, ChaineSql)
            End Get
        End Property

        Public Function LireTableSysReference(ByVal NoPvue As Integer, Optional ByVal UtiConnecte As String = "") As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModele, VirInstanceBd)
            Constructeur.NombredeRequetes(NoPvue, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = True
            Constructeur.NoInfoSelection(0, 1) = 1
            Constructeur.InfoExtraite(0, 1, 1) = 1

            If UtiConnecte = "" Then
                UtiConnecte = VirNomUtilisateur
            End If
            Return VirServiceServeur.RequeteSql_ToListeType(UtiConnecte, NoPvue, 1, Constructeur.OrdreSqlDynamique)

        End Function

        Public ReadOnly Property TabledesCategories() As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Get
                Dim Tabliste As New List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
                Dim Categorie As Virtualia.Net.ServiceServeur.VirRequeteType
                Dim I As Integer
                For I = 0 To 11
                    Categorie = New Virtualia.Net.ServiceServeur.VirRequeteType
                    Categorie.Ide_Dossier = 9999
                    Categorie.Valeurs = New List(Of String)
                    Categorie.Valeurs.Add(LibelleCategorie(I))
                    Tabliste.Add(Categorie)
                Next I
                Return Tabliste
            End Get
        End Property

        Public Function LireTableGeneraleSimple(ByVal Intitule As String) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim Resultat As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim IndiceI As Integer

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModele, VirInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueGeneral, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = True
            Constructeur.NoInfoSelection(0, 1) = 1
            Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Intitule
            Constructeur.InfoExtraite(0, 2, 1) = 1
            Constructeur.InfoExtraite(1, 2, 0) = 2

            Resultat = VirServiceServeur.RequeteSql_ToListeType(VirNomUtilisateur, VI.PointdeVue.PVueGeneral, 1, Constructeur.OrdreSqlDynamique)

            If Resultat IsNot Nothing Then
                Constructeur = Nothing
                Return Resultat
            End If

            '*** Si Aucune valeur et la table générale n'existe pas alors Création de celle-ci. ******
            '*** Si Aucune valeur et que la table générale existe alors initialisation avec (Aucun) ****
            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModele, VirInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueGeneral, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = True
            Constructeur.NoInfoSelection(0, 1) = 1
            Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Intitule
            Constructeur.InfoExtraite(0, 1, 0) = 1

            Resultat = VirServiceServeur.RequeteSql_ToListeType(VirNomUtilisateur, VI.PointdeVue.PVueGeneral, 1, Constructeur.OrdreSqlDynamique)

            Dim FicheRef As New TablesObjet.ShemaREF.TAB_DESCRIPTION
            Dim Cretour As Boolean
            If Resultat Is Nothing Then
                IndiceI = VirServiceServeur.ObtenirUnCompteur(VirNomUtilisateur, "TAB_DESCRIPTION", "Max") + 1
                FicheRef.Ide_Dossier = IndiceI
                FicheRef.Intitule = Intitule
                FicheRef.SiModifiable = "Oui"
                Cretour = VirServiceServeur.MiseAjour_Fiche(VirNomUtilisateur, VI.PointdeVue.PVueGeneral, 1, IndiceI, "C", "", FicheRef.ContenuTable)
            Else
                FicheRef.ContenuTable = Resultat.Item(0).Ide_Dossier & VI.Tild & VI.Tild
            End If
            Dim FicheVal As New TablesObjet.ShemaREF.TAB_LISTE
            FicheVal.Ide_Dossier = FicheRef.Ide_Dossier
            FicheVal.Valeur = "(Aucun)"
            FicheVal.Reference = ""
            Cretour = VirServiceServeur.MiseAjour_Fiche(VirNomUtilisateur, VI.PointdeVue.PVueGeneral, 2, FicheVal.Ide_Dossier, "C", "", FicheVal.ContenuTable)

            Constructeur = Nothing

            Resultat = New List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim Aucun As New Virtualia.Net.ServiceServeur.VirRequeteType
            Aucun.Ide_Dossier = FicheVal.Ide_Dossier
            Aucun.Valeurs = New List(Of String)
            Aucun.Valeurs.Add(FicheVal.Valeur)
            Resultat.Add(Aucun)

            Return Resultat
        End Function

        Public ReadOnly Property ListeTablesGenerales(ByVal IndexCategorie As Integer) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Get
                Dim LstRes As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
                Dim LstTrie As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
                Dim ItemRes As Virtualia.Net.ServiceServeur.VirRequeteType
                Dim IndiceI As Integer
                Dim SiAfaire As Boolean
                Dim Rupture As String = ""

                LstRes = New List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
                For Each PtdeVue In VirModele
                    For Each Objet In PtdeVue
                        If Objet.CategorieRH = IndexCategorie Then
                            For Each Info In Objet
                                If Info.PointdeVueInverse = VI.PointdeVue.PVueGeneral And Info.TabledeReference <> "" Then
                                    SiAfaire = True
                                    If Info.TabledeReference = "Booléen" Then
                                        SiAfaire = False
                                    End If
                                    If Info.TabledeReference.StartsWith("$") Then
                                        SiAfaire = False
                                    End If
                                    If SiAfaire = True Then
                                        ItemRes = New Virtualia.Net.ServiceServeur.VirRequeteType
                                        ItemRes.Ide_Dossier = 0
                                        ItemRes.Valeurs = New List(Of String)
                                        ItemRes.Valeurs.Add(Info.TabledeReference)
                                        LstRes.Add(ItemRes)
                                    End If
                                End If
                            Next
                        End If
                    Next
                Next
                If LstRes.Count = 0 Then
                    Return Nothing
                End If
                LstTrie = (From instance In LstRes Order By instance.Valeurs(0)).ToList
                LstRes = New List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
                For IndiceI = 0 To LstTrie.Count - 1
                    If LstTrie.Item(IndiceI).Valeurs(0) <> Rupture Then
                        LstRes.Add(LstTrie.Item(IndiceI))
                    End If
                    Rupture = LstTrie.Item(IndiceI).Valeurs(0)
                Next IndiceI
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property LibelleCategorie(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case VI.CategorieRH.InfosPersonnelles
                        Return "Informations personnelles"
                    Case VI.CategorieRH.Diplomes_Qualification
                        Return "Diplômes et qualificatons"
                    Case VI.CategorieRH.SituationAdministrative
                        Return "Situation administrative"
                    Case VI.CategorieRH.AffectationsFonctionnelles
                        Return "Affectations fonctionnelles"
                    Case VI.CategorieRH.AffectationsBudgetaires
                        Return "Affectations budgétaires"
                    Case VI.CategorieRH.TempsdeTravail
                        Return "Temps de travail, congés et absences"
                    Case VI.CategorieRH.Formation
                        Return "Formation continue"
                    Case VI.CategorieRH.Evaluation_Gpec
                        Return "Entretiens et évaluations "
                    Case VI.CategorieRH.Remuneration
                        Return "Rémunération"
                    Case VI.CategorieRH.Frais_Deplacement
                        Return "Frais de déplacement"
                    Case VI.CategorieRH.Social
                        Return "Informations sociales"
                    Case VI.CategorieRH.Retraite
                        Return "Retraite"
                    Case Else
                        Return "(Non défini)"
                End Select
            End Get
        End Property

        Public ReadOnly Property IntitulesMois As List(Of String)
            Get
                Dim IndiceM As Integer
                Dim Lst As New List(Of String)
                For IndiceM = 1 To 12
                    Lst.Add(WsRhDates.MoisEnClair(IndiceM))
                Next IndiceM
                Return Lst
            End Get
        End Property

        Public ReadOnly Property ControleDoublon(ByVal PointdeVue As Integer, ByVal Ide As Integer, ByVal NumInfo As Integer, ByVal Valeur_N1 As String, _
                                                 Optional ByVal Valeur_N2 As String = "", Optional ByVal Valeur_N3 As String = "") As List(Of Integer)
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim LstRes As List(Of String)
                Dim TableauData(0) As String
                Dim NbSelect As Integer = 1

                If Valeur_N2 <> "" Then
                    NbSelect += 1
                End If
                If Valeur_N3 <> "" Then
                    NbSelect += 1
                End If
                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsRhModele, VirInstanceBd)
                Constructeur.NombredeRequetes(PointdeVue, "", "", VI.Operateurs.ET) = NbSelect
                Constructeur.SiPasdeTriSurIdeDossier = True

                Constructeur.NoInfoSelection(0, 1) = NumInfo
                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Valeur_N1
                Constructeur.InfoExtraite(0, 1, 0) = NumInfo
                If Valeur_N2 <> "" Then
                    Constructeur.NoInfoSelection(1, 1) = NumInfo + 1
                    Constructeur.ValeuraComparer(1, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Valeur_N2
                    Constructeur.InfoExtraite(1, 1, 0) = NumInfo + 1
                End If
                If Valeur_N3 <> "" Then
                    Constructeur.NoInfoSelection(2, 1) = NumInfo + 2
                    Constructeur.ValeuraComparer(2, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Valeur_N3
                    Constructeur.InfoExtraite(2, 1, 0) = NumInfo + 2
                End If

                LstRes = VirServiceServeur.RequeteSql_ToListeChar(VirNomUtilisateur, PointdeVue, 1, Constructeur.OrdreSqlDynamique)

                Constructeur = Nothing

                If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                    Return Nothing
                End If
                Dim TabRes As New List(Of Integer)
                TableauData = Strings.Split(LstRes(0), VI.Tild, -1)
                If IsNumeric(TableauData(0)) Then
                    If TableauData(0) <> Ide.ToString Then
                        TabRes.Add(CInt(TableauData(0)))
                    End If
                End If
                If TabRes.Count > 0 Then
                    Return TabRes
                Else
                    Return Nothing
                End If
            End Get
        End Property

        Private ReadOnly Property ListeManager(ByVal Niveau As Integer, ByVal Intitule As String, ByVal SiDelegation As Boolean) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim LstRes As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
                Dim LstRes2nd As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
                Dim LstIde As List(Of Integer)
                Dim IndiceI As Integer

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsRhModele, VirInstanceBd)
                'Constructeur.NombredeRequetes(VI.PointdeVue.PVueVueLogique, "", "", VI.Operateurs.ET) = 1
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueDirection, "", "", VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True
                Constructeur.NoInfoSelection(0, 2) = 1

                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Intitule
                Constructeur.InfoExtraite(0, 2, 0) = 1
                Select Case SiDelegation
                    Case True
                        Constructeur.InfoExtraite(1, 2, 0) = 10
                    Case False
                        Constructeur.InfoExtraite(1, 2, 0) = 4
                End Select

                'LstRes = VirServiceServeur.RequeteSql_ToListeType(VirNomUtilisateur, VI.PointdeVue.PVueVueLogique, 1, Constructeur.OrdreSqlDynamique)
                LstRes = VirServiceServeur.RequeteSql_ToListeType(VirNomUtilisateur, VI.PointdeVue.PVueDirection, 1, Constructeur.OrdreSqlDynamique)

                If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                    Return Nothing
                End If
                LstIde = New List(Of Integer)
                For Each Resultat In LstRes
                    If Strings.Right(CStr(Resultat.Ide_Dossier), 1) = CStr(Niveau) Then
                        If IsNumeric(Resultat.Valeurs(1)) Then
                            LstIde.Add(CInt(Resultat.Valeurs(1)))
                        End If
                    End If
                Next
                If LstIde.Count = 0 Then
                    Return Nothing
                End If

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsRhModele, VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", VirRhDates.DateduJour, VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True
                Constructeur.PreselectiondIdentifiants = LstIde
                Constructeur.NoInfoSelection(0, VI.ObjetPer.ObaOrganigramme) = 1
                For IndiceI = 0 To 3
                    Constructeur.InfoExtraite(IndiceI, VI.ObjetPer.ObaOrganigramme, 0) = IndiceI + 1
                Next
                Constructeur.InfoExtraite(0, VI.ObjetPer.ObaOrganigramme, 0) = 14
                Constructeur.InfoExtraite(0, VI.ObjetPer.ObaOrganigramme, 0) = 15

                LstRes = VirServiceServeur.RequeteSql_ToListeType(VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaOrganigramme, Constructeur.OrdreSqlDynamique)
                If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                    Return Nothing
                End If

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsRhModele, VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", VirRhDates.DateduJour, VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True
                Constructeur.PreselectiondIdentifiants = LstIde
                Constructeur.NoInfoSelection(0, VI.ObjetPer.ObaAffectation2nd) = 1
                For IndiceI = 0 To 3
                    Constructeur.InfoExtraite(IndiceI, VI.ObjetPer.ObaAffectation2nd, 0) = IndiceI + 1
                Next
                Constructeur.InfoExtraite(0, VI.ObjetPer.ObaAffectation2nd, 0) = 14
                Constructeur.InfoExtraite(0, VI.ObjetPer.ObaAffectation2nd, 0) = 15
                LstRes2nd = VirServiceServeur.RequeteSql_ToListeType(VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaAffectation2nd, Constructeur.OrdreSqlDynamique)

                If LstRes2nd IsNot Nothing AndAlso LstRes2nd.Count > 0 Then
                    LstRes.AddRange(LstRes2nd)
                End If
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property VirIdentifiantManager(ByVal IdeSalarie As Integer, ByVal NiveauConcerne As Integer) As Integer
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim LstAffectation As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
                Dim LstChef As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
                Dim IndiceI As Integer

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsRhModele, VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", VirRhDates.DateduJour, VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True
                Constructeur.IdentifiantDossier = IdeSalarie
                Constructeur.NoInfoSelection(0, VI.ObjetPer.ObaOrganigramme) = 1
                For IndiceI = 0 To 3
                    Constructeur.InfoExtraite(IndiceI, VI.ObjetPer.ObaOrganigramme, 0) = IndiceI + 1
                Next
                Constructeur.InfoExtraite(0, VI.ObjetPer.ObaOrganigramme, 0) = 14
                Constructeur.InfoExtraite(0, VI.ObjetPer.ObaOrganigramme, 0) = 15

                LstAffectation = VirServiceServeur.RequeteSql_ToListeType(VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaOrganigramme, Constructeur.OrdreSqlDynamique)

                If LstAffectation Is Nothing Then
                    Return 0
                End If
                Select Case NiveauConcerne
                    Case 0
                        NiveauConcerne = 6
                End Select
                For IndiceI = NiveauConcerne - 1 To 0 Step -1
                    If LstAffectation.Item(0).Valeurs(IndiceI) <> "" Then
                        LstChef = ListeManager(IndiceI + 1, LstAffectation.Item(0).Valeurs(IndiceI), False)
                        If LstChef IsNot Nothing AndAlso LstChef.Count > 0 Then
                            For Each Chef In LstChef
                                If Chef.Valeurs(0) = LstAffectation.Item(0).Valeurs(0) Then
                                    Return Chef.Ide_Dossier
                                End If
                            Next
                        End If
                    End If
                Next IndiceI
                Return 0
            End Get
        End Property

        Public ReadOnly Property VirIdentifiantDelegue(ByVal IdeSalarie As Integer) As Integer
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim LstAffectation As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
                Dim LstDelegue As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
                Dim IndiceI As Integer

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsRhModele, VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", VirRhDates.DateduJour, VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True
                Constructeur.IdentifiantDossier = IdeSalarie
                Constructeur.NoInfoSelection(0, VI.ObjetPer.ObaOrganigramme) = 1
                For IndiceI = 0 To 3
                    Constructeur.InfoExtraite(IndiceI, VI.ObjetPer.ObaOrganigramme, 0) = IndiceI + 1
                Next
                Constructeur.InfoExtraite(0, VI.ObjetPer.ObaOrganigramme, 0) = 14
                Constructeur.InfoExtraite(0, VI.ObjetPer.ObaOrganigramme, 0) = 15

                LstAffectation = VirServiceServeur.RequeteSql_ToListeType(VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaOrganigramme, Constructeur.OrdreSqlDynamique)

                If LstAffectation Is Nothing Then
                    Return 0
                End If
                For IndiceI = 5 To 0 Step -1
                    If LstAffectation.Item(0).Valeurs(IndiceI) <> "" Then
                        LstDelegue = ListeManager(IndiceI + 1, LstAffectation.Item(0).Valeurs(IndiceI), True)
                        If LstDelegue IsNot Nothing AndAlso LstDelegue.Count > 0 Then
                            For Each Delegue In LstDelegue
                                If Delegue.Valeurs(0) = LstAffectation.Item(0).Valeurs(0) Then
                                    Return Delegue.Ide_Dossier
                                End If
                            Next
                        End If
                    End If
                Next IndiceI
                Return 0
            End Get
        End Property

        Public ReadOnly Property VirMailEtablissement(ByVal IdeSalarie As Integer) As String
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim LstRes As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
                Dim Etablissement As String

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsRhModele, VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", VirRhDates.DateduJour, VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True
                Constructeur.IdentifiantDossier = IdeSalarie
                Constructeur.NoInfoSelection(0, VI.ObjetPer.ObaSociete) = 1
                Constructeur.InfoExtraite(0, VI.ObjetPer.ObaSociete, 0) = 1
                LstRes = VirServiceServeur.RequeteSql_ToListeType(VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaSociete, Constructeur.OrdreSqlDynamique)
                If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                    Return ""
                End If
                Etablissement = LstRes.Item(0).Valeurs(0)
                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsRhModele, VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueEtablissement, "", VirRhDates.DateduJour, VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True
                Constructeur.NoInfoSelection(0, 1) = 1
                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Etablissement
                Constructeur.InfoExtraite(0, 1, 0) = 19
                LstRes = VirServiceServeur.RequeteSql_ToListeType(VirNomUtilisateur, VI.PointdeVue.PVueEtablissement, 1, Constructeur.OrdreSqlDynamique)
                If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                    Return ""
                End If
                Return LstRes.Item(0).Valeurs(0)
            End Get
        End Property

        Public Sub EcrireLogTraitement(ByVal Nature As String, ByVal SiDatee As Boolean, ByVal Msg As String)
            Dim FicStream As System.IO.FileStream
            Dim FicWriter As System.IO.StreamWriter
            Dim NomLog As String
            Dim NomRep As String
            Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
            Dim dateValue As Date

            NomRep = System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia")
            NomLog = Virtualia.Systeme.Constantes.DossierVirtualiaService("Logs") & Nature & ".log"
            Try
                FicStream = New System.IO.FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
                FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)
                dateValue = System.DateTime.Now
                Select Case Msg
                    Case Is = ""
                        FicWriter.WriteLine(Strings.StrDup(20, "-") & Strings.Space(1) & dateValue.ToString("dd/MM/yyyy hh:mm:ss.fff tt") & Space(1) & Strings.StrDup(20, "-"))
                    Case Else
                        Select Case SiDatee
                            Case True
                                FicWriter.WriteLine(dateValue.ToString("dd/MM/yyyy hh:mm:ss.fff tt") & Space(1) & Msg)
                            Case False
                                FicWriter.WriteLine(Space(5) & Msg)
                        End Select
                End Select
                FicWriter.Flush()
                FicWriter.Close()
            Catch ex As Exception
                Exit Try
            End Try
        End Sub

        Private Sub ChangerBaseCourante(Optional ByVal UtiConnecte As String = "")
            Dim CodeRetour As Boolean
            If UtiConnecte = "" Then
                UtiConnecte = VirNomUtilisateur
            End If
            CodeRetour = VirServiceServeur.ChangerBasedeDonneesCourante(UtiConnecte, WsNoBd)
        End Sub

        Public Sub Reinitialiser(Optional ByVal UtiConnecte As String = "")
            Try
                WsEnsembleREF = Nothing
            Catch ex As Exception
                Exit Try
            End Try
            Try
                GC.Collect()
                GC.WaitForPendingFinalizers()
                GC.Collect()
            Catch ex As Exception
                Exit Try
            End Try

            WsClientServiceWcf = Nothing
            WsClientServiceWeb = Nothing

            Call ChangerBaseCourante(UtiConnecte)

            'Call EcrireLogTraitement("ErreurInitialisation", True, "AKR OBjet Global " & UtiConnecte)

            Call InitialiserArmoireComplete(UtiConnecte)
            WsEnsembleREF = New Virtualia.Ressources.Datas.EnsembleReferences(WsNomUtilisateurBd)

            Dim ObjetInit As Object
            ObjetInit = VirInstanceReferentiel.PointeurPays

            Select Case WsRhModele.InstanceProduit.ClefModele
                Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                    ObjetInit = VirInstanceReferentiel.PointeurEtablissements
                Case Else
                    ObjetInit = VirInstanceReferentiel.PointeurEtablissements_Prives
            End Select
            ObjetInit = VirInstanceReferentiel.PointeurAbsences
            ObjetInit = VirInstanceReferentiel.PointeurPresences
            ObjetInit = VirInstanceReferentiel.PointeurStagesFormation
            ObjetInit = VirInstanceReferentiel.PointeurCalcul
        End Sub

        Private Sub InitialiserArmoireComplete(ByVal UtiConnecte As String)
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim LstRes As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModele, VirInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.SiHistoriquedeSituation = False

            Constructeur.NoInfoSelection(0, 1) = 2
            Constructeur.InfoExtraite(0, 1, 0) = 2
            Constructeur.InfoExtraite(1, 1, 0) = 3
            Constructeur.InfoExtraite(2, 1, 0) = 4

            If UtiConnecte = "" Then
                UtiConnecte = VirNomUtilisateur
            End If

            LstRes = VirServiceServeur.RequeteSql_ToListeType(UtiConnecte, VI.PointdeVue.PVueApplicatif, 1, Constructeur.OrdreSqlDynamique)

            If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                Call EcrireLogTraitement("ErreurInitialisation", True, "Base de données N° " & VirNoBd & " Aucun dossier.")
            End If
            Dim PerDossier As Virtualia.Ressources.Datas.ObjetDossierPER
            WsListeCompletePER = New List(Of Virtualia.Ressources.Datas.ObjetDossierPER)

            For Each Resultat In LstRes
                PerDossier = New Virtualia.Ressources.Datas.ObjetDossierPER(VirModele, Resultat.Ide_Dossier)
                PerDossier.Nom = Resultat.Valeurs(0)
                PerDossier.Prenom = Resultat.Valeurs(1)
                PerDossier.Date_de_Naissance = Resultat.Valeurs(2)
                WsListeCompletePER.Add(PerDossier)
            Next

        End Sub

        Private Sub InitialiserListeImages()
            Dim Chemin As String = "~/Images/Armoire/"
            WsUrlImageArmoire = New List(Of String)
            WsUrlImageArmoire.Add(Chemin & "JauneFermer16.bmp")
            WsUrlImageArmoire.Add(Chemin & "OrangeFonceFermer16.bmp")
            WsUrlImageArmoire.Add(Chemin & "TurquoiseFermer16.bmp")
            WsUrlImageArmoire.Add(Chemin & "BleuFermer16.bmp")
            WsUrlImageArmoire.Add(Chemin & "BleuFonceFermer16.bmp")
            WsUrlImageArmoire.Add(Chemin & "SaumonFermer16.bmp")
            WsUrlImageArmoire.Add(Chemin & "RougeCarminFermer16.bmp")
            WsUrlImageArmoire.Add(Chemin & "MarronFermer16.bmp")
            WsUrlImageArmoire.Add(Chemin & "GrisClairFermer16.bmp")
            WsUrlImageArmoire.Add(Chemin & "GrisFonceFermer16.bmp")
            WsUrlImageArmoire.Add(Chemin & "NoirFermer16.bmp")
            WsUrlImageArmoire.Add(Chemin & "VerdatreFermer16.bmp")
            WsUrlImageArmoire.Add(Chemin & "VertClairFermer16.bmp")
            WsUrlImageArmoire.Add(Chemin & "VertFermer16.bmp")
            WsUrlImageArmoire.Add(Chemin & "VertFonceFermer16.bmp")
            WsUrlImageArmoire.Add(Chemin & "RougeFermer16.bmp")
            WsUrlImageArmoire.Add(Chemin & "JauneOuvert16.bmp")
            WsUrlImageArmoire.Add(Chemin & "OrangeFonceOuvert16.bmp")
            WsUrlImageArmoire.Add(Chemin & "TurquoiseOuvert16.bmp")
            WsUrlImageArmoire.Add(Chemin & "BleuOuvert16.bmp")
            WsUrlImageArmoire.Add(Chemin & "BleuFonceOuvert16.bmp")
            WsUrlImageArmoire.Add(Chemin & "SaumonOuvert16.bmp")
            WsUrlImageArmoire.Add(Chemin & "RougeCarminOuvert16.bmp")
            WsUrlImageArmoire.Add(Chemin & "OrangeOuvert16.bmp")
            WsUrlImageArmoire.Add(Chemin & "GrisClairOuvert16.bmp")
            WsUrlImageArmoire.Add(Chemin & "GrisFonceOuvert16.bmp")
            WsUrlImageArmoire.Add(Chemin & "NoirOuvert16.bmp")
            WsUrlImageArmoire.Add(Chemin & "VerdatreOuvert16.bmp")
            WsUrlImageArmoire.Add(Chemin & "VertClairOuvert16.bmp")
            WsUrlImageArmoire.Add(Chemin & "VertOuvert16.bmp")
            WsUrlImageArmoire.Add(Chemin & "VertFonceOuvert16.bmp")
            WsUrlImageArmoire.Add(Chemin & "RougeOuvert16.bmp")
        End Sub

        Private Sub PurgerTemporaire()
            Dim RepTemp As String = VirRepertoireTemporaire
            Try
                My.Computer.FileSystem.DeleteDirectory(RepTemp, FileIO.DeleteDirectoryOption.DeleteAllContents)
            Catch ex As Exception
                Exit Try
            End Try
        End Sub

        Public ReadOnly Property IDUtilisateur(ByVal NoSession As String) As String
            Get
                If VirClientSessionWcf Is Nothing Then
                    Return ""
                End If
                Dim CnxUti As Virtualia.Net.ServiceSessions.SessionUtilisateurType
                CnxUti = VirClientSessionWcf.Utilisateur(NoSession)
                If CnxUti Is Nothing Then
                    Return ""
                End If
                Return CnxUti.ID_NomConnexion
            End Get
        End Property

        Public ReadOnly Property NomPrenomUtilisateur(ByVal NoSession As String) As String
            Get
                If VirClientSessionWcf Is Nothing Then
                    Return ""
                End If
                Dim CnxUti As Virtualia.Net.ServiceSessions.SessionUtilisateurType
                CnxUti = VirClientSessionWcf.Utilisateur(NoSession)
                If CnxUti Is Nothing Then
                    Return ""
                End If
                Return CnxUti.Nom_Usuel & Strings.Space(1) & CnxUti.Prenom_Usuel
            End Get
        End Property

        Public ReadOnly Property ContexteUtilisateur(ByVal NoSession As String) As Virtualia.Net.ServiceSessions.ContexteUtilisateurType
            Get
                If VirClientSessionWcf Is Nothing Then
                    Return Nothing
                End If
                Return VirClientSessionWcf.Contexte(NoSession)
            End Get
        End Property
        Public Sub New(ByVal NomUti As String, ByVal NoBd As Integer)
            WsNomUtilisateurBd = NomUti
            WsNoBd = NoBd
            WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates
            WsRhFct = New Virtualia.Systeme.Fonctions.Generales

            'Obtenir les paramètres de fonctionnement à partir du WcfServeur *******
            WsInstanceSgbd = VirServiceServeur.Instance_Database
            WsRhModele = VirServiceServeur.Instance_ModeleRH
            WsRhExpertes = VirServiceServeur.Instance_ExperteRH
            '************************************************************************
            Call Reinitialiser()
        End Sub

        Public Sub New()
            Call PurgerTemporaire()
        End Sub
    End Class
End Namespace
