﻿Option Strict On
Option Explicit On
Option Compare Text
Namespace Evenements
    Public Class EvtExecutionScript
        Inherits EventArgs
        Private WsScriptExe As VCaches.CacheScript = Nothing
        Private WsIndexCourant As Integer
        Private WsLigneCourante As VCaches.CacheLigneScript = Nothing
        Private WsIDAppelant As String = ""

        Public ReadOnly Property Index As Integer
            Get
                Return WsIndexCourant
            End Get
        End Property

        Public ReadOnly Property Ligne_Script As VCaches.CacheLigneScript
            Get
                Return WsLigneCourante
            End Get
        End Property

        Public ReadOnly Property Script_A_Executer As VCaches.CacheScript
            Get
                Return WsScriptExe
            End Get
        End Property

        Public Property ID_Appelant As String
            Get
                Return WsIDAppelant
            End Get
            Set(value As String)
                WsIDAppelant = value
            End Set
        End Property

        Public Sub New(ByVal Indice As Integer, ByVal LigneScript As VCaches.CacheLigneScript)
            WsIndexCourant = Indice
            WsLigneCourante = LigneScript
        End Sub

        Public Sub New(ByVal Script As VCaches.CacheScript)
            WsScriptExe = Script
        End Sub
    End Class
End Namespace