﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports System.Web.UI
Namespace Controles
    Public Class SystemeReferences
        Inherits System.Web.UI.UserControl
        Public Delegate Sub ValeurSelectionneeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs)
        Public Event ValeurSelectionnee As ValeurSelectionneeEventHandler
        Private WebFct As Virtualia.Net.Controles.WebFonctions
        Private WsNomStateCache As String = "VSysRef"
        Private WsCtl_Cache As Virtualia.Net.VCaches.CacheSysRef

        Private WsSiMajMenuVisible As Boolean = False

        Protected Overridable Sub V_Selection(ByVal e As Virtualia.Systeme.Evenements.ValeurSelectionneeEventArgs)
            RaiseEvent ValeurSelectionnee(Me, e)
        End Sub

        Private Property CacheVirControle As Virtualia.Net.VCaches.CacheSysRef
            Get
                If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                    Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.VCaches.CacheSysRef)
                End If
                Dim NewCache As Virtualia.Net.VCaches.CacheSysRef
                NewCache = New Virtualia.Net.VCaches.CacheSysRef
                Return NewCache
            End Get
            Set(value As Virtualia.Net.VCaches.CacheSysRef)
                If value Is Nothing Then
                    Exit Property
                End If
                If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                    Me.ViewState.Remove(WsNomStateCache)
                End If
                Me.ViewState.Add(WsNomStateCache, value)
            End Set
        End Property

        Public Property V_SiMajMenuVisible() As Boolean
            Get
                Return WsSiMajMenuVisible
            End Get
            Set(ByVal value As Boolean)
                WsSiMajMenuVisible = value
            End Set
        End Property

        Public ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
            Get
                If WebFct Is Nothing Then
                    WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
                End If
                Return WebFct
            End Get
        End Property

        Public Property V_PointdeVue As Integer
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.PointdeVueInverse
            End Get
            Set(ByVal value As Integer)
                WsCtl_Cache = CacheVirControle
                If WsCtl_Cache.PointdeVueInverse <> value Then
                    WsCtl_Cache.PointdeVueInverse = value
                    WsCtl_Cache.Duo_PointdeVueInverse(0) = 0
                    WsCtl_Cache.Duo_PointdeVueInverse(1) = 0
                    WsCtl_Cache.LettreSelectionnee = ""
                    CacheVirControle = WsCtl_Cache
                End If
            End Set
        End Property

        Public Property V_NomTable As String
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.NomTable
            End Get
            Set(ByVal value As String)
                WsCtl_Cache = CacheVirControle
                If WsCtl_Cache.NomTable <> value Then
                    WsCtl_Cache.NomTable = value
                    WsCtl_Cache.Duo_NomTable(0) = ""
                    WsCtl_Cache.Duo_NomTable(1) = ""
                    WsCtl_Cache.LettreSelectionnee = ""
                    CacheVirControle = WsCtl_Cache
                End If
            End Set
        End Property

        Public Property V_SiTriOrganise() As Boolean
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.SiTriOrganise
            End Get
            Set(ByVal value As Boolean)
                WsCtl_Cache = CacheVirControle
                WsCtl_Cache.SiTriOrganise = value
                CacheVirControle = WsCtl_Cache
            End Set
        End Property

        Public Property V_VirtuelPath As String
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.CheminVirtuel
            End Get
            Set(value As String)
                WsCtl_Cache = CacheVirControle
                WsCtl_Cache.CheminVirtuel = value
                CacheVirControle = WsCtl_Cache
            End Set
        End Property

        Public Property V_RechercheCourante As String
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.RechercheCourante
            End Get
            Set(value As String)
                WsCtl_Cache = CacheVirControle
                WsCtl_Cache.RechercheCourante = value
                CacheVirControle = WsCtl_Cache
            End Set
        End Property

        Public Property V_IndexCourant As Integer
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.IndexCourant
            End Get
            Set(value As Integer)
                WsCtl_Cache = CacheVirControle
                WsCtl_Cache.IndexCourant = value
                CacheVirControle = WsCtl_Cache
            End Set
        End Property

        Public Property V_Valeur_Selectionnee As String
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.ValeurSelection
            End Get
            Set(value As String)
                WsCtl_Cache = CacheVirControle
                WsCtl_Cache.ValeurSelection = value
                CacheVirControle = WsCtl_Cache
            End Set
        End Property

        Public Property V_Date_Selectionnee As String
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.Date_Selection
            End Get
            Set(value As String)
                WsCtl_Cache = CacheVirControle
                WsCtl_Cache.Date_Selection = value
                CacheVirControle = WsCtl_Cache
            End Set
        End Property

        Public WriteOnly Property V_DuoTable(ByVal PtdeVue_1 As Integer, ByVal Nomtable_1 As String, ByVal PtdeVue_2 As Integer) As String
            Set(ByVal value As String)
                WsCtl_Cache = CacheVirControle
                WsCtl_Cache.Duo_PointdeVueInverse(0) = PtdeVue_1
                WsCtl_Cache.Duo_NomTable(0) = Nomtable_1
                WsCtl_Cache.Duo_PointdeVueInverse(1) = PtdeVue_2
                WsCtl_Cache.Duo_NomTable(1) = value
                WsCtl_Cache.LettreSelectionnee = ""
                CacheVirControle = WsCtl_Cache
            End Set
        End Property

        Public WriteOnly Property V_Appelant(ByVal NoObjet As Integer) As String
            Set(ByVal value As String)
                WsCtl_Cache = CacheVirControle
                WsCtl_Cache.IDAppelant = value
                WsCtl_Cache.ObjetAppelant = NoObjet
                WsCtl_Cache.ValeurSelection = ""
                CacheVirControle = WsCtl_Cache
            End Set
        End Property

        Public WriteOnly Property V_Appelant(ByVal NoObjet As Integer, ByVal DateValeur As String) As String
            Set(ByVal value As String)
                WsCtl_Cache = CacheVirControle
                WsCtl_Cache.IDAppelant = value
                WsCtl_Cache.ObjetAppelant = NoObjet
                WsCtl_Cache.Date_Selection = DateValeur
                CacheVirControle = WsCtl_Cache
            End Set
        End Property

        Public ReadOnly Property V_IdAppelant As String
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.IDAppelant
            End Get
        End Property

        Public ReadOnly Property V_ObjetAppelant As Integer
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.ObjetAppelant
            End Get
        End Property

        Public ReadOnly Property V_DuoNomTable(ByVal Index As Integer) As String
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.Duo_NomTable(Index)
            End Get
        End Property

        Public ReadOnly Property V_DuoPVueInverse(ByVal Index As Integer) As Integer
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.Duo_PointdeVueInverse(Index)
            End Get
        End Property

        Public Property V_Lettre As String
            Get
                WsCtl_Cache = CacheVirControle
                Return WsCtl_Cache.LettreSelectionnee
            End Get
            Set(value As String)
                WsCtl_Cache = CacheVirControle
                WsCtl_Cache.LettreSelectionnee = value
                CacheVirControle = WsCtl_Cache
            End Set
        End Property

        Public Sub V_SelectSurDicoRef(ByVal Noeud As WebControls.TreeNode)
            Dim Tableaudata As String()
            Dim SelPvue As Integer
            Dim SelTable As String = ""

            Tableaudata = Strings.Split(Noeud.Value, VI.Tild, -1)
            If IsNumeric(Tableaudata(0)) = False Then
                Exit Sub
            End If
            SelPvue = CInt(Tableaudata(0))
            If Tableaudata.Count > 1 Then
                SelTable = Tableaudata(1)
            End If
            Noeud.PopulateOnDemand = False

            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.LettreSelectionnee = ""
            WsCtl_Cache.NomTable = SelTable
            WsCtl_Cache.PointdeVueInverse = SelPvue
            WsCtl_Cache.IDAppelant = ""
            WsCtl_Cache.ObjetAppelant = 0
            WsCtl_Cache.ValeurSelection = ""
            CacheVirControle = WsCtl_Cache
        End Sub

        Public Function V_AlleraInfoDico(ByVal Conteneur As WebControls.TreeView, ByVal Index As Integer, ByVal SiPtdevue As Boolean, ByVal Valeur As String) As WebControls.TreeNode
            Dim Tableaudata(0) As String
            Dim Noeud0 As WebControls.TreeNode
            Dim NoeudPvue As WebControls.TreeNode
            Dim NoeudTable As WebControls.TreeNode
            Dim Lg As Integer = Valeur.Length
            Dim Rec As Integer = 0
            Dim IndexRec As Integer = 0

            For Each Noeud0 In Conteneur.Nodes
                Select Case SiPtdevue
                    Case False
                        Select Case Strings.Left(Valeur, 1)
                            Case "*"
                                If Strings.InStr(Noeud0.Text, Strings.Right(Valeur, Lg - 1)) > 0 Then
                                    If IndexRec = Index Then
                                        Return Noeud0
                                    End If
                                    IndexRec += 1
                                End If
                            Case Else
                                Rec = Noeud0.Text.Length
                                If Rec >= Lg Then
                                    If Strings.Left(Noeud0.Text, Lg) = Valeur Then
                                        If IndexRec = Index Then
                                            Return Noeud0
                                        End If
                                        IndexRec += 1
                                    End If
                                End If
                        End Select
                End Select
                Noeud0.Expand()
                For Each NoeudPvue In Noeud0.ChildNodes
                    Select Case SiPtdevue
                        Case True
                            Tableaudata = Strings.Split(NoeudPvue.Value, VI.Tild, -1)
                            If IsNumeric(Tableaudata(0)) = True Then
                                If CInt(Tableaudata(0)) = V_PointdeVue Then
                                    Return NoeudPvue
                                End If
                            End If
                        Case False
                            Select Case Strings.Left(Valeur, 1)
                                Case "*"
                                    If Strings.InStr(NoeudPvue.Text, Strings.Right(Valeur, Lg - 1)) > 0 Then
                                        If IndexRec = Index Then
                                            Return NoeudPvue
                                        End If
                                        IndexRec += 1
                                    End If
                                Case Else
                                    Rec = NoeudPvue.Text.Length
                                    If Rec >= Lg Then
                                        If Strings.Left(NoeudPvue.Text, Lg) = Valeur Then
                                            If IndexRec = Index Then
                                                Return NoeudPvue
                                            End If
                                            IndexRec += 1
                                        End If
                                    End If
                            End Select
                    End Select
                    NoeudPvue.Expand()
                    For Each NoeudTable In NoeudPvue.ChildNodes
                        Select Case SiPtdevue
                            Case False
                                Select Case Strings.Left(Valeur, 1)
                                    Case "*"
                                        If Strings.InStr(NoeudTable.Text, Strings.Right(Valeur, Lg - 1)) > 0 Then
                                            If IndexRec = Index Then
                                                Return NoeudTable
                                            End If
                                            IndexRec += 1
                                        End If
                                    Case Else
                                        Rec = NoeudTable.Text.Length
                                        If Rec >= Lg Then
                                            If Strings.Left(NoeudTable.Text, Lg) = Valeur Then
                                                If IndexRec = Index Then
                                                    Return NoeudTable
                                                End If
                                                IndexRec += 1
                                            End If
                                        End If
                                End Select
                        End Select
                    Next
                    NoeudPvue.Collapse()
                Next
            Next
            Return Nothing
        End Function

        Public Sub V_ChargerListeTablesGenerales(ByVal Conteneur As WebControls.TreeView, ByVal Noeud As WebControls.TreeNode)
            Dim LstObjets As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim VImage As String = ""
            Dim CptSel As Integer = 0
            Dim FicheRef As Virtualia.Ressources.Datas.ObjetDossierREF
            Dim SiAfaire As Boolean = False
            Dim Predicat As Virtualia.Ressources.Predicats.PredicateSysRef
            Dim LstFiches As List(Of Virtualia.Ressources.Datas.ObjetDossierREF)
            Dim Chaine As String

            If V_WebFonction.PointeurContexte.SysRef_Listes.Count = 0 Then
                SiAfaire = True
            Else
                Predicat = New Virtualia.Ressources.Predicats.PredicateSysRef(V_NomTable, "")
                LstFiches = New List(Of Virtualia.Ressources.Datas.ObjetDossierREF)
                LstFiches = V_WebFonction.PointeurContexte.SysRef_Listes.FindAll(AddressOf Predicat.RechercherTable)
                If LstFiches.Count = 0 Then
                    SiAfaire = True
                End If
                Predicat = Nothing
                LstFiches = Nothing
            End If

            If SiAfaire = True Then
                LstObjets = V_WebFonction.PointeurGlobal.LireTableSysReference(VI.PointdeVue.PVueGeneral)
                For Each ResRequete In LstObjets
                    Chaine = ResRequete.Ide_Dossier & VI.Tild & ResRequete.Valeurs(0) & VI.Tild
                    If ResRequete.Valeurs.Count > 1 Then
                        Chaine &= ResRequete.Valeurs(1)
                    End If
                    FicheRef = New Virtualia.Ressources.Datas.ObjetDossierREF(V_WebFonction.PointeurGlobal.VirModele, VI.PointdeVue.PVueGeneral, "TablesGenerales", Chaine)
                    V_WebFonction.PointeurContexte.SysRef_Listes.Add(FicheRef)
                Next
            End If

            Predicat = New Virtualia.Ressources.Predicats.PredicateSysRef("TablesGenerales", "")
            LstFiches = New List(Of Virtualia.Ressources.Datas.ObjetDossierREF)
            LstFiches = V_WebFonction.PointeurContexte.SysRef_Listes.FindAll(AddressOf Predicat.RechercherTable)
            If LstFiches.Count = 0 Then
                Exit Sub
            End If

            For Each FicheRef In LstFiches
                Dim NewNoeud As WebControls.TreeNode = New WebControls.TreeNode(FicheRef.Valeur, VI.PointdeVue.PVueGeneral & VI.Tild & FicheRef.Valeur)
                NewNoeud.PopulateOnDemand = False
                NewNoeud.SelectAction = WebControls.TreeNodeSelectAction.Select
                If Noeud Is Nothing Then
                    Conteneur.Nodes.Add(NewNoeud)
                Else
                    Noeud.ChildNodes.Add(NewNoeud)
                End If
                CptSel += 1
            Next
        End Sub

        Public Function V_AjouterNoeudValeur(ByVal Conteneur As WebControls.TreeView, ByVal Noeud As WebControls.TreeNode, ByVal Valeur As String, ByVal Clef As String, _
                                             Optional ByVal SiSelSurParent As Boolean = False) As WebControls.TreeNode
            Dim VImage As String = ""
            Dim VClef As String = Strings.Replace(Clef, "/", "")

            If Noeud Is Nothing Then
                Select Case V_SiTriOrganise
                    Case True
                        VImage = "~/Images/Armoire/BleuFonceFermer16.bmp"
                    Case False
                        VImage = "~/Images/Armoire/FicheBleuStandard16.bmp"
                End Select
            Else
                Select Case Noeud.Depth
                    Case Is = Conteneur.MaxDataBindDepth
                        VImage = "~/Images/Armoire/FicheJaune.bmp"
                    Case Else
                        Select Case Noeud.Depth
                            Case Is = 0
                                VImage = "~/Images/Armoire/BleuFonceFermer16.bmp"
                            Case Is = 1
                                VImage = "~/Images/Armoire/TurquoiseFermer16.bmp"
                            Case Is = 2
                                VImage = "~/Images/Armoire/RougeCarminFermer16.bmp"
                            Case Is = 3
                                VImage = "~/Images/Armoire/BleuClairFermer16.bmp"
                            Case Is = 4
                                VImage = "~/Images/Armoire/GrisFonceFermer16.bmp"
                        End Select
                End Select
            End If

            Dim NewNoeud As WebControls.TreeNode = New WebControls.TreeNode(Valeur, VClef)
            NewNoeud.ImageUrl = VImage
            NewNoeud.PopulateOnDemand = False
            If Noeud IsNot Nothing Then
                Select Case Noeud.Depth
                    Case Is = Conteneur.MaxDataBindDepth
                        NewNoeud.SelectAction = WebControls.TreeNodeSelectAction.Select
                    Case Else
                        Select Case SiSelSurParent
                            Case True
                                Select Case Noeud.Depth
                                    Case Is = Conteneur.MaxDataBindDepth - 1
                                        NewNoeud.SelectAction = WebControls.TreeNodeSelectAction.Expand
                                    Case Else
                                        NewNoeud.SelectAction = WebControls.TreeNodeSelectAction.Expand
                                End Select
                            Case False
                                NewNoeud.SelectAction = WebControls.TreeNodeSelectAction.Expand
                        End Select
                End Select
            End If
            If Noeud Is Nothing Then
                Conteneur.Nodes.Add(NewNoeud)
            Else
                Noeud.ChildNodes.Add(NewNoeud)
            End If
            WsCtl_Cache = CacheVirControle
            If Noeud IsNot Nothing And WsCtl_Cache.ValeurSelection <> "" Then
                If NewNoeud.Text = WsCtl_Cache.ValeurSelection Then
                    V_VirtuelPath = NewNoeud.ValuePath
                End If
            End If
            Return NewNoeud
        End Function
    End Class
End Namespace