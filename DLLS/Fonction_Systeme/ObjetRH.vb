Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes

Namespace MetaModele
    Namespace Donnees
        Public Class Objet
            Inherits List(Of Information)
            Private ReadOnly _PvueVirtualia As PointdeVue

            Private WsIndex As Integer
            Private WsPointdeVue As Integer
            Private WsNumero As Integer
            Private WsIntitule As String
            Private WsNature As Integer
            Private WsStructure As Integer
            Private WsLimite As Integer
            Private WsPositionDateFin As Integer
            Private WsNomTableSGBDR As String
            Private WsCategorie As Integer = 0
            Private WsSiCertification As Integer = 0

            Public ReadOnly Property VParent() As PointdeVue
                Get
                    Return _PvueVirtualia
                End Get
            End Property

            Public Property VIndex() As Integer
                Get
                    Return WsIndex
                End Get
                Set(ByVal value As Integer)
                    WsIndex = value
                End Set
            End Property

            Public Property PointdeVue() As Integer
                Get
                    Return WsPointdeVue
                End Get
                Set(ByVal value As Integer)
                    WsPointdeVue = value
                End Set
            End Property

            Public Property Numero() As Integer
                Get
                    Return WsNumero
                End Get
                Set(ByVal value As Integer)
                    WsNumero = value
                End Set
            End Property

            Public Property Intitule() As String
                Get
                    Return WsIntitule
                End Get
                Set(ByVal value As String)
                    WsIntitule = value
                End Set
            End Property

            Public Property VNature() As Integer
                Get
                    Return WsNature
                End Get
                Set(ByVal value As Integer)
                    WsNature = value
                End Set
            End Property

            Public Property VStructure() As Integer
                Get
                    Return WsStructure
                End Get
                Set(ByVal value As Integer)
                    WsStructure = value
                End Set
            End Property

            Public Property LimiteMaximum() As Integer
                Get
                    Return WsLimite
                End Get
                Set(ByVal value As Integer)
                    WsLimite = value
                End Set
            End Property

            Public Property PositionDateFin() As Integer
                Get
                    Return WsPositionDateFin
                End Get
                Set(ByVal value As Integer)
                    WsPositionDateFin = value
                End Set
            End Property

            Public Property NomTableSGBDR() As String
                Get
                    Return WsNomTableSGBDR
                End Get
                Set(ByVal value As String)
                    WsNomTableSGBDR = value
                End Set
            End Property

            Public Property CategorieRH() As Integer
                Get
                    Return WsCategorie
                End Get
                Set(ByVal value As Integer)
                    WsCategorie = value
                End Set
            End Property

            Public Property SiCertification() As Integer
                Get
                    Return WsSiCertification
                End Get
                Set(ByVal value As Integer)
                    WsSiCertification = value
                End Set
            End Property

            Public ReadOnly Property NombredInformations() As Integer
                Get
                    Return Count
                End Get
            End Property

            Public Overloads ReadOnly Property Item(ByVal Index As Integer) As Information
                Get
                    If (Index < 0 Or Index >= Count) Then
                        Return Nothing
                    End If

                    Return Me(Index)

                End Get
            End Property

            Public Function IndexInformation(ByVal NumInfo As Integer) As Integer
                Dim info As Information = (From it In Me Where it.Numero = NumInfo Select it).FirstOrDefault()
                If (info Is Nothing) Then
                    Return 0
                End If
                Return IndexOf(info)
            End Function

            Public Function SiAuMoinsuneInfodeReference() As Boolean
                Dim nb As Integer = (From it In Me Where it.ZoneEdition(VI.OptionInfo.DicoStats) = 1 And it.PointdeVueInverse > 0 Select it).Count()
                Return nb > 0
            End Function

            Public Function CreerUneInformation() As Integer
                Dim ObjetInfo As Information = New Information(Me)
                Add(ObjetInfo)
                ObjetInfo.PointdeVue = PointdeVue
                ObjetInfo.Objet = Numero
                ObjetInfo.VIndex = Count - 1

                Return Count - 1

            End Function

            Public Overloads Sub Remove()
                If Me.Count > 0 Then
                    RemoveAt(Count - 1)
                End If
            End Sub

            Public Sub New(ByVal host As PointdeVue)
                _PvueVirtualia = host
            End Sub

        End Class
    End Namespace
End Namespace

