Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Sgbd
    Namespace Sql
        Public Class SqlInterne
            Private WsRhFonction As Virtualia.Systeme.Fonctions.Generales
            Private WsModeleRh As Virtualia.Systeme.MetaModele.Donnees.ModeleRH
            Private WsRhPointdeVue As Virtualia.Systeme.MetaModele.Donnees.PointdeVue
            Private WsRhObjet As Virtualia.Systeme.MetaModele.Donnees.Objet
            Private WsRhInfo As Virtualia.Systeme.MetaModele.Donnees.Information
            'Sgbd
            Private WsTypeSgbd As Integer
            Private WsShemaSgbd As String
            Private WsDatesSgbd As Integer
            Private WsDecimalSgbd As String
            '************* D�finition des requ�tes ******************************
            Private WsStructureRequete As SqlInterne_Structure
            Private WsLstInfosSelection As List(Of SqlInterne_Selection) = Nothing
            Private WsLstInfosExtraites As List(Of SqlInterne_Extraction) = Nothing
            Private WsClauseFiltreIN As List(Of String)

            Private WsAliasInstance As List(Of String) = Nothing
            Private WsAliasSelectMax As List(Of String) = Nothing
            '
            '*************************
            Private WsSqlInfosTri(50) As String
            Private WsSqlAliasEnPlus(0) As String
            '
            Private WsClauseGroupeBy As String
            Private WsTableauInner As List(Of Integer)
            '
            Private WsTableauVal As List(Of String)
          
            Public ReadOnly Property OrdreSqlDynamique() As String
                Get
                    If WsStructureRequete Is Nothing Then
                        Return ""
                    End If
                    If WsLstInfosSelection Is Nothing Then
                        Return ""
                    End If
                    Dim I As Integer
                    Dim K As Integer
                    Dim NoObjet As Integer
                    Dim NoInfo As Integer
                    Dim Chaine As New System.Text.StringBuilder
                    Dim ChampDate As String
                    Dim CmpDateFin As String
                    Dim ClauseDate As String
                    Dim ClauseIde As String

                    If WsAliasInstance Is Nothing Then
                        Call PreparerLesAlias(WsLstInfosSelection.Count)
                    End If
                    Select Case WsStructureRequete.SiJointurePlutotQueSelectMax
                        Case True
                            WsStructureRequete.SiJointurePlutotQueSelectMax = SiJointureNecessaire
                    End Select

                    Chaine.Append(ClauseSelectPrincipale)
                    For I = 0 To WsLstInfosSelection.Count - 1
                        Chaine.Append(ClauseCondition(I))
                    Next I
                    For I = 0 To WsLstInfosSelection.Count - 1
                        NoObjet = WsLstInfosSelection.Item(I).Numero_Objet
                        Select Case AliasVerifie(NoObjet)
                            Case Is <> WsAliasInstance(0)
                                ClauseIde = "AND (" & WsAliasInstance(0) & VI.PointFinal & "Ide_Dossier = " & AliasVerifie(NoObjet) & VI.PointFinal & "Ide_Dossier)"
                                If Strings.InStr(Chaine.ToString, ClauseIde) = 0 Then
                                    Chaine.Append(Strings.Space(1) & ClauseIde)
                                End If
                        End Select
                    Next I

                    Select Case WsStructureRequete.SiMultiIdentifiants
                        Case False
                            Select Case WsStructureRequete.IdentifiantDossier
                                Case Is > 0
                                    Chaine.Append(" AND (" & WsAliasInstance(0) & VI.PointFinal & "Ide_Dossier = " & WsStructureRequete.IdentifiantDossier & ")")
                            End Select
                        Case True
                            Chaine.Append(" AND (" & WsAliasInstance(0) & VI.PointFinal & "Ide_Dossier IN (")
                            For I = 0 To WsStructureRequete.PreselectiondIdentifiants.Count - 1
                                Select Case I
                                    Case Is = WsStructureRequete.PreselectiondIdentifiants.Count - 1
                                        Chaine.Append(WsStructureRequete.PreselectiondIdentifiants.Item(I) & ")")
                                    Case Else
                                        Chaine.Append(WsStructureRequete.PreselectiondIdentifiants.Item(I) & ", ")
                                End Select
                            Next I
                            Chaine.Append(")")
                    End Select

                    For I = 0 To WsLstInfosSelection.Count - 1
                        NoObjet = WsLstInfosSelection.Item(I).Numero_Objet
                        WsRhObjet = InstanceObjet(NoObjet)
                        If WsRhObjet Is Nothing Then
                            Exit For
                        End If
                        'Pr�cision date de valeur et date de fin
                        Select Case WsRhObjet.VNature
                            Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetDateRang
                                NoInfo = WsLstInfosSelection.Item(I).Numero_Info
                                WsRhInfo = InstanceInformation(NoInfo)
                                Select Case WsRhInfo.VNature
                                    Case VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeHeure, VI.NatureDonnee.DonneeDateTime
                                        Exit Select
                                    Case Else
                                        ClauseDate = ClauseConditiondeDate(I, False)
                                        If WsStructureRequete.SiPasdeControleDatedeEffet = False Then
                                            If Chaine.ToString.Contains(ClauseDate) = False Then
                                                Chaine.Append(ClauseDate)
                                            End If
                                        End If
                                End Select
                                Select Case WsStructureRequete.SiHistoriquedeSituation
                                    Case False
                                        Select Case NoInfo
                                            Case Is > 0
                                                Select Case WsStructureRequete.Date_Debut
                                                    Case ""
                                                        Select Case WsStructureRequete.SiJointurePlutotQueSelectMax
                                                            Case False
                                                                Chaine.Append(ClauseSelectMax(I))
                                                            Case True
                                                                ClauseDate = ClauseConditiondeDate(I, True)
                                                                If Chaine.ToString.Contains(ClauseDate) = False Then
                                                                    Chaine.Append(ClauseDate)
                                                                End If
                                                        End Select
                                                End Select
                                        End Select
                                End Select
                        End Select
                    Next I
                    For I = 1 To WsSqlAliasEnPlus.Count - 1
                        Select Case WsSqlAliasEnPlus(I)
                            Case Is <> ""
                                Select Case WsSqlAliasEnPlus(I)
                                    Case Is <> WsAliasInstance(0)
                                        Chaine.Append(" AND (" & WsAliasInstance(0) & VI.PointFinal & "Ide_Dossier = " & WsSqlAliasEnPlus(I) & VI.PointFinal & "Ide_Dossier)")
                                End Select
                        End Select
                    Next I

                    K = 0
                    Select Case WsStructureRequete.SiJointurePlutotQueSelectMax
                        Case True
                            Chaine.Append(WsClauseGroupeBy)
                            Chaine.Append(" HAVING ")
                            For I = 0 To WsTableauInner.Count - 1
                                ChampDate = NomduChampDateValeur(WsTableauInner(I))
                                Select Case ChampDate
                                    Case Is <> ""
                                        Select Case WsStructureRequete.SiSelectMin_PlutotQue_Max
                                            Case False
                                                Select Case K
                                                    Case Is > 0
                                                        Chaine.Append(" AND ")
                                                End Select
                                                Chaine.Append(ChampDate & " = MAX(")
                                                If Chaine.ToString.Contains(WsAliasSelectMax(WsTableauInner(I)) & " ON") And ChampDate.Length - WsAliasSelectMax(WsTableauInner(I)).Length > 0 Then
                                                    Chaine.Append(WsAliasSelectMax(WsTableauInner(I)) & Strings.Right(ChampDate, ChampDate.Length - WsAliasSelectMax(WsTableauInner(I)).Length) & ")")
                                                Else
                                                    Chaine.Append(ChampDate & ")")
                                                End If
                                            Case True
                                                If WsStructureRequete.Numero_Objet_DateFin > 0 Then
                                                    CmpDateFin = NomduChamp(WsStructureRequete.Numero_Objet_DateFin, WsStructureRequete.Numero_Info_DateFin)
                                                Else
                                                    CmpDateFin = WsRhFonction.ChaineSql(WsTypeSgbd, WsStructureRequete.Date_Fin, VI.NatureDonnee.DonneeDate, WsDatesSgbd)
                                                End If
                                                If ChampDate <> CmpDateFin Then
                                                    Chaine.Append(ChampDate & " > MIN(" & CmpDateFin & ")")
                                                    Exit For
                                                End If
                                        End Select
                                        K += 1
                                End Select
                            Next I
                    End Select

                    If WsClauseFiltreIN IsNot Nothing Then
                        For I = 0 To WsClauseFiltreIN.Count - 1
                            Select Case WsClauseFiltreIN(I)
                                Case Is = ""
                                    Exit For
                            End Select
                            Select Case WsStructureRequete.Operateur_Liaison
                                Case VI.Operateurs.ET
                                    Chaine.Append(" AND (")
                                Case VI.Operateurs.OU
                                    Chaine.Append(" OR (")
                            End Select
                            Chaine.Append(WsAliasInstance(0) & VI.PointFinal & "Ide_Dossier IN (")
                            Chaine.Append(WsClauseFiltreIN(I))
                            Chaine.Append("))")
                        Next I
                    End If

                    Chaine.Append(ClausedeTri)
                    Return Chaine.ToString
                End Get
            End Property

            Public WriteOnly Property LettreAlias() As String
                Set(ByVal value As String)
                    If WsStructureRequete Is Nothing Then
                        Exit Property
                    End If
                    WsStructureRequete.Lettre_Alias = value
                End Set
            End Property

            Public WriteOnly Property ClauseFiltreIN() As String
                Set(ByVal value As String)
                    If WsClauseFiltreIN Is Nothing Then
                        WsClauseFiltreIN = New List(Of String)
                    End If
                    WsClauseFiltreIN.Add(value)
                End Set
            End Property

            Public WriteOnly Property NombredeRequetes(ByVal NoPvue As Integer, ByVal DatedeDebut As String, ByVal DatedeFin As String, ByVal LiaisondesRequetes As Integer) As Integer
                Set(ByVal Value As Integer)
                    WsStructureRequete = New SqlInterne_Structure(NoPvue)
                    WsStructureRequete.Operateur_Liaison = LiaisondesRequetes
                    WsStructureRequete.Date_Debut = DatedeDebut
                    WsStructureRequete.Date_Fin = DatedeFin
                    If DatedeFin = "" Then
                        WsStructureRequete.Date_Fin = Strings.Format(System.DateTime.Now, "d")
                    End If
                    Select Case WsTypeSgbd
                        Case VI.TypeSgbdrNumeric.SgbdrAccess, VI.TypeSgbdrNumeric.SgbdrMySql
                            WsStructureRequete.SiJointurePlutotQueSelectMax = True
                    End Select
                    WsRhPointdeVue = InstancePointdeVue(NoPvue)

                End Set
            End Property

            Public WriteOnly Property SiJointurePlutotQueSelectMax() As Boolean
                Set(ByVal Value As Boolean)
                    If WsStructureRequete Is Nothing Then
                        Exit Property
                    End If
                    WsStructureRequete.SiJointurePlutotQueSelectMax = Value
                End Set
            End Property

            Public WriteOnly Property SiPasdeControleDatedeFin() As Boolean
                Set(ByVal Value As Boolean)
                    If WsStructureRequete Is Nothing Then
                        Exit Property
                    End If
                    WsStructureRequete.SiPasdeControleDatedeFin = Value
                End Set
            End Property


            ''**** AKR Ajout critere de non selection de la date d'effet
            Public WriteOnly Property SiPasdeControleDatedeEffet() As Boolean
                Set(ByVal Value As Boolean)
                    If WsStructureRequete Is Nothing Then
                        Exit Property
                    End If
                    WsStructureRequete.SiPasdeControleDatedeEffet = Value
                End Set
            End Property

            Public ReadOnly Property Lettre_AliasObjet(ByVal Nobjet As Integer) As String
                Get
                    Return AliasVerifie(Nobjet)
                End Get
            End Property

            Public WriteOnly Property SiForcerClauseDistinct() As Boolean
                Set(ByVal Value As Boolean)
                    If WsStructureRequete Is Nothing Then
                        Exit Property
                    End If
                    WsStructureRequete.SiForcerClauseDistinct = Value
                End Set
            End Property

            Public WriteOnly Property ListeInfosASelectionner As List(Of SqlInterne_Selection)
                Set(value As List(Of SqlInterne_Selection))
                    WsLstInfosSelection = value
                End Set
            End Property

            Public WriteOnly Property NoInfoSelection(ByVal Index As Integer, ByVal NoObjet As Integer) As Integer
                Set(ByVal Value As Integer)
                    If WsLstInfosSelection Is Nothing Then
                        WsLstInfosSelection = New List(Of SqlInterne_Selection)
                    End If
                    Dim SelectionInfo As SqlInterne_Selection
                    SelectionInfo = New SqlInterne_Selection(NoObjet, Value)
                    WsLstInfosSelection.Add(SelectionInfo)
                End Set
            End Property

            Public WriteOnly Property SiPasdeTriSurIdeDossier() As Boolean
                Set(ByVal Value As Boolean)
                    If WsStructureRequete Is Nothing Then
                        Exit Property
                    End If
                    WsStructureRequete.SiPasdeTriSurIdeDossier = Value
                End Set
            End Property

            Public WriteOnly Property SiPasdExtractionIdeDossier() As Boolean
                Set(ByVal Value As Boolean)
                    If WsStructureRequete Is Nothing Then
                        Exit Property
                    End If
                    WsStructureRequete.SiPasdExtractionIdeDossier = Value
                End Set
            End Property

            Public WriteOnly Property SiHistoriquedeSituation() As Boolean
                Set(ByVal Value As Boolean)
                    If WsStructureRequete Is Nothing Then
                        Exit Property
                    End If
                    WsStructureRequete.SiHistoriquedeSituation = Value
                End Set
            End Property

            Public WriteOnly Property IdentifiantDossier() As Integer
                Set(ByVal Value As Integer)
                    If WsStructureRequete Is Nothing Then
                        Exit Property
                    End If
                    WsStructureRequete.IdentifiantDossier = Value
                End Set
            End Property

            Public WriteOnly Property PreselectiondIdentifiants() As List(Of Integer)
                Set(ByVal Value As List(Of Integer))
                    If WsStructureRequete Is Nothing Then
                        Exit Property
                    End If
                    WsStructureRequete.PreselectiondIdentifiants = Value
                End Set
            End Property

            Public WriteOnly Property StructureRequete As SqlInterne_Structure
                Set(value As SqlInterne_Structure)
                    WsStructureRequete = value
                    If WsStructureRequete Is Nothing Then
                        Exit Property
                    End If
                    If WsStructureRequete.Date_Fin = "" Then
                        WsStructureRequete.Date_Fin = Strings.Format(System.DateTime.Now, "d")
                    End If
                    Select Case WsTypeSgbd
                        Case VI.TypeSgbdrNumeric.SgbdrAccess, VI.TypeSgbdrNumeric.SgbdrMySql
                            WsStructureRequete.SiJointurePlutotQueSelectMax = True
                    End Select
                    WsRhPointdeVue = InstancePointdeVue(WsStructureRequete.Point_de_Vue)
                End Set
            End Property

            Public WriteOnly Property ListeInfosAExtraire As List(Of SqlInterne_Extraction)
                Set(value As List(Of SqlInterne_Extraction))
                    WsLstInfosExtraites = value
                End Set
            End Property

            Public WriteOnly Property InfoExtraite(ByVal Index As Integer, ByVal NoObjet As Integer, ByVal NoTri As Integer) As Integer
                Set(ByVal Value As Integer)
                    If WsLstInfosExtraites Is Nothing Then
                        WsLstInfosExtraites = New List(Of SqlInterne_Extraction)
                    End If
                    Dim ExtractionInfo As SqlInterne_Extraction
                    ExtractionInfo = New SqlInterne_Extraction(NoObjet, Value, NoTri)
                    WsLstInfosExtraites.Add(ExtractionInfo)
                End Set
            End Property

            Public WriteOnly Property ValeuraComparer(ByVal Index As Integer, ByVal OpeLiaison As Integer, ByVal OpeComparaison As Integer, ByVal SiValNulle As Boolean) As String
                Set(ByVal Value As String)
                    If WsLstInfosSelection IsNot Nothing Then
                        Select Case Index
                            Case 0 To WsLstInfosSelection.Count - 1
                                If Value <> "" Then
                                    Dim LstDatas As New List(Of String)
                                    Dim TableauData(0) As String
                                    TableauData = Strings.Split(Value, VI.PointVirgule)
                                    For Each It In TableauData
                                        LstDatas.Add(It)
                                    Next
                                    WsLstInfosSelection.Item(Index).Valeurs_AComparer = LstDatas
                                End If
                                WsLstInfosSelection.Item(Index).Operateur_Liaison = OpeLiaison
                                WsLstInfosSelection.Item(Index).Operateur_Comparaison = OpeComparaison
                                WsLstInfosSelection.Item(Index).SiAjouter_ValeurNulle = SiValNulle
                                WsLstInfosSelection.Item(Index).Numero_Objet_AComparer = 0
                                WsLstInfosSelection.Item(Index).Numero_Info_AComparer = 0
                        End Select
                    End If
                End Set
            End Property

            Public WriteOnly Property ValeuraComparer(ByVal Index As Integer, ByVal NoObjet As Integer, ByVal OpeLiaison As Integer, ByVal OpeComparaison As Integer) As Integer
                Set(ByVal Value As Integer)
                    If WsLstInfosSelection IsNot Nothing Then
                        Select Case Index
                            Case 0 To WsLstInfosSelection.Count - 1
                                WsLstInfosSelection.Item(Index).Valeurs_AComparer = Nothing
                                WsLstInfosSelection.Item(Index).Operateur_Liaison = OpeLiaison
                                WsLstInfosSelection.Item(Index).Operateur_Comparaison = OpeComparaison
                                WsLstInfosSelection.Item(Index).SiAjouter_ValeurNulle = False
                                WsLstInfosSelection.Item(Index).Numero_Objet_AComparer = NoObjet
                                WsLstInfosSelection.Item(Index).Numero_Info_AComparer = Value
                        End Select
                    End If
                End Set
            End Property

            Private ReadOnly Property NomdelaTable(ByVal Index As Integer) As String
                Get
                    WsRhObjet = InstanceObjet(WsLstInfosSelection.Item(Index).Numero_Objet)
                    If WsRhObjet Is Nothing Then
                        Return ""
                    End If
                    Select Case WsShemaSgbd
                        Case Is = ""
                            Return WsRhObjet.NomTableSGBDR
                        Case Else
                            Return WsShemaSgbd & VI.PointFinal & WsRhObjet.NomTableSGBDR
                    End Select
                End Get
            End Property

            Private ReadOnly Property NomduChamp(ByVal Index As Integer) As String
                Get
                    WsRhObjet = InstanceObjet(WsLstInfosSelection.Item(Index).Numero_Objet)
                    If WsRhObjet Is Nothing Then
                        Return ""
                    End If
                    Select Case WsLstInfosSelection.Item(Index).Numero_Info
                        Case Is = 99
                            Return AliasVerifie(WsLstInfosSelection.Item(Index).Numero_Objet) & VI.PointFinal & "Ide_Dossier"
                        Case Else
                            WsRhInfo = InstanceInformation(WsLstInfosSelection.Item(Index).Numero_Info)
                            Return AliasVerifie(WsLstInfosSelection.Item(Index).Numero_Objet) & VI.PointFinal _
                                & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, WsLstInfosSelection.Item(Index).Numero_Info, WsTypeSgbd)
                    End Select
                End Get
            End Property

            Private ReadOnly Property NomduChamp(ByVal NoObjet As Integer, ByVal NoInfo As Integer) As String
                Get
                    WsRhObjet = InstanceObjet(NoObjet)
                    If WsRhObjet Is Nothing Then
                        Return ""
                    End If
                    Dim Prefixe As String = AliasVerifie(NoObjet)
                    If Prefixe = "" Then
                        Prefixe = WsRhObjet.NomTableSGBDR
                    End If
                    WsRhInfo = InstanceInformation(NoInfo)
                    Return Prefixe & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, NoInfo, WsTypeSgbd)
                End Get
            End Property

            Private ReadOnly Property NomdelaTableExtraite(ByVal Index As Integer) As String
                Get
                    WsRhObjet = InstanceObjet(WsLstInfosExtraites.Item(Index).Numero_Objet)
                    If WsRhObjet Is Nothing Then
                        Return ""
                    End If
                    Select Case WsShemaSgbd
                        Case Is = ""
                            Return WsRhObjet.NomTableSGBDR
                        Case Else
                            Return WsShemaSgbd & VI.PointFinal & WsRhObjet.NomTableSGBDR
                    End Select
                End Get
            End Property

            Private ReadOnly Property NomduChampExtrait(ByVal Index As Integer, ByVal AliasTable As String) As String
                Get
                    Try
                        Select Case Index
                            Case 0 To WsLstInfosExtraites.Count - 1
                                WsRhObjet = InstanceObjet(WsLstInfosExtraites.Item(Index).Numero_Objet)
                                WsRhInfo = InstanceInformation(WsLstInfosExtraites.Item(Index).Numero_Info)
                                Return AliasTable & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, WsLstInfosExtraites.Item(Index).Numero_Info, WsTypeSgbd)
                        End Select
                    Catch ex As Exception
                        Return ""
                    End Try
                    Return ""
                End Get
            End Property

            Private ReadOnly Property SensduTri(ByVal Index As Integer) As String
                Get
                    Select Case Index
                        Case 0 To WsLstInfosExtraites.Count - 1
                            WsRhObjet = InstanceObjet(WsLstInfosExtraites.Item(Index).Numero_Objet)
                            WsRhInfo = InstanceInformation(WsLstInfosExtraites.Item(Index).Numero_Info)
                            Select Case WsRhInfo.VNature
                                Case VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeHeure, VI.NatureDonnee.DonneeDateTime
                                    Return "DESC"
                            End Select
                    End Select
                    Return "ASC"
                End Get
            End Property

            Private ReadOnly Property NomduChampDateValeur(ByVal Index As Integer) As String
                Get
                    Select Case Index
                        Case Is >= WsLstInfosSelection.Count
                            Return ""
                    End Select
                    WsRhObjet = InstanceObjet(WsLstInfosSelection.Item(Index).Numero_Objet)
                    If WsRhObjet Is Nothing Then
                        Return ""
                    End If
                    Select Case WsRhObjet.VNature
                        Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetDateRang
                            WsRhInfo = InstanceInformation(0)
                            Return AliasVerifie(WsLstInfosSelection.Item(Index).Numero_Objet) & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 0, WsTypeSgbd)
                        Case Else
                            Return ""
                    End Select
                End Get
            End Property

            Private ReadOnly Property NomduChampDatedeFin(ByVal Index As Integer) As String
                Get
                    Select Case Index
                        Case Is >= WsLstInfosSelection.Count
                            Return ""
                    End Select
                    WsRhObjet = InstanceObjet(WsLstInfosSelection.Item(Index).Numero_Objet)
                    If WsRhObjet Is Nothing Then
                        Return ""
                    End If
                    Select Case WsRhObjet.VNature
                        Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetDateRang
                            If WsRhObjet.PositionDateFin > 0 Then
                                WsRhInfo = InstanceInformation(WsRhObjet.PositionDateFin)
                                Return AliasVerifie(WsLstInfosSelection.Item(Index).Numero_Objet) & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, WsRhObjet.PositionDateFin, WsTypeSgbd)
                            Else
                                Return ""
                            End If
                        Case Else
                            Return ""
                    End Select
                End Get
            End Property

            Private ReadOnly Property NomduChamp_A_Comparer(ByVal Index As Integer) As String
                Get
                    Select Case Index
                        Case Is >= WsLstInfosSelection.Count
                            Return ""
                    End Select
                    WsRhObjet = InstanceObjet(WsLstInfosSelection.Item(Index).Numero_Objet_AComparer)
                    If WsRhObjet Is Nothing Then
                        Return ""
                    End If
                    Dim Prefixe As String = AliasVerifie(WsLstInfosSelection.Item(Index).Numero_Objet_AComparer)
                    If Prefixe = "" Then
                        Prefixe = WsRhObjet.NomTableSGBDR
                    End If
                    Select Case WsLstInfosSelection.Item(Index).Numero_Info_AComparer
                        Case Is = 99
                            Return Prefixe & VI.PointFinal & "Ide_Dossier"
                        Case Else
                            WsRhInfo = InstanceInformation(WsLstInfosSelection.Item(Index).Numero_Info_AComparer)
                            Return Prefixe & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, WsLstInfosSelection.Item(Index).Numero_Info_AComparer, WsTypeSgbd)
                    End Select
                End Get
            End Property

            Private ReadOnly Property ClauseInnerJoin(ByVal Index As Integer) As String
                Get
                    Dim K As Integer
                    Dim NoObjet As Integer
                    Dim NoInfo As Integer
                    Dim Chaine As New System.Text.StringBuilder
                    Dim ChampDate As String

                    Select Case Index
                        Case Is >= WsLstInfosSelection.Count
                            Return ""
                    End Select
                    NoObjet = WsLstInfosSelection.Item(Index).Numero_Objet
                    WsRhObjet = InstanceObjet(NoObjet)
                    Select Case WsRhObjet.VNature
                        Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetDateRang
                            NoInfo = WsLstInfosSelection.Item(Index).Numero_Info
                            WsRhInfo = InstanceInformation(NoInfo)
                            Select Case WsStructureRequete.SiHistoriquedeSituation
                                Case False
                                    Select Case NoInfo
                                        Case Is > 0
                                            Select Case WsStructureRequete.Date_Debut
                                                Case Is = ""
                                                    Select Case Index
                                                        Case Is > 0
                                                            For K = 0 To Index - 1
                                                                Select Case NomdelaTable(K)
                                                                    Case Is = NomdelaTable(Index)
                                                                        Return ""
                                                                End Select
                                                            Next K
                                                    End Select
                                                    ChampDate = NomduChampDateValeur(Index)
                                                    Select Case ChampDate
                                                        Case Is = ""
                                                            Return ""
                                                    End Select
                                                    Chaine.Append(" INNER JOIN " & NomdelaTable(Index) & Strings.Space(1) & WsAliasSelectMax(Index))
                                                    Chaine.Append(" ON " & AliasVerifie(WsLstInfosSelection.Item(Index).Numero_Objet) & VI.PointFinal & "Ide_Dossier")
                                                    Chaine.Append(" = " & WsAliasSelectMax(Index) & VI.PointFinal & "Ide_Dossier")
                                            End Select
                                    End Select
                            End Select
                    End Select
                    Return Chaine.ToString
                End Get
            End Property

            Private ReadOnly Property ClauseSelectMax(ByVal Index As Integer) As String
                Get
                    Dim Chaine As New System.Text.StringBuilder
                    Dim ChampDate As String
                    Dim K As Integer
                    Dim Operande As String = " <= "
                    Dim CmpDateFin As String

                    ChampDate = NomduChampDateValeur(Index)
                    Select Case ChampDate
                        Case Is = ""
                            Return ""
                    End Select
                    Select Case Index
                        Case Is > 0
                            For K = 0 To Index - 1
                                Select Case NomdelaTable(K)
                                    Case Is = NomdelaTable(Index)
                                        Return ""
                                End Select
                            Next K
                    End Select
                    WsRhInfo = InstanceInformation(0)

                    Chaine.Append(" AND " & ChampDate)
                    Select Case WsStructureRequete.SiSelectMin_PlutotQue_Max
                        Case False
                            Chaine.Append(" =(SELECT MAX(")
                        Case True
                            Chaine.Append(" =(SELECT MIN(")
                            Operande = " > "
                    End Select
                    If WsStructureRequete.Numero_Objet_DateFin > 0 Then
                        CmpDateFin = NomduChamp(WsStructureRequete.Numero_Objet_DateFin, WsStructureRequete.Numero_Info_DateFin)
                    Else
                        CmpDateFin = WsRhFonction.ChaineSql(WsTypeSgbd, WsStructureRequete.Date_Fin, VI.NatureDonnee.DonneeDate, WsDatesSgbd)
                    End If

                    Chaine.Append(WsAliasSelectMax(Index) & Strings.Right(ChampDate, ChampDate.Length - WsAliasSelectMax(Index).Length))
                    Chaine.Append(") FROM " & NomdelaTable(Index) & Strings.Space(1) & WsAliasSelectMax(Index))
                    Chaine.Append(" WHERE " & WsAliasSelectMax(Index) & Strings.Right(ChampDate, ChampDate.Length - WsAliasSelectMax(Index).Length) & Operande)
                    Chaine.Append(CmpDateFin)
                    Chaine.Append(" AND " & AliasVerifie(WsLstInfosSelection.Item(Index).Numero_Objet) & VI.PointFinal & "Ide_Dossier")
                    Chaine.Append(" = " & WsAliasSelectMax(Index) & VI.PointFinal & "Ide_Dossier" & ")")

                    Return Chaine.ToString
                End Get
            End Property

            Private ReadOnly Property ClauseSelectPrincipale() As String
                Get
                    If WsStructureRequete Is Nothing Then
                        Return ""
                    End If
                    Dim SqlClause As System.Text.StringBuilder
                    Dim AliasDeduit As String
                    Dim I As Integer
                    Dim K As Integer
                    Dim TableauData(0) As String

                    ReDim TableauData(50)
                    Erase WsSqlInfosTri
                    ReDim WsSqlInfosTri(50)
                    Erase WsSqlAliasEnPlus
                    ReDim WsSqlAliasEnPlus(TableauData.Count)
                    WsClauseGroupeBy = ""
                    WsTableauInner = New List(Of Integer)

                    K = 0
                    '
                    SqlClause = New System.Text.StringBuilder
                    SqlClause.Append("SELECT ")
                    Select Case WsStructureRequete.SiForcerClauseDistinct
                        Case True
                            SqlClause.Append(" DISTINCT ")
                            For I = 0 To WsLstInfosSelection.Count - 1
                                WsRhObjet = InstanceObjet(WsLstInfosSelection.Item(I).Numero_Objet)
                                Select Case NomdelaTable(I)
                                    Case Is <> ""
                                        TableauData(I) = NomdelaTable(I) & Strings.Space(1) & AliasVerifie(WsLstInfosSelection.Item(I).Numero_Objet)
                                        Select Case SiDejaLa(I)
                                            Case False
                                                WsTableauInner.Add(I)
                                        End Select
                                End Select
                            Next I
                        Case False
                            For I = 0 To WsLstInfosSelection.Count - 1
                                WsRhObjet = InstanceObjet(WsLstInfosSelection.Item(I).Numero_Objet)
                                Select Case WsRhObjet.VNature
                                    Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetDateRang
                                        Select Case WsLstInfosSelection.Item(I).Numero_Info
                                            Case Is = 0
                                                If Strings.InStr(SqlClause.ToString, "DISTINCT") = 0 Then
                                                    SqlClause.Append(" DISTINCT ")
                                                End If
                                        End Select
                                    Case VI.TypeObjet.ObjetTableau
                                        Select Case WsLstInfosSelection.Item(I).Numero_Info
                                            Case Is = 1
                                                If Strings.InStr(SqlClause.ToString, "DISTINCT") = 0 Then
                                                    SqlClause.Append(" DISTINCT ")
                                                End If
                                        End Select
                                End Select
                                Select Case NomdelaTable(I)
                                    Case Is <> ""
                                        TableauData(I) = NomdelaTable(I) & Strings.Space(1) & AliasVerifie(WsLstInfosSelection.Item(I).Numero_Objet)
                                        Select Case SiDejaLa(I)
                                            Case False
                                                WsTableauInner.Add(I)
                                        End Select
                                End Select
                            Next I
                    End Select

                    Select Case WsStructureRequete.SiPasdExtractionIdeDossier
                        Case False
                            SqlClause.Append(WsAliasInstance(0) & VI.PointFinal & "Ide_Dossier")
                            'Cas de la jointure - Identit� avec Clause Select
                            WsClauseGroupeBy = " GROUP BY " & WsAliasInstance(0) & VI.PointFinal & "Ide_Dossier"
                    End Select
                    '
                    If WsLstInfosExtraites IsNot Nothing Then
                        K = I
                        For I = 0 To WsLstInfosExtraites.Count - 1
                            Select Case WsLstInfosExtraites.Item(I).Numero_Objet
                                Case Is = 0
                                    Exit For
                            End Select
                            AliasDeduit = AliasVerifie(WsLstInfosExtraites.Item(I).Numero_Objet)
                            If I = 0 Then
                                Select Case WsStructureRequete.SiPasdExtractionIdeDossier
                                    Case False
                                        SqlClause.Append(", " & NomduChampExtrait(I, AliasDeduit))
                                        WsClauseGroupeBy = WsClauseGroupeBy & ", " & NomduChampExtrait(I, AliasDeduit)
                                    Case True 'Pas de virgule
                                        SqlClause.Append(NomduChampExtrait(I, AliasDeduit))
                                        WsClauseGroupeBy = WsClauseGroupeBy & NomduChampExtrait(I, AliasDeduit)
                                End Select
                            Else
                                SqlClause.Append(", " & NomduChampExtrait(I, AliasDeduit))
                                WsClauseGroupeBy = WsClauseGroupeBy & ", " & NomduChampExtrait(I, AliasDeduit)
                            End If
                            TableauData(K) = NomdelaTableExtraite(I) & Strings.Space(1) & AliasDeduit
                            Select Case SiDejaLa(I)
                                Case False
                                    WsTableauInner.Add(I)
                            End Select
                            WsSqlAliasEnPlus(K) = AliasDeduit
                            Select Case WsLstInfosExtraites.Item(I).Numero_Tri
                                Case Is > 0
                                    WsSqlInfosTri(WsLstInfosExtraites.Item(I).Numero_Tri) = NomduChampExtrait(I, AliasDeduit) & Strings.Space(1) & SensduTri(I)
                            End Select
                            K += 1
                        Next I

                        Select Case WsStructureRequete.SiJointurePlutotQueSelectMax
                            Case True
                                For I = 0 To K
                                    Select Case TableauData(I)
                                        Case Is = ""
                                            Exit For
                                    End Select
                                    Select Case I
                                        Case Is <= WsTableauInner.Count - 1
                                            Select Case NomduChampDateValeur(WsTableauInner(I))
                                                Case Is <> ""
                                                    If SqlClause.ToString.Contains(NomduChampDateValeur(WsTableauInner(I))) = False Then
                                                        SqlClause.Append(", " & NomduChampDateValeur(WsTableauInner(I)))
                                                    End If
                                                    If WsClauseGroupeBy.Contains(NomduChampDateValeur(WsTableauInner(I))) = False Then
                                                        WsClauseGroupeBy = WsClauseGroupeBy & ", " & NomduChampDateValeur(WsTableauInner(I))
                                                    End If
                                            End Select
                                    End Select
                                Next I
                        End Select

                    End If

                    SqlClause.Append(" FROM " & TableauData(0))
                    Select Case WsStructureRequete.SiJointurePlutotQueSelectMax
                        Case True
                            SqlClause.Append(ClauseInnerJoin(WsTableauInner(0)))
                    End Select
                    For I = 1 To K
                        Select Case TableauData(I)
                            Case Is = ""
                                Exit For
                        End Select
                        Select Case Strings.InStr(SqlClause.ToString, TableauData(I))
                            Case Is = 0
                                SqlClause.Append(", " & TableauData(I))
                                Select Case WsStructureRequete.SiJointurePlutotQueSelectMax
                                    Case True
                                        Select Case I
                                            Case Is <= WsTableauInner.Count - 1
                                                SqlClause.Append(ClauseInnerJoin(WsTableauInner(I)))
                                        End Select
                                End Select
                            Case Else
                                WsSqlAliasEnPlus(I) = ""
                        End Select
                    Next I
                    Return SqlClause.ToString
                End Get
            End Property

            Private ReadOnly Property ClauseCondition(ByVal Index As Integer) As String
                Get
                    Dim SqlCondition As System.Text.StringBuilder
                    Dim SqlLiaison As String = ""
                    Dim SqlOperateur As String = ""
                    Dim SqlChamp As String
                    Dim NoObjet As Integer
                    Dim NoInfo As Integer
                    Dim I As Integer
                    Dim ClauseWHERE As String = ""
                    Dim TableauData(0) As String

                    Select Case Index
                        Case Is >= WsLstInfosSelection.Count
                            Return ""
                    End Select
                    SqlCondition = New System.Text.StringBuilder
                    NoObjet = WsLstInfosSelection.Item(Index).Numero_Objet
                    WsRhObjet = InstanceObjet(NoObjet)
                    If WsRhObjet Is Nothing Then
                        Return ""
                    End If
                    NoInfo = WsLstInfosSelection.Item(Index).Numero_Info
                    WsRhInfo = InstanceInformation(NoInfo)
                    Select Case WsLstInfosSelection.Item(Index).Operateur_Liaison
                        Case VI.Operateurs.ET
                            SqlLiaison = " AND "
                        Case VI.Operateurs.OU
                            SqlLiaison = " OR "
                    End Select
                    Select Case WsLstInfosSelection.Item(Index).Operateur_Comparaison
                        Case VI.Operateurs.Egalite
                            Select Case WsRhInfo.VNature
                                Case VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeDateTime
                                    SqlOperateur = " = "
                                Case Else
                                    SqlOperateur = " LIKE "
                            End Select
                        Case VI.Operateurs.Difference
                            Select Case WsRhInfo.VNature
                                Case VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeDateTime
                                    SqlOperateur = " <> "
                                Case Else
                                    SqlOperateur = " NOT LIKE "
                            End Select
                        Case VI.Operateurs.Inclu
                            Select Case WsRhInfo.VNature
                                Case VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeHeure, VI.NatureDonnee.DonneeDateTime
                                    SqlOperateur = " BETWEEN "
                                    SqlLiaison = " AND "
                                Case Else
                                    SqlOperateur = " IN ("
                                    SqlLiaison = " ,"
                            End Select
                        Case VI.Operateurs.Exclu
                            Select Case WsRhInfo.VNature
                                Case VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeHeure, VI.NatureDonnee.DonneeDateTime
                                    SqlOperateur = " NOT BETWEEN "
                                    SqlLiaison = " AND "
                                Case Else
                                    SqlOperateur = " NOT IN ("
                                    SqlLiaison = " ,"
                            End Select
                        Case VI.Operateurs.Superieur
                            SqlOperateur = " > "
                        Case VI.Operateurs.Inferieur
                            SqlOperateur = " < "
                        Case VI.Operateurs.Superieur_Egal
                            SqlOperateur = " >= "
                        Case VI.Operateurs.Inferieur_Egal
                            SqlOperateur = " <= "
                    End Select
                    SqlChamp = NomduChamp(Index)
                    Select Case Index
                        Case 0
                            ClauseWHERE = " WHERE "
                            Select Case WsLstInfosSelection.Count
                                Case Is > 1
                                    ClauseWHERE = " WHERE ("
                            End Select
                        Case Else
                            Select Case WsStructureRequete.Operateur_Liaison
                                Case VI.Operateurs.ET
                                    ClauseWHERE = " AND ("
                                Case VI.Operateurs.OU
                                    ClauseWHERE = " OR ("
                            End Select
                    End Select
                    Call ChargerLesValeursAComparer(Index)
                    If WsTableauVal Is Nothing Then
                        SqlCondition.Append(ClauseWHERE)
                        SqlCondition.Append(SqlChamp & SqlOperateur & NomduChamp_A_Comparer(Index))
                    Else
                        Select Case WsTableauVal(0)
                            Case ""
                                SqlCondition.Append(ClauseWHERE & "NOT(" & SqlChamp & " IS NULL)")
                            Case Is = "NULLE"
                                SqlCondition.Append(ClauseWHERE & SqlChamp & " IS NULL")
                            Case Else
                                SqlCondition.Append(ClauseWHERE)
                                Select Case WsRhObjet.VNature
                                    Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetDateRang
                                        SqlCondition.Append("(")
                                    Case Else
                                        Select Case SqlLiaison
                                            Case Is = " OR "
                                                SqlCondition.Append("(")
                                        End Select
                                End Select
                                For I = 0 To WsTableauVal.Count - 1
                                    Select Case WsTableauVal(I)
                                        Case Is = ""
                                            Exit For
                                    End Select
                                    SqlCondition.Append(SqlChamp & SqlOperateur & WsRhFonction.ChaineSql(WsTypeSgbd, WsTableauVal(I), WsRhInfo.VNature, WsDatesSgbd))
                                    Select Case I
                                        Case 0
                                            Select Case WsLstInfosSelection.Item(Index).Operateur_Comparaison
                                                Case VI.Operateurs.Inclu, VI.Operateurs.Exclu
                                                    SqlOperateur = ""
                                                    SqlChamp = ""
                                            End Select
                                    End Select
                                    Select Case I
                                        Case Is < WsTableauVal.Count - 1
                                            Select Case WsTableauVal(I + 1)
                                                Case Is <> ""
                                                    SqlCondition.Append(SqlLiaison)
                                            End Select
                                    End Select
                                Next I
                                Select Case WsRhObjet.VNature
                                    Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetDateRang
                                        SqlCondition.Append(")")
                                    Case Else
                                        Select Case SqlLiaison
                                            Case Is = " OR "
                                                SqlCondition.Append(")")
                                        End Select
                                End Select
                                Select Case SqlLiaison
                                    Case Is = " ,"
                                        SqlCondition.Append(")")
                                End Select
                                Select Case WsLstInfosSelection.Item(Index).SiAjouter_ValeurNulle
                                    Case True
                                        Select Case WsRhObjet.VNature
                                            Case VI.TypeObjet.ObjetSimple
                                                SqlCondition.Append(" OR " & NomduChamp(Index) & " IS NULL")
                                        End Select
                                End Select
                        End Select
                    End If
                    Select Case Index
                        Case Is > 0
                            SqlCondition.Append(")")
                            Select Case Index
                                Case Is = WsLstInfosSelection.Count - 1
                                    SqlCondition.Append(")")
                            End Select
                    End Select
                    ''******* AKR 05/12/2017
                    Select Case WsLstInfosSelection.Item(Index).Operateur_Comparaison
                        Case 999
                            ClauseWHERE = " AND "
                            SqlOperateur = " = (SELECT MAX("
                            SqlCondition.Append(ClauseWHERE & NomduChamp(Index) & SqlOperateur & WsRhObjet.NomTableSGBDR & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, WsLstInfosSelection.Item(Index).Numero_Info_AComparer, WsTypeSgbd) & ")")
                            SqlCondition.Append(" FROM  " & WsRhObjet.NomTableSGBDR)
                            SqlCondition.Append(" WHERE " & AliasVerifie(NoObjet) & VI.PointFinal & "Ide_Dossier" & " = " & WsRhObjet.NomTableSGBDR & VI.PointFinal & "Ide_Dossier " & ")")
                    End Select
                    ''*******
                    Return SqlCondition.ToString
                End Get
            End Property

            Private ReadOnly Property ClauseConditiondeDate(ByVal Index As Integer, ByVal SiADoubler As Boolean) As String
                Get
                    Dim SqlCondition As System.Text.StringBuilder
                    Dim SqlOperateur As String
                    Dim NoObjet As Integer
                    Dim NoInfo As Integer
                    Dim ChaineW As String = ""
                    Dim ChampDateValeur As String = ""
                    Dim ChampDateFin As String
                    Dim CmpDateValeur As String = ""
                    Dim CmpDateFin As String = ""

                    Select Case Index
                        Case Is >= WsLstInfosSelection.Count
                            Return ""
                    End Select
                    If WsStructureRequete.SiSelectMin_PlutotQue_Max = True Then
                        Return ""
                    End If
                    SqlCondition = New System.Text.StringBuilder
                    NoObjet = WsLstInfosSelection.Item(Index).Numero_Objet
                    WsRhObjet = InstanceObjet(NoObjet)
                    If WsRhObjet Is Nothing Then
                        Return ""
                    End If
                    NoInfo = WsLstInfosSelection.Item(Index).Numero_Info

                    ChampDateValeur = NomduChampDateValeur(Index)
                    WsRhInfo = InstanceInformation(0)
                    If WsStructureRequete.Numero_Objet_DateDebut > 0 Then
                        CmpDateValeur = NomduChamp(WsStructureRequete.Numero_Objet_DateDebut, WsStructureRequete.Numero_Info_DateDebut)
                    ElseIf WsStructureRequete.Date_Debut <> "" Then
                        CmpDateValeur = WsRhFonction.ChaineSql(WsTypeSgbd, WsStructureRequete.Date_Debut, WsRhInfo.VNature, WsDatesSgbd)
                    End If
                    If WsStructureRequete.Numero_Objet_DateFin > 0 Then
                        CmpDateFin = NomduChamp(WsStructureRequete.Numero_Objet_DateFin, WsStructureRequete.Numero_Info_DateFin)
                    Else
                        CmpDateFin = WsRhFonction.ChaineSql(WsTypeSgbd, WsStructureRequete.Date_Fin, VI.NatureDonnee.DonneeDate, WsDatesSgbd)
                    End If

                    Select Case SiADoubler
                        Case True
                            If ChampDateValeur <> "" Then
                                ChaineW = ChampDateValeur
                                ChampDateValeur = WsAliasSelectMax(Index) & Strings.Right(ChaineW, ChaineW.Length - WsAliasSelectMax(Index).Length)
                            End If
                    End Select
                    Select Case WsRhObjet.VStructure
                        Case VI.ComportementObjet.ObjetEvenement
                            ChampDateFin = NomduChampDatedeFin(Index)
                            Select Case SiADoubler
                                Case True
                                    If ChampDateFin <> "" Then
                                        ChaineW = ChampDateFin
                                        ChampDateFin = WsAliasSelectMax(Index) & Strings.Right(ChaineW, ChaineW.Length - WsAliasSelectMax(Index).Length)
                                    End If
                            End Select
                            Select Case CmpDateValeur
                                Case ""
                                    WsRhInfo = InstanceInformation(0)
                                    SqlCondition.Append(" AND " & ChampDateValeur & " <= " & CmpDateFin)
                                    Select Case SiADoubler
                                        Case False
                                            Select Case WsStructureRequete.SiPasdeControleDatedeFin
                                                Case False
                                                    SqlCondition.Append(" AND (" & ChampDateFin & " IS NULL ")
                                                    SqlCondition.Append(" OR " & ChampDateFin & " >= " & CmpDateFin & ")")
                                            End Select
                                    End Select
                                Case Else
                                    WsRhInfo = InstanceInformation(WsRhObjet.PositionDateFin)
                                    SqlOperateur = " BETWEEN "

                                    SqlCondition.Append(" AND (" & ChampDateFin & SqlOperateur & CmpDateValeur)
                                    SqlCondition.Append(" AND " & CmpDateFin)

                                    WsRhInfo = InstanceInformation(0)
                                    SqlCondition.Append(" OR " & ChampDateValeur & SqlOperateur & CmpDateValeur)
                                    SqlCondition.Append(" AND " & CmpDateFin)

                                    SqlCondition.Append(" OR (" & ChampDateValeur & " <= " & CmpDateFin)
                                    SqlCondition.Append(" AND " & ChampDateFin & " >= " & CmpDateValeur)
                                    SqlCondition.Append(")")

                                    SqlCondition.Append(")")
                            End Select
                        Case Else
                            WsRhInfo = InstanceInformation(0)
                            Select Case CmpDateValeur
                                Case ""
                                    WsRhInfo = InstanceInformation(0)
                                    SqlCondition.Append(" AND " & ChampDateValeur & " <= " & CmpDateFin)
                                    Select Case WsRhObjet.VStructure
                                        Case VI.ComportementObjet.ObjetSimultane
                                            If WsStructureRequete.SiHistoriquedeSituation = True Then
                                                ChampDateFin = NomduChampDatedeFin(Index)
                                                If ChampDateFin <> "" Then
                                                    SqlCondition.Append(" AND (" & ChampDateFin & " IS NULL ")
                                                    SqlCondition.Append("OR " & ChampDateFin & " >= " & CmpDateFin & ")")
                                                End If
                                            End If
                                    End Select
                                Case Else
                                    SqlOperateur = " BETWEEN "
                                    Select Case WsRhObjet.PositionDateFin
                                        Case Is > 0
                                            SqlCondition.Append(" AND ((" & ChampDateValeur & SqlOperateur)
                                        Case Else
                                            SqlCondition.Append(" AND (" & ChampDateValeur & SqlOperateur)
                                    End Select
                                    SqlCondition.Append(CmpDateValeur & " AND " & CmpDateFin & ")")
                            End Select
                            Select Case SiADoubler
                                Case False
                                    Select Case WsStructureRequete.SiPasdeControleDatedeFin
                                        Case False
                                            Select Case WsRhObjet.PositionDateFin
                                                Case Is > 0
                                                    ChampDateFin = NomduChampDatedeFin(Index)
                                                    WsRhInfo = InstanceInformation(0)
                                                    Select Case CmpDateValeur
                                                        Case ""
                                                            Select Case WsStructureRequete.SiHistoriquedeSituation
                                                                Case False
                                                                    SqlCondition.Append(" AND (" & ChampDateFin & " IS NULL ")
                                                                    SqlCondition.Append("OR " & ChampDateFin & " >= " & CmpDateFin & ")")
                                                            End Select
                                                        Case Else
                                                            SqlOperateur = " BETWEEN "
                                                            SqlCondition.Append(" OR (" & ChampDateFin & SqlOperateur)
                                                            SqlCondition.Append(CmpDateValeur & " AND " & CmpDateFin & ")")
                                                            '
                                                            SqlCondition.Append(" OR (" & ChampDateValeur & " <= " & CmpDateFin)
                                                            SqlCondition.Append(" AND (" & ChampDateFin & " >= " & CmpDateValeur)
                                                            SqlCondition.Append(" OR " & ChampDateFin & " IS NULL " & ")))")
                                                    End Select
                                            End Select
                                    End Select
                            End Select
                    End Select
                    Return SqlCondition.ToString
                End Get
            End Property

            Private ReadOnly Property ClausedeTri() As String
                Get
                    Dim SqlTri As String
                    Dim I As Integer
                    'Tri sur num�ro de dossier
                    SqlTri = ""
                    Select Case WsStructureRequete.SiPasdeTriSurIdeDossier
                        Case False
                            SqlTri = " ORDER BY " & WsAliasInstance(0) & VI.PointFinal & "Ide_Dossier" & " ASC"
                    End Select
                    For I = 1 To WsSqlInfosTri.Count - 1
                        Select Case WsSqlInfosTri(I)
                            Case Is = ""
                                Exit For
                        End Select
                        Select Case SqlTri
                            Case Is = ""
                                SqlTri = " ORDER BY " & WsSqlInfosTri(I)
                            Case Else
                                SqlTri = SqlTri & ", " & WsSqlInfosTri(I)
                        End Select
                    Next I
                    Return SqlTri
                End Get
            End Property

            Private Sub ChargerLesValeursAComparer(ByVal Index As Integer)
                Dim I As Integer

                Select Case Index
                    Case Is >= WsLstInfosSelection.Count
                        Exit Sub
                End Select
                If WsLstInfosSelection.Item(Index).Numero_Objet_AComparer > 0 And WsLstInfosSelection.Item(Index).Valeurs_AComparer Is Nothing Then
                    WsTableauVal = Nothing
                    Exit Sub
                End If
                If WsLstInfosSelection.Item(Index).Valeurs_AComparer Is Nothing Then
                    WsTableauVal = New List(Of String)
                    WsTableauVal.Add("")
                    Exit Sub
                End If
                WsTableauVal = WsLstInfosSelection.Item(Index).Valeurs_AComparer
                Select Case WsLstInfosSelection.Item(Index).Valeurs_AComparer(0)
                    Case "", "TOUS"
                        WsTableauVal = New List(Of String)
                        WsTableauVal.Add("")
                        Exit Sub
                End Select
                WsRhObjet = InstanceObjet(WsLstInfosSelection.Item(Index).Numero_Objet)
                WsRhInfo = InstanceInformation(WsLstInfosSelection.Item(Index).Numero_Info)
                Select Case WsRhInfo.VNature
                    Case VI.NatureDonnee.DonneeTexte, VI.NatureDonnee.DonneeMemo, VI.NatureDonnee.DonneeTableMemo
                        For I = 0 To WsTableauVal.Count - 1
                            Select Case WsTableauVal(I)
                                Case Is = ""
                                    Exit For
                            End Select
                            Select Case Strings.InStr(WsTableauVal(I), "*")
                                Case Is > 0
                                    Select Case WsTypeSgbd
                                        Case Is <> VI.TypeSgbdrNumeric.SgbdrAccess
                                            WsTableauVal(I) = WsTableauVal(I).Replace("*", "%")
                                    End Select
                            End Select
                        Next I
                End Select
            End Sub

            Private Sub PreparerLesAlias(ByVal Nombre As Integer)
                Dim I As Integer
                WsAliasInstance = New List(Of String)
                For I = 0 To Nombre
                    WsAliasInstance.Add(WsStructureRequete.Lettre_Alias & Chr(97 + I))
                Next I
                WsAliasSelectMax = New List(Of String)
                For I = 0 To Nombre
                    WsAliasSelectMax.Add(WsStructureRequete.Lettre_Alias & Chr(122 - I))
                Next I
            End Sub

            Private ReadOnly Property AliasVerifie(ByVal NoObjet As Integer) As String
                Get
                    Dim I As Integer
                    For I = 0 To WsLstInfosSelection.Count - 1
                        Select Case WsLstInfosSelection.Item(I).Numero_Objet
                            Case Is = NoObjet
                                Return WsAliasInstance(I)
                        End Select
                    Next I
                    For I = 0 To WsLstInfosExtraites.Count - 1
                        Select Case WsLstInfosExtraites.Item(I).Numero_Objet
                            Case 0, NoObjet
                                Return Chr(97 + WsLstInfosSelection.Count + I)
                        End Select
                    Next I
                    Return ""
                End Get
            End Property

            Private ReadOnly Property SiJointureNecessaire() As Boolean
                Get
                    Dim I As Integer
                    Dim NoObjet As Integer
                    Dim NoInfo As Integer
                    Dim ChampDate As String

                    For I = 0 To WsLstInfosSelection.Count - 1
                        NoObjet = WsLstInfosSelection.Item(I).Numero_Objet
                        WsRhObjet = InstanceObjet(NoObjet)
                        If WsRhObjet Is Nothing Then
                            Exit For
                        End If
                        Select Case WsRhObjet.VNature
                            Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetDateRang
                                NoInfo = WsLstInfosSelection.Item(I).Numero_Info
                                Select Case WsStructureRequete.SiHistoriquedeSituation
                                    Case False
                                        Select Case NoInfo
                                            Case Is > 0
                                                Select Case WsStructureRequete.Date_Debut
                                                    Case Is = ""
                                                        ChampDate = NomduChampDateValeur(I)
                                                        Select Case ChampDate
                                                            Case Is <> ""
                                                                Return True
                                                        End Select
                                                End Select
                                        End Select
                                End Select
                        End Select
                    Next I
                    Return False
                End Get
            End Property

            Private Function SiDejaLa(ByVal Index As Integer) As Boolean
                Dim I As Integer
                If WsTableauInner.Count = 0 Then
                    Return False
                End If
                For I = 0 To WsTableauInner.Count - 1
                    Select Case WsTableauInner(I)
                        Case Is = Index
                            Return True
                    End Select
                Next I
                Return False
            End Function

            Private ReadOnly Property InstancePointdeVue(ByVal PtdeVue As Integer) As Virtualia.Systeme.MetaModele.Donnees.PointdeVue
                Get
                    If WsModeleRh.NombredePointdeVue = 0 Then
                        Return Nothing
                    End If
                    Dim IndiceK As Integer
                    Dim RhPvue As Virtualia.Systeme.MetaModele.Donnees.PointdeVue
                    For IndiceK = 0 To WsModeleRh.NombredePointdeVue - 1
                        RhPvue = WsModeleRh.Item(IndiceK)
                        If RhPvue.Numero = PtdeVue Then
                            Return RhPvue
                        End If
                    Next IndiceK
                    Return Nothing
                End Get
            End Property

            Private ReadOnly Property InstanceObjet(ByVal NoObjet As Integer) As Virtualia.Systeme.MetaModele.Donnees.Objet
                Get
                    Dim IndiceO As Integer
                    Dim RhObjet As Virtualia.Systeme.MetaModele.Donnees.Objet

                    Select Case NoObjet
                        Case Is = 0
                            NoObjet = WsLstInfosExtraites.Item(0).Numero_Objet
                            If NoObjet = 0 Then
                                Return Nothing
                            End If
                    End Select

                    For IndiceO = 0 To WsRhPointdeVue.NombredObjets - 1
                        RhObjet = WsRhPointdeVue.Item(IndiceO)
                        If RhObjet.Numero = NoObjet Then
                            Return RhObjet
                        End If
                    Next IndiceO
                    Return Nothing
                End Get
            End Property

            Private ReadOnly Property InstanceInformation(ByVal NoInfo As Integer) As Virtualia.Systeme.MetaModele.Donnees.Information
                Get
                    Dim IndiceI As Integer
                    Dim RhInfo As Virtualia.Systeme.MetaModele.Donnees.Information

                    For IndiceI = 0 To WsRhObjet.NombredInformations - 1
                        RhInfo = WsRhObjet.Item(IndiceI)
                        If RhInfo.Numero = NoInfo Then
                            Return RhInfo
                        End If
                    Next IndiceI
                    Return Nothing
                End Get
            End Property

            Public ReadOnly Property SqlUnObjet(ByVal NoPvue As Integer, ByVal NoObjet As Integer, ByVal Identifiant As Integer, ByVal SiTri As Boolean) As String
                Get
                    Const SqlAlias1 As String = "a"
                    Dim SqlClause As String
                    Dim SqlCondition As System.Text.StringBuilder
                    Dim Nomtable As String
                    Dim I As Integer

                    WsRhPointdeVue = InstancePointdeVue(NoPvue)
                    If WsRhPointdeVue Is Nothing Then
                        Return ""
                    End If
                    WsRhObjet = InstanceObjet(NoObjet)
                    If WsRhObjet Is Nothing Then
                        Return ""
                    End If
                    Nomtable = WsRhObjet.NomTableSGBDR
                    Select Case WsShemaSgbd
                        Case Is <> ""
                            Nomtable = WsShemaSgbd & VI.PointFinal & WsRhObjet.NomTableSGBDR
                    End Select
                    'SqlClause = "SELECT ALL * FROM " & Nomtable & Strings.Space(1) & SqlAlias1
                    SqlClause = "SELECT * FROM " & Nomtable & Strings.Space(1) & SqlAlias1
                    SqlCondition = New System.Text.StringBuilder
                    If WsClauseFiltreIN IsNot Nothing Then
                        For I = 0 To WsClauseFiltreIN.Count - 1
                            Select Case WsClauseFiltreIN(I)
                                Case Is = ""
                                    Exit For
                            End Select
                            If I = 0 Then
                                SqlCondition.Append(" WHERE (")
                            Else
                                SqlCondition.Append(" AND (")
                            End If
                            SqlCondition.Append(SqlAlias1 & VI.PointFinal & "Ide_Dossier IN(")
                            SqlCondition.Append(WsClauseFiltreIN(I))
                            SqlCondition.Append("))")
                        Next I
                        SqlCondition.Append(" ORDER BY " & SqlAlias1 & VI.PointFinal & "Ide_Dossier")
                    Else
                        If Identifiant = 0 Then
                            SqlCondition.Append(" ORDER BY " & SqlAlias1 & VI.PointFinal & "Ide_Dossier")
                        Else
                            SqlCondition.Append(" WHERE " & SqlAlias1 & VI.PointFinal & "Ide_Dossier = " & WsRhFonction.ChaineSql(WsTypeSgbd, Identifiant.ToString, VI.NatureDonnee.DonneeNumerique, WsDatesSgbd))
                        End If
                    End If
                    Select Case SiTri
                        Case True
                            Select Case WsRhObjet.VNature
                                Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetDateRang
                                    If Identifiant = 0 Then
                                        SqlCondition.Append(", ")
                                    Else
                                        SqlCondition.Append(" ORDER BY ")
                                    End If
                                    WsRhInfo = InstanceInformation(0)
                                    SqlCondition.Append(SqlAlias1 & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 0, WsTypeSgbd) & " DESC")
                                Case VI.TypeObjet.ObjetTableau
                                    If Identifiant = 0 Then
                                        SqlCondition.Append(", ")
                                    Else
                                        SqlCondition.Append(" ORDER BY ")
                                    End If
                                    WsRhInfo = InstanceInformation(2)
                                    If WsRhInfo Is Nothing Then
                                        WsRhInfo = InstanceInformation(1)
                                        SqlCondition.Append(SqlAlias1 & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 1, WsTypeSgbd))
                                    Else
                                        Select Case WsRhInfo.VNature
                                            Case VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeDateTime
                                                SqlCondition.Append(SqlAlias1 & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 2, WsTypeSgbd) & " DESC")
                                            Case Else
                                                WsRhInfo = InstanceInformation(1)
                                                SqlCondition.Append(SqlAlias1 & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 1, WsTypeSgbd))
                                        End Select
                                    End If
                            End Select
                    End Select
                    Return SqlClause & SqlCondition.ToString
                End Get
            End Property

            Public ReadOnly Property SqlUnObjet(ByVal NoPvue As Integer, ByVal NoObjet As Integer, ByVal LstIdentifiant As Integer(), ByVal SiTri As Boolean) As String
                Get
                    Const SqlAlias1 As String = "a"
                    Dim SqlClause As String
                    Dim SqlCondition As System.Text.StringBuilder
                    Dim Nomtable As String
                    Dim IndiceI As Integer

                    WsRhPointdeVue = InstancePointdeVue(NoPvue)
                    If WsRhPointdeVue Is Nothing Then
                        Return ""
                    End If
                    WsRhObjet = InstanceObjet(NoObjet)
                    If WsRhObjet Is Nothing Then
                        Return ""
                    End If
                    Nomtable = WsRhObjet.NomTableSGBDR
                    Select Case WsShemaSgbd
                        Case Is <> ""
                            Nomtable = WsShemaSgbd & VI.PointFinal & WsRhObjet.NomTableSGBDR
                    End Select
                    'SqlClause = "SELECT ALL * FROM " & Nomtable & Strings.Space(1) & SqlAlias1
                    SqlClause = "SELECT * FROM " & Nomtable & Strings.Space(1) & SqlAlias1

                    SqlCondition = New System.Text.StringBuilder
                    SqlCondition.Append(" WHERE " & SqlAlias1 & VI.PointFinal & "Ide_Dossier IN (")
                    For IndiceI = 0 To LstIdentifiant.Count - 1
                        SqlCondition.Append(WsRhFonction.ChaineSql(WsTypeSgbd, LstIdentifiant(IndiceI).ToString, _
                                             VI.NatureDonnee.DonneeNumerique, WsDatesSgbd))
                        If IndiceI < LstIdentifiant.Count - 1 Then
                            SqlCondition.Append(", ")
                        End If
                    Next IndiceI
                    SqlCondition.Append(")")

                    SqlCondition.Append(" ORDER BY " & SqlAlias1 & VI.PointFinal & "Ide_Dossier")

                    Select Case SiTri
                        Case True
                            Select Case WsRhObjet.VNature
                                Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetDateRang
                                    SqlCondition.Append(", ")
                                    WsRhInfo = InstanceInformation(0)
                                    SqlCondition.Append(SqlAlias1 & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 0, WsTypeSgbd) & " DESC")
                                Case VI.TypeObjet.ObjetTableau
                                    SqlCondition.Append(", ")
                                    WsRhInfo = InstanceInformation(2)
                                    If WsRhInfo Is Nothing Then
                                        WsRhInfo = InstanceInformation(1)
                                        SqlCondition.Append(SqlAlias1 & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 1, WsTypeSgbd))
                                    Else
                                        Select Case WsRhInfo.VNature
                                            Case VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeDateTime
                                                SqlCondition.Append(SqlAlias1 & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 2, WsTypeSgbd) & " DESC")
                                            Case Else
                                                WsRhInfo = InstanceInformation(1)
                                                SqlCondition.Append(SqlAlias1 & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 1, WsTypeSgbd))
                                        End Select
                                    End If
                            End Select
                    End Select

                    Return SqlClause & SqlCondition.ToString
                End Get
            End Property

            Public ReadOnly Property SqlUnObjet(ByVal NoPvue As Integer, ByVal NoObjet As Integer, ByVal IdentifiantDebut As Integer, ByVal IdentifiantFin As Integer, ByVal SiTri As Boolean) As String
                Get
                    Const SqlAlias1 As String = "a"
                    Dim SqlClause As String
                    Dim SqlCondition As System.Text.StringBuilder
                    Dim Nomtable As String

                    WsRhPointdeVue = InstancePointdeVue(NoPvue)
                    If WsRhPointdeVue Is Nothing Then
                        Return ""
                    End If
                    WsRhObjet = InstanceObjet(NoObjet)
                    If WsRhObjet Is Nothing Then
                        Return ""
                    End If
                    Nomtable = WsRhObjet.NomTableSGBDR
                    Select Case WsShemaSgbd
                        Case Is <> ""
                            Nomtable = WsShemaSgbd & VI.PointFinal & WsRhObjet.NomTableSGBDR
                    End Select
                    'SqlClause = "SELECT ALL * FROM " & Nomtable & Strings.Space(1) & SqlAlias1
                    SqlClause = "SELECT * FROM " & Nomtable & Strings.Space(1) & SqlAlias1

                    SqlCondition = New System.Text.StringBuilder
                    SqlCondition.Append(" WHERE " & SqlAlias1 & VI.PointFinal & "Ide_Dossier >= ")
                    SqlCondition.Append(WsRhFonction.ChaineSql(WsTypeSgbd, IdentifiantDebut.ToString, _
                                             VI.NatureDonnee.DonneeNumerique, WsDatesSgbd))
                    SqlCondition.Append(" AND " & SqlAlias1 & VI.PointFinal & "Ide_Dossier <= ")
                    SqlCondition.Append(WsRhFonction.ChaineSql(WsTypeSgbd, IdentifiantFin.ToString, _
                                             VI.NatureDonnee.DonneeNumerique, WsDatesSgbd))

                    SqlCondition.Append(" ORDER BY " & SqlAlias1 & VI.PointFinal & "Ide_Dossier")

                    Select Case SiTri
                        Case True
                            Select Case WsRhObjet.VNature
                                Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetDateRang
                                    SqlCondition.Append(", ")
                                    WsRhInfo = InstanceInformation(0)
                                    SqlCondition.Append(SqlAlias1 & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 0, WsTypeSgbd) & " DESC")
                                Case VI.TypeObjet.ObjetTableau
                                    SqlCondition.Append(", ")
                                    WsRhInfo = InstanceInformation(2)
                                    If WsRhInfo Is Nothing Then
                                        WsRhInfo = InstanceInformation(1)
                                        SqlCondition.Append(SqlAlias1 & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 1, WsTypeSgbd))
                                    Else
                                        Select Case WsRhInfo.VNature
                                            Case VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeDateTime
                                                SqlCondition.Append(SqlAlias1 & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 2, WsTypeSgbd) & " DESC")
                                            Case Else
                                                WsRhInfo = InstanceInformation(1)
                                                SqlCondition.Append(SqlAlias1 & VI.PointFinal & WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 1, WsTypeSgbd))
                                        End Select
                                    End If
                            End Select
                    End Select

                    Return SqlClause & SqlCondition.ToString
                End Get
            End Property

            Public ReadOnly Property SqlInsertObjet(ByVal NoPvue As Integer, ByVal NoObjet As Integer, ByVal Identifiant As Integer, ByVal Valeur As String) As String
                Get
                    Dim Tableaudata() As String = Strings.Split(Valeur, VI.Tild, -1)
                    Dim SqlClause As System.Text.StringBuilder
                    Dim SqlColonne As System.Text.StringBuilder
                    Dim SqlDonnee As System.Text.StringBuilder
                    Dim IndiceI As Integer
                    Dim IndexInfo As Integer = 0

                    Select Case Identifiant
                        Case Is <= 0
                            Return ""
                    End Select
                    WsRhPointdeVue = InstancePointdeVue(NoPvue)
                    If WsRhPointdeVue Is Nothing Then
                        Return ""
                    End If
                    WsRhObjet = InstanceObjet(NoObjet)
                    If WsRhObjet Is Nothing Then
                        Return ""
                    End If

                    Select Case WsRhObjet.VNature
                        Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetDateRang
                            If Tableaudata(0) = "" Then
                                Return ""
                            End If
                    End Select

                    SqlClause = New System.Text.StringBuilder
                    SqlColonne = New System.Text.StringBuilder
                    SqlDonnee = New System.Text.StringBuilder

                    Select Case WsShemaSgbd
                        Case Is = ""
                            SqlClause.Append("INSERT INTO " & WsRhObjet.NomTableSGBDR)
                        Case Is <> ""
                            SqlClause.Append("INSERT INTO " & WsShemaSgbd & VI.PointFinal & WsRhObjet.NomTableSGBDR)
                    End Select
                    SqlColonne.Append(" (Ide_Dossier")
                    SqlDonnee.Append(Identifiant.ToString.Trim)

                    For IndiceI = 0 To WsRhObjet.NombredInformations - 1
                        WsRhInfo = WsRhObjet.Item(IndiceI)
                        IndexInfo = WsRhInfo.Numero
                        Select Case WsRhInfo.ZoneEdition(VI.OptionInfo.DicoDonMaj)
                            Case Is = 1
                                Select Case WsRhObjet.VNature
                                    Case VI.TypeObjet.ObjetMemo
                                        Select Case Valeur
                                            Case Is <> ""
                                                SqlColonne.Append(VI.Virgule & Strings.Space(1))
                                                SqlDonnee.Append(VI.Virgule & Strings.Space(1))
                                                SqlColonne.Append(WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, IndexInfo, WsTypeSgbd))
                                                SqlDonnee.Append(WsRhFonction.ChaineSql(WsTypeSgbd, Tableaudata(1), WsRhInfo.VNature, WsDatesSgbd))
                                        End Select
                                    Case Else
                                        If IndexInfo <= Tableaudata.Count - 1 Then
                                            Select Case Tableaudata(IndexInfo)
                                                Case Is <> ""
                                                    SqlColonne.Append(VI.Virgule & Strings.Space(1))
                                                    SqlDonnee.Append(VI.Virgule & Strings.Space(1))
                                                    SqlColonne.Append(WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, IndexInfo, WsTypeSgbd))

                                                    Select Case WsRhInfo.VNature
                                                        Case VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeCalculee
                                                            Select Case WsRhInfo.Format
                                                                Case VI.FormatDonnee.Duree, VI.FormatDonnee.Heure, VI.FormatDonnee.DureeHeure
                                                                    SqlDonnee.Append(VI.Quote & Tableaudata(IndexInfo) & VI.Quote)
                                                                Case VI.FormatDonnee.Fixe
                                                                    Select Case WsTypeSgbd
                                                                        Case VI.TypeSgbdrNumeric.SgbdrAccess, VI.TypeSgbdrNumeric.SgbdrDb2, VI.TypeSgbdrNumeric.SgbdrSqlServer, VI.TypeSgbdrNumeric.SgbdrProgress, VI.TypeSgbdrNumeric.SgbdrMySql
                                                                            SqlDonnee.Append(Tableaudata(IndexInfo))
                                                                        Case VI.TypeSgbdrNumeric.SgbdrOracle
                                                                            SqlDonnee.Append(VI.Quote & Tableaudata(IndexInfo) & VI.Quote)
                                                                    End Select
                                                                Case Else
                                                                    Select Case WsTypeSgbd
                                                                        Case VI.TypeSgbdrNumeric.SgbdrAccess, VI.TypeSgbdrNumeric.SgbdrDb2, VI.TypeSgbdrNumeric.SgbdrSqlServer, VI.TypeSgbdrNumeric.SgbdrProgress
                                                                            SqlDonnee.Append(WsRhFonction.VirgulePoint(Tableaudata(IndexInfo)))
                                                                        Case VI.TypeSgbdrNumeric.SgbdrOracle, VI.TypeSgbdrNumeric.SgbdrMySql
                                                                            Select Case WsDatesSgbd
                                                                                Case VI.FormatDateSgbdr.DateStandard
                                                                                    Select Case WsDecimalSgbd
                                                                                        Case Is = VI.PointFinal
                                                                                            SqlDonnee.Append(VI.Quote & WsRhFonction.VirgulePoint(Tableaudata(IndexInfo)) & VI.Quote)
                                                                                        Case Else
                                                                                            Select Case WsRhInfo.Format
                                                                                                Case VI.FormatDonnee.Decimal1
                                                                                                    SqlDonnee.Append(VI.Quote & Strings.Format(WsRhFonction.ConversionDouble(Tableaudata(IndexInfo)), "#########0.0") & VI.Quote)
                                                                                                Case VI.FormatDonnee.Decimal2
                                                                                                    SqlDonnee.Append(VI.Quote & Strings.Format(WsRhFonction.ConversionDouble(Tableaudata(IndexInfo)), "#########0.00") & VI.Quote)
                                                                                                Case VI.FormatDonnee.Decimal3
                                                                                                    SqlDonnee.Append(VI.Quote & Strings.Format(WsRhFonction.ConversionDouble(Tableaudata(IndexInfo)), "#########0.000") & VI.Quote)
                                                                                                Case VI.FormatDonnee.Decimal4
                                                                                                    SqlDonnee.Append(VI.Quote & Strings.Format(WsRhFonction.ConversionDouble(Tableaudata(IndexInfo)), "#########0.0000") & VI.Quote)
                                                                                                Case VI.FormatDonnee.Decimal5
                                                                                                    SqlDonnee.Append(VI.Quote & Strings.Format(WsRhFonction.ConversionDouble(Tableaudata(IndexInfo)), "#########0.00000") & VI.Quote)
                                                                                                Case Else
                                                                                                    SqlDonnee.Append(VI.Quote & Strings.Format(WsRhFonction.ConversionDouble(Tableaudata(IndexInfo)), "#########0.00000000") & VI.Quote)
                                                                                            End Select
                                                                                    End Select
                                                                                Case VI.FormatDateSgbdr.DateUS
                                                                                    SqlDonnee.Append(VI.Quote & WsRhFonction.VirgulePoint(Tableaudata(IndexInfo)) & VI.Quote)
                                                                            End Select
                                                                    End Select
                                                            End Select
                                                        Case VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeDateTime
                                                            SqlDonnee.Append(WsRhFonction.ChaineSql(WsTypeSgbd, Tableaudata(IndexInfo), WsRhInfo.VNature, WsDatesSgbd))
                                                        Case VI.NatureDonnee.DonneeHeure
                                                            SqlDonnee.Append(VI.Quote & Tableaudata(IndexInfo) & VI.Quote)
                                                        Case Else
                                                            Select Case WsRhInfo.LongueurSGBDR
                                                                Case Is > 0
                                                                    Select Case Tableaudata(IndexInfo).Length
                                                                        Case Is > WsRhInfo.LongueurSGBDR
                                                                            Tableaudata(IndexInfo) = Strings.Left(Tableaudata(IndexInfo), WsRhInfo.LongueurSGBDR)
                                                                    End Select
                                                            End Select
                                                            SqlDonnee.Append(WsRhFonction.ChaineSql(WsTypeSgbd, Tableaudata(IndexInfo), WsRhInfo.VNature, WsDatesSgbd))
                                                    End Select
                                            End Select
                                        End If
                                End Select
                        End Select
                    Next IndiceI
                    SqlColonne.Append(") VALUES (")
                    SqlDonnee.Append(")")
                    Return SqlClause.ToString & SqlColonne.ToString & SqlDonnee.ToString
                End Get
            End Property

            Public ReadOnly Property SqlUpdateObjet(ByVal NoPvue As Integer, ByVal NoObjet As Integer, ByVal Identifiant As Integer, ByVal EnrPrecedent As String, ByVal Valeur As String) As String
                Get
                    Dim TableauData() As String = Strings.Split(Valeur, VI.Tild, -1)
                    Dim TableauOld() As String = Strings.Split(EnrPrecedent, VI.Tild, -1)
                    Dim SqlClause As String = ""
                    Dim SqlColonne As String = ""
                    Dim SqlDonnee As System.Text.StringBuilder
                    Dim SqlCondition As System.Text.StringBuilder
                    Dim IndiceI As Integer
                    Dim IndexInfo As Integer

                    Select Case Identifiant
                        Case Is <= 0
                            Return ""
                    End Select
                    WsRhPointdeVue = InstancePointdeVue(NoPvue)
                    If WsRhPointdeVue Is Nothing Then
                        Return ""
                    End If
                    WsRhObjet = InstanceObjet(NoObjet)
                    If WsRhObjet Is Nothing Then
                        Return ""
                    End If

                    Select Case WsRhObjet.VNature
                        Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetDateRang
                            If TableauData(0) = "" Then
                                Return ""
                            End If
                    End Select
                    SqlCondition = New System.Text.StringBuilder
                    SqlCondition.Append(" WHERE ")
                    SqlCondition.Append("Ide_Dossier" & " = " & WsRhFonction.ChaineSql(WsTypeSgbd, Identifiant.ToString, VI.NatureDonnee.DonneeNumerique, WsDatesSgbd))
                    Select Case WsRhObjet.VNature
                        Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetDateRang
                            Select Case TableauOld(0)
                                Case Is = ""
                                    Return SqlInsertObjet(NoPvue, NoObjet, Identifiant, Valeur)
                            End Select
                            SqlCondition.Append(" AND ")
                            WsRhInfo = InstanceInformation(0)
                            SqlCondition.Append(WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 0, WsTypeSgbd) & " = " & WsRhFonction.ChaineSql(WsTypeSgbd, TableauOld(0), WsRhInfo.VNature, WsDatesSgbd))
                    End Select
                    Select Case WsRhObjet.VNature
                        Case VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetTableau, VI.TypeObjet.ObjetDateRang
                            SqlCondition.Append(" AND ")
                            WsRhInfo = InstanceInformation(1)
                            Select Case WsRhInfo.VNature
                                Case VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeDateTime
                                    SqlCondition.Append(WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 1, WsTypeSgbd) & " = " & WsRhFonction.ChaineSql(WsTypeSgbd, TableauOld(1), WsRhInfo.VNature, WsDatesSgbd))
                                Case Else
                                    SqlCondition.Append(WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 1, WsTypeSgbd) & " LIKE " & WsRhFonction.ChaineSql(WsTypeSgbd, TableauOld(1), WsRhInfo.VNature, WsDatesSgbd))
                            End Select
                    End Select

                    SqlClause = "UPDATE " & WsRhObjet.NomTableSGBDR & " SET "
                    Select Case WsShemaSgbd
                        Case Is <> ""
                            SqlClause = "UPDATE " & WsShemaSgbd & VI.PointFinal & WsRhObjet.NomTableSGBDR & " SET "
                    End Select
                    SqlDonnee = New System.Text.StringBuilder
                    For IndiceI = 0 To WsRhObjet.NombredInformations - 1
                        WsRhInfo = WsRhObjet.Item(IndiceI)
                        IndexInfo = WsRhInfo.Numero
                        Select Case WsRhInfo.ZoneEdition(VI.OptionInfo.DicoDonMaj)
                            Case Is = 1
                                Select Case WsRhInfo.IndexSecondaire
                                    Case Is = 1
                                        Select Case WsRhObjet.VNature
                                            Case VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetTableau, VI.TypeObjet.ObjetDateRang
                                                SqlCondition.Append(" AND ")
                                                SqlCondition.Append(WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, IndexInfo, WsTypeSgbd) & " = " & WsRhFonction.ChaineSql(WsTypeSgbd, TableauOld(IndexInfo), WsRhInfo.VNature, WsDatesSgbd))
                                        End Select
                                End Select
                                Select Case IndiceI
                                    Case Is > 0
                                        SqlDonnee.Append(VI.Virgule & Strings.Space(1))
                                End Select
                                SqlDonnee.Append(WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, IndexInfo, WsTypeSgbd) & " = ")
                                Select Case WsRhObjet.VNature
                                    Case VI.TypeObjet.ObjetMemo
                                        SqlDonnee.Append(WsRhFonction.ChaineSql(WsTypeSgbd, TableauData(1), WsRhInfo.VNature, WsDatesSgbd))
                                    Case Else
                                        Select Case WsRhInfo.VNature
                                            Case VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeCalculee
                                                If TableauData(IndexInfo) = "" Then TableauData(IndexInfo) = "0"
                                                Select Case WsRhInfo.Format
                                                    Case VI.FormatDonnee.Duree, VI.FormatDonnee.Heure, VI.FormatDonnee.DureeHeure
                                                        SqlDonnee.Append(VI.Quote & TableauData(IndexInfo) & VI.Quote)
                                                    Case VI.FormatDonnee.Fixe
                                                        Select Case WsTypeSgbd
                                                            Case VI.TypeSgbdrNumeric.SgbdrAccess, VI.TypeSgbdrNumeric.SgbdrDb2, VI.TypeSgbdrNumeric.SgbdrSqlServer, VI.TypeSgbdrNumeric.SgbdrProgress, VI.TypeSgbdrNumeric.SgbdrMySql
                                                                SqlDonnee.Append(TableauData(IndexInfo))
                                                            Case VI.TypeSgbdrNumeric.SgbdrOracle
                                                                SqlDonnee.Append(VI.Quote & TableauData(IndexInfo) & VI.Quote)
                                                        End Select
                                                    Case Else
                                                        Select Case WsTypeSgbd
                                                            Case VI.TypeSgbdrNumeric.SgbdrAccess, VI.TypeSgbdrNumeric.SgbdrDb2, VI.TypeSgbdrNumeric.SgbdrSqlServer, VI.TypeSgbdrNumeric.SgbdrProgress
                                                                SqlDonnee.Append(WsRhFonction.VirgulePoint(TableauData(IndexInfo)))
                                                            Case VI.TypeSgbdrNumeric.SgbdrOracle, VI.TypeSgbdrNumeric.SgbdrMySql
                                                                Select Case WsDatesSgbd
                                                                    Case VI.FormatDateSgbdr.DateStandard
                                                                        Select Case WsDecimalSgbd
                                                                            Case Is = VI.PointFinal
                                                                                SqlDonnee.Append(VI.Quote & WsRhFonction.VirgulePoint(TableauData(IndexInfo)) & VI.Quote)
                                                                            Case Else
                                                                                Select Case WsRhInfo.Format
                                                                                    Case VI.FormatDonnee.Decimal1
                                                                                        SqlDonnee.Append(VI.Quote & Format(WsRhFonction.ConversionDouble(TableauData(IndexInfo)), "#########0.0") & VI.Quote)
                                                                                    Case VI.FormatDonnee.Decimal2
                                                                                        SqlDonnee.Append(VI.Quote & Format(WsRhFonction.ConversionDouble(TableauData(IndexInfo)), "#########0.00") & VI.Quote)
                                                                                    Case VI.FormatDonnee.Decimal3
                                                                                        SqlDonnee.Append(VI.Quote & Format(WsRhFonction.ConversionDouble(TableauData(IndexInfo)), "#########0.000") & VI.Quote)
                                                                                    Case VI.FormatDonnee.Decimal4
                                                                                        SqlDonnee.Append(VI.Quote & Format(WsRhFonction.ConversionDouble(TableauData(IndexInfo)), "#########0.0000") & VI.Quote)
                                                                                    Case VI.FormatDonnee.Decimal5
                                                                                        SqlDonnee.Append(VI.Quote & Format(WsRhFonction.ConversionDouble(TableauData(IndexInfo)), "#########0.00000") & VI.Quote)
                                                                                    Case Else
                                                                                        SqlDonnee.Append(VI.Quote & Format(WsRhFonction.ConversionDouble(TableauData(IndexInfo)), "#########0.00000000") & VI.Quote)
                                                                                End Select
                                                                        End Select
                                                                    Case VI.FormatDateSgbdr.DateUS
                                                                        SqlDonnee.Append(VI.Quote & WsRhFonction.VirgulePoint(TableauData(IndexInfo)) & VI.Quote)
                                                                End Select
                                                        End Select
                                                End Select
                                            Case VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeDateTime
                                                SqlDonnee.Append(WsRhFonction.ChaineSql(WsTypeSgbd, TableauData(IndexInfo), WsRhInfo.VNature, WsDatesSgbd))
                                            Case VI.NatureDonnee.DonneeHeure
                                                SqlDonnee.Append(VI.Quote & TableauData(IndexInfo) & VI.Quote)
                                            Case VI.NatureDonnee.DonneeMemo
                                                SqlDonnee.Append(WsRhFonction.ChaineSql(WsTypeSgbd, TableauData(IndexInfo), WsRhInfo.VNature, WsDatesSgbd))
                                            Case Else
                                                Select Case WsRhInfo.LongueurSGBDR
                                                    Case Is > 0
                                                        Select Case TableauData(IndexInfo).Length
                                                            Case Is > WsRhInfo.LongueurSGBDR
                                                                TableauData(IndexInfo) = Strings.Left(TableauData(IndexInfo), WsRhInfo.LongueurSGBDR)
                                                        End Select
                                                End Select
                                                SqlDonnee.Append(WsRhFonction.ChaineSql(WsTypeSgbd, TableauData(IndexInfo), WsRhInfo.VNature, WsDatesSgbd))
                                        End Select
                                End Select
                        End Select
                    Next IndiceI
                    Return SqlClause & SqlDonnee.ToString & SqlCondition.ToString
                End Get
            End Property

            Public ReadOnly Property SqlUpdateObjet_Lot(ByVal NoPvue As Integer, ByVal NoObjet As Integer, ByVal ClauseWhere As String, _
                                                        ByVal Paires As List(Of KeyValuePair(Of Integer, String))) As String
                Get
                    Dim SqlClause As String = ""
                    Dim SqlColonne As String = ""
                    Dim SqlDonnee As System.Text.StringBuilder
                    Dim SqlCondition As System.Text.StringBuilder
                    Dim IndiceI As Integer
                    Dim SiPremier As Boolean = True
                    Dim IndexInfo As Integer
                    Dim Valeur As String = ""

                    If IsNothing(Paires) Then
                        Return ""
                    End If
                    Select Case ClauseWhere
                        Case Is = ""
                            Return ""
                    End Select
                    WsRhPointdeVue = InstancePointdeVue(NoPvue)
                    If WsRhPointdeVue Is Nothing Then
                        Return ""
                    End If
                    WsRhObjet = InstanceObjet(NoObjet)
                    If WsRhObjet Is Nothing Then
                        Return ""
                    End If

                    SqlCondition = New System.Text.StringBuilder
                    SqlCondition.Append(" WHERE ")
                    SqlCondition.Append(ClauseWhere)

                    SqlClause = "UPDATE " & WsRhObjet.NomTableSGBDR & " SET "
                    Select Case WsShemaSgbd
                        Case Is <> ""
                            SqlClause = "UPDATE " & WsShemaSgbd & VI.PointFinal & WsRhObjet.NomTableSGBDR & " SET "
                    End Select
                    SqlDonnee = New System.Text.StringBuilder

                    For Each Element As KeyValuePair(Of Integer, String) In Paires
                        Valeur = Element.Value
                        For IndiceI = 0 To WsRhObjet.NombredInformations - 1
                            WsRhInfo = WsRhObjet.Item(IndiceI)
                            IndexInfo = WsRhInfo.Numero
                            If IndexInfo = Element.Key Then
                                If SiPremier = False Then
                                    SqlDonnee.Append(VI.Virgule & Strings.Space(1))
                                End If
                                SqlDonnee.Append(WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, IndexInfo, WsTypeSgbd) & " = ")
                                Select Case WsRhObjet.VNature
                                    Case VI.TypeObjet.ObjetMemo
                                        SqlDonnee.Append(WsRhFonction.ChaineSql(WsTypeSgbd, Valeur, WsRhInfo.VNature, WsDatesSgbd))
                                    Case Else
                                        Select Case WsRhInfo.VNature
                                            Case VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeCalculee
                                                If Valeur = "" Then Valeur = "0"
                                                Select Case WsRhInfo.Format
                                                    Case VI.FormatDonnee.Duree, VI.FormatDonnee.Heure, VI.FormatDonnee.DureeHeure
                                                        SqlDonnee.Append(VI.Quote & Valeur & VI.Quote)
                                                    Case VI.FormatDonnee.Fixe
                                                        Select Case WsTypeSgbd
                                                            Case VI.TypeSgbdrNumeric.SgbdrAccess, VI.TypeSgbdrNumeric.SgbdrDb2, VI.TypeSgbdrNumeric.SgbdrSqlServer, VI.TypeSgbdrNumeric.SgbdrProgress, VI.TypeSgbdrNumeric.SgbdrMySql
                                                                SqlDonnee.Append(Valeur)
                                                            Case VI.TypeSgbdrNumeric.SgbdrOracle
                                                                SqlDonnee.Append(VI.Quote & Valeur & VI.Quote)
                                                        End Select
                                                    Case Else
                                                        Select Case WsTypeSgbd
                                                            Case VI.TypeSgbdrNumeric.SgbdrAccess, VI.TypeSgbdrNumeric.SgbdrDb2, VI.TypeSgbdrNumeric.SgbdrSqlServer, VI.TypeSgbdrNumeric.SgbdrProgress
                                                                SqlDonnee.Append(WsRhFonction.VirgulePoint(Valeur))
                                                            Case VI.TypeSgbdrNumeric.SgbdrOracle, VI.TypeSgbdrNumeric.SgbdrMySql
                                                                Select Case WsDatesSgbd
                                                                    Case VI.FormatDateSgbdr.DateStandard
                                                                        Select Case WsDecimalSgbd
                                                                            Case Is = VI.PointFinal
                                                                                SqlDonnee.Append(VI.Quote & WsRhFonction.VirgulePoint(Valeur) & VI.Quote)
                                                                            Case Else
                                                                                Select Case WsRhInfo.Format
                                                                                    Case VI.FormatDonnee.Decimal1
                                                                                        SqlDonnee.Append(VI.Quote & Format(WsRhFonction.ConversionDouble(Valeur), "#########0.0") & VI.Quote)
                                                                                    Case VI.FormatDonnee.Decimal2
                                                                                        SqlDonnee.Append(VI.Quote & Format(WsRhFonction.ConversionDouble(Valeur), "#########0.00") & VI.Quote)
                                                                                    Case VI.FormatDonnee.Decimal3
                                                                                        SqlDonnee.Append(VI.Quote & Format(WsRhFonction.ConversionDouble(Valeur), "#########0.000") & VI.Quote)
                                                                                    Case VI.FormatDonnee.Decimal4
                                                                                        SqlDonnee.Append(VI.Quote & Format(WsRhFonction.ConversionDouble(Valeur), "#########0.0000") & VI.Quote)
                                                                                    Case VI.FormatDonnee.Decimal5
                                                                                        SqlDonnee.Append(VI.Quote & Format(WsRhFonction.ConversionDouble(Valeur), "#########0.00000") & VI.Quote)
                                                                                    Case Else
                                                                                        SqlDonnee.Append(VI.Quote & Format(WsRhFonction.ConversionDouble(Valeur), "#########0.00000000") & VI.Quote)
                                                                                End Select
                                                                        End Select
                                                                    Case VI.FormatDateSgbdr.DateUS
                                                                        SqlDonnee.Append(VI.Quote & WsRhFonction.VirgulePoint(Valeur) & VI.Quote)
                                                                End Select
                                                        End Select
                                                End Select
                                            Case VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeDateTime
                                                SqlDonnee.Append(WsRhFonction.ChaineSql(WsTypeSgbd, Valeur, WsRhInfo.VNature, WsDatesSgbd))
                                            Case VI.NatureDonnee.DonneeHeure
                                                SqlDonnee.Append(VI.Quote & Valeur & VI.Quote)
                                            Case VI.NatureDonnee.DonneeMemo
                                                SqlDonnee.Append(WsRhFonction.ChaineSql(WsTypeSgbd, Valeur, WsRhInfo.VNature, WsDatesSgbd))
                                            Case Else
                                                Select Case WsRhInfo.LongueurSGBDR
                                                    Case Is > 0
                                                        Select Case Valeur.Length
                                                            Case Is > WsRhInfo.LongueurSGBDR
                                                                Valeur = Strings.Left(Valeur, WsRhInfo.LongueurSGBDR)
                                                        End Select
                                                End Select
                                                SqlDonnee.Append(WsRhFonction.ChaineSql(WsTypeSgbd, Valeur, WsRhInfo.VNature, WsDatesSgbd))
                                        End Select
                                End Select
                            End If
                        Next IndiceI
                        SiPremier = False
                    Next Element
                    Return SqlClause & SqlDonnee.ToString & SqlCondition.ToString
                End Get
            End Property

            Public ReadOnly Property SqlDeleteObjet(ByVal NoPvue As Integer, ByVal NoObjet As Integer, ByVal Identifiant As Integer, ByVal Valeur As String, ByVal CodeMaj As String) As String
                Get
                    Dim Tableaudata() As String = Strings.Split(Valeur, VI.Tild, -1)
                    Dim SqlClause As String = ""
                    Dim SqlColonne As String = ""
                    Dim SqlDonnee As String = ""
                    Dim SqlCondition As System.Text.StringBuilder
                    Dim IndiceI As Integer

                    Select Case Identifiant
                        Case Is <= 0
                            Return ""
                    End Select
                    WsRhPointdeVue = InstancePointdeVue(NoPvue)
                    If WsRhPointdeVue Is Nothing Then
                        Return ""
                    End If
                    WsRhObjet = InstanceObjet(NoObjet)
                    If WsRhObjet Is Nothing Then
                        Return ""
                    End If

                    Select Case WsRhObjet.VNature
                        Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetDateRang
                            Select Case CodeMaj
                                Case Is = "S"
                                    If Tableaudata(0) = "" Then
                                        Return ""
                                    End If
                            End Select
                    End Select
                    SqlCondition = New System.Text.StringBuilder
                    Select Case CodeMaj
                        Case Is = "S"
                            SqlCondition.Append(" WHERE ")
                            SqlCondition.Append("Ide_Dossier" & " = " & WsRhFonction.ChaineSql(WsTypeSgbd, Identifiant.ToString, VI.NatureDonnee.DonneeNumerique, WsDatesSgbd))
                            Select Case WsRhObjet.VNature
                                Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetDateRang
                                    SqlCondition.Append(" AND ")
                                    WsRhInfo = InstanceInformation(0)
                                    SqlCondition.Append(WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 0, WsTypeSgbd) & " = " & WsRhFonction.ChaineSql(WsTypeSgbd, Tableaudata(0), WsRhInfo.VNature, WsDatesSgbd))
                            End Select
                            Select Case WsRhObjet.VNature
                                Case VI.TypeObjet.ObjetHeure, VI.TypeObjet.ObjetTableau, VI.TypeObjet.ObjetDateRang
                                    SqlCondition.Append(" AND ")
                                    WsRhInfo = InstanceInformation(1)
                                    Select Case WsRhInfo.VNature
                                        Case VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeDateTime
                                            SqlCondition.Append(WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 1, WsTypeSgbd) & " = " & WsRhFonction.ChaineSql(WsTypeSgbd, Tableaudata(1), WsRhInfo.VNature, WsDatesSgbd))
                                        Case Else
                                            SqlCondition.Append(WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, 1, WsTypeSgbd) & " LIKE " & WsRhFonction.ChaineSql(WsTypeSgbd, Tableaudata(1), WsRhInfo.VNature, WsDatesSgbd))
                                    End Select
                            End Select
                            For IndiceI = 0 To WsRhObjet.NombredInformations - 1
                                WsRhInfo = WsRhObjet.Item(IndiceI)
                                Select Case WsRhInfo.ZoneEdition(VI.OptionInfo.DicoDonMaj)
                                    Case Is = 1
                                        Select Case WsRhInfo.IndexSecondaire
                                            Case Is = 1
                                                SqlCondition.Append(" AND ")
                                                SqlCondition.Append(WsRhFonction.NomChampSgbdr(WsRhInfo.NomInterne, WsRhInfo.Numero, WsTypeSgbd) & " LIKE " & WsRhFonction.ChaineSql(WsTypeSgbd, Tableaudata(WsRhInfo.Numero), WsRhInfo.VNature, WsDatesSgbd))
                                        End Select
                                End Select
                            Next IndiceI
                            SqlClause = "DELETE FROM " & WsRhObjet.NomTableSGBDR
                            Select Case WsShemaSgbd
                                Case Is <> ""
                                    SqlClause = "DELETE FROM " & WsShemaSgbd & VI.PointFinal & WsRhObjet.NomTableSGBDR
                            End Select
                            Return SqlClause & SqlCondition.ToString
                        Case Is = "D"
                            SqlCondition.Append(" WHERE ")
                            SqlCondition.Append("Ide_Dossier" & " = " & WsRhFonction.ChaineSql(WsTypeSgbd, Identifiant.ToString, VI.NatureDonnee.DonneeNumerique, WsDatesSgbd))
                            SqlClause = "DELETE FROM " & WsRhObjet.NomTableSGBDR
                            Select Case WsShemaSgbd
                                Case Is <> ""
                                    SqlClause = "DELETE FROM " & WsShemaSgbd & VI.PointFinal & WsRhObjet.NomTableSGBDR
                            End Select
                            Return SqlClause & SqlCondition.ToString
                        Case Else
                            Return ""
                    End Select
                End Get
            End Property

            Public Sub New(ByVal Modele As Virtualia.Systeme.MetaModele.Donnees.ModeleRH, ByVal TypeduSgbd As Integer, ByVal Shema As String, ByVal FormatDates As Integer, ByVal CaractereDecimal As String)
                MyBase.New()
                WsModeleRh = Modele
                WsLstInfosSelection = Nothing
                WsLstInfosExtraites = Nothing
                WsTypeSgbd = TypeduSgbd
                WsShemaSgbd = Shema
                WsDatesSgbd = FormatDates
                WsDecimalSgbd = CaractereDecimal
                WsRhFonction = New Virtualia.Systeme.Fonctions.Generales

            End Sub

            Public Sub New(ByVal Modele As Virtualia.Systeme.MetaModele.Donnees.ModeleRH, ByVal PointeurDatabase As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdDatabase)
                MyBase.New()
                WsModeleRh = Modele
                WsLstInfosSelection = Nothing
                WsLstInfosExtraites = Nothing
                If PointeurDatabase Is Nothing Then
                    WsTypeSgbd = VI.TypeSgbdrNumeric.SgbdrSqlServer
                    WsShemaSgbd = ""
                    WsDatesSgbd = VI.FormatDateSgbdr.DateStandard
                    WsDecimalSgbd = VI.Virgule
                Else
                    WsTypeSgbd = PointeurDatabase.TypeduSgbd
                    WsShemaSgbd = PointeurDatabase.Schema
                    WsDatesSgbd = PointeurDatabase.FormatDesDates
                    WsDecimalSgbd = PointeurDatabase.SymboleDecimal
                End If
                WsRhFonction = New Virtualia.Systeme.Fonctions.Generales

            End Sub
        End Class

        Public Class SqlInterne_Structure
            Private WsNumPtdeVue As Integer
            Private WsOperateur_Liaison As Integer = VI.Operateurs.ET 'ET ou OU
            '*************
            Private WsDateDebut As String
            Private WsDateFin As String
            'Ou
            Private WsNumObjet_DateDebut As Integer
            Private WsNumInfo_DateDebut As Integer
            Private WsNumObjet_DateFin As Integer
            Private WsNumInfo_DateFin As Integer
            '************
            Private WsLettreAlias As String = ""
            Private WsSiSelectMinPlutotQueMax As Boolean = False

            Private WsSiJointureGroupeBy As Boolean = False
            Private WsSiSansControleDateFin As Boolean = False
            Private WsSiSansControleDateEffet As Boolean = False

            Private WsSiForcerDistinct As Boolean = False

            Private WsSiHistorique As Boolean = False
            Private WsSiPasdeTriNoDossier As Boolean = False
            Private WsSiPasdExtractionNoDossier As Boolean = False

            Private WsIdentifiantDossier As Integer 'S�lection sur un seul dossier
            Private WsListeIdentifiants As List(Of Integer) = Nothing 'S�lection sur plusieurs

            Public ReadOnly Property Point_de_Vue As Integer
                Get
                    Return WsNumPtdeVue
                End Get
            End Property

            Public Property Operateur_Liaison As Integer
                Get
                    Return WsOperateur_Liaison
                End Get
                Set(value As Integer)
                    WsOperateur_Liaison = value
                End Set
            End Property

            Public Property Date_Debut As String
                Get
                    Return WsDateDebut
                End Get
                Set(value As String)
                    WsDateDebut = value
                End Set
            End Property

            Public Property Date_Fin As String
                Get
                    Return WsDateFin
                End Get
                Set(value As String)
                    WsDateFin = value
                End Set
            End Property

            Public Property Numero_Objet_DateDebut As Integer
                Get
                    Return WsNumObjet_DateDebut
                End Get
                Set(value As Integer)
                    WsNumObjet_DateDebut = value
                End Set
            End Property

            Public Property Numero_Info_DateDebut As Integer
                Get
                    Return WsNumInfo_DateDebut
                End Get
                Set(value As Integer)
                    WsNumInfo_DateDebut = value
                End Set
            End Property

            Public Property Numero_Objet_DateFin As Integer
                Get
                    Return WsNumObjet_DateFin
                End Get
                Set(value As Integer)
                    WsNumObjet_DateFin = value
                End Set
            End Property

            Public Property Numero_Info_DateFin As Integer
                Get
                    Return WsNumInfo_DateFin
                End Get
                Set(value As Integer)
                    WsNumInfo_DateFin = value
                End Set
            End Property

            Public Property Lettre_Alias As String
                Get
                    Return WsLettreAlias
                End Get
                Set(value As String)
                    WsLettreAlias = value
                End Set
            End Property

            Public Property SiSelectMin_PlutotQue_Max As Boolean
                Get
                    Return WsSiSelectMinPlutotQueMax
                End Get
                Set(value As Boolean)
                    WsSiSelectMinPlutotQueMax = value
                End Set
            End Property

            Public Property SiJointurePlutotQueSelectMax() As Boolean
                Get
                    Return WsSiJointureGroupeBy
                End Get
                Set(ByVal Value As Boolean)
                    WsSiJointureGroupeBy = Value
                End Set
            End Property

            Public Property SiPasdeControleDatedeFin() As Boolean
                Get
                    Return WsSiSansControleDateFin
                End Get
                Set(ByVal Value As Boolean)
                    WsSiSansControleDateFin = Value
                End Set
            End Property

            ''**** AKR Ajout critere de non selection de la date d'effet
            Public Property SiPasdeControleDatedeEffet() As Boolean
                Get
                    Return WsSiSansControleDateEffet
                End Get
                Set(ByVal Value As Boolean)
                    WsSiSansControleDateEffet = Value
                End Set
            End Property

            Public Property SiForcerClauseDistinct() As Boolean
                Get
                    Return WsSiForcerDistinct
                End Get
                Set(ByVal Value As Boolean)
                    WsSiForcerDistinct = Value
                End Set
            End Property

            Public Property SiPasdeTriSurIdeDossier() As Boolean
                Get
                    Return WsSiPasdeTriNoDossier
                End Get
                Set(ByVal Value As Boolean)
                    WsSiPasdeTriNoDossier = Value
                End Set
            End Property

            Public Property SiPasdExtractionIdeDossier() As Boolean
                Get
                    Return WsSiPasdExtractionNoDossier
                End Get
                Set(ByVal Value As Boolean)
                    WsSiPasdExtractionNoDossier = Value
                End Set
            End Property

            Public Property SiHistoriquedeSituation() As Boolean
                Get
                    Return WsSiHistorique
                End Get
                Set(ByVal Value As Boolean)
                    WsSiHistorique = Value
                End Set
            End Property

            Public Property IdentifiantDossier() As Integer
                Get
                    Return WsIdentifiantDossier
                End Get
                Set(ByVal Value As Integer)
                    WsIdentifiantDossier = Value
                End Set
            End Property

            Public Property PreselectiondIdentifiants() As List(Of Integer)
                Get
                    Return WsListeIdentifiants
                End Get
                Set(ByVal Value As List(Of Integer))
                    If Value Is Nothing Then
                        Exit Property
                    End If
                    WsListeIdentifiants = Value
                End Set
            End Property

            Public ReadOnly Property SiMultiIdentifiants As Boolean
                Get
                    If WsListeIdentifiants Is Nothing OrElse WsListeIdentifiants.Count = 0 Then
                        Return False
                    End If
                    Return True
                End Get
            End Property

            Public Sub New(ByVal PtDeVue As Integer)
                WsNumPtdeVue = PtDeVue
            End Sub
        End Class

        Public Class SqlInterne_Selection
            Private WsNumObjet As Integer
            Private WsNumInfo As Integer
            '
            Private WsOperateur_Liaison As Integer 'ET ou OU
            Private WsOperateur_Comparaison As Integer 'Egalit�, Diff�rence, Inclu, Exclu
            '*************
            Private WsValeurs_AComparer As List(Of String)
            Private WsSiAjouterValeurNulle As Boolean
            'Ou
            Private WsNumObjet_AComparer As Integer 'Pour trouver le Champ de la table � comparer
            Private WsNumInfo_AComparer As Integer
            '************
            Private WsNumIndex As Integer

            Public ReadOnly Property Numero_Objet As Integer
                Get
                    Return WsNumObjet
                End Get
            End Property

            Public ReadOnly Property Numero_Info As Integer
                Get
                    Return WsNumInfo
                End Get
            End Property

            Public Property Valeurs_AComparer As List(Of String)
                Get
                    Return WsValeurs_AComparer
                End Get
                Set(value As List(Of String))
                    WsValeurs_AComparer = value
                End Set
            End Property

            Public Property SiAjouter_ValeurNulle As Boolean
                Get
                    Return WsSiAjouterValeurNulle
                End Get
                Set(value As Boolean)
                    WsSiAjouterValeurNulle = value
                End Set
            End Property

            Public Property Operateur_Liaison As Integer
                Get
                    Return WsOperateur_Liaison
                End Get
                Set(value As Integer)
                    WsOperateur_Liaison = value
                End Set
            End Property

            Public Property Operateur_Comparaison As Integer
                Get
                    Return WsOperateur_Comparaison
                End Get
                Set(value As Integer)
                    WsOperateur_Comparaison = value
                End Set
            End Property

            Public Property Numero_Objet_AComparer As Integer
                Get
                    Return WsNumObjet_AComparer
                End Get
                Set(value As Integer)
                    WsNumObjet_AComparer = value
                End Set
            End Property

            Public Property Numero_Info_AComparer As Integer
                Get
                    Return WsNumInfo_AComparer
                End Get
                Set(value As Integer)
                    WsNumInfo_AComparer = value
                End Set
            End Property

            Public Property Numero_Index As Integer
                Get
                    Return WsNumIndex
                End Get
                Set(value As Integer)
                    WsNumIndex = value
                End Set
            End Property

            Public Sub New(ByVal NoObjet As Integer, ByVal NoInfo As Integer)
                WsNumObjet = NoObjet
                WsNumInfo = NoInfo
                WsOperateur_Liaison = VI.Operateurs.OU
                WsOperateur_Comparaison = VI.Operateurs.Egalite
                WsValeurs_AComparer = Nothing
                WsNumObjet_AComparer = 0
                WsNumInfo_AComparer = 0
            End Sub
        End Class

        Public Class SqlInterne_Extraction
            Private WsNumObjet As Integer
            Private WsNumInfo As Integer
            Private WsNumTri As Integer

            Private WsNumIndex As Integer

            Public ReadOnly Property Numero_Objet As Integer
                Get
                    Return WsNumObjet
                End Get
            End Property

            Public ReadOnly Property Numero_Info As Integer
                Get
                    Return WsNumInfo
                End Get
            End Property

            Public ReadOnly Property Numero_Tri As Integer
                Get
                    Return WsNumTri
                End Get
            End Property

            Public Property Numero_Index As Integer
                Get
                    Return WsNumIndex
                End Get
                Set(value As Integer)
                    WsNumIndex = value
                End Set
            End Property

            Public Sub New(ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal NoTri As Integer)
                WsNumObjet = NoObjet
                WsNumInfo = NoInfo
                WsNumTri = NoTri
            End Sub
        End Class
    End Namespace
End Namespace