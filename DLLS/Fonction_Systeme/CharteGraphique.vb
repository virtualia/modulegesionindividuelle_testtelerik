﻿Option Strict On
Option Explicit On
Option Compare Text
Namespace Fonctions
    Public Class CharteGraphique
        Public Enum ModeleCouleur As Integer
            Standard = 0
            StandardSysref = 1
            VertClair = 2
            Bleue = 3
            BleuViolet = 4
            Rouge = 5
            Jaune = 6
            VertFonce = 7
            Orange = 8
            Violet = 9
            Rose = 10
            Beige = 11
        End Enum

        Public ReadOnly Property ImageTuile(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 1
                        Return "VirImag_BleuVert.jpg"
                    Case 2
                        Return "VirImag_BleuVertGris.jpg"
                    Case 3
                        Return "VirImag_Bleu.jpg"
                    Case 4
                        Return "VirImag_BleuGris.jpg"
                    Case 5
                        Return "VirImag_Violet.jpg"
                    Case 6
                        Return "VirImag_VioletGris.jpg"
                    Case 7
                        Return "VirImag_Mauve.jpg"
                    Case 8
                        Return "VirImag_MauveGris.jpg"
                    Case 9
                        Return "VirImag_Rose.jpg"
                    Case 10
                        Return "VirImag_RoseGris.jpg"
                    Case 11
                        Return "VirImag_Rouge.jpg"
                    Case 12
                        Return "VirImag_RougeGris.jpg"
                    Case 13
                        Return "VirImag_Orange.jpg"
                    Case 14
                        Return "VirImag_OrangeGris.jpg"
                    Case 15
                        Return "VirImag_Marron.jpg"
                    Case 16
                        Return "VirImag_MarronGris.jpg"
                    Case 17
                        Return "VirImag_Or.jpg"
                    Case 18
                        Return "VirImag_OrGris.jpg"
                    Case 19
                        Return "VirImag_JauneVert.jpg"
                    Case 20
                        Return "VirImag_JauneVertGris.jpg"
                    Case 21
                        Return "VirImag_Bronze.jpg"
                    Case 22
                        Return "VirImag_BronzeGris.jpg"
                    Case 23
                        Return "VirImag_Vert.jpg"
                    Case 24
                        Return "VirImag_VertGris.jpg"
                    Case Else
                        Return "VirImag_BleuVert.jpg"
                End Select
            End Get
        End Property

        Public ReadOnly Property CouleurTuile(ByVal Index As Integer) As System.Drawing.Color
            Get
                'Du Plus foncé au plus clair en Bleu-vert
                Select Case Index
                    Case 1, 2, 3
                        Return ConvertCouleur("#1C5151")
                    Case 4, 5, 6
                        Return ConvertCouleur("#0E5F5C")
                    Case 7, 8, 9
                        Return ConvertCouleur("#137A76")
                    Case 10, 11, 12, 22, 23, 24
                        Return ConvertCouleur("#19968D")
                    Case 13, 14, 15, 19, 20, 21
                        Return ConvertCouleur("#1EB3A8")
                    Case 16, 17, 18
                        Return ConvertCouleur("#7EC8BE")
                    Case Else
                        Return ConvertCouleur("#1C5151")
                End Select
            End Get
        End Property

        Public ReadOnly Property CouleurParSonNom(ByVal Index As Integer, ByVal Nom As String) As System.Drawing.Color
            Get
                Select Case Nom
                    Case Is = "Base"                        'Numéro 15
                        Return Base_BackColor(Index)
                    Case Is = "Bordure"                     'Numéro 3
                        Return Etiquette_Bordercolor(Index)
                    Case Is = "Cadre"                       'Numéro 2 Colonne 5
                        Return FondsCadre_BackColor(Index)
                    Case Is = "Donnee_BorderColor"          'Numéro 5
                        Return Donnee_Bordercolor(Index)
                    Case Is = "Etiquette_BackColor"         'Numéro 4
                        Return Etiquette_Backcolor(Index)
                    Case Is = "Etiquette_ForeColor"         'Numéro 17
                        Return Etiquette_Forecolor(Index)
                    Case Is = "Police_Claire"               'Numéro 1
                        Return Police_Claire(Index)
                    Case Is = "Police_Fonce"                'Numéro 18
                        Return Police_Fonce(Index)
                    Case Is = "Selection"                   'Numéro 14
                        Return Selection_BackColor(Index)
                    Case Is = "Sous-Titre"                  'Numéro 8
                        Return SousTitre_BackColor(Index)
                    Case Is = "SysRef_Base"                 'Numéro 6 Colonne 2
                        Return SysRef_Base(Index)
                    Case Is = "SysRef_Bordure"              'Numéro 4 Colonne 2
                        Return SysRef_Bordure(Index)
                    Case Is = "SysRef_ForeColor"            'Numéro 2
                        Return SysRef_ForeColor(Index)
                    Case Is = "SysRef_Text"                 'Numéro 2 Colonne 2
                        Return SysRef_Text(Index)
                    Case Is = "SysRef_Titre"                'Numéro 16
                        Return SysRef_Titre(Index)
                    Case Is = "SysRef_Sous-Titre"           'Numéro 2 Colonne 3
                        Return SysRef_SousTitre(Index)
                    Case Is = "Titre"                       'Numéro 12
                        Return Titre_BackColor(Index)
                End Select
            End Get
        End Property

        Public ReadOnly Property Base_BackColor(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case ModeleCouleur.Standard
                        Return ConvertCouleur("#124545") 'Numéro 15
                    Case ModeleCouleur.StandardSysref
                        Return ConvertCouleur("#173434")
                    Case ModeleCouleur.Beige
                        Return ConvertCouleur("#73571E")
                    Case ModeleCouleur.VertClair
                        Return ConvertCouleur("#516B1C")
                    Case ModeleCouleur.Bleue
                        Return ConvertCouleur("#1A294D")
                    Case ModeleCouleur.BleuViolet
                        Return ConvertCouleur("#281B4F")
                    Case ModeleCouleur.Rouge
                        Return ConvertCouleur("#731E1E")
                    Case ModeleCouleur.Jaune
                        Return ConvertCouleur("#73731E")
                    Case ModeleCouleur.VertFonce
                        Return ConvertCouleur("#185C18")
                    Case ModeleCouleur.Orange
                        Return ConvertCouleur("#73451E")
                    Case ModeleCouleur.Violet
                        Return ConvertCouleur("#3A174D")
                    Case ModeleCouleur.Rose
                        Return ConvertCouleur("#5C183F")
                End Select
            End Get
        End Property

        Public ReadOnly Property Titre_BackColor(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case ModeleCouleur.Standard
                        Return ConvertCouleur("#216B68") 'Numéro 12
                    Case ModeleCouleur.StandardSysref
                        Return ConvertCouleur("#27504F")
                    Case ModeleCouleur.Beige
                        Return ConvertCouleur("#B08736")
                    Case ModeleCouleur.VertClair
                        Return ConvertCouleur("#80A533")
                    Case ModeleCouleur.Bleue
                        Return ConvertCouleur("#2D4476")
                    Case ModeleCouleur.BleuViolet
                        Return ConvertCouleur("#402F79")
                    Case ModeleCouleur.Rouge
                        Return ConvertCouleur("#AF3639")
                    Case ModeleCouleur.Jaune
                        Return ConvertCouleur("#B0AF36")
                    Case ModeleCouleur.VertFonce
                        Return ConvertCouleur("#256C25") 'Numéro 14
                    Case ModeleCouleur.Orange
                        Return ConvertCouleur("#B06C36")
                    Case ModeleCouleur.Violet
                        Return ConvertCouleur("#592976")
                    Case ModeleCouleur.Rose
                        Return ConvertCouleur("#8C2B64")
                End Select
            End Get
        End Property

        Public ReadOnly Property SousTitre_BackColor(ByVal Index As Integer) As System.Drawing.Color
            Get
                'Numéro 8
                Select Case Index
                    Case ModeleCouleur.Standard
                        Return ConvertCouleur("#1EB3A8") 'Numéro 8
                    Case ModeleCouleur.StandardSysref
                        Return ConvertCouleur("#397B76")
                    Case ModeleCouleur.Beige
                        Return ConvertCouleur("#FFB62B")
                    Case ModeleCouleur.VertClair
                        Return ConvertCouleur("#BBF045")
                    Case ModeleCouleur.Bleue
                        Return ConvertCouleur("#6084C6")
                    Case ModeleCouleur.BleuViolet
                        Return ConvertCouleur("#5B43B6")
                    Case ModeleCouleur.Rouge
                        Return ConvertCouleur("#FC2A35")
                    Case ModeleCouleur.Jaune
                        Return ConvertCouleur("#FFFD49")
                    Case ModeleCouleur.VertFonce
                        Return ConvertCouleur("#A0E791") 'Numéro 5
                    Case ModeleCouleur.Orange
                        Return ConvertCouleur("#FF9849")
                    Case ModeleCouleur.Violet
                        Return ConvertCouleur("#833AB1")
                    Case ModeleCouleur.Rose
                        Return ConvertCouleur("#CD3B93")
                End Select
            End Get
        End Property

        Public ReadOnly Property FondsCadre_BackColor(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case ModeleCouleur.Standard
                        Return ConvertCouleur("#E2F5F1") 'Numéro 2 Colonne 5
                        'Return Drawing.Color.Transparent
                    Case ModeleCouleur.StandardSysref
                        Return ConvertCouleur("#9EB0AC")
                    Case ModeleCouleur.Beige
                        Return ConvertCouleur("#FFF8EB") 'Numéro 2 Colonne 5
                    Case ModeleCouleur.VertClair
                        Return ConvertCouleur("#F9FEEA")
                    Case ModeleCouleur.Bleue
                        Return ConvertCouleur("#E4EBF5")
                    Case ModeleCouleur.BleuViolet
                        Return ConvertCouleur("#E8E6F6")
                    Case ModeleCouleur.Rouge
                        Return ConvertCouleur("#F8E5F3")
                    Case ModeleCouleur.Jaune
                        Return ConvertCouleur("#FFFEEB")
                    Case ModeleCouleur.VertFonce
                        Return ConvertCouleur("#EDFBE8")
                    Case ModeleCouleur.Orange
                        Return ConvertCouleur("#FFF3EB")
                    Case ModeleCouleur.Violet
                        Return ConvertCouleur("#EEE4F6")
                    Case ModeleCouleur.Rose
                        Return ConvertCouleur("#FDEAED")
                End Select
            End Get
        End Property

        Public ReadOnly Property Selection_BackColor(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case ModeleCouleur.Standard
                        Return ConvertCouleur("#1C5151") 'Numéro 14
                    Case ModeleCouleur.StandardSysref
                        Return ConvertCouleur("#1F3D3D")
                    Case ModeleCouleur.Beige
                        Return ConvertCouleur("#876A2F")
                    Case ModeleCouleur.VertClair
                        Return ConvertCouleur("#637E2C")
                    Case ModeleCouleur.Bleue
                        Return ConvertCouleur("#25355A")
                    Case ModeleCouleur.BleuViolet
                        Return ConvertCouleur("#34275D")
                    Case ModeleCouleur.Rouge
                        Return ConvertCouleur("#872F2F")
                    Case ModeleCouleur.Jaune
                        Return ConvertCouleur("#87872F")
                    Case ModeleCouleur.VertFonce
                        Return ConvertCouleur("#256C25")
                    Case ModeleCouleur.Orange
                        Return ConvertCouleur("#87572F")
                    Case ModeleCouleur.Violet
                        Return ConvertCouleur("#46225A")
                    Case ModeleCouleur.Rose
                        Return ConvertCouleur("#6C254E")
                End Select
            End Get
        End Property

        Public ReadOnly Property Police_Claire(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case ModeleCouleur.Standard
                        Return ConvertCouleur("#D7FAF3") 'Numéro 1
                    Case ModeleCouleur.StandardSysref
                        Return ConvertCouleur("#D7FAF3")
                    Case ModeleCouleur.Beige
                        Return ConvertCouleur("#FFF2DB")
                    Case ModeleCouleur.VertClair
                        Return ConvertCouleur("#F5FEDB")
                    Case ModeleCouleur.Bleue
                        Return ConvertCouleur("#DAE7FA")
                    Case ModeleCouleur.BleuViolet
                        Return ConvertCouleur("#E1DCFB")
                    Case ModeleCouleur.Rouge
                        Return ConvertCouleur("#FEDBE0")
                    Case ModeleCouleur.Jaune
                        Return ConvertCouleur("#FFFED8")
                    Case ModeleCouleur.VertFonce
                        Return ConvertCouleur("#E2FDD9")
                    Case ModeleCouleur.Orange
                        Return ConvertCouleur("#FFE9DB")
                    Case ModeleCouleur.Violet
                        Return ConvertCouleur("#ECDAFA")
                    Case ModeleCouleur.Rose
                        Return ConvertCouleur("#FCD8F1")
                End Select
            End Get
        End Property

        Public ReadOnly Property Police_Fonce(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case ModeleCouleur.Standard
                        Return ConvertCouleur("#142425")  'Numéro 18
                    Case ModeleCouleur.StandardSysref
                        Return ConvertCouleur("#121B1C")
                    Case ModeleCouleur.Beige
                        Return ConvertCouleur("#3D3421")
                    Case ModeleCouleur.VertClair
                        Return ConvertCouleur("#30391F")
                    Case ModeleCouleur.Bleue
                        Return ConvertCouleur("#181D29")
                    Case ModeleCouleur.BleuViolet
                        Return ConvertCouleur("#1D192A")
                    Case ModeleCouleur.Rouge
                        Return ConvertCouleur("#3D2221")
                    Case ModeleCouleur.Jaune
                        Return ConvertCouleur("#3D3D21")
                    Case ModeleCouleur.VertFonce
                        Return ConvertCouleur("#1A301B")
                    Case ModeleCouleur.Orange
                        Return ConvertCouleur("#3D2E21")
                    Case ModeleCouleur.Violet
                        Return ConvertCouleur("#231729")
                    Case ModeleCouleur.Rose
                        Return ConvertCouleur("#321B27")
                End Select
            End Get
        End Property

        Public ReadOnly Property Donnee_Bordercolor(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case ModeleCouleur.Standard
                        Return ConvertCouleur("#7EC8BE") 'Numéro 5
                    Case ModeleCouleur.StandardSysref
                        Return ConvertCouleur("#6C9690")
                    Case ModeleCouleur.Beige
                        Return ConvertCouleur("#FFDDA1")
                    Case ModeleCouleur.VertClair
                        Return ConvertCouleur("#DCF69B")
                    Case ModeleCouleur.Bleue
                        Return ConvertCouleur("#88A2CD")
                    Case ModeleCouleur.BleuViolet
                        Return ConvertCouleur("#9A8DD1")
                    Case ModeleCouleur.Rouge
                        Return ConvertCouleur("#FA9EA6")
                    Case ModeleCouleur.Jaune
                        Return ConvertCouleur("#FFFDA1")
                    Case ModeleCouleur.VertFonce
                        Return ConvertCouleur("#ADE791")
                    Case ModeleCouleur.Orange
                        Return ConvertCouleur("#FFC8A1")
                    Case ModeleCouleur.Violet
                        Return ConvertCouleur("#B087CE")
                    Case ModeleCouleur.Rose
                        Return ConvertCouleur("#DD8BC1")
                End Select
            End Get
        End Property

        Public ReadOnly Property Etiquette_Backcolor(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case ModeleCouleur.Standard
                        Return ConvertCouleur("#98D4CA") 'Numéro 4
                    Case ModeleCouleur.StandardSysref
                        Return ConvertCouleur("7D9F99")
                    Case ModeleCouleur.Beige
                        Return ConvertCouleur("#FFE5B7")
                    Case ModeleCouleur.VertClair
                        Return ConvertCouleur("#E5F8B2")
                    Case ModeleCouleur.Bleue
                        Return ConvertCouleur("#A0B5D7")
                    Case ModeleCouleur.BleuViolet
                        Return ConvertCouleur("#AEA5DB")
                    Case ModeleCouleur.Rouge
                        Return ConvertCouleur("#FAB3BC")
                    Case ModeleCouleur.Jaune
                        Return ConvertCouleur("#FFFDB7")
                    Case ModeleCouleur.VertFonce
                        Return ConvertCouleur("#CBF2BE") 'Numéro 3
                    Case ModeleCouleur.Orange
                        Return ConvertCouleur("#FFD487")
                    Case ModeleCouleur.Violet
                        Return ConvertCouleur("#C09FD8")
                    Case ModeleCouleur.Rose
                        Return ConvertCouleur("#E3A3CE")
                End Select
            End Get
        End Property

        Public ReadOnly Property Etiquette_Forecolor(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case ModeleCouleur.Standard
                        Return ConvertCouleur("#0F2F30") 'Numéro 17
                    Case ModeleCouleur.StandardSysref
                        Return ConvertCouleur("#112324")
                    Case ModeleCouleur.Beige
                        Return ConvertCouleur("#4F3D18")
                    Case ModeleCouleur.VertClair
                        Return ConvertCouleur("#384A17")
                    Case ModeleCouleur.Bleue
                        Return ConvertCouleur("#141E35")
                    Case ModeleCouleur.BleuViolet
                        Return ConvertCouleur("#1D1537")
                    Case ModeleCouleur.Rouge
                        Return ConvertCouleur("#4F1A18")
                    Case ModeleCouleur.Jaune
                        Return ConvertCouleur("#4E4F18")
                    Case ModeleCouleur.VertFonce
                        Return ConvertCouleur("#133E16")
                    Case ModeleCouleur.Orange
                        Return ConvertCouleur("#4F3218")
                    Case ModeleCouleur.Violet
                        Return ConvertCouleur("#291235")
                    Case ModeleCouleur.Rose
                        Return ConvertCouleur("#40142C")
                End Select
            End Get
        End Property

        Public ReadOnly Property Etiquette_Bordercolor(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case ModeleCouleur.Standard
                        Return ConvertCouleur("#B0E0D7") 'Numéro 3
                    Case ModeleCouleur.StandardSysref
                        Return ConvertCouleur("#8DA8A3")
                    Case ModeleCouleur.Beige
                        Return ConvertCouleur("#FFEBC8")
                    Case ModeleCouleur.VertClair
                        Return ConvertCouleur("#ECFAC5")
                    Case ModeleCouleur.Bleue
                        Return ConvertCouleur("#B6C7E2")
                    Case ModeleCouleur.BleuViolet
                        Return ConvertCouleur("#C0BAE5")
                    Case ModeleCouleur.Rouge
                        Return ConvertCouleur("#FBC5CD")
                    Case ModeleCouleur.Jaune
                        Return ConvertCouleur("#FFFDC8")
                    Case ModeleCouleur.VertFonce
                        Return ConvertCouleur("#B0E0D7") 'Std
                    Case ModeleCouleur.Orange
                        Return ConvertCouleur("#FFDEC8")
                    Case ModeleCouleur.Violet
                        Return ConvertCouleur("#CEB6E3")
                    Case ModeleCouleur.Rose
                        Return ConvertCouleur("#EAB8DB")
                End Select
            End Get
        End Property

        Public ReadOnly Property SysRef_Base(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case ModeleCouleur.Standard, ModeleCouleur.StandardSysref
                        Return ConvertCouleur("#528F89") 'Numéro 6 Colonne 2
                    Case ModeleCouleur.Beige
                        Return ConvertCouleur("#BFA36D")
                    Case ModeleCouleur.VertClair
                        Return ConvertCouleur("#A0B869")
                    Case ModeleCouleur.Bleue
                        Return ConvertCouleur("#5B7095")
                    Case ModeleCouleur.BleuViolet
                        Return ConvertCouleur("#6A5F98")
                    Case ModeleCouleur.Rouge
                        Return ConvertCouleur("#BC6C71")
                    Case ModeleCouleur.Jaune
                        Return ConvertCouleur("#BFBE48")
                    Case ModeleCouleur.VertFonce
                        Return ConvertCouleur("#6BA961")
                    Case ModeleCouleur.Orange
                        Return ConvertCouleur("#BF906D")
                    Case ModeleCouleur.Violet
                        Return ConvertCouleur("#7D5995")
                    Case ModeleCouleur.Rose
                        Return ConvertCouleur("#A35D89")
                End Select
            End Get
        End Property

        Public ReadOnly Property SysRef_Bordure(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case ModeleCouleur.Standard, ModeleCouleur.StandardSysref
                        Return ConvertCouleur("#7D9F99") 'Numéro 4 Colonne 2
                    Case ModeleCouleur.Beige
                        Return ConvertCouleur("#BFB197")
                    Case ModeleCouleur.VertClair
                        Return ConvertCouleur("#AFBA93")
                    Case ModeleCouleur.Bleue
                        Return ConvertCouleur("#828EA1")
                    Case ModeleCouleur.BleuViolet
                        Return ConvertCouleur("#8B86A4")
                    Case ModeleCouleur.Rouge
                        Return ConvertCouleur("#BC9498")
                    Case ModeleCouleur.Jaune
                        Return ConvertCouleur("#BFBE97")
                    Case ModeleCouleur.VertFonce
                        Return ConvertCouleur("#94B18C")
                    Case ModeleCouleur.Orange
                        Return ConvertCouleur("#BFA797")
                    Case ModeleCouleur.Violet
                        Return ConvertCouleur("#9482A2")
                    Case ModeleCouleur.Rose
                        Return ConvertCouleur("#AA869F")
                End Select
            End Get
        End Property

        Public ReadOnly Property SysRef_ForeColor(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case ModeleCouleur.Standard, ModeleCouleur.StandardSysref
                        Return ConvertCouleur("#CAEBE4") 'Numéro 2
                    Case ModeleCouleur.Beige
                        Return ConvertCouleur("#FFF2DB")
                    Case ModeleCouleur.VertClair
                        Return ConvertCouleur("#F3FCD9")
                    Case ModeleCouleur.Bleue
                        Return ConvertCouleur("#CEDAEC")
                    Case ModeleCouleur.BleuViolet
                        Return ConvertCouleur("#D5D1EE")
                    Case ModeleCouleur.Rouge
                        Return ConvertCouleur("#FCD9DE")
                    Case ModeleCouleur.Jaune
                        Return ConvertCouleur("#FFFDDB")
                    Case ModeleCouleur.VertFonce
                        Return ConvertCouleur("#DEF7D4")
                    Case ModeleCouleur.Orange
                        Return ConvertCouleur("#FFE9DB")
                    Case ModeleCouleur.Violet
                        Return ConvertCouleur("#DECEED")
                    Case ModeleCouleur.Rose
                        Return ConvertCouleur("#F1CFE7")
                End Select
            End Get
        End Property

        Public ReadOnly Property SysRef_Text(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case ModeleCouleur.Standard, ModeleCouleur.StandardSysref
                        Return ConvertCouleur("#9EB0AC") 'Numéro 2 Colonne 2
                    Case ModeleCouleur.Beige
                        Return ConvertCouleur("#BFB8AB")
                    Case ModeleCouleur.VertClair
                        Return ConvertCouleur("#B8BDA9")
                    Case ModeleCouleur.Bleue
                        Return ConvertCouleur("#A0A7B1")
                    Case ModeleCouleur.BleuViolet
                        Return ConvertCouleur("#A4A2B2")
                    Case ModeleCouleur.Rouge
                        Return ConvertCouleur("#BDA9AC")
                    Case ModeleCouleur.Jaune
                        Return ConvertCouleur("#BFBEAB")
                    Case ModeleCouleur.VertFonce
                        Return ConvertCouleur("#ABB9A6")
                    Case ModeleCouleur.Orange
                        Return ConvertCouleur("#BFB3AB")
                    Case ModeleCouleur.Violet
                        Return ConvertCouleur("#A9A0B1")
                    Case ModeleCouleur.Rose
                        Return ConvertCouleur("#85A2AF")
                End Select
            End Get
        End Property

        Public ReadOnly Property SysRef_Titre(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case ModeleCouleur.Standard, ModeleCouleur.StandardSysref
                        Return ConvertCouleur("#083A3A") 'Numéro 16
                    Case ModeleCouleur.Beige
                        Return ConvertCouleur("#61450D")
                    Case ModeleCouleur.VertClair
                        Return ConvertCouleur("#405A0C")
                    Case ModeleCouleur.Bleue
                        Return ConvertCouleur("#0F1E41")
                    Case ModeleCouleur.BleuViolet
                        Return ConvertCouleur("#1C1043")
                    Case ModeleCouleur.Rouge
                        Return ConvertCouleur("#610D0D")
                    Case ModeleCouleur.Jaune
                        Return ConvertCouleur("#61610D")
                    Case ModeleCouleur.VertFonce
                        Return ConvertCouleur("#0B4D0B")
                    Case ModeleCouleur.Orange
                        Return ConvertCouleur("#61330D")
                    Case ModeleCouleur.Violet
                        Return ConvertCouleur("#2E0C41")
                    Case ModeleCouleur.Rose
                        Return ConvertCouleur("#4E0B30")
                End Select
            End Get
        End Property

        Public ReadOnly Property SysRef_SousTitre(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case ModeleCouleur.Standard, ModeleCouleur.StandardSysref
                        Return ConvertCouleur("#B0E0D7") 'Numéro 2 Colonne 3
                    Case ModeleCouleur.Beige
                        Return ConvertCouleur("#FFEBC8")
                    Case ModeleCouleur.VertClair
                        Return ConvertCouleur("#ECFAC5")
                    Case ModeleCouleur.Bleue
                        Return ConvertCouleur("#B6C7E2")
                    Case ModeleCouleur.BleuViolet
                        Return ConvertCouleur("#50449B")
                    Case ModeleCouleur.Rouge
                        Return ConvertCouleur("#A44655")
                    Case ModeleCouleur.Jaune
                        Return ConvertCouleur("#A6A247")
                    Case ModeleCouleur.VertFonce
                        Return ConvertCouleur("#5EA045")
                    Case ModeleCouleur.Orange
                        Return ConvertCouleur("#A66C47")
                    Case ModeleCouleur.Violet
                        Return ConvertCouleur("#71439A")
                    Case ModeleCouleur.Rose
                        Return ConvertCouleur("#9C4384")
                End Select
            End Get
        End Property

        Private Function ConvertCouleur(ByVal Valeur As String) As System.Drawing.Color
            Dim R As Integer
            Dim G As Integer
            Dim B As Integer
            Select Case Valeur.Length
                Case Is = 6
                    R = CInt(Val("&H" & Strings.Left(Valeur, 2) & "&"))
                    G = CInt(Val("&H" & Strings.Mid(Valeur, 3, 2) & "&"))
                    B = CInt(Val("&H" & Strings.Right(Valeur, 2) & "&"))
                    Return System.Drawing.Color.FromArgb(R, G, B)
                Case Is = 7
                    R = CInt(Val("&H" & Strings.Mid(Valeur, 2, 2) & "&"))
                    G = CInt(Val("&H" & Strings.Mid(Valeur, 4, 2) & "&"))
                    B = CInt(Val("&H" & Strings.Right(Valeur, 2) & "&"))
                    Return System.Drawing.Color.FromArgb(R, G, B)
                Case Else
                    Return Drawing.Color.White
            End Select
        End Function
    End Class
End Namespace
