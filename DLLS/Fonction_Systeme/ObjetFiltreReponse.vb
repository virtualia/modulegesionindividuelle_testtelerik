﻿Option Strict On
Option Explicit On
Option Compare Text
Imports System.IO

Namespace Outils
    Public Class FiltreReponseParMemoryStream
        Inherits MemoryStream

        Private WsOutputStream As Stream = Nothing
        Private WsFichierHtml As FileStream

        Public Sub New(ByVal FluxOutput As Stream, ByVal NomFichier As String)
            WsOutputStream = FluxOutput
            If My.Computer.FileSystem.FileExists(NomFichier) Then
                Try
                    My.Computer.FileSystem.DeleteFile(NomFichier)
                Catch ex As Exception
                    WsFichierHtml = Nothing
                    Exit Sub
                End Try
            End If
            Try
                WsFichierHtml = New FileStream(NomFichier, FileMode.OpenOrCreate, FileAccess.Write)
            Catch ex As Exception
                WsFichierHtml = Nothing
            End Try
        End Sub

        Public Overrides Sub Write(ByVal Buffer As Byte(), ByVal Offset As Integer, ByVal Count As Integer)
            Try
                'Response - Navigateur
                WsOutputStream.Write(Buffer, Offset, Count)

                'Ecriture du fichier pour utilisation postérieure (PDF)
                If WsFichierHtml Is Nothing Then
                    Exit Sub
                End If
                WsFichierHtml.Write(Buffer, Offset, Count)
            Catch ex As Exception
                Exit Try
            End Try
        End Sub

        Public Overrides Function Read(ByVal Buffer As Byte(), ByVal Offset As Integer, ByVal Count As Integer) As Integer
            Try
                Return WsOutputStream.Read(Buffer, Offset, Count)
            Catch ex As Exception
                Return 0
            End Try
        End Function

        Public Overrides Sub Flush()
            Try
                WsOutputStream.Flush()
            Catch ex As Exception
                Exit Sub
            End Try
        End Sub

        Public Overrides Sub Close()
            Try
                WsOutputStream.Close()
                WsFichierHtml.Close()
            Catch ex As Exception
                Exit Sub
            End Try
        End Sub
    End Class

    Public Class FiltreReponseParFileStream
        Inherits Stream

        Private WsFichierHtml As FileStream
        Private WsSink As Stream
        Private WsPosition As Long

        Sub New(ByVal sink As Stream, ByVal NomFichier As String)
            Try
                WsSink = sink
                WsFichierHtml = New FileStream(NomFichier, FileMode.OpenOrCreate, FileAccess.Write)
            Catch ex As Exception
                Return
            End Try
        End Sub

        'The following members of Stream must be overridden.
        Public Overrides ReadOnly Property CanRead() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property CanSeek() As Boolean
            Get
                Return False
            End Get

        End Property

        Public Overrides ReadOnly Property CanWrite() As Boolean
            Get
                Return False
            End Get
        End Property

        Public Overrides ReadOnly Property Length() As Long
            Get
                Return 0
            End Get
        End Property

        Public Overrides Property Position() As Long
            Get
                Return WsPosition
            End Get
            Set(ByVal Value As Long)
                WsPosition = Value
            End Set
        End Property

        Public Overrides Function Seek(ByVal offset As Long, ByVal direction As SeekOrigin) As Long
            Return 0
        End Function

        Public Overrides Sub SetLength(ByVal length As Long)
            WsSink.SetLength(length)
        End Sub

        Public Overrides Sub Close()
            Try
                WsSink.Close()
                WsFichierHtml.Close()
            Catch ex As Exception
                Exit Sub
            End Try
        End Sub

        Public Overrides Sub Flush()
            Try
                WsSink.Flush()
            Catch ex As Exception
                Exit Sub
            End Try
        End Sub

        Public Overrides Function Read(ByVal buffer() As Byte, ByVal offset As Int32, ByVal count As Int32) As Int32
            Try
                Return WsSink.Read(buffer, offset, count)
            Catch ex As Exception
                Return 0
            End Try
        End Function

        Public Overrides Sub Write(ByVal buffer() As Byte, ByVal offset As Int32, ByVal count As Int32)
            Try
                ' Write out the response to the browser.
                WsSink.Write(buffer, 0, count)
                ' Write out the response to the file.
                WsFichierHtml.Write(buffer, 0, count)
            Catch ex As Exception
                Exit Sub
            End Try
        End Sub
    End Class
End Namespace