Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Fonctions
    Public Class Generales
        Private Const SysAssurance As String = "1209195119580912"
        Private Const SysClefPrivee As String = "X1STaAT7+BdrxaewH2nGA/d8AX6RW/1auZTDe1fWs5Y="
        Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates
        '
        Private OptFormatAlpha(2) As String
        Private OptFormatNum(6) As String
        Private OptFormatDate(11) As String

        Private Const Cst_Symbole_SigneInferieur As String = "&lt;"
        Private Const Cst_Symbole_SigneSuperieur As String = "&gt;"
        Private Const Cst_Symbole_Quote As String = "&apos;"
        Private Const Cst_Symbole_DoubleQuote As String = "&quot;"
        Private Const Cst_Symbole_EtCommercial As String = "&amp;"

        Public ReadOnly Property SiNomdeFichierValide(ByVal NomFichier As String) As Boolean
            Get
                Dim Indice As Integer
                Dim strInvalidChars As String

                If NomFichier.Length > 255 Then
                    Return False
                End If
                '
                strInvalidChars = "\/:*?"

                For Indice = 1 To strInvalidChars.Length
                    If Strings.InStr(NomFichier, Strings.Mid(strInvalidChars, Indice, 1)) <> 0 Then
                        Return False
                    End If
                Next Indice

                Return True

            End Get
        End Property

        Public ReadOnly Property MontantEdite(ByVal Valeur As String, Optional ByVal NbDecimales As Integer = 2) As String
            Get
                Dim Resultat As Double
                Resultat = ConversionDouble(Valeur)
                Return MontantEdite(Resultat, NbDecimales)
            End Get
        End Property

        Public ReadOnly Property MontantEdite(ByVal Montant As Double, Optional ByVal NbDecimales As Integer = 2, Optional ByVal BlancSiZero As Boolean = False) As String
            Get
                If BlancSiZero = True AndAlso Montant = 0 Then
                    Return ""
                End If
                Select Case NbDecimales
                    Case 0
                        Return Strings.Format(Math.Round(Montant, 0), "###,###,##0")
                    Case 1
                        Return Strings.Format(Math.Round(Montant, 1), "###,###,##0.0")
                    Case 2
                        Return Strings.Format(Math.Round(Montant, 2), "###,###,##0.00")
                    Case 3
                        Return Strings.Format(Math.Round(Montant, 3), "###,###,##0.000")
                    Case 4
                        Return Strings.Format(Math.Round(Montant, 4), "###,###,##0.0000")
                    Case 5
                        Return Strings.Format(Math.Round(Montant, 4), "###,###,##0.00000")
                    Case 6
                        Return Strings.Format(Math.Round(Montant, 4), "###,###,##0.000000")
                    Case Else
                        Return Strings.Format(Math.Round(Montant, NbDecimales), "###,###,##0.000000000")
                End Select
            End Get
        End Property

        Public ReadOnly Property MontantStocke(ByVal Valeur As String) As String
            Get
                Dim Resultat As Double
                Resultat = ConversionDouble(Valeur)
                Return Strings.Format(Resultat, "#0.00")
            End Get
        End Property

        Public Function ControleNumericite(ByVal Chaine As String) As String
            Dim Indice As Integer
            Dim Rejet As Boolean

            Rejet = False
            For Indice = 1 To Chaine.Length
                If (Strings.Mid(Chaine, Indice, 1) <> VI.PointFinal And Strings.Mid(Chaine, Indice, 1) <> VI.Virgule) And (Strings.Mid(Chaine, Indice, 1) < "0" Or Strings.Mid(Chaine, Indice, 1) > "9") Then
                    Rejet = True
                    Exit For
                End If
            Next Indice
            If Rejet = True Then
                Return ""
            Else
                Return Chaine
            End If
        End Function

        Public Function ControleZoneNumerique(ByVal Touche As String) As Integer
            If Touche = VI.PointFinal Then
                Return 44
            End If
            If Touche = VI.Virgule Then
                Return Asc(Touche)
            End If
            If (Touche < "0" Or Touche > "9") And (Asc(Touche) <> 8) Then
                Return 0
            End If
            Return Asc(Touche)
        End Function

        Public Function Lettre1Capi(ByVal Entree As String, ByVal Param As Integer) As String
            ' modification de la 1ere lettre de chaque mot du texte
            ' en entree en majuscule
            Dim Blan As String
            Dim Tiret As String
            Dim Texte As String
            Dim Lettre As String
            Dim NewChaine As String
            Dim Indice As Integer
            Dim Pbl As Integer

            Blan = Chr(32)
            Tiret = Chr(45)
            Texte = Entree.ToLower.Trim
            If Texte.Length = 0 Then Return "" : Exit Function
            Lettre = Strings.Left(Texte, 1).ToUpper
            Mid(Texte, 1, 1) = Lettre
            Select Case Param
                Case Is = 0
                    Return Texte
                    Exit Function
                Case Is = 2 'Particule
                    Texte = Entree.ToUpper.Trim
                    NewChaine = Texte.Replace("DE ", "de ")
                    Texte = NewChaine
                    NewChaine = Texte.Replace("DU ", "du ")
                    Texte = NewChaine
                    NewChaine = Texte.Replace("D'", "d'")
                    Return NewChaine
                    Exit Function
            End Select
            For Indice = 1 To Texte.Length
                Pbl = Strings.InStr(Indice, Texte, Blan)
                Select Case Pbl
                    Case Is > 0
                        Lettre = Strings.Right(Texte, Texte.Length - Pbl)
                        Lettre = Lettre.ToUpper
                        Select Case Pbl + 1
                            Case Is < Texte.Length
                                Mid(Texte, Pbl + 1, 1) = Lettre
                        End Select
                End Select
                Pbl = Strings.InStr(Indice, Texte, Tiret)
                Select Case Pbl
                    Case Is > 0
                        Lettre = Strings.Right(Texte, Texte.Length - Pbl)
                        Lettre = Lettre.ToUpper
                        Select Case Pbl + 1
                            Case Is < Texte.Length
                                Mid(Texte, Pbl + 1, 1) = Lettre
                        End Select
                End Select
            Next Indice
            Return Texte
        End Function

        Public Function TransChiffreLettre(ByVal Chiffre As String) As String
            Select Case Chiffre
                Case "1"
                    Return "A"
                Case "2"
                    Return "B"
                Case "3"
                    Return "C"
                Case "4"
                    Return "D"
                Case "5"
                    Return "E"
                Case "6"
                    Return "F"
                Case "7"
                    Return "G"
                Case "8"
                    Return "H"
                Case "9"
                    Return "I"
                Case Else
                    Return Chiffre
            End Select
        End Function

        Public Function TransLettreChiffre(ByVal Lettre As String) As Integer
            Select Case Lettre
                Case "A"
                    Return 1
                Case "B"
                    Return 2
                Case "C"
                    Return 3
                Case "D"
                    Return 4
                Case "E"
                    Return 5
                Case "F"
                    Return 6
                Case "G"
                    Return 7
                Case "H"
                    Return 8
                Case "I"
                    Return 9
                Case Else
                    Return 0
            End Select
        End Function

        Public ReadOnly Property PositionDansAlphabet(ByVal Lettre As String) As Integer
            Get
                Select Case Lettre
                    Case "A"
                        Return 1
                    Case "B"
                        Return 2
                    Case "C"
                        Return 3
                    Case "D"
                        Return 4
                    Case "E"
                        Return 5
                    Case "F"
                        Return 6
                    Case "G"
                        Return 7
                    Case "H"
                        Return 8
                    Case "I"
                        Return 9
                    Case "J"
                        Return 10
                    Case "K"
                        Return 11
                    Case "L"
                        Return 12
                    Case "M"
                        Return 13
                    Case "N"
                        Return 14
                    Case "O"
                        Return 15
                    Case "P"
                        Return 16
                    Case "Q"
                        Return 17
                    Case "R"
                        Return 18
                    Case "S"
                        Return 19
                    Case "T"
                        Return 20
                    Case "U"
                        Return 21
                    Case "V"
                        Return 22
                    Case "W"
                        Return 23
                    Case "X"
                        Return 24
                    Case "Y"
                        Return 25
                    Case "Z"
                        Return 26
                    Case Else
                        Return 0
                End Select
            End Get
        End Property

        Public Function VerificationCleUrssaf(ByVal Numero As String, ByVal Cle As String) As Boolean
            Dim WsDivise As Double
            Dim WsCle As Integer
            Dim Corsica As Integer

            If IsNumeric(Cle) = False Then
                Return False
            End If
            If IsNumeric(Numero) = False Then
                Select Case Strings.Mid(Numero, 7, 1)
                    Case "A", "B"
                        Corsica = 0
                    Case Else
                        If ConversionDouble(Cle, 0) = 0 Then
                            Return True
                        Else
                            Return False
                        End If
                End Select
            End If
            Try
                Corsica = 0
                Select Case Numero.Length
                    Case Is > 8
                        Select Case Strings.Mid(Numero, 7, 1)
                            Case Is = "A"
                                Mid(Numero, 7, 1) = "0"
                                Corsica = 1000000
                            Case Is = "B"
                                Mid(Numero, 7, 1) = "0"
                                Corsica = 2000000
                        End Select
                End Select
                WsDivise = Int((CDbl(Numero) - Corsica) / 97)
                WsCle = CInt(97 - ((CDbl(Numero) - Corsica) - (WsDivise * 97)))
                If WsCle = CInt(Cle) Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function VerificationNoUrssaf(ByVal Numero As String, ByVal Sexe As String, ByVal DatNai As String, ByVal Departement As String) As String
            Dim Char1 As String
            Dim Char23 As String
            Dim Char45 As String
            Dim Char67 As String

            Select Case Sexe
                Case ""
                    Char1 = "1"
                Case Else
                    Select Case Strings.Left(Sexe, 1)
                        Case "F"
                            Char1 = "2"
                        Case Else
                            Char1 = "1"
                    End Select
            End Select
            Select Case DatNai
                Case ""
                    Char23 = "00"
                    Char45 = "00"
                Case Else
                    Char23 = Strings.Right(DatNai, 2)
                    Char45 = Strings.Mid(DatNai, 4, 2)
            End Select
            Select Case Departement.Length
                Case 2
                    Char67 = Departement
                Case Else
                    Char67 = "99"
            End Select
            If Numero.Length > 7 Then
                Return Char1 & Char23 & Char45 & Char67 & Strings.Right(Numero, Numero.Length - 7)
            Else
                Return Char1 & Char23 & Char45 & Char67
            End If
        End Function

        Public Function VirgulePoint(ByVal Nombre As String) As String
            Dim Valeur As System.Text.StringBuilder
            Dim Indice As Integer
            Dim IndexVirgule As Integer
            Dim SiNegatif As Boolean = False
            Dim TableauData(0) As String
            '
            If Nombre Is Nothing Then
                Return "0"
            End If
            Nombre = Nombre.Trim
            If Nombre.Length = 0 Then
                Return "0"
            End If
            Select Case Strings.Left(Nombre, 1)
                Case Is = VI.Tiret
                    SiNegatif = True
                    Valeur = New System.Text.StringBuilder
                    Valeur.Append(Strings.Right(Nombre, Nombre.Length - 1))
                    Nombre = Valeur.ToString
            End Select
            If Strings.Left(Nombre, 1) < "0" Or Strings.Left(Nombre, 1) > "9" Then
                Select Case SiNegatif
                    Case False
                        Return Nombre
                    Case True
                        Return VI.Tiret & Nombre
                End Select
            End If
            IndexVirgule = 0
            '
            Erase TableauData
            ReDim TableauData(Nombre.Length)
            For Indice = 0 To TableauData.Count - 1
                Select Case Strings.Mid(Nombre, Indice + 1, 1)
                    Case Is = VI.Virgule, VI.PointFinal, Strings.Space(1)
                        TableauData(Indice) = VI.PointFinal
                        IndexVirgule = Indice
                    Case Else
                        If (Strings.Mid(Nombre, Indice + 1, 1) >= "0" And Strings.Mid(Nombre, Indice + 1, 1) <= "9") Then
                            TableauData(Indice) = Strings.Mid(Nombre, Indice + 1, 1)
                        End If
                End Select
            Next Indice
            Valeur = New System.Text.StringBuilder
            For Indice = 0 To TableauData.Count - 1
                Select Case TableauData(Indice)
                    Case Is = VI.PointFinal
                        Select Case Indice
                            Case Is = IndexVirgule
                                Valeur.Append(TableauData(Indice))
                        End Select
                    Case Else
                        Valeur.Append(TableauData(Indice))
                End Select
            Next Indice
            Select Case SiNegatif
                Case False
                    Return Valeur.ToString
                Case True
                    Return VI.Tiret & Valeur.ToString
            End Select
            Erase TableauData
            Return Valeur.ToString
        End Function

        Public Function VerificationRib(ByVal Banque As String, ByVal Guichet As String, ByVal Compte As String, ByVal Clef As String) As Boolean
            ' verification du RIB ou RIP
            ' si le RIB-RIP est correct : VerificationRib = True
            Dim Lb As Integer
            Dim Lg As Integer
            Dim Lcp As Integer
            Dim Lcl As Integer
            Dim Res As Integer
            Dim Z1 As Double
            Dim Z2 As Double
            Dim Z3 As Double
            Dim Z4 As Double
            Dim Qt As Double
            Dim Reste As Integer
            Dim Cr As Integer
            Dim Indice As Integer
            Dim Zcpt(11) As String

            Lb = 0
            Lg = 0
            Lcp = 0
            Lcl = 0
            Res = 0

            ' controler longueur des 4 zones
            If Banque.Length <> 5 Then Lb = 1 : GoTo ErreurRib
            If Guichet.Length <> 5 Then Lg = 1 : GoTo ErreurRib
            If Compte.Length <> 11 Then Lcp = 1 : GoTo ErreurRib
            If Clef.Length <> 2 Then Lcl = 1 : GoTo ErreurRib

            ' changer lettre en chiffre (rip)
            For Indice = 1 To 11
                Zcpt(Indice) = (Strings.Mid(Compte, Indice, 1))
            Next
            For Indice = 1 To 11
                If Zcpt(Indice) = Strings.Chr(65) Or Zcpt(Indice) = Strings.Chr(74) Then Zcpt(Indice) = Strings.Chr(49)
                If Zcpt(Indice) = Strings.Chr(66) Or Zcpt(Indice) = Strings.Chr(75) Or Zcpt(Indice) = Strings.Chr(83) Then Zcpt(Indice) = Strings.Chr(50)
                If Zcpt(Indice) = Strings.Chr(67) Or Zcpt(Indice) = Strings.Chr(76) Or Zcpt(Indice) = Strings.Chr(84) Then Zcpt(Indice) = Strings.Chr(51)
                If Zcpt(Indice) = Strings.Chr(68) Or Zcpt(Indice) = Strings.Chr(77) Or Zcpt(Indice) = Strings.Chr(85) Then Zcpt(Indice) = Strings.Chr(52)
                If Zcpt(Indice) = Strings.Chr(69) Or Zcpt(Indice) = Strings.Chr(78) Or Zcpt(Indice) = Strings.Chr(86) Then Zcpt(Indice) = Strings.Chr(53)
                If Zcpt(Indice) = Strings.Chr(70) Or Zcpt(Indice) = Strings.Chr(79) Or Zcpt(Indice) = Strings.Chr(87) Then Zcpt(Indice) = Strings.Chr(54)
                If Zcpt(Indice) = Strings.Chr(71) Or Zcpt(Indice) = Strings.Chr(80) Or Zcpt(Indice) = Strings.Chr(88) Then Zcpt(Indice) = Strings.Chr(55)
                If Zcpt(Indice) = Strings.Chr(72) Or Zcpt(Indice) = Strings.Chr(81) Or Zcpt(Indice) = Strings.Chr(89) Then Zcpt(Indice) = Strings.Chr(56)
                If Zcpt(Indice) = Strings.Chr(73) Or Zcpt(Indice) = Strings.Chr(82) Or Zcpt(Indice) = Strings.Chr(90) Then Zcpt(Indice) = Strings.Chr(57)
            Next Indice
            For Indice = 1 To 11
                Mid(Compte, Indice, 1) = Zcpt(Indice)
            Next Indice

            ' alimenter zones de calcul
            Z1 = Val(Banque & (Strings.Left(Guichet, 1)))
            Z2 = Val((Strings.Right(Guichet, 4)) & (Strings.Left(Compte, 2)))
            Z3 = Val(Strings.Mid(Compte, 3, 6))
            Z4 = Val((Strings.Mid(Compte, 9, 3)) & Clef)

            ' calculer le reste de la division du rib par 97
            Qt = System.Convert.ToInt64(Z1) \ 97
            Reste = CInt(Z1 Mod 97)
            Cr = CInt((Reste * 1000000) + Z2)
            Qt = Cr \ 97 : Reste = Cr Mod 97
            Cr = CInt((Reste * 1000000) + Z3)
            Qt = Cr \ 97 : Reste = Cr Mod 97
            Cr = CInt((Reste * 100000) + Z4)
            Qt = Cr \ 97 : Reste = Cr Mod 97
            If Reste > 0 Then
                Res = 1
            Else
                Return True
                Exit Function
            End If
            ' traiter les messages d'erreurs
ErreurRib:
            If Lb = 1 Then Indice = 6
            If Lg = 1 Then Indice = 7
            If Lcp = 1 Then Indice = 8
            If Lcl = 1 Then Indice = 9
            If Res = 1 Then Indice = 5

            Return False
        End Function

        Public Function ReferenceEmploiBud(ByVal NumEmploi As String, ByVal Budget As String, ByVal Branche As String, ByVal Specialite As String, ByVal Corps As String) As String
            '
            'SYS_APPLI = SYS_ETAT
            '
            'Reference de l'emploi budgetaire
            ' XX = Code Budget - Budget avec ()
            ' Blanc
            ' YY = Code Branche d'activite - Branche avec ()
            ' ZZ = Code Specialite - Specialite avec () ou "00" si absent
            ' Blanc
            ' CCCC = Code Corps - Corps avec ()
            ' Blanc
            ' NNNNNNNNNN = Numero de l'emploi budgetaire - NumEmploi
            '
            ReferenceEmploiBud = ""
            '
            Dim CodeRefer As String
            CodeRefer = ""
            Dim CodeBudget As String
            CodeBudget = ""
            Dim CodeBAP As String
            CodeBAP = ""
            Dim CodeCorps As String
            CodeCorps = ""
            Dim P1 As Integer
            P1 = 0
            Dim P2 As Integer
            P2 = 0
            '
            If NumEmploi = "" Then Exit Function
            '
            '1 - Budget
            '
            Select Case Budget
                Case Is <> ""
                    P1 = Strings.InStr(Budget, "(")
                    P2 = Strings.InStr(Budget, ")")
                    If (P1 > 0) And (P2 > 0) Then
                        CodeBudget = Strings.Mid(Budget, P1 + 1, P2 - P1 - 1)
                    End If
            End Select
            CodeRefer = CodeBudget
            '
            '2 - Branche d'activite et specialite
            '
            Select Case Specialite 'Specialite
                Case Is <> ""
                    P1 = Strings.InStr(Specialite, "(")
                    P2 = Strings.InStr(Specialite, ")")
                    If (P1 > 0) And (P2 > 0) Then
                        CodeBAP = Strings.Space(1) & Strings.Mid(Specialite, P1 + 1, P2 - P1 - 1)
                    End If
                Case Else
                    Select Case Branche 'Branche d'activite
                        Case Is <> ""
                            P1 = Strings.InStr(Branche, "(")
                            P2 = Strings.InStr(Branche, ")")
                            If (P1 > 0) And (P2 > 0) Then
                                CodeBAP = Strings.Space(1) & Strings.Mid(Branche, P1 + 1, P2 - P1 - 1) & "00"
                            End If
                    End Select
            End Select
            If CodeBAP <> "" Then
                If CodeRefer <> "" Then
                    CodeRefer = CodeRefer & CodeBAP
                Else
                    CodeRefer = CodeBAP
                End If
            End If
            '
            '3 - Corps
            '
            Select Case Corps
                Case Is <> ""
                    P1 = Strings.InStr(Corps, "(")
                    P2 = Strings.InStr(Corps, ")")
                    If (P1 > 0) And (P2 > 0) Then
                        CodeCorps = Strings.Space(1) & Strings.Mid(Corps, P1 + 1, P2 - P1 - 1)
                    End If
            End Select
            If CodeCorps <> "" Then
                If CodeRefer <> "" Then
                    CodeRefer = CodeRefer & CodeCorps
                Else
                    CodeRefer = CodeCorps
                End If
            End If
            '
            '4 - Numero d'emploi
            '
            CodeRefer = CodeRefer & Strings.Space(1) & NumEmploi
            '
            ReferenceEmploiBud = CodeRefer
            ''
        End Function

        Public Sub New()
            MyBase.New()
            WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates
            Call Initialiser()
        End Sub

        Public Function MontantMonnaieLocale(ByVal Valeur As String, ByVal SymboleMonnaie As String, ByVal DateValeur As String, ByVal SiEuro As Integer) As String
            Dim Resultat As Double
            Dim Taux As Double
            Dim Centime As Integer

            Select Case SymboleMonnaie
                Case "", "�" 'Pas de conversion
                    Taux = 1
                    Centime = 1
                    'Case Else
                    'Taux = SysDialogue.TauxEnEuro(SymboleMonnaie, DateValeur)
                    'Centime = SysDialogue.FormatdelaMonnaie(SymboleMonnaie)
            End Select
            If Taux = 0 Then Taux = 1
            Select Case SiEuro
                Case 1
                    Resultat = ConversionDouble(Valeur) * Taux
                Case 0
                    Resultat = ConversionDouble(Valeur)
            End Select
            Return Strings.Format(Math.Round(Resultat, 2), "###,###,##0.00")
        End Function

        Public Function ConvertisseurListeFiches(Of T As {MetaModele.VIR_FICHE})(ByVal LstFiches As List(Of MetaModele.VIR_FICHE)) As List(Of T)
            Dim LstTypees As List(Of T) = New List(Of T)
            If LstFiches Is Nothing Then
                Return LstTypees
            End If
            LstFiches.ForEach(Sub(Source)
                                  LstTypees.Add(DirectCast(Source, T))
                              End Sub)
            Return LstTypees
        End Function

        Public Function TransformeEnListe(Of T As {MetaModele.VIR_FICHE})(ByVal Fiche As T) As List(Of String)
            Dim TableauData(0) As String
            Dim LstDatas As List(Of String)

            If Fiche Is Nothing Then
                Return Nothing
            End If
            TableauData = Strings.Split(DirectCast(Fiche, T).ContenuTable, VI.Tild, -1)
            LstDatas = New List(Of String)
            For Each Element As String In TableauData
                LstDatas.Add(Element)
            Next
            Return LstDatas
        End Function

        Public Function NomChampSgbdr(ByVal ChampVirtualia As String, ByVal Index As Integer, ByVal TypeDuSgbd As Integer) As String
            Dim VExterne(0) As String
            Dim Chaine As New System.Text.StringBuilder
            Dim ChampCodifie As String
            Dim IndiceI As Integer

            If ChampVirtualia = "" Then
                Return ""
                Exit Function
            End If
            Select Case Strings.Left(ChampVirtualia, 1)
                Case "1" To "9"
                    ChampVirtualia = "V" & ChampVirtualia
            End Select
            Chaine.Append(Strings.Left(ChampVirtualia, 1).ToUpper & Strings.Right(ChampVirtualia, ChampVirtualia.Length - 1).ToLower)
            ChampVirtualia = Chaine.ToString
            Erase VExterne
            ReDim VExterne(ChampVirtualia.Length)
            Chaine.Clear()
            For IndiceI = 1 To VExterne.Count - 1
                VExterne(IndiceI) = Strings.Mid(ChampVirtualia, IndiceI, 1)
                Select Case VExterne(IndiceI)
                    Case Strings.Space(1), "'", ",", "�", "-", "/", "\"
                        VExterne(IndiceI) = "_"
                    Case ">", "<", "="
                        VExterne(IndiceI) = "p"
                    Case "�", "�", "�"
                        VExterne(IndiceI) = "e"
                    Case "�", "�"
                        VExterne(IndiceI) = "a"
                    Case "�", "�"
                        VExterne(IndiceI) = "u"
                    Case "�"
                        VExterne(IndiceI) = "o"
                End Select
            Next IndiceI
            For IndiceI = 1 To VExterne.Count - 1
                Select Case VExterne(IndiceI)
                    Case "."
                        ChampCodifie = ""
                    Case "_"
                        Select Case IndiceI
                            Case Is < VExterne.Count
                                If VExterne(IndiceI + 1) <> "_" Then
                                    Chaine.Append(VExterne(IndiceI))
                                End If
                        End Select
                    Case Else
                        Chaine.Append(VExterne(IndiceI))
                End Select
            Next IndiceI
            ChampCodifie = Chaine.ToString
            Select Case TypeDuSgbd
                Case VI.TypeSgbdrNumeric.SgbdrDb2
                    ChampCodifie = UCase(Chaine.ToString)
                    Select Case Chaine.Length
                        Case Is >= 15
                            ChampCodifie = Strings.Left(Chaine.ToString, 15).ToUpper & "_" & Strings.Format(Index + 1, "00")
                    End Select
                    If Chaine.ToString = "Date" Then ChampCodifie = "VIRDATE"
                    If Chaine.ToString = "Note" Then ChampCodifie = "VIRNOTE"
                    If Chaine.ToString = "Database" Then ChampCodifie = "VIRDATABASE"
                Case VI.TypeSgbdrNumeric.SgbdrProgress, VI.TypeSgbdrNumeric.SgbdrMySql
                    Select Case Chaine.ToString
                        Case Is = "Date", "Note", "Database"
                            ChampCodifie = "Vir" & Chaine.ToString
                        Case Is = "Alias", "Code", "Definition", "Export", "Format", "Horizon", "Information", "Maximum", "Message", "Minimum", "Orientation", "Total", "Type"
                            ChampCodifie = "W" & Chaine.ToString
                        Case Else
                            Select Case Chaine.Length
                                Case Is >= 30
                                    ChampCodifie = Strings.Left(Chaine.ToString, 30)
                            End Select
                    End Select
                Case Else
                    Select Case Chaine.Length
                        Case Is >= 30
                            ChampCodifie = Strings.Left(Chaine.ToString, 30)
                    End Select
                    Select Case Chaine.ToString
                        Case Is = "Date", "Note", "Database"
                            ChampCodifie = "Vir" & Chaine.ToString
                    End Select
            End Select
            Return ChampCodifie
        End Function

        Public Function MiseEnFormeDonneeDB(ByVal InDonnee As String, ByVal TypeDonnee As Integer, ByVal FormatDonnee As Integer, ByVal LgDonnee As Integer) As String
            Dim Pos1 As Integer
            Dim JJ As String
            Dim MM As String
            Dim AaSs As String
            Dim RangHeure As String = ""
            Dim InDonneeT As String = ""
            Dim TableauData(0) As String
            Dim annee As String

            If InDonnee Is Nothing Then
                Return ""
            End If
            InDonnee = InDonnee.Trim
            If InDonnee = "" Then
                Return ""
            End If
            Select Case TypeDonnee
                Case VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeDateTime
                    Select Case InDonnee.Length
                        Case Is > 10
                            RangHeure = ChaineHeureValide(Strings.Right(InDonnee, InDonnee.Length - 11), False)
                            If Strings.Left(RangHeure, 2) = "00" Then
                                RangHeure = ""
                            End If
                            ''**** AKR
                            If Strings.InStr(InDonnee, "AM") > 0 Then
                                RangHeure = ""
                            End If
                            If Strings.InStr(InDonnee, "PM") > 0 Then
                                RangHeure = ""
                            End If
                            ''****
                            InDonnee = Strings.Left(InDonnee, 10)
                        Case Is <= 9
                            Try
                                InDonnee = WsRhDates.DateStandardVirtualia(DateValue(InDonnee).ToShortDateString)
                            Catch ex As Exception
                                Pos1 = 0
                            End Try
                    End Select
                    Pos1 = Strings.InStr(InDonnee, "-") 'Cas Sql Server
                    Select Case Pos1
                        Case Is = 5
                            JJ = Strings.Right(InDonnee, 2)
                            MM = Strings.Mid(InDonnee, 6, 2)
                            AaSs = Strings.Left(InDonnee, 4)
                            InDonnee = JJ & VI.Slash & MM & VI.Slash & AaSs
                    End Select
                    ''**** AKR S�curisation pour dates avec jour tronqu� 
                    TableauData = Strings.Split(InDonnee, "/", -1)
                    Select Case TableauData.Count
                        Case Is = 3
                            annee = Strings.Format(Year(System.DateTime.Now), "0000")
                            If TableauData(2).Length >= 4 Then
                                annee = Strings.Left(TableauData(2), 4)
                            End If

                            InDonnee = Strings.Format(CInt(TableauData(1)), "00") & VI.Slash _
                                            & Strings.Format(CInt(TableauData(0)), "00") & VI.Slash _
                                            & Strings.Format(CInt(annee), "0000")
                    End Select
                    ''****
                    Try
                        InDonnee = WsRhDates.DateStandardVirtualia(InDonnee)
                        If RangHeure <> "" Then
                            InDonnee &= Strings.Space(1) & RangHeure
                        End If
                    Catch ex As Exception
                        InDonnee = ""
                    End Try

                Case VI.NatureDonnee.DonneeHeure
                    Select Case FormatDonnee
                        Case VI.FormatDonnee.Standard, VI.FormatDonnee.Heure
                            Select Case InDonnee.Length
                                Case Is >= 5
                                    InDonnee = Strings.Left(InDonnee, 5)
                            End Select
                    End Select
                Case VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeCalculee, VI.NatureDonnee.DonneeExperte
                    Select Case FormatDonnee
                        Case VI.FormatDonnee.Standard
                            InDonnee = Strings.Format(ConversionDouble(InDonnee), "####0")
                        Case VI.FormatDonnee.Fixe
                            InDonnee = Strings.LTrim(Strings.Format(ConversionDouble(InDonnee), "###0"))
                        Case VI.FormatDonnee.Monetaire
                            InDonnee = Strings.Format(ConversionDouble(InDonnee), "###,###,##0.000000")
                        Case VI.FormatDonnee.Pourcentage
                            InDonnee = Strings.Format(ConversionDouble(InDonnee), "##0.00#")
                        Case VI.FormatDonnee.Decimal1
                            InDonnee = Strings.Format(ConversionDouble(InDonnee), "###,###,##0.0")
                            Select Case Strings.Right(InDonnee, 2)
                                Case ".0", ",0"
                                    InDonnee = Strings.Format(ConversionDouble(InDonnee), "####0")
                            End Select
                        Case VI.FormatDonnee.Decimal2
                            InDonnee = Strings.Format(ConversionDouble(InDonnee), "###,###,##0.00")
                        Case VI.FormatDonnee.Decimal3
                            InDonnee = Strings.Format(ConversionDouble(InDonnee), "###,###,##0.000")
                        Case VI.FormatDonnee.Decimal4
                            InDonnee = Strings.Format(ConversionDouble(InDonnee), "###,###,##0.0000")
                        Case VI.FormatDonnee.Decimal5
                            InDonnee = Strings.Format(ConversionDouble(InDonnee), "###,###,##0.00000")
                        Case VI.FormatDonnee.Decimal6
                            InDonnee = Strings.Format(ConversionDouble(InDonnee), "###,###,##0.000000")
                    End Select
                    If ConversionDouble(InDonnee) = 0 Then InDonnee = ""

            End Select
            Return InDonnee
        End Function

        Public ReadOnly Property ChaineSql(ByVal TypeDuSgbdr As Integer, ByVal Chaine As String, ByVal NaturedelaDonnee As Integer, ByVal FormatDatesSgbdr As Integer) As String
            Get
                Dim OraMon As String
                Dim ChaineRes As String

                ChaineRes = ""
                Try
                    Select Case NaturedelaDonnee
                        Case VI.NatureDonnee.DonneeDate
                            Select Case Chaine
                                Case Is <> ""
                                    Select Case Chaine.Length
                                        Case Is <> 10
                                            Chaine = WsRhDates.DateStandardVirtualia(DateValue(Chaine).ToShortDateString)
                                    End Select
                            End Select
                    End Select
                    Select Case Chaine
                        Case Is = ""
                            Select Case NaturedelaDonnee
                                Case Is <> VI.NatureDonnee.DonneeNumerique
                                    Select Case TypeDuSgbdr
                                        Case VI.TypeSgbdrNumeric.SgbdrOracle
                                            ChaineRes = VI.Quote & VI.Quote
                                        Case VI.TypeSgbdrNumeric.SgbdrAccess
                                            ChaineRes = "Null"
                                        Case Else
                                            ChaineRes = "NULL"
                                    End Select
                            End Select
                            Return ChaineRes
                    End Select
                    Select Case NaturedelaDonnee
                        Case VI.NatureDonnee.DonneeDate, VI.NatureDonnee.DonneeDateTime
                            ChaineRes = ""
                        Case VI.NatureDonnee.DonneeNumerique, VI.NatureDonnee.DonneeCalculee
                            Select Case Strings.InStr(Chaine, ":") 'cas donnee heure
                                Case Is > 0
                                    ChaineRes = VI.Quote & Chaine & VI.Quote
                                Case Else
                                    Select Case TypeDuSgbdr
                                        Case VI.TypeSgbdrNumeric.SgbdrAccess, VI.TypeSgbdrNumeric.SgbdrDb2, VI.TypeSgbdrNumeric.SgbdrSqlServer, VI.TypeSgbdrNumeric.SgbdrProgress
                                            ChaineRes = VirgulePoint(Chaine)
                                        Case VI.TypeSgbdrNumeric.SgbdrOracle, VI.TypeSgbdrNumeric.SgbdrMySql
                                            ChaineRes = VI.Quote & VirgulePoint(Chaine) & VI.Quote
                                    End Select
                            End Select
                            Return ChaineRes
                        Case VI.NatureDonnee.DonneeMemo
                            Dim ChaineW As String
                            Dim TableauData(0) As String
                            If Strings.InStr(Chaine, vbCrLf) > 0 Then
                                TableauData = Strings.Split(Chaine, vbCrLf)
                            Else
                                TableauData = Strings.Split(Chaine, vbLf)
                            End If
                            ChaineRes = ""
                            For Each Element In TableauData
                                If Element <> "" Then
                                    ChaineW = Strings.Replace(Element, VI.Quote, VI.Quote & VI.Quote)
                                    Select Case TypeDuSgbdr
                                        Case VI.TypeSgbdrNumeric.SgbdrMySql
                                            ChaineW = Strings.Replace(ChaineW, VI.AntiSlash, VI.AntiSlash & VI.AntiSlash)
                                    End Select
                                    If ChaineRes <> "" And ChaineW.Trim <> "" Then
                                        ChaineRes &= vbLf & ChaineW
                                    Else
                                        ChaineRes &= ChaineW
                                    End If
                                    If TableauData.Count > 1 Then
                                        ChaineRes &= vbCrLf
                                    End If
                                End If
                            Next
                            Return VI.Quote & ChaineRes & VI.Quote
                        Case Else
                            Dim ChaineW As String = ""
                            ChaineW = Strings.Replace(Chaine, VI.Quote, VI.Quote & VI.Quote)
                            Select Case TypeDuSgbdr
                                Case VI.TypeSgbdrNumeric.SgbdrMySql
                                    ChaineW = Strings.Replace(ChaineW, VI.AntiSlash, VI.AntiSlash & VI.AntiSlash)
                            End Select
                            Return VI.Quote & ChaineW & VI.Quote
                    End Select

                    Dim ChaineDate As String = ""
                    Dim ChaineHeure As String = ""

                    If NaturedelaDonnee = VI.NatureDonnee.DonneeDateTime Then
                        If Chaine.Length > 10 Then
                            Select Case TypeDuSgbdr
                                Case VI.TypeSgbdrNumeric.SgbdrSqlServer
                                    ChaineHeure = Strings.Space(1) & ChaineHeureValide(Strings.Right(Chaine, Chaine.Length - 11), True)
                                Case Else
                                    ChaineHeure = Strings.Space(1) & ChaineHeureValide(Strings.Right(Chaine, Chaine.Length - 11), False)
                            End Select
                        End If
                    End If
                    Select Case TypeDuSgbdr
                        Case VI.TypeSgbdrNumeric.SgbdrAccess  'Format americain
                            ChaineDate = Strings.Mid(Chaine, 4, 2) & VI.Tiret & Strings.Left(Chaine, 2) & VI.Tiret & Strings.Right(Chaine, 4)
                            ChaineRes = "#" & ChaineDate & ChaineHeure & "#"
                        Case VI.TypeSgbdrNumeric.SgbdrSqlServer, VI.TypeSgbdrNumeric.SgbdrProgress
                            Select Case FormatDatesSgbdr
                                Case VI.FormatDateSgbdr.DateStandard
                                    ChaineDate = Strings.Left(Chaine, 10)
                                Case VI.FormatDateSgbdr.DateUS
                                    ChaineDate = Strings.Mid(Chaine, 4, 2) & VI.Tiret & Strings.Left(Chaine, 2) & VI.Tiret & Strings.Right(Chaine, 4)
                                Case VI.FormatDateSgbdr.DateGB
                                    OraMon = ""
                                    Select Case Val(Strings.Mid(Chaine, 4, 2))
                                        Case 1
                                            OraMon = "Jan"
                                        Case 2
                                            OraMon = "Feb"
                                        Case 3
                                            OraMon = "Mar"
                                        Case 4
                                            OraMon = "Apr"
                                        Case 5
                                            OraMon = "May"
                                        Case 6
                                            OraMon = "Jun"
                                        Case 7
                                            OraMon = "Jul"
                                        Case 8
                                            OraMon = "Aug"
                                        Case 9
                                            OraMon = "Sep"
                                        Case 10
                                            OraMon = "Oct"
                                        Case 11
                                            OraMon = "Nov"
                                        Case 12
                                            OraMon = "Dec"
                                    End Select
                                    ChaineDate = Strings.Left(Chaine, 2) & VI.Tiret & OraMon & VI.Tiret & Strings.Right(Chaine, 4)
                            End Select
                            ChaineRes = VI.Quote & ChaineDate & ChaineHeure & VI.Quote
                        Case VI.TypeSgbdrNumeric.SgbdrOracle
                            Select Case FormatDatesSgbdr
                                Case VI.FormatDateSgbdr.DateStandard
                                    ChaineDate = Strings.Left(Chaine, 10)
                                    ChaineRes = "TO_DATE('" & ChaineDate & "','DD/MM/YYYY')"
                                    If NaturedelaDonnee = VI.NatureDonnee.DonneeDateTime And ChaineHeure <> "" Then
                                        ChaineRes = "TO_DATE('" & ChaineDate & ChaineHeure & "','DD/MM/YYYY HH24:MI:SS')"
                                    End If
                                Case VI.FormatDateSgbdr.DateUS
                                    ChaineDate = Strings.Right(Chaine, 4) & VI.Tiret & Strings.Mid(Chaine, 4, 2) & VI.Tiret & Strings.Left(Chaine, 2)
                                    ChaineRes = "TO_DATE('" & ChaineDate & "','YYYY-MM-DD')"
                                    If NaturedelaDonnee = VI.NatureDonnee.DonneeDateTime And ChaineHeure <> "" Then
                                        ChaineRes = "TO_DATE('" & ChaineDate & ChaineHeure & "','YYYY-MM-DD HH24:MI:SS')"
                                    End If
                                Case VI.FormatDateSgbdr.DateGB
                                    OraMon = ""
                                    Select Case Val(Strings.Mid(Chaine, 4, 2))
                                        Case 1
                                            OraMon = "Jan"
                                        Case 2
                                            OraMon = "Feb"
                                        Case 3
                                            OraMon = "Mar"
                                        Case 4
                                            OraMon = "Apr"
                                        Case 5
                                            OraMon = "May"
                                        Case 6
                                            OraMon = "Jun"
                                        Case 7
                                            OraMon = "Jul"
                                        Case 8
                                            OraMon = "Aug"
                                        Case 9
                                            OraMon = "Sep"
                                        Case 10
                                            OraMon = "Oct"
                                        Case 11
                                            OraMon = "Nov"
                                        Case 12
                                            OraMon = "Dec"
                                    End Select
                                    ChaineDate = Strings.Left(Chaine, 2) & VI.Tiret & OraMon & VI.Tiret & Strings.Right(Chaine, 4)
                                    ChaineRes = VI.Quote & ChaineDate & ChaineHeure & VI.Quote
                                    If NaturedelaDonnee = VI.NatureDonnee.DonneeDateTime And ChaineHeure <> "" Then
                                        ChaineDate = Strings.Right(Chaine, 4) & VI.Tiret & Strings.Mid(Chaine, 4, 2) & VI.Tiret & Strings.Left(Chaine, 2)
                                        ChaineRes = "TO_DATE('" & ChaineDate & ChaineHeure & "','YYYY-MM-DD HH24:MI:SS')"
                                    End If
                            End Select
                        Case VI.TypeSgbdrNumeric.SgbdrDb2, VI.TypeSgbdrNumeric.SgbdrMySql
                            ChaineDate = Strings.Right(DateValue(Chaine).ToShortDateString, 4) & VI.Tiret & Strings.Mid(DateValue(Chaine).ToShortDateString, 4, 2) & VI.Tiret & Strings.Left(DateValue(Chaine).ToShortDateString, 2)
                            ChaineRes = VI.Quote & ChaineDate & VI.Quote
                            If NaturedelaDonnee = VI.NatureDonnee.DonneeDateTime And ChaineHeure <> "" Then
                                ChaineRes = VI.Quote & ChaineDate & ChaineHeure & VI.Quote
                                If TypeDuSgbdr = VI.TypeSgbdrNumeric.SgbdrDb2 Then
                                    ChaineRes = "TIMESTAMP_FORMAT('" & ChaineDate & ChaineHeure & "','YYYY-MM-DD HH24:MI:SS')"
                                End If
                            End If
                    End Select

                    Return ChaineRes
                Catch ex As Exception
                    Return "NULL"
                End Try
            End Get
        End Property

        Public Function ChaineFiltrePourSqlInterne(Of T)(ByVal LstSource As IEnumerable(Of T), ByVal Separateur As String) As String
            If LstSource.Count <= 0 Then
                Return ""
            End If
            Dim Chaine As System.Text.StringBuilder
            Dim Sep As String = ""
            Chaine = New System.Text.StringBuilder
            For Each Element In LstSource
                Chaine.Append(Sep & Element.ToString)
                Sep = Separateur
            Next
            Return Chaine.ToString
        End Function

        Public Function MiseEnFormeAnciennete(ByVal Chaine As String) As String
            Dim Ans As Integer
            Dim Mois As Integer
            Dim Jours As Integer
            Dim LibelAn As String = " an "

            Select Case Val(Chaine)
                Case Is = 0
                    Return ""
            End Select
            Select Case Chaine.Length
                Case Is < 8
                    Chaine = "00" & Format(Val(Chaine), "000000")
            End Select
            Select Case Strings.Mid(Chaine, 3, 2)
                Case Is <> "00"
                    Ans = CInt(Strings.Mid(Chaine, 3, 2))
                    If Ans > 1 Then
                        LibelAn = " ans "
                    End If
            End Select
            Select Case Strings.Mid(Chaine, 5, 2)
                Case Is <> "00"
                    Mois = CInt(Strings.Mid(Chaine, 5, 2))
            End Select
            Select Case Mid(Chaine, 7, 2)
                Case Is <> "00"
                    Jours = CInt(Strings.Mid(Chaine, 7, 2))
            End Select
            Return Ans.ToString & LibelAn & Mois.ToString & " mois " & Jours.ToString & " jours"
        End Function

        Public Function HashMD5(ByVal Chaine As String) As String
            'Obsolete
            'Return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(Chaine, "MD5")
            '*******************
            Dim algorithm As System.Security.Cryptography.MD5 = System.Security.Cryptography.MD5.Create()
            Dim data As Byte() = algorithm.ComputeHash(System.Text.Encoding.UTF8.GetBytes(Chaine))
            Dim hashed As New System.Text.StringBuilder
            Dim i As Integer
            For i = 0 To data.Length - 1
                hashed.Append(data(i).ToString("x2"))
            Next i
            Return hashed.ToString
        End Function

        Public Function HashSHA(ByVal Chaine As String) As String
            'Obsolete
            'Return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(Chaine, "SHA1")
            '******************
            Dim algorithm As System.Security.Cryptography.SHA1 = System.Security.Cryptography.SHA1.Create()
            Dim data As Byte() = algorithm.ComputeHash(System.Text.Encoding.UTF8.GetBytes(Chaine))
            Dim hashed As New System.Text.StringBuilder
            Dim i As Integer
            For i = 0 To data.Length - 1
                hashed.Append(data(i).ToString("x2"))
            Next i
            Return hashed.ToString
        End Function

        Public Function DecryptageVirtualia(ByVal Valeur As String) As String
            Dim Indice As Integer
            Dim IndiceK As Integer
            Dim Chaine As String
            Dim MotDecrypte As String

            If Valeur = "" Then
                Return ""
            ElseIf Asc(Strings.Left(Valeur, 1)) = 0 Then
                Return ""
            End If
            Chaine = Valeur
            Select Case Chaine.Length
                Case Is < 48
                    Chaine = Chaine & Strings.Space(48 - Chaine.Length)
                Case Is > 48
                    Chaine = Left(Chaine, 48)
            End Select
            MotDecrypte = ""
            IndiceK = 1
            For Indice = 1 To 16
                MotDecrypte = MotDecrypte & Chr(Convert.ToInt32(Mid(Chaine, IndiceK, 3)) - Convert.ToInt32(Mid(SysAssurance, Indice, 1)))
                IndiceK = IndiceK + 3
            Next Indice
            Return MotDecrypte.Trim
        End Function

        Public Function CryptageVirtualia(ByVal Valeur As String) As String
            Dim Indice As Integer
            Dim Chaine As String
            Dim Cryptage As String

            If Valeur = "" Then
                Return ""
            End If
            Cryptage = ""
            Chaine = Valeur
            Select Case Chaine.Length
                Case Is < 16
                    Chaine = Chaine & Strings.Space(16 - Chaine.Length)
                Case Is > 16
                    Chaine = Strings.Left(Chaine, 16)
            End Select
            For Indice = 1 To 16
                Cryptage = Cryptage & Strings.Format(Asc(Mid(Chaine, Indice, 1)) + Val(Mid(SysAssurance, Indice, 1)), "000")
            Next Indice
            Return Cryptage
        End Function

        Public Function CompresserFichier(ByVal Source As String, ByVal Cible As String) As Boolean
            Try
                Dim FicSource As System.IO.FileStream = System.IO.File.OpenRead(Source)
                Dim FicDest As System.IO.FileStream = System.IO.File.Create(Cible)
                Dim FluxCompresse As New System.IO.Compression.GZipStream(FicDest, IO.Compression.CompressionMode.Compress)
                Dim IOctet As Integer = FicSource.ReadByte()
                While IOctet <> -1
                    FluxCompresse.WriteByte(CType(IOctet, Byte))
                    IOctet = FicSource.ReadByte()
                End While
                FluxCompresse.Flush()
                FluxCompresse.Close()
                FicSource.Close()
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function DecompresserFichier(ByVal Source As String, ByVal Cible As String) As Boolean
            Try
                Dim FicSource As System.IO.FileStream = System.IO.File.OpenRead(Source)
                Dim FicDest As System.IO.FileStream = System.IO.File.Create(Cible)
                Dim FluxCompresse As New System.IO.Compression.GZipStream(FicSource, IO.Compression.CompressionMode.Decompress)
                Dim IOctet As Integer = FluxCompresse.ReadByte()
                While IOctet <> -1
                    FicDest.WriteByte(CType(IOctet, Byte))
                    IOctet = FluxCompresse.ReadByte()
                End While
                FicDest.Flush()
                FluxCompresse.Close()
                FicDest.Close()
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Private Sub Compresser(ByVal Source As String, ByVal Cible As String)
            Using Entree As System.IO.FileStream = System.IO.File.OpenRead(Source)
                Using Sortie As System.IO.FileStream = System.IO.File.Create(Cible)
                    Using Compress As System.IO.Compression.GZipStream = _
                     New System.IO.Compression.GZipStream(Sortie, System.IO.Compression.CompressionMode.Compress)
                        Call Entree.CopyTo(Compress)
                    End Using
                End Using
            End Using
        End Sub

        Private Sub Decompresser(ByVal Source As String, ByVal Cible As String)
            Using Entree As System.IO.FileStream = System.IO.File.OpenRead(Source)
                Using Sortie As System.IO.FileStream = System.IO.File.Create(Cible)
                    Using Decompress As System.IO.Compression.GZipStream = New System.IO.Compression.GZipStream(Entree, _
                                            System.IO.Compression.CompressionMode.Decompress)
                        Call Decompress.CopyTo(Sortie)
                    End Using
                End Using
            End Using
        End Sub

        Public Function ObtenirClesCryptage() As String
            Dim Symetrie As System.Security.Cryptography.SymmetricAlgorithm
            Symetrie = New System.Security.Cryptography.RijndaelManaged
            Symetrie.GenerateKey()
            Dim Clef As String = Convert.ToBase64String(Symetrie.Key)
            Symetrie.GenerateIV()
            Dim ClefIV As String = Convert.ToBase64String(Symetrie.IV)
            Return Clef & "Virtualia" & ClefIV
        End Function

        Public Function ChaineCryptee(ByVal Valeur As String, ByVal ClefPublique As Byte(), Optional ByVal ClefPrivee As Byte() = Nothing) As String
            Dim Symetrie As System.Security.Cryptography.SymmetricAlgorithm
            Symetrie = New System.Security.Cryptography.RijndaelManaged
            Dim Transformation As System.Security.Cryptography.ICryptoTransform
            Dim FluxMemoire As System.IO.MemoryStream
            Dim FluxCryptage As System.Security.Cryptography.CryptoStream
            Dim TableauByte() As Byte

            If ClefPrivee Is Nothing Then
                ClefPrivee = System.Convert.FromBase64String(SysClefPrivee)
            End If
            Transformation = Symetrie.CreateEncryptor(ClefPrivee, ClefPublique)
            TableauByte = System.Text.Encoding.UTF8.GetBytes(Valeur.Replace("&", "&&"))
            FluxMemoire = New System.IO.MemoryStream
            FluxCryptage = New System.Security.Cryptography.CryptoStream(FluxMemoire, Transformation, Security.Cryptography.CryptoStreamMode.Write)
            Try
                FluxCryptage.Write(TableauByte, 0, TableauByte.Length)
                FluxCryptage.FlushFinalBlock()
                FluxCryptage.Close()
                Symetrie.Clear()
            Catch ex As Exception
                Symetrie.Clear()
                Return ""
                Exit Function
            End Try
            Try
                Return Convert.ToBase64String(FluxMemoire.ToArray)
            Catch ex As Exception
                Return ""
            End Try
        End Function

        Public Function ChaineDecryptee(ByVal Valeur As String, ByVal ClefPublique As Byte(), Optional ByVal ClefPrivee As Byte() = Nothing) As String
            Dim Symetrie As System.Security.Cryptography.SymmetricAlgorithm
            Symetrie = New System.Security.Cryptography.RijndaelManaged
            Dim Transformation As System.Security.Cryptography.ICryptoTransform
            Dim FluxMemoire As System.IO.MemoryStream
            Dim FluxCryptage As System.Security.Cryptography.CryptoStream
            Dim TableauByte() As Byte

            If Valeur Is Nothing Then
                Return ""
                Exit Function
            End If
            If ClefPrivee Is Nothing Then
                ClefPrivee = System.Convert.FromBase64String(SysClefPrivee)
            End If
            Transformation = Symetrie.CreateDecryptor(ClefPrivee, ClefPublique)
            TableauByte = Convert.FromBase64String(Valeur)
            FluxMemoire = New System.IO.MemoryStream
            FluxCryptage = New System.Security.Cryptography.CryptoStream(FluxMemoire, Transformation, Security.Cryptography.CryptoStreamMode.Write)
            Try
                FluxCryptage.Write(TableauByte, 0, TableauByte.Length)
                FluxCryptage.FlushFinalBlock()
                FluxCryptage.Close()
                Symetrie.Clear()
            Catch Ex As Exception
                Symetrie.Clear()
                Return ""
                Exit Function
            End Try
            Try
                Return System.Text.Encoding.UTF8.GetString(FluxMemoire.ToArray)
            Catch Ex As Exception
                Return ""
            End Try
        End Function

        Public Function TableauCrypte(ByVal Valeurs As Byte(), ByVal ClefPublique As Byte(), Optional ByVal ClefPrivee As Byte() = Nothing) As Byte()
            Dim Symetrie As System.Security.Cryptography.SymmetricAlgorithm
            Symetrie = New System.Security.Cryptography.RijndaelManaged
            Dim Transformation As System.Security.Cryptography.ICryptoTransform
            Dim FluxMemoire As System.IO.MemoryStream
            Dim FluxCryptage As System.Security.Cryptography.CryptoStream

            If ClefPrivee Is Nothing Then
                ClefPrivee = System.Convert.FromBase64String(SysClefPrivee)
            End If
            Transformation = Symetrie.CreateEncryptor(ClefPrivee, ClefPublique)
            FluxMemoire = New System.IO.MemoryStream
            FluxCryptage = New System.Security.Cryptography.CryptoStream(FluxMemoire, Transformation, Security.Cryptography.CryptoStreamMode.Write)
            Try
                FluxCryptage.Write(Valeurs, 0, Valeurs.Length)
                FluxCryptage.FlushFinalBlock()
                FluxCryptage.Close()
                Symetrie.Clear()
            Catch ex As Exception
                Symetrie.Clear()
                Return Nothing
                Exit Function
            End Try
            Try
                Return FluxMemoire.ToArray
            Catch ex As Exception
                Return Nothing
            End Try
        End Function

        Public Function TableauDecrypte(ByVal Valeurs As Byte(), ByVal ClefPublique As Byte(), Optional ByVal ClefPrivee As Byte() = Nothing) As Byte()
            Dim Symetrie As System.Security.Cryptography.SymmetricAlgorithm
            Symetrie = New System.Security.Cryptography.RijndaelManaged
            Dim Transformation As System.Security.Cryptography.ICryptoTransform
            Dim FluxMemoire As System.IO.MemoryStream
            Dim FluxCryptage As System.Security.Cryptography.CryptoStream

            If Valeurs Is Nothing Then
                Return Nothing
                Exit Function
            End If
            If ClefPrivee Is Nothing Then
                ClefPrivee = System.Convert.FromBase64String(SysClefPrivee)
            End If
            Transformation = Symetrie.CreateDecryptor(ClefPrivee, ClefPublique)
            FluxMemoire = New System.IO.MemoryStream
            FluxCryptage = New System.Security.Cryptography.CryptoStream(FluxMemoire, Transformation, Security.Cryptography.CryptoStreamMode.Write)
            Try
                FluxCryptage.Write(Valeurs, 0, Valeurs.Length)
                FluxCryptage.FlushFinalBlock()
                FluxCryptage.Close()
                Symetrie.Clear()
            Catch Ex As Exception
                Symetrie.Clear()
                Return Nothing
                Exit Function
            End Try
            Try
                Return FluxMemoire.ToArray
            Catch Ex As Exception
                Return Nothing
            End Try
        End Function

        Public Sub VerifierFichier(ByVal NomFichier As String)
            Dim CodeIso As System.Text.Encoding = System.Text.Encoding.UTF8
            Dim FLuxLecture As System.IO.FileStream
            Dim FicReader As System.IO.StreamReader
            Dim FLuxEcriture As System.IO.FileStream
            Dim FicWriter As System.IO.StreamWriter
            Dim Champlu As String
            Dim NomBak As String = Strings.Left(NomFichier, NomFichier.Length - 3) & "Bak"

            My.Computer.FileSystem.CopyFile(NomFichier, NomBak, True)

            FLuxLecture = New System.IO.FileStream(NomBak, IO.FileMode.Open, IO.FileAccess.Read)
            FicReader = New System.IO.StreamReader(FLuxLecture, CodeIso)

            FLuxEcriture = New System.IO.FileStream(NomFichier, IO.FileMode.Create, IO.FileAccess.Write)
            FicWriter = New System.IO.StreamWriter(FLuxEcriture, CodeIso)

            Champlu = FicReader.ReadLine
            Do Until FicReader.EndOfStream
                FicWriter.WriteLine(Champlu.Trim)
                Champlu = FicReader.ReadLine
            Loop
            FicWriter.WriteLine(Champlu.Trim)
            FicWriter.Flush()
            FicWriter.Close()
            FicReader.Close()
            My.Computer.FileSystem.DeleteFile(NomBak)
        End Sub

        Public ReadOnly Property IndexOptionInfo(ByVal INature As String, ByVal Valeur As String) As Integer
            Get
                Dim IndiceA As Integer
                Select Case INature
                    Case "Alpha"
                        For IndiceA = 0 To OptFormatAlpha.Count - 1
                            If OptFormatAlpha(IndiceA) = Valeur Then
                                Return (IndiceA)
                            End If
                        Next IndiceA
                    Case "Numeric"
                        For IndiceA = 0 To OptFormatNum.Count - 1
                            If OptFormatNum(IndiceA) = Valeur Then
                                Return (IndiceA)
                            End If
                        Next IndiceA
                    Case "Date"
                        For IndiceA = 0 To OptFormatDate.Count - 1
                            If OptFormatDate(IndiceA) = Valeur Then
                                Return (IndiceA)
                            End If
                        Next IndiceA
                    Case Else
                        Return 0
                End Select
                Return 0
            End Get
        End Property

        Public ReadOnly Property OptionInformation(ByVal INature As String, ByVal Index As Integer) As String
            Get
                Select Case INature
                    Case "Alpha"
                        Select Case Index
                            Case 0 To OptFormatAlpha.Count - 1
                                Return OptFormatAlpha(Index)
                            Case Else
                                Return ""
                        End Select
                    Case "Numeric"
                        Select Case Index
                            Case 0 To OptFormatNum.Count - 1
                                Return OptFormatNum(Index)
                            Case Else
                                Return ""
                        End Select
                    Case "Date"
                        Select Case Index
                            Case 0 To OptFormatDate.Count - 1
                                Return OptFormatDate(Index)
                            Case Else
                                Return ""
                        End Select
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public ReadOnly Property ChaineSansAccent(ByVal Source As String, ByVal SiMajuscule As Boolean) As String
            Get
                If Source Is Nothing Then
                    Return ""
                End If
                If Source = "" Then
                    Return ""
                End If
                Dim IndiceI As Integer
                Dim Chaine As String = ""
                For IndiceI = 1 To Source.Length
                    Select Case Strings.Mid(Source, IndiceI, 1)
                        Case "'", ",", "�", "-", "/", "\"
                            Chaine &= Strings.Space(1)
                        Case ">", "<", "="
                            Chaine &= Strings.Space(1)
                        Case "�", "�", "�"
                            Chaine &= "a"
                        Case "�"
                            Chaine &= "A"
                        Case "�"
                            Chaine &= "c"
                        Case "�"
                            Chaine &= "C"
                        Case "�", "�", "�", "�"
                            Chaine &= "e"
                        Case "�", "�", "�", "�"
                            Chaine &= "E"
                        Case "�", "�"
                            Chaine &= "i"
                        Case "�", "�"
                            Chaine &= "I"
                        Case "�", "�"
                            Chaine &= "o"
                        Case "�", "�"
                            Chaine &= "O"
                        Case "�", "�"
                            Chaine &= "u"
                        Case "�", "�"
                            Chaine &= "U"
                        Case "�", "�", "�", "�"
                            Chaine &= "E"
                        Case VI.DoubleQuote
                            Chaine &= Strings.Space(1)
                        Case Else
                            Chaine &= Strings.Mid(Source, IndiceI, 1)
                    End Select
                Next IndiceI
                If SiMajuscule = True Then
                    Return Chaine.ToUpper
                Else
                    Return Chaine
                End If
            End Get
        End Property

        Public Function ValeurAutoriseeRS1001(ByVal Valeur As String) As Boolean
            Dim J As Integer
            Dim Caractere As String = ""
            Dim SiOk As Boolean

            If Valeur.Length = 0 Then
                Return True
            End If
            For J = 1 To Valeur.Length
                SiOk = False
                'De A => Z
                If Asc(Mid(Valeur, J, 1)) >= 65 And Asc(Mid(Valeur, J, 1)) <= 90 Then
                    SiOk = True
                End If
                'De a => z
                If Asc(Mid(Valeur, J, 1)) >= 97 And Asc(Mid(Valeur, J, 1)) <= 122 Then
                    SiOk = True
                End If
                ' ������������������������������� -'
                Select Case Asc(Mid(Valeur, J, 1))
                    Case 193, 194, 196
                        SiOk = True
                    Case 39, 45, 32, 255, 249, 252, 251, 246, 244
                        SiOk = True
                    Case 239, 238, 235, 234, 232, 233, 231, 228, 226
                        SiOk = True
                    Case 224, 217, 220, 219, 214, 212, 207, 206, 203
                        SiOk = True
                    Case 202, 200, 201, 199
                        SiOk = True
                End Select
                Return False
            Next J
            Return True
        End Function

        Public Function ConversionDouble(ByVal Valeur As String, Optional ByVal NbDecimales As Integer = 99) As Double
            If Valeur = "" Then
                Return 0
            End If
            If Val(VirgulePoint(Valeur)) = 0 Then
                Return 0
            End If
            Dim ciClone As Globalization.CultureInfo = CType(Globalization.CultureInfo.InvariantCulture.Clone(), Globalization.CultureInfo)
            ciClone.NumberFormat.NumberDecimalSeparator = "."
            If NbDecimales = 99 Then
                Return Convert.ToDouble(VirgulePoint(Valeur), ciClone)
            End If
            Return Math.Round(Convert.ToDouble(VirgulePoint(Valeur), ciClone), NbDecimales)
        End Function

        Public Function ChaineHeureValide(ByVal Valeur As String, ByVal SiMilliSeconde As Boolean) As String
            Dim TableauHeure(0) As String
            Dim IndiceH As Integer
            Dim Chaine As New System.Text.StringBuilder

            TableauHeure = Strings.Split(Valeur.Trim, ":", -1)
            If TableauHeure.Count < 3 Then
                ReDim Preserve TableauHeure(2)
            End If
            For IndiceH = 0 To TableauHeure.Count - 1
                If TableauHeure(IndiceH) Is Nothing Then
                    TableauHeure(IndiceH) = "0"
                End If
                If IndiceH > 0 Then
                    Chaine.Append(":")
                End If
                If IsNumeric(TableauHeure(IndiceH)) Then
                    Select Case IndiceH
                        Case 3
                            Chaine.Append(Strings.Format(CInt(TableauHeure(IndiceH)), "000"))
                        Case Else
                            Chaine.Append(Strings.Format(CInt(TableauHeure(IndiceH)), "00"))
                    End Select
                Else
                    Select Case IndiceH
                        Case 3
                            Chaine.Append("000")
                        Case Else
                            Chaine.Append("00")
                    End Select
                End If
            Next IndiceH
            If SiMilliSeconde = True And TableauHeure.Count = 3 Then
                Chaine.Append(":000")
            End If
            Return Chaine.ToString
        End Function

        Public Function ChaineVirtualiaValide(ByVal Valeur As String) As String
            If Valeur = "" Then
                Return Valeur
            End If
            Dim Chaine As String
            Chaine = Valeur
            Select Case Strings.InStr(Valeur, VI.SigneBarre)
                Case Is > 0
                    Chaine = Strings.Replace(Valeur, VI.SigneBarre, VI.Virgule)
                    Valeur = Chaine
            End Select
            Select Case Strings.InStr(Valeur, VI.Tild)
                Case Is > 0
                    Chaine = Strings.Replace(Valeur, VI.Tild, VI.Virgule)
            End Select
            Return Chaine
        End Function

        Public Function ChaineXMLValide(ByVal Valeur As String) As String
            Dim Chaine As String
            If Valeur = "" Then
                Return Valeur
            End If
            Chaine = Valeur
            Select Case Strings.InStr(Valeur, "<")
                Case Is > 0
                    Chaine = Strings.Replace(Valeur, "<", Cst_Symbole_SigneInferieur)
                    Valeur = Chaine
            End Select
            Select Case Strings.InStr(Valeur, ">")
                Case Is > 0
                    Chaine = Strings.Replace(Valeur, ">", Cst_Symbole_SigneSuperieur)
            End Select
            Select Case Strings.InStr(Valeur, "'")
                Case Is > 0
                    Chaine = Strings.Replace(Valeur, "'", Cst_Symbole_Quote)
            End Select
            Select Case Strings.InStr(Valeur, "&")
                Case Is > 0
                    Chaine = Strings.Replace(Valeur, "&", Cst_Symbole_EtCommercial)
            End Select
            Return Chaine
        End Function

        Public Function ConvertCouleur(ByVal Valeur As String) As System.Drawing.Color
            Dim R As Integer
            Dim G As Integer
            Dim B As Integer
            Select Case Valeur.Length
                Case Is = 6
                    R = CInt(Val("&H" & Strings.Left(Valeur, 2) & "&"))
                    G = CInt(Val("&H" & Strings.Mid(Valeur, 3, 2) & "&"))
                    B = CInt(Val("&H" & Strings.Right(Valeur, 2) & "&"))
                    Return System.Drawing.Color.FromArgb(R, G, B)
                Case Is = 7
                    R = CInt(Val("&H" & Strings.Mid(Valeur, 2, 2) & "&"))
                    G = CInt(Val("&H" & Strings.Mid(Valeur, 4, 2) & "&"))
                    B = CInt(Val("&H" & Strings.Right(Valeur, 2) & "&"))
                    Return System.Drawing.Color.FromArgb(R, G, B)
                Case Else
                    Return Drawing.Color.White
            End Select
        End Function

        Private Sub Initialiser()
            OptFormatAlpha(0) = "Format standard"
            OptFormatAlpha(1) = "Format Minuscule (Ex : Virtualia)"
            OptFormatAlpha(2) = "Format Majuscule (Ex : VIRTUALIA)"

            OptFormatNum(0) = "Format Virtualia"
            OptFormatNum(1) = "Format DBF (Ex : 1500.00)"
            OptFormatNum(2) = "Format Mon�taire (Ex : 1 500,00)"
            OptFormatNum(3) = "Format Num�rique entier (Ex : 1500)"
            OptFormatNum(4) = "Format Omission de la virgule(Ex : 150000)"
            OptFormatNum(5) = "Euro"
            OptFormatNum(6) = "Anciennet�"

            OptFormatDate(0) = "jj/mm/ssaa"
            OptFormatDate(1) = "mm-dd-yyyy"
            OptFormatDate(2) = "jj/mm/aa"
            OptFormatDate(3) = "mm-dd-yy"
            OptFormatDate(4) = "aammjj"
            OptFormatDate(5) = "ssaammjj"
            OptFormatDate(6) = "ssaa-mm-jj"
            OptFormatDate(7) = "jjmmssaa"
            OptFormatDate(8) = "Nombre Julien"
            OptFormatDate(9) = "Ann�e"
            OptFormatDate(10) = "Jour en clair"
            OptFormatDate(11) = "Date en clair"
        End Sub
    End Class
End Namespace

