﻿Option Strict On
Option Explicit On
Option Compare Text
Namespace Outils
    Public Class EmissionMail
        'Exemple SiHtml Email.Body="<HTML><BODY style=" & Doublequote & "font-size:12pt;font-family:'Trebuchet MS';" & Doublequote
        '& ">" & WsContenu & ".</BODY></HTML>"
        Private Const Doublequote As String = """"
        Private WsMsgErreurMail As String = ""
        Private WsMsgOrigine As String
        Private WsSiContenuHtml As Boolean = False

        Public Property SiContenuHtml As Boolean
            Get
                Return WsSiContenuHtml
            End Get
            Set(value As Boolean)
                WsSiContenuHtml = value
            End Set
        End Property

        Public ReadOnly Property MessageErreur As String
            Get
                Return WsMsgErreurMail
            End Get
        End Property

        Private Function EnvoyerEmail(ByVal InfosMail As DonneeMail) As Boolean
            WsMsgErreurMail = ""
            WsMsgOrigine = ""
            If InfosMail.ServeurSmtp = "" Then
                Return False
            End If
            Dim Email As New System.Net.Mail.MailMessage

            Try
                Email.From = New System.Net.Mail.MailAddress(InfosMail.Emetteur)
            Catch ex As Exception
                WsMsgErreurMail = "Emetteur invalide " & ex.Message
                Call EcrireLog()
                Return False
            End Try

            Dim ChaineMail As String = ""
            Dim IndiceI As Integer = 0
            Dim BodyMail As String = ""

            If InfosMail.NomPieceAttachee <> "" Then
                Dim NomFic = Constantes.DossierVirtualiaService("Temp") & InfosMail.NomPieceAttachee
                Try
                    My.Computer.FileSystem.WriteAllBytes(NomFic, InfosMail.StreamPieceAttachee, False)
                Catch ex As Exception
                    WsMsgErreurMail = ex.Message
                    Call EcrireLog()
                    Return False
                End Try

                Dim ItemAtt As System.Net.Mail.Attachment = New System.Net.Mail.Attachment(NomFic, System.Net.Mime.MediaTypeNames.Application.Octet)
                ItemAtt.ContentDisposition.CreationDate = System.IO.File.GetCreationTime(NomFic)
                ItemAtt.ContentDisposition.ModificationDate = System.IO.File.GetLastWriteTime(NomFic)
                ItemAtt.ContentDisposition.ReadDate = System.IO.File.GetLastAccessTime(NomFic)

                Email.Attachments.Add(ItemAtt)
            End If

            WsMsgOrigine = "Emetteur : " & InfosMail.Emetteur

            For Each Personne As String In InfosMail.Destinataires
                Try
                    Email.To.Add(New System.Net.Mail.MailAddress(Personne))
                Catch ex As Exception
                    WsMsgErreurMail = "Destinataire invalide " & ex.Message
                    Call EcrireLog()
                    Return False
                End Try
                WsMsgOrigine &= " " & Personne
            Next

            WsMsgOrigine &= " - Contenu : " & InfosMail.Contenu

            Email.IsBodyHtml = WsSiContenuHtml
            Email.Body = InfosMail.Contenu
            Email.Subject = InfosMail.ObjetSujet

            Dim SmtpMail As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient(InfosMail.ServeurSmtp, InfosMail.PortSmtp)
            SmtpMail.DeliveryMethod = Net.Mail.SmtpDeliveryMethod.Network
            If InfosMail.UtilsateurSmtp <> "" And InfosMail.PwSmtp <> "" Then
                SmtpMail.UseDefaultCredentials = False
                If InfosMail.NomDomaine <> "" Then
                    SmtpMail.Credentials = New System.Net.NetworkCredential(InfosMail.UtilsateurSmtp, InfosMail.PwSmtp, InfosMail.NomDomaine)
                Else
                    SmtpMail.Credentials = New System.Net.NetworkCredential(InfosMail.UtilsateurSmtp, InfosMail.PwSmtp)
                End If
            Else
                SmtpMail.UseDefaultCredentials = True
            End If
            'Si le serveur requiert TLS encryption
            SmtpMail.EnableSsl = InfosMail.SiCryptageSSLRequis
            Try
                SmtpMail.Send(Email)
            Catch ex As Exception
                WsMsgErreurMail = ex.Message
                Email.To.Clear()
                Call EcrireLog()
                SmtpMail.Dispose()
                GC.Collect()
                GC.WaitForPendingFinalizers()
                Return False
            End Try

            SmtpMail.Dispose()
            Call EcrireLog()
            GC.Collect()
            GC.WaitForPendingFinalizers()
            Return True
        End Function

        Private Sub EcrireLog()
            Dim FicStream As System.IO.FileStream
            Dim FicWriter As System.IO.StreamWriter
            Dim NomLog As String
            Dim NomRep As String
            Dim SysFicLog As String = "WebMail.log"
            Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.UTF8

            NomRep = System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia")
            NomLog = Constantes.DossierVirtualiaService("Logs") & SysFicLog
            FicStream = New System.IO.FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
            FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)
            FicWriter.WriteLine(Strings.Format(System.DateTime.Now, "g"))
            If WsMsgOrigine.Length < 500 Then
                FicWriter.WriteLine(WsMsgOrigine)
            Else
                FicWriter.WriteLine(Strings.Left(WsMsgOrigine, 500))
            End If
            If WsMsgErreurMail <> "" Then
                FicWriter.WriteLine(WsMsgErreurMail)
            End If
            FicWriter.WriteLine(Strings.StrDup(40, "-"))
            FicWriter.Flush()
            FicWriter.Close()
        End Sub

        Private Sub ChargeDonneeMailParConfig(ByVal InfosMail As DonneeMail)
            InfosMail.ServeurSmtp = System.Configuration.ConfigurationManager.AppSettings("SmtpServeur")
            If IsNumeric(System.Configuration.ConfigurationManager.AppSettings("SmtpPort")) Then
                InfosMail.PortSmtp = Integer.Parse(System.Configuration.ConfigurationManager.AppSettings("SmtpPort"))
            Else
                InfosMail.PortSmtp = 25
            End If
            InfosMail.UtilsateurSmtp = System.Configuration.ConfigurationManager.AppSettings("SmtpUser")
            If System.Configuration.ConfigurationManager.AppSettings("SmtpPw") <> "" Then
                Dim RhFonction As New Virtualia.Systeme.Fonctions.Generales
                InfosMail.PwSmtp = RhFonction.DecryptageVirtualia(System.Configuration.ConfigurationManager.AppSettings("SmtpPw"))
            End If
            InfosMail.NomDomaine = System.Configuration.ConfigurationManager.AppSettings("SmtpDomaine")
            If System.Configuration.ConfigurationManager.AppSettings("SmtpCryptageSSL") = "Oui" Then
                InfosMail.SiCryptageSSLRequis = True
            End If
            InfosMail.Emetteur = System.Configuration.ConfigurationManager.AppSettings("SmtpEmetteur")
        End Sub

        Public Function SendMail(ByVal Contenu As String, ByVal Destinataires As List(Of String)) As Boolean
            Dim InfosMail As DonneeMail = New DonneeMail
            Call ChargeDonneeMailParConfig(InfosMail)
            If Destinataires IsNot Nothing AndAlso Destinataires.Count > 0 Then
                For Each Personne In Destinataires
                    InfosMail.Destinataires.Add(Personne.ToString)
                Next
            End If
            InfosMail.Contenu = Contenu
            Return EnvoyerEmail(InfosMail)
        End Function

        Public Function SendMail(ByVal Contenu As String, ByVal Destinataire As String) As Boolean
            Dim InfosMail As DonneeMail = New DonneeMail
            Call ChargeDonneeMailParConfig(InfosMail)
            If Destinataire IsNot Nothing AndAlso Destinataire.Trim <> "" Then
                InfosMail.Destinataires.Add(Destinataire)
            End If
            InfosMail.Contenu = Contenu
            Return EnvoyerEmail(InfosMail)
        End Function

        Public Function SendMail(ByVal Contenu As String, ByVal Emetteur As String, ByVal Destinataires As List(Of String)) As Boolean
            Dim InfosMail As DonneeMail = New DonneeMail
            Call ChargeDonneeMailParConfig(InfosMail)
            If Emetteur <> "" Then
                InfosMail.Emetteur = Emetteur
            End If
            If Destinataires IsNot Nothing AndAlso Destinataires.Count > 0 Then
                For Each Personne In Destinataires
                    InfosMail.Destinataires.Add(Personne.ToString)
                Next
            End If
            InfosMail.Contenu = Contenu
            Return EnvoyerEmail(InfosMail)
        End Function

        Public Function SendMail(ByVal Sujet As String, ByVal Contenu As String, ByVal Emetteur As String, ByVal Destinataires As List(Of String)) As Boolean
            Dim InfosMail As DonneeMail = New DonneeMail
            Call ChargeDonneeMailParConfig(InfosMail)
            If Emetteur <> "" Then
                InfosMail.Emetteur = Emetteur
            End If
            If Destinataires IsNot Nothing AndAlso Destinataires.Count > 0 Then
                For Each Personne In Destinataires
                    InfosMail.Destinataires.Add(Personne.ToString)
                Next
            End If
            InfosMail.ObjetSujet = Sujet
            InfosMail.Contenu = Contenu
            Return EnvoyerEmail(InfosMail)
        End Function

        Public Function SendMail(ByVal Contenu As String, ByVal Emetteur As String, ByVal Destinataire As String) As Boolean
            Dim InfosMail As DonneeMail = New DonneeMail()
            Call ChargeDonneeMailParConfig(InfosMail)
            If Emetteur <> "" Then
                InfosMail.Emetteur = Emetteur
            End If
            If Destinataire IsNot Nothing AndAlso Destinataire.Trim <> "" Then
                InfosMail.Destinataires.Add(Destinataire)
            End If
            InfosMail.Contenu = Contenu
            Return EnvoyerEmail(InfosMail)
        End Function

        Public Function SendMail(ByVal Sujet As String, ByVal Contenu As String, ByVal Emetteur As String, ByVal Destinataire As String) As Boolean
            Dim InfosMail As DonneeMail = New DonneeMail()
            Call ChargeDonneeMailParConfig(InfosMail)
            If Emetteur <> "" Then
                InfosMail.Emetteur = Emetteur
            End If
            If Destinataire IsNot Nothing AndAlso Destinataire.Trim <> "" Then
                InfosMail.Destinataires.Add(Destinataire)
            End If
            InfosMail.ObjetSujet = Sujet
            InfosMail.Contenu = Contenu
            Return EnvoyerEmail(InfosMail)
        End Function

        Public Function SendMail(ByVal Contenu As String, ByVal Emetteur As String, ByVal Destinataires As List(Of String), ByVal NomPieceAttachee As String, ByVal StreamPieceAttachee As Byte()) As Boolean
            Dim InfosMail As DonneeMail = New DonneeMail
            Call ChargeDonneeMailParConfig(InfosMail)
            If Emetteur <> "" Then
                InfosMail.Emetteur = Emetteur
            End If
            If Not Destinataires IsNot Nothing AndAlso Destinataires.Count > 0 Then
                For Each it In Destinataires
                    InfosMail.Destinataires.Add(it.ToString())
                Next
            End If
            InfosMail.Contenu = Contenu
            InfosMail.NomPieceAttachee = NomPieceAttachee
            InfosMail.StreamPieceAttachee = StreamPieceAttachee
            Return EnvoyerEmail(InfosMail)
        End Function

        Public Function SendMail(ByVal Contenu As String, ByVal Emetteur As String, ByVal Destinataire As String, ByVal NomPieceAttachee As String, ByVal StreamPieceAttachee As Byte()) As Boolean

            Dim InfosMail As DonneeMail = New DonneeMail
            Call ChargeDonneeMailParConfig(InfosMail)
            If Emetteur <> "" Then
                InfosMail.Emetteur = Emetteur
            End If
            If Destinataire IsNot Nothing AndAlso Destinataire.Trim <> "" Then
                InfosMail.Destinataires.Add(Destinataire)
            End If
            InfosMail.Contenu = Contenu
            InfosMail.NomPieceAttachee = NomPieceAttachee
            InfosMail.StreamPieceAttachee = StreamPieceAttachee
            Return EnvoyerEmail(InfosMail)
        End Function

        Private Class DonneeMail
            Private WsServeurSmtp As String = ""
            Private WsPortSmtp As Integer = 25
            Private WsUtilsateurSmtp As String = ""
            Private WsPwSmtp As String = ""
            Private WsNomDomaine As String = ""
            Private WsSiCryptageSSLRequis As Boolean = False
            Private WsEmetteur As String = ""
            Private WsDestinataires As List(Of String) = New List(Of String)
            Private WsObjet As String = "Virtualia.Net"
            Private WsContenu As String = ""
            Private WsNomPieceAttachee As String = ""
            Private WsStreamPieceAttachee As Byte()

            Public Property ServeurSmtp() As String
                Get
                    Return WsServeurSmtp
                End Get
                Set(ByVal value As String)
                    WsServeurSmtp = value
                End Set
            End Property

            Public Property PortSmtp() As Integer
                Get
                    Return WsPortSmtp
                End Get
                Set(ByVal value As Integer)
                    WsPortSmtp = value
                End Set
            End Property

            Public Property UtilsateurSmtp() As String
                Get
                    Return WsUtilsateurSmtp
                End Get
                Set(ByVal value As String)
                    WsUtilsateurSmtp = value
                End Set
            End Property

            Public Property PwSmtp() As String
                Get
                    Return WsPwSmtp
                End Get
                Set(ByVal value As String)
                    WsPwSmtp = value
                End Set
            End Property

            Public Property NomDomaine() As String
                Get
                    Return WsNomDomaine
                End Get
                Set(ByVal value As String)
                    WsNomDomaine = value
                End Set
            End Property

            Public Property SiCryptageSSLRequis() As Boolean
                Get
                    Return WsSiCryptageSSLRequis
                End Get
                Set(ByVal value As Boolean)
                    WsSiCryptageSSLRequis = value
                End Set
            End Property

            Public Property Emetteur() As String
                Get
                    Return WsEmetteur
                End Get
                Set(ByVal value As String)
                    WsEmetteur = value
                End Set
            End Property

            Public Property Destinataires() As List(Of String)
                Get
                    Return WsDestinataires
                End Get
                Set(ByVal value As List(Of String))
                    WsDestinataires = value
                End Set
            End Property

            Public Property ObjetSujet() As String
                Get
                    Return WsObjet
                End Get
                Set(ByVal value As String)
                    WsObjet = value
                End Set
            End Property

            Public Property Contenu() As String
                Get
                    Return WsContenu
                End Get
                Set(ByVal value As String)
                    WsContenu = value
                End Set
            End Property

            Public Property NomPieceAttachee() As String
                Get
                    Return WsNomPieceAttachee
                End Get
                Set(ByVal value As String)
                    WsNomPieceAttachee = value
                End Set
            End Property

            Public Property StreamPieceAttachee() As Byte()
                Get
                    Return WsStreamPieceAttachee
                End Get
                Set(ByVal value As Byte())
                    WsStreamPieceAttachee = value
                End Set
            End Property
        End Class

    End Class
End Namespace
