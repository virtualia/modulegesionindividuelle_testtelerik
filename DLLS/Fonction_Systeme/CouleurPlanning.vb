﻿Option Strict On
Option Explicit On
Option Compare Text
Namespace Planning
    Public Class CouleurJour
        Private WsLibelle As String
        Private WsCouleurWeb As System.Drawing.Color
        Private WsCouleurHexa As String
        Private WsValeurs As List(Of String)

        Public Property Libelle As String
            Get
                Return WsLibelle
            End Get
            Set(value As String)
                WsLibelle = value
            End Set
        End Property

        Public Property Couleur_Web As System.Drawing.Color
            Get
                Return WsCouleurWeb
            End Get
            Set(value As System.Drawing.Color)
                WsCouleurWeb = value
            End Set
        End Property

        Public Property Couleur_Hexa As String
            Get
                Return WsCouleurHexa
            End Get
            Set(value As String)
                WsCouleurHexa = value
                WsCouleurWeb = ConvertCouleur(value)
            End Set
        End Property

        Public Property Valeur(ByVal Index As Integer) As String
            Get
                If Index > WsValeurs.Count - 1 Then
                    Return ""
                End If
                Return WsValeurs(Index)
            End Get
            Set(value As String)
                WsValeurs.Add(value)
            End Set
        End Property

        Public Property Valeurs As List(Of String)
            Get
                Return WsValeurs
            End Get
            Set(value As List(Of String))
                WsValeurs = value
            End Set
        End Property

        Public ReadOnly Property Couleur_Etiquette As System.Drawing.Color
            Get
                If WsCouleurWeb.GetBrightness > 0.35 Then
                    Return Drawing.Color.Black
                Else
                    Return Drawing.Color.White
                End If
            End Get
        End Property

        Private Function ConvertCouleur(ByVal Valeur As String) As System.Drawing.Color
            Dim R As Integer
            Dim G As Integer
            Dim B As Integer
            Select Case Valeur.Length
                Case Is = 6
                    R = CInt(Val("&H" & Strings.Left(Valeur, 2) & "&"))
                    G = CInt(Val("&H" & Strings.Mid(Valeur, 3, 2) & "&"))
                    B = CInt(Val("&H" & Strings.Right(Valeur, 2) & "&"))
                    Return System.Drawing.Color.FromArgb(R, G, B)
                Case Is = 7
                    R = CInt(Val("&H" & Strings.Mid(Valeur, 2, 2) & "&"))
                    G = CInt(Val("&H" & Strings.Mid(Valeur, 4, 2) & "&"))
                    B = CInt(Val("&H" & Strings.Right(Valeur, 2) & "&"))
                    Return System.Drawing.Color.FromArgb(R, G, B)
                Case Else
                    Return Drawing.Color.White
            End Select
        End Function

        Public Sub New()
            WsValeurs = New List(Of String)
        End Sub
    End Class
End Namespace