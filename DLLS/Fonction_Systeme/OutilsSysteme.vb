Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Outils
    Public Class Ordinateur
        Public ReadOnly Property NomMyUtilisateur() As String
            Get
                Dim TableauData() As String = Strings.Split(My.User.Name, "\")
                Return TableauData(1)
            End Get
        End Property

        Public ReadOnly Property DomaineMyUtilisateur() As String
            Get
                Dim TableauData() As String = Strings.Split(My.User.Name, "\")
                Return TableauData(0)
            End Get
        End Property

        Public ReadOnly Property NomdelOrdinateur() As String
            Get
                Return System.Environment.MachineName.ToString
            End Get
        End Property

        Public ReadOnly Property NomDNSOrdinateur() As String
            Get
                Return System.Net.Dns.GetHostEntry(NomdelOrdinateur).HostName
            End Get
        End Property

        Public ReadOnly Property AdresseIPOrdinateur() As String
            Get
                Dim NomDNS As String = System.Net.Dns.GetHostEntry(NomdelOrdinateur).HostName
                Return RenvoyerIP(NomDNS)
            End Get
        End Property

        Private Function RenvoyerIP(ByVal Hote As String) As String
            Dim Adresse As String = ""
            Dim IP As System.Net.IPHostEntry
            Dim IndiceM As Integer
            IP = System.Net.Dns.GetHostEntry(Hote)
            For IndiceM = 0 To IP.AddressList.Length - 1
                Select Case IP.AddressList(IndiceM).AddressFamily
                    Case Is = Net.Sockets.AddressFamily.InterNetwork
                        Adresse = IP.AddressList(IndiceM).ToString
                        Exit For
                End Select
            Next IndiceM
            Return Adresse
        End Function

        Public ReadOnly Property SiOrdinateurRelie() As Boolean
            Get
                Return My.Computer.Network.IsAvailable
            End Get
        End Property

        Public ReadOnly Property NomUtilisateurDomaine() As String
            Get
                Return System.Environment.UserDomainName.ToString
            End Get
        End Property

        Public ReadOnly Property NomUtilisateurOrdinateur() As String
            Get
                Return System.Environment.UserName.ToString
            End Get
        End Property

        Public ReadOnly Property VersiondelOS() As String
            Get
                Return System.Environment.OSVersion.ToString
            End Get
        End Property

        Public ReadOnly Property DossierSysteme() As String
            Get
                Return System.Environment.SystemDirectory.ToString
            End Get
        End Property

        Public ReadOnly Property DossierProgramFiles() As String
            Get
                Return My.Computer.FileSystem.SpecialDirectories.ProgramFiles.ToUpper
            End Get
        End Property

        Public ReadOnly Property DossierMesDocuments() As String
            Get
                Return My.Computer.FileSystem.SpecialDirectories.MyDocuments
            End Get
        End Property

        Public ReadOnly Property DossierTemp() As String
            Get
                Return My.Computer.FileSystem.SpecialDirectories.Temp.ToString
            End Get
        End Property

        Public ReadOnly Property DossierApplicationUtilisateur() As String
            Get
                Return My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData.ToString
            End Get
        End Property

        Public ReadOnly Property DossierApplicationAllUser() As String
            Get
                Return My.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData.ToString
            End Get
        End Property

        Public Function SiDossierExiste(ByVal NomDirectory As String) As Boolean

            Return My.Computer.FileSystem.DirectoryExists(NomDirectory)

        End Function

        Public Function SiFichierExiste(ByVal NomFichier As String) As Boolean

            Return My.Computer.FileSystem.FileExists(NomFichier)

        End Function

        Public Function TailleduFichier(ByVal NomFichier As String) As Long

            Dim InfoFic As System.IO.FileInfo = My.Computer.FileSystem.GetFileInfo(NomFichier)
            Return InfoFic.Length

        End Function

        Public Sub ActiverToucheVerNum()
            My.Computer.Registry.SetValue("HKEY_USERS\.DEFAULT\Control Panel\Keyboard", "InitialKeyboardIndicators", 2)
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\.DEFAULT\Control Panel\Keyboard", "InitialKeyboardIndicators", 2)
        End Sub

        Public Sub ArreterunProcess(ByVal NomProcessus As String)
            Dim InstanceP As System.Diagnostics.Process
            For Each InstanceP In Process.GetProcessesByName(NomProcessus)
                InstanceP.Kill()
            Next
        End Sub

    End Class
End Namespace

