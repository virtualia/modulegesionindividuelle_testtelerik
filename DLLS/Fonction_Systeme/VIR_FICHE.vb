﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.MetaModele.Outils
Namespace MetaModele
    Public Class VIR_FICHE
        Private WsLstDicoVirtuel As List(Of DictionnaireVirtuel)
        Private Const Tild As String = "~"
        Private Const PointDecimal As String = "."
        Private Const Virgule As String = ","
        Private WsIde_Dossier As Integer = 0
        Private WsDate_de_valeur As String = ""
        Private WsDate_de_fin As String = ""
        Private WsClef As String = ""
        Private WsCertification As String = ""
        Private WsContenuTable As String
        '
        Private WsID_Sgbd As String = ""
        Private WsINum_Lien As Integer = 0
        Private WsINum_Pvue As Integer = 1
        Private WsINum_Objet As Integer = 0
        Private WsSiACertifier As Boolean = False
        Private WsINum_InfoCer As Integer = 0
        Private WsDateDeFinCalculee As String = ""
        '
        Private WsTagNomPrenom As String = ""
        Private WsTab_Tag As ArrayList = Nothing
        Private TsTagVirtualia As String = ""
        '
        Private WsTab_Err As ArrayList = Nothing
        Public Overridable ReadOnly Property ID_Sgbd() As String
            Get
                Return WsID_Sgbd
            End Get
        End Property

        Public Overridable ReadOnly Property PointdeVue() As Integer
            Get
                Return WsINum_Pvue
            End Get
        End Property

        Public Overridable ReadOnly Property NumeroObjet() As Integer
            Get
                Return WsINum_Objet
            End Get
        End Property

        Public Overridable ReadOnly Property SiACertifier() As Boolean
            Get
                Return WsSiACertifier
            End Get
        End Property

        Public Overridable ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return WsINum_InfoCer
            End Get
        End Property

        Public Property Ide_Dossier() As Integer
            Get
                Return WsIde_Dossier
            End Get
            Set(ByVal value As Integer)
                WsIde_Dossier = value
            End Set
        End Property

        Public Property Date_de_Valeur() As String
            Get
                Return WsDate_de_valeur
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    WsDate_de_valeur = ""
                    Exit Property
                End If
                Try
                    ' WsDate_de_valeur = Strings.Format(DateValue(value), "dd/MM/yyyy")
                    WsDate_de_valeur = value
                Catch ex As Exception
                    WsDate_de_valeur = ""
                End Try
            End Set
        End Property
        'Private Sub EcrireLog(ByVal msg As String)
        '    Dim FicStream As System.IO.FileStream
        '    Dim FicWriter As System.IO.StreamWriter
        '    Dim NomLog As String
        '    Dim NomRep As String
        '    Dim SysFicLog As String = "PER_AKR.log"
        '    Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.UTF8

        '    NomRep = System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia")
        '    NomLog = Constantes.DossierVirtualiaService("Logs") & SysFicLog
        '    FicStream = New System.IO.FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
        '    FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)
        '    FicWriter.WriteLine(Strings.Format(System.DateTime.Now, "g"))
        '    FicWriter.WriteLine(msg)
        '    FicWriter.Flush()
        '    FicWriter.Close()
        'End Sub

        Public ReadOnly Property Date_Valeur_ToDate() As Date
            Get
                If WsDate_de_valeur = "" Then
                    Return DateValue("01/01/1950")
                Else
                    Try
                        Return DateValue(WsDate_de_valeur)
                    Catch ex As Exception
                        Return DateValue("01/01/1950")
                    End Try
                End If
            End Get
        End Property

        Public Property Date_de_Fin() As String
            Get
                Return WsDate_de_fin
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    WsDate_de_fin = ""
                    Exit Property
                End If
                Try
                    'WsDate_de_fin = Strings.Format(DateValue(value), "dd/MM/yyyy")
                    WsDate_de_fin = value
                Catch ex As Exception
                    WsDate_de_fin = ""
                End Try
            End Set
        End Property

        Public ReadOnly Property Date_Fin_ToDate() As Date
            Get
                If WsDate_de_fin = "" Then
                    Return DateValue("31/12/2299")
                Else
                    Try
                        Return DateValue(WsDate_de_fin)
                    Catch ex As Exception
                        Return DateValue("31/12/2299")
                    End Try
                End If
            End Get
        End Property

        Public Property DateDeFinCalculee() As String
            Get
                Return WsDateDeFinCalculee
            End Get
            Set(ByVal value As String)
                WsDateDeFinCalculee = value
            End Set
        End Property

        Public ReadOnly Property Date_FinCalculee_ToDate() As Date
            Get
                If WsDateDeFinCalculee = "" Then
                    Return DateValue("31/12/2299")
                Else
                    Try
                        Return DateValue(WsDateDeFinCalculee)
                    Catch ex As Exception
                        Return DateValue("31/12/2299")
                    End Try
                End If
            End Get
        End Property

        Public Property Clef() As String
            Get
                Return WsClef
            End Get
            Set(ByVal value As String)
                WsClef = value
            End Set
        End Property

        Public Property Certification() As String
            Get
                Return WsCertification
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsCertification = value
                    Case Else
                        WsCertification = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Overridable ReadOnly Property FicheLue() As String
            Get
                Return ""
            End Get
        End Property

        Public Overridable Property ContenuTable() As String
            Get
                Return WsContenuTable
            End Get
            Set(ByVal value As String)
                WsContenuTable = value
            End Set
        End Property

        Public Overridable Property IPoint_de_Vue() As Integer
            Get
                Return WsINum_Pvue
            End Get
            Set(ByVal value As Integer)
                WsINum_Pvue = value
            End Set
        End Property

        Public Overridable Property INum_Objet() As Integer
            Get
                Return WsINum_Objet
            End Get
            Set(ByVal value As Integer)
                WsINum_Objet = value
            End Set
        End Property

        Public Overridable Property INum_Lien() As Integer
            Get
                Return WsINum_Lien
            End Get
            Set(ByVal value As Integer)
                WsINum_Lien = value
            End Set
        End Property

        Public Overridable Property V_TableauData() As ArrayList
            Get
                Dim TableauW(0) As String
                Dim I As Integer
                Dim TabArray As New ArrayList
                TableauW = Strings.Split(ContenuTable, Tild, -1)
                For I = 0 To TableauW.Count - 1
                    TabArray.Add(TableauW(I))
                Next I
                Return TabArray
            End Get
            Set(ByVal value As ArrayList)
                Dim Chaine As New System.Text.StringBuilder
                Dim I As Integer
                For I = 0 To value.Count - 1
                    Chaine.Append(value(I).ToString & Tild)
                Next I
                ContenuTable = Ide_Dossier.ToString & Tild & Chaine.ToString
            End Set
        End Property

        Public Overridable ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(";", "-"), Tild)
                Chaine = New System.Text.StringBuilder
                If INum_Objet = 192 Or (INum_Objet = 8 And WsINum_Pvue = 2) Then
                    For IndiceI = 0 To TableauData.Count - 1
                        Chaine.Append(TableauData(IndiceI) & ";")
                    Next IndiceI
                Else
                    For IndiceI = 0 To TableauData.Count - 2
                        Chaine.Append(TableauData(IndiceI) & ";")
                    Next IndiceI
                End If

                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property CodeMiseAjour() As String
            Get
                If Ide_Dossier = 0 Then
                    Return ""
                End If
                Select Case FicheLue
                    Case Is = ""
                        Return "C"
                End Select
                If FicheLue <> ContenuTable Then
                    Return "M"
                End If
                Return ""
            End Get
        End Property

        Public Property V_TableauErreur() As ArrayList
            Get
                Return WsTab_Err
            End Get
            Set(ByVal value As ArrayList)
                WsTab_Err = value
            End Set
        End Property

        Public Property V_Tag As String
            Get
                Return TsTagVirtualia
            End Get
            Set(ByVal value As String)
                TsTagVirtualia = value
            End Set
        End Property

        Public Property V_Tag_NomPrenom As String
            Get
                Return WsTagNomPrenom
            End Get
            Set(ByVal value As String)
                WsTagNomPrenom = value
            End Set
        End Property

        Public Property V_TableauTag() As ArrayList
            Get
                If WsTab_Tag Is Nothing Then
                    WsTab_Tag = New ArrayList
                End If
                Return WsTab_Tag
            End Get
            Set(ByVal value As ArrayList)
                WsTab_Tag = value
            End Set
        End Property

        Public Property V_ListeDicoVirtuel As List(Of DictionnaireVirtuel)
            Get
                If WsLstDicoVirtuel Is Nothing Then
                    WsLstDicoVirtuel = New List(Of DictionnaireVirtuel)
                End If
                Return WsLstDicoVirtuel
            End Get
            Set(ByVal value As List(Of DictionnaireVirtuel))
                WsLstDicoVirtuel = value
            End Set
        End Property

        Public Shared Function F_FormatAlpha(ByVal Valeur As String, ByVal Longueur As Integer) As String

            Select Case Valeur.Length
                Case Is <= Longueur
                    Return Valeur.Trim()
                Case Else
                    Return Strings.Left(Valeur, Longueur).Trim()
            End Select

        End Function

        Public Shared Function F_FormatNumerique(ByVal Valeur As String) As String
            If Valeur = "" Then
                Return "0"
            End If
            If IsNumeric(Valeur) Then
                Return Valeur
            Else
                Return "0"
            End If
        End Function

        Public Shared Function F_FormatNumerique(ByVal Valeur As String, ByVal NbDecimales As Integer) As String
            If Valeur = "" Then
                Return "0"
            End If
            Dim Chaine As String

            If NbDecimales = 0 Then
                If IsNumeric(Valeur) Then
                    Return Valeur
                Else
                    Return "0"
                End If
            End If

            Chaine = Valeur.Replace(PointDecimal, Virgule)
            If IsNumeric(Chaine) Then
                Return Math.Round(CDbl(Chaine), NbDecimales).ToString
            End If

            Chaine = Valeur.Replace(Virgule, PointDecimal)
            If Val(Chaine) = 0 Then
                Return "0"
            End If
            Return Math.Round(Val(Chaine), NbDecimales).ToString
        End Function

        Public Overridable ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Return ""
            End Get
        End Property

        Public Sub New()
        End Sub
        Public Sub New(contenu As String)
            Me.New()
            Me.ContenuTable = contenu
        End Sub

        Protected Overrides Sub Finalize()
            GC.SuppressFinalize(Me)
        End Sub

    End Class

    Public Interface IGestionVirFicheBase
        ReadOnly Property NombredItems As Integer
        Sub Actualiser()
    End Interface

    Public Interface IGestionVirFiche(Of Out R As Virtualia.Systeme.MetaModele.VIR_FICHE)
        Inherits IGestionVirFicheBase
        Function GetFiche(ByVal Ide_Dossier As Integer) As R
        Function GetFiche(ByVal Intitule As String) As R
    End Interface

    Public Interface IGestionVirFicheModif(Of Out R As Virtualia.Systeme.MetaModele.VIR_FICHE)
        Inherits IGestionVirFiche(Of R)
        Sub SupprimerItem(ByVal Ide As Integer)
        Sub AjouterItem(ByVal Ide As Integer)
    End Interface
End Namespace

