Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.Fonctions

Namespace MetaModele
    Namespace Armoire
        Public Class ArmoiresRH
            Inherits List(Of ArmoireStandard)

            Private Const SysFicArmoire As String = "VirtualiaArmoires"
            Private WsRepertoire As String

            Public ReadOnly Property NomCompletFichierXml() As String
                Get
                    Return WsRepertoire & SysFicArmoire
                End Get
            End Property

            Public ReadOnly Property RepertoireduModele() As String
                Get
                    Return WsRepertoire
                End Get
            End Property

            Public ReadOnly Property NombredArmoires() As Integer
                Get
                    Return Count
                End Get
            End Property

            Public Overloads ReadOnly Property Item(ByVal Index As Integer) As ArmoireStandard
                Get
                    If (Index < 0 Or Index >= Count) Then
                        Return Nothing
                    End If

                    Return Me(Index)
                End Get
            End Property

            Public Sub New()
                Dim FicXML As FichierXML
                Dim LireXML As LectureXML
                Dim LireXMLExtraction As LectureXML
                Dim BaliseCategorie As String = "VirtualiaArmoires~Categorie"
                Dim BaliseSelection As String = "VirtualiaArmoires~Categorie~Selection"
                Dim BaliseExtraction As String = "VirtualiaArmoires~Categorie~Extraction"
                Dim Element As String
                Dim IArm As Integer
                Dim ICourant As Integer
                Dim ItemArmoire As ArmoireStandard

                WsRepertoire = VI.ParametreApplication("RepertoireVirtualia")
                If WsRepertoire <> "" Then
                    WsRepertoire = VI.DossierVirtualiaService("Modele")
                End If

                FicXML = New FichierXML(WsRepertoire & SysFicArmoire & ".Xsd")
                FicXML.FichierXML() = WsRepertoire & SysFicArmoire & ".Xml"

                LireXML = New LectureXML(FicXML)

                LireXML.SelectData(BaliseCategorie, "Nom")
                Do Until LireXML.EndOfData
                    Element = LireXML.GetData()
                    IArm = CreerUneArmoire()
                    ItemArmoire = Me(IArm)
                    ItemArmoire.Categorie = Element
                    ItemArmoire.Particularite = LireXML.GetDataVoisin(BaliseCategorie, "Particularite", "After")

                    Element = LireXML.GetDataVoisin(BaliseCategorie, "PointdeVue", "After")
                    If Element <> "" Then
                        ItemArmoire.PointdeVue = CInt(Val(Element))
                    End If

                    ICourant = 0
                    LireXMLExtraction = New LectureXML(FicXML)
                    LireXMLExtraction.RattacherAuPere(LireXML.PointeurDebut, LireXML.PointeurFin)
                    LireXMLExtraction.SelectData(BaliseSelection, "SelObjet")

                    Do Until LireXMLExtraction.EndOfData
                        Element = LireXMLExtraction.GetData()
                        If Element <> "" Then
                            ItemArmoire.ObjetSelection(ICourant) = CInt(Val(Element))
                        End If

                        Element = LireXML.GetDataVoisin(BaliseSelection, "SelInformation", "After")
                        If Element <> "" Then
                            ItemArmoire.InfoSelection(ICourant) = CInt(Val(Element))
                        End If

                        ItemArmoire.ConditionSelection(ICourant) = LireXML.GetDataVoisin(BaliseSelection, "SelCondition", "After")
                        ICourant += 1
                        LireXMLExtraction.FindNextData()
                    Loop

                    Element = ""
                    ICourant = 0
                    LireXMLExtraction = New LectureXML(FicXML)
                    LireXMLExtraction.RattacherAuPere(LireXML.PointeurDebut, LireXML.PointeurFin)
                    LireXMLExtraction.SelectData(BaliseExtraction, "Objet")

                    Do Until LireXMLExtraction.EndOfData
                        Element = LireXMLExtraction.GetData()
                        If Element <> "" Then
                            ItemArmoire.ObjetExtrait(ICourant) = CInt(Val(Element))
                        End If

                        Element = LireXML.GetDataVoisin(BaliseSelection, "Information", "After")
                        If Element <> "" Then
                            ItemArmoire.InfoExtraite(ICourant) = CInt(Val(Element))
                        End If

                        Element = LireXML.GetDataVoisin(BaliseSelection, "Tri", "After")
                        If Element <> "" Then
                            ItemArmoire.TriExtrait(ICourant) = CInt(Val(Element))
                        End If

                        Element = LireXML.GetDataVoisin(BaliseSelection, "Rupture", "After")
                        If Element <> "" Then
                            ItemArmoire.NiveauHierarchique(ICourant) = CInt(Val(Element))
                        End If

                        ICourant += 1
                        LireXMLExtraction.FindNextData()
                    Loop

                    LireXMLExtraction = Nothing
                    LireXML.FindNextData()
                Loop
            End Sub

            Private Function CreerUneArmoire() As Integer
                Dim ObjetArmoire As ArmoireStandard = New ArmoireStandard(Me)
                Add(ObjetArmoire)
                ObjetArmoire.VIndex = Count - 1
                Return Count - 1
            End Function

        End Class
    End Namespace
End Namespace
