Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Parametrage
    Namespace BasesdeDonnees
        Public Class ParamSgbdDatabase
            Inherits List(Of ParamSgbdConnexion)
            Private ReadOnly WsObjetSgbd As ParamSgbd

            Private WsIndex As Integer
            Private WsNumero As Integer = 0
            Private WsIntitule As String = ""
            Private WsNom As String = ""
            Private WsTypeSgbdAlpha As String = ""
            Private WsFormatdesDatesAlpha As String = "Fr"
            Private WsSymboleDecimal As String = ","
            Private WsSchema As String = ""
            Private WsSiTraceSql As Integer = 0

            Public Property VIndex() As Integer
                Get
                    Return WsIndex
                End Get
                Set(ByVal value As Integer)
                    WsIndex = value
                End Set
            End Property

            Public Property Numero() As Integer
                Get
                    Return WsNumero
                End Get
                Set(ByVal value As Integer)
                    WsNumero = value
                End Set
            End Property

            Public Property Intitule() As String
                Get
                    Return WsIntitule
                End Get
                Set(ByVal value As String)
                    WsIntitule = value
                End Set
            End Property

            Public Property Nom() As String
                Get
                    Return WsNom
                End Get
                Set(ByVal value As String)
                    WsNom = value
                End Set
            End Property

            Public Property TypeduSgbdAlpha() As String
                Get
                    Return WsTypeSgbdAlpha
                End Get
                Set(ByVal value As String)
                    WsTypeSgbdAlpha = value
                End Set
            End Property

            Public ReadOnly Property TypeduSgbd() As Integer
                Get
                    Select Case TypeduSgbdAlpha
                        Case Is = VI.XmlSgbdrSqlServer
                            Return VI.TypeSgbdrNumeric.SgbdrSqlServer
                        Case Is = VI.XmlSgbdrOracle
                            Return VI.TypeSgbdrNumeric.SgbdrOracle
                        Case Is = VI.XmlSgbdrDb2
                            Return VI.TypeSgbdrNumeric.SgbdrDb2
                        Case Is = VI.XmlSgbdrMySql
                            Return VI.TypeSgbdrNumeric.SgbdrMySql
                        Case Is = VI.XmlSgbdrAccess
                            Return VI.TypeSgbdrNumeric.SgbdrAccess
                        Case Is = VI.XmlSgbdrProgress
                            Return VI.TypeSgbdrNumeric.SgbdrProgress
                        Case Else
                            Return VI.TypeSgbdrNumeric.SgbdrSqlServer
                    End Select
                End Get
            End Property

            Public Property FormatdesDatesAlpha() As String
                Get
                    Return WsFormatdesDatesAlpha
                End Get
                Set(ByVal value As String)
                    WsFormatdesDatesAlpha = value
                End Set
            End Property

            Public ReadOnly Property FormatDesDates() As Integer
                Get
                    Select Case FormatdesDatesAlpha
                        Case Is = "Uk"
                            Return VI.FormatDateSgbdr.DateGB
                        Case Is = "Us"
                            Return VI.FormatDateSgbdr.DateUS
                        Case Else
                            Return VI.FormatDateSgbdr.DateStandard
                    End Select
                End Get
            End Property

            Public Property SymboleDecimal() As String
                Get
                    Return WsSymboleDecimal
                End Get
                Set(ByVal value As String)
                    Select Case value
                        Case ",", "."
                            WsSymboleDecimal = value
                        Case Else
                            WsSymboleDecimal = ","
                    End Select
                End Set
            End Property

            Public Property Schema() As String
                Get
                    Return WsSchema
                End Get
                Set(ByVal value As String)
                    WsSchema = value
                End Set
            End Property

            Public Property SiTraceSql() As Integer
                Get
                    Return WsSiTraceSql
                End Get
                Set(ByVal value As Integer)
                    WsSiTraceSql = value
                End Set
            End Property

            Public ReadOnly Property NombredeConnexions() As Integer
                Get
                    Return Count
                End Get
            End Property

            Public Overloads ReadOnly Property Item(ByVal Index As Integer) As ParamSgbdConnexion
                Get
                    If (Index < 0 Or Index > Count - 1) Then
                        Return Nothing
                    End If

                    Return Me(Index)
                End Get
            End Property

            Public Function CreerUneConnexion() As Integer
                Dim ObjetCnx As ParamSgbdConnexion = New ParamSgbdConnexion(Me)
                Add(ObjetCnx)
                ObjetCnx.NumDatabase = Numero
                ObjetCnx.VIndex = Count - 1

                Return Count - 1

            End Function

            Public Sub New(ByVal host As ParamSgbd)
                WsObjetSgbd = host
            End Sub

            Public Sub New()
                MyBase.New()
            End Sub
        End Class
    End Namespace
End Namespace
