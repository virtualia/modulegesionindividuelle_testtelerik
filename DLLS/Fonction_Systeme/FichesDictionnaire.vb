﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele.Donnees

Namespace MetaModele
    Namespace Outils
        Public Class FicheObjetDictionnaire
            Private WsPointeur As Virtualia.Systeme.MetaModele.Donnees.Objet
            Public Property VIndex As Integer

            Public ReadOnly Property PointeurModele() As Virtualia.Systeme.MetaModele.Donnees.Objet
                Get
                    Return WsPointeur
                End Get
            End Property

            Public ReadOnly Property PointdeVue() As Integer
                Get
                    Return WsPointeur.PointdeVue
                End Get
            End Property

            Public ReadOnly Property Objet() As Integer
                Get
                    Return WsPointeur.Numero
                End Get
            End Property

            Public ReadOnly Property Intitule() As String
                Get
                    Return WsPointeur.Intitule
                End Get
            End Property

            Public ReadOnly Property Categorie() As Integer
                Get
                    Return WsPointeur.CategorieRH
                End Get
            End Property

            Public ReadOnly Property SiCertification() As Integer
                Get
                    Return WsPointeur.SiCertification
                End Get
            End Property

            Public Sub New(ByVal Pointeur As Virtualia.Systeme.MetaModele.Donnees.Objet)
                WsPointeur = Pointeur
            End Sub
        End Class

        Public Class FicheInfoDictionnaire
            Private WsPointeur As Virtualia.Systeme.MetaModele.Donnees.Information
            Private WsIndex As Integer

            Public ReadOnly Property PointeurModele() As Virtualia.Systeme.MetaModele.Donnees.Information
                Get
                    Return WsPointeur
                End Get
            End Property

            Public Property VIndex() As Integer
                Get
                    Return WsIndex
                End Get
                Set(ByVal value As Integer)
                    WsIndex = value
                End Set
            End Property

            Public ReadOnly Property PointdeVue() As Integer
                Get
                    Return WsPointeur.PointdeVue
                End Get
            End Property

            Public ReadOnly Property Objet() As Integer
                Get
                    Return WsPointeur.Objet
                End Get
            End Property

            Public ReadOnly Property Information() As Integer
                Get
                    Return WsPointeur.Numero
                End Get
            End Property

            Public ReadOnly Property Intitule() As String
                Get
                    Return WsPointeur.Intitule
                End Get
            End Property

            Public ReadOnly Property Categorie() As Integer
                Get
                    Return WsPointeur.VParent.CategorieRH
                End Get
            End Property

            Public ReadOnly Property SiEditable(ByVal Index As Integer) As Boolean
                Get
                    Dim NoEdition As Integer = 0
                    Select Case Index
                        Case VI.OptionInfo.DicoEtiVisible To VI.OptionInfo.DicoSpecifique
                            NoEdition = WsPointeur.ZoneEdition(Index)
                    End Select
                    Select Case NoEdition
                        Case 0
                            Return False
                        Case Else
                            Return True
                    End Select
                End Get
            End Property

            Public Sub New(ByVal Pointeur As Virtualia.Systeme.MetaModele.Donnees.Information)
                WsPointeur = Pointeur
            End Sub

        End Class

        Public Class FicheInfoExperte
            Private WsPointeur As Virtualia.Systeme.MetaModele.Expertes.InformationExperte
            Private WsIndex As Integer
            Private WsPointdeVue As Integer
            Private WsNoObjet As Integer
            Private WsNoInfo As Integer
            Private WsIntitule As String

            Public ReadOnly Property PointeurModele() As Virtualia.Systeme.MetaModele.Expertes.InformationExperte
                Get
                    Return WsPointeur
                End Get
            End Property

            Public Property VIndex() As Integer
                Get
                    Return WsIndex
                End Get
                Set(ByVal value As Integer)
                    WsIndex = value
                End Set
            End Property

            Public ReadOnly Property PointdeVue() As Integer
                Get
                    Return WsPointdeVue
                End Get
            End Property

            Public ReadOnly Property Objet() As Integer
                Get
                    Return WsNoObjet
                End Get
            End Property

            Public ReadOnly Property Information() As Integer
                Get
                    Return WsNoInfo
                End Get
            End Property

            Public ReadOnly Property Intitule() As String
                Get
                    Return WsIntitule
                End Get
            End Property

            Public ReadOnly Property Descriptif() As String
                Get
                    Return WsPointeur.Description
                End Get
            End Property

            Public ReadOnly Property SiEditable(ByVal Index As Integer) As Boolean
                Get
                    Dim NoEdition As Integer = 0
                    Select Case Index
                        Case VI.OptionInfo.DicoEtiVisible To VI.OptionInfo.DicoSpecifique
                            NoEdition = WsPointeur.ZoneEdition(Index)
                    End Select
                    Select Case NoEdition
                        Case 0
                            Return False
                        Case Else
                            Return True
                    End Select
                End Get
            End Property

            Public Sub New(ByVal Pointeur As Virtualia.Systeme.MetaModele.Expertes.InformationExperte)
                WsPointeur = Pointeur
                WsPointdeVue = Pointeur.PointdeVue
                WsNoObjet = Pointeur.Objet
                WsNoInfo = Pointeur.Numero
                WsIntitule = Pointeur.Intitule
            End Sub

        End Class

        Public Class FicheObjetDictionnaireCollection
            Inherits List(Of FicheObjetDictionnaire)

            Public Sub New()
                MyBase.New()
            End Sub
            Public Sub New(lst As IEnumerable(Of FicheObjetDictionnaire))
                MyBase.New(lst)
            End Sub

            Public Overloads Sub Add(fo As FicheObjetDictionnaire)
                MyBase.Add(fo)
                fo.VIndex = Count
            End Sub
            Public Overloads Sub Add(obj As Objet)
                If (obj Is Nothing) Then
                    Return
                End If

                Me.Add(New FicheObjetDictionnaire(obj))
            End Sub

            Public Function ToPointeurDicoObjet(ByVal PointdeVue As Integer, ByVal Numobjet As Integer) As Objet
                Dim f As FicheObjetDictionnaire = (From it In Me Where it.PointdeVue = PointdeVue And it.Objet = Numobjet Select it).FirstOrDefault()

                If (f Is Nothing) Then
                    Return Nothing
                End If

                Return f.PointeurModele
            End Function
        End Class

        Public Class FicheInfoDictionnaireCollection
            Inherits List(Of FicheInfoDictionnaire)

            Public Overloads Sub Add(fo As FicheInfoDictionnaire)
                MyBase.Add(fo)
                fo.VIndex = Count
            End Sub
            Public Overloads Sub Add(obj As Virtualia.Systeme.MetaModele.Donnees.Information)
                If (obj Is Nothing) Then
                    Return
                End If

                Me.Add(New FicheInfoDictionnaire(obj))
            End Sub

            Public Sub New()
                MyBase.New()
            End Sub
            Public Sub New(lst As IEnumerable(Of FicheInfoDictionnaire))
                MyBase.New(lst)
            End Sub

            Public Function ToPointeurDicoInfo(ByVal PointdeVue As Integer, ByVal Numobjet As Integer, ByVal NumInfo As Integer) As Virtualia.Systeme.MetaModele.Donnees.Information
                Dim f As FicheInfoDictionnaire = (From it In Me Where it.PointdeVue = PointdeVue And it.Objet = Numobjet And it.Information = NumInfo Select it).FirstOrDefault()

                If (f Is Nothing) Then
                    Return Nothing
                End If

                Return f.PointeurModele
            End Function

        End Class

        Public Class FicheInfoExperteCollection
            Inherits List(Of FicheInfoExperte)

            Public Overloads Sub Add(fo As FicheInfoExperte)
                MyBase.Add(fo)
                fo.VIndex = Count
            End Sub
            Public Overloads Sub Add(obj As Virtualia.Systeme.MetaModele.Expertes.InformationExperte)
                If (obj Is Nothing) Then
                    Return
                End If

                Me.Add(New FicheInfoExperte(obj))
            End Sub

            Public Sub New()
                MyBase.New()
            End Sub
            Public Sub New(lst As IEnumerable(Of FicheInfoExperte))
                MyBase.New(lst)
            End Sub

            Public Function ToPointeurDicoInfo(ByVal PointdeVue As Integer, ByVal Numobjet As Integer, ByVal NumInfo As Integer) As Virtualia.Systeme.MetaModele.Expertes.InformationExperte
                Dim f As FicheInfoExperte = (From it In Me Where it.PointdeVue = PointdeVue And it.Objet = Numobjet And it.Information = NumInfo Select it).FirstOrDefault()

                If (f Is Nothing) Then
                    Return Nothing
                End If

                Return f.PointeurModele
            End Function
        End Class

    End Namespace
End Namespace
