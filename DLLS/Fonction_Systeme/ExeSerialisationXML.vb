﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports System.IO
Imports System.Xml.Serialization
Namespace Outils
    Public Class ExeSerialisationXML(Of T As {Class, New})
        Private WsMessage As String = ""

        Private Const Cst_Symbole_SigneInferieur As String = "&lt;"
        Private Const Cst_Symbole_SigneSuperieur As String = "&gt;"
        Private Const Cst_Symbole_Quote As String = "&apos;"
        Private Const Cst_Symbole_DoubleQuote As String = "&quot;"
        Private Const Cst_Symbole_EtCommercial As String = "&amp;"

        Public Function Deserialise(ByVal NomFichier As String) As T
            If My.Computer.FileSystem.FileExists(NomFichier) = False Then
                WsMessage = "Le fichier n'existe pas."
                Return Nothing
            End If
            Dim ObjStreamReader As System.IO.StreamReader = New System.IO.StreamReader(NomFichier)
            Dim InstanceT As T = New T
            Dim Exe As New XmlSerializer(InstanceT.GetType)
            Try
                InstanceT = Exe.Deserialize(ObjStreamReader)
            Catch ex As Exception
                WsMessage = ex.Message
                ObjStreamReader.Close()
                Return Nothing
            End Try
            ObjStreamReader.Close()
            Return InstanceT
        End Function

        Public Function VirSerialise(ByVal InstanceT As T, ByVal NomFichier As String) As Boolean
            Dim ObjStreamWriter As System.IO.StreamWriter = New System.IO.StreamWriter(NomFichier)
            Dim Exe As System.Xml.Serialization.XmlSerializer = New System.Xml.Serialization.XmlSerializer(InstanceT.GetType)
            Try
                Exe.Serialize(ObjStreamWriter, InstanceT)
            Catch ex As Exception
                WsMessage = ex.Message
                ObjStreamWriter.Close()
                Return False
            End Try
            ObjStreamWriter.Close()
            Return True
        End Function

        Public Sub VerifierFichier(ByVal NomFichier As String)
            Dim CodeIso As System.Text.Encoding = System.Text.Encoding.UTF8
            Dim FLuxLecture As System.IO.FileStream
            Dim FicReader As System.IO.StreamReader
            Dim FLuxEcriture As System.IO.FileStream
            Dim FicWriter As System.IO.StreamWriter
            Dim Champlu As String
            Dim NomBak As String = My.Computer.FileSystem.GetFileInfo(NomFichier).DirectoryName & "\VirCopie_" & My.Computer.FileSystem.GetName(NomFichier)
            Dim CodeIsoLu As System.Text.Encoding

            My.Computer.FileSystem.CopyFile(NomFichier, NomBak, True)
            CodeIsoLu = ObtenirEncoding(NomBak)

            FLuxLecture = New System.IO.FileStream(NomBak, IO.FileMode.Open, IO.FileAccess.Read)
            FicReader = New System.IO.StreamReader(FLuxLecture, CodeIsoLu)

            FLuxEcriture = New System.IO.FileStream(NomFichier, IO.FileMode.Create, IO.FileAccess.Write)
            FicWriter = New System.IO.StreamWriter(FLuxEcriture, CodeIso)

            Champlu = FicReader.ReadLine
            Do Until FicReader.EndOfStream
                If Strings.InStr(Champlu, Cst_Symbole_EtCommercial) = 0 Then
                    FicWriter.WriteLine(Champlu.Replace("&", Cst_Symbole_EtCommercial))
                Else
                    FicWriter.WriteLine(Champlu)
                End If
                Champlu = FicReader.ReadLine
            Loop
            FicWriter.WriteLine(Champlu.Trim)
            FicWriter.Flush()
            FicWriter.Close()
            FicReader.Close()
            My.Computer.FileSystem.DeleteFile(NomBak)

        End Sub

        Private Function ObtenirEncoding(ByVal NomFichier As String) As System.Text.Encoding
            Dim Encodage As System.Text.Encoding = Nothing
            Dim FLuxLecture As System.IO.FileStream
            Dim ByteOrderMark As Byte()

            FLuxLecture = New System.IO.FileStream(NomFichier, IO.FileMode.Open, IO.FileAccess.Read)
            If FLuxLecture.CanSeek Then
                ReDim ByteOrderMark(4)
                FLuxLecture.Read(ByteOrderMark, 0, 4)
                If ByteOrderMark(0) = 239 And ByteOrderMark(1) = 187 And ByteOrderMark(2) = 191 Then
                    Encodage = System.Text.Encoding.UTF8
                ElseIf (ByteOrderMark(0) = 255 And ByteOrderMark(1) = 254) Or (ByteOrderMark(0) = 0 And ByteOrderMark(1) = 0 And ByteOrderMark(2) = 254 And ByteOrderMark(3) = 255) Then
                    Encodage = System.Text.Encoding.Unicode
                ElseIf ByteOrderMark(0) = 254 And ByteOrderMark(1) = 255 Then
                    Encodage = System.Text.Encoding.BigEndianUnicode
                Else
                    Encodage = System.Text.Encoding.Default
                End If
                FLuxLecture.Seek(0, SeekOrigin.Begin)
            Else
                Encodage = System.Text.Encoding.Default
            End If
            FLuxLecture.Close()
            Return Encodage
        End Function

        Private Function ChaineXMLValide(ByVal Valeur As String) As String
            Dim Chaine As String
            If Valeur = "" Then
                Return Valeur
            End If
            Chaine = Valeur
            Select Case Strings.InStr(Valeur, "<")
                Case Is > 0
                    Chaine = Strings.Replace(Valeur, "<", Cst_Symbole_SigneInferieur)
                    Valeur = Chaine
            End Select
            Select Case Strings.InStr(Valeur, ">")
                Case Is > 0
                    Chaine = Strings.Replace(Valeur, ">", Cst_Symbole_SigneSuperieur)
            End Select
            Select Case Strings.InStr(Valeur, "'")
                Case Is > 0
                    Chaine = Strings.Replace(Valeur, "'", Cst_Symbole_Quote)
            End Select
            Select Case Strings.InStr(Valeur, "&")
                Case Is > 0
                    Chaine = Strings.Replace(Valeur, "&", Cst_Symbole_EtCommercial)
            End Select
            Return Chaine
        End Function

        Public ReadOnly Property Message_Erreur As String
            Get
                Return WsMessage
            End Get
        End Property
    End Class
End Namespace