﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Fonctions
Namespace Parametrage
    Namespace Produit
        Public Class VParamXMLStandard
            Inherits List(Of VSousSection)
            Private WsRepertoire As String
            Private SysFicParam As String

            Public ReadOnly Property NomCompletFichierXml() As String
                Get
                    Return WsRepertoire & SysFicParam
                End Get
            End Property

            Public ReadOnly Property NombredeSousSections() As Integer
                Get
                    Return Count
                End Get
            End Property

            Public Overloads ReadOnly Property Item(ByVal Index As Integer) As VSousSection
                Get
                    If (Index < 0 Or Index > Count - 1) Then
                        Return Nothing
                    End If

                    Return Me(Index)
                End Get
            End Property

            Public Sub New(ByVal NomRepertoire As String, ByVal NomFichier As String)
                Dim FicXML As FichierXML
                Dim LireXML As LectureXML
                Dim LireXMLSousSection As LectureXML
                Dim LireXMLValeurs As LectureXML
                Dim BaliseSection As String = "VirtualiaConfiguration~Section"
                Dim BaliseSousSection As String = "VirtualiaConfiguration~Section~SousSection"
                Dim BaliseParametres As String = "VirtualiaConfiguration~Section~SousSection~Parametres"
                Dim Element As String
                Dim IParam As Integer
                Dim NomSection As String = ""
                Dim Cpt As Integer

                Dim ItemParametre As VSousSection = Nothing

                WsRepertoire = NomRepertoire
                SysFicParam = NomFichier

                FicXML = New FichierXML(WsRepertoire & Strings.Left(SysFicParam, SysFicParam.Length - 3) & "Xsd")
                FicXML.FichierXML() = WsRepertoire & SysFicParam

                LireXML = New LectureXML(FicXML)

                LireXML.SelectData(BaliseSection, "NomSection")
                Do Until LireXML.EndOfData
                    NomSection = LireXML.GetData()

                    LireXMLSousSection = New LectureXML(FicXML)
                    LireXMLSousSection.RattacherAuPere(LireXML.PointeurDebut, LireXML.PointeurFin)
                    LireXMLSousSection.SelectData(BaliseSousSection, "NomSousSection")
                    Do Until LireXMLSousSection.EndOfData
                        Element = LireXMLSousSection.GetData()
                        IParam = CreerUneSousSection(NomSection, Element)

                        LireXMLValeurs = New LectureXML(FicXML)
                        LireXMLValeurs.RattacherAuPere(LireXMLSousSection.PointeurDebut, LireXMLSousSection.PointeurFin)
                        LireXMLValeurs.SelectData(BaliseParametres, "Clef")
                        Do Until LireXMLValeurs.EndOfData
                            ItemParametre = Me(IParam)
                            ItemParametre.ElementCompile("Clef") = LireXMLValeurs.GetData()
                            ItemParametre.ElementCompile("Valeur") = LireXMLValeurs.GetDataVoisin(BaliseParametres, "Valeur", "After")
                            LireXMLValeurs.FindNextData()
                        Loop
                        LireXMLValeurs = Nothing
                        Cpt = ItemParametre.CompteClefs
                        LireXMLSousSection.FindNextData()
                    Loop
                    LireXMLSousSection = Nothing
                    Element = ""
                    LireXML.FindNextData()
                Loop
            End Sub

            Private Function CreerUneSousSection(ByVal VSection As String, ByVal VSousSection As String) As Integer
                Dim Param As VSousSection = New VSousSection()
                Param.Categorie = VSection
                Param.Nom = VSousSection
                Add(Param)
                Param.VIndex = Count - 1
                Return Count - 1
            End Function
        End Class
    End Namespace
End Namespace

