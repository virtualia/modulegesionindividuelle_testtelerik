﻿Option Strict On
Option Explicit On
Option Compare Text
Namespace Planning
    Public Class CouleursJour
        Inherits List(Of CouleurJour)
        Public Function AjouterItem(ByVal Etiquette As String, ByVal HexaCouleur As String, ByVal LstValeurs As List(Of String)) As CouleurJour
            Dim Occurence As New CouleurJour
            Occurence.Libelle = Etiquette
            Occurence.Couleur_Hexa = HexaCouleur
            Occurence.Valeurs = LstValeurs
            Me.Add(Occurence)
            Return Occurence
        End Function

        Public Sub Initialiser_Defaut()
            Dim Occurence As CouleurJour
            Dim IndiceA As Integer
            Me.Clear()
            For IndiceA = 0 To 7
                Occurence = New CouleurJour
                Select Case IndiceA
                    Case 0
                        Occurence.Couleur_Web = Drawing.Color.LightGray
                        Occurence.Libelle = "Jours travaillés"
                    Case 1
                        Occurence.Couleur_Web = Drawing.Color.Gray
                        Occurence.Libelle = "Jours non travaillés"
                    Case 2
                        Occurence.Couleur_Web = Drawing.Color.Snow
                        Occurence.Libelle = "Missions"
                        Occurence.Valeur(0) = "Mission"
                    Case 3
                        Occurence.Couleur_Web = Drawing.Color.Green
                        Occurence.Libelle = "Formations"
                        Occurence.Valeur(0) = "Formation"
                        Occurence.Valeur(1) = "Formation matinée"
                        Occurence.Valeur(2) = "Formation aprés-midi"
                    Case 4
                        Occurence.Couleur_Web = Drawing.Color.Orange
                        Occurence.Libelle = "Prévisionnel"
                    Case 5
                        Occurence.Couleur_Web = Drawing.Color.LightBlue
                        Occurence.Libelle = "Congés annuels"
                        Occurence.Valeur(0) = "Congés annuels"
                    Case 6
                        Occurence.Couleur_Web = Drawing.Color.Salmon
                        Occurence.Libelle = "Journées ARTT"
                        Occurence.Valeur(0) = "Journées ARTT"
                    Case 7
                        Occurence.Couleur_Web = Drawing.Color.Yellow
                        Occurence.Libelle = "Maladies et maternités"
                        Occurence.Valeur(0) = "Maladie"
                        Occurence.Valeur(1) = "Maternité"
                End Select
                Me.Add(Occurence)
            Next IndiceA
        End Sub

        Public ReadOnly Property LegendePlanning(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To Me.Count - 1
                        Return Me.Item(Index).Libelle
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public ReadOnly Property LibellePlanning(ByVal Index As Integer, ByVal Rang As Integer) As String
            Get
                Select Case Index
                    Case 0 To Me.Count - 1
                        Return Me.Item(Index).Valeur(Rang)
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public ReadOnly Property CouleurPlanning(ByVal Index As Integer) As System.Drawing.Color
            Get
                Select Case Index
                    Case 0 To Me.Count - 1
                        Return Me.Item(Index).Couleur_Web
                    Case Else
                        Return Drawing.Color.LightGray
                End Select
            End Get
        End Property

        Public ReadOnly Property IndexCouleurPlanning(ByVal Intitule As String) As Integer
            Get
                Dim I As Integer
                Dim R As Integer
                For I = 0 To Me.Count - 1
                    For R = 0 To Me.Item(I).Valeurs.Count - 1
                        If Me.Item(I).Valeurs(R) = Intitule Then
                            Return I
                        End If
                    Next R
                Next I
                '** Approchant ***
                For I = 0 To Me.Count - 1
                    If Strings.Left(Me.Item(I).Libelle, 6) = Strings.Left(Intitule, 6) Then
                        Return I
                    End If
                    For R = 0 To Me.Item(I).Valeurs.Count - 1
                        If Strings.Left(Me.Item(I).Valeurs(R), 6) = Strings.Left(Intitule, 6) Then
                            Return I
                        End If
                    Next R
                Next I
                Return 1
            End Get
        End Property
    End Class
End Namespace