Option Strict On
Option Explicit On
Option Compare Text
Namespace MetaModele
    Namespace Donnees
        Public Class Produit
            Private WsNumeroLicence As String
            Private WsProduit As String
            Private WsVersion As String
            Private WsIDDN As String
            Private WsSiteWeb As String
            Private WsOrganisme As String
            Private WsNbAcces As Integer
            Private WsSiIntranet As Integer
            Private WsModele As String
            Private WsTypeModele As Integer
            Private WsClef As String
            Private WsClefCryptage As String

            Public Property NumVersion() As String
                Get
                    Return WsVersion
                End Get
                Set(ByVal value As String)
                    WsVersion = value
                End Set
            End Property

            Public Property NumeroLicence() As String
                Get
                    Return WsNumeroLicence
                End Get
                Set(ByVal value As String)
                    WsNumeroLicence = value
                End Set
            End Property

            Public Property NomProduit() As String
                Get
                    Return WsProduit
                End Get
                Set(ByVal value As String)
                    WsProduit = value
                End Set
            End Property

            Public Property NumeroIDDN() As String
                Get
                    Return WsIDDN
                End Get
                Set(ByVal value As String)
                    WsIDDN = value
                End Set
            End Property

            Public Property SiteWeb() As String
                Get
                    Return WsSiteWeb
                End Get
                Set(ByVal value As String)
                    WsSiteWeb = value
                End Set
            End Property

            Public Property Organisme() As String
                Get
                    Return WsOrganisme
                End Get
                Set(ByVal value As String)
                    WsOrganisme = value
                End Set
            End Property

            Public Property NomModele() As String
                Get
                    Return WsModele
                End Get
                Set(ByVal value As String)
                    WsModele = value
                End Set
            End Property

            Public Property ClefModele() As Integer
                Get
                    Return WsTypeModele
                End Get
                Set(ByVal value As Integer)
                    WsTypeModele = value
                End Set
            End Property

            Public Property ClefProduit() As String
                Get
                    Return WsClef
                End Get
                Set(ByVal value As String)
                    WsClef = value
                End Set
            End Property

            Public Property ClefCryptage() As String
                Get
                    Return WsClefCryptage
                End Get
                Set(ByVal value As String)
                    WsClefCryptage = value
                End Set
            End Property

            Public Property NombreAcces() As Integer
                Get
                    Return WsNbAcces
                End Get
                Set(ByVal value As Integer)
                    WsNbAcces = value
                End Set
            End Property

            Public Property Intranet() As Integer
                Get
                    Return WsSiIntranet
                End Get
                Set(ByVal value As Integer)
                    WsSiIntranet = value
                End Set
            End Property

            Public ReadOnly Property SiClefOk() As Boolean
                Get
                    Dim ChaineSha As String
                    Dim RhFonction As Virtualia.Systeme.Fonctions.Generales
                    Dim IndiceA As Integer
                    Dim ChaineAComparer As String
                    Dim Cpt As Integer
                    RhFonction = New Virtualia.Systeme.Fonctions.Generales
                    ChaineSha = RhFonction.HashSHA("V_" & NumeroLicence.Trim & "RpMb" & NombreAcces.ToString.Trim & "_V")
                    ChaineAComparer = ""
                    Cpt = 0
                    For IndiceA = 1 To ChaineSha.Length Step 8
                        ChaineAComparer = ChaineAComparer & Mid(ChaineSha, IndiceA, 5)
                        Cpt += 1
                        Select Case Cpt
                            Case Is < 5
                                ChaineAComparer = ChaineAComparer & "-"
                            Case Else
                                Exit For
                        End Select
                    Next IndiceA
                    If ChaineAComparer = ClefProduit Then
                        Return True
                    Else
                        Return False
                    End If
                End Get
            End Property

            Public Sub New()
                WsProduit = "Virtualia"
            End Sub

        End Class
    End Namespace
End Namespace
