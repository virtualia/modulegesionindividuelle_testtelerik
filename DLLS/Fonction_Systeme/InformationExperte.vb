Option Strict On
Option Explicit On
Option Compare Text
Namespace MetaModele
    Namespace Expertes
        Public Class InformationExperte
            Private ReadOnly ObjetVirtualia As Virtualia.Systeme.MetaModele.Expertes.ObjetExpert
            Private WsIndex As Integer
            Private WsPointdeVue As Integer
            Private WsObjet As Integer
            Private WsNumero As Integer
            Private WsIntitule As String
            Private WsNature As Integer
            Private WsFormat As Integer
            Private WsCategorieDLL As Integer
            Private WsAlgorithme As Integer
            Private WsFormeduResultat As Integer
            Private WsExploitation As Integer
            Private WsInfoReference(2) As Integer
            Private WsZoneEdition(10) As Integer
            Private WsDescription As String

            Public ReadOnly Property VParent() As Virtualia.Systeme.MetaModele.Expertes.ObjetExpert
                Get
                    Return ObjetVirtualia
                End Get
            End Property

            Public Property VIndex() As Integer
                Get
                    Return WsIndex
                End Get
                Set(ByVal value As Integer)
                    WsIndex = value
                End Set
            End Property

            Public Property PointdeVue() As Integer
                Get
                    Return WsPointdeVue
                End Get
                Set(ByVal value As Integer)
                    WsPointdeVue = value
                End Set
            End Property

            Public Property Objet() As Integer
                Get
                    Return WsObjet
                End Get
                Set(ByVal value As Integer)
                    WsObjet = value
                End Set
            End Property

            Public Property Numero() As Integer
                Get
                    Return WsNumero
                End Get
                Set(ByVal value As Integer)
                    WsNumero = value
                End Set
            End Property

            Public Property Intitule() As String
                Get
                    Return WsIntitule
                End Get
                Set(ByVal value As String)
                    WsIntitule = value
                End Set
            End Property

            Public Property VNature() As Integer
                Get
                    Return WsNature
                End Get
                Set(ByVal value As Integer)
                    WsNature = value
                End Set
            End Property

            Public Property Format() As Integer
                Get
                    Return WsFormat
                End Get
                Set(ByVal value As Integer)
                    WsFormat = value
                End Set
            End Property

            Public Property CategorieDLL() As Integer
                Get
                    Return WsCategorieDLL
                End Get
                Set(ByVal value As Integer)
                    WsCategorieDLL = value
                End Set
            End Property

            Public Property Algorithme() As Integer
                Get
                    Return WsAlgorithme
                End Get
                Set(ByVal value As Integer)
                    WsAlgorithme = value
                End Set
            End Property

            Public Property FormeduResultat() As Integer
                Get
                    Return WsFormeduResultat
                End Get
                Set(ByVal value As Integer)
                    WsFormeduResultat = value
                End Set
            End Property

            Public Property Exploitation() As Integer
                Get
                    Return WsExploitation
                End Get
                Set(ByVal value As Integer)
                    WsExploitation = value
                End Set
            End Property

            Public Property InfoReference(ByVal Index As Integer) As Integer
                Get
                    Select Case Index
                        Case 0 To WsInfoReference.Count - 1
                            Return WsInfoReference(Index)
                        Case Else
                            Return -1
                    End Select
                End Get
                Set(ByVal value As Integer)
                    Select Case Index
                        Case 0 To WsInfoReference.Count - 1
                            WsInfoReference(Index) = value
                    End Select
                End Set
            End Property

            Public Property ZoneEdition(ByVal Index As Integer) As Integer
                Get
                    Select Case Index
                        Case 0 To WsZoneEdition.Count - 1
                            Return WsZoneEdition(Index)
                        Case Else
                            Return -1
                    End Select
                End Get
                Set(ByVal value As Integer)
                    Select Case Index
                        Case 0 To WsZoneEdition.Count - 1
                            WsZoneEdition(Index) = value
                    End Select
                End Set
            End Property

            Public Property Description() As String
                Get
                    Return WsDescription
                End Get
                Set(ByVal value As String)
                    WsDescription = value
                End Set
            End Property

            Public Sub New(ByVal host As Virtualia.Systeme.MetaModele.Expertes.ObjetExpert)
                ObjetVirtualia = host
            End Sub
        End Class
    End Namespace
End Namespace