﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace MetaModele
    Namespace Predicats
        Public Class PredicateFiche
            Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates
            Private WsPvue As Integer = 1
            Private WsIdentifiant As Integer
            Private WsNumObjet As Integer
            Private WsDateDebut As String
            Private WsDateFin As String
            Private WsLstObjets As List(Of Integer)
            '
            Public Function SiIdentifiantOK(ByVal Fiche As VIR_FICHE) As Boolean
                Return Fiche.Ide_Dossier = WsIdentifiant
            End Function

            Public Function SiFicheEvtDansPeriode(ByVal Fiche As VIR_FICHE) As Boolean
                If WsRhDates Is Nothing Then
                    WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates
                End If

                If Not (Fiche.PointdeVue = WsPvue And Fiche.NumeroObjet = WsNumObjet) Then
                    Return False
                End If

                Select Case WsRhDates.ComparerDates(Fiche.Date_de_Valeur, WsDateDebut)
                    Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                        Select Case WsRhDates.ComparerDates(Fiche.Date_de_Fin, WsDateFin)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                Return True
                        End Select
                    Case Else
                        Select Case WsRhDates.ComparerDates(Fiche.Date_de_Fin, WsDateDebut)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                                Return True
                        End Select
                End Select

                Return False
            End Function

            Public Function SiFichePerenneDansPeriode(ByVal Fiche As VIR_FICHE) As Boolean
                If WsRhDates Is Nothing Then
                    WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates
                End If

                If Fiche.PointdeVue = WsPvue And Fiche.NumeroObjet = WsNumObjet Then
                    Select Case WsRhDates.ComparerDates(Fiche.Date_de_Valeur, WsDateDebut)
                        Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                            Select Case WsRhDates.ComparerDates(Fiche.Date_de_Valeur, WsDateFin)
                                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                    Return True
                            End Select
                    End Select
                End If
                Return False
            End Function

            Public Function SiFicheDansListeObjets(ByVal Fiche As VIR_FICHE) As Boolean

                For Each it In WsLstObjets
                    If Fiche.PointdeVue = WsPvue Then
                        Return Fiche.NumeroObjet = Convert.ToInt16(it)
                    End If
                Next

                Return False
            End Function

            Public Function SiPointdeVueOK(ByVal Fiche As VIR_FICHE) As Boolean
                Return Fiche.PointdeVue = WsPvue
            End Function

            Public Sub New(ByVal Ide As Integer)
                MyBase.New()

                WsPvue = 0
                WsNumObjet = 0
                WsIdentifiant = Ide
                WsDateDebut = ""
                WsDateFin = ""
            End Sub

            Public Sub New(ByVal PtdeVue As Integer, Optional NoObjet As Integer = 0)
                MyBase.New()

                WsPvue = PtdeVue
                WsNumObjet = NoObjet
                WsIdentifiant = 0
                WsDateDebut = ""
                WsDateFin = ""
            End Sub

            Public Sub New(ByVal PtdeVue As Integer, ByVal LstObjets As List(Of Integer))
                MyBase.New()

                WsPvue = PtdeVue

                If LstObjets Is Nothing Then
                    WsLstObjets = New List(Of Integer)
                    Return
                End If

                WsLstObjets = LstObjets
            End Sub

            Public Sub New(ByVal LstObjets As List(Of Integer))
                MyBase.New()

                If LstObjets Is Nothing Then
                    WsLstObjets = New List(Of Integer)
                    Return
                End If

                WsLstObjets = LstObjets
            End Sub

            Public Sub New(ByVal PtdeVue As Integer, ByVal NumObjet As Integer, ByVal DateDebut As String, ByVal DateFin As String)
                MyBase.New()

                WsPvue = PtdeVue
                WsNumObjet = NumObjet
                WsIdentifiant = 0
                WsDateDebut = DateDebut
                WsDateFin = DateFin
            End Sub

            Public Sub New(ByVal NumObjet As Integer, ByVal DateDebut As String, ByVal DateFin As String)
                MyBase.New()

                WsNumObjet = NumObjet
                WsIdentifiant = 0
                WsDateDebut = DateDebut
                WsDateFin = DateFin
            End Sub

        End Class
    End Namespace
End Namespace
