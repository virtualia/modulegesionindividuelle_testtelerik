﻿Option Strict On
Option Explicit On
Option Compare Text
Namespace Fonctions
    Public Class CodesEBCDIC
        Private WsListeEBCDIC As List(Of CodeEBCDIC) = New List(Of CodeEBCDIC)

        Public Function Chiffre(ByVal Valeur As String) As String
            If IsNumeric(Valeur) Then
                Return Valeur
            End If
            Dim Codage As CodeEBCDIC = (From it As CodeEBCDIC In WsListeEBCDIC Where it.Code = Valeur Select it).FirstOrDefault()
            If Codage Is Nothing Then
                Return ""
            End If
            Return "" & Codage.Valeur
        End Function

        Public Function Signe(ByVal Valeur As String) As String
            If IsNumeric(Valeur) Then
                Return "+"
            End If
            Dim Codage As CodeEBCDIC = (From it As CodeEBCDIC In WsListeEBCDIC Where it.Code = Valeur Select it).FirstOrDefault()
            If Codage Is Nothing Then
                Return "+"
            End If
            Return Codage.Signe
        End Function

        Public Sub New()
            WsListeEBCDIC.Add(New CodeEBCDIC("{", 0, "+"))
            WsListeEBCDIC.Add(New CodeEBCDIC("}", 0, "-"))
            WsListeEBCDIC.Add(New CodeEBCDIC("A", 1, "+"))
            WsListeEBCDIC.Add(New CodeEBCDIC("B", 2, "+"))
            WsListeEBCDIC.Add(New CodeEBCDIC("C", 3, "+"))
            WsListeEBCDIC.Add(New CodeEBCDIC("D", 4, "+"))
            WsListeEBCDIC.Add(New CodeEBCDIC("E", 5, "+"))
            WsListeEBCDIC.Add(New CodeEBCDIC("F", 6, "+"))
            WsListeEBCDIC.Add(New CodeEBCDIC("G", 7, "+"))
            WsListeEBCDIC.Add(New CodeEBCDIC("H", 8, "+"))
            WsListeEBCDIC.Add(New CodeEBCDIC("I", 9, "+"))
            WsListeEBCDIC.Add(New CodeEBCDIC("J", 1, "-"))
            WsListeEBCDIC.Add(New CodeEBCDIC("K", 2, "-"))
            WsListeEBCDIC.Add(New CodeEBCDIC("L", 3, "-"))
            WsListeEBCDIC.Add(New CodeEBCDIC("M", 4, "-"))
            WsListeEBCDIC.Add(New CodeEBCDIC("N", 5, "-"))
            WsListeEBCDIC.Add(New CodeEBCDIC("O", 6, "-"))
            WsListeEBCDIC.Add(New CodeEBCDIC("P", 7, "-"))
            WsListeEBCDIC.Add(New CodeEBCDIC("Q", 8, "-"))
            WsListeEBCDIC.Add(New CodeEBCDIC("R", 9, "-"))
            WsListeEBCDIC.Add(New CodeEBCDIC("é", 0, "+"))
            WsListeEBCDIC.Add(New CodeEBCDIC("è", 0, "-"))
        End Sub

    End Class

    Public Class CodeEBCDIC
        Private WsCode As String = ""
        Private Wsvaleur As Integer
        Private WsSigne As String = ""

        Public ReadOnly Property Code As String
            Get
                Return WsCode
            End Get
        End Property

        Public ReadOnly Property Valeur As Integer
            Get
                Return Wsvaleur
            End Get
        End Property

        Public ReadOnly Property Signe As String
            Get
                Return WsSigne
            End Get
        End Property

        Public Sub New(ByVal CodageEBCDIC As String, ByVal ValeurNumerique As Integer, ByVal Operateur As String)
            WsCode = CodageEBCDIC
            Wsvaleur = ValeurNumerique
            WsSigne = Operateur
        End Sub
    End Class
End Namespace