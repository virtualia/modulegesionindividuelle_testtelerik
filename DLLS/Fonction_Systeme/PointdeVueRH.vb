Option Strict On
Option Explicit On
Option Compare Text
Namespace MetaModele
    Namespace Donnees
        Public Class PointdeVue
            Inherits List(Of MetaModele.Donnees.Objet)
            Private ReadOnly WsModeleVirtualia As MetaModele.Donnees.ModeleRH
            Private WsIndex As Integer
            Private WsNumero As Integer
            Private WsIntitule As String

            Public Property VIndex() As Integer
                Get
                    Return WsIndex
                End Get
                Set(ByVal value As Integer)
                    WsIndex = value
                End Set
            End Property

            Public ReadOnly Property VParent() As MetaModele.Donnees.ModeleRH
                Get
                    Return WsModeleVirtualia
                End Get
            End Property

            Public Property Numero() As Integer
                Get
                    Return WsNumero
                End Get
                Set(ByVal value As Integer)
                    WsNumero = value
                End Set
            End Property

            Public Property Intitule() As String
                Get
                    Return WsIntitule
                End Get
                Set(ByVal value As String)
                    WsIntitule = value
                End Set
            End Property

            Public ReadOnly Property NombredObjets() As Integer
                Get
                    Return Count
                End Get
            End Property

            Public Overloads ReadOnly Property Item(ByVal Index As Integer) As MetaModele.Donnees.Objet
                Get
                    If (Index < 0 Or Index >= Count) Then
                        Return Nothing
                    End If

                    Return Me(Index)
                End Get
            End Property

            Public Function ItemObjetParUneClef(ByVal Reference As String, ByVal Clef As String) As Objet
                Dim Predicat As Func(Of MetaModele.Donnees.Objet, Boolean) = Nothing

                Select Case Reference
                    Case "Interne"
                        Predicat = Function(it As MetaModele.Donnees.Objet)
                                       Return it.Intitule = Clef
                                   End Function
                    Case "Sql"
                        Predicat = Function(it As MetaModele.Donnees.Objet)
                                       Return it.NomTableSGBDR = Clef
                                   End Function
                End Select

                Return (From it As MetaModele.Donnees.Objet In Me Where Predicat(it) Select it).FirstOrDefault()

            End Function

            Public Function IndexObjet(ByVal Numero As Integer) As Integer
                Dim Retour As MetaModele.Donnees.Objet
                Retour = (From it As MetaModele.Donnees.Objet In Me Where it.Numero = Numero Select it).FirstOrDefault()

                If Retour Is Nothing Then
                    Return 0
                End If
                Return IndexOf(Retour)
            End Function

            Public Function CreerUnObjet() As Integer
                Dim ObjetModele As MetaModele.Donnees.Objet = New MetaModele.Donnees.Objet(Me)
                Me.Add(ObjetModele)
                ObjetModele.PointdeVue = Numero
                ObjetModele.VIndex = Count - 1
                Return Count - 1

            End Function

            Public Sub New(ByVal host As ModeleRH)
                WsModeleVirtualia = host
            End Sub

            Public Sub New()
                MyBase.New()
            End Sub
        End Class
    End Namespace
End Namespace
