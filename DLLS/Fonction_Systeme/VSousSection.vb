﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Parametrage
    Namespace Produit
        Public Class VSousSection
            Private WsIndex As Integer
            Private WsNomSection As String
            Private WsNomSousSection As String
            Private WsChaineClef As System.Text.StringBuilder
            Private WsChaineValeur As System.Text.StringBuilder
            Private WsClef(0) As String
            Private WsValeur(0) As String
            '
            Public ReadOnly Property NombredeClefs() As Integer
                Get
                    Return WsClef.Count - 1
                End Get
            End Property

            Public ReadOnly Property CompteClefs() As Integer
                Get
                    WsClef = Strings.Split(WsChaineClef.ToString, VI.Tild, -1)
                    WsValeur = Strings.Split(WsChaineValeur.ToString, VI.Tild, -1)
                    WsChaineClef = Nothing
                    WsChaineValeur = Nothing
                    Return WsClef.Count - 1
                End Get
            End Property

            Public Property VIndex() As Integer
                Get
                    Return WsIndex
                End Get
                Set(ByVal value As Integer)
                    WsIndex = value
                End Set
            End Property

            Public Property Categorie() As String
                Get
                    Return WsNomSection
                End Get
                Set(ByVal value As String)
                    WsNomSection = value
                End Set
            End Property

            Public Property Nom() As String
                Get
                    Return WsNomSousSection
                End Get
                Set(ByVal value As String)
                    WsNomSousSection = value
                End Set
            End Property

            Public ReadOnly Property Clef(ByVal Index As Integer) As String
                Get
                    Select Case Index
                        Case 0 To WsClef.Count - 1
                            Return WsClef(Index)
                        Case Else
                            Return ""
                    End Select
                End Get
            End Property

            Public ReadOnly Property Valeur(ByVal Index As Integer) As String
                Get
                    Select Case Index
                        Case 0 To WsValeur.Count - 1
                            Return WsValeur(Index)
                        Case Else
                            Return ""
                    End Select
                End Get
            End Property

            Public WriteOnly Property ElementCompile(ByVal Balise As String) As String
                Set(ByVal value As String)
                    Select Case Balise
                        Case Is = "NomSection"
                            Categorie = value
                        Case Is = "NomSousSection"
                            Nom = value
                        Case Is = "Clef"
                            WsChaineClef.Append(value & VI.Tild)
                        Case Is = "Valeur"
                            WsChaineValeur.Append(value & VI.Tild)
                    End Select
                End Set
            End Property

            Public Sub New()
                MyBase.New()
                WsChaineClef = New System.Text.StringBuilder
                WsChaineValeur = New System.Text.StringBuilder
            End Sub
        End Class
    End Namespace
End Namespace