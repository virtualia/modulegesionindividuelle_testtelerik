﻿Option Explicit On
Option Strict On
Option Compare Text
Namespace MetaModele
    Namespace Outils
        'TODO : C'est une classe statique
        Public Class FenetreVirtuelle

            Public Function Recalcul_DatesFin() As List(Of DictionnaireVirtuel)
                Dim InfoBase As DictionnaireVirtuel
                Dim WsListeDicoVirtuel As List(Of DictionnaireVirtuel) = New List(Of DictionnaireVirtuel)()

                InfoBase = New DictionnaireVirtuel
                InfoBase.Etiquette = "Sur l'objet"
                InfoBase.Longueur = 80
                InfoBase.NatureDonnee = "Table"
                InfoBase.FormatDonnee = ""
                InfoBase.IdentifiantTable = 99
                InfoBase.NomTable = "Objet"
                WsListeDicoVirtuel.Add(InfoBase)

                Return WsListeDicoVirtuel

            End Function

            Public Function Epuration_Historique() As List(Of DictionnaireVirtuel)

                Dim InfoBase As DictionnaireVirtuel

                Dim WsListeDicoVirtuel As List(Of DictionnaireVirtuel) = New List(Of DictionnaireVirtuel)()

                InfoBase = New DictionnaireVirtuel()
                InfoBase.Etiquette = "Depuis le"
                InfoBase.Longueur = 10
                InfoBase.NatureDonnee = "Date"
                InfoBase.FormatDonnee = ""
                InfoBase.IdentifiantTable = 0
                InfoBase.NomTable = ""
                WsListeDicoVirtuel.Add(InfoBase)

                InfoBase = New DictionnaireVirtuel()
                InfoBase.Etiquette = "Jusqu'au"
                InfoBase.Longueur = 10
                InfoBase.NatureDonnee = "Date"
                InfoBase.FormatDonnee = ""
                InfoBase.IdentifiantTable = 0
                InfoBase.NomTable = ""
                WsListeDicoVirtuel.Add(InfoBase)

                InfoBase = New DictionnaireVirtuel()
                InfoBase.Etiquette = "Sur l'objet"
                InfoBase.Longueur = 80
                InfoBase.NatureDonnee = "Table"
                InfoBase.FormatDonnee = ""
                InfoBase.IdentifiantTable = 99
                InfoBase.NomTable = "Objet"
                WsListeDicoVirtuel.Add(InfoBase)

                InfoBase = New DictionnaireVirtuel()
                InfoBase.Etiquette = "Uniquement pour la valeur"
                InfoBase.Longueur = 120
                InfoBase.NatureDonnee = "Alpha"
                InfoBase.FormatDonnee = ""
                InfoBase.IdentifiantTable = 0
                InfoBase.NomTable = ""
                WsListeDicoVirtuel.Add(InfoBase)

                Return WsListeDicoVirtuel

            End Function

            Public Function Substitution_Valeurs() As List(Of DictionnaireVirtuel)

                Dim InfoBase As DictionnaireVirtuel

                Dim WsListeDicoVirtuel As List(Of DictionnaireVirtuel) = New List(Of DictionnaireVirtuel)()

                InfoBase = New DictionnaireVirtuel()
                InfoBase.Etiquette = "Sur l'objet"
                InfoBase.Longueur = 80
                InfoBase.NatureDonnee = "Table"
                InfoBase.FormatDonnee = ""
                InfoBase.IdentifiantTable = 99
                InfoBase.NomTable = "Objet"
                WsListeDicoVirtuel.Add(InfoBase)

                InfoBase = New DictionnaireVirtuel()
                InfoBase.Etiquette = "Pour l'information"
                InfoBase.Longueur = 80
                InfoBase.NatureDonnee = "Table"
                InfoBase.FormatDonnee = ""
                InfoBase.IdentifiantTable = 99
                InfoBase.NomTable = "Information"
                WsListeDicoVirtuel.Add(InfoBase)

                InfoBase = New DictionnaireVirtuel()
                InfoBase.Etiquette = "Remplacer la valeur"
                InfoBase.Longueur = 120
                InfoBase.NatureDonnee = "Alpha"
                InfoBase.FormatDonnee = ""
                InfoBase.IdentifiantTable = 0
                InfoBase.NomTable = ""
                WsListeDicoVirtuel.Add(InfoBase)

                InfoBase = New DictionnaireVirtuel()
                InfoBase.Etiquette = "Par la valeur"
                InfoBase.Longueur = 120
                InfoBase.NatureDonnee = "Alpha"
                InfoBase.FormatDonnee = ""
                InfoBase.IdentifiantTable = 0
                InfoBase.NomTable = ""
                WsListeDicoVirtuel.Add(InfoBase)

                Return WsListeDicoVirtuel

            End Function

        End Class
    End Namespace
End Namespace