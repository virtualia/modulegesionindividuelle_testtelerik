Option Strict On
Option Explicit On
Option Compare Text
Imports System.Collections.Generic

Namespace MetaModele
    Namespace Expertes
        Public Class PointdevueExpert
            Inherits List(Of MetaModele.Expertes.ObjetExpert)

            Private ReadOnly WsExpertes As MetaModele.Expertes.ExpertesRH
            Private WsIndex As Integer
            Private WsNumero As Integer
            Private WsIntitule As String

            Public Property VIndex() As Integer
                Get
                    Return WsIndex
                End Get
                Set(ByVal value As Integer)
                    WsIndex = value
                End Set
            End Property

            Public Property Numero() As Integer
                Get
                    Return WsNumero
                End Get
                Set(ByVal value As Integer)
                    WsNumero = value
                End Set
            End Property

            Public Property Intitule() As String
                Get
                    Return WsIntitule
                End Get
                Set(ByVal value As String)
                    WsIntitule = value
                End Set
            End Property

            Public ReadOnly Property NombredObjets() As Integer
                Get
                    Return Count
                End Get
            End Property

            Public ReadOnly Property IndexObjet(ByVal Numero As Integer) As Integer
                Get
                    Dim ItemObjet As MetaModele.Expertes.ObjetExpert
                    ItemObjet = (From obj As MetaModele.Expertes.ObjetExpert In Me Where obj.Numero = Numero Select obj).FirstOrDefault()
                    If ItemObjet Is Nothing Then
                        Return 0
                    End If
                    Return IndexOf(ItemObjet)
                End Get
            End Property

            Public Overloads ReadOnly Property Item(ByVal Index As Integer) As MetaModele.Expertes.ObjetExpert
                Get
                    If Index < 0 Or Index > Count - 1 Then
                        Return Nothing
                    End If
                    Return Me(Index)
                End Get
            End Property

            Public Sub New(ByVal host As MetaModele.Expertes.ExpertesRH)
                WsExpertes = host
            End Sub

            Public Sub New()
                MyBase.New()
            End Sub

            Public Function CreerUnObjet() As Integer
                Dim ObjetModele As MetaModele.Expertes.ObjetExpert = New MetaModele.Expertes.ObjetExpert(Me)

                Me.Add(ObjetModele)
                ObjetModele.PointdeVue = Numero
                ObjetModele.VIndex = Count - 1

                Return Count - 1
            End Function

        End Class
    End Namespace
End Namespace