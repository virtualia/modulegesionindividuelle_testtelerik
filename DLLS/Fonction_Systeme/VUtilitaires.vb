﻿Option Strict On
Option Explicit On
Option Compare Text
Imports System.IO

Namespace Outils
    Public Class Utilitaires
        Private Const Doublequote As String = """"

        Public Sub SourceHtmlEditee(ByVal NomFichier As String, ByVal ChaineSource As String)
            Dim ChaineEditee As System.Text.StringBuilder

            ChaineEditee = New System.Text.StringBuilder
            '** Formulaire générique
            ChaineEditee.Append("<!DOCTYPE html PUBLIC " & Doublequote & "-//W3C//DTD XHTML 1.0 Transitional//EN" & Doublequote)
            ChaineEditee.Append(Strings.Space(1) & Doublequote & "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" & Doublequote & ">")
            ChaineEditee.Append(vbCrLf)
            ChaineEditee.Append("<html xmlns=" & Doublequote & "http://www.w3.org/1999/xhtml" & Doublequote & ">")
            ChaineEditee.Append(vbCrLf)
            ChaineEditee.Append("<head id=" & Doublequote & "HeadVirtu" & Doublequote & ">" & vbCrLf)
            ChaineEditee.Append("<title>Virtualia.Net</title>" & vbCrLf)
            ChaineEditee.Append("</head>" & vbCrLf)
            ChaineEditee.Append("<body>" & vbCrLf)
            ChaineEditee.Append("<form id=" & Doublequote & "VirtualiaEdition" & Doublequote & ">" & vbCrLf)
            ChaineEditee.Append("<div>" & vbCrLf)
            '*****
            ChaineEditee.Append(ChaineSource)
            '**
            ChaineEditee.Append("</div>" & vbCrLf)
            ChaineEditee.Append("</form>" & vbCrLf)
            ChaineEditee.Append("</body>" & vbCrLf)
            ChaineEditee.Append("</html>" & vbCrLf)
            '**
            Try
                My.Computer.FileSystem.WriteAllText(NomFichier, ChaineEditee.ToString, False, System.Text.Encoding.UTF8)
            Catch ex As Exception
                Exit Sub
            End Try
        End Sub

        Public Sub TransformeFicHtml(ByVal NomFichier As String)
            Dim CodeUnicode As System.Text.Encoding = System.Text.Encoding.Unicode
            Dim FLuxLecture As FileStream
            Dim FicReader As StreamReader
            Dim FLuxEcriture As FileStream
            Dim FicWriter As StreamWriter
            Dim Champlu As String
            Dim NomBak As String = Strings.Left(NomFichier, NomFichier.Length - 3) & "Bak"
            Dim TableauData(0) As String
            Dim SiOK As Boolean
            Dim SiNePasFaire As Boolean

            My.Computer.FileSystem.CopyFile(NomFichier, NomBak, True)

            FLuxLecture = New FileStream(NomBak, IO.FileMode.Open, IO.FileAccess.Read)
            FicReader = New StreamReader(FLuxLecture, CodeUnicode)

            FLuxEcriture = New FileStream(NomFichier, IO.FileMode.Create, IO.FileAccess.Write)
            FicWriter = New StreamWriter(FLuxEcriture, CodeUnicode)

            SiNePasFaire = False
            Champlu = FicReader.ReadLine

            Do Until FicReader.EndOfStream
                SiOK = True
                Select Case Strings.Left(Champlu.Trim, 11)
                    Case "<form metho"
                        TableauData = Strings.Split(Champlu.Trim, "Id=", -1)
                        Champlu = "<form id=" & TableauData(TableauData.Count - 1)
                    Case "<div class="
                        If Champlu.Trim().Contains("aspNetHidden") Then
                            SiOK = False
                            SiNePasFaire = True
                        End If
                    Case Is = "<input type"
                        If Champlu.Trim().Contains("image") Then
                            If Champlu.Trim().Contains("CommandePDF") Then
                                SiOK = False
                            End If
                        End If
                        If Champlu.Trim().Contains("hidden") Then
                            SiOK = False
                        End If
                    Case Else
                        If SiNePasFaire Then
                            If Strings.Left(Champlu.Trim, 6) = "</div>" Then
                                SiNePasFaire = False
                            Else
                                SiOK = False
                            End If
                        End If
                End Select
                If SiOK Then
                    FicWriter.WriteLine(Champlu)
                End If
                Champlu = FicReader.ReadLine
            Loop
            FicWriter.WriteLine(Champlu.Trim)
            FicWriter.Flush()
            FicWriter.Close()
            FicReader.Close()
            My.Computer.FileSystem.DeleteFile(NomBak)

            Champlu = ReplaceTextArea(My.Computer.FileSystem.ReadAllText(NomFichier, CodeUnicode))
            My.Computer.FileSystem.WriteAllText(NomFichier, Champlu, False)

        End Sub

        Public Sub TransformeFicHtml(ByVal NomFichier As String, ByVal IDTable As String, ByVal NbBalises As Integer)
            Dim CodeUnicode As System.Text.Encoding = System.Text.Encoding.Unicode
            Dim FLuxLecture As FileStream
            Dim FicReader As StreamReader
            Dim FLuxEcriture As FileStream
            Dim FicWriter As StreamWriter
            Dim Champlu As String
            Dim NomBak As String = Strings.Left(NomFichier, NomFichier.Length - 3) & "Bak"
            Dim SiOK As Boolean
            Dim Cpt As Integer = 0

            My.Computer.FileSystem.CopyFile(NomFichier, NomBak, True)

            FLuxLecture = New FileStream(NomBak, IO.FileMode.Open, IO.FileAccess.Read)
            FicReader = New StreamReader(FLuxLecture, CodeUnicode)

            FLuxEcriture = New FileStream(NomFichier, IO.FileMode.Create, IO.FileAccess.Write)
            FicWriter = New StreamWriter(FLuxEcriture, CodeUnicode)

            '** Formulaire générique
            Champlu = "<!DOCTYPE html PUBLIC " & Doublequote & "-//W3C//DTD XHTML 1.0 Transitional//EN" & Doublequote
            Champlu &= Strings.Space(1) & Doublequote & "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" & Doublequote & ">"
            FicWriter.WriteLine(Champlu)
            Champlu = "<html xmlns=" & Doublequote & "http://www.w3.org/1999/xhtml" & Doublequote & ">"
            FicWriter.WriteLine(Champlu)
            Champlu = "<head id=" & Doublequote & "HeadVirtu" & Doublequote & ">"
            FicWriter.WriteLine(Champlu)
            FicWriter.WriteLine("<title>Virtualia.Net</title>")
            FicWriter.WriteLine("</head>")
            FicWriter.WriteLine("<body>")
            Champlu = "<form id=" & Doublequote & "VirtualiaEdition" & Doublequote & ">"
            FicWriter.WriteLine(Champlu)
            FicWriter.WriteLine("<div>")
            '*****

            SiOK = False
            Champlu = FicReader.ReadLine
            Do Until FicReader.EndOfStream
                If Champlu.Contains("<table id=") Then
                    If Champlu.Contains(IDTable) Then
                        SiOK = True
                    End If
                ElseIf Champlu.Contains(IDTable) Then
                    If Strings.Left(Champlu.Trim, 10) = "<table id=" Or Strings.Left(Champlu.Trim, 14) = "<td><table id=" Then
                        SiOK = True
                    End If
                End If
                If SiOK Then
                    If Strings.Left(Champlu.Trim, 8) = "</table>" Then
                        Cpt += 1
                    End If
                End If
                If SiOK Then
                    FicWriter.WriteLine(Champlu)
                End If
                If Cpt = NbBalises Then
                    Exit Do
                End If
                Champlu = FicReader.ReadLine
            Loop
            '**
            FicWriter.WriteLine("</div>")
            FicWriter.WriteLine("</form>")
            FicWriter.WriteLine("</body>")
            FicWriter.WriteLine("</html>")
            '**
            FicWriter.Flush()
            FicWriter.Close()
            FicReader.Close()
            My.Computer.FileSystem.DeleteFile(NomBak)

            Champlu = ReplaceTextArea(My.Computer.FileSystem.ReadAllText(NomFichier, CodeUnicode))
            My.Computer.FileSystem.WriteAllText(NomFichier, Champlu, False)

        End Sub

        Private Function ReplaceTextArea(ByVal ChaineText As String) As String
            Dim TableauData(0) As String
            Dim IndiceI As Integer
            Dim Hauteur As Integer
            Dim PositionFinDefinition As Integer
            Dim PositionFinData As Integer
            Dim PositionHeight As Integer
            Dim Positionpx As Integer
            Dim ChaineNew As System.Text.StringBuilder
            Dim ChaineTravail As String
            Dim ChaineData As String
            Dim ChaineHeight As String

            TableauData = Strings.Split(ChaineText, "<textarea", -1)
            If TableauData.Count = 1 Then
                Return ChaineText
            End If
            For IndiceI = 1 To TableauData.Count - 1
                Hauteur = 100
                PositionHeight = Strings.InStr(TableauData(IndiceI), "height:", CompareMethod.Text)
                If PositionHeight > 0 Then
                    Positionpx = Strings.InStr(PositionHeight, TableauData(IndiceI), "px", CompareMethod.Text)
                    If Positionpx > 0 Then
                        ChaineHeight = Strings.Mid(TableauData(IndiceI), PositionHeight + 7, Positionpx - PositionHeight - 7)
                        If IsNumeric(ChaineHeight) Then
                            Hauteur = CInt(ChaineHeight)
                        End If
                    End If
                End If

                PositionFinDefinition = Strings.InStr(TableauData(IndiceI), ">", CompareMethod.Text)

                ChaineTravail = Strings.Left(TableauData(IndiceI), PositionFinDefinition - 1)
                ChaineTravail &= "><tr><td valign=" & Doublequote & "top" & Doublequote & " height=" & Doublequote & Hauteur.ToString & Doublequote & ">"

                PositionFinData = Strings.InStr(TableauData(IndiceI), "</", CompareMethod.Text)
                ChaineData = Strings.Mid(TableauData(IndiceI), PositionFinDefinition + 2, PositionFinData - PositionFinDefinition - 1)
                If ChaineData.Contains(vbCrLf) Then
                    ChaineTravail &= ChaineData.Replace(vbCrLf, "<br/>")
                ElseIf ChaineTravail.Contains(vbCr) Then
                    ChaineTravail &= ChaineData.Replace(vbCr, "<br/>")
                ElseIf ChaineTravail.Contains(vbLf) Then
                    ChaineTravail &= ChaineData.Replace(vbLf, "<br/>")
                Else
                    ChaineTravail &= ChaineData
                End If
                ChaineTravail &= Strings.Mid(TableauData(IndiceI), PositionFinData + 1)

                TableauData(IndiceI) = ChaineTravail
            Next IndiceI

            ChaineNew = New System.Text.StringBuilder

            For IndiceI = 0 To TableauData.Count - 2
                ChaineNew.Append(TableauData(IndiceI) & "<table")
            Next IndiceI

            ChaineNew.Append(TableauData(TableauData.Count - 1))

            Return Strings.Replace(ChaineNew.ToString, "</textarea>", "</td></tr></table>", 1)

        End Function

    End Class
End Namespace

