Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.Fonctions
Imports System.Text

Namespace Parametrage
    Namespace BasesdeDonnees
        Public Class ParamSgbd
            Inherits List(Of Parametrage.BasesdeDonnees.ParamSgbdDatabase)
            Private Const SysFicParam As String = "VirtualiaSgbd"
            Private WsRepertoire As String

            Public ReadOnly Property NomCompletFichierXml() As String
                Get
                    Return WsRepertoire & SysFicParam & ".Xml"
                End Get
            End Property

            Public ReadOnly Property RepertoireduModele() As String
                Get
                    Return WsRepertoire
                End Get
            End Property

            Public ReadOnly Property NombredeDatabases() As Integer
                Get
                    Return Me.Count
                End Get
            End Property

            Public Overloads ReadOnly Property Item(ByVal Index As Integer) As Parametrage.BasesdeDonnees.ParamSgbdDatabase
                Get
                    If (Index < 0 Or Index >= Count) Then
                        Return Nothing
                    End If
                    Return Me(Index)
                End Get
            End Property

            Public Sub New(ByVal LstDatabases As List(Of Parametrage.BasesdeDonnees.ParamSgbdDatabase))
                If LstDatabases Is Nothing Then
                    Exit Sub
                End If
                Dim IndiceK As Integer
                Dim ISgbd As Integer

                For IndiceK = 0 To LstDatabases.Count - 1
                    ISgbd = CreerUneDatabase()
                    Me(ISgbd) = LstDatabases(IndiceK)
                    Me(ISgbd).VIndex = ISgbd
                Next IndiceK
            End Sub

            Public Sub New()
                Dim FicXML As FichierXML
                Dim LireXML As LectureXML
                Dim Element As String
                Dim ICnx As Integer
                Dim ISgbd As Integer
                Dim BaliseDatabase As String = "VirtualiaSgbd~Database"
                Dim BaliseConnexion As String = "VirtualiaSgbd~Database~Connexion"
                Dim ItemDatabase As ParamSgbdDatabase = Nothing
                Dim ItemConnexion As ParamSgbdConnexion = Nothing

                WsRepertoire = VI.DossierVirtualiaService("Parametrage")

                FicXML = New FichierXML(WsRepertoire & SysFicParam & ".Xsd")
                FicXML.FichierXML() = WsRepertoire & SysFicParam & ".Xml"

                LireXML = New LectureXML(FicXML)
                LireXML.SelectData(BaliseDatabase, "Numero")

                Do Until LireXML.EndOfData
                    Element = LireXML.GetData()
                    ISgbd = CreerUneDatabase()
                    ItemDatabase = Me(ISgbd)

                    ItemDatabase.Numero = CInt(Val(Element))
                    ItemDatabase.Intitule = LireXML.GetDataVoisin(BaliseDatabase, "Intitule", "After")
                    ItemDatabase.Nom = LireXML.GetDataVoisin(BaliseDatabase, "Alias", "Before")
                    ItemDatabase.TypeduSgbdAlpha = LireXML.GetDataVoisin(BaliseDatabase, "TypeSgbd", "After")
                    ItemDatabase.FormatdesDatesAlpha = LireXML.GetDataVoisin(BaliseDatabase, "FormatdesDates", "After")
                    ItemDatabase.SymboleDecimal = LireXML.GetDataVoisin(BaliseDatabase, "SymboleDecimal", "After")
                    ItemDatabase.Schema = LireXML.GetDataVoisin(BaliseDatabase, "Schema", "After")
                    Element = LireXML.GetDataVoisin(BaliseDatabase, "SiTraceSql", "After")
                    If Element <> "" Then
                        ItemDatabase.SiTraceSql = CInt(Val(Element))
                    End If

                    Element = LireXML.GetDataVoisin(BaliseConnexion, "DsnODBC", "After")
                    ICnx = ItemDatabase.CreerUneConnexion
                    ItemConnexion = ItemDatabase(ICnx)

                    ItemConnexion.ElementCompile("DsnODBC") = Element
                    Element = LireXML.GetDataVoisin(BaliseConnexion, "Provider", "After")
                    ItemConnexion.ElementCompile("Provider") = Element
                    Element = LireXML.GetDataVoisin(BaliseConnexion, "Precision", "After")
                    ItemConnexion.ElementCompile("Precision") = Element
                    Element = LireXML.GetDataVoisin(BaliseConnexion, "Serveur", "After")
                    ItemConnexion.ElementCompile("Serveur") = Element
                    Element = LireXML.GetDataVoisin(BaliseConnexion, "Utilisateur", "After")
                    ItemConnexion.ElementCompile("Utilisateur") = Element
                    Element = LireXML.GetDataVoisin(BaliseConnexion, "Password", "After")
                    ItemConnexion.ElementCompile("Password") = Element
                    Element = LireXML.GetDataVoisin(BaliseConnexion, "TypeCnx", "After")
                    ItemConnexion.ElementCompile("TypeCnx") = Element

                    Element = ""
                    LireXML.FindNextData()
                Loop

            End Sub

            Public Function CreerUneDatabase() As Integer
                Dim ObjetDatabase As ParamSgbdDatabase = New ParamSgbdDatabase(Me)
                Me.Add(ObjetDatabase)
                ObjetDatabase.VIndex = Count - 1
                Return Count - 1
            End Function

            Public Sub EcrireFichierXML()
                Dim CodeIso As Encoding = Encoding.UTF8
                Dim FicStream As System.IO.FileStream
                Dim FicWriter As System.IO.StreamWriter
                Dim NomFichier As String

                NomFichier = WsRepertoire & SysFicParam & ".Xml"

                If My.Computer.FileSystem.FileExists(NomFichier) Then
                    My.Computer.FileSystem.CopyFile(NomFichier, WsRepertoire & Left(SysFicParam, SysFicParam.Length - 3) & "bak", True)
                    My.Computer.FileSystem.DeleteFile(NomFichier)
                End If

                FicStream = New System.IO.FileStream(NomFichier, IO.FileMode.Create, IO.FileAccess.Write)
                FicWriter = New System.IO.StreamWriter(FicStream, CodeIso)

                FicWriter.WriteLine("<?xml version=" & VI.DoubleQuote & "1.0" & VI.DoubleQuote & " encoding=" & VI.DoubleQuote & "utf-8" & VI.DoubleQuote & " ?>")
                FicWriter.WriteLine("<VirtualiaSgbd xmlns:xsi=" & VI.DoubleQuote & "http://www.w3.org/2001/XMLSchema-instance" & VI.DoubleQuote & " xsi:noNamespaceSchemaLocation=" & VI.DoubleQuote & "VirtualiaSgbd.xsd" & VI.DoubleQuote & ">")

                For Each ItemDatabase As ParamSgbdDatabase In Me
                    FicWriter.WriteLine("<Database Numero=" & VI.DoubleQuote & ItemDatabase.Numero.ToString.Trim & VI.DoubleQuote & " Alias=" & VI.DoubleQuote & ItemDatabase.Nom.Trim & VI.DoubleQuote & " >")
                    FicWriter.WriteLine("<Intitule>" & ItemDatabase.Intitule.Trim & "</Intitule>")
                    FicWriter.WriteLine("<TypeSgbd>" & ItemDatabase.TypeduSgbdAlpha.Trim & "</TypeSgbd>")
                    FicWriter.WriteLine("<FormatdesDates>" & ItemDatabase.FormatdesDatesAlpha.Trim & "</FormatdesDates>")
                    FicWriter.WriteLine("<SymboleDecimal>" & ItemDatabase.SymboleDecimal.Trim & "</SymboleDecimal>")
                    FicWriter.WriteLine("<Schema>" & ItemDatabase.Schema.Trim & "</Schema>")
                    FicWriter.WriteLine("<SiTraceSql>" & ItemDatabase.SiTraceSql.ToString.Trim & "</SiTraceSql>")

                    For Each ItemConnexion As ParamSgbdConnexion In ItemDatabase
                        FicWriter.WriteLine("<Connexion TypeCnx=" & VI.DoubleQuote & ItemConnexion.TypeCnx.Trim & VI.DoubleQuote & ">")
                        FicWriter.WriteLine("<DsnODBC>" & ItemConnexion.DsnODBC.Trim & "</DsnODBC>")
                        FicWriter.WriteLine("<Provider>" & ItemConnexion.Serveur.Trim & "</Provider>")
                        FicWriter.WriteLine("<Precision>" & ItemConnexion.Serveur.Trim & "</Precision>")
                        FicWriter.WriteLine("<Serveur>" & ItemConnexion.Serveur.Trim & "</Serveur>")
                        FicWriter.WriteLine("<Utilisateur>" & ItemConnexion.Utilisateur.Trim & "</Utilisateur>")
                        FicWriter.WriteLine("<Password>" & ItemConnexion.PasswordStocke.Trim & "</Password>")
                        FicWriter.WriteLine("</Connexion>")
                    Next
                    FicWriter.WriteLine("</Database>")
                Next

                FicWriter.WriteLine("</VirtualiaSgbd>")
                FicWriter.Flush()
                FicWriter.Close()
            End Sub

        End Class
    End Namespace
End Namespace
