Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Fonctions
    ''' <summary>
    ''' Classe de chargement des fichiers XML
    ''' </summary>
    Public Class FichierXML
        Private Structure StructureXML
            Dim Localisation As String
            Dim Balise As String
            Dim Nature As String
        End Structure

        Private Structure Structure_Attribut
            Dim Nom_Attribut As String
            Dim Valeur_Attribut As String
        End Structure

        Private Const Cst_Element As String = "Element"
        Private Const Cst_Attribut As String = "Attribut"
        Private Const Cst_Espace As String = " "

        Private Const Cst_Symbole_SigneInferieur As String = "&lt;"
        Private Const Cst_Symbole_SigneSuperieur As String = "&gt;"
        Private Const Cst_Symbole_Quote As String = "&apos;"
        Private Const Cst_Symbole_DoubleQuote As String = "&quot;"
        Private Const Cst_Symbole_EtCommercial As String = "&amp;"

        Private disposedValue As Boolean = False        ' Pour d�tecter les appels redondants
        Private WsNomFicXSD As String
        Private WsNomFicXML As String
        Private WsLignesFichierXSD(0) As String
        Private WsLignesFichierXML(0) As String

        Private WsStructureXSD(0) As StructureXML
        Private WsStructure_Attribut(0) As Structure_Attribut
        Private WsLigneDataNew(0, 0) As String 'Sur deuxi�me dimension => O = Key Structure , 1 Valeur
        Private WsIndexDataNew As Integer

        Private WsIndexDataCourant As Integer
        Private WsIndexDataPrecedent As Integer
        Private WsIndexDataSuivant As Integer

        Private WsIndexCourantStructure As Integer

        Private WsCodeIso As System.Text.Encoding = System.Text.Encoding.UTF8

        Public Property CodeIsoUtilise As System.Text.Encoding
            Get
                Return WsCodeIso
            End Get
            Set(value As System.Text.Encoding)
                WsCodeIso = value
            End Set
        End Property

        ''' <summary>
        ''' Propri�t� Balise d'un Index donn�
        ''' </summary>
        ''' <returns>Renvoi la Balise</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property StructureXSD_Balise(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To WsStructureXSD.Count - 1
                        Return WsStructureXSD(Index).Balise
                End Select
                Return ""
            End Get
        End Property

        ''' <summary>
        ''' Propri�t� Localisation d'un Index donn�
        ''' </summary>
        ''' <returns>Renvoi la Localisation</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property StructureXSD_Localisation(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To WsStructureXSD.Count - 1
                        Return WsStructureXSD(Index).Localisation
                End Select
                Return ""
            End Get
        End Property

        ''' <summary>
        ''' Propri�t� Nombre de lignes du tableau WsLigneDataNew
        ''' </summary>
        ''' <returns>Renvoi le Nombre de lignes</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property NombredeLignesData() As Integer
            Get
                Return WsIndexDataNew
            End Get
        End Property

        ''' <summary>
        ''' Fonction Index correspondant � une Balise et une Localisation
        ''' </summary>
        ''' <returns>Renvoi l'Index correspondant � une Balise et une Localisation</returns>
        ''' <remarks></remarks>
        Public Function GetIndexCourantStructure(ByVal SLocalisation As String, ByVal SNomBalise As String, ByVal IPointeurCourantStructure As Integer) As Integer
            Dim i As Integer

            'Premi�re recherche avec pointeur IPointeurCourantStructure (Optimisation)
            For i = IPointeurCourantStructure To WsStructureXSD.Count - 1
                If Not (WsStructureXSD(i).Localisation Is Nothing) Then
                    If WsStructureXSD(i).Localisation = SLocalisation Then
                        If WsStructureXSD(i).Balise = SNomBalise Then
                            Return i
                        End If
                    End If
                End If
            Next i
            'Deuxi�me recherche sans Optimisation
            For i = 0 To WsStructureXSD.Count - 1
                If Not (WsStructureXSD(i).Localisation Is Nothing) Then
                    If WsStructureXSD(i).Localisation = SLocalisation Then
                        If WsStructureXSD(i).Balise = SNomBalise Then
                            Return i
                        End If
                    End If
                End If
            Next i
            Return -1
        End Function

        ''' <summary>
        ''' Fonction Valeur dans le tableau WsLigneDataNew
        ''' </summary>
        ''' <returns>Renvoi la valeur dans le tableau WsLigneDataNew � l'index demand�</returns>
        ''' <remarks></remarks>
        Public Function GetData(ByVal IndexDataCourant As Integer) As String
            Dim Chaine As String = ""
            If IndexDataCourant >= 0 Then
                Chaine = WsLigneDataNew(IndexDataCourant, 1)
                If Chaine <> "" Then
                    Chaine = Strings.Replace(Chaine, Cst_Symbole_SigneInferieur, "<", 1)
                    Chaine = Strings.Replace(Chaine, Cst_Symbole_SigneSuperieur, ">", 1)
                    Chaine = Strings.Replace(Chaine, Cst_Symbole_Quote, "'", 1)
                    Chaine = Strings.Replace(Chaine, Cst_Symbole_DoubleQuote, """", 1)
                    Chaine = Strings.Replace(Chaine, Cst_Symbole_EtCommercial, "&", 1)
                End If
            End If
            Return Chaine
        End Function

        ''' <summary>
        ''' Propri�t� Valeur ou Index dans la structure dans le tableau WsLigneDataNew
        ''' </summary>
        ''' <returns>Renvoi la valeur ou l'index du tableau WsLigneDataNew(pour j = 0 => l'index, pour j = 1 => la valeur)</returns>
        ''' <remarks></remarks>
        Public ReadOnly Property GetTableauDataInfo(ByVal i As Integer, ByVal j As Integer) As String
            Get
                Dim SValeur As String
                SValeur = WsLigneDataNew(i, j)
                If SValeur <> "" And j = 1 Then
                    SValeur = Strings.Replace(SValeur, Cst_Symbole_SigneInferieur, "<", 1)
                    SValeur = Strings.Replace(SValeur, Cst_Symbole_SigneSuperieur, ">", 1)
                    SValeur = Strings.Replace(SValeur, Cst_Symbole_Quote, "'", 1)
                    SValeur = Strings.Replace(SValeur, Cst_Symbole_DoubleQuote, """", 1)
                    SValeur = Strings.Replace(SValeur, Cst_Symbole_EtCommercial, "&", 1)
                End If
                Return SValeur
            End Get
        End Property

        ''' <summary>
        ''' Propri�t� FichierXSD
        ''' </summary>
        ''' <value>Assigne le Fichier XSD au tableau WsStructureXSD</value>
        ''' <remarks></remarks>
        Private WriteOnly Property FichierXSD() As String
            Set(ByVal value As String)
                Dim ChaineLectureFichier As System.Text.StringBuilder
                Dim FicStream As System.IO.FileStream
                Dim FicReader As System.IO.StreamReader
                Dim Champlu As String
                Dim ChampW As String
                Dim NomElement As String
                Dim Pos1 As Integer
                Dim INbLignesFichierXSD As Integer
                Dim IndiceCourantFichier As Integer
                Dim IndiceCourantStructureXSD As Integer
                Dim INiveauArbo As Integer
                Dim SCheminCourant As String

                INbLignesFichierXSD = 0
                NomElement = ""
                ChaineLectureFichier = New System.Text.StringBuilder

                'Chargement du fichier XSD dans le tableau WsLignesFichierXSD
                FicStream = New System.IO.FileStream(value, IO.FileMode.Open, IO.FileAccess.Read)
                FicReader = New System.IO.StreamReader(FicStream, WsCodeIso)
                Do Until FicReader.EndOfStream
                    Champlu = FicReader.ReadLine
                    ChampW = Champlu.Trim
                    Select Case Strings.Left(ChampW, 1)
                        Case Is <> "<"
                            Pos1 = Strings.InStr(Champlu, "<")
                            Select Case Pos1
                                Case Is > 0
                                    ChampW = Strings.Right(Champlu, Champlu.Length - Pos1 - 1)
                            End Select
                    End Select
                    Select Case ChampW.Length
                        Case Is > 12
                            Select Case Strings.Left(ChampW, 12)
                                Case Is = "<xsd:complex", "</xsd:comple", "<xsd:element", "</xsd:elemen", "<xsd:attribu", "<xsd:simpleT", "</xsd:schema"
                                    INbLignesFichierXSD += 1
                                    ChaineLectureFichier.Append(ChampW & VI.SigneBarre)
                            End Select
                    End Select
                Loop
                FicReader.Close()

                ChaineLectureFichier.Replace(VI.SigneBarre, "", ChaineLectureFichier.Length - 1, 1)
                WsLignesFichierXSD = Strings.Split(ChaineLectureFichier.ToString, VI.SigneBarre, -1)

                If INbLignesFichierXSD > 0 Then

                    ReDim WsStructureXSD(300)
                    IndiceCourantFichier = 0
                    IndiceCourantStructureXSD = -1
                    SCheminCourant = ""
                    INiveauArbo = 0
                    IndiceCourantFichier = LectureFichierXSD(IndiceCourantFichier, INiveauArbo, IndiceCourantStructureXSD, SCheminCourant)
                End If

            End Set
        End Property

        ''' <summary>
        ''' Fonction LectureFichierXSD R�cursive
        ''' </summary>
        ''' <returns>Renvoi l'index courant dans le fichier XSD</returns>
        ''' <remarks></remarks>
        Private Function LectureFichierXSD(ByVal IndiceCourantFichier As Integer, ByVal INiveauArbo As Integer, ByRef IndiceCourantStructureXSD As Integer, ByVal SCheminCourant As String) As Integer
            Dim ChampW As String
            Dim SChemin As String
            Dim NomElement As String
            Dim SBaliseCourante As String
            Dim SBalisePrecedente As String
            Dim NomBalise As String
            Dim Pos1 As Integer
            Dim Pos2 As Integer
            Dim INiveauArboCourant As Integer

            INiveauArboCourant = INiveauArbo
            LectureFichierXSD = IndiceCourantFichier

            NomElement = ""
            SBalisePrecedente = ""
            SBaliseCourante = ""
            SChemin = ""

            Do While IndiceCourantFichier <= WsLignesFichierXSD.Count - 1
                ChampW = WsLignesFichierXSD(IndiceCourantFichier)
                Select Case ChampW.Length
                    Case Is > 12
                        SBaliseCourante = Strings.Left(ChampW, 12)
                        Select Case SBaliseCourante
                            Case Is = "<xsd:element"
                                Pos1 = Strings.InStr(ChampW, "name=")
                                Select Case Pos1
                                    Case Is > 0
                                        Select Case Right(ChampW, 2)
                                            Case Is = "/>"
                                                ChampW = Mid(ChampW, Pos1 + 6, ChampW.Length - Pos1 - 8)
                                                Pos2 = Strings.InStr(ChampW, " Type")
                                                Select Case Pos2
                                                    Case Is > 0
                                                        IndiceCourantStructureXSD += 1
                                                        NomElement = Left(ChampW, Pos2 - 1)

                                                        WsStructureXSD(IndiceCourantStructureXSD).Localisation = SChemin
                                                        WsStructureXSD(IndiceCourantStructureXSD).Balise = ExtraireBalise(NomElement)
                                                        WsStructureXSD(IndiceCourantStructureXSD).Nature = Cst_Element

                                                End Select
                                            Case Else
                                                If INiveauArboCourant > INiveauArbo Then
                                                    '***************  Attention Proc�dure R�cursive  *******************
                                                    IndiceCourantFichier = LectureFichierXSD(IndiceCourantFichier, INiveauArboCourant, IndiceCourantStructureXSD, SChemin)
                                                Else
                                                    INiveauArboCourant += 1

                                                    NomElement = Mid(ChampW, Pos1 + 6, ChampW.Length - Pos1 - 7)

                                                    NomBalise = ExtraireBalise(NomElement)
                                                    If SCheminCourant <> "" Then
                                                        SChemin = SCheminCourant & VI.Tild & NomBalise
                                                    Else
                                                        SChemin = NomBalise
                                                    End If
                                                End If
                                        End Select
                                End Select
                            Case Is = "</xsd:elemen"
                                INiveauArboCourant += -1

                                If INiveauArboCourant = INiveauArbo And INiveauArbo > 0 Then
                                    Exit Do
                                End If

                            Case Is = "<xsd:simpleT"
                                If SBalisePrecedente = "<xsd:element" Then
                                    IndiceCourantStructureXSD += 1
                                    WsStructureXSD(IndiceCourantStructureXSD).Localisation = SCheminCourant
                                    WsStructureXSD(IndiceCourantStructureXSD).Balise = ExtraireBalise(NomElement)
                                    WsStructureXSD(IndiceCourantStructureXSD).Nature = Cst_Element
                                End If

                            Case Is = "<xsd:attribu"
                                Pos1 = Strings.InStr(ChampW, "name=")
                                Select Case Pos1
                                    Case Is > 0
                                        Select Case Right(ChampW, 2)
                                            Case Is = "/>"
                                                ChampW = Mid(ChampW, Pos1 + 6, ChampW.Length - Pos1 - 8)
                                                Pos2 = Strings.InStr(ChampW, " Type")
                                                Select Case Pos2
                                                    Case Is > 0
                                                        IndiceCourantStructureXSD += 1
                                                        NomElement = Left(ChampW, Pos2 - 1)

                                                        WsStructureXSD(IndiceCourantStructureXSD).Localisation = SChemin
                                                        WsStructureXSD(IndiceCourantStructureXSD).Balise = ExtraireBalise(NomElement)
                                                        WsStructureXSD(IndiceCourantStructureXSD).Nature = Cst_Attribut
                                                End Select
                                            Case Else
                                                IndiceCourantStructureXSD += 1
                                                NomElement = Mid(ChampW, Pos1 + 6, ChampW.Length - Pos1 - 7)
                                                WsStructureXSD(IndiceCourantStructureXSD).Localisation = SChemin
                                                WsStructureXSD(IndiceCourantStructureXSD).Balise = ExtraireBalise(NomElement)
                                                WsStructureXSD(IndiceCourantStructureXSD).Nature = Cst_Attribut
                                        End Select
                                End Select
                            Case Is = "</xsd:schema"
                                Exit Function
                            Case Else
                                NomElement = ""
                        End Select
                End Select
                IndiceCourantFichier += 1
                SBalisePrecedente = SBaliseCourante
            Loop

            Return IndiceCourantFichier

        End Function

        ''' <summary>
        ''' Propri�t� FichierXML
        ''' </summary>
        ''' <value>Assigne le Fichier XML au tableau WsLigneDataNew</value>
        ''' <returns>Renvoi le nom du Fichier XML</returns>
        ''' <remarks></remarks>
        Public Property FichierXML() As String
            Get
                Return WsNomFicXML
            End Get

            Set(ByVal value As String)
                Dim ChaineLectureFichier As System.Text.StringBuilder
                Dim FicStream As System.IO.FileStream
                Dim FicReader As System.IO.StreamReader
                Dim Champlu As String
                Dim ChampW As String
                Dim NbMaxi As Integer
                Dim INbCourant As Integer
                Dim INbLignesFichierXML As Integer
                Dim IndiceCourantFichier As Integer
                Dim IndiceCourantStructureXSD As Integer
                Dim INiveauArbo As Integer
                Dim SCheminCourant As String
                Dim SiOK As Boolean

                INbCourant = 1
                WsNomFicXML = value
                Select Case WsNomFicXSD
                    Case Is = ""
                        FichierXSD = Strings.Left(WsNomFicXML, WsNomFicXML.Length - 3) & "Xsd"
                    Case Else
                        FichierXSD = WsNomFicXSD
                End Select

                '*************************************************************
                'Chargement du fichier XML dans le tableau WsLignesFichierXML
                '*************************************************************

                'D�compte du nombre de lignes � charger
                INbLignesFichierXML = 0
                FicStream = New System.IO.FileStream(WsNomFicXML, IO.FileMode.Open, IO.FileAccess.Read)
                FicReader = New System.IO.StreamReader(FicStream, WsCodeIso)
                SiOK = True
                ChampW = ""
                Do Until FicReader.EndOfStream
                    Champlu = FicReader.ReadLine
                    If INbLignesFichierXML < 5 Then
                        If Strings.Right(Champlu.Trim, 1) <> ">" Then
                            If SiOK = True Then
                                ChampW = Champlu.Trim
                            Else
                                ChampW &= Strings.Space(1) & Champlu.Trim
                            End If
                            SiOK = False
                        Else
                            If SiOK = False Then
                                ChampW &= Strings.Space(1) & Champlu.Trim
                            Else
                                ChampW = Champlu.Trim
                            End If
                            SiOK = True
                        End If
                        If InStr(1, ChampW, "<?xml") = 0 And SiOK = True Then
                            INbLignesFichierXML += 1
                        End If
                    Else
                        ChampW = Champlu.Trim
                        INbLignesFichierXML += 1
                    End If
                Loop
                FicReader.Close()

                If INbLignesFichierXML = 1 Then
                    'Traitement sp�cifique pour fichiers DGCL CDF
                    INbLignesFichierXML = 0
                    ChaineLectureFichier = New System.Text.StringBuilder
                    FicStream = New System.IO.FileStream(WsNomFicXML, IO.FileMode.Open, IO.FileAccess.Read)
                    FicReader = New System.IO.StreamReader(FicStream, WsCodeIso)
                    Do Until FicReader.EndOfStream
                        Champlu = FicReader.ReadLine
                        ChampW = Champlu.Trim
                        If InStr(1, ChampW, "<?xml") = 0 Then
                            ChaineLectureFichier.Append(ChampW & VI.SigneBarre)
                            INbLignesFichierXML += 1
                        End If
                    Loop
                    FicReader.Close()

                    ChaineLectureFichier.Replace(VI.SigneBarre, "", ChaineLectureFichier.Length - 1, 1)

                    ChaineLectureFichier.Replace(">", ">" & VI.SigneBarre)
                    WsLignesFichierXML = Strings.Split(ChaineLectureFichier.ToString, VI.SigneBarre, -1)
                Else
                    SiOK = True
                    ChampW = ""
                    ReDim WsLignesFichierXML(INbLignesFichierXML)
                    INbLignesFichierXML = 0
                    FicStream = New System.IO.FileStream(WsNomFicXML, IO.FileMode.Open, IO.FileAccess.Read)
                    FicReader = New System.IO.StreamReader(FicStream, WsCodeIso)
                    Do Until FicReader.EndOfStream
                        Champlu = FicReader.ReadLine
                        If INbLignesFichierXML < 5 Then
                            If Strings.Right(Champlu.Trim, 1) <> ">" Then
                                If SiOK = True Then
                                    ChampW = Champlu.Trim
                                Else
                                    ChampW &= Strings.Space(1) & Champlu.Trim
                                End If
                                SiOK = False
                            Else
                                If SiOK = False Then
                                    ChampW &= Strings.Space(1) & Champlu.Trim
                                Else
                                    ChampW = Champlu.Trim
                                End If
                                SiOK = True
                            End If
                            If InStr(1, ChampW, "<?xml") = 0 And SiOK = True Then
                                WsLignesFichierXML(INbLignesFichierXML) = ChampW
                                INbLignesFichierXML += 1
                            End If
                        Else
                            ChampW = Champlu.Trim
                            WsLignesFichierXML(INbLignesFichierXML) = ChampW
                            INbLignesFichierXML += 1
                        End If
                    Loop
                    FicReader.Close()

                End If

                If INbLignesFichierXML > 0 Then

                    WsIndexDataNew = -1

                    NbMaxi = NbEnrXml(WsNomFicXML)
                    ReDim WsLigneDataNew(NbMaxi, 1)

                    IndiceCourantFichier = 0
                    IndiceCourantStructureXSD = 0
                    SCheminCourant = ""
                    INiveauArbo = 0
                    IndiceCourantFichier = LectureFichierXML(IndiceCourantFichier, INiveauArbo, IndiceCourantStructureXSD, SCheminCourant)

                End If

            End Set
        End Property

        ''' <summary>
        ''' Fonction LectureFichierXML R�cursive
        ''' </summary>
        ''' <returns>Renvoi l'index courant dans le fichier XML</returns>
        ''' <remarks></remarks>
        Private Function LectureFichierXML(ByVal IndiceCourantFichier As Integer, ByVal INiveauArbo As Integer, ByRef IndiceCourantStructureXSD As Integer, ByVal SCheminCourant As String) As Integer
            Dim ChampW As String
            Dim IndexCourant As Integer
            Dim Pos As Integer
            Dim NomElement As String
            Dim SLocalisation As String
            Dim NbAttributs As Integer
            Dim SBaliseCourante As String
            Dim i As Integer
            Dim BFind_Element As Boolean
            Dim INiveauArboCourant As Integer
            Dim SChemin As String
            Dim SPremiereBalise As String
            Dim SContenuBalise As String
            Dim SDeuxi�meBalise As String
            Dim BBaliseFermante As Boolean
            Dim IndexOpti As Integer

            INiveauArboCourant = INiveauArbo
            IndexCourant = 0
            IndexOpti = 0
            Do While IndiceCourantFichier <= WsLignesFichierXML.Count - 1
                ChampW = WsLignesFichierXML(IndiceCourantFichier)
                Select Case Strings.Left(ChampW, 1)
                    Case Is = VI.SigneInferieur '<  (en 1�re position)
                        Select Case Strings.Mid(ChampW, 2, 1)
                            Case Is = VI.Slash '/  (Slash en 2i�me position) Fermeture balise sans valeur
                                'Quitter la proc�dure
                                LectureFichierXML = IndiceCourantFichier
                                Exit Function
                            Case Is <> VI.Slash
                                Pos = Strings.InStr(ChampW, VI.SigneSuperieur)
                                Select Case Pos
                                    Case Is > 0 '> (dans la chaine)
                                        'Traitement 1�re Balise
                                        BBaliseFermante = False
                                        SPremiereBalise = Strings.Mid(ChampW, 2, Pos - 2)
                                        If Strings.Mid(SPremiereBalise, Len(SPremiereBalise), 1) = VI.Slash Then
                                            SPremiereBalise = Left(SPremiereBalise, Len(SPremiereBalise) - 1)
                                            BBaliseFermante = True
                                            'Attention balise fermante
                                        End If
                                        SBaliseCourante = ""
                                        NbAttributs = ChaineDecodee(SPremiereBalise, SBaliseCourante)
                                        NomElement = SBaliseCourante
                                        If NbAttributs > 0 Then
                                            If SCheminCourant <> "" Then
                                                SLocalisation = SCheminCourant & VI.Tild & NomElement
                                            Else
                                                SLocalisation = NomElement
                                            End If
                                            For i = 0 To WsStructure_Attribut.Count - 1
                                                BFind_Element = IndexBalise(IndexOpti, IndexCourant, SLocalisation, WsStructure_Attribut(i).Nom_Attribut, Cst_Attribut)
                                                If BFind_Element Then
                                                    IndexOpti = IndexCourant
                                                    WsIndexDataNew += 1
                                                    WsLigneDataNew(WsIndexDataNew, 0) = IndexCourant.ToString
                                                    WsLigneDataNew(WsIndexDataNew, 1) = WsStructure_Attribut(i).Valeur_Attribut
                                                End If
                                            Next i
                                            If Not BBaliseFermante Then
                                                'Balise ouvrante seule
                                                If SCheminCourant <> "" Then
                                                    SChemin = SCheminCourant & VI.Tild & NomElement
                                                Else
                                                    SChemin = NomElement
                                                End If
                                                IndiceCourantFichier += 1
                                                IndiceCourantFichier = LectureFichierXML(IndiceCourantFichier, INiveauArboCourant, IndiceCourantStructureXSD, SChemin)
                                            End If
                                        Else

                                            If Len(ChampW) > Pos + 1 Then
                                                Dim Pos_Balise_Closed As Integer
                                                SContenuBalise = Mid(ChampW, Pos + 1, Len(ChampW) - Pos)
                                                '   </
                                                Pos_Balise_Closed = Strings.InStr(SContenuBalise, VI.SigneInferieur & VI.Slash)
                                                If Pos_Balise_Closed = 0 Then
                                                    Do While Pos_Balise_Closed = 0 And IndiceCourantFichier + 1 <= WsLignesFichierXML.Count - 1
                                                        IndiceCourantFichier += 1
                                                        ChampW = WsLignesFichierXML(IndiceCourantFichier)
                                                        SContenuBalise = SContenuBalise & ChampW
                                                        Pos_Balise_Closed = Strings.InStr(SContenuBalise, VI.SigneInferieur & VI.Slash)
                                                    Loop
                                                End If

                                                If Pos_Balise_Closed = 1 Then
                                                    SDeuxi�meBalise = SContenuBalise
                                                    SContenuBalise = ""
                                                Else
                                                    SDeuxi�meBalise = Mid(SContenuBalise, Pos_Balise_Closed, Len(SContenuBalise) - Pos_Balise_Closed + 1)
                                                    SContenuBalise = Left(SContenuBalise, Pos_Balise_Closed - 1)
                                                End If

                                                BFind_Element = IndexBalise(IndexOpti, IndexCourant, SCheminCourant, NomElement, Cst_Element)
                                                If BFind_Element Then
                                                    IndexOpti = IndexCourant
                                                    WsIndexDataNew += 1
                                                    WsLigneDataNew(WsIndexDataNew, 0) = IndexCourant.ToString
                                                    WsLigneDataNew(WsIndexDataNew, 1) = SContenuBalise
                                                End If

                                            Else
                                                If Not BBaliseFermante Then
                                                    'Balise ouvrante seule
                                                    If SCheminCourant <> "" Then
                                                        SChemin = SCheminCourant & VI.Tild & NomElement
                                                    Else
                                                        SChemin = NomElement
                                                    End If

                                                    IndiceCourantFichier += 1
                                                    IndiceCourantFichier = LectureFichierXML(IndiceCourantFichier, INiveauArboCourant, IndiceCourantStructureXSD, SChemin)
                                                End If
                                            End If
                                        End If

                                End Select
                        End Select
                End Select
                IndiceCourantFichier += 1
            Loop
            Return IndiceCourantFichier

        End Function

        ''' <summary>
        ''' Propri�t� NbEnrXml
        ''' </summary>
        ''' <returns>Renvoi le nombre de lignes du fichier XML</returns>
        ''' <remarks></remarks>
        Private ReadOnly Property NbEnrXml(ByVal Valeur As String) As Integer
            Get
                Return WsLignesFichierXML.Count
            End Get
        End Property

        ''' <summary>
        ''' Fonction IndexBalise
        ''' </summary>
        ''' <returns>Renvoi l'index dans WsStructureXSD pour une balise, une localisation et un type balise donn�s</returns>
        ''' <remarks></remarks>
        Private Function IndexBalise(ByVal IndexDepart As Integer, ByRef Index As Integer, ByVal Localisation As String, ByVal NomBalise As String, ByVal TypeBalise As String) As Boolean
            Dim i As Integer

            'Optimisation
            For i = IndexDepart To WsStructureXSD.Count - 1
                If Not (WsStructureXSD(i).Localisation Is Nothing) Then
                    If WsStructureXSD(i).Localisation = Localisation Then
                        If WsStructureXSD(i).Balise = NomBalise Then
                            If WsStructureXSD(i).Nature = TypeBalise Then
                                Index = i
                                Return True
                                Exit Function
                            End If
                        End If
                    End If
                End If
            Next i

            For i = 0 To WsStructureXSD.Count - 1
                If Not (WsStructureXSD(i).Localisation Is Nothing) Then
                    If WsStructureXSD(i).Localisation = Localisation Then
                        If WsStructureXSD(i).Balise = NomBalise Then
                            If WsStructureXSD(i).Nature = TypeBalise Then
                                Index = i
                                Return True
                                Exit Function
                            End If
                        End If
                    End If
                End If
            Next i
            Return False
        End Function

        ''' <summary>
        ''' Fonction ExtraireBalise
        ''' </summary>
        ''' <returns>Renvoi la balise contenue dans Valeur</returns>
        ''' <remarks></remarks>
        Private Function ExtraireBalise(ByVal Valeur As String) As String
            Dim Chaine As String
            Dim Pos As Integer
            Chaine = Valeur.Replace(VI.DoubleQuote, "")
            Pos = Strings.InStr(Chaine, Strings.Space(1))
            Select Case Pos
                Case Is = 0
                    Return Chaine
                Case Else
                    Return Strings.Left(Chaine, Pos - 1)
            End Select
        End Function

        ''' <summary>
        ''' Fonction ChaineDecodee (Si Attributs renseigne le tableau WsStructure_Attribut)
        ''' </summary>
        ''' <returns>Renvoi le nombre d'attributs</returns>
        ''' <remarks></remarks>
        Private Function ChaineDecodee(ByVal SValeur As String, ByRef SBalise As String) As Integer
            Dim TableauData(0) As String
            Dim Res As Integer
            Dim Pos As Integer
            Dim Pos_SigneEgal As Integer
            Dim Indice As Integer
            Dim Pos_DebValue As Integer
            Dim SCaractereDelimiteur As String
            Dim SChaineEgal As String
            Dim Pos_Attribut As Integer

            ReDim WsStructure_Attribut(0)
            Indice = -1
            Res = 0

            SChaineEgal = ""
            SValeur = SValeur.Trim
            Pos = Strings.InStr(SValeur, Cst_Espace)
            Select Case Pos
                Case Is = 0
                    SBalise = SValeur
                    Return Res
                    Exit Function
                Case Is > 1
                    SBalise = Left(SValeur, Pos - 1)
                    SValeur = Strings.Mid(SValeur, Pos + 1, Strings.Len(SValeur) - Pos)

                    Pos_SigneEgal = Strings.InStr(1, SValeur, VI.SigneEgal)
                    Select Case Strings.Mid(SValeur, Pos_SigneEgal + 1, 1)
                        Case VI.DoubleQuote
                            SCaractereDelimiteur = VI.DoubleQuote
                            SChaineEgal = VI.SigneEgal & SCaractereDelimiteur
                        Case VI.Quote
                            SCaractereDelimiteur = VI.Quote
                            SChaineEgal = VI.SigneEgal & SCaractereDelimiteur
                        Case Cst_Espace
                            Select Case Strings.Mid(SValeur, Pos_SigneEgal + 2, 1)
                                Case VI.DoubleQuote
                                    SCaractereDelimiteur = VI.DoubleQuote
                                    SChaineEgal = VI.SigneEgal & Cst_Espace & SCaractereDelimiteur
                                Case VI.Quote
                                    SCaractereDelimiteur = VI.Quote
                                    SChaineEgal = VI.SigneEgal & Cst_Espace & SCaractereDelimiteur
                                Case Else
                                    SCaractereDelimiteur = ""
                                    SChaineEgal = VI.SigneEgal & Cst_Espace
                            End Select
                        Case Else
                            SCaractereDelimiteur = ""
                            SChaineEgal = VI.SigneEgal
                    End Select

                    Do While SValeur <> ""
                        'Recherche des attributs

                        Select Case Strings.Mid(SValeur, Strings.Len(SValeur), 1)
                            Case VI.DoubleQuote
                                SCaractereDelimiteur = VI.DoubleQuote
                            Case VI.Quote
                                SCaractereDelimiteur = VI.Quote
                            Case Else
                                SCaractereDelimiteur = ""
                        End Select

                        Pos_SigneEgal = Strings.InStrRev(SValeur, SChaineEgal)
                        Select Case Pos_SigneEgal
                            Case Is > 1
                                If Strings.Mid(SValeur, Pos_SigneEgal - 1, 1) = Cst_Espace Then
                                    Pos_DebValue = Pos_SigneEgal + Len(SChaineEgal)
                                    Pos_Attribut = Strings.InStrRev(SValeur, Cst_Espace, Pos_SigneEgal - 2)
                                Else
                                    Pos_DebValue = Pos_SigneEgal + Len(SChaineEgal)
                                    Pos_Attribut = Strings.InStrRev(SValeur, Cst_Espace, Pos_SigneEgal - 1)
                                End If

                                Pos_Attribut += 1
                                Res += 1

                                Indice += 1
                                If Indice > WsStructure_Attribut.Count - 1 Then
                                    ReDim Preserve WsStructure_Attribut(WsStructure_Attribut.Count + 1)
                                End If

                                WsStructure_Attribut(Indice).Nom_Attribut = Strings.Mid(SValeur, Pos_Attribut, Pos_SigneEgal - Pos_Attribut).TrimEnd
                                WsStructure_Attribut(Indice).Valeur_Attribut = Strings.Mid(SValeur, Pos_DebValue, Len(SValeur) - Len(SCaractereDelimiteur) - Pos_DebValue + 1)
                                If Pos_Attribut > 1 Then
                                    SValeur = Strings.Left(SValeur, Pos_Attribut - 2)
                                Else
                                    SValeur = ""
                                End If
                            Case Else
                                SValeur = ""
                        End Select
                    Loop
            End Select
            Return Res
        End Function

        Public Sub New(ByVal NomFichierXSD As String)
            WsNomFicXSD = NomFichierXSD
        End Sub

        Public Sub New(ByVal NomFichierXSD As String, ByVal CodeIso As System.Text.Encoding)
            WsNomFicXSD = NomFichierXSD
            WsCodeIso = CodeIso
        End Sub
    End Class

    ''' <summary>
    ''' Classe de lecture des fichiers XML
    ''' </summary>
    Public Class LectureXML
        Private InstanceFichierXML As Virtualia.Systeme.Fonctions.FichierXML
        Private WsIndexDataCourant As Integer
        Private WsIndexDataPrecedent As Integer
        Private WsIndexDataSuivant As Integer
        Private WsIndexCourantStructure As Integer

        'Optimisation
        Private WsIndexDebCourant As Integer
        Private WsPointeurCourantStructure As Integer

        Private WsPointeurDeb As Integer
        Private WsPointeurFin As Integer

        ''' <summary>
        ''' Cr�ation d'une instance de la classe LectureXML
        ''' </summary>
        ''' <param name="InstFichierXML">Instance de la classe FichierXML</param>
        ''' <remarks></remarks>
        ''' 
        Public Sub New(ByVal InstFichierXML As Virtualia.Systeme.Fonctions.FichierXML)
            InstanceFichierXML = InstFichierXML

            WsPointeurDeb = -1

            'Optimisation
            WsIndexDebCourant = -1
            WsPointeurCourantStructure = 0

            WsPointeurFin = InstanceFichierXML.NombredeLignesData

            WsIndexDataCourant = WsPointeurDeb
        End Sub

        Public ReadOnly Property PointeurDebut() As Integer
            Get
                Return WsIndexDataCourant
            End Get
        End Property

        Public ReadOnly Property PointeurFin() As Integer
            Get
                If WsIndexDataSuivant <= WsIndexDataCourant Then
                    WsIndexDataSuivant = FindNextDataLocal(WsIndexCourantStructure)
                    If WsIndexDataSuivant = -1 Then
                        Return WsPointeurFin
                    End If
                    Return WsIndexDataSuivant
                End If
                Return WsIndexDataSuivant
            End Get
        End Property

        Public Sub RattacherAuPere(ByVal PointeurDeb As Integer, ByVal PointeurFin As Integer)
            WsPointeurDeb = PointeurDeb
            WsIndexDebCourant = PointeurDeb
            If PointeurFin = -1 Then
                WsPointeurFin = InstanceFichierXML.NombredeLignesData
            Else
                WsPointeurFin = PointeurFin
            End If
        End Sub

        Public Sub SelectData(ByVal SLocalisation As String, ByVal SNomBalise As String)

            WsIndexDataCourant = WsPointeurDeb
            WsIndexDataPrecedent = -1
            WsIndexDataSuivant = -1

            WsIndexCourantStructure = InstanceFichierXML.GetIndexCourantStructure(SLocalisation, SNomBalise, WsPointeurCourantStructure)
            If WsIndexCourantStructure = -1 Then
                WsPointeurCourantStructure = 0
            Else
                WsPointeurCourantStructure = WsIndexCourantStructure
            End If

            WsIndexDataCourant = FindNextDataLocal(WsIndexCourantStructure)

        End Sub

        Public Function GetData() As String
            Dim Chaine As String = InstanceFichierXML.GetData(WsIndexDataCourant)
            WsIndexDebCourant = WsIndexDataCourant
            Return Chaine
        End Function

        Public Function FindNextDataLocal(ByVal IndexCourantStructure As Integer) As Integer
            Dim i As Integer
            Dim BFindNext As Boolean
            BFindNext = False

            If IndexCourantStructure >= 0 Then
                For i = WsIndexDataCourant + 1 To WsPointeurFin
                    If InstanceFichierXML.GetTableauDataInfo(i, 0) = Convert.ToString(IndexCourantStructure) Then
                        Return i
                    End If
                Next i
                If Not BFindNext Then
                    Return -1
                End If
            End If
            Return -1
        End Function

        Public Sub FindNextData()

            If WsIndexCourantStructure >= 0 Then
                WsIndexDataPrecedent = WsIndexDataCourant
                If WsIndexDataSuivant > WsIndexDataCourant Then
                    WsIndexDataCourant = WsIndexDataSuivant
                Else
                    WsIndexDataCourant = FindNextDataLocal(WsIndexCourantStructure)
                End If
            End If

        End Sub

        Public Function EndOfData() As Boolean
            If WsIndexDataCourant >= 0 Then
                Return False
            Else
                Return True
            End If

        End Function

        Public Function GetDataVoisin(ByVal SLocalisation As String, ByVal SNomBalise As String, ByVal SSens As String) As String
            Dim i As Integer
            Dim IndexDeb As Integer
            Dim IndexFin As Integer
            Dim IndexCourantStructure As Integer
            Dim Chaine As String = ""

            If SSens <> "Before" And SSens <> "After" Then
                Return Chaine
            End If

            If SSens = "Before" Then
                IndexDeb = WsIndexDataPrecedent
                If IndexDeb = -1 Then
                    IndexDeb = 0
                End If
                IndexFin = WsIndexDataCourant
            End If
            If SSens = "After" Then
                'Optimisation
                IndexDeb = WsIndexDataCourant

                If WsIndexDataSuivant <= WsIndexDataCourant Then
                    WsIndexDataSuivant = FindNextDataLocal(WsIndexCourantStructure)
                End If
                IndexFin = WsIndexDataSuivant
                If IndexFin = -1 Then
                    IndexFin = WsPointeurFin
                End If
            End If

            If WsIndexDebCourant = -1 Then
                WsIndexDebCourant = IndexDeb
            End If

            IndexCourantStructure = InstanceFichierXML.GetIndexCourantStructure(SLocalisation, SNomBalise, WsPointeurCourantStructure)
            If IndexCourantStructure = -1 Then
                WsPointeurCourantStructure = 0
            Else
                WsPointeurCourantStructure = IndexCourantStructure
            End If

            If IndexCourantStructure >= 0 Then
                'Premi�re recherche avec pointeur WsIndexDebCourant (Optimisation)
                For i = WsIndexDebCourant To IndexFin
                    If InstanceFichierXML.GetTableauDataInfo(i, 0) = Convert.ToString(IndexCourantStructure) Then
                        Chaine = InstanceFichierXML.GetData(i)
                        WsIndexDebCourant = i
                        Return Chaine
                    End If
                Next i
                'Deuxi�me recherche sans Optimisation
                For i = IndexDeb To IndexFin
                    If InstanceFichierXML.GetTableauDataInfo(i, 0) = Convert.ToString(IndexCourantStructure) Then
                        Chaine = InstanceFichierXML.GetData(i)
                        WsIndexDebCourant = i
                        Return Chaine
                    End If
                Next i
            End If
            'Aucun r�sultat
            Return ""
        End Function
    End Class

End Namespace
