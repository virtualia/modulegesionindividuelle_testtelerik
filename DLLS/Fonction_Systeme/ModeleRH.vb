Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.Fonctions
Namespace MetaModele
    Namespace Donnees
        Public Class ModeleRH
            Inherits List(Of MetaModele.Donnees.PointdeVue)

            Private Const SysFicDico As String = "VirtualiaModele"
            Private WsRepertoire As String
            Private WsItemLicence As MetaModele.Donnees.Produit

            Public Property InstanceProduit() As MetaModele.Donnees.Produit
                Get
                    Return WsItemLicence
                End Get
                Set(value As MetaModele.Donnees.Produit)
                    WsItemLicence = value
                End Set
            End Property

            Public ReadOnly Property NomCompletFichierXml() As String
                Get
                    Return WsRepertoire & SysFicDico & ".Xml"
                End Get
            End Property

            Public ReadOnly Property RepertoireduModele() As String
                Get
                    Return WsRepertoire
                End Get
            End Property

            Public ReadOnly Property NombredePointdeVue() As Integer
                Get
                    Return Me.Count
                End Get
            End Property

            Public Overloads ReadOnly Property Item(ByVal Index As Integer) As MetaModele.Donnees.PointdeVue
                Get
                    If (Index < 0 Or Index >= Count) Then
                        Return Nothing
                    End If

                    Return Me(Index)
                End Get
            End Property

            Private Function CreerUnPointdeVue() As Integer
                Dim PtdeVue As MetaModele.Donnees.PointdeVue = New MetaModele.Donnees.PointdeVue(Me)
                Me.Add(PtdeVue)
                Return Count - 1
            End Function

            Public Sub New(ByVal LstPointdeVue As List(Of MetaModele.Donnees.PointdeVue))
                If LstPointdeVue Is Nothing Then
                    Exit Sub
                End If
                Dim IndiceK As Integer
                Dim IPvue As Integer

                For IndiceK = 0 To LstPointdeVue.Count - 1
                    IPvue = CreerUnPointdeVue()
                    Me(IPvue) = LstPointdeVue(IndiceK)
                    Me(IPvue).VIndex = IPvue
                Next IndiceK
            End Sub

            Public Sub New()
                Dim FicXML As FichierXML
                Dim LireXML As LectureXML
                Dim LireXMLObjet As LectureXML
                Dim LireXMLInfo As LectureXML
                Dim Element As String
                Dim IPvue As Integer
                Dim IObjet As Integer
                Dim IInfo As Integer
                Dim BaliseLicence As String = "VirtualiaModele~Licence"
                Dim BalisePvue As String = "VirtualiaModele~PointdeVue"
                Dim BaliseObjet As String = "VirtualiaModele~PointdeVue~Objet"
                Dim BaliseInfo As String = "VirtualiaModele~PointdeVue~Objet~Information"
                Dim BaliseInfoAccess As String = "VirtualiaModele~PointdeVue~Objet~Information~Accessibilite"

                Dim ItemPtdeVue As PointdeVue
                Dim ItemObjet As Objet
                Dim ItemInfo As Information

                WsRepertoire = VI.DossierVirtualiaService("Modele")

                FicXML = New FichierXML(WsRepertoire & SysFicDico & ".Xsd")
                FicXML.FichierXML() = WsRepertoire & SysFicDico & ".Xml"

                LireXML = New LectureXML(FicXML)

                LireXML.SelectData(BaliseLicence, "Produit")

                Element = LireXML.GetData()

                WsItemLicence = New Produit()
                WsItemLicence.NomProduit = Element
                WsItemLicence.NumVersion = LireXML.GetDataVoisin(BaliseLicence, "Version", "After")
                WsItemLicence.SiteWeb = LireXML.GetDataVoisin(BaliseLicence, "SiteWeb", "After")
                WsItemLicence.NumeroLicence = LireXML.GetDataVoisin(BaliseLicence, "Numero", "After")
                WsItemLicence.Organisme = LireXML.GetDataVoisin(BaliseLicence, "Organisme", "After")

                Element = LireXML.GetDataVoisin(BaliseLicence, "Acces", "After")
                If Element <> "" Then
                    WsItemLicence.NombreAcces = CInt(Element)
                End If

                Element = LireXML.GetDataVoisin(BaliseLicence, "Particularite", "After")
                If Element <> "" Then
                    WsItemLicence.Intranet = CInt(Element)
                End If

                WsItemLicence.NomModele = LireXML.GetDataVoisin(BaliseLicence, "MetaModele", "After")

                Element = LireXML.GetDataVoisin(BaliseLicence, "CleModele", "After")
                If Element <> "" Then
                    WsItemLicence.ClefModele = Integer.Parse(Element)
                End If

                WsItemLicence.ClefProduit = LireXML.GetDataVoisin(BaliseLicence, "Clef", "After")
                WsItemLicence.ClefCryptage = LireXML.GetDataVoisin(BaliseLicence, "CleCryptage", "After")

                'Les points de Vue
                LireXML = New LectureXML(FicXML)
                LireXML.SelectData(BalisePvue, "Numero")

                Do Until LireXML.EndOfData
                    Element = LireXML.GetData()
                    IPvue = CreerUnPointdeVue()
                    ItemPtdeVue = Me(IPvue)
                    ItemPtdeVue.Numero = Integer.Parse(Element)
                    ItemPtdeVue.Intitule = LireXML.GetDataVoisin(BalisePvue, "Intitule", "After")

                    'Objets
                    IObjet = 0
                    LireXMLObjet = New LectureXML(FicXML)
                    LireXMLObjet.RattacherAuPere(LireXML.PointeurDebut, LireXML.PointeurFin)
                    LireXMLObjet.SelectData(BaliseObjet, "Numero")

                    Do Until LireXMLObjet.EndOfData
                        Element = LireXMLObjet.GetData()
                        IObjet = ItemPtdeVue.CreerUnObjet()
                        ItemObjet = ItemPtdeVue(IObjet)
                        ItemObjet.Numero = Integer.Parse(Element)
                        ItemObjet.Intitule = LireXMLObjet.GetDataVoisin(BaliseObjet, "Intitule", "After")

                        Element = LireXMLObjet.GetDataVoisin(BaliseObjet, "Nature", "After")
                        If Element <> "" Then
                            ItemObjet.VNature = Integer.Parse(Element)
                        End If

                        Element = LireXMLObjet.GetDataVoisin(BaliseObjet, "Structure", "After")
                        If Element <> "" Then
                            ItemObjet.VStructure = Integer.Parse(Element)
                        End If

                        Element = LireXMLObjet.GetDataVoisin(BaliseObjet, "Limite", "After")
                        If Element <> "" Then
                            ItemObjet.LimiteMaximum = Integer.Parse(Element)
                        End If

                        Element = LireXMLObjet.GetDataVoisin(BaliseObjet, "PositionDateFin", "After")
                        If Element <> "" Then
                            ItemObjet.PositionDateFin = Integer.Parse(Element)
                        End If

                        ItemObjet.NomTableSGBDR = LireXMLObjet.GetDataVoisin(BaliseObjet, "NomSGBDR", "After")

                        Element = LireXMLObjet.GetDataVoisin(BaliseObjet, "CategorieRH", "After")
                        If Element <> "" Then
                            ItemObjet.CategorieRH = Integer.Parse(Element)
                        End If

                        Element = LireXMLObjet.GetDataVoisin(BaliseObjet, "SiCertification", "After")
                        If Element <> "" Then
                            ItemObjet.SiCertification = Integer.Parse(Element)
                        End If

                        IInfo = 0
                        'Informations
                        LireXMLInfo = New LectureXML(FicXML)
                        LireXMLInfo.RattacherAuPere(LireXMLObjet.PointeurDebut, LireXMLObjet.PointeurFin)
                        LireXMLInfo.SelectData(BaliseInfo, "Numero")

                        Do Until LireXMLInfo.EndOfData
                            Element = LireXMLInfo.GetData()
                            IInfo = ItemObjet.CreerUneInformation()
                            ItemInfo = ItemObjet(IInfo)
                            ItemInfo.Numero = Integer.Parse(Element)
                            ItemInfo.Intitule = LireXMLInfo.GetDataVoisin(BaliseInfo, "Intitule", "After")

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfo, "IndexSecondaire", "After")
                            If Element <> "" Then
                                ItemInfo.IndexSecondaire = Integer.Parse(Element)
                            End If

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfo, "Classement", "After")
                            If Element <> "" Then
                                ItemInfo.Classement = Integer.Parse(Element)
                            End If

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfo, "RegleUnicite", "After")
                            If Element <> "" Then
                                ItemInfo.RegleUnicite = Integer.Parse(Element)
                            End If

                            ItemInfo.NomInterne = LireXMLInfo.GetDataVoisin(BaliseInfo, "NomInterne", "After")
                            ItemInfo.Etiquette = LireXMLInfo.GetDataVoisin(BaliseInfo, "Etiquette", "After")

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfo, "CadrageLabel", "After")
                            If Element <> "" Then
                                ItemInfo.CadrageLabel = Integer.Parse(Element)
                            End If

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfo, "Nature", "After")
                            If Element <> "" Then
                                ItemInfo.VNature = Integer.Parse(Element)
                            End If

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfo, "Format", "After")
                            If Element <> "" Then
                                ItemInfo.Format = Integer.Parse(Element)
                            End If

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfo, "Inverse", "After")
                            If Element <> "" Then
                                Try
                                    ItemInfo.PointdeVueInverse = Integer.Parse(Element)
                                Catch ex As Exception
                                    ItemInfo.PointdeVueInverse = 0
                                End Try
                            End If

                            ItemInfo.TabledeReference = LireXMLInfo.GetDataVoisin(BaliseInfo, "Reference", "After")
                            ItemInfo.ValeurDefaut = LireXMLInfo.GetDataVoisin(BaliseInfo, "Default", "After")

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfo, "LongueurMaxi", "After")
                            If Element <> "" Then
                                ItemInfo.LongueurMaxi = Integer.Parse(Element)
                            End If

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfo, "LongueurSGBDR", "After")
                            If Element <> "" Then
                                ItemInfo.LongueurSGBDR = Integer.Parse(Element)
                            End If

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoAccess, "Label", "After")
                            If Element <> "" Then
                                ItemInfo.ZoneEdition(0) = Integer.Parse(Element)
                            End If

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoAccess, "Donnee", "After")
                            If Element <> "" Then
                                ItemInfo.ZoneEdition(1) = Integer.Parse(Element)
                            End If

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoAccess, "Individuelle", "After")
                            If Element <> "" Then
                                ItemInfo.ZoneEdition(2) = Integer.Parse(Element)
                            End If

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoAccess, "MultiCriteres", "After")
                            If Element <> "" Then
                                ItemInfo.ZoneEdition(3) = Integer.Parse(Element)
                            End If

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoAccess, "Statistiques", "After")
                            If Element <> "" Then
                                ItemInfo.ZoneEdition(4) = Integer.Parse(Element)
                            End If

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoAccess, "Impression", "After")
                            If Element <> "" Then
                                ItemInfo.ZoneEdition(5) = Integer.Parse(Element)
                            End If

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoAccess, "Export", "After")
                            If Element <> "" Then
                                ItemInfo.ZoneEdition(6) = Integer.Parse(Element)
                            End If

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoAccess, "Import", "After")
                            If Element <> "" Then
                                ItemInfo.ZoneEdition(7) = Integer.Parse(Element)
                            End If

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoAccess, "Saisie", "After")
                            If Element <> "" Then
                                ItemInfo.ZoneEdition(8) = Integer.Parse(Element)
                            End If

                            Element = LireXMLInfo.GetDataVoisin(BaliseInfoAccess, "Specifique", "After")
                            If Element <> "" Then
                                ItemInfo.ZoneEdition(9) = Integer.Parse(Element)
                            End If

                            LireXMLInfo.FindNextData()
                        Loop
                        LireXMLInfo = Nothing
                        LireXMLObjet.FindNextData()
                    Loop
                    LireXMLObjet = Nothing
                    Element = ""
                    LireXML.FindNextData()
                Loop

            End Sub

        End Class
    End Namespace
End Namespace
