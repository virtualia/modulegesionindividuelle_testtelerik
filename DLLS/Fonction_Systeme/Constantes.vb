Option Strict On
Option Explicit On
Option Compare Text
Namespace Constantes
    Public Module Constantes
        'Bases de donnees **************************************
        Public Enum TypeSgbdrNumeric As Integer
            SgbdrAccess = 1
            SgbdrSqlServer = 2
            SgbdrOracle = 3
            SgbdrDb2 = 4
            SgbdrProgress = 5
            SgbdrMySql = 6
        End Enum

        Public Enum CnxUtilisateur As Integer
            CnxOK = 0
            CnxDejaConnecte = 1
            CnxReprise = 2
            CnxNbLicences = 3
            CnxVirtualia = 4
            CnxRefus = 5
            CnxErreurSysteme = 6
        End Enum

        Public Enum FormatDateSgbdr As Integer
            ' dates SGBDR
            DateStandard = 1
            DateUS = 2
            DateGB = 3
        End Enum

        'Modele
        Public Enum ModeleRh As Integer
            FPCollectivite = 1
            FPPompier = 2
            Sante = 3
            Prive = 4
            Presse = 5
            FPEtat = 6
            CccaBtp = 7
            CfaBtp = 8
            ParaPublic = 9
            Banque = 10
            AfriquePublic = 11
            AfriquePrive = 12
        End Enum

        'Cat�gories RH
        Public Enum CategorieRH As Integer
            InfosPersonnelles = 1
            Diplomes_Qualification = 2
            SituationAdministrative = 3
            AffectationsFonctionnelles = 4
            AffectationsBudgetaires = 5
            TempsdeTravail = 6
            Formation = 7
            Evaluation_Gpec = 8
            Remuneration = 9
            Frais_Deplacement = 10
            Social = 11
            Retraite = 12
        End Enum

        'Points de vue
        Public Enum PointdeVue As Integer
            PVueApplicatif = 1
            PVueGeneral = 2
            PVueGrades = 3
            PVueGrilles = 4
            PVuePosition = 5
            PVueAbsences = 6
            PVueDirection = 7
            PVuePosteBud = 8
            PVueFormation = 9
            PVueEntreprise = 10
            PVuePrimes = 11
            PVuePaie = 12
            PVueMetier = 13
            PVueEtablissement = 14
            PVueBaseHebdo = 15
            PVueCycle = 16
            PVuePresences = 17
            PVueDegres = 18
            PVueQualites = 19
            PVueValTemps = 20
            PVueItineraire = 21
            PVueCommission = 22
            PVueDicoCompetence = 23
            PVueSavoirFaire = 24
            PVueScriptCmc = 25
            PVueScriptStats = 26
            PVueScriptExport = 27
            PVueScriptImport = 28
            PVueScriptEdition = 29
            PVueLiaisonWord = 30
            PVueTranscodification = 31
            PVueDocComposites = 32
            PVueMission = 33
            PVuePerExterne = 34
            PVueTraduction = 35
            PVueInterface = 36
            PVueTableauBord = 37
            PVueSiteGeo = 38
            PVueMonnaie = 39
            PVuePosteFct = 40
            PVueTransaction = 41
            PVueFonctionSupport = 42
            PVueCommunesFr = 43
            PVueTableHie = 44
            PVuePays = 45
            PVueLolf = 46
            PVueEtaEnsEN = 47
            PVueActivite = 48
            PVueContextesJour = 49
            PVueUtilisateur = 50
            PVueVueLogique = 51
            PVueRegles = 52
            PVueConfiguration = 53
            PVueSession = 54
            PVueMaxi = 54
        End Enum

        Public Enum ObjetPer As Integer
            ObaCivil = 1
            ObaConjoint = 2
            ObaEnfant = 3
            ObaAdresse = 4
            ObaBanque = 5
            ObaDiplome = 6
            ObaExperience = 7
            ObaAcquis = 8
            ObaLangue = 9
            ObaLoisir = 10
            ObaStageCv = 11

            ObaAbsence = 15
            ObaDroits = 16
            ObaOrganigramme = 17

            ObaMemento = 20
            ObaIndemnite = 21
            ObaArmee = 22
            ObaExterne = 23  '+ Specif Belge (N� DIMONA)
            ObaAdrPro = 24

            ObaCaisse = 27
            ObaAgenda = 28
            ObaFormation = 29
            ObaPrevenir = 30
            ObaEtranger = 31
            ObaMedical = 32
            ObaPresence = 33
            ObaCompetence = 34
            ObaSanction = 35
            ObaDecoration = 36
            ObaEligible = 37
            ObaRangement = 38
            ObaHandicap = 39
            ObaJournee = 40
            ObaValtemps = 41
            ObaAnnualisation = 42
            ObaHebdoCET = 43
            ObaAdresseHorsFr = 44

            ObaFrais = 47

            ObaActiAnnexes = 48
            ObaAvantage = 49
            ObaEntretien = 50  'Specif Rci Diac
            ObaDocuments = 51
            ObaAppreciation = 52
            ObaSpecialite = 53

            ObaBulletin = 57
            ObaVaccination = 58
            ObaMutuelle = 59

            ObaHeuresSup = 61
            ObaVirtualia = 62

            ObaLoginIntranet = 63
            ObaMessageIntranet = 64
            ObaSaisieIntranet = 65

            ObaVariablePaie = 67

            ObaAccesIntranet = 71
            ObaAdresse2 = 72
            ObaAdresseConge = 73
            ObaMateriel = 74

            ObaPrevention = 76

            ObaAffectation2nd = 78
            ObaPointage = 79
            ObaEpargneTemps = 80
            ObaDemandeFormation = 81
            ObaPrets = 82

            ObaAuthentificationBadge = 86
            ObaThese = 87
            ObaModifPlanning = 91
            ObaDIF = 93
            ObaContextePaie = 94
            'Dependant du modele ************************
            ObaStatut = 12  ' Contrat (Prive)
            ObaActivite = 13  ' Position (Public)
            ObaGrade = 14  ' Emploi conventionnel (Prive)

            ObaPostebud = 18  ' Affectation analytique (Prive)
            ObaNotation = 19

            ObaOrigine = 25  ' Affiliation Groupe (Prive)
            ObaSociete = 26  ' Collectivite ou Administration (Public)

            ObaAutAdm = 45  'Public
            ObaOrdreMission = 46  'Public , Prive
            ObaAgrement = 45  'CCCA-BTP
            ObaSuiviEnseignant = 46  'CCCA-BTP

            ObaStatutDetache = 54  ' Public
            ObaPositionDetache = 55  ' Public
            ObaGradeDetache = 56  ' Public
            ObaInteressement = 54  ' Prive
            ObaPEA = 55  ' Prive
            ObaParticipation = 56  ' Prive
            '
            ObaNotationDetache = 60
            '
            ObaObjectifs = 85  ' Prive
            '
            ObaIntegrationCorps = 68  'Public
            ObaEntretienLangue = 68  'Rci Diac
            ObaCumulRem = 69
            ObaEntretienEvolFonction = 69  'RCI Diac
            ObaFIA = 70  ' Public
            ObaMission = 70  ' Specif Rci Diac

            ObaActiInternes = 75  'Public
            ObaCreditdHeures = 75  ' Specif Honda
            ObaEvolFonction_CP = 75  'Rci Diac

            ObaRetraite = 77  ' Public
            ObaRttHs = 77  ' Specif Lefevre & Pelletier

            ObaDecRecrutement = 83  'Public
            ObaPaieVacation = 84  'Public
            ObaLolf = 92 'Public

            'Version 4 -- � partir de 100
            ObaAnnuaire = 100
            ObaActiviteMensuelle = 110
            ObaActiviteHonda = 110
            ObaActiviteJour = 112
            ObaLanguePratiquee = 120
            ObaTOEIC = 121
            ObaDetailHS = 130
            ObaMensuelHS = 131
            ObaAffectation3rd = 141
            ObaAffectation4rth = 142
            ObaAffectation5th = 143
            ObaAffectation6th = 144

            ObaEntretienPro = 150
            ObaEntretienResultat = 151
            ObaEntretienObjectif = 152
            ObaEntretienServir = 153
            ObaEntretienObs = 154
            ObaEntretienFormation = 155
            ObaEntretienSynthese = 156
            ObaEntretienComplement = 157
            ObaPaieGEST = 160
            ObaCIR = 161
            ObaPaieBASE = 162
            ObaDemandeAcompte = 170
            ObaSIFAC = 180

            ObaCPF = 192
            ObaCPFH = 191
        End Enum

        Public Enum Langue As Integer
            Fran�ais = 1
            Anglais = 2
            Allemand = 3
            Espagnol = 4
            Italien = 5
            Portugais = 6
            Neerlandais = 7
            Flamand = 8
        End Enum

        Public Enum Pays As Integer
            France = 1
            Angleterre = 2
            Allemagne = 3
            Espagne = 4
            Italie = 5
            Portugal = 6
            Hollande = 7
            Belgique = 8
            EtatsUnis = 9
            Luxembourg = 10
        End Enum

        Public Enum FormatDatesSysteme As Integer
            DateFrance10 = 1
            DateFrance8 = 2
            DateUS10 = 3
            DateUS8 = 4
            DateInverse10 = 5
            DateInverse8 = 6
            DateUS5 = 7
        End Enum

        Public Enum OptionInfo As Integer
            DicoEtiVisible = 0
            DicoDonVisible = 1
            DicoRecherche = 2
            DicoCmc = 3
            DicoStats = 4
            DicoImpression = 5
            DicoExport = 6
            DicoImport = 7
            DicoDonMaj = 8
            DicoSpecifique = 9
            DicoWord = 16
        End Enum

        Public Enum TypeObjet As Integer
            ObjetSimple = 0
            ObjetDate = 1
            ObjetMemo = 2
            ObjetTableau = 3
            ObjetHeure = 4
            ObjetDateRang = 5
        End Enum

        Public Enum ComportementObjet As Integer
            ObjetEvenement = 1
            ObjetPermanent = 2
            ObjetPerenne = 3
            ObjetSimultane = 4
        End Enum

        Public Enum NatureDonnee As Integer
            DonneeTexte = 0
            DonneeDate = 1
            DonneeNumerique = 2
            DonneeBooleenne = 3
            DonneeCalculee = 4
            DonneeTable = 5
            DonneeExperte = 6
            DonneeHeure = 7
            DonneeMemo = 8
            DonneeTableMemo = 9
            DonneeFichier = 10
            DonneeIntegrite = 11
            DonneeDateTime = 12
        End Enum

        Public Enum FormatDonnee As Integer
            Standard = 0
            Minuscule = 1
            Majuscule = 2
            L1Capi = 3
            Coche = 5
            OptionRadio = 6
            Monetaire = 20
            Pourcentage = 21
            Fixe = 22
            Decimal1 = 23
            Decimal2 = 24
            Decimal3 = 25
            Decimal4 = 26
            Decimal5 = 27
            Decimal6 = 28
            Annee = 30
            Duree = 31
            Age = 32
            FinAnnee = 33
            Heure = 34
            DureeHeure = 35
            Crypte = 40
            Url = 41
        End Enum

        Public Enum StructureEcran As Integer
            Simple = 0
            AListe = 1
            ADoubleListe = 2
            AColonneGauche = 10
            AColonneDroite = 11
            AColonneXGauche = 12
            AColonneXDroite = 13
        End Enum

        Public Enum StructureFicheEcran As Integer
            Simple = 0
            AListe = 1
            ADoubleListe = 2
            AColonneGauche = 10
            AColonneDroite = 11
            AColonneXGauche = 12
            AColonneXDroite = 13
            SimpleSurOnglet = 20
            AListeSurOnglet = 21
            A2ListesSurOnglet = 22
        End Enum

        Public Enum Circulation As Integer
            SituPrecedente = -1
            SituCourante = 0
            SituSuivante = 1
        End Enum

        Public Enum TypeExperte As Integer
            'Expertes
            Directe = 0
            Liste = 1
            ParReference = 2
        End Enum

        Public Enum Autorisations As Integer
            'Gestion des autorisations
            Rien = 0
            Tout = 1
            Lecture = 2
            Seule = 3
        End Enum

        Public Enum Jours As Integer
            Lundi = 1
            Mardi = 2
            Mercredi = 3
            Jeudi = 4
            Vendredi = 5
            Samedi = 6
            Dimanche = 7
        End Enum

        Public Enum ComparaisonDates As Integer
            Egalite = 0
            PlusGrand = 1
            PlusPetit = 2
        End Enum

        Public Enum TypeEcran As Integer
            Vga = 1
            Svga8 = 2
            Svga17 = 3
        End Enum

        Public Enum Operateurs As Integer
            ET = 0
            OU = 1
            Egalite = 0
            Difference = 1
            Inclu = 2
            Exclu = 3
            Superieur = 4
            Inferieur = 5
            Superieur_Egal = 6
            Inferieur_Egal = 7
            Constante = 99
            Variable = 0
            Fixe = 1
        End Enum

        Public Enum TypedeConnexion As Integer
            CnxStandard = 1
            CnxFullLdap = 2
            CnxPki = 3
            CnxIdeLdap = 4
            CnxNumen = 5
        End Enum

        Public Enum DemandeIntranet As Integer
            MajDanslaDatabase = 1
            ValideParManager = 2
            EnAttente = 3
            RefuseParDrh = 4
            RefuseParManager = 5
            VisaDrh = 6
            ValidePuisAnnule = 7
        End Enum

        Public Enum ModeCalculPrime As Integer
            RegimeIndemnitaire = 0
            NombredePoints = 1
            Pourcentage = 2
            MontantForfaitaire = 3
            Anciennete = 4
            TauxHoraire = 5
        End Enum

        Public Enum NaturePlage As Integer
            HorsEfectif = 0
            Presence = 1
            Absence = 2
            Formation = 3
            ReposHebdomadaire = 4
            JourFerie = 5
            Conge = 6
            Maladie = 7
            EvtAbsence = 8
            EvtPresence = 9
            Deplacement = 10
            Rtt = 11
            ViaIntranet = 12
            CompteEpargneTemps = 13
        End Enum

        Public Enum NumeroPlage As Integer
            Aucune = 0
            Plage1_Presence = 1
            Plage2_Presence = 2
            Plage3_Presence = 3
            Plage4_Presence = 4
            Plage5_Presence = 5
            Plage6_Presence = 6
            Plage7_Presence = 7
            Plage8_Presence = 8
            Jour_Absence = 10
            Plage1_Absence = 11
            Plage2_Absence = 12
            Jour_Formation = 20
            Plage1_Formation = 21
            Plage2_Formation = 22
            Jour_Mission = 30
            Plage1_Mission = 31
            Plage2_Mission = 32
            Regle1_Valorisation = 101
            Regle2_Valorisation = 102
            Regle3_Valorisation = 103
            Regle4_Valorisation = 104
            Regle5_Valorisation = 105
            Regle6_Valorisation = 106
            Regle7_Valorisation = 107
            Regle8_Valorisation = 108
            Regle9_Valorisation = 109
            Regle10_Valorisation = 110
            Regle11_Valorisation = 111
            Regle12_Valorisation = 112
        End Enum

        Public Enum TypedeCycleTravail As Integer
            Aucun = 0
            Administratif = 1
            CycledeTravail = 2
            Personnalise = 3
            EmploiduTemps = 4
            Ajustement = 5
        End Enum

        Public Enum ObjetReg As Integer
            ObaDefinition = 1
            ObaRegle = 2
            ObaDeclenchement = 3
            ObaResultat = 4
            ObaFormule = 5
            ObaCritere = 6
            ObaValeur = 7
        End Enum

        Public Enum CategoriePaie As Integer
            NetAPayer = 4
            Pr�compte_Retenues = 5
            Acomptes = 6
            Brut = 10
            IR = 11
            SFT = 12
            Primes = 13
            HeuresSup = 14
            Transport = 15
            Rappel = 16
            RetenuePC = 17
            Brut_non_renseign� = 19
            Po_SS = 20
            Po_CSG = 21
            Po_RDS = 22
            Po_Solidarite = 23
            Po_Retraite = 24
            Po_FNAL = 25
            Po_Chomage = 26
            Po_PensionCivile = 27
            Po_Divers = 28
            Po_non_renseign�e = 29
            PP_SS = 30
            PP_Pension_Civile = 31
            PP_PRATI = 32
            PP_Solidarite = 33
            PP_Retraite = 34
            PP_FNAL = 35
            PP_Chomage = 36
            PP_Transport = 37
            PP_Divers = 38
            Compensation_Charges = 40
            Imposable = 50
            Base_Deplafonnee = 51
            Base_Plafonnee = 52
            Prets = 53
            Mutuelles = 54
            Infos = 55

        End Enum

        Public Enum IdentifiantPaie As Integer
            DGCL = 7
            GAPAIE = 6
            GMT = 9
            KA_KX = 2
            MK = 3
            SAGE = 5
            VIRTUALIA = 1
        End Enum

        Public Enum TypeRequete As Integer
            VirFiche = 0
            Selection = 1
        End Enum

        'Constantes Caracteres
        Public Const PointVirgule As String = ";"
        Public Const Virgule As String = ","
        Public Const PointFinal As String = "."
        Public Const Quote As String = "'"
        Public Const DoubleQuote As String = """"
        Public Const Arobase As String = "@"
        Public Const AntiSlash As String = "\"
        Public Const Slash As String = "/"
        Public Const SigneEgal As String = "="
        Public Const Tild As String = "~"
        Public Const Tiret As String = "-"
        Public Const SigneBarre As String = "|"
        Public Const SigneInferieur As String = "<"
        Public Const SigneSuperieur As String = ">"

        Public Const ViaUtiAdmin As String = "A"
        Public Const ViaUtiUtiGestion As String = "U"

        Public Const XmlSgbdrAccess As String = "Access"
        Public Const XmlSgbdrSqlServer As String = "SqlServer"
        Public Const XmlSgbdrOracle As String = "Oracle"
        Public Const XmlSgbdrDb2 As String = "Db2"
        Public Const XmlSgbdrProgress As String = "Progress"
        Public Const XmlSgbdrMySql As String = "MySql"

        Public Const ChaineAlphabetique As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

        'Types de Paie
        Public Const PaieDGCL As String = "DGCL"
        Public Const PaieGAPAIE As String = "GAPAIE"
        Public Const PaieGMT As String = "GMT"
        Public Const PaieKA As String = "KA"
        Public Const PaieKX As String = "KX"
        Public Const PaieMK As String = "MK" 'Fonction publique Paie Expatri�e
        Public Const PaieSAGE As String = "SAGE"
        'Types de remuneration
        Public Const TYPE_REMUN As String = "Type de remuneration"
        Public Const REGIME_REMUN As String = "Regime de cotisation"
        Public Const REGLE_REMUN As String = "Regles de remuneration"
        Public Const ELEMENT_REMUN As String = "Element de remuneration"
        Public Const TYPAIE_INDICIAIRE As String = "Indiciaire"
        Public Const TYPAIE_HORAIRE As String = "Horaire"
        Public Const TYPAIE_HORSMIC As String = "Horaire SMIC"
        Public Const TYPAIE_POINT As String = "Au point"
        Public Const TYPAIE_FORFAIT As String = "Forfait mensuel"
        Public Const TYPAIE_FORFANN As String = "Forfait annuel"
        Public Const TYPAIE_ACTE As String = "A l'acte"
        Public Const TYPAIE_HORSECH As String = "Hors-echelle"

        'Ide CPF dans TAB
        Public Const Ide_CPF As Integer = 500


        Public Function PositiondansAlphabet(ByVal Lettre As String, ByVal SiInverse As Boolean) As Integer
            Dim IndiceI As Integer
            Dim IndiceK As Integer
            Select Case Lettre
                Case Is = "�"
                    Lettre = "A"
                Case Is = "e", "e", "�"
                    Lettre = "E"
                Case Is = "�"
                    Lettre = "U"
            End Select
            Select Case SiInverse
                Case True
                    IndiceK = 0
                    For IndiceI = 26 To 1 Step -1
                        IndiceK += 1
                        Select Case Strings.Mid(ChaineAlphabetique, IndiceI, 1)
                            Case Is = Lettre
                                Return IndiceK
                                Exit For
                        End Select
                    Next IndiceI
                Case False
                    For IndiceI = 1 To 26
                        Select Case Strings.Mid(ChaineAlphabetique, IndiceI, 1)
                            Case Is = Lettre
                                Return IndiceI
                                Exit For
                        End Select
                    Next IndiceI
            End Select
            Return 1
        End Function

        Public Function LettredelAlphabet(ByVal Position As Integer, ByVal SiInverse As Boolean) As String
            Select Case SiInverse
                Case True
                    Return Strings.Mid(ChaineAlphabetique, 27 - Position, 1)
                Case Else
                    Return Strings.Mid(ChaineAlphabetique, Position, 1)
            End Select
        End Function

        Public Function SqlDateduServeur(ByVal TypeDateServeur As Integer, ByVal TypeDuSgbdr As Integer, ByVal Valeur As String) As String
            Dim Chaine As String

            Chaine = Valeur
            Select Case TypeDuSgbdr
                Case TypeSgbdrNumeric.SgbdrAccess ' americain
                    Chaine = "#" & DateValue(Valeur).ToString & "#"
                Case TypeSgbdrNumeric.SgbdrSqlServer, TypeSgbdrNumeric.SgbdrProgress
                    Select Case TypeDateServeur
                        Case FormatDateSgbdr.DateStandard
                            Chaine = Quote & Valeur & Quote
                        Case FormatDateSgbdr.DateUS
                            Chaine = Quote & DateValue(Valeur).ToString & Quote
                    End Select
                Case TypeSgbdrNumeric.SgbdrOracle
                    Select Case TypeDateServeur
                        Case FormatDateSgbdr.DateStandard
                            Chaine = "TO_DATE('" & Valeur & "','DD/MM/YYYY')"
                        Case FormatDateSgbdr.DateUS
                            Chaine = "TO_DATE('" & Valeur & "','MM-DD-YYYY')"
                    End Select
                Case TypeSgbdrNumeric.SgbdrDb2, TypeSgbdrNumeric.SgbdrMySql
                    Chaine = Quote & DateValue(Valeur).ToShortDateString & Quote
            End Select
            Return Chaine
        End Function

        Public Function ParametreApplication(ByVal Clef As String) As String
            Dim appSettings As System.Collections.Specialized.NameValueCollection = System.Configuration.ConfigurationManager.AppSettings
            Dim appSettingsEnum As IEnumerator = appSettings.Keys.GetEnumerator()
            Dim Valeur As String
            Dim i As Integer = 0
            While appSettingsEnum.MoveNext()
                Dim key As String = appSettings.Keys(i)
                Valeur = appSettings(key)
                If key = Clef Then
                    Return Valeur
                    Exit Function
                End If
                i += 1
            End While
            Return ""
        End Function

        Public ReadOnly Property DossierVirtualiaService(ByVal NatureDossier As String) As String
            Get
                Dim Repertoire As String = ""
                Repertoire = System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia")
                If Repertoire Is Nothing OrElse Repertoire = "" Then
                    Repertoire = My.Computer.FileSystem.SpecialDirectories.MyDocuments.ToString & "\VirtualiaVersion4\"
                End If
                If Strings.Right(Repertoire, 1) <> AntiSlash Then
                    Repertoire = Repertoire & AntiSlash
                End If
                Select Case NatureDossier
                    Case Is = "Temp"
                        Repertoire = Repertoire & "Temp"
                    Case Else
                        Repertoire = Repertoire & "XML\" & NatureDossier
                End Select
                If My.Computer.FileSystem.DirectoryExists(Repertoire) = False Then
                    My.Computer.FileSystem.CreateDirectory(Repertoire)
                End If
                Return Repertoire & "\"
            End Get
        End Property
    End Module
End Namespace