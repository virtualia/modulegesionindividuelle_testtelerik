﻿Option Strict On
Option Explicit On
Option Compare Text
Namespace WebService
    Public Class ServiceWcfSessions
        Public ReadOnly Property ID_Generique(ByVal NoSession As String) As Boolean
            Get
                Dim WcfWebService As Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                Dim RhFct As Virtualia.Systeme.Fonctions.Generales
                Dim Cretour As Boolean
                Dim Chaine As String

                RhFct = New Virtualia.Systeme.Fonctions.Generales
                Chaine = RhFct.HashSHA("Antigone_" & System.DateTime.Now.DayOfYear).ToLower
                WcfWebService = New Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                Cretour = WcfWebService.ID_SessionVirtualia(Chaine, NoSession)
                WcfWebService.Close()

                Return Cretour
            End Get
        End Property

        Public ReadOnly Property Url_Applicative(ByVal NoSession As String, ByVal Index As Integer, ByVal Param As String) As String
            Get
                Dim WcfWebService As Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                Dim UrlAppli As String

                WcfWebService = New Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                UrlAppli = WcfWebService.UrlWebApplication(NoSession, Index, Param)
                WcfWebService.Close()

                Return UrlAppli
            End Get
        End Property

        Public ReadOnly Property Url_WebService_Outil() As String
            Get
                Dim WcfWebService As Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                Dim UrlAppli As String

                WcfWebService = New Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                UrlAppli = WcfWebService.UrlWebServiceOutils
                WcfWebService.Close()

                Return UrlAppli
            End Get
        End Property

        Public ReadOnly Property Contexte(ByVal NoSession As String) As Virtualia.Net.ServiceSessions.ContexteUtilisateurType
            Get
                Dim WcfWebService As Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                Dim ContexteUti As Virtualia.Net.ServiceSessions.ContexteUtilisateurType

                WcfWebService = New Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                ContexteUti = WcfWebService.ContexteSession(NoSession)
                WcfWebService.Close()

                Return ContexteUti
            End Get
        End Property

        Public ReadOnly Property Utilisateur(ByVal NoSession As String) As Virtualia.Net.ServiceSessions.SessionUtilisateurType
            Get
                Dim WcfWebService As Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                Dim CnxUti As Virtualia.Net.ServiceSessions.SessionUtilisateurType

                WcfWebService = New Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                CnxUti = WcfWebService.UtilisateurVirtualia(NoSession)
                WcfWebService.Close()

                Return CnxUti
            End Get
        End Property

        Public ReadOnly Property Si_ConnexionEstAutorisee(ByVal NoBd As Integer, ByVal ModedeConnexion As String, _
                                                          ByVal ChaindeConnexion As String, ByVal Identite As String, ByVal PasswordCrypte As String) As Boolean
            Get
                Dim WcfWebService As Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                Dim SiOK As Boolean
                WcfWebService = New Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                SiOK = WcfWebService.SiConnexionAutorisee(NoBd, ModedeConnexion, ChaindeConnexion, Identite, PasswordCrypte)
                WcfWebService.Close()

                Return SiOK
            End Get
        End Property

        Public ReadOnly Property Si_ModificatonPasswordOK(ByVal NoBd As Integer, ByVal ModedeConnexion As String, ByVal Identite As String, _
                                                          ByVal PasswordCrypte As String) As Boolean
            Get
                Dim WcfWebService As Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                Dim SiOK As Boolean
                WcfWebService = New Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                SiOK = WcfWebService.SiModificationPasswordOK(NoBd, ModedeConnexion, Identite, PasswordCrypte)
                WcfWebService.Close()

                Return SiOK
            End Get
        End Property

        Public ReadOnly Property Nouveau_Password(ByVal NoBd As Integer, ByVal ModedeConnexion As String, ByVal Identite As String, _
                                                          ByVal EMail As String) As String
            Get
                Dim WcfWebService As Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                Dim NewPw As String
                WcfWebService = New Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                NewPw = WcfWebService.NouveauPassword(NoBd, ModedeConnexion, Identite, EMail)
                WcfWebService.Close()

                Return NewPw
            End Get
        End Property

        Public ReadOnly Property FinSession(ByVal NoSession As String) As Boolean
            Get
                Dim WcfWebService As Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                Dim Cretour As Boolean

                WcfWebService = New Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                Cretour = WcfWebService.Deconnexion(NoSession)
                WcfWebService.Close()

                Return Cretour
            End Get
        End Property
    End Class
End Namespace