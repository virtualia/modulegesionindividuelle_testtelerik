﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele

Namespace WebService
    Public Class ServiceWebServeur
        Implements WebService.IServiceServeur
        Private WsTypeModele As Integer = VI.ModeleRh.FPEtat
        Private WsSpecificite As Integer = 0
        Private WsUrlWebServeur As String

        Public Function ChangerBasedeDonneesCourante(NomUtiSgbd As String, NoBd As Integer) As Boolean Implements IServiceServeur.ChangerBasedeDonneesCourante
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim Cretour As Boolean
            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            Cretour = Proxy.ChangerlaBasedeDonnees(NomUtiSgbd, NoBd)
            Proxy.Dispose()
            Return Cretour
        End Function

        Public Function FermetureSession(NomUtiSgbd As String) As Boolean Implements IServiceServeur.FermetureSession
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim Cretour As Boolean

            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            Cretour = Proxy.FermetureSession(NomUtiSgbd)
            Proxy.Dispose()
            Return Cretour
        End Function

        Public ReadOnly Property InformationLogiciel() As ServiceServeur.ProduitType Implements IServiceServeur.InformationLogiciel
            Get
                Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
                Dim InfoProduit As Virtualia.Net.ServiceServeur.ProduitType

                Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
                InfoProduit = New Virtualia.Net.ServiceServeur.ProduitType

                InfoProduit.ClefCryptage = Proxy.InformationsProduit("Cryptage")
                InfoProduit.NombreLicences = CInt(Proxy.InformationsProduit("Droits"))
                InfoProduit.Nom = Proxy.InformationsProduit("Intitule")
                InfoProduit.ClefLicence = Proxy.InformationsProduit("Licence")
                InfoProduit.Organisme = Proxy.InformationsProduit("Organisme")
                InfoProduit.SiteWeb = Proxy.InformationsProduit("SiteWebVirtualia")
                InfoProduit.NumeroVersion = Proxy.InformationsProduit("Version")

                Proxy.Dispose()
                Return InfoProduit
            End Get
        End Property

        Public Function Inserer_Lot_Fiches(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, Ide As Integer, ValeursMaj As String, Optional SiJournaliser As Boolean = True) As Integer Implements IServiceServeur.Inserer_Lot_Fiches
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim IndiceK As Integer

            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)

            IndiceK = Proxy.InsererLotFiches(NomUtiSgbd, Pvue, NoObjet, Ide, ValeursMaj)

            If IndiceK = 0 And SiJournaliser = True Then
                Dim Jrn As Virtualia.TablesObjet.ShemaVUE.VUE_Journal
                Dim LstJrn As New List(Of String)
                Dim IndiceI As Integer
                Dim TableauData(0) As String
                Dim Cretour As Boolean
                Dim IndiceJ As Integer
                Dim LstSimple As List(Of String)

                TableauData = Strings.Split(ValeursMaj, VI.SigneBarre, -1)
                For IndiceI = 0 To TableauData.Count - 1
                    Jrn = New Virtualia.TablesObjet.ShemaVUE.VUE_Journal(NomUtiSgbd, "C", Pvue, NoObjet, Ide)
                    Jrn.ContenuTable = ValeursMaj
                    LstJrn.Add(Jrn.Enregistrement)
                    If IndiceI Mod 20 = 0 Then
                        Try
                            Cretour = Proxy.Journaliser(NomUtiSgbd, LstJrn.ToArray)
                        Catch ex1 As Exception
                            For IndiceJ = 0 To LstJrn.Count - 1
                                LstSimple = New List(Of String)
                                LstSimple.Add(LstJrn(IndiceJ))
                                Cretour = Proxy.Journaliser(NomUtiSgbd, LstSimple.ToArray)
                            Next IndiceJ
                        End Try
                        LstJrn = New List(Of String)
                    End If
                Next IndiceI
                If LstJrn.Count > 0 Then
                    Try
                        Cretour = Proxy.Journaliser(NomUtiSgbd, LstJrn.ToArray)
                    Catch ex2 As Exception
                        For IndiceJ = 0 To LstJrn.Count - 1
                            LstSimple = New List(Of String)
                            LstSimple.Add(LstJrn(IndiceJ))
                            Cretour = Proxy.Journaliser(NomUtiSgbd, LstSimple.ToArray)
                        Next IndiceJ
                    End Try
                End If
            End If

            Proxy.Dispose()

            Return IndiceK
        End Function

        Public ReadOnly Property Instance_ConfigV4 As Systeme.Configuration.FichierConfig Implements IServiceServeur.Instance_ConfigV4
            Get
                Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
                Dim InstanceConfig As Systeme.Configuration.FichierConfig
                Dim NomFicXml As String = VI.DossierVirtualiaService("Parametrage") & "VirtualiaConfig.Xml"
                Dim TabChaine As Byte()

                InstanceConfig = New Systeme.Configuration.FichierConfig
                If InstanceConfig.ListeSections IsNot Nothing Then
                    Return InstanceConfig
                End If
                Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
                TabChaine = Proxy.ObtenirFichierXml("Virtualia", "Config.Xml")
                Proxy.Dispose()
                If TabChaine Is Nothing Then
                    Return InstanceConfig
                End If
                InstanceConfig = New Systeme.Configuration.FichierConfig
                Return InstanceConfig
            End Get
        End Property

        Public ReadOnly Property Instance_Database() As Systeme.Parametrage.BasesdeDonnees.ParamSgbd Implements IServiceServeur.Instance_Database
            Get
                Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
                Dim InstanceClientBd As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbd
                Dim NomFicXml As String = VI.DossierVirtualiaService("Parametrage") & "VirtualiaSgbd.Xml"
                Dim NomFicXsd As String = VI.DossierVirtualiaService("Parametrage") & "VirtualiaSgbd.Xsd"
                Dim TabChaine As Byte()

                InstanceClientBd = New Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbd
                If InstanceClientBd IsNot Nothing Then
                    Return InstanceClientBd
                End If
                Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
                TabChaine = Proxy.ObtenirFichierXml("Virtualia", "SgbdXml")
                If TabChaine Is Nothing Then
                    Proxy.Dispose()
                    Return InstanceClientBd
                End If
                My.Computer.FileSystem.WriteAllBytes(NomFicXml, TabChaine, False)
                TabChaine = Proxy.ObtenirFichierXml("Virtualia", "SgbdXsd")
                If TabChaine Is Nothing Then
                    Proxy.Dispose()
                    Return InstanceClientBd
                End If
                My.Computer.FileSystem.WriteAllBytes(NomFicXsd, TabChaine, False)
                InstanceClientBd = New Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbd
                Proxy.Dispose()

                Return InstanceClientBd
            End Get
        End Property

        Public ReadOnly Property Instance_ExperteRH() As Expertes.ExpertesRH Implements IServiceServeur.Instance_ExperteRH
            Get
                Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
                Dim InstanceExperte As Virtualia.Systeme.MetaModele.Expertes.ExpertesRH = Nothing
                Dim NomFicXml As String = VI.DossierVirtualiaService("Modele") & "VirtualiaExpertes.Xml"
                Dim NomFicXsd As String = VI.DossierVirtualiaService("Modele") & "VirtualiaExpertes.Xsd"
                Dim TabChaine As Byte()

                InstanceExperte = New Virtualia.Systeme.MetaModele.Expertes.ExpertesRH
                If InstanceExperte IsNot Nothing Then
                    Return InstanceExperte
                End If
                Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
                TabChaine = Proxy.ObtenirFichierXml("Virtualia", "ExpertesXml")
                If TabChaine Is Nothing Then
                    Proxy.Dispose()
                    Return InstanceExperte
                End If
                My.Computer.FileSystem.WriteAllBytes(NomFicXml, TabChaine, False)
                TabChaine = Proxy.ObtenirFichierXml("Virtualia", "ExpertesXsd")
                If TabChaine Is Nothing Then
                    Proxy.Dispose()
                    Return InstanceExperte
                End If
                My.Computer.FileSystem.WriteAllBytes(NomFicXsd, TabChaine, False)
                InstanceExperte = New Virtualia.Systeme.MetaModele.Expertes.ExpertesRH
                Proxy.Dispose()

                Return InstanceExperte
            End Get
        End Property

        Public ReadOnly Property Instance_ModeleRH() As Donnees.ModeleRH Implements IServiceServeur.Instance_ModeleRH
            Get
                Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
                Dim InstanceModele As Virtualia.Systeme.MetaModele.Donnees.ModeleRH = Nothing
                Dim NomFicXml As String = VI.DossierVirtualiaService("Modele") & "VirtualiaModele.Xml"
                Dim NomFicXsd As String = VI.DossierVirtualiaService("Modele") & "VirtualiaModele.Xsd"
                Dim Chaine As String
                Dim TabChaine As Byte()

                Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
                Select Case My.Computer.FileSystem.FileExists(NomFicXml)
                    Case True
                        Chaine = Proxy.InformationsProduit("Version")
                        InstanceModele = New Virtualia.Systeme.MetaModele.Donnees.ModeleRH
                        Select Case InstanceModele.InstanceProduit.NumVersion
                            Case Is = Chaine
                                Proxy.Dispose()
                                Return InstanceModele
                        End Select
                End Select

                TabChaine = Proxy.ObtenirFichierXml("Virtualia", "ModeleXml")
                If TabChaine Is Nothing Then
                    Proxy.Dispose()
                    Return InstanceModele
                End If
                My.Computer.FileSystem.WriteAllBytes(NomFicXml, TabChaine, False)
                TabChaine = Proxy.ObtenirFichierXml("Virtualia", "ModeleXsd")
                If TabChaine Is Nothing Then
                    Proxy.Dispose()
                    Return InstanceModele
                End If
                My.Computer.FileSystem.WriteAllBytes(NomFicXsd, TabChaine, False)
                InstanceModele = Nothing
                InstanceModele = New Virtualia.Systeme.MetaModele.Donnees.ModeleRH
                Proxy.Dispose()

                Return InstanceModele
            End Get
        End Property

        Public Function Journaliser_Maj(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, Ide As Integer, CodeMaj As String, ContenuFiche As String) As Boolean Implements IServiceServeur.Journaliser_Maj
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim Jrn As Virtualia.TablesObjet.ShemaVUE.VUE_Journal
            Dim LstJrn As New List(Of String)
            Dim Cretour As Boolean
            Jrn = New Virtualia.TablesObjet.ShemaVUE.VUE_Journal(NomUtiSgbd, CodeMaj, Pvue, NoObjet, Ide)
            LstJrn.Add(ContenuFiche)
            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            Cretour = Proxy.Journaliser(NomUtiSgbd, LstJrn.ToArray)
            Proxy.Dispose()
            Return Cretour
        End Function

        Public Function LectureDossier_ToFiches(NomUtiSgbd As String, Pvue As Integer, Ide As Integer, SiTri As Boolean, ListeNoObjets As List(Of Integer)) As List(Of VIR_FICHE) Implements IServiceServeur.LectureDossier_ToFiches
            Dim LstComplet As List(Of String)
            Dim LstObjet As List(Of String)
            Dim LstFiches As List(Of VIR_FICHE)
            Dim IndiceI As Integer
            Dim NoObjet As Integer = 1
            Dim TableauData(0) As String

            LstComplet = LectureDossier_ToListeChar(NomUtiSgbd, Pvue, Ide, SiTri, ListeNoObjets)
            If LstComplet Is Nothing Then
                Return Nothing
            End If
            LstFiches = New List(Of VIR_FICHE)
            LstObjet = New List(Of String)
            For IndiceI = 0 To LstComplet.Count - 1
                If Strings.Left(LstComplet.Item(IndiceI), 6) = "Objet=" Then
                    If LstObjet.Count > 0 Then
                        LstFiches.AddRange(ListeFichesVirtualia(Pvue, NoObjet, LstObjet))
                    End If
                    TableauData = Strings.Split(LstComplet.Item(IndiceI), VI.Tild, -1)
                    NoObjet = CInt(Strings.Right(TableauData(0), TableauData(0).Length - 6))
                    LstObjet = New List(Of String)
                Else
                    LstObjet.Add(LstComplet.Item(IndiceI))
                End If
            Next IndiceI
            Return LstFiches
        End Function

        Public Function LectureDossier_ToListeChar(NomUtiSgbd As String, Pvue As Integer, Ide As Integer, SiTri As Boolean, ListeNoObjets As List(Of Integer)) As List(Of String) Implements IServiceServeur.LectureDossier_ToListeChar
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim ChaineLue As String = ""
            Dim LstComplet As List(Of String)
            Dim TableauObjet(0) As String
            Dim ListeChaineObjet As String = ""

            For Each Obj As Integer In ListeNoObjets
                ListeChaineObjet &= CStr(Obj) & VI.PointVirgule
            Next
            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            ChaineLue = Proxy.LireUnDossier(NomUtiSgbd, Pvue, Strings.Left(ListeChaineObjet, ListeChaineObjet.Length - 1), Ide)
            Proxy.Dispose()
            If ChaineLue = "" Then
                Return Nothing
            End If
            TableauObjet = Strings.Split(ChaineLue, VI.SigneBarre, -1)
            LstComplet = New List(Of String)
            For Each Ligne As String In TableauObjet
                If Ligne <> "" Then
                    LstComplet.Add(Ligne)
                End If
            Next
            Return LstComplet
        End Function

        Public Function LectureObjet_Plage_ToFiches(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, SiTri As Boolean, IdeDebut As Integer, IdeFin As Integer) As List(Of VIR_FICHE) Implements IServiceServeur.LectureObjet_Plage_ToFiches
            Dim LstComplet As List(Of String)
            LstComplet = LectureObjet_Plage_ToListeChar(NomUtiSgbd, Pvue, NoObjet, SiTri, IdeDebut, IdeFin)
            If LstComplet Is Nothing Then
                Return Nothing
            End If
            Return ListeFichesVirtualia(Pvue, NoObjet, LstComplet)
        End Function

        Public Function LectureObjet_Plage_ToListeChar(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, SiTri As Boolean, IdeDebut As Integer, IdeFin As Integer) As List(Of String) Implements IServiceServeur.LectureObjet_Plage_ToListeChar
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim ChaineLue As System.Text.StringBuilder
            Dim ChainePage As String
            Dim LstComplet As List(Of String)
            Dim NoPage As Integer = 1
            Dim TableauObjet(0) As String

            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            ChaineLue = New System.Text.StringBuilder
            Do
                ChainePage = Proxy.LireUnObjetPlage(NomUtiSgbd, Pvue, NoObjet, IdeDebut, IdeFin, SiTri, NoPage)
                If ChainePage = "" Then
                    Exit Do
                End If
                ChaineLue.Append(ChainePage)
                NoPage += 1
            Loop
            Proxy.Dispose()
            If ChaineLue.ToString = "" Then
                Return Nothing
            End If
            TableauObjet = Strings.Split(ChaineLue.ToString, VI.SigneBarre, -1)
            LstComplet = New List(Of String)
            For NoPage = 0 To TableauObjet.Count - 1
                If TableauObjet(NoPage) <> "" Then
                    LstComplet.Add(TableauObjet(NoPage))
                End If
            Next NoPage
            Return LstComplet
        End Function

        Public Function LectureObjet_Plus_ToFiches(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, SiTri As Boolean, ListeIde As List(Of Integer)) As List(Of VIR_FICHE) Implements IServiceServeur.LectureObjet_Plus_ToFiches
            Dim LstComplet As List(Of String)
            LstComplet = LectureObjet_Plus_ToListeChar(NomUtiSgbd, Pvue, NoObjet, SiTri, ListeIde)
            If LstComplet Is Nothing Then
                Return Nothing
            End If
            Return ListeFichesVirtualia(Pvue, NoObjet, LstComplet)
        End Function

        Public Function LectureObjet_Plus_ToListeChar(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, SiTri As Boolean, ListeIde As List(Of Integer)) As List(Of String) Implements IServiceServeur.LectureObjet_Plus_ToListeChar
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim ChaineLue As System.Text.StringBuilder
            Dim ChainePage As String
            Dim LstComplet As List(Of String)
            Dim NoPage As Integer = 1
            Dim TableauObjet(0) As String

            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            ChaineLue = New System.Text.StringBuilder
            Do
                ChainePage = Proxy.LireUnObjetPlus(NomUtiSgbd, Pvue, NoObjet, ListeIde.ToArray, SiTri, NoPage)
                If ChainePage = "" Then
                    Exit Do
                End If
                ChaineLue.Append(ChainePage)
                NoPage += 1
            Loop
            Proxy.Dispose()
            If ChaineLue.ToString = "" Then
                Return Nothing
            End If
            TableauObjet = Strings.Split(ChaineLue.ToString, VI.SigneBarre, -1)
            LstComplet = New List(Of String)
            For NoPage = 0 To TableauObjet.Count - 1
                If TableauObjet(NoPage) <> "" Then
                    LstComplet.Add(TableauObjet(NoPage))
                End If
            Next NoPage
            Return LstComplet
        End Function

        Public Function LectureObjet_ToFiches(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, Ide As Integer, SiTri As Boolean, Optional Filtres As List(Of String) = Nothing) As List(Of VIR_FICHE) Implements IServiceServeur.LectureObjet_ToFiches
            Dim LstComplet As List(Of String)
            LstComplet = LectureObjet_ToListeChar(NomUtiSgbd, Pvue, NoObjet, Ide, SiTri, Filtres)
            If LstComplet Is Nothing Then
                Return Nothing
            End If
            Return ListeFichesVirtualia(Pvue, NoObjet, LstComplet)
        End Function

        Public Function LectureObjet_ToListeChar(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, Ide As Integer, SiTri As Boolean, Optional Filtres As List(Of String) = Nothing) As List(Of String) Implements IServiceServeur.LectureObjet_ToListeChar
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim ChaineLue As System.Text.StringBuilder
            Dim ChainePage As String
            Dim LstComplet As List(Of String)
            Dim NoPage As Integer = 1
            Dim TableauObjet(0) As String

            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            ChaineLue = New System.Text.StringBuilder
            Do
                If Filtres Is Nothing Then
                    ChainePage = Proxy.LireUnObjet(NomUtiSgbd, Pvue, NoObjet, Ide, SiTri, NoPage)
                Else
                    ChainePage = Proxy.LireUnObjetFiltre(NomUtiSgbd, Pvue, NoObjet, Filtres.ToArray, SiTri, NoPage)
                End If
                If ChainePage = "" Then
                    Exit Do
                End If
                ChaineLue.Append(ChainePage)
                NoPage += 1
            Loop
            Proxy.Dispose()
            If ChaineLue.ToString = "" Then
                Return Nothing
            End If
            TableauObjet = Strings.Split(ChaineLue.ToString, VI.SigneBarre, -1)
            LstComplet = New List(Of String)
            For NoPage = 0 To TableauObjet.Count - 1
                If TableauObjet(NoPage) <> "" Then
                    LstComplet.Add(TableauObjet(NoPage))
                End If
            Next NoPage
            Return LstComplet
        End Function

        Public Function LireProfilUtilisateurVirtualia(NomUtiSgbd As String, NomGestionnaire As String, NoBd As Integer) As ServiceServeur.UtilisateurType Implements IServiceServeur.LireProfilUtilisateurVirtualia
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim Uti As Virtualia.Net.ServiceServeur.UtilisateurType = Nothing
            Dim Chaine As String = ""
            Dim TableauData(0) As String

            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            Chaine = Proxy.LireProfilUtilisateur(NomUtiSgbd, NomGestionnaire, NoBd, 1)
            Proxy.Dispose()

            If Chaine = "" Then
                Return Nothing
            End If
            TableauData = Strings.Split(Chaine, VI.Tild, -1)
            Uti = New Virtualia.Net.ServiceServeur.UtilisateurType
            If IsNumeric(TableauData(0)) Then
                Uti.Identifiant = CInt(TableauData(0))
            End If
            Uti.Nom = TableauData(1)
            Uti.Prenom = TableauData(2)
            Uti.Initiales = TableauData(3)
            Uti.Nature = TableauData(4)
            Uti.Groupe = TableauData(5)
            Uti.MotPasse = TableauData(6)
            Uti.DateMotPasse = TableauData(7)
            If IsNumeric(TableauData(8)) Then
                Uti.InstanceBd = CInt(TableauData(8))
            End If
            Uti.DateMaj = TableauData(9)
            Uti.Initiales = TableauData(10)

            Return Uti
        End Function

        Public Function ListeUtilisateurs(Nature As String, NoBd As Integer) As List(Of ServiceServeur.UtilisateurType) Implements IServiceServeur.ListeUtilisateurs
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim LstUti As List(Of Virtualia.Net.ServiceServeur.UtilisateurType)
            Dim Uti As Virtualia.Net.ServiceServeur.UtilisateurType = Nothing
            Dim Chaine As String = ""
            Dim TableauObjet(0) As String
            Dim TableauData(0) As String
            Dim LstFiltres As List(Of String)
            Dim IndiceI As Integer

            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            Select Case Nature
                Case "Session"
                    Chaine = Proxy.ListedesUtilisateursEnSession
                Case "Tous"
                    Chaine = Proxy.ListedeTouslesUtilisateurs(NoBd)
            End Select
            Proxy.Dispose()
            If Chaine = "" Then
                Return Nothing
            End If
            LstUti = New List(Of Virtualia.Net.ServiceServeur.UtilisateurType)
            TableauObjet = Strings.Split(Chaine, VI.SigneBarre, -1)
            For Each Ligne In TableauObjet
                If Ligne = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(Ligne, VI.Tild, -1)
                Uti = New Virtualia.Net.ServiceServeur.UtilisateurType
                Uti.Nom = TableauData(0)
                Uti.Prenom = TableauData(1)
                Uti.Groupe = TableauData(2)
                Uti.Nature = TableauData(3)
                If TableauData.Count > 4 Then
                    LstFiltres = New List(Of String)
                    For IndiceI = 4 To TableauData.Count - 1
                        LstFiltres.Add(TableauData(IndiceI))
                    Next IndiceI
                    Uti.FiltreEtablissement = LstFiltres
                End If
                LstUti.Add(Uti)
            Next
            Return LstUti
        End Function

        Public Function MajSqlDirecte(NomUtiSgbd As String, OrdreSql As String) As Boolean Implements IServiceServeur.MajSqlDirecte
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim Cretour As Boolean

            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            Cretour = Proxy.MiseAJourSql(NomUtiSgbd, OrdreSql)
            Proxy.Dispose()

            Return Cretour
        End Function

        Public Function MajSqlDirecte(NomUtiSgbd As String, FluxSql As List(Of String)) As Integer Implements IServiceServeur.MajSqlDirecte
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim IndiceK As Integer
            Dim ChaineFlux As String = ""

            For Each Requete As String In FluxSql
                ChaineFlux &= Requete & VI.SigneBarre
            Next
            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            IndiceK = Proxy.MiseAJourFluxSql(NomUtiSgbd, ChaineFlux)
            Proxy.Dispose()

            Return IndiceK
        End Function

        Public Function MiseAjour_Fiche(NomUtiSgbd As String, PVue As Integer, NoObjet As Integer, Ide As Integer, CodeMaj As String, ValeursLues As String, ValeursMaj As String, Optional SiJournaliser As Boolean = True) As Boolean Implements IServiceServeur.MiseAjour_Fiche
            Dim Cretour As Boolean
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees

            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            Select Case CodeMaj
                Case "C"
                    Cretour = Proxy.InsererUneFiche(NomUtiSgbd, PVue, NoObjet, Ide, ValeursMaj)
                Case "M"
                    Cretour = Proxy.MettreAJourUneFiche(NomUtiSgbd, PVue, NoObjet, Ide, ValeursLues, ValeursMaj)
                Case "S"
                    Cretour = Proxy.SupprimerUneFiche(NomUtiSgbd, PVue, NoObjet, Ide, ValeursLues)
            End Select

            If Cretour = True And SiJournaliser = True Then
                Dim Jrn As Virtualia.TablesObjet.ShemaVUE.VUE_Journal
                Dim TableauData(0) As String
                Jrn = New Virtualia.TablesObjet.ShemaVUE.VUE_Journal(NomUtiSgbd, CodeMaj, PVue, NoObjet, Ide)
                If CodeMaj = "S" Then
                    Jrn.ContenuTable = ValeursLues
                Else
                    Jrn.ContenuTable = ValeursMaj
                End If
                TableauData(0) = Jrn.Enregistrement
                Cretour = Proxy.Journaliser(NomUtiSgbd, TableauData)
            End If

            Proxy.Dispose()

            Return Cretour
        End Function

        Public Function MiseAjour_SqlExterne(NomUtiClient As String, NomUtiSgbd As String, PwSgbdDecrypte As String, FileDsn As String, TypeduSgbd As Integer, Requete As String, Optional FluxRequetes As List(Of String) = Nothing) As Boolean Implements IServiceServeur.MiseAjour_SqlExterne
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim IndiceK As Integer
            Dim Cretour As Boolean

            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            If FluxRequetes Is Nothing Then
                Cretour = Proxy.MiseAJourSqlExterne(NomUtiClient, FileDsn, NomUtiSgbd, PwSgbdDecrypte, TypeduSgbd, Requete)
            Else
                Dim ChaineFlux As System.Text.StringBuilder
                ChaineFlux = New System.Text.StringBuilder
                For Each OrdreSql As String In FluxRequetes
                    ChaineFlux.Append(OrdreSql & VI.SigneBarre)
                Next
                IndiceK = Proxy.MiseAJourSqlFluxExterne(NomUtiClient, FileDsn, NomUtiSgbd, PwSgbdDecrypte, TypeduSgbd, ChaineFlux.ToString)
                If IndiceK = 0 Then
                    Cretour = True
                Else
                    Cretour = False
                End If
            End If
            Proxy.Dispose()

            Return Cretour
        End Function

        Public Function ObtenirUnCompteur(NomUtiSgbd As String, NomTable As String, Categorie As String) As Integer Implements IServiceServeur.ObtenirUnCompteur
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim Cpt As Integer

            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            Cpt = Proxy.ObtenirUnCompteur(NomUtiSgbd, NomTable, Categorie)
            Proxy.Dispose()
            Return Cpt
        End Function

        Public Function ObtenirUnFichierParametre(NomUtiSgbd As String, Categorie As String) As Byte() Implements IServiceServeur.ObtenirUnFichierParametre
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim TableauBytes(0) As Byte

            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            TableauBytes = Proxy.ObtenirFichierXml(NomUtiSgbd, Categorie)
            Proxy.Dispose()
            Return TableauBytes
        End Function

        Public Function OuvertureSession(NomUtiSgbd As String, Pw_Sha As String, NoBd As Integer) As Integer Implements IServiceServeur.OuvertureSession
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim RhOrdi As Virtualia.Systeme.Outils.Ordinateur
            Dim Cretour As Integer

            RhOrdi = New Virtualia.Systeme.Outils.Ordinateur
            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            Cretour = Proxy.OuvertureSession(NomUtiSgbd, Pw_Sha, RhOrdi.NomdelOrdinateur, RhOrdi.AdresseIPOrdinateur, NoBd)
            Proxy.Dispose()
            Return Cretour
        End Function

        Public Function RequeteSql_ToFiches(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, OrdreSql As String) As List(Of VIR_FICHE) Implements IServiceServeur.RequeteSql_ToFiches
            Dim LstComplet As List(Of String)
            LstComplet = RequeteSql_ToListeChar(NomUtiSgbd, Pvue, NoObjet, OrdreSql)
            If LstComplet Is Nothing Then
                Return Nothing
            End If
            Return ListeFichesVirtualia(Pvue, NoObjet, LstComplet)
        End Function

        Public Function RequeteSql_ToListeChar(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, OrdreSql As String) As List(Of String) Implements IServiceServeur.RequeteSql_ToListeChar
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim ChaineLue As System.Text.StringBuilder
            Dim ChainePage As String
            Dim LstComplet As List(Of String)
            Dim NoPage As Integer = 1
            Dim TableauObjet(0) As String

            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            ChaineLue = New System.Text.StringBuilder
            Do
                ChainePage = Proxy.SelectionSql(NomUtiSgbd, Pvue, NoObjet, OrdreSql, NoPage)
                If ChainePage = "" Then
                    Exit Do
                End If
                ChaineLue.Append(ChainePage)
                NoPage += 1
            Loop
            Proxy.Dispose()
            If ChaineLue.ToString = "" Then
                Return Nothing
            End If
            TableauObjet = Strings.Split(ChaineLue.ToString, VI.SigneBarre, -1)
            LstComplet = New List(Of String)
            For NoPage = 0 To TableauObjet.Count - 1
                If TableauObjet(NoPage) <> "" Then
                    LstComplet.Add(TableauObjet(NoPage))
                End If
            Next NoPage
            Return LstComplet
        End Function

        Public Function RequeteSql_ToListeType(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, OrdreSql As String) As List(Of ServiceServeur.VirRequeteType) Implements IServiceServeur.RequeteSql_ToListeType
            Dim LstRequete As List(Of String)
            LstRequete = RequeteSql_ToListeChar(NomUtiSgbd, Pvue, NoObjet, OrdreSql)
            If LstRequete Is Nothing Then
                Return Nothing
            End If
            If LstRequete.Count = 0 Then
                Return Nothing
            End If
            Dim IndiceI As Integer
            Dim FicheRes As Virtualia.Net.ServiceServeur.VirRequeteType
            Dim LstResultat As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim LstValeurs As List(Of String)
            Dim TableauData(0) As String

            LstResultat = New List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            For Each Ligne As String In LstRequete
                TableauData = Strings.Split(Ligne, VI.Tild, -1)
                FicheRes = New Virtualia.Net.ServiceServeur.VirRequeteType
                FicheRes.PointdeVue = Pvue
                FicheRes.NumObjet = NoObjet
                If IsNumeric(TableauData(0)) Then
                    FicheRes.Ide_Dossier = CInt(TableauData(0))
                End If
                LstValeurs = New List(Of String)
                If TableauData.Count = 1 Then
                    LstValeurs.Add(TableauData(0))
                Else
                    For IndiceI = 1 To TableauData.Count - 2
                        LstValeurs.Add(TableauData(IndiceI))
                    Next
                End If
                FicheRes.Valeurs = LstValeurs
                LstResultat.Add(FicheRes)
            Next
            Return LstResultat
        End Function

        Public Function RequeteSqlExterne_ToListeChar(NomUtiClient As String, NomUtiSgbd As String, PwSgbdDecrypte As String, FileDsn As String, TypeduSgbd As Integer, Requete As String) As List(Of String) Implements IServiceServeur.RequeteSqlExterne_ToListeChar
            Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
            Dim ChainePage As String
            Dim LstComplet As List(Of String)
            Dim IndiceI As Integer
            Dim TableauObjet(0) As String

            Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
            ChainePage = Proxy.SelectionSqlExterne(NomUtiClient, FileDsn, NomUtiSgbd, PwSgbdDecrypte, TypeduSgbd, Requete)
            Proxy.Dispose()
            If ChainePage = "" Then
                Return Nothing
            End If
            TableauObjet = Strings.Split(ChainePage, VI.SigneBarre, -1)
            LstComplet = New List(Of String)
            For IndiceI = 0 To TableauObjet.Count - 1
                If TableauObjet(IndiceI) <> "" Then
                    LstComplet.Add(TableauObjet(IndiceI))
                End If
            Next IndiceI
            Return LstComplet
        End Function

        Public Function RequeteSqlExterne_ToListeType(NomUtiClient As String, NomUtiSgbd As String, PwSgbdDecrypte As String, FileDsn As String, TypeduSgbd As Integer, Requete As String) As List(Of ServiceServeur.VirRequeteType) Implements IServiceServeur.RequeteSqlExterne_ToListeType
            Dim LstRequete As List(Of String)
            LstRequete = RequeteSqlExterne_ToListeChar(NomUtiClient, NomUtiSgbd, PwSgbdDecrypte, FileDsn, TypeduSgbd, Requete)
            If LstRequete Is Nothing Then
                Return Nothing
            End If
            If LstRequete.Count = 0 Then
                Return Nothing
            End If
            Dim IndiceI As Integer
            Dim FicheRes As Virtualia.Net.ServiceServeur.VirRequeteType
            Dim LstResultat As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim LstValeurs As List(Of String)
            Dim TableauData(0) As String

            LstResultat = New List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            For Each Ligne As String In LstRequete
                TableauData = Strings.Split(Ligne, VI.Tild, -1)
                FicheRes = New Virtualia.Net.ServiceServeur.VirRequeteType
                FicheRes.PointdeVue = 0
                FicheRes.NumObjet = 0
                If IsNumeric(TableauData(0)) Then
                    FicheRes.Ide_Dossier = CInt(TableauData(0))
                End If
                LstValeurs = New List(Of String)
                If TableauData.Count = 1 Then
                    LstValeurs.Add(TableauData(0))
                Else
                    For IndiceI = 1 To TableauData.Count - 1
                        LstValeurs.Add(TableauData(IndiceI))
                    Next
                End If
                FicheRes.Valeurs = LstValeurs
                LstResultat.Add(FicheRes)
            Next
            Return LstResultat
        End Function

        Private ReadOnly Property ListeFichesVirtualia(ByVal PointdeVue As Integer, ByVal NumObjet As Integer, ByVal ListeResultats As List(Of String)) As List(Of VIR_FICHE)
            Get
                If ListeResultats Is Nothing Then
                    Return Nothing
                End If
                If ListeResultats.Count = 0 Then
                    Return Nothing
                End If
                Dim GenericConstructeur As Virtualia.Systeme.IVConstructeur
                Dim IndiceI As Integer
                Dim LstFiches As List(Of VIR_FICHE)
                Dim FicheVirtualia As VIR_FICHE
                Dim LstValeurs As List(Of String)
                Dim Ide_dossier As Integer
                Dim TableauData(0) As String

                LstFiches = New List(Of VIR_FICHE)
                Select Case PointdeVue
                    Case VI.PointdeVue.PVueApplicatif
                        GenericConstructeur = New Virtualia.TablesObjet.ShemaPER.VConstructeur(WsTypeModele)
                        GenericConstructeur.V_Parametre = WsSpecificite
                    Case VI.PointdeVue.PVueContextesJour, VI.PointdeVue.PVueVueLogique
                        GenericConstructeur = New Virtualia.TablesObjet.ShemaVUE.VConstructeur(PointdeVue)
                    Case Else
                        GenericConstructeur = New Virtualia.TablesObjet.ShemaREF.VConstructeur(WsTypeModele, PointdeVue)
                        GenericConstructeur.V_Parametre = WsSpecificite
                End Select
                For Each Ligne As String In ListeResultats
                    TableauData = Strings.Split(Ligne, VI.Tild, -1)
                    LstValeurs = New List(Of String)
                    Ide_dossier = CInt(TableauData(0))
                    For IndiceI = 1 To TableauData.Count - 1
                        LstValeurs.Add(TableauData(IndiceI))
                    Next IndiceI
                    FicheVirtualia = GenericConstructeur.V_NouvelleFiche(NumObjet, Ide_dossier, LstValeurs)
                    If FicheVirtualia IsNot Nothing Then
                        LstFiches.Add(FicheVirtualia)
                    End If
                Next
                Return LstFiches
            End Get
        End Property

        Public ReadOnly Property IPServeur As String Implements IServiceServeur.IPServeur
            Get
                Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
                Dim Chaine As String

                Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
                Chaine = Proxy.IPServeur
                Proxy.Dispose()
                Return Chaine
            End Get
        End Property

        Public ReadOnly Property NomDNSServeur As String Implements IServiceServeur.NomDNSServeur
            Get
                Dim Proxy As Virtualia.Net.WebService.ServeurDonnees
                Dim Chaine As String

                Proxy = New Virtualia.Net.WebService.ServeurDonnees(WsUrlWebServeur)
                Chaine = Proxy.NomDNSServeur
                Proxy.Dispose()
                Return Chaine
            End Get
        End Property

        '************** Pour la constitution des liste VIR_FICHE
        Public Sub New(ByVal UrlWebService As String, ByVal TypeduModele As Integer, Optional ByVal Specifique As Integer = 0)
            WsUrlWebServeur = UrlWebService
            WsTypeModele = TypeduModele
            WsSpecificite = Specifique
        End Sub

        Public Sub New(ByVal UrlWebService As String)
            WsUrlWebServeur = UrlWebService
        End Sub

    End Class
End Namespace