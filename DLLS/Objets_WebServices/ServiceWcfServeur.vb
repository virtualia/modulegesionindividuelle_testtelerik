﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Net.ServiceServeur
Imports Virtualia.Systeme.MetaModele

Namespace WebService
    Public Class ServiceWcfServeur
        Implements WebService.IServiceServeur
        Private WsTypeModele As Integer = VI.ModeleRh.FPEtat
        Private WsSpecificite As Integer = 0

        Public Function ChangerBasedeDonneesCourante(NomUtiSgbd As String, NoBd As Integer) As Boolean Implements IServiceServeur.ChangerBasedeDonneesCourante
            Dim WcfWebService As VirtualiaServeurClient
            Dim Cretour As Boolean
            WcfWebService = New VirtualiaServeurClient
            Cretour = WcfWebService.ChangerlaBasedeDonnees(NomUtiSgbd, NoBd)
            WcfWebService.Close()
            Return Cretour
        End Function

        Public Function FermetureSession(NomUtiSgbd As String) As Boolean Implements IServiceServeur.FermetureSession
            Dim WcfWebService As VirtualiaServeurClient
            Dim Cretour As Boolean

            WcfWebService = New VirtualiaServeurClient
            Cretour = WcfWebService.FermetureSession(NomUtiSgbd)
            WcfWebService.Close()
            Return Cretour
        End Function

        Public ReadOnly Property InformationLogiciel() As ProduitType Implements IServiceServeur.InformationLogiciel
            Get
                Dim WcfService As VirtualiaServeurClient
                Dim InfoProduit As ProduitType

                WcfService = New VirtualiaServeurClient
                InfoProduit = WcfService.InformationsProduit
                WcfService.Close()
                Return InfoProduit
            End Get
        End Property

        Public Function Inserer_Lot_Fiches(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, Ide As Integer, ValeursMaj As String, Optional SiJournaliser As Boolean = True) As Integer Implements IServiceServeur.Inserer_Lot_Fiches
            Dim WcfWebService As VirtualiaServeurClient
            Dim CmdSqlVirtualia As SqlVirtualiaType
            Dim IndiceK As Integer

            CmdSqlVirtualia = New SqlVirtualiaType
            CmdSqlVirtualia.Utilisateur = NomUtiSgbd
            CmdSqlVirtualia.PointdeVue = Pvue
            CmdSqlVirtualia.NumObjet = NoObjet
            CmdSqlVirtualia.Identifiant = Ide
            CmdSqlVirtualia.ValeursNew = ValeursMaj

            WcfWebService = New VirtualiaServeurClient
            IndiceK = WcfWebService.InsererLotFiches(CmdSqlVirtualia)

            If IndiceK = 0 And SiJournaliser = True Then
                Dim Jrn As Virtualia.TablesObjet.ShemaVUE.VUE_Journal
                Dim LstJrn As New List(Of String)
                Dim IndiceI As Integer
                Dim TableauData(0) As String
                Dim Cretour As Boolean
                Dim IndiceJ As Integer
                Dim LstSimple As List(Of String)

                TableauData = Strings.Split(ValeursMaj, VI.SigneBarre, -1)
                For IndiceI = 0 To TableauData.Count - 1
                    Jrn = New Virtualia.TablesObjet.ShemaVUE.VUE_Journal(NomUtiSgbd, "C", Pvue, NoObjet, Ide)
                    Jrn.ContenuTable = ValeursMaj
                    LstJrn.Add(Jrn.Enregistrement)
                    If IndiceI Mod 20 = 0 Then
                        Try
                            Cretour = WcfWebService.Journaliser(NomUtiSgbd, LstJrn)
                        Catch ex1 As Exception
                            For IndiceJ = 0 To LstJrn.Count - 1
                                LstSimple = New List(Of String)
                                LstSimple.Add(LstJrn(IndiceJ))
                                Cretour = WcfWebService.Journaliser(NomUtiSgbd, LstSimple)
                            Next IndiceJ
                        End Try
                        LstJrn = New List(Of String)
                    End If
                Next IndiceI
                If LstJrn.Count > 0 Then
                    Try
                        Cretour = WcfWebService.Journaliser(NomUtiSgbd, LstJrn)
                    Catch ex2 As Exception
                        For IndiceJ = 0 To LstJrn.Count - 1
                            LstSimple = New List(Of String)
                            LstSimple.Add(LstJrn(IndiceJ))
                            Cretour = WcfWebService.Journaliser(NomUtiSgbd, LstSimple)
                        Next IndiceJ
                    End Try
                End If
            End If

            WcfWebService.Close()

            Return IndiceK
        End Function

        Public ReadOnly Property Instance_ConfigV4 As Systeme.Configuration.FichierConfig Implements IServiceServeur.Instance_ConfigV4
            Get
                Dim WcfWebService As VirtualiaServeurClient
                Dim InstanceConfig As Systeme.Configuration.FichierConfig
                Dim NomFicXml As String = VI.DossierVirtualiaService("Parametrage") & "VirtualiaConfig.Xml"
                Dim TabChaine As Byte()

                InstanceConfig = New Systeme.Configuration.FichierConfig
                If InstanceConfig.ListeSections IsNot Nothing Then
                    Return InstanceConfig
                End If
                WcfWebService = New VirtualiaServeurClient
                TabChaine = WcfWebService.ObtenirFichierXml("Virtualia", "Config.Xml")
                WcfWebService.Close()
                If TabChaine Is Nothing Then
                    Return InstanceConfig
                End If
                InstanceConfig = New Systeme.Configuration.FichierConfig
                Return InstanceConfig
            End Get
        End Property

        Public ReadOnly Property Instance_Database() As Systeme.Parametrage.BasesdeDonnees.ParamSgbd Implements IServiceServeur.Instance_Database
            Get
                Dim WcfWebService As VirtualiaServeurClient
                Dim LstWcfBases As List(Of BasedeDonneesType)

                WcfWebService = New VirtualiaServeurClient
                LstWcfBases = WcfWebService.ListedesDatabases
                WcfWebService.Close()

                If LstWcfBases Is Nothing Then
                    Return Nothing
                End If

                Dim InstanceClientBd As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbd
                Dim LstClientBd As List(Of Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdDatabase)
                Dim ItemClientBd As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdDatabase
                Dim ItemClientCnx As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdConnexion
                Dim ICnx As Integer

                LstClientBd = New List(Of Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdDatabase)
                For Each Database As BasedeDonneesType In LstWcfBases
                    ItemClientBd = New Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdDatabase
                    ItemClientBd.Numero = Database.Numero
                    ItemClientBd.Nom = Database.Nom_Alias
                    ItemClientBd.Intitule = Database.Intitule
                    ItemClientBd.TypeduSgbdAlpha = Database.Typegbd
                    ItemClientBd.FormatdesDatesAlpha = Database.FormatdesDates
                    ItemClientBd.SymboleDecimal = Database.SymboleDecimal
                    ItemClientBd.Schema = Database.Schema
                    ItemClientBd.SiTraceSql = Database.SiTraceSql
                    LstClientBd.Add(ItemClientBd)

                    For Each CnxAdo As ConnexionODBCType In Database.Connexions
                        ItemClientCnx = New Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdConnexion(ItemClientBd)
                        ItemClientCnx.DsnODBC = CnxAdo.FileDsn
                        ItemClientCnx.Provider = CnxAdo.ProviderOleDb
                        ItemClientCnx.PrecisionSurConnexion = CnxAdo.PrecisionCnx
                        ItemClientCnx.Serveur = CnxAdo.ServeurSgbDb
                        ItemClientCnx.Utilisateur = CnxAdo.UtiSgbd
                        ItemClientCnx.Password = CnxAdo.PwSgbd
                        ItemClientCnx.TypeCnx = CnxAdo.TypeCnx
                        ICnx = ItemClientBd.CreerUneConnexion()
                        ItemClientBd(ICnx) = ItemClientCnx
                    Next
                Next

                InstanceClientBd = New Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbd(LstClientBd)

                Return InstanceClientBd
            End Get
        End Property

        Public ReadOnly Property Instance_ExperteRH() As Expertes.ExpertesRH Implements IServiceServeur.Instance_ExperteRH
            Get
                Dim WcfWebService As VirtualiaServeurClient
                Dim LstPvues As List(Of PointdeVueExpertType)

                WcfWebService = New VirtualiaServeurClient
                LstPvues = WcfWebService.ModeleExpert
                WcfWebService.Close()

                If LstPvues Is Nothing Then
                    Return Nothing
                End If

                Dim InstanceModele As Virtualia.Systeme.MetaModele.Expertes.ExpertesRH
                Dim LstClientPvue As List(Of Virtualia.Systeme.MetaModele.Expertes.PointdevueExpert)
                Dim ItemClientPvue As Virtualia.Systeme.MetaModele.Expertes.PointdevueExpert
                Dim ItemClientObjet As Virtualia.Systeme.MetaModele.Expertes.ObjetExpert
                Dim ItemClientInfo As Virtualia.Systeme.MetaModele.Expertes.InformationExperte
                Dim IGen As Integer

                LstClientPvue = New List(Of Virtualia.Systeme.MetaModele.Expertes.PointdevueExpert)
                For Each PtdeVue As PointdeVueExpertType In LstPvues
                    ItemClientPvue = New Virtualia.Systeme.MetaModele.Expertes.PointdevueExpert
                    ItemClientPvue.Numero = PtdeVue.Numero
                    ItemClientPvue.Intitule = PtdeVue.Intitule

                    LstClientPvue.Add(ItemClientPvue)

                    For Each DicoObjet As ObjetExpertType In PtdeVue.Objets
                        ItemClientObjet = New Virtualia.Systeme.MetaModele.Expertes.ObjetExpert(ItemClientPvue)
                        ItemClientObjet.PointdeVue = DicoObjet.PointdeVue
                        ItemClientObjet.Numero = DicoObjet.Numero
                        IGen = ItemClientPvue.CreerUnObjet
                        ItemClientPvue(IGen) = ItemClientObjet

                        For Each DicoInfo In DicoObjet.Informations
                            ItemClientInfo = New Virtualia.Systeme.MetaModele.Expertes.InformationExperte(ItemClientObjet)
                            ItemClientInfo.PointdeVue = DicoInfo.PointdeVue
                            ItemClientInfo.Objet = DicoInfo.NumObjet
                            ItemClientInfo.Numero = DicoInfo.Numero
                            ItemClientInfo.Intitule = DicoInfo.Intitule
                            ItemClientInfo.VNature = DicoInfo.VNature
                            ItemClientInfo.Format = DicoInfo.VFormat
                            ItemClientInfo.CategorieDLL = DicoInfo.CategorieDLL
                            ItemClientInfo.Algorithme = DicoInfo.Algorithme
                            ItemClientInfo.FormeduResultat = DicoInfo.FormeResultat
                            ItemClientInfo.Exploitation = DicoInfo.FormeExploitation
                            For IGen = 0 To DicoInfo.InfoReference.Count - 1
                                ItemClientInfo.InfoReference(IGen) = DicoInfo.InfoReference(IGen)
                            Next IGen
                            For IGen = 0 To DicoInfo.ZoneEdition.Count - 1
                                ItemClientInfo.ZoneEdition(IGen) = DicoInfo.ZoneEdition(IGen)
                            Next IGen
                            ItemClientInfo.Description = DicoInfo.Description
                            IGen = ItemClientObjet.CreerUneInformation
                            ItemClientObjet(IGen) = ItemClientInfo
                        Next
                    Next
                Next
                InstanceModele = New Virtualia.Systeme.MetaModele.Expertes.ExpertesRH(LstClientPvue)

                Return InstanceModele
            End Get
        End Property

        Public ReadOnly Property Instance_ModeleRH() As Donnees.ModeleRH Implements IServiceServeur.Instance_ModeleRH
            Get
                Dim WcfWebService As VirtualiaServeurClient
                Dim LstPvues As List(Of PointdeVueApplicatifType)
                Dim InfoProduit As ProduitType

                WcfWebService = New VirtualiaServeurClient
                LstPvues = WcfWebService.ModeleApplicatif
                InfoProduit = WcfWebService.InformationsProduit
                WcfWebService.Close()

                If LstPvues Is Nothing Then
                    Return Nothing
                End If

                Dim InstanceModele As Virtualia.Systeme.MetaModele.Donnees.ModeleRH
                Dim ItemClientLicence As Virtualia.Systeme.MetaModele.Donnees.Produit
                Dim LstClientPvue As List(Of Virtualia.Systeme.MetaModele.Donnees.PointdeVue)
                Dim ItemClientPvue As Virtualia.Systeme.MetaModele.Donnees.PointdeVue
                Dim ItemClientObjet As Virtualia.Systeme.MetaModele.Donnees.Objet
                Dim ItemClientInfo As Virtualia.Systeme.MetaModele.Donnees.Information
                Dim IGen As Integer

                LstClientPvue = New List(Of Virtualia.Systeme.MetaModele.Donnees.PointdeVue)
                For Each PtdeVue As PointdeVueApplicatifType In LstPvues
                    ItemClientPvue = New Virtualia.Systeme.MetaModele.Donnees.PointdeVue
                    ItemClientPvue.Numero = PtdeVue.Numero
                    ItemClientPvue.Intitule = PtdeVue.Intitule

                    LstClientPvue.Add(ItemClientPvue)

                    For Each DicoObjet As ObjetApplicatifType In PtdeVue.Objets
                        ItemClientObjet = New Virtualia.Systeme.MetaModele.Donnees.Objet(ItemClientPvue)
                        ItemClientObjet.PointdeVue = DicoObjet.PointdeVue
                        ItemClientObjet.Numero = DicoObjet.Numero
                        ItemClientObjet.Intitule = DicoObjet.Intitule
                        ItemClientObjet.VNature = DicoObjet.VNature
                        ItemClientObjet.VStructure = DicoObjet.VStructure
                        ItemClientObjet.LimiteMaximum = DicoObjet.Limite
                        ItemClientObjet.PositionDateFin = DicoObjet.PositionDateFin
                        ItemClientObjet.NomTableSGBDR = DicoObjet.NomTableSGBDR
                        ItemClientObjet.CategorieRH = DicoObjet.Categorie
                        ItemClientObjet.SiCertification = DicoObjet.SiCertifierTransacion
                        IGen = ItemClientPvue.CreerUnObjet
                        ItemClientPvue(IGen) = ItemClientObjet

                        For Each DicoInfo As InformationApplicatifType In DicoObjet.Informations
                            ItemClientInfo = New Virtualia.Systeme.MetaModele.Donnees.Information(ItemClientObjet)
                            ItemClientInfo.PointdeVue = DicoInfo.PointdeVue
                            ItemClientInfo.Objet = DicoInfo.NumObjet
                            ItemClientInfo.Numero = DicoInfo.Numero
                            ItemClientInfo.Intitule = DicoInfo.Intitule
                            ItemClientInfo.VNature = DicoInfo.VNature
                            ItemClientInfo.Format = DicoInfo.VFormat
                            ItemClientInfo.IndexSecondaire = DicoInfo.IndexSecondaire
                            ItemClientInfo.Classement = DicoInfo.Classement
                            ItemClientInfo.RegleUnicite = DicoInfo.RegleUnicite
                            ItemClientInfo.NomInterne = DicoInfo.NomInterne
                            ItemClientInfo.Etiquette = DicoInfo.Etiquette
                            ItemClientInfo.CadrageLabel = DicoInfo.CadrageEti
                            ItemClientInfo.PointdeVueInverse = DicoInfo.Pvue_Reference
                            ItemClientInfo.TabledeReference = DicoInfo.NomTable_Reference
                            ItemClientInfo.ValeurDefaut = DicoInfo.ValeurDefaut
                            ItemClientInfo.LongueurMaxi = DicoInfo.LongueurMaxi
                            ItemClientInfo.LongueurSGBDR = DicoInfo.LongueurSgbdr
                            For IGen = 0 To DicoInfo.ZoneEdition.Count - 1
                                ItemClientInfo.ZoneEdition(IGen) = DicoInfo.ZoneEdition(IGen)
                            Next IGen
                            IGen = ItemClientObjet.CreerUneInformation
                            ItemClientObjet(IGen) = ItemClientInfo
                        Next
                    Next
                Next

                InstanceModele = New Virtualia.Systeme.MetaModele.Donnees.ModeleRH(LstClientPvue)

                ItemClientLicence = New Virtualia.Systeme.MetaModele.Donnees.Produit
                ItemClientLicence.ClefCryptage = InfoProduit.ClefCryptage
                ItemClientLicence.ClefModele = InfoProduit.TypeModele
                ItemClientLicence.ClefProduit = InfoProduit.ClefLicence
                ItemClientLicence.Intranet = InfoProduit.SiIntranetRH
                ItemClientLicence.NombreAcces = InfoProduit.NombreLicences
                ItemClientLicence.NomModele = InfoProduit.Modele
                ItemClientLicence.NomProduit = InfoProduit.Nom
                ItemClientLicence.NumeroLicence = InfoProduit.NumeroLicence
                ItemClientLicence.NumVersion = InfoProduit.NumeroVersion
                ItemClientLicence.Organisme = InfoProduit.Organisme
                ItemClientLicence.SiteWeb = InfoProduit.SiteWeb

                InstanceModele.InstanceProduit = ItemClientLicence

                Return InstanceModele
            End Get
        End Property

        Public Function Journaliser_Maj(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, Ide As Integer, CodeMaj As String, ContenuFiche As String) As Boolean Implements IServiceServeur.Journaliser_Maj
            Dim WcfWebService As VirtualiaServeurClient
            Dim Jrn As Virtualia.TablesObjet.ShemaVUE.VUE_Journal
            Dim LstJrn As New List(Of String)
            Dim Cretour As Boolean
            Jrn = New Virtualia.TablesObjet.ShemaVUE.VUE_Journal(NomUtiSgbd, CodeMaj, Pvue, NoObjet, Ide)
            LstJrn.Add(ContenuFiche)
            WcfWebService = New VirtualiaServeurClient
            Cretour = WcfWebService.Journaliser(NomUtiSgbd, LstJrn)
            WcfWebService.Close()
            Return Cretour
        End Function

        Public Function LectureDossier_ToFiches(NomUtiSgbd As String, Pvue As Integer, Ide As Integer, SiTri As Boolean, ListeNoObjets As List(Of Integer)) As List(Of VIR_FICHE) Implements IServiceServeur.LectureDossier_ToFiches
            Dim WcfWebService As VirtualiaServeurClient
            Dim CmdSqlVirtualia As SqlVirtualiaType
            Dim LstComplet As List(Of VirRequeteType)

            CmdSqlVirtualia = New SqlVirtualiaType
            CmdSqlVirtualia.Utilisateur = NomUtiSgbd
            CmdSqlVirtualia.PointdeVue = Pvue
            CmdSqlVirtualia.Identifiant = Ide
            CmdSqlVirtualia.SiTri = SiTri

            WcfWebService = New VirtualiaServeurClient
            LstComplet = WcfWebService.Lire_Dossier(CmdSqlVirtualia, ListeNoObjets)
            WcfWebService.Close()

            Return ListeFichesVirtualia(Pvue, LstComplet)
        End Function

        Public Function LectureDossier_ToListeChar(NomUtiSgbd As String, Pvue As Integer, Ide As Integer, SiTri As Boolean, ListeNoObjets As List(Of Integer)) As List(Of String) Implements IServiceServeur.LectureDossier_ToListeChar
            Dim WcfWebService As VirtualiaServeurClient
            Dim CmdSqlVirtualia As SqlVirtualiaType
            Dim LstComplet As List(Of String)

            CmdSqlVirtualia = New SqlVirtualiaType
            CmdSqlVirtualia.Utilisateur = NomUtiSgbd
            CmdSqlVirtualia.PointdeVue = Pvue
            CmdSqlVirtualia.Identifiant = Ide
            CmdSqlVirtualia.SiTri = SiTri

            WcfWebService = New VirtualiaServeurClient
            LstComplet = WcfWebService.LireUnDossier(CmdSqlVirtualia, ListeNoObjets)
            WcfWebService.Close()

            Return LstComplet
        End Function

        Public Function LectureObjet_Plage_ToFiches(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, SiTri As Boolean, IdeDebut As Integer, IdeFin As Integer) As List(Of VIR_FICHE) Implements IServiceServeur.LectureObjet_Plage_ToFiches
            Dim WcfWebService As VirtualiaServeurClient
            Dim CmdSqlVirtualia As SqlVirtualiaType
            Dim LstComplet As List(Of VirRequeteType)
            Dim LstPage As List(Of VirRequeteType)
            Dim NoPage As Integer = 1

            CmdSqlVirtualia = New SqlVirtualiaType
            CmdSqlVirtualia.Utilisateur = NomUtiSgbd
            CmdSqlVirtualia.PointdeVue = Pvue
            CmdSqlVirtualia.NumObjet = NoObjet
            CmdSqlVirtualia.SiTri = SiTri

            WcfWebService = New VirtualiaServeurClient
            LstComplet = New List(Of VirRequeteType)
            Do
                CmdSqlVirtualia.NumerodePage = NoPage
                LstPage = WcfWebService.Lire_ObjetPlage(CmdSqlVirtualia, IdeDebut, IdeFin)
                If LstPage Is Nothing Then
                    Exit Do
                End If
                LstComplet.AddRange(LstPage)
                NoPage += 1
            Loop
            WcfWebService.Close()

            Return ListeFichesVirtualia(Pvue, LstComplet)
        End Function

        Public Function LectureObjet_Plage_ToListeChar(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, SiTri As Boolean, IdeDebut As Integer, IdeFin As Integer) As List(Of String) Implements IServiceServeur.LectureObjet_Plage_ToListeChar
            Dim WcfWebService As VirtualiaServeurClient
            Dim CmdSqlVirtualia As SqlVirtualiaType
            Dim LstComplet As List(Of String)
            Dim LstPage As List(Of String)
            Dim NoPage As Integer = 1

            CmdSqlVirtualia = New SqlVirtualiaType
            CmdSqlVirtualia.Utilisateur = NomUtiSgbd
            CmdSqlVirtualia.PointdeVue = Pvue
            CmdSqlVirtualia.NumObjet = NoObjet
            CmdSqlVirtualia.SiTri = SiTri

            WcfWebService = New VirtualiaServeurClient
            LstComplet = New List(Of String)
            Do
                CmdSqlVirtualia.NumerodePage = NoPage
                LstPage = WcfWebService.LireUnObjetPlage(CmdSqlVirtualia, IdeDebut, IdeFin)
                If LstPage Is Nothing Then
                    Exit Do
                End If
                LstComplet.AddRange(LstPage)
                NoPage += 1
            Loop
            WcfWebService.Close()

            Return LstComplet
        End Function

        Public Function LectureObjet_Plus_ToFiches(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, SiTri As Boolean, ListeIde As List(Of Integer)) As List(Of VIR_FICHE) Implements IServiceServeur.LectureObjet_Plus_ToFiches
            Dim WcfWebService As VirtualiaServeurClient
            Dim CmdSqlVirtualia As SqlVirtualiaType
            Dim LstComplet As List(Of VirRequeteType)
            Dim LstPage As List(Of VirRequeteType)
            Dim NoPage As Integer = 1

            CmdSqlVirtualia = New SqlVirtualiaType
            CmdSqlVirtualia.Utilisateur = NomUtiSgbd
            CmdSqlVirtualia.PointdeVue = Pvue
            CmdSqlVirtualia.NumObjet = NoObjet
            CmdSqlVirtualia.SiTri = SiTri

            WcfWebService = New VirtualiaServeurClient
            LstComplet = New List(Of VirRequeteType)
            Do
                CmdSqlVirtualia.NumerodePage = NoPage
                LstPage = WcfWebService.Lire_ObjetPlus(CmdSqlVirtualia, ListeIde)
                If LstPage Is Nothing Then
                    Exit Do
                End If
                LstComplet.AddRange(LstPage)
                NoPage += 1
            Loop
            WcfWebService.Close()

            Return ListeFichesVirtualia(Pvue, LstComplet)
        End Function

        Public Function LectureObjet_Plus_ToListeChar(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, SiTri As Boolean, ListeIde As List(Of Integer)) As List(Of String) Implements IServiceServeur.LectureObjet_Plus_ToListeChar
            Dim WcfWebService As VirtualiaServeurClient
            Dim CmdSqlVirtualia As SqlVirtualiaType
            Dim LstComplet As List(Of String)
            Dim LstPage As List(Of String)
            Dim NoPage As Integer = 1

            CmdSqlVirtualia = New SqlVirtualiaType
            CmdSqlVirtualia.Utilisateur = NomUtiSgbd
            CmdSqlVirtualia.PointdeVue = Pvue
            CmdSqlVirtualia.NumObjet = NoObjet
            CmdSqlVirtualia.SiTri = SiTri

            WcfWebService = New VirtualiaServeurClient
            LstComplet = New List(Of String)
            Do
                CmdSqlVirtualia.NumerodePage = NoPage
                LstPage = WcfWebService.LireUnObjetPlus(CmdSqlVirtualia, ListeIde)
                If LstPage Is Nothing Then
                    Exit Do
                End If
                LstComplet.AddRange(LstPage)
                NoPage += 1
            Loop
            WcfWebService.Close()

            Return LstComplet
        End Function

        Public Function LectureObjet_ToFiches(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, Ide As Integer, SiTri As Boolean, Optional Filtres As List(Of String) = Nothing) As List(Of VIR_FICHE) Implements IServiceServeur.LectureObjet_ToFiches
            Dim WcfWebService As VirtualiaServeurClient
            Dim CmdSqlVirtualia As SqlVirtualiaType
            Dim LstComplet As List(Of VirRequeteType)
            Dim LstPage As List(Of VirRequeteType)
            Dim NoPage As Integer = 1

            CmdSqlVirtualia = New SqlVirtualiaType
            CmdSqlVirtualia.Utilisateur = NomUtiSgbd
            CmdSqlVirtualia.PointdeVue = Pvue
            CmdSqlVirtualia.NumObjet = NoObjet
            CmdSqlVirtualia.Identifiant = Ide
            CmdSqlVirtualia.SiTri = SiTri

            WcfWebService = New VirtualiaServeurClient
            LstComplet = New List(Of VirRequeteType)
            Do
                CmdSqlVirtualia.NumerodePage = NoPage
                If Filtres Is Nothing Then
                    LstPage = WcfWebService.Lire_Objet(CmdSqlVirtualia)
                Else
                    LstPage = WcfWebService.Lire_ObjetFiltre(CmdSqlVirtualia, Filtres)
                End If
                If LstPage Is Nothing Then
                    Exit Do
                End If
                LstComplet.AddRange(LstPage)
                NoPage += 1
            Loop
            WcfWebService.Close()

            Return ListeFichesVirtualia(Pvue, LstComplet)
        End Function

        Public Function LectureObjet_ToListeChar(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, Ide As Integer, SiTri As Boolean, Optional Filtres As List(Of String) = Nothing) As List(Of String) Implements IServiceServeur.LectureObjet_ToListeChar
            Dim WcfWebService As VirtualiaServeurClient
            Dim CmdSqlVirtualia As SqlVirtualiaType
            Dim LstComplet As List(Of String)
            Dim LstPage As List(Of String)
            Dim NoPage As Integer = 1

            CmdSqlVirtualia = New SqlVirtualiaType
            CmdSqlVirtualia.Utilisateur = NomUtiSgbd
            CmdSqlVirtualia.PointdeVue = Pvue
            CmdSqlVirtualia.NumObjet = NoObjet
            CmdSqlVirtualia.Identifiant = Ide
            CmdSqlVirtualia.SiTri = SiTri

            WcfWebService = New VirtualiaServeurClient
            LstComplet = New List(Of String)
            Do
                CmdSqlVirtualia.NumerodePage = NoPage
                If Filtres Is Nothing Then
                    LstPage = WcfWebService.LireUnObjet(CmdSqlVirtualia)
                Else
                    LstPage = WcfWebService.LireUnObjetFiltre(CmdSqlVirtualia, Filtres)
                End If
                If LstPage Is Nothing Then
                    Exit Do
                End If
                LstComplet.AddRange(LstPage)
                NoPage += 1
            Loop
            WcfWebService.Close()

            Return LstComplet
        End Function

        Public Function LireProfilUtilisateurVirtualia(NomUtiSgbd As String, NomGestionnaire As String, NoBd As Integer) As UtilisateurType Implements IServiceServeur.LireProfilUtilisateurVirtualia
            Dim WcfWebService As VirtualiaServeurClient
            Dim Uti As UtilisateurType = Nothing

            WcfWebService = New VirtualiaServeurClient
            Uti = WcfWebService.LireProfilUtilisateur(NomUtiSgbd, NomGestionnaire, NoBd)
            WcfWebService.Close()
            Return Uti
        End Function

        Public Function ListeUtilisateurs(Nature As String, NoBd As Integer) As List(Of UtilisateurType) Implements IServiceServeur.ListeUtilisateurs
            Dim WcfWebService As VirtualiaServeurClient
            Dim LstUti As List(Of UtilisateurType) = Nothing

            WcfWebService = New VirtualiaServeurClient
            Select Case Nature
                Case "Session"
                    LstUti = WcfWebService.ListedesUtilisateursEnSession
                Case "Tous"
                    LstUti = WcfWebService.ListedeTouslesUtilisateurs(NoBd)
            End Select
            WcfWebService.Close()
            Return LstUti
        End Function

        Public Function MajSqlDirecte(NomUtiSgbd As String, OrdreSql As String) As Boolean Implements IServiceServeur.MajSqlDirecte
            Dim WcfWebService As VirtualiaServeurClient
            Dim CmdSqlVirtualia As SqlVirtualiaType
            Dim Cretour As Boolean

            CmdSqlVirtualia = New SqlVirtualiaType
            CmdSqlVirtualia.Utilisateur = NomUtiSgbd
            CmdSqlVirtualia.OrdreSql = OrdreSql

            WcfWebService = New VirtualiaServeurClient
            Cretour = WcfWebService.MiseAJourSql(CmdSqlVirtualia)
            WcfWebService.Close()

            Return Cretour
        End Function

        Public Function MajSqlDirecte(NomUtiSgbd As String, FluxSql As List(Of String)) As Integer Implements IServiceServeur.MajSqlDirecte
            Dim WcfWebService As VirtualiaServeurClient
            Dim Indicek As Integer

            WcfWebService = New VirtualiaServeurClient
            Indicek = WcfWebService.MiseAJourFluxSql(NomUtiSgbd, FluxSql)
            WcfWebService.Close()

            Return Indicek
        End Function

        Public Function MiseAjour_Fiche(NomUtiSgbd As String, PVue As Integer, NoObjet As Integer, Ide As Integer, CodeMaj As String, ValeursLues As String, ValeursMaj As String, Optional SiJournaliser As Boolean = True) As Boolean Implements IServiceServeur.MiseAjour_Fiche
            Dim WcfWebService As VirtualiaServeurClient
            Dim CmdSqlVirtualia As SqlVirtualiaType
            Dim Cretour As Boolean

            CmdSqlVirtualia = New SqlVirtualiaType
            CmdSqlVirtualia.Utilisateur = NomUtiSgbd
            CmdSqlVirtualia.PointdeVue = PVue
            CmdSqlVirtualia.NumObjet = NoObjet
            CmdSqlVirtualia.Identifiant = Ide
            CmdSqlVirtualia.ValeursNew = ValeursMaj
            CmdSqlVirtualia.ValeursLues = ValeursLues

            WcfWebService = New VirtualiaServeurClient
            Select Case CodeMaj
                Case "C"
                    Cretour = WcfWebService.InsererUneFiche(CmdSqlVirtualia)
                Case "M"
                    Cretour = WcfWebService.MettreAJourUneFiche(CmdSqlVirtualia)
                Case "S"
                    Cretour = WcfWebService.SupprimerUneFiche(CmdSqlVirtualia)
            End Select

            If Cretour = True And SiJournaliser = True Then
                Dim Jrn As Virtualia.TablesObjet.ShemaVUE.VUE_Journal
                Dim LstJrn As New List(Of String)
                Jrn = New Virtualia.TablesObjet.ShemaVUE.VUE_Journal(NomUtiSgbd, CodeMaj, PVue, NoObjet, Ide)
                If CodeMaj = "S" Then
                    Jrn.ContenuTable = ValeursLues
                Else
                    Jrn.ContenuTable = ValeursMaj
                End If
                LstJrn.Add(Jrn.Enregistrement)
                Cretour = WcfWebService.Journaliser(NomUtiSgbd, LstJrn)
            End If

            WcfWebService.Close()

            Return Cretour
        End Function

        Public Function MiseAjour_SqlExterne(NomUtiClient As String, NomUtiSgbd As String, PwSgbdDecrypte As String, FileDsn As String, TypeduSgbd As Integer, Requete As String, Optional FluxRequetes As List(Of String) = Nothing) As Boolean Implements IServiceServeur.MiseAjour_SqlExterne
            Dim WcfWebService As VirtualiaServeurClient
            Dim CmdSqlVirtualia As SqlExterneType
            Dim CnxODBC As ConnexionODBCType
            Dim Cretour As Boolean
            Dim IndiceA As Integer

            CnxODBC = New ConnexionODBCType
            CnxODBC.FileDsn = FileDsn
            CnxODBC.UtiSgbd = NomUtiSgbd
            CnxODBC.PwSgbd = PwSgbdDecrypte

            CmdSqlVirtualia = New SqlExterneType
            CmdSqlVirtualia.Utilisateur = NomUtiClient
            CmdSqlVirtualia.TypeSgbd = TypeduSgbd
            If FluxRequetes Is Nothing Then
                CmdSqlVirtualia.OrdreSql = Requete
                CmdSqlVirtualia.FluxOrdreSql = Nothing
            Else
                CmdSqlVirtualia.FluxOrdreSql = FluxRequetes
                CmdSqlVirtualia.OrdreSql = ""
            End If
            CmdSqlVirtualia.Connexion = CnxODBC

            WcfWebService = New VirtualiaServeurClient
            If CmdSqlVirtualia.FluxOrdreSql Is Nothing Then
                Cretour = WcfWebService.MiseAJourSqlExterne(CmdSqlVirtualia)
            Else
                IndiceA = WcfWebService.MiseAJourSqlFluxExterne(CmdSqlVirtualia)
                If IndiceA = 0 Then
                    Cretour = True
                Else
                    Cretour = False
                End If
            End If
            WcfWebService.Close()

            Return Cretour
        End Function

        Public Function ObtenirUnCompteur(NomUtiSgbd As String, NomTable As String, Categorie As String) As Integer Implements IServiceServeur.ObtenirUnCompteur
            Dim WcfWebService As VirtualiaServeurClient
            Dim Cpt As Integer

            WcfWebService = New VirtualiaServeurClient
            Cpt = WcfWebService.ObtenirUnCompteur(NomUtiSgbd, NomTable, Categorie)
            WcfWebService.Close()
            Return Cpt
        End Function

        Public Function ObtenirUnFichierParametre(NomUtiSgbd As String, Categorie As String) As Byte() Implements IServiceServeur.ObtenirUnFichierParametre
            Dim WcfWebService As VirtualiaServeurClient
            Dim TableauBytes(0) As Byte

            WcfWebService = New VirtualiaServeurClient
            TableauBytes = WcfWebService.ObtenirFichierXml(NomUtiSgbd, Categorie)
            WcfWebService.Close()
            Return TableauBytes
        End Function

        Public Function OuvertureSession(NomUtiSgbd As String, Pw_Sha As String, NoBd As Integer) As Integer Implements IServiceServeur.OuvertureSession
            Dim WcfWebService As VirtualiaServeurClient
            Dim Uti As UtilisateurType
            Dim RhOrdi As Virtualia.Systeme.Outils.Ordinateur
            Dim Cretour As Integer

            Uti = New UtilisateurType
            RhOrdi = New Virtualia.Systeme.Outils.Ordinateur
            Uti.Nom = NomUtiSgbd
            Uti.Machine = RhOrdi.NomdelOrdinateur
            Uti.AdresseIP = RhOrdi.AdresseIPOrdinateur
            Uti.MotPasse = Pw_Sha
            Uti.InstanceBd = NoBd
            WcfWebService = New VirtualiaServeurClient
            Cretour = WcfWebService.OuvertureSession(Uti)
            WcfWebService.Close()
            Return Cretour
        End Function

        Public Function RequeteSql_ToFiches(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, OrdreSql As String) As List(Of VIR_FICHE) Implements IServiceServeur.RequeteSql_ToFiches
            Dim WcfWebService As VirtualiaServeurClient
            Dim CmdSqlVirtualia As SqlVirtualiaType
            Dim LstComplet As List(Of VirRequeteType)
            Dim LstPage As List(Of VirRequeteType)
            Dim NoPage As Integer = 1

            CmdSqlVirtualia = New SqlVirtualiaType
            CmdSqlVirtualia.Utilisateur = NomUtiSgbd
            CmdSqlVirtualia.PointdeVue = Pvue
            CmdSqlVirtualia.NumObjet = NoObjet
            CmdSqlVirtualia.OrdreSql = OrdreSql

            WcfWebService = New VirtualiaServeurClient

            LstComplet = New List(Of VirRequeteType)
            Do
                CmdSqlVirtualia.NumerodePage = NoPage
                LstPage = WcfWebService.Selection_Sql(CmdSqlVirtualia)
                If LstPage Is Nothing Then
                    Exit Do
                End If
                LstComplet.AddRange(LstPage)
                NoPage += 1
            Loop

            WcfWebService.Close()

            Return ListeFichesVirtualia(Pvue, LstComplet)
        End Function

        Public Function RequeteSql_ToListeChar(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, OrdreSql As String) As List(Of String) Implements IServiceServeur.RequeteSql_ToListeChar
            Dim WcfWebService As VirtualiaServeurClient
            Dim CmdSqlVirtualia As SqlVirtualiaType
            Dim LstComplet As List(Of String)
            Dim LstPage As List(Of String)
            Dim NoPage As Integer = 1

            CmdSqlVirtualia = New SqlVirtualiaType
            CmdSqlVirtualia.Utilisateur = NomUtiSgbd
            CmdSqlVirtualia.PointdeVue = Pvue
            CmdSqlVirtualia.NumObjet = NoObjet
            CmdSqlVirtualia.OrdreSql = OrdreSql

            WcfWebService = New VirtualiaServeurClient

            LstComplet = New List(Of String)
            Do
                CmdSqlVirtualia.NumerodePage = NoPage
                LstPage = WcfWebService.SelectionSql(CmdSqlVirtualia)
                If LstPage Is Nothing Then
                    Exit Do
                End If
                LstComplet.AddRange(LstPage)
                NoPage += 1
            Loop

            WcfWebService.Close()

            Return LstComplet
        End Function

        Public Function RequeteSql_ToListeType(NomUtiSgbd As String, Pvue As Integer, NoObjet As Integer, OrdreSql As String) As List(Of VirRequeteType) Implements IServiceServeur.RequeteSql_ToListeType
            Dim WcfWebService As VirtualiaServeurClient
            Dim CmdSqlVirtualia As SqlVirtualiaType
            Dim LstComplet As List(Of VirRequeteType)
            Dim LstPage As List(Of VirRequeteType)
            Dim NoPage As Integer = 1

            CmdSqlVirtualia = New SqlVirtualiaType
            CmdSqlVirtualia.Utilisateur = NomUtiSgbd
            CmdSqlVirtualia.PointdeVue = Pvue
            CmdSqlVirtualia.NumObjet = NoObjet
            CmdSqlVirtualia.OrdreSql = OrdreSql

            WcfWebService = New VirtualiaServeurClient

            LstComplet = New List(Of VirRequeteType)
            Do
                CmdSqlVirtualia.NumerodePage = NoPage
                LstPage = WcfWebService.Selection_Sql(CmdSqlVirtualia)
                If LstPage Is Nothing Then
                    Exit Do
                End If
                LstComplet.AddRange(LstPage)
                NoPage += 1
            Loop

            WcfWebService.Close()

            Return LstComplet
        End Function

        Public Function RequeteSqlExterne_ToListeChar(NomUtiClient As String, NomUtiSgbd As String, PwSgbdDecrypte As String, FileDsn As String, TypeduSgbd As Integer, Requete As String) As List(Of String) Implements IServiceServeur.RequeteSqlExterne_ToListeChar
            Dim WcfWebService As VirtualiaServeurClient
            Dim CmdSqlVirtualia As SqlExterneType
            Dim CnxODBC As ConnexionODBCType
            Dim LstComplet As List(Of String)
            Dim LstPage As List(Of String)
            Dim NoPage As Integer = 1

            CnxODBC = New ConnexionODBCType
            CnxODBC.FileDsn = FileDsn
            CnxODBC.UtiSgbd = NomUtiSgbd
            CnxODBC.PwSgbd = PwSgbdDecrypte

            CmdSqlVirtualia = New SqlExterneType
            CmdSqlVirtualia.Utilisateur = NomUtiClient
            CmdSqlVirtualia.TypeSgbd = TypeduSgbd
            CmdSqlVirtualia.OrdreSql = Requete
            CmdSqlVirtualia.Connexion = CnxODBC

            WcfWebService = New VirtualiaServeurClient
            LstComplet = New List(Of String)
            Do
                CmdSqlVirtualia.NumerodePage = NoPage
                LstPage = WcfWebService.SelectionSqlExterne(CmdSqlVirtualia)
                If LstPage Is Nothing Then
                    Exit Do
                End If
                LstComplet.AddRange(LstPage)
                NoPage += 1
            Loop

            WcfWebService.Close()

            Return LstComplet
        End Function

        Public Function RequeteSqlExterne_ToListeType(NomUtiClient As String, NomUtiSgbd As String, PwSgbdDecrypte As String, FileDsn As String, TypeduSgbd As Integer, Requete As String) As List(Of VirRequeteType) Implements IServiceServeur.RequeteSqlExterne_ToListeType
            Dim WcfWebService As VirtualiaServeurClient
            Dim CmdSqlVirtualia As SqlExterneType
            Dim CnxODBC As ConnexionODBCType
            Dim LstComplet As List(Of VirRequeteType)
            Dim LstPage As List(Of VirRequeteType)
            Dim NoPage As Integer = 1

            CnxODBC = New ConnexionODBCType
            CnxODBC.FileDsn = FileDsn
            CnxODBC.UtiSgbd = NomUtiSgbd
            CnxODBC.PwSgbd = PwSgbdDecrypte

            CmdSqlVirtualia = New SqlExterneType
            CmdSqlVirtualia.Utilisateur = NomUtiClient
            CmdSqlVirtualia.TypeSgbd = TypeduSgbd
            CmdSqlVirtualia.OrdreSql = Requete
            CmdSqlVirtualia.Connexion = CnxODBC

            WcfWebService = New VirtualiaServeurClient
            LstComplet = New List(Of VirRequeteType)
            Do
                CmdSqlVirtualia.NumerodePage = NoPage
                LstPage = WcfWebService.Selection_SqlExterne(CmdSqlVirtualia)
                If LstPage Is Nothing Then
                    Exit Do
                End If
                LstComplet.AddRange(LstPage)
                NoPage += 1
            Loop

            WcfWebService.Close()

            Return LstComplet
        End Function

        Private ReadOnly Property ListeFichesVirtualia(ByVal PointdeVue As Integer, ByVal ListeResultats As List(Of VirRequeteType)) As List(Of VIR_FICHE)
            Get
                If ListeResultats Is Nothing Then
                    Return Nothing
                End If
                If ListeResultats.Count = 0 Then
                    Return Nothing
                End If
                Dim GenericConstructeur As Virtualia.Systeme.IVConstructeur
                Dim FicheVirtualia As VIR_FICHE
                Dim LstFiches As New List(Of VIR_FICHE)
                Select Case PointdeVue
                    Case VI.PointdeVue.PVueApplicatif
                        GenericConstructeur = New Virtualia.TablesObjet.ShemaPER.VConstructeur(WsTypeModele)
                        GenericConstructeur.V_Parametre = WsSpecificite
                    Case VI.PointdeVue.PVueContextesJour, VI.PointdeVue.PVueVueLogique
                        GenericConstructeur = New Virtualia.TablesObjet.ShemaVUE.VConstructeur(PointdeVue)
                    Case Else
                        GenericConstructeur = New Virtualia.TablesObjet.ShemaREF.VConstructeur(WsTypeModele, PointdeVue)
                        GenericConstructeur.V_Parametre = WsSpecificite
                End Select
                For Each Ligne As VirRequeteType In ListeResultats
                    FicheVirtualia = GenericConstructeur.V_NouvelleFiche(Ligne.NumObjet, Ligne.Ide_Dossier, Ligne.Valeurs)
                    If FicheVirtualia IsNot Nothing Then
                        LstFiches.Add(FicheVirtualia)
                    End If
                Next
                Return LstFiches
            End Get
        End Property

        '************** Pour la constitution des liste VIR_FICHE
        Public Sub New(ByVal TypeduModele As Integer, Optional ByVal Specifique As Integer = 0)
            Me.New()
            WsTypeModele = TypeduModele
            WsSpecificite = Specifique
        End Sub

        Public Sub New()
            MyBase.New()
        End Sub

        Public ReadOnly Property OuvertureSessionWcf(ByVal NomUtiSgbd As String, ByVal Pw_Sha As String, ByVal NoBd As Integer) As Integer
            Get
                Dim WcfWebService As VirtualiaServeurClient
                Dim Uti As UtilisateurType
                Dim RhOrdi As Virtualia.Systeme.Outils.Ordinateur
                Dim Cretour As Integer

                Uti = New UtilisateurType
                RhOrdi = New Virtualia.Systeme.Outils.Ordinateur
                Uti.Nom = NomUtiSgbd
                Uti.Machine = RhOrdi.NomdelOrdinateur
                Uti.AdresseIP = RhOrdi.AdresseIPOrdinateur
                Uti.MotPasse = Pw_Sha
                Uti.InstanceBd = NoBd
                WcfWebService = New VirtualiaServeurClient
                Cretour = WcfWebService.OuvertureSession(Uti)
                WcfWebService.Close()
                Return Cretour
            End Get
        End Property

        Public ReadOnly Property FermetureSessionWcf(ByVal NomUtiSgbd As String) As Boolean
            Get
                Dim WcfWebService As VirtualiaServeurClient
                Dim Cretour As Boolean

                WcfWebService = New VirtualiaServeurClient
                Cretour = WcfWebService.FermetureSession(NomUtiSgbd)
                WcfWebService.Close()
                Return Cretour
            End Get
        End Property

        Public ReadOnly Property IPServeur As String Implements IServiceServeur.IPServeur
            Get
                Dim WcfWebService As VirtualiaServeurClient
                Dim Cretour As String

                WcfWebService = New VirtualiaServeurClient
                Cretour = WcfWebService.IPServeur
                WcfWebService.Close()
                Return Cretour
            End Get
        End Property

        Public ReadOnly Property NomDNSServeur As String Implements IServiceServeur.NomDNSServeur
            Get
                Dim WcfWebService As VirtualiaServeurClient
                Dim Cretour As String

                WcfWebService = New VirtualiaServeurClient
                Cretour = WcfWebService.NomDnsServeur
                WcfWebService.Close()
                Return Cretour
            End Get
        End Property
    End Class
End Namespace