﻿'------------------------------------------------------------------------------
' <auto-generated>
'     Ce code a été généré par un outil.
'     Version du runtime :4.0.30319.42000
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Namespace ServiceCarriere
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ServiceModel.ServiceContractAttribute(ConfigurationName:="ServiceCarriere.IVirtualiaCarriere")>  _
    Public Interface IVirtualiaCarriere
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaCarriere/PreparationVUES_Generer", ReplyAction:="http://tempuri.org/IVirtualiaCarriere/PreparationVUES_GenererResponse")>  _
        Function PreparationVUES_Generer(ByVal Identifiants As System.Collections.Generic.List(Of Integer)) As Boolean
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaCarriere/PreparationVUES_Generer", ReplyAction:="http://tempuri.org/IVirtualiaCarriere/PreparationVUES_GenererResponse")>  _
        Function PreparationVUES_GenererAsync(ByVal Identifiants As System.Collections.Generic.List(Of Integer)) As System.Threading.Tasks.Task(Of Boolean)
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaCarriere/PreparationVUES_Lecture", ReplyAction:="http://tempuri.org/IVirtualiaCarriere/PreparationVUES_LectureResponse")>  _
        Function PreparationVUES_Lecture(ByVal Parametre As String) As Boolean
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaCarriere/PreparationVUES_Lecture", ReplyAction:="http://tempuri.org/IVirtualiaCarriere/PreparationVUES_LectureResponse")>  _
        Function PreparationVUES_LectureAsync(ByVal Parametre As String) As System.Threading.Tasks.Task(Of Boolean)
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaCarriere/PreparationVUES_ListeDossiers", ReplyAction:="http://tempuri.org/IVirtualiaCarriere/PreparationVUES_ListeDossiersResponse")>  _
        Function PreparationVUES_ListeDossiers() As System.Collections.Generic.List(Of Integer)
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaCarriere/PreparationVUES_ListeDossiers", ReplyAction:="http://tempuri.org/IVirtualiaCarriere/PreparationVUES_ListeDossiersResponse")>  _
        Function PreparationVUES_ListeDossiersAsync() As System.Threading.Tasks.Task(Of System.Collections.Generic.List(Of Integer))
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaCarriere/PreparationVUES_Finaliser", ReplyAction:="http://tempuri.org/IVirtualiaCarriere/PreparationVUES_FinaliserResponse")>  _
        Function PreparationVUES_Finaliser() As Boolean
        
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/IVirtualiaCarriere/PreparationVUES_Finaliser", ReplyAction:="http://tempuri.org/IVirtualiaCarriere/PreparationVUES_FinaliserResponse")>  _
        Function PreparationVUES_FinaliserAsync() As System.Threading.Tasks.Task(Of Boolean)
    End Interface
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Public Interface IVirtualiaCarriereChannel
        Inherits ServiceCarriere.IVirtualiaCarriere, System.ServiceModel.IClientChannel
    End Interface
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Partial Public Class VirtualiaCarriereClient
        Inherits System.ServiceModel.ClientBase(Of ServiceCarriere.IVirtualiaCarriere)
        Implements ServiceCarriere.IVirtualiaCarriere
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String)
            MyBase.New(endpointConfigurationName)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As String)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal binding As System.ServiceModel.Channels.Binding, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(binding, remoteAddress)
        End Sub
        
        Public Function PreparationVUES_Generer(ByVal Identifiants As System.Collections.Generic.List(Of Integer)) As Boolean Implements ServiceCarriere.IVirtualiaCarriere.PreparationVUES_Generer
            Return MyBase.Channel.PreparationVUES_Generer(Identifiants)
        End Function
        
        Public Function PreparationVUES_GenererAsync(ByVal Identifiants As System.Collections.Generic.List(Of Integer)) As System.Threading.Tasks.Task(Of Boolean) Implements ServiceCarriere.IVirtualiaCarriere.PreparationVUES_GenererAsync
            Return MyBase.Channel.PreparationVUES_GenererAsync(Identifiants)
        End Function
        
        Public Function PreparationVUES_Lecture(ByVal Parametre As String) As Boolean Implements ServiceCarriere.IVirtualiaCarriere.PreparationVUES_Lecture
            Return MyBase.Channel.PreparationVUES_Lecture(Parametre)
        End Function
        
        Public Function PreparationVUES_LectureAsync(ByVal Parametre As String) As System.Threading.Tasks.Task(Of Boolean) Implements ServiceCarriere.IVirtualiaCarriere.PreparationVUES_LectureAsync
            Return MyBase.Channel.PreparationVUES_LectureAsync(Parametre)
        End Function
        
        Public Function PreparationVUES_ListeDossiers() As System.Collections.Generic.List(Of Integer) Implements ServiceCarriere.IVirtualiaCarriere.PreparationVUES_ListeDossiers
            Return MyBase.Channel.PreparationVUES_ListeDossiers
        End Function
        
        Public Function PreparationVUES_ListeDossiersAsync() As System.Threading.Tasks.Task(Of System.Collections.Generic.List(Of Integer)) Implements ServiceCarriere.IVirtualiaCarriere.PreparationVUES_ListeDossiersAsync
            Return MyBase.Channel.PreparationVUES_ListeDossiersAsync
        End Function
        
        Public Function PreparationVUES_Finaliser() As Boolean Implements ServiceCarriere.IVirtualiaCarriere.PreparationVUES_Finaliser
            Return MyBase.Channel.PreparationVUES_Finaliser
        End Function
        
        Public Function PreparationVUES_FinaliserAsync() As System.Threading.Tasks.Task(Of Boolean) Implements ServiceCarriere.IVirtualiaCarriere.PreparationVUES_FinaliserAsync
            Return MyBase.Channel.PreparationVUES_FinaliserAsync
        End Function
    End Class
End Namespace
