﻿Option Strict On
Option Explicit On
Option Compare Text
Namespace WebService
    Public Class VirtualiaWcfMetier
        Private WcfServiceAnnuaire As Virtualia.Net.ServiceAnnuaire.VirtualiaAnnuaireClient
        Private WcfServiceCarriere As Virtualia.Net.ServiceCarriere.VirtualiaCarriereClient
        Private WcfServiceExpertes As Virtualia.Net.ServiceExpertes.VirtualiaExperteClient
        Private WcfServiceGapaie As Virtualia.Net.InterfaceGapaie.InterfaceGapaieClient
        Private WcfServicePrimes As Virtualia.Net.ServicePrimes.VirtualiaPrimeClient

        Public ReadOnly Property Annuaire As Virtualia.Net.ServiceAnnuaire.VirtualiaAnnuaireClient
            Get
                If WcfServiceAnnuaire Is Nothing Then
                    WcfServiceAnnuaire = New Virtualia.Net.ServiceAnnuaire.VirtualiaAnnuaireClient
                End If
                Return WcfServiceAnnuaire
            End Get
        End Property
        Public ReadOnly Property Carriere As Virtualia.Net.ServiceCarriere.VirtualiaCarriereClient
            Get
                If WcfServiceCarriere Is Nothing Then
                    WcfServiceCarriere = New Virtualia.Net.ServiceCarriere.VirtualiaCarriereClient
                End If
                Return WcfServiceCarriere
            End Get
        End Property
        Public ReadOnly Property Experte As Virtualia.Net.ServiceExpertes.VirtualiaExperteClient
            Get
                If WcfServiceExpertes Is Nothing Then
                    WcfServiceExpertes = New Virtualia.Net.ServiceExpertes.VirtualiaExperteClient
                End If
                Return WcfServiceExpertes
            End Get
        End Property
        Public ReadOnly Property Gapaie As Virtualia.Net.InterfaceGapaie.InterfaceGapaieClient
            Get
                If WcfServiceGapaie Is Nothing Then
                    WcfServiceGapaie = New Virtualia.Net.InterfaceGapaie.InterfaceGapaieClient
                End If
                Return WcfServiceGapaie
            End Get
        End Property
        Public ReadOnly Property Prime As Virtualia.Net.ServicePrimes.VirtualiaPrimeClient
            Get
                If WcfServicePrimes Is Nothing Then
                    WcfServicePrimes = New Virtualia.Net.ServicePrimes.VirtualiaPrimeClient
                End If
                Return WcfServicePrimes
            End Get
        End Property
    End Class
End Namespace