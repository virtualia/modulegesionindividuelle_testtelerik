﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_MATERIEL
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_d_effet As String
        Private WsNature As String
        Private WsModele As String
        Private WsNumeroSerie As String
        Private WsAbonnement As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_MATERIEL"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaMateriel
            End Get
        End Property

        Public Property Date_d_effet() As String
            Get
                Return WsDate_d_effet
            End Get
            Set(ByVal value As String)
                WsDate_d_effet = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_d_effet
            End Set
        End Property

        Public Property Nature_Materiel() As String
            Get
                Return WsNature
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsNature = value
                    Case Else
                        WsNature = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsNature
            End Set
        End Property

        Public Property Modele_Materiel() As String
            Get
                Return WsModele
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsModele = value
                    Case Else
                        WsModele = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Numero_Serie() As String
            Get
                Return WsNumeroSerie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsNumeroSerie = value
                    Case Else
                        WsNumeroSerie = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Abonnement() As String
            Get
                Return WsAbonnement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsAbonnement = value
                    Case Else
                        WsAbonnement = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_d_effet & VI.Tild)
                Chaine.Append(Nature_Materiel & VI.Tild)
                Chaine.Append(Modele_Materiel & VI.Tild)
                Chaine.Append(Numero_Serie & VI.Tild)
                Chaine.Append(Abonnement & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_d_effet = TableauData(1)
                Nature_Materiel = TableauData(2)
                Modele_Materiel = TableauData(3)
                Numero_Serie = TableauData(4)
                Abonnement = TableauData(5)
                MyBase.Date_de_Fin = TableauData(6)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_d_effet
                    Case 1
                        Return WsNature
                    Case 2
                        Return WsModele
                    Case 3
                        Return WsNumeroSerie
                    Case 4
                        Return WsAbonnement
                    Case 5
                        Return Date_de_Fin
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


