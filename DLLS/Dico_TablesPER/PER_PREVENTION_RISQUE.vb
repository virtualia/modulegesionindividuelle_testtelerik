﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_PREVENTION_RISQUE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsSituationPrincipale As String
        Private WsSituationSecondaire As String
        Private WsSituationTertiaire As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_PREVENTION_RISQUE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaPrevention
            End Get
        End Property

        Public Property Situation_Principale() As String
            Get
                Return WsSituationPrincipale
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsSituationPrincipale = value
                    Case Else
                        WsSituationPrincipale = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsSituationPrincipale
            End Set
        End Property

        Public Property Situation_Secondaire() As String
            Get
                Return WsSituationSecondaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsSituationSecondaire = value
                    Case Else
                        WsSituationSecondaire = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Situation_Tertiaire() As String
            Get
                Return WsSituationTertiaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsSituationTertiaire = value
                    Case Else
                        WsSituationTertiaire = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Situation_Principale & VI.Tild)
                Chaine.Append(Situation_Secondaire & VI.Tild)
                Chaine.Append(Situation_Tertiaire)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Situation_Principale = TableauData(1)
                Situation_Secondaire = TableauData(2)
                Situation_Tertiaire = TableauData(3)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return ""
                    Case 1
                        Return WsSituationPrincipale
                    Case 2
                        Return WsSituationSecondaire
                    Case 3
                        Return WsSituationTertiaire
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

