﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_HANDICAP
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsSiSalarieHandicape As String
        Private WsTauxInvalidite As Double
        Private WsDate_Taux As String
        Private WsClassification As String
        Private WsDate_Reconnaissance As String
        Private WsDate_Fin_Reconnaissance As String
        Private WsDate_Orientation As String
        Private WsDate_Fin_Orientation As String
        Private WsCategorieCOTOREP As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_HANDICAP"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaHandicap
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 10
            End Get
        End Property

        Public Property SiHandicap() As String
            Get
                Return WsSiSalarieHandicape
            End Get
            Set(ByVal value As String)
                WsSiSalarieHandicape = value
            End Set
        End Property

        Public Property Taux_Invalidite() As Double
            Get
                Return WsTauxInvalidite
            End Get
            Set(ByVal value As Double)
                WsTauxInvalidite = value
            End Set
        End Property

        Public Property Date_Taux_Invalidite() As String
            Get
                Return WsDate_Taux
            End Get
            Set(ByVal value As String)
                WsDate_Taux = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Classification() As String
            Get
                Return WsClassification
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsClassification = value
                    Case Else
                        WsClassification = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Property Date_Reconnaissance() As String
            Get
                Return WsDate_Reconnaissance
            End Get
            Set(ByVal value As String)
                WsDate_Reconnaissance = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Fin_Reconnaissance() As String
            Get
                Return WsDate_Fin_Reconnaissance
            End Get
            Set(ByVal value As String)
                WsDate_Fin_Reconnaissance = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Orientation_Cotorep() As String
            Get
                Return WsDate_Orientation
            End Get
            Set(ByVal value As String)
                WsDate_Orientation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Fin_Orientation_Cotorep() As String
            Get
                Return WsDate_Orientation
            End Get
            Set(ByVal value As String)
                WsDate_Fin_Orientation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Categorie_Cotorep() As String
            Get
                Return WsCategorieCOTOREP
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsCategorieCOTOREP = value
                    Case Else
                        WsCategorieCOTOREP = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(SiHandicap & VI.Tild)
                Chaine.Append(Taux_Invalidite.ToString & VI.Tild)
                Chaine.Append(Date_Taux_Invalidite & VI.Tild)
                Chaine.Append(Classification & VI.Tild)
                Chaine.Append(Date_Reconnaissance & VI.Tild)
                Chaine.Append(Date_Fin_Reconnaissance & VI.Tild)
                Chaine.Append(Date_Orientation_Cotorep & VI.Tild)
                Chaine.Append(Date_Fin_Orientation_Cotorep & VI.Tild)
                Chaine.Append(Categorie_Cotorep & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                SiHandicap = TableauData(1)
                If TableauData(2) = "" Then TableauData(2) = "0"
                Taux_Invalidite = VirRhFonction.ConversionDouble(TableauData(2))
                Date_Taux_Invalidite = TableauData(3)
                Classification = TableauData(4)
                Date_Reconnaissance = TableauData(5)
                Date_Fin_Reconnaissance = TableauData(6)
                Date_Orientation_Cotorep = TableauData(7)
                Date_Fin_Orientation_Cotorep = TableauData(8)
                Categorie_Cotorep = TableauData(9)
                MyBase.Certification = TableauData(10)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

