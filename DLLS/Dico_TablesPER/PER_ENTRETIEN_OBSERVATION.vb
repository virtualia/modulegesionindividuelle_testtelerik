﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_ENTRETIEN_OBSERVATION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNumeroFamille As String
        Private WsIntituleFamille As String
        Private WsObservation As String = ""

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENTRETIEN_OBSERVATION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaEntretienObs
            End Get
        End Property

        Public Property Numero_Famille() As String
            Get
                Return WsNumeroFamille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsNumeroFamille = value
                    Case Else
                        WsNumeroFamille = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Intitule_Famille() As String
            Get
                Return WsIntituleFamille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntituleFamille = value
                    Case Else
                        WsIntituleFamille = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Observation() As String
            Get
                Return WsObservation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsObservation = value
                    Case Else
                        WsObservation = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Numero_Famille & VI.Tild)
                Chaine.Append(Intitule_Famille & VI.Tild)
                Chaine.Append(Observation)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Numero_Famille = TableauData(2)
                Intitule_Famille = TableauData(3)
                Observation = TableauData(4)

                If WsFicheLue Is Nothing Then
                    WsFicheLue = New System.Text.StringBuilder
                    For IndiceI = 1 To TableauData.Count - 1
                        WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                    Next IndiceI
                End If
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
