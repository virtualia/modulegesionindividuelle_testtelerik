﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_STATUT_DETACHE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsStatut As String
        Private WsSituation As String
        Private WsDate_de_l_arrete As String
        Private WsReference_de_l_arrete As String
        Private WsMode_de_remuneration As String
        Private WsRegime_de_cotisation As String
        Private WsAvenant As Integer = 0
        '
        Private TsDate_de_fin As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_STATUT_DETACHE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaStatutDetache
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 9
            End Get
        End Property

        Public Property Statut() As String
            Get
                Return WsStatut
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsStatut = value
                    Case Else
                        WsStatut = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsStatut
            End Set
        End Property

        Public Property Situation() As String
            Get
                Return WsSituation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsSituation = value
                    Case Else
                        WsSituation = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Date_de_l_arrete() As String
            Get
                Return WsDate_de_l_arrete
            End Get
            Set(ByVal value As String)
                WsDate_de_l_arrete = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Reference_de_l_arrete() As String
            Get
                Return WsReference_de_l_arrete
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsReference_de_l_arrete = value
                    Case Else
                        WsReference_de_l_arrete = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Property Mode_de_remuneration() As String
            Get
                Return WsMode_de_remuneration
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsMode_de_remuneration = value
                    Case Else
                        WsMode_de_remuneration = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Regime_de_cotisation() As String
            Get
                Return WsRegime_de_cotisation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsRegime_de_cotisation = value
                    Case Else
                        WsRegime_de_cotisation = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Avenant() As Integer
            Get
                Return WsAvenant
            End Get
            Set(ByVal value As Integer)
                WsAvenant = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Statut & VI.Tild)
                Chaine.Append(Situation & VI.Tild)
                Chaine.Append(Date_de_l_arrete & VI.Tild)
                Chaine.Append(Reference_de_l_arrete & VI.Tild)
                Chaine.Append(Mode_de_remuneration & VI.Tild)
                Chaine.Append(Regime_de_cotisation & VI.Tild)
                Select Case Avenant
                    Case Is = 0
                        Chaine.Append(VI.Tild)
                    Case Else
                        Chaine.Append(Avenant.ToString & VI.Tild)
                End Select
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Statut = TableauData(2)
                Situation = TableauData(3)
                Date_de_l_arrete = TableauData(4)
                Reference_de_l_arrete = TableauData(5)
                Mode_de_remuneration = TableauData(6)
                Regime_de_cotisation = TableauData(7)
                Select Case TableauData(8)
                    Case Is = ""
                        Avenant = 0
                    Case Else
                        Avenant = CInt(TableauData(8))
                End Select
                MyBase.Date_de_Fin = TableauData(9)
                MyBase.Certification = TableauData(10)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = Date_de_Fin
            End Set
        End Property

        Public Property VDate_de_Fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
