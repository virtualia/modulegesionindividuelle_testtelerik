﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_AUTORISATION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsAutorisationVP As String
        Private WsTypeAutorisationVP As String
        Private WsPuissanceFiscale As String
        Private WsNombreMaxiKms As Integer
        Private WsReductionTPublics As String
        Private WsSiPersonnelServiceActif As String
        Private WsObservations As String
        Private WsDate_Arrete As String
        Private WsChapitreImputation As String
        Private WsSousChapitreImputation As String
        Private WsArticleImputation As String
        Private WsEnveloppeImputation As String
        '
        Private TsDate_de_fin As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_AUTORISATION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaAutAdm 'Spécifique Secteur Public
            End Get
        End Property

        Public Property Autorisation_Vehicule() As String
            Get
                Return WsAutorisationVP
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsAutorisationVP = value
                    Case Else
                        WsAutorisationVP = Strings.Left(value, 40)
                End Select
                MyBase.Clef = WsAutorisationVP
            End Set
        End Property

        Public Property TypeAutorisation() As String
            Get
                Return WsTypeAutorisationVP
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsTypeAutorisationVP = value
                    Case Else
                        WsTypeAutorisationVP = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property PuissanceFiscale_Vehicule() As String
            Get
                Return WsPuissanceFiscale
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsPuissanceFiscale = value
                    Case Else
                        WsPuissanceFiscale = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property NombreMaximum_KmsAutorises() As Integer
            Get
                Return WsNombreMaxiKms
            End Get
            Set(ByVal value As Integer)
                WsNombreMaxiKms = value
            End Set
        End Property

        Public Property Reduction_Sur_TransportsPublics() As String
            Get
                Return WsReductionTPublics
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsReductionTPublics = value
                    Case Else
                        WsReductionTPublics = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property SiPersonnel_ServiceActif() As String
            Get
                Return WsSiPersonnelServiceActif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiPersonnelServiceActif = value
                    Case Else
                        WsSiPersonnelServiceActif = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Date_Arrete() As String
            Get
                Return WsDate_Arrete
            End Get
            Set(ByVal value As String)
                WsDate_Arrete = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Imputation_Chapitre() As String
            Get
                Return WsChapitreImputation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsChapitreImputation = value
                    Case Else
                        WsChapitreImputation = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Imputation_SousChapitre() As String
            Get
                Return WsSousChapitreImputation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsSousChapitreImputation = value
                    Case Else
                        WsSousChapitreImputation = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Imputation_Article() As String
            Get
                Return WsArticleImputation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsArticleImputation = value
                    Case Else
                        WsArticleImputation = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Imputation_Enveloppe() As String
            Get
                Return WsEnveloppeImputation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsEnveloppeImputation = value
                    Case Else
                        WsEnveloppeImputation = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Autorisation_Vehicule & VI.Tild)
                Chaine.Append(TypeAutorisation & VI.Tild)
                Chaine.Append(PuissanceFiscale_Vehicule & VI.Tild)
                Chaine.Append(NombreMaximum_KmsAutorises.ToString & VI.Tild)
                Chaine.Append(Reduction_Sur_TransportsPublics & VI.Tild)
                Chaine.Append(SiPersonnel_ServiceActif & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Date_Arrete & VI.Tild)
                Chaine.Append(Imputation_Chapitre & VI.Tild)
                Chaine.Append(Imputation_SousChapitre & VI.Tild)
                Chaine.Append(Imputation_Article & VI.Tild)
                Chaine.Append(Imputation_Enveloppe)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 15 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Autorisation_Vehicule = TableauData(2)
                TypeAutorisation = TableauData(3)
                PuissanceFiscale_Vehicule = TableauData(4)
                If TableauData(5) = "" Then TableauData(5) = "0"
                NombreMaximum_KmsAutorises = CInt(TableauData(5))
                Reduction_Sur_TransportsPublics = TableauData(6)
                SiPersonnel_ServiceActif = TableauData(7)
                Observations = TableauData(8)
                MyBase.Date_de_Fin = TableauData(9)
                Date_Arrete = TableauData(10)
                Imputation_Chapitre = TableauData(11)
                Imputation_SousChapitre = TableauData(12)
                Imputation_Article = TableauData(13)
                Imputation_Enveloppe = TableauData(14)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = Date_de_fin
            End Set
        End Property

        Public Property VDate_de_Fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return Date_de_Valeur
                    Case 1
                        Return WsAutorisationVP
                    Case 2
                        Return WsTypeAutorisationVP
                    Case 3
                        Return WsPuissanceFiscale
                    Case 4
                        Return WsNombreMaxiKms.ToString
                    Case 5
                        Return WsReductionTPublics
                    Case 6
                        Return WsSiPersonnelServiceActif
                    Case 7
                        Return WsObservations
                    Case 8
                        Return Date_de_Fin
                    Case 9
                        Return WsDate_Arrete
                    Case 10
                        Return WsChapitreImputation
                    Case 11
                        Return WsSousChapitreImputation
                    Case 12
                        Return WsArticleImputation
                    Case 13
                        Return WsEnveloppeImputation
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

