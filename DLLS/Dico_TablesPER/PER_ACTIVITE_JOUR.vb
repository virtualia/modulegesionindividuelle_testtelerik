﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_ACTIVITE_JOUR
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsActivite As String
        Private WsTache_Etape As String
        Private WsPrecision As String
        Private WsPourcentageRepartition As Integer
        Private WsDateSaisie As String
        Private WsDateVisa As String
        Private WsVisa As String
        Private WsCommentaire As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ACTIVITE_JOUR"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaActiviteJour
            End Get
        End Property

        Public Property Activite() As String
            Get
                Return WsActivite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsActivite = value
                    Case Else
                        WsActivite = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Tache_Etape() As String
            Get
                Return WsTache_Etape
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsTache_Etape = value
                    Case Else
                        WsTache_Etape = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Precision() As String
            Get
                Return WsPrecision
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsPrecision = value
                    Case Else
                        WsPrecision = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Pourcentage_Repartition() As Double
            Get
                Return WsPourcentageRepartition
            End Get
            Set(ByVal value As Double)
                WsPourcentageRepartition = value
            End Set
        End Property

        Public Property Date_Saisie() As String
            Get
                Return WsDateSaisie
            End Get
            Set(ByVal value As String)
                WsDateSaisie = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Visa() As String
            Get
                Return WsDateVisa
            End Get
            Set(ByVal value As String)
                WsDateVisa = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Visa() As String
            Get
                Return WsVisa
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsVisa = value
                    Case Else
                        WsVisa = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Commentaire() As String
            Get
                Return WsCommentaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsCommentaire = value
                    Case Else
                        WsCommentaire = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(MyBase.Clef & VI.Tild)
                Chaine.Append(Activite & VI.Tild)
                Chaine.Append(Tache_Etape & VI.Tild)
                Chaine.Append(Precision & VI.Tild)
                Chaine.Append(Pourcentage_Repartition.ToString & VI.Tild)
                Chaine.Append(Date_Saisie & VI.Tild)
                Chaine.Append(Date_Visa & VI.Tild)
                Chaine.Append(Visa & VI.Tild)
                Chaine.Append(Commentaire)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 10 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If IsNumeric(TableauData(2)) Then
                    MyBase.Clef = TableauData(2)
                Else
                    MyBase.Clef = "0"
                End If
                Activite = TableauData(3)
                Tache_Etape = TableauData(4)
                Precision = TableauData(5)
                If TableauData(6) = "" Then TableauData(6) = "0"
                Pourcentage_Repartition = VirRhFonction.ConversionDouble(TableauData(6))
                Date_Saisie = TableauData(7)
                Date_Visa = TableauData(8)
                Visa = TableauData(9)
                Commentaire = TableauData(10)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ClefConcatenee As String
            Get
                Return Activite & Tache_Etape & Precision
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
