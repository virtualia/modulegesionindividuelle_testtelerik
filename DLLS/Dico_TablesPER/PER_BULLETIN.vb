﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_BULLETIN
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsRubrique As String
        Private WsMontant As Double = 0
        Private WsNombre As Double = 0
        Private WsTaux As Double = 0
        Private WsImputation As String
        Private WsCategorie As Integer = 0
        Private WsComplement As String
        Private WsBase As Double = 0
        Private WsCodeCaisse As String
        Private WsNoOrdre As Integer = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_BULLETIN"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaBulletin
            End Get
        End Property

        Public Property Rubrique() As String
            Get
                Return WsRubrique
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsRubrique = value
                    Case Else
                        WsRubrique = Strings.Left(value, 20)
                End Select
                MyBase.Clef = WsRubrique
            End Set
        End Property

        Public Property Montant() As Double
            Get
                Return WsMontant
            End Get
            Set(ByVal value As Double)
                WsMontant = value
            End Set
        End Property

        Public Property Nombre() As Double
            Get
                Return WsNombre
            End Get
            Set(ByVal value As Double)
                WsNombre = value
            End Set
        End Property

        Public Property Taux() As Double
            Get
                Return WsTaux
            End Get
            Set(ByVal value As Double)
                WsTaux = value
            End Set
        End Property

        Public Property Imputation() As String
            Get
                Return WsImputation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsImputation = value
                    Case Else
                        WsImputation = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Categorie() As Integer
            Get
                Return WsCategorie
            End Get
            Set(ByVal value As Integer)
                WsCategorie = value
            End Set
        End Property

        Public Property Complement() As String
            Get
                Return WsComplement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsComplement = value
                    Case Else
                        WsComplement = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Base_Calcul() As Double
            Get
                Return WsBase
            End Get
            Set(ByVal value As Double)
                WsBase = value
            End Set
        End Property

        Public Property Code_Caisse() As String
            Get
                Return WsCodeCaisse
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsCodeCaisse = value
                    Case Else
                        WsCodeCaisse = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Numero_Ordre() As Integer
            Get
                Return WsNoOrdre
            End Get
            Set(ByVal value As Integer)
                WsNoOrdre = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Rubrique & VI.Tild)
                Select Case Montant
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant.ToString) & VI.Tild)
                End Select
                Select Case Nombre
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Nombre.ToString) & VI.Tild)
                End Select
                Select Case Taux
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Taux.ToString) & VI.Tild)
                End Select
                Chaine.Append(Imputation & VI.Tild)
                Select Case Categorie
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(Categorie.ToString & VI.Tild)
                End Select
                Chaine.Append(Complement & VI.Tild)
                Select Case Base_Calcul
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Base_Calcul.ToString) & VI.Tild)
                End Select
                Chaine.Append(Code_Caisse & VI.Tild)
                Select Case Numero_Ordre
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(Numero_Ordre.ToString)
                End Select
                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 9 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Rubrique = TableauData(2)
                If TableauData(3) <> "" Then Montant = VirRhFonction.ConversionDouble(TableauData(3))
                If TableauData(4) <> "" Then Nombre = VirRhFonction.ConversionDouble(TableauData(4))
                If TableauData(5) <> "" Then Taux = VirRhFonction.ConversionDouble(TableauData(5))
                Imputation = TableauData(6)
                If TableauData(7) <> "" Then Categorie = VirRhFonction.ConversionDouble(TableauData(7))
                Complement = TableauData(8)
                If TableauData.Count > 11 Then
                    If TableauData(9) <> "" Then Base_Calcul = VirRhFonction.ConversionDouble(TableauData(9))
                    Code_Caisse = TableauData(10)
                    If TableauData(11) <> "" Then Numero_Ordre = VirRhFonction.ConversionDouble(TableauData(11))
                End If

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return Date_de_Valeur
                    Case 1
                        Return WsRubrique
                    Case 2
                        Select Case Montant
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Montant.ToString)
                        End Select
                    Case 3
                        Select Case Nombre
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Nombre.ToString)
                        End Select
                    Case 4
                        Select Case Taux
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Taux.ToString)
                        End Select
                    Case 5
                        Return WsImputation
                    Case 6
                        Return WsCategorie.ToString
                    Case 7
                        Return WsComplement
                    Case 8
                        Select Case Base_Calcul
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Base_Calcul.ToString)
                        End Select
                    Case 9
                        Return WsCodeCaisse
                    Case 10
                        Select Case WsNoOrdre
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(WsNoOrdre.ToString)
                        End Select
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public ReadOnly Property V_TriCategorie As Integer
            Get
                Select Case Categorie
                    Case Is < 10
                        Return WsCategorie + 100
                    Case Else
                        Return WsCategorie
                End Select
            End Get
        End Property

        Public ReadOnly Property KAKX_Programme As String
            Get
                If Imputation.Length = 8 Then
                    If IsNumeric(Strings.Left(Imputation, 4)) Then
                        Return CInt(Strings.Left(Imputation, 4)).ToString
                    End If
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property KAKX_Action As String
            Get
                If Imputation.Length = 8 Then
                    Return Strings.Mid(Imputation, 5, 2)
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property KAKX_PCE As String
            Get
                If Imputation.Length = 8 Then
                    If IsNumeric(Strings.Left(Imputation, 4)) Then
                        Return Strings.Right(Imputation, 2)
                    End If
                End If
                Return ""
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

