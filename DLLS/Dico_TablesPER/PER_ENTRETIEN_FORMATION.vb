﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_ENTRETIEN_FORMATION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsIntitule As String
        Private WsTypeBesoin As String
        Private WsExpression As String
        Private WsPeriodeSouhaitee As String
        Private WsObjectif As String
        Private WsAvisEvaluateur As String
        Private WsMotifRefus As String
        Private WsEcheanceSouhaitee As String
        Private WsNumeroTri As Integer
        Private WsInitiative As String
        Private WsNombreHeures As String
        Private WsTypeFormationDIF As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENTRETIEN_FORMATION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaEntretienFormation
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Type_du_Besoin() As String
            Get
                Return WsTypeBesoin
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsTypeBesoin = value
                    Case Else
                        WsTypeBesoin = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Expression_Besoin() As String
            Get
                Return WsExpression
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsExpression = value
                    Case Else
                        WsExpression = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Periode_Souhaitee() As String
            Get
                Return WsPeriodeSouhaitee
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsPeriodeSouhaitee = value
                    Case Else
                        WsPeriodeSouhaitee = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Objectif() As String
            Get
                Return WsObjectif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObjectif = value
                    Case Else
                        WsObjectif = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Avis_Evaluateur() As String
            Get
                Return WsAvisEvaluateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsAvisEvaluateur = value
                    Case Else
                        WsAvisEvaluateur = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Motif_Refus() As String
            Get
                Return WsMotifRefus
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsMotifRefus = value
                    Case Else
                        WsMotifRefus = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Echeance_Souhaitee() As String
            Get
                Return WsEcheanceSouhaitee
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsEcheanceSouhaitee = value
                    Case Else
                        WsEcheanceSouhaitee = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property NumeroTri() As Integer
            Get
                Return WsNumeroTri
            End Get
            Set(ByVal value As Integer)
                WsNumeroTri = value
            End Set
        End Property

        Public Property Initiative_Demande() As String
            Get
                Return WsInitiative
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsInitiative = value
                    Case Else
                        WsInitiative = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Nombre_Heures_Formation() As String
            Get
                Return WsNombreHeures
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsNombreHeures = value
                    Case Else
                        WsNombreHeures = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Type_de_Formation_DIF() As String
            Get
                Return WsTypeFormationDIF
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsTypeFormationDIF = value
                    Case Else
                        WsTypeFormationDIF = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Type_du_Besoin & VI.Tild)
                Chaine.Append(Expression_Besoin & VI.Tild)
                Chaine.Append(Periode_Souhaitee & VI.Tild)
                Chaine.Append(Objectif & VI.Tild)
                Chaine.Append(Avis_Evaluateur & VI.Tild)
                Chaine.Append(Motif_Refus & VI.Tild)
                Chaine.Append(Echeance_Souhaitee & VI.Tild)
                Chaine.Append(NumeroTri.ToString & VI.Tild)
                Chaine.Append(Initiative_Demande & VI.Tild)
                Chaine.Append(Nombre_Heures_Formation & VI.Tild)
                Chaine.Append(Type_de_Formation_DIF)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 13 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Intitule = TableauData(2)
                Type_du_Besoin = TableauData(3)
                Expression_Besoin = TableauData(4)
                Periode_Souhaitee = TableauData(5)
                Objectif = TableauData(6)
                Avis_Evaluateur = TableauData(7)
                Motif_Refus = TableauData(8)
                Echeance_Souhaitee = TableauData(9)
                If TableauData(10) <> "" Then NumeroTri = CInt(VirRhFonction.ConversionDouble(TableauData(10)))
                Initiative_Demande = TableauData(11)
                Nombre_Heures_Formation = TableauData(12)
                Type_de_Formation_DIF = TableauData(13)

                If WsFicheLue Is Nothing Then
                    WsFicheLue = New System.Text.StringBuilder
                    For IndiceI = 1 To TableauData.Count - 1
                        WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                    Next IndiceI
                End If
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
