﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_CFA
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_d_effet As String
        Private WsCentredeFormation As String
        Private WsTypeSociete As String
        Private WsTrancheEffectif As String
        Private WsDate_Anciennete_Conservee As String
        Private WsRegion As String
        Private WsDepartement As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_CFA"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 26 'Spécifique CCCA - BTP
            End Get
        End Property

        Public Property Date_d_effet() As String
            Get
                Return WsDate_d_effet
            End Get
            Set(ByVal value As String)
                WsDate_d_effet = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_d_effet
            End Set
        End Property

        Public Property CentredeFormation() As String
            Get
                Return WsCentredeFormation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsCentredeFormation = value
                    Case Else
                        WsCentredeFormation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property TypedeSociete() As String
            Get
                Return WsTypeSociete
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsTypeSociete = value
                    Case Else
                        WsTypeSociete = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property TranchedEffectif() As String
            Get
                Return WsTrancheEffectif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsTrancheEffectif = value
                    Case Else
                        WsTrancheEffectif = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Date_d_Anciennete_Conservee() As String
            Get
                Return WsDate_Anciennete_Conservee
            End Get
            Set(ByVal value As String)
                WsDate_Anciennete_Conservee = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Region() As String
            Get
                Return WsRegion
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsRegion = value
                    Case Else
                        WsRegion = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Departement() As String
            Get
                Return WsDepartement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsDepartement = value
                    Case Else
                        WsDepartement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_d_effet & VI.Tild)
                Chaine.Append(CentredeFormation & VI.Tild)
                Chaine.Append(TypedeSociete & VI.Tild)
                Chaine.Append(TranchedEffectif & VI.Tild)
                Chaine.Append(Date_d_Anciennete_Conservee & VI.Tild)
                Chaine.Append(Region & VI.Tild)
                Chaine.Append(Departement)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_d_effet = TableauData(1)
                CentredeFormation = TableauData(2)
                TypedeSociete = TableauData(3)
                TranchedEffectif = TableauData(4)
                Date_d_Anciennete_Conservee = TableauData(5)
                Region = TableauData(6)
                Departement = TableauData(7)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


