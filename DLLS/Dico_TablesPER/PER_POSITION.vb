Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_POSITION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsPosition As String
        Private WsTaux_d_activite As String
        Private WsDate_de_l_arrete As String
        Private WsReference_de_l_arrete As String
        Private WsDetachement As String
        Private WsDuree As String
        Private WsMotif As String
        Private WsDate_du_decret As String
        Private WsDate_de_parution As String
        Private WsModaliteservice As String
        Private WsTauxremuneration As String
        Private WsSiDetachementAPension As String
        Private WsSiDetachementInternational As String
        Private WsSiCotisPaysAccueil As String
        Private WsSiCotisationsVersees As String
        Private WsTauxSurCotisation As String
        Private WsSiPaye As String = ""
        Private WsSiGere As String = ""
        Private WsSiEmploye As String = ""
        '
        Private TsDate_de_fin As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_POSITION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaActivite 'Spécifique Secteur Public
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 21
            End Get
        End Property

        Public Property Position() As String
            Get
                Return WsPosition
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsPosition = value
                    Case Else
                        WsPosition = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsPosition
            End Set
        End Property

        Public Property Taux_d_activite() As String
            Get
                Return WsTaux_d_activite
            End Get
            Set(ByVal value As String)
                WsTaux_d_activite = Strings.Format(VirRhFonction.ConversionDouble(value), "0.00")
            End Set
        End Property

        Public Property Date_de_l_arrete() As String
            Get
                Return WsDate_de_l_arrete
            End Get
            Set(ByVal value As String)
                WsDate_de_l_arrete = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Reference_de_l_arrete() As String
            Get
                Return WsReference_de_l_arrete
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsReference_de_l_arrete = value
                    Case Else
                        WsReference_de_l_arrete = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Property Detachement() As String
            Get
                Return WsDetachement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsDetachement = value
                    Case Else
                        WsDetachement = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Duree() As String
            Get
                Return WsDuree
            End Get
            Set(ByVal value As String)
                WsDuree = value
            End Set
        End Property

        Public Property Motif() As String
            Get
                Return WsMotif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsMotif = value
                    Case Else
                        WsMotif = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Date_du_decret() As String
            Get
                Return WsDate_du_decret
            End Get
            Set(ByVal value As String)
                WsDate_du_decret = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_parution() As String
            Get
                Return WsDate_de_parution
            End Get
            Set(ByVal value As String)
                WsDate_de_parution = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Modaliteservice() As String
            Get
                Return WsModaliteservice
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsModaliteservice = value
                    Case Else
                        WsModaliteservice = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Tauxremuneration() As String
            Get
                Return WsTauxremuneration
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is <> ""
                        WsTauxremuneration = Strings.Format(VirRhFonction.ConversionDouble(value), "0.00")
                End Select
            End Set
        End Property

        Public Property SiDetachementConduisantaPension() As String
            Get
                Return WsSiDetachementAPension
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiDetachementAPension = value
                    Case Else
                        WsSiDetachementAPension = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property SiDetachementAlInternational() As String
            Get
                Return WsSiDetachementInternational
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiDetachementInternational = value
                    Case Else
                        WsSiDetachementInternational = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property SiCotisationPaysdAccueil() As String
            Get
                Return WsSiCotisPaysAccueil
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiCotisPaysAccueil = value
                    Case Else
                        WsSiCotisPaysAccueil = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property SiCotisationsVersees() As String
            Get
                Return WsSiCotisationsVersees
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiCotisationsVersees = value
                    Case Else
                        WsSiCotisationsVersees = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property TauxdeSurcotisation() As String
            Get
                Return WsTauxSurCotisation
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is <> ""
                        WsTauxSurCotisation = Strings.Format(VirRhFonction.ConversionDouble(value), "0.00")
                End Select
            End Set
        End Property

        Public ReadOnly Property SiPaye As Boolean
            Get
                If WsSiPaye = "Non" Then
                    Return False
                Else
                    Return True
                End If
            End Get
        End Property

        Public Property SiPayeAlpha() As String
            Get
                If WsSiPaye <> "Non" Then
                    Return "Oui"
                Else
                    Return "Non"
                End If
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiPaye = value
                    Case Else
                        WsSiPaye = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public ReadOnly Property SiGere As Boolean
            Get
                If WsSiGere = "Non" Then
                    Return False
                Else
                    Return True
                End If
            End Get
        End Property

        Public Property SiGereAlpha() As String
            Get
                If WsSiGere <> "Non" Then
                    Return "Oui"
                Else
                    Return "Non"
                End If
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiGere = value
                    Case Else
                        WsSiGere = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public ReadOnly Property SiEmploye As Boolean
            Get
                If WsSiEmploye = "Non" Then
                    Return False
                Else
                    Return True
                End If
            End Get
        End Property

        Public Property SiEmployeAlpha() As String
            Get
                If WsSiEmploye <> "Non" Then
                    Return "Oui"
                Else
                    Return "Non"
                End If
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiEmploye = value
                    Case Else
                        WsSiEmploye = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Position & VI.Tild)
                Chaine.Append(Taux_d_activite & VI.Tild)
                Chaine.Append(Date_de_l_arrete & VI.Tild)
                Chaine.Append(Reference_de_l_arrete & VI.Tild)
                Chaine.Append(Detachement & VI.Tild)
                Chaine.Append(Duree & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Motif & VI.Tild)
                Chaine.Append(Date_du_decret & VI.Tild)
                Chaine.Append(Date_de_parution & VI.Tild)
                Chaine.Append(Modaliteservice & VI.Tild)
                Chaine.Append(Tauxremuneration & VI.Tild)
                Chaine.Append(SiDetachementConduisantaPension & VI.Tild)
                Chaine.Append(SiDetachementAlInternational & VI.Tild)
                Chaine.Append(SiCotisationPaysdAccueil & VI.Tild)
                Chaine.Append(SiCotisationsVersees & VI.Tild)
                Chaine.Append(TauxdeSurcotisation & VI.Tild)
                Chaine.Append(SiPayeAlpha & VI.Tild)
                Chaine.Append(SiGereAlpha & VI.Tild)
                Chaine.Append(SiEmployeAlpha & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 22 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Position = TableauData(2)
                TableauData(3) = VirRhFonction.VirgulePoint(TableauData(3))
                Taux_d_activite = TableauData(3)
                Date_de_l_arrete = TableauData(4)
                Reference_de_l_arrete = TableauData(5)
                Detachement = TableauData(6)
                Duree = TableauData(7)
                MyBase.Date_de_Fin = TableauData(8)
                Motif = TableauData(9)
                Date_du_decret = TableauData(10)
                Date_de_parution = TableauData(11)
                Modaliteservice = TableauData(12)
                TableauData(13) = VirRhFonction.VirgulePoint(TableauData(13))
                Tauxremuneration = TableauData(13)
                SiDetachementConduisantaPension = TableauData(14)
                SiDetachementAlInternational = TableauData(15)
                SiCotisationPaysdAccueil = TableauData(16)
                SiCotisationsVersees = TableauData(17)
                TauxdeSurcotisation = TableauData(18)
                SiPayeAlpha = TableauData(19)
                SiGereAlpha = TableauData(20)
                SiEmployeAlpha = TableauData(21)
                MyBase.Certification = TableauData(22)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = Date_de_fin
            End Set
        End Property

        Public Property VDate_de_Fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<DateEffetPosition>" & MyBase.Date_de_Valeur & "</DateEffetPosition>" & vbCrLf)
                Chaine.Append("<DateFinPosition>" & MyBase.Date_de_Fin & "</DateFinPosition>" & vbCrLf)
                Chaine.Append("<PositionAdministrative>" & VirRhFonction.ChaineXMLValide(Position) & "</PositionAdministrative>" & vbCrLf)
                Chaine.Append("<MotifPosition>" & VirRhFonction.ChaineXMLValide(Motif) & "</MotifPosition>" & vbCrLf)
                Chaine.Append("<ModaliteService>" & VirRhFonction.ChaineXMLValide(Modaliteservice) & "</ModaliteService>" & vbCrLf)
                Chaine.Append("<AdministrationDetachement>" & VirRhFonction.ChaineXMLValide(Detachement) & "</AdministrationDetachement>" & vbCrLf)
                Chaine.Append("<TauxActivite>" & VirRhFonction.VirgulePoint(Taux_d_activite) & "</TauxActivite>" & vbCrLf)
                Chaine.Append("<TauxRemuneration>" & VirRhFonction.VirgulePoint(Tauxremuneration) & "</TauxRemuneration>" & vbCrLf)
                Chaine.Append("<DateArrete>" & Date_de_l_arrete & "</DateArrete>" & vbCrLf)
                Chaine.Append("<ReferenceArrete>" & VirRhFonction.ChaineXMLValide(Reference_de_l_arrete) & "</ReferenceArrete>" & vbCrLf)
                Chaine.Append("<DateDecret>" & Date_du_decret & "</DateDecret>" & vbCrLf)
                Chaine.Append("<DateParutionJO>" & Date_de_parution & "</DateParutionJO>" & vbCrLf)
                Chaine.Append("<SiDetachementConduisantaPension>" & SiDetachementConduisantaPension & "</SiDetachementConduisantaPension>" & vbCrLf)
                Chaine.Append("<SiDetachementAlInternational>" & SiDetachementAlInternational & "</SiDetachementAlInternational>" & vbCrLf)
                Chaine.Append("<SiCotisationPaysdAccueil>" & SiCotisationPaysdAccueil & "</SiCotisationPaysdAccueil>" & vbCrLf)
                Chaine.Append("<SiCotisationsVersees>" & SiCotisationsVersees & "</SiCotisationsVersees>" & vbCrLf)
                Chaine.Append("<TauxSurcotisation>" & TauxdeSurcotisation & "</TauxSurcotisation>" & vbCrLf)
                Chaine.Append("<SiPaye>" & SiPayeAlpha & "</SiPaye>" & vbCrLf)
                Chaine.Append("<SiGere>" & SiGereAlpha & "</SiGere>" & vbCrLf)
                Chaine.Append("<SiEmploye>" & SiEmployeAlpha & "</SiEmploye>" & vbCrLf)

                Return Chaine.ToString

            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

