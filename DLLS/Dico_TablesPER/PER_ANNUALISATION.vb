﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_ANNUALISATION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_du_Contrat As String
        Private WsTempsAnnualiseHeures As String
        Private WsCETAnnuel As Double = 0
        Private WsReductionAnnualise As Double = 0
        Private WsPrevisionnel As String
        Private WsComptabilise As String
        Private WsSoldeAnnualisation As String
        Private WsTempsAnnualiseJours As Integer = 0
        Private WsUniteContrat As String
        Private WsRegime As String
        Private WsEpargneCA As Double = 0
        Private WsEpargneRTT As Double = 0
        Private WsEpargneRC As Double = 0
        Private WsSiDepassementAutorise As String
        Private WsSoldeDebitCredit As String
        Private WsSensduSoldeDC As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ANNUALISATION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaAnnualisation
            End Get
        End Property

        Public Property Date_du_Contrat() As String
            Get
                Return WsDate_du_Contrat
            End Get
            Set(ByVal value As String)
                WsDate_du_Contrat = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_du_Contrat
            End Set
        End Property

        Public Property TempsAnnualiseEnHeures() As String
            Get
                Return WsTempsAnnualiseHeures
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsTempsAnnualiseHeures = value
                    Case Else
                        WsTempsAnnualiseHeures = Strings.Left(value, 9)
                End Select
                MyBase.Clef = WsTempsAnnualiseHeures
            End Set
        End Property

        Public Property CompteEpargneTempsAnnuel() As Double
            Get
                Return WsCETAnnuel
            End Get
            Set(ByVal value As Double)
                WsCETAnnuel = value
            End Set
        End Property

        Public Property ReductionAnnualisee() As Double
            Get
                Return WsReductionAnnualise
            End Get
            Set(ByVal value As Double)
                WsReductionAnnualise = value
            End Set
        End Property

        Public Property Previsionnel() As String
            Get
                Return WsPrevisionnel
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsPrevisionnel = value
                    Case Else
                        WsPrevisionnel = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Comptabilise() As String
            Get
                Return WsComptabilise
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsComptabilise = value
                    Case Else
                        WsComptabilise = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property SoldeAnnualisation() As String
            Get
                Return WsSoldeAnnualisation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsSoldeAnnualisation = value
                    Case Else
                        WsSoldeAnnualisation = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property TempsAnnualiseEnJours() As Integer
            Get
                Select Case WsTempsAnnualiseJours
                    Case Is > 0
                        Return WsTempsAnnualiseJours
                    Case Else
                        Return 227
                End Select
            End Get
            Set(ByVal value As Integer)
                WsTempsAnnualiseJours = value
            End Set
        End Property

        Public Property UniteduContrat() As String
            Get
                Return WsUniteContrat
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsUniteContrat = value
                    Case Else
                        WsUniteContrat = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property RegimeduContrat() As String
            Get
                Return WsRegime
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsRegime = value
                    Case Else
                        WsRegime = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Epargne_CA() As Double
            Get
                Return WsEpargneCA
            End Get
            Set(ByVal value As Double)
                WsEpargneCA = value
            End Set
        End Property

        Public Property Epargne_RTT() As Double
            Get
                Return WsEpargneRTT
            End Get
            Set(ByVal value As Double)
                WsEpargneRTT = value
            End Set
        End Property

        Public Property Epargne_RC() As Double
            Get
                Return WsEpargneRC
            End Get
            Set(ByVal value As Double)
                WsEpargneRC = value
            End Set
        End Property

        Public Property SiDepassementAutorise() As String
            Get
                Return WsSiDepassementAutorise
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsSiDepassementAutorise = value
                    Case Is = "0", "Non"
                        WsSiDepassementAutorise = "Non"
                    Case Else
                        WsSiDepassementAutorise = "Oui"
                End Select
            End Set
        End Property

        Public Property SoldeDebitCredit() As String
            Get
                Return WsSoldeDebitCredit
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsSoldeDebitCredit = value
                    Case Else
                        WsSoldeDebitCredit = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property SensduSoldeDC() As String
            Get
                Return WsSensduSoldeDC
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1
                        WsSensduSoldeDC = value
                    Case Else
                        WsSensduSoldeDC = Strings.Left(value, 1)
                End Select
                If WsSensduSoldeDC = "" Then
                    WsSensduSoldeDC = "C"
                End If
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_du_Contrat & VI.Tild)
                Chaine.Append(TempsAnnualiseEnHeures & VI.Tild)
                Select Case CompteEpargneTempsAnnuel
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(CompteEpargneTempsAnnuel.ToString) & VI.Tild)
                End Select
                Select Case ReductionAnnualisee
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(ReductionAnnualisee.ToString) & VI.Tild)
                End Select
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Previsionnel & VI.Tild)
                Chaine.Append(Comptabilise & VI.Tild)
                Chaine.Append(SoldeAnnualisation & VI.Tild)
                Chaine.Append(TempsAnnualiseEnJours.ToString & VI.Tild)
                Chaine.Append(UniteduContrat & VI.Tild)
                Chaine.Append(RegimeduContrat & VI.Tild)
                Select Case Epargne_CA
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Epargne_CA.ToString) & VI.Tild)
                End Select
                Select Case Epargne_RTT
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Epargne_RTT.ToString) & VI.Tild)
                End Select
                Select Case Epargne_RC
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Epargne_RC.ToString) & VI.Tild)
                End Select
                Chaine.Append(SiDepassementAutorise & VI.Tild)
                Chaine.Append(SoldeDebitCredit & VI.Tild)
                Chaine.Append(SensduSoldeDC)
                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 18 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_du_Contrat = TableauData(1)
                TempsAnnualiseEnHeures = TableauData(2)
                If TableauData(3) <> "" Then CompteEpargneTempsAnnuel = VirRhFonction.ConversionDouble(TableauData(3))
                If TableauData(4) <> "" Then ReductionAnnualisee = VirRhFonction.ConversionDouble(TableauData(4))
                MyBase.Date_de_Fin = TableauData(5)
                Previsionnel = TableauData(6)
                Comptabilise = TableauData(7)
                SoldeAnnualisation = TableauData(8)
                If TableauData(9) = "" Then
                    TableauData(9) = "0"
                End If
                TempsAnnualiseEnJours = CInt(TableauData(9))
                UniteduContrat = TableauData(10)
                RegimeduContrat = TableauData(11)
                If TableauData(12) <> "" Then Epargne_CA = VirRhFonction.ConversionDouble(TableauData(12))
                If TableauData(13) <> "" Then Epargne_RTT = VirRhFonction.ConversionDouble(TableauData(13))
                If TableauData(14) <> "" Then Epargne_RC = VirRhFonction.ConversionDouble(TableauData(14))
                SiDepassementAutorise = TableauData(15)
                SoldeDebitCredit = TableauData(16)
                SensduSoldeDC = TableauData(17)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ContratAnnuel_En_Minutes() As Integer
            Get
                Return Val(VirRhDate.CalcHeure(TempsAnnualiseEnHeures, "", 1))
            End Get
        End Property

        Public ReadOnly Property SoldeDebitCredit_En_Minutes() As Integer
            Get
                Dim DureeMn As Integer = 0
                DureeMn = Val(VirRhDate.CalcHeure(SoldeDebitCredit, "", 1))
                Select Case SensduSoldeDC
                    Case Is = "D"
                        Return -DureeMn
                    Case Else
                        Return DureeMn
                End Select
            End Get
        End Property

        Public ReadOnly Property MoyenneHebdomadaire_En_Minutes() As Double
            Get
                Dim NbSemaines As Double = 0
                NbSemaines = TempsAnnualiseEnJours / 5
                Select Case ContratAnnuel_En_Minutes
                    Case Is > 0
                        Return ContratAnnuel_En_Minutes / NbSemaines
                End Select
                Return 0
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_du_Contrat
                    Case 1
                        Return WsTempsAnnualiseHeures
                    Case 2
                        Select Case CompteEpargneTempsAnnuel
                            Case Is = 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(CompteEpargneTempsAnnuel.ToString)
                        End Select
                    Case 3
                        Select Case ReductionAnnualisee
                            Case Is = 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(ReductionAnnualisee.ToString)
                        End Select
                    Case 4
                        Return Date_de_Fin
                    Case 5
                        Return WsPrevisionnel
                    Case 6
                        Return WsComptabilise
                    Case 7
                        Return WsSoldeAnnualisation
                    Case 8
                        Return WsTempsAnnualiseJours.ToString
                    Case 9
                        Return WsUniteContrat
                    Case 10
                        Return WsRegime
                    Case 11
                        Select Case Epargne_CA
                            Case Is = 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Epargne_CA.ToString)
                        End Select
                    Case 12
                        Select Case Epargne_RTT
                            Case Is = 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Epargne_RTT.ToString)
                        End Select
                    Case 13
                        Select Case Epargne_RC
                            Case Is = 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Epargne_RC.ToString)
                        End Select
                    Case 14
                        Return WsSiDepassementAutorise
                    Case 15
                        Return WsSoldeDebitCredit
                    Case 16
                        Return WsSensduSoldeDC
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Property V_GrilleVirtuelle As List(Of String)
            Get
                Dim LstGrid As New List(Of String)
                LstGrid.Add(RegimeduContrat)
                LstGrid.Add(MyBase.Date_de_Valeur)
                LstGrid.Add(MyBase.Date_de_Fin)
                LstGrid.Add(TempsAnnualiseEnHeures)
                LstGrid.Add(TempsAnnualiseEnJours)
                LstGrid.Add(MyBase.Ide_Dossier.ToString & "_" & MyBase.Date_de_Valeur)
                Return LstGrid
            End Get
            Set(ByVal value As List(Of String))
                If value Is Nothing Then
                    Exit Property
                End If
                RegimeduContrat = value(0)
                MyBase.Date_de_Valeur = value(1)
                MyBase.Date_de_Fin = value(2)
                TempsAnnualiseEnHeures = value(3)
                TempsAnnualiseEnJours = value(4)
            End Set
        End Property

        Public Sub FaireDicoVirtuel()
            Dim InfoBase As Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Régime"
            InfoBase.Longueur = 50
            InfoBase.NatureDonnee = "Table"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = VI.PointdeVue.PVueGeneral
            InfoBase.NomTable = "Régime RTT"
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Date de valeur"
            InfoBase.Longueur = 10
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Date de Fin"
            InfoBase.Longueur = 10
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Temps annualisé en heures"
            InfoBase.Longueur = 9
            InfoBase.NatureDonnee = "Heure"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Temps annualisé en jours"
            InfoBase.Longueur = 9
            InfoBase.NatureDonnee = "Heure"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)
        End Sub

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace



