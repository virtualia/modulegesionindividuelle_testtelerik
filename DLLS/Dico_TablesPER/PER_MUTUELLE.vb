﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_MUTUELLE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNom As String
        Private WsNumeroAffiliation As String
        Private WsNatureGaranties As String
        Private WsDate_Depot As String
        Private WsDate_Affiliation As String
        Private WsDate_Radiation As String
        Private WsPersonneGarantie As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_MUTUELLE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaMutuelle
            End Get
        End Property

        Public Property Nom() As String
            Get
                Return WsNom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsNom = value
                    Case Else
                        WsNom = Strings.Left(value, 50)
                End Select
                MyBase.Clef = WsNom
            End Set
        End Property

        Public Property NumeroAffiliation() As String
            Get
                Return WsNumeroAffiliation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNumeroAffiliation = value
                    Case Else
                        WsNumeroAffiliation = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Nature_Garanties() As String
            Get
                Return WsNatureGaranties
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsNatureGaranties = value
                    Case Else
                        WsNatureGaranties = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Date_Depot() As String
            Get
                Return WsDate_Depot
            End Get
            Set(ByVal value As String)
                WsDate_Depot = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Affiliation() As String
            Get
                Return WsDate_Affiliation
            End Get
            Set(ByVal value As String)
                WsDate_Affiliation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Radiation() As String
            Get
                Return WsDate_Radiation
            End Get
            Set(ByVal value As String)
                WsDate_Radiation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property PersonneGarantie() As String
            Get
                Return WsPersonneGarantie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsPersonneGarantie = value
                    Case Else
                        WsPersonneGarantie = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Nom & VI.Tild)
                Chaine.Append(NumeroAffiliation & VI.Tild)
                Chaine.Append(Nature_Garanties & VI.Tild)
                Chaine.Append(Date_Depot & VI.Tild)
                Chaine.Append(Date_Affiliation & VI.Tild)
                Chaine.Append(Date_Radiation & VI.Tild)
                Chaine.Append(PersonneGarantie)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Nom = TableauData(1)
                NumeroAffiliation = TableauData(2)
                Nature_Garanties = TableauData(3)
                Date_Depot = TableauData(4)
                Date_Affiliation = TableauData(5)
                Date_Radiation = TableauData(6)
                PersonneGarantie = TableauData(7)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 1
                        Return WsNom
                    Case 2
                        Return WsNumeroAffiliation
                    Case 3
                        Return WsNatureGaranties
                    Case 4
                        Return WsDate_Depot
                    Case 5
                        Return WsDate_Affiliation
                    Case 6
                        Return WsDate_Radiation
                    Case 7
                        Return WsPersonneGarantie
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

