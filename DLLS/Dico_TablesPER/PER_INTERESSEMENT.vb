﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_INTERESSEMENT
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsMontantTotal As Double
        Private WsPotentielEpargne As Double
        Private WsHorsEpargne As Double
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_INTERESSEMENT"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaInteressement
            End Get
        End Property

        Public Property MontantTotal() As Double
            Get
                Return WsMontantTotal
            End Get
            Set(ByVal value As Double)
                WsMontantTotal = value
                MyBase.Clef = WsMontantTotal.ToString
            End Set
        End Property

        Public Property PotentielEpargne() As Double
            Get
                Return WsPotentielEpargne
            End Get
            Set(ByVal value As Double)
                WsPotentielEpargne = value
            End Set
        End Property

        Public Property HorsEpargne() As Double
            Get
                Return WsHorsEpargne
            End Get
            Set(ByVal value As Double)
                WsHorsEpargne = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(MontantTotal.ToString & VI.Tild)
                Chaine.Append(PotentielEpargne.ToString & VI.Tild)
                Chaine.Append(HorsEpargne.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 5 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) = "" Then TableauData(2) = "0"
                MontantTotal = VirRhFonction.ConversionDouble(TableauData(2))
                If TableauData(3) = "" Then TableauData(3) = "0"
                PotentielEpargne = VirRhFonction.ConversionDouble(TableauData(3))
                If TableauData(4) = "" Then TableauData(4) = "0"
                HorsEpargne = VirRhFonction.ConversionDouble(TableauData(4))

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return Date_de_Valeur
                    Case 1
                        Return WsMontantTotal.ToString
                    Case 2
                        Return WsPotentielEpargne.ToString
                    Case 3
                        Return WsHorsEpargne.ToString
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


