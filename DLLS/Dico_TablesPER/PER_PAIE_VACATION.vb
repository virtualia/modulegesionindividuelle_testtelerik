﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_PAIE_VACATION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_Debut As String
        Private WsCodeAnalytique As String
        Private WsAnneeUniversitaire As String
        Private WsCodeEnseignement As String
        Private WsIntituleEnseignement As String
        Private WsVolumeHoraire_CoursMagistraux As Double
        Private WsVolumeHoraire_TD As Double
        Private WsVolumeHoraire_TP As Double
        Private WsVolumeHoraire_EquiHED As Double
        Private WsMontantRemu_EquiHED As Double
        Private WsDate_Paiement As String
        Private WsSiEDite As String
        Private WsSiEnAttente As String
        Private WsObservations As String
        '
        Private TsDate_de_fin As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_PAIE_VACATION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaPaieVacation 'Spécifique Secteur Public
            End Get
        End Property

        Public Property Date_Debut() As String
            Get
                Return WsDate_Debut
            End Get
            Set(ByVal value As String)
                WsDate_Debut = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property CodeAnalytique() As String
            Get
                Return WsCodeAnalytique
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsCodeAnalytique = value
                    Case Else
                        WsCodeAnalytique = Strings.Left(value, 10)
                End Select
                MyBase.Clef = WsCodeAnalytique
            End Set
        End Property

        Public Property AnneeUniversitaire() As String
            Get
                Return WsAnneeUniversitaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 12
                        WsAnneeUniversitaire = value
                    Case Else
                        WsAnneeUniversitaire = Strings.Left(value, 12)
                End Select
            End Set
        End Property

        Public Property CodeEnseignement() As String
            Get
                Return WsCodeEnseignement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsCodeEnseignement = value
                    Case Else
                        WsCodeEnseignement = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property IntituleEnseignement() As String
            Get
                Return WsIntituleEnseignement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsIntituleEnseignement = value
                    Case Else
                        WsIntituleEnseignement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property VolumeHoraire_CoursMagistraux() As Double
            Get
                Return WsVolumeHoraire_CoursMagistraux
            End Get
            Set(ByVal value As Double)
                WsVolumeHoraire_CoursMagistraux = value
            End Set
        End Property

        Public Property VolumeHoraire_TravauxDiriges() As Double
            Get
                Return WsVolumeHoraire_TD
            End Get
            Set(ByVal value As Double)
                WsVolumeHoraire_TD = value
            End Set
        End Property

        Public Property VolumeHoraire_TravauxPratiques() As Double
            Get
                Return WsVolumeHoraire_TP
            End Get
            Set(ByVal value As Double)
                WsVolumeHoraire_TP = value
            End Set
        End Property

        Public Property VolumeHoraire_EquivalentHED() As Double
            Get
                Return WsVolumeHoraire_EquiHED
            End Get
            Set(ByVal value As Double)
                WsVolumeHoraire_EquiHED = value
            End Set
        End Property

        Public Property MontantRemuneration_EquivalentHED() As Double
            Get
                Return WsMontantRemu_EquiHED
            End Get
            Set(ByVal value As Double)
                WsMontantRemu_EquiHED = value
            End Set
        End Property

        Public Property Date_de_Paiement() As String
            Get
                Return WsDate_Paiement
            End Get
            Set(ByVal value As String)
                WsDate_Paiement = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property SiEdite() As String
            Get
                Return WsSiEDite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiEDite = value
                    Case Else
                        WsSiEDite = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property SiEnAttente() As String
            Get
                Return WsSiEnAttente
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiEnAttente = value
                    Case Else
                        WsSiEnAttente = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_Debut & VI.Tild)
                Chaine.Append(CodeAnalytique & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(AnneeUniversitaire & VI.Tild)
                Chaine.Append(CodeEnseignement & VI.Tild)
                Chaine.Append(IntituleEnseignement & VI.Tild)
                Chaine.Append(VolumeHoraire_CoursMagistraux.ToString & VI.Tild)
                Chaine.Append(VolumeHoraire_TravauxDiriges.ToString & VI.Tild)
                Chaine.Append(VolumeHoraire_TravauxPratiques.ToString & VI.Tild)
                Chaine.Append(VolumeHoraire_EquivalentHED.ToString & VI.Tild)
                Chaine.Append(MontantRemuneration_EquivalentHED.ToString & VI.Tild)
                Chaine.Append(Date_de_Paiement & VI.Tild)
                Chaine.Append(SiEdite & VI.Tild)
                Chaine.Append(SiEnAttente & VI.Tild)
                Chaine.Append(Observations)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 16 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_Debut = TableauData(1)
                CodeAnalytique = TableauData(2)
                MyBase.Date_de_Fin = TableauData(3)
                AnneeUniversitaire = TableauData(4)
                CodeEnseignement = TableauData(5)
                IntituleEnseignement = TableauData(6)
                If TableauData(7) = "" Then TableauData(7) = "0"
                VolumeHoraire_CoursMagistraux = VirRhFonction.ConversionDouble(TableauData(7))
                If TableauData(8) = "" Then TableauData(8) = "0"
                VolumeHoraire_TravauxDiriges = VirRhFonction.ConversionDouble(TableauData(8))
                If TableauData(9) = "" Then TableauData(9) = "0"
                VolumeHoraire_TravauxPratiques = VirRhFonction.ConversionDouble(TableauData(9))
                If TableauData(10) = "" Then TableauData(10) = "0"
                VolumeHoraire_EquivalentHED = VirRhFonction.ConversionDouble(TableauData(10))
                If TableauData(11) = "" Then TableauData(11) = "0"
                MontantRemuneration_EquivalentHED = VirRhFonction.ConversionDouble(TableauData(11))
                Date_de_Paiement = TableauData(12)
                SiEdite = TableauData(13)
                SiEnAttente = TableauData(14)
                Observations = TableauData(15)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = Date_de_fin
            End Set
        End Property

        Public Property VDate_de_Fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_Debut
                    Case 1
                        Return WsCodeAnalytique
                    Case 2
                        Return Date_de_Fin
                    Case 3
                        Return WsAnneeUniversitaire
                    Case 4
                        Return WsCodeEnseignement
                    Case 5
                        Return WsIntituleEnseignement
                    Case 6
                        Return WsVolumeHoraire_CoursMagistraux.ToString
                    Case 7
                        Return WsVolumeHoraire_TD.ToString
                    Case 8
                        Return WsVolumeHoraire_TP.ToString
                    Case 9
                        Return WsVolumeHoraire_EquiHED.ToString
                    Case 10
                        Return WsMontantRemu_EquiHED.ToString
                    Case 11
                        Return WsDate_Paiement
                    Case 12
                        Return WsSiEDite
                    Case 13
                        Return WsSiEnAttente
                    Case 14
                        Return WsObservations
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

