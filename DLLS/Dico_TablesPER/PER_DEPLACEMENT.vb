﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_DEPLACEMENT
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsLibelleObjet As String
        Private WsMoyen As String
        Private WsHeure_DepartResidence As String
        Private WsHeure_ArriveeLieuMission As String
        Private WsDate_de_Retour As String
        Private WsHeure_DepartLieuMission As String
        Private WsHeure_ArriveeResidence As String
        Private WsMontant_Avion As Double = 0
        Private WsMontant_Train As Double = 0
        Private WsMontant_Taxi As Double = 0
        Private WsMontant_Location As Double = 0
        Private WsCategorieVehicule As Integer = 0
        Private WsNombreKmsParcourus As Double = 0
        Private WsCodePays As String
        Private WsGroupe As Integer = 0
        Private WsNombre_RestosAdministratifs As Integer = 0
        Private WsNombre_Repas As Integer = 0
        Private WsNombre_Nuitees As Integer = 0
        Private WsNombre_Journees As Double = 0
        Private WsNombre_Repas_Gratuits As Integer = 0
        Private WsNombre_Nuitees_Gratuits As Integer = 0
        Private WsMontant_Parking As Double = 0
        Private WsMontant_Divers As Double = 0
        Private WsMontant_Avance As Double = 0
        Private WsDate_Liquidation As String
        Private WsNumero_Liquidation As String
        Private WsDate_Certification As String
        Private WsTauxChange As Double = 0
        Private WsNumLieu_Deplacement As Integer = 0
        Private WsDate_ArriveeLieuMission As String
        Private WsDate_DepartLieuMission As String
        Private WsNumero_Mission As String
        Private WsSiRetourQuotidien As Integer = 0
        Private WsNumero_Engagement As String
        Private WsSiPrisEnCharge As Integer = 0
        Private WsObservations As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_DEPLACEMENT" 'Secteur Public
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaFrais
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 37
            End Get
        End Property
        Public Property LibelleObjet() As String
            Get
                Return WsLibelleObjet
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsLibelleObjet = value
                    Case Else
                        WsLibelleObjet = Strings.Left(value, 120)
                End Select
                MyBase.Clef = WsLibelleObjet
            End Set
        End Property

        Public Property Moyen() As String
            Get
                Return WsMoyen
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsMoyen = value
                    Case Else
                        WsMoyen = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Heure_Depart_Residence() As String
            Get
                Return WsHeure_DepartResidence
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeure_DepartResidence = value
                    Case Else
                        WsHeure_DepartResidence = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Heure_Arrivee_LieuMission() As String
            Get
                Return WsHeure_ArriveeLieuMission
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeure_ArriveeLieuMission = value
                    Case Else
                        WsHeure_ArriveeLieuMission = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Date_de_Retour() As String
            Get
                Return WsDate_de_Retour
            End Get
            Set(ByVal value As String)
                WsDate_de_Retour = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Heure_Depart_LieuMission() As String
            Get
                Return WsHeure_DepartLieuMission
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeure_DepartLieuMission = value
                    Case Else
                        WsHeure_DepartLieuMission = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Heure_Arrivee_Residence() As String
            Get
                Return WsHeure_ArriveeResidence
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeure_ArriveeResidence = value
                    Case Else
                        WsHeure_ArriveeResidence = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Montant_Avion() As Double
            Get
                Return WsMontant_Avion
            End Get
            Set(ByVal value As Double)
                WsMontant_Avion = value
            End Set
        End Property

        Public Property Montant_Train() As Double
            Get
                Return WsMontant_Train
            End Get
            Set(ByVal value As Double)
                WsMontant_Train = value
            End Set
        End Property

        Public Property Montant_Taxi() As Double
            Get
                Return WsMontant_Taxi
            End Get
            Set(ByVal value As Double)
                WsMontant_Taxi = value
            End Set
        End Property

        Public Property Montant_Location() As Double
            Get
                Return WsMontant_Location
            End Get
            Set(ByVal value As Double)
                WsMontant_Location = value
            End Set
        End Property

        Public Property Categorie_Vehicule() As Integer
            Get
                Return WsCategorieVehicule
            End Get
            Set(ByVal value As Integer)
                WsCategorieVehicule = value
            End Set
        End Property

        Public Property Nombre_Kms_Parcourus() As Double
            Get
                Return WsNombreKmsParcourus
            End Get
            Set(ByVal value As Double)
                WsNombreKmsParcourus = value
            End Set
        End Property

        Public Property CodePays() As String
            Get
                Return WsCodePays
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsCodePays = value
                    Case Else
                        WsCodePays = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Groupe() As Integer
            Get
                Return WsGroupe
            End Get
            Set(ByVal value As Integer)
                WsGroupe = value
            End Set
        End Property

        Public Property Nombre_Restos_Administratifs() As Integer
            Get
                Return WsNombre_RestosAdministratifs
            End Get
            Set(ByVal value As Integer)
                WsNombre_RestosAdministratifs = value
            End Set
        End Property

        Public Property Nombre_Repas() As Integer
            Get
                Return WsNombre_Repas
            End Get
            Set(ByVal value As Integer)
                WsNombre_Repas = value
            End Set
        End Property

        Public Property Nombre_Nuitees() As Integer
            Get
                Return WsNombre_Nuitees
            End Get
            Set(ByVal value As Integer)
                WsNombre_Nuitees = value
            End Set
        End Property

        Public Property Nombre_Journees() As Double
            Get
                Return WsNombre_Journees
            End Get
            Set(ByVal value As Double)
                WsNombre_Journees = value
            End Set
        End Property

        Public Property Nombre_Repas_Gratuits() As Integer
            Get
                Return WsNombre_Repas_Gratuits
            End Get
            Set(ByVal value As Integer)
                WsNombre_Repas_Gratuits = value
            End Set
        End Property

        Public Property Nombre_Nuitees_Gratuites() As Integer
            Get
                Return WsNombre_Nuitees_Gratuits
            End Get
            Set(ByVal value As Integer)
                WsNombre_Nuitees_Gratuits = value
            End Set
        End Property

        Public Property Montant_Parking() As Double
            Get
                Return WsMontant_Parking
            End Get
            Set(ByVal value As Double)
                WsMontant_Parking = value
            End Set
        End Property

        Public Property Montant_Divers() As Double
            Get
                Return WsMontant_Divers
            End Get
            Set(ByVal value As Double)
                WsMontant_Divers = value
            End Set
        End Property

        Public Property Montant_Avance() As Double
            Get
                Return WsMontant_Avance
            End Get
            Set(ByVal value As Double)
                WsMontant_Avance = value
            End Set
        End Property

        Public Property Date_de_Liquidation() As String
            Get
                Return WsDate_Liquidation
            End Get
            Set(ByVal value As String)
                WsDate_Liquidation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Numero_Liquidation() As String
            Get
                Return WsNumero_Liquidation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 15
                        WsNumero_Liquidation = value
                    Case Else
                        WsNumero_Liquidation = Strings.Left(value, 15)
                End Select
            End Set
        End Property

        Public Property Date_de_Certification() As String
            Get
                Return WsDate_Certification
            End Get
            Set(ByVal value As String)
                WsDate_Certification = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Taux_de_Change() As Double
            Get
                Return WsTauxChange
            End Get
            Set(ByVal value As Double)
                WsTauxChange = value
            End Set
        End Property

        Public Property Numero_Lieu() As Integer
            Get
                Return WsNumLieu_Deplacement
            End Get
            Set(ByVal value As Integer)
                WsNumLieu_Deplacement = value
            End Set
        End Property

        Public Property Date_Arrivee_LieuMission() As String
            Get
                Return WsDate_ArriveeLieuMission
            End Get
            Set(ByVal value As String)
                WsDate_ArriveeLieuMission = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Depart_LieuMission() As String
            Get
                Return WsDate_DepartLieuMission
            End Get
            Set(ByVal value As String)
                WsDate_DepartLieuMission = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Numero_Mission() As String
            Get
                Return WsNumero_Mission
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsNumero_Mission = value
                    Case Else
                        WsNumero_Mission = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property SiRetourQuotidien() As Integer
            Get
                Return WsSiRetourQuotidien
            End Get
            Set(ByVal value As Integer)
                WsSiRetourQuotidien = value
            End Set
        End Property

        Public Property Numero_Engagement() As String
            Get
                Return WsNumero_Engagement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 15
                        WsNumero_Engagement = value
                    Case Else
                        WsNumero_Engagement = Strings.Left(value, 15)
                End Select
            End Set
        End Property

        Public Property SiPriseEnCharge() As Integer
            Get
                Return WsSiPrisEnCharge
            End Get
            Set(ByVal value As Integer)
                WsSiPrisEnCharge = value
            End Set
        End Property

        Public Property Observation() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder
                Chaine = New System.Text.StringBuilder
                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(LibelleObjet & VI.Tild)
                Chaine.Append(Moyen & VI.Tild)
                Chaine.Append(Heure_Depart_Residence & VI.Tild)
                Chaine.Append(Heure_Arrivee_LieuMission & VI.Tild)
                Chaine.Append(Date_de_Retour & VI.Tild)
                Chaine.Append(Heure_Depart_LieuMission & VI.Tild)
                Chaine.Append(Heure_Arrivee_Residence & VI.Tild)
                Select Case Montant_Avion
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_Avion.ToString) & VI.Tild)
                End Select
                Select Case Montant_Train
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_Train.ToString) & VI.Tild)
                End Select
                Select Case Montant_Taxi
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_Taxi.ToString) & VI.Tild)
                End Select
                Select Case Montant_Location
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_Location.ToString) & VI.Tild)
                End Select
                Chaine.Append(Categorie_Vehicule & VI.Tild)
                Select Case Nombre_Kms_Parcourus
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Nombre_Kms_Parcourus.ToString) & VI.Tild)
                End Select
                Chaine.Append(CodePays & VI.Tild)
                Chaine.Append(Groupe & VI.Tild)
                Chaine.Append(Nombre_Restos_Administratifs & VI.Tild)
                Chaine.Append(Nombre_Repas & VI.Tild)
                Chaine.Append(Nombre_Nuitees & VI.Tild)
                Select Case Nombre_Journees
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Nombre_Journees.ToString) & VI.Tild)
                End Select
                Chaine.Append(Nombre_Repas_Gratuits & VI.Tild)
                Chaine.Append(Nombre_Nuitees_Gratuites & VI.Tild)
                Select Case Montant_Parking
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_Parking.ToString) & VI.Tild)
                End Select
                Select Case Montant_Divers
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_Divers.ToString) & VI.Tild)
                End Select
                Select Case Montant_Avance
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_Avance.ToString) & VI.Tild)
                End Select
                Chaine.Append(Date_de_Liquidation & VI.Tild)
                Chaine.Append(Numero_Liquidation & VI.Tild)
                Chaine.Append(Date_de_Certification & VI.Tild)
                Select Case Taux_de_Change
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Taux_de_Change.ToString) & VI.Tild)
                End Select
                Chaine.Append(Numero_Lieu & VI.Tild)
                Chaine.Append(Date_Arrivee_LieuMission & VI.Tild)
                Chaine.Append(Date_Depart_LieuMission & VI.Tild)
                Chaine.Append(Numero_Mission & VI.Tild)
                Chaine.Append(SiRetourQuotidien & VI.Tild)
                Chaine.Append(Numero_Engagement & VI.Tild)
                Chaine.Append(SiPriseEnCharge & VI.Tild)
                Chaine.Append(Observation & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 39 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                LibelleObjet = TableauData(2)
                Moyen = TableauData(3)
                Heure_Depart_Residence = TableauData(4)
                Heure_Arrivee_LieuMission = TableauData(5)
                Date_de_Retour = TableauData(6)
                Heure_Depart_LieuMission = TableauData(7)
                Heure_Arrivee_Residence = TableauData(8)

                If TableauData(9) <> "" Then Montant_Avion = VirRhFonction.ConversionDouble(TableauData(9))
                If TableauData(10) <> "" Then Montant_Train = VirRhFonction.ConversionDouble(TableauData(10))
                If TableauData(11) <> "" Then Montant_Taxi = VirRhFonction.ConversionDouble(TableauData(11))
                If TableauData(12) <> "" Then Montant_Location = VirRhFonction.ConversionDouble(TableauData(12))
                If TableauData(13) <> "" Then Categorie_Vehicule = CInt(VirRhFonction.ConversionDouble(TableauData(13), 0))
                If TableauData(14) <> "" Then Nombre_Kms_Parcourus = VirRhFonction.ConversionDouble(TableauData(14))
                CodePays = TableauData(15)
                For IndiceI = 16 To 19
                    If TableauData(IndiceI) = "" Then
                        TableauData(IndiceI) = "0"
                    End If
                Next IndiceI
                Groupe = CInt(VirRhFonction.ConversionDouble(TableauData(16), 0))
                Nombre_Restos_Administratifs = CInt(VirRhFonction.ConversionDouble(TableauData(17), 0))
                Nombre_Repas = CInt(VirRhFonction.ConversionDouble(TableauData(18), 0))
                Nombre_Nuitees = CInt(VirRhFonction.ConversionDouble(TableauData(19), 0))
                If TableauData(20) <> "" Then Nombre_Journees = VirRhFonction.ConversionDouble(TableauData(20), 1)
                For IndiceI = 21 To 22
                    If TableauData(IndiceI) = "" Then
                        TableauData(IndiceI) = "0"
                    Else
                        TableauData(IndiceI) = VirRhFonction.ConversionDouble(TableauData(IndiceI), 0)
                    End If
                Next IndiceI
                Nombre_Repas_Gratuits = CInt(VirRhFonction.ConversionDouble(TableauData(21), 0))
                Nombre_Nuitees_Gratuites = CInt(VirRhFonction.ConversionDouble(TableauData(22), 0))
                If TableauData(23) <> "" Then Montant_Parking = VirRhFonction.ConversionDouble(TableauData(23))
                If TableauData(24) <> "" Then Montant_Divers = VirRhFonction.ConversionDouble(TableauData(24))
                If TableauData(25) <> "" Then Montant_Avance = VirRhFonction.ConversionDouble(TableauData(25))
                Date_de_Liquidation = TableauData(26)
                Numero_Liquidation = TableauData(27)
                Date_de_Certification = TableauData(28)
                For IndiceI = 29 To 30
                    If TableauData(IndiceI) = "" Then
                        TableauData(IndiceI) = "0"
                    End If
                Next IndiceI
                Taux_de_Change = VirRhFonction.ConversionDouble(TableauData(29))
                Numero_Lieu = CInt(VirRhFonction.ConversionDouble(TableauData(30)))
                Date_Arrivee_LieuMission = TableauData(31)
                Date_Depart_LieuMission = TableauData(32)
                Numero_Mission = TableauData(33)
                If TableauData(34) = "" Then
                    TableauData(34) = "0"
                End If
                SiRetourQuotidien = CInt(VirRhFonction.ConversionDouble(TableauData(34), 0))
                Numero_Engagement = TableauData(35)
                If TableauData(36) = "" Then
                    TableauData(36) = "0"
                End If
                SiPriseEnCharge = CInt(VirRhFonction.ConversionDouble(TableauData(36), 0))
                Observation = TableauData(37)
                MyBase.Certification = TableauData(38)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
