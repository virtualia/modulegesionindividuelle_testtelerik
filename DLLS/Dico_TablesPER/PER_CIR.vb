﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_CIR
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        Private WsRang As Integer 'Numéro d'ordre Virtualia
        '
        Private WsCodeArticle As String 'Alpha 2 de 00 à 05 cf catalogue 40
        Private WsTypeMvt As String 'Alpha 2 de ST à EN cf catalogue 41
        Private WsDateEnvoi As String
        Private WsContenuArticle As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_CIR"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaCIR 'Spécifique Secteur Public - Compte Individuel de retraite CIR
            End Get
        End Property

        Public Property Rang As Integer
            Get
                Return WsRang
            End Get
            Set(ByVal value As Integer)
                WsRang = value
            End Set
        End Property

        Public Property Code_Article As String
            Get
                Return WsCodeArticle
            End Get
            Set(ByVal value As String)
                WsCodeArticle = F_FormatAlpha_CIR(value, 2, True)
            End Set
        End Property

        Public Property Type_Mouvement As String
            Get
                Return WsTypeMvt
            End Get
            Set(ByVal value As String)
                WsTypeMvt = F_FormatAlpha_CIR(value, 2)
            End Set
        End Property

        Public Property Date_Envoi() As String
            Get
                Return WsDateEnvoi
            End Get
            Set(ByVal value As String)
                WsDateEnvoi = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Contenu_Article As String
            Get
                Return WsContenuArticle
            End Get
            Set(ByVal value As String)
                WsContenuArticle = F_FormatAlpha(value, 800)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Rang.ToString & VI.Tild)
                Chaine.Append(Code_Article & VI.Tild)
                Chaine.Append(Type_Mouvement & VI.Tild)
                Chaine.Append(Date_Envoi & VI.Tild)
                Chaine.Append(Contenu_Article)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 5 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) = "" Then TableauData(1) = "1"
                Rang = CInt(TableauData(1))
                Code_Article = TableauData(2)
                Type_Mouvement = TableauData(3)
                Date_Envoi = TableauData(4)
                Contenu_Article = TableauData(5)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        '**** Fonctions de Mise en forme communes à tous les mouvements CIR

        Public ReadOnly Property F_FormatAlpha_CIR(ByVal Valeur As String, ByVal Longueur As Integer, Optional ByVal SiBlancZero As Boolean = False) As String
            Get
                If SiBlancZero Then
                    If Valeur = Strings.StrDup(Valeur.Length, "0") Then
                        Valeur = ""
                    End If
                End If
                Dim Chaine As String = Valeur & Strings.Space(Longueur)
                Return Strings.Left(Chaine, Longueur)
            End Get
        End Property

        Public ReadOnly Property F_FormatNumerique_CIR(ByVal Valeur As String, ByVal Longueur As Integer) As String
            Get
                If Valeur.Length > 0 Then
                    If Strings.Left(Valeur, 1) = "Z" Then
                        Return Strings.StrDup(Longueur, "Z")
                    End If
                End If
                Dim Chaine As String = Strings.StrDup(Longueur, "0") & Valeur
                Return Strings.Right(Chaine, Longueur)
            End Get
        End Property

        Public ReadOnly Property F_FormatNumerique_CIR(ByVal Valeur As String, ByVal Longueur As Integer, ByVal SiBlanc As Boolean) As String
            Get
                If Valeur.Length > 0 Then
                    If Strings.Left(Valeur, 1) = "Z" Then
                        Return Strings.StrDup(Longueur, "Z")
                    End If
                End If
                If SiBlanc = True Then
                    If Valeur.Trim = "" Or Val(Strings.Replace(Valeur, ",", ".")) = 0 Then
                        Return Strings.Space(Longueur)
                    End If
                End If
                Dim Chaine As String = Strings.StrDup(Longueur, "0") & Valeur
                Return Strings.Right(Chaine, Longueur)
            End Get
        End Property

        Public ReadOnly Property F_FormatNumerique_CIR(ByVal Valeur As String, ByVal Longueur As Integer, ByVal NbDecimales As Integer, Optional ByVal SiBlanc As Boolean = False) As String
            Get
                If Valeur.Length > 0 Then
                    If Strings.Left(Valeur, 1) = "Z" Then
                        Return Strings.StrDup(Longueur, "Z")
                    End If
                End If
                If SiBlanc = True Then
                    If Valeur.Trim = "" Or Val(Strings.Replace(Valeur, ",", ".")) = 0 Then
                        Return Strings.Space(Longueur)
                    End If
                End If

                Dim PartieEntiere As String
                Dim PartieDecimale As String
                Dim Chaine As String
                Dim PositionSymbole As Integer

                If NbDecimales = 0 Then
                    PartieEntiere = Strings.StrDup(Longueur, "0") & Valeur
                    Return Strings.Right(PartieEntiere, Longueur)
                End If

                PositionSymbole = Strings.InStr(Valeur, VI.PointFinal)
                If PositionSymbole = 0 Then
                    PositionSymbole = Strings.InStr(Valeur, VI.Virgule)
                End If

                If PositionSymbole = 0 Then
                    Chaine = Strings.StrDup(Longueur, "0") & Valeur
                    PartieEntiere = Strings.Right(Chaine, Longueur - NbDecimales)
                    Return PartieEntiere & Strings.StrDup(NbDecimales, "0")
                End If

                Chaine = Strings.StrDup(Longueur, "0") & Strings.Left(Valeur, PositionSymbole - 1)
                PartieEntiere = Strings.Right(Chaine, Longueur - NbDecimales)
                Chaine = Strings.Right(Valeur, Valeur.Length - PositionSymbole) & Strings.StrDup(NbDecimales, "0")
                PartieDecimale = Strings.Left(Chaine, NbDecimales)

                Return PartieEntiere & PartieDecimale
            End Get
        End Property

        Public ReadOnly Property F_FormatDate_CIR(ByVal ArgumentDate As String, ByVal FormatSortie As String, Optional ByVal CaractereRemplissage As String = " ") As String
            Get
                'ArgumentDate au format Virtualia - JJ/MM/SSAAA
                Dim ChaineRes As String = Strings.StrDup(FormatSortie.Length, CaractereRemplissage)
                Dim JJ As String
                Dim MM As String
                Dim AAAA As String

                If ArgumentDate.Length <> 10 Then
                    Return ChaineRes
                End If
                JJ = Strings.Left(ArgumentDate, 2)
                MM = Strings.Mid(ArgumentDate, 4, 2)
                AAAA = Strings.Right(ArgumentDate, 4)
                Select Case FormatSortie
                    Case Is = "AAMM"
                        ChaineRes = Strings.Right(AAAA, 2) & MM
                    Case Is = "AAAAMMJJ"
                        ChaineRes = Strings.Right(AAAA, 2) & MM & JJ
                    Case Is = "AAMMJJ"
                        ChaineRes = AAAA & MM & JJ
                End Select
                Return ChaineRes
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class

End Namespace

