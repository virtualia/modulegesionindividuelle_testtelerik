﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_CUMUL_EMPLOI
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_Debut As String
        Private WsTypeCumul As String
        Private WsOrganisme As String
        Private WsMontantNet As Double
        Private WsMontantBrut As Double
        Private WsDate_Arrete As String
        Private WsNatureActivite As String
        Private WsNombreHeures As String
        Private WsObservations As String
        Private WsVolumeHoraire_EquiHED As Double
        Private WsVolumeHoraire_CoursMagistraux As Double
        Private WsVolumeHoraire_TD As Double
        Private WsVolumeHoraire_TP As Double
        '
        Private TsDate_de_fin As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_CUMUL_EMPLOI"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaCumulRem 'Spécifique Secteur Public
            End Get
        End Property

        Public Property Date_Debut() As String
            Get
                Return WsDate_Debut
            End Get
            Set(ByVal value As String)
                WsDate_Debut = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_Debut
            End Set
        End Property

        Public Property Type_Cumul() As String
            Get
                Return WsTypeCumul
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsTypeCumul = value
                    Case Else
                        WsTypeCumul = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsTypeCumul
            End Set
        End Property

        Public Property Organisme() As String
            Get
                Return WsOrganisme
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsOrganisme = value
                    Case Else
                        WsOrganisme = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property MontantNet_Percu() As Double
            Get
                Return WsMontantNet
            End Get
            Set(ByVal value As Double)
                WsMontantNet = value
            End Set
        End Property

        Public Property MontantBrut_Percu() As Double
            Get
                Return WsMontantBrut
            End Get
            Set(ByVal value As Double)
                WsMontantBrut = value
            End Set
        End Property

        Public Property Date_Arrete() As String
            Get
                Return WsDate_Arrete
            End Get
            Set(ByVal value As String)
                WsDate_Arrete = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property NatureActivite() As String
            Get
                Return WsNatureActivite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsNatureActivite = value
                    Case Else
                        WsNatureActivite = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property NombreHeures() As String
            Get
                Return WsNombreHeures
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 7
                        WsNombreHeures = value
                    Case Else
                        WsNombreHeures = Strings.Left(value, 7)
                End Select
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property VolumeHoraire_EquivalentHED() As Double
            Get
                Return WsVolumeHoraire_EquiHED
            End Get
            Set(ByVal value As Double)
                WsVolumeHoraire_EquiHED = value
            End Set
        End Property

        Public Property VolumeHoraire_CoursMagistraux() As Double
            Get
                Return WsVolumeHoraire_CoursMagistraux
            End Get
            Set(ByVal value As Double)
                WsVolumeHoraire_CoursMagistraux = value
            End Set
        End Property

        Public Property VolumeHoraire_TravauxDiriges() As Double
            Get
                Return WsVolumeHoraire_TD
            End Get
            Set(ByVal value As Double)
                WsVolumeHoraire_TD = value
            End Set
        End Property

        Public Property VolumeHoraire_TravauxPratiques() As Double
            Get
                Return WsVolumeHoraire_TP
            End Get
            Set(ByVal value As Double)
                WsVolumeHoraire_TP = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_Debut & VI.Tild)
                Chaine.Append(Type_Cumul & VI.Tild)
                Chaine.Append(Organisme & VI.Tild)
                Chaine.Append(MontantNet_Percu.ToString & VI.Tild)
                Chaine.Append(MontantBrut_Percu.ToString & VI.Tild)
                Chaine.Append(Date_Arrete & VI.Tild)
                Chaine.Append(NatureActivite & VI.Tild)
                Chaine.Append(NombreHeures & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(VolumeHoraire_EquivalentHED.ToString & VI.Tild)
                Chaine.Append(VolumeHoraire_CoursMagistraux.ToString & VI.Tild)
                Chaine.Append(VolumeHoraire_TravauxDiriges.ToString & VI.Tild)
                Chaine.Append(VolumeHoraire_TravauxPratiques.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 15 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_Debut = TableauData(1)
                Type_Cumul = TableauData(2)
                Organisme = TableauData(3)
                If TableauData(4) = "" Then TableauData(4) = "0"
                MontantNet_Percu = VirRhFonction.ConversionDouble(TableauData(4))
                If TableauData(5) = "" Then TableauData(5) = "0"
                MontantBrut_Percu = VirRhFonction.ConversionDouble(TableauData(5))
                Date_Arrete = TableauData(6)
                NatureActivite = TableauData(7)
                NombreHeures = TableauData(8)
                MyBase.Date_de_Fin = TableauData(9)
                Observations = TableauData(10)
                If TableauData(11) = "" Then TableauData(11) = "0"
                VolumeHoraire_EquivalentHED = VirRhFonction.ConversionDouble(TableauData(11))
                If TableauData(12) = "" Then TableauData(12) = "0"
                VolumeHoraire_CoursMagistraux = VirRhFonction.ConversionDouble(TableauData(12))
                If TableauData(13) = "" Then TableauData(13) = "0"
                VolumeHoraire_TravauxDiriges = VirRhFonction.ConversionDouble(TableauData(13))
                If TableauData(14) = "" Then TableauData(14) = "0"
                VolumeHoraire_TravauxPratiques = VirRhFonction.ConversionDouble(TableauData(14))

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = Date_de_fin
            End Set
        End Property

        Public Property VDate_de_Fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_Debut
                    Case 1
                        Return WsTypeCumul
                    Case 2
                        Return WsOrganisme
                    Case 3
                        Return WsMontantNet.ToString
                    Case 4
                        Return WsMontantBrut.ToString
                    Case 5
                        Return WsDate_Arrete
                    Case 6
                        Return WsNatureActivite
                    Case 7
                        Return WsNombreHeures
                    Case 8
                        Return Date_de_Fin
                    Case 9
                        Return WsObservations
                    Case 10
                        Return WsVolumeHoraire_EquiHED.ToString
                    Case 11
                        Return WsVolumeHoraire_CoursMagistraux.ToString
                    Case 12
                        Return WsVolumeHoraire_TD.ToString
                    Case 13
                        Return WsVolumeHoraire_TP.ToString
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

