﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_TRAVAIL
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_d_effet As String
        Private WsLundi_Matin As String
        Private WsLundi_ApresMidi As String
        Private WsMardi_Matin As String
        Private WsMardi_ApresMidi As String
        Private WsMercredi_Matin As String
        Private WsMercredi_ApresMidi As String
        Private WsJeudi_Matin As String
        Private WsJeudi_ApresMidi As String
        Private WsVendredi_Matin As String
        Private WsVendredi_ApresMidi As String
        Private WsSamedi_Matin As String
        Private WsSamedi_ApresMidi As String
        Private WsDimanche_Matin As String
        Private WsDimanche_ApresMidi As String
        Private WsCycleTravail As String
        Private WsHoraireJournalier As String
        Private WsObservations As String
        Private WsSiJFTravailles As String
        Private WsTypeHoraire As String
        '
        Private TsDate_de_fin As String
        Private TsTypeCycle As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_TRAVAIL"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaPresence
            End Get
        End Property

        Public Property Date_d_effet() As String
            Get
                Return WsDate_d_effet
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is <> ""
                        WsDate_d_effet = VirRhDate.DateStandardVirtualia(value)
                    Case Else
                        WsDate_d_effet = ""
                End Select
                MyBase.Date_de_Valeur = WsDate_d_effet
            End Set
        End Property

        Public Property Lundi_Matin() As String
            Get
                Return WsLundi_Matin
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsLundi_Matin = value
                    Case Else
                        WsLundi_Matin = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Lundi_ApresMidi() As String
            Get
                Return WsLundi_ApresMidi
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsLundi_ApresMidi = value
                    Case Else
                        WsLundi_ApresMidi = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Mardi_ApresMidi() As String
            Get
                Return WsMardi_ApresMidi
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsMardi_ApresMidi = value
                    Case Else
                        WsMardi_ApresMidi = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Mardi_Matin() As String
            Get
                Return WsMardi_Matin
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsMardi_Matin = value
                    Case Else
                        WsMardi_Matin = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Mercredi_Matin() As String
            Get
                Return WsMercredi_Matin
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsMercredi_Matin = value
                    Case Else
                        WsMercredi_Matin = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Mercredi_ApresMidi() As String
            Get
                Return WsMercredi_ApresMidi
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsMercredi_ApresMidi = value
                    Case Else
                        WsMercredi_ApresMidi = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Jeudi_Matin() As String
            Get
                Return WsJeudi_Matin
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsJeudi_Matin = value
                    Case Else
                        WsJeudi_Matin = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Jeudi_ApresMidi() As String
            Get
                Return WsJeudi_ApresMidi
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsJeudi_ApresMidi = value
                    Case Else
                        WsJeudi_ApresMidi = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Vendredi_Matin() As String
            Get
                Return WsVendredi_Matin
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsVendredi_Matin = value
                    Case Else
                        WsVendredi_Matin = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Vendredi_ApresMidi() As String
            Get
                Return WsVendredi_ApresMidi
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsVendredi_ApresMidi = value
                    Case Else
                        WsVendredi_ApresMidi = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Samedi_Matin() As String
            Get
                Return WsSamedi_Matin
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsSamedi_Matin = value
                    Case Else
                        WsSamedi_Matin = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Samedi_ApresMidi() As String
            Get
                Return WsSamedi_ApresMidi
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsSamedi_ApresMidi = value
                    Case Else
                        WsSamedi_ApresMidi = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Dimanche_Matin() As String
            Get
                Return WsDimanche_Matin
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsDimanche_Matin = value
                    Case Else
                        WsDimanche_Matin = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Dimanche_ApresMidi() As String
            Get
                Return WsDimanche_ApresMidi
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsDimanche_ApresMidi = value
                    Case Else
                        WsDimanche_ApresMidi = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property CycledeTravail() As String
            Get
                Select Case TypeduCycle
                    Case VI.TypedeCycleTravail.Administratif
                        Return "Cycle administratif"
                    Case VI.TypedeCycleTravail.CycledeTravail
                        Return WsCycleTravail
                    Case VI.TypedeCycleTravail.Personnalise
                        Return "Cycle personnalisé"
                    Case VI.TypedeCycleTravail.EmploiduTemps
                        Return "Emploi du temps"
                End Select
                Return WsCycleTravail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsCycleTravail = value
                    Case Else
                        WsCycleTravail = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property HoraireJournalier() As String
            Get
                Return WsHoraireJournalier
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHoraireJournalier = value
                    Case Else
                        WsHoraireJournalier = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property SiJFTravailles() As String
            Get
                Return WsSiJFTravailles
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "Oui"
                        WsSiJFTravailles = "Oui"
                    Case Else
                        WsSiJFTravailles = "Non"
                End Select
            End Set
        End Property

        Public Property TypeHoraire() As String
            Get
                Return WsTypeHoraire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsTypeHoraire = value.ToUpper
                    Case Else
                        WsTypeHoraire = Strings.Left(value.ToUpper, 3)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_d_effet & VI.Tild)
                Chaine.Append(Lundi_Matin & VI.Tild)
                Chaine.Append(Lundi_ApresMidi & VI.Tild)
                Chaine.Append(Mardi_Matin & VI.Tild)
                Chaine.Append(Mardi_ApresMidi & VI.Tild)
                Chaine.Append(Mercredi_Matin & VI.Tild)
                Chaine.Append(Mercredi_ApresMidi & VI.Tild)
                Chaine.Append(Jeudi_Matin & VI.Tild)
                Chaine.Append(Jeudi_ApresMidi & VI.Tild)
                Chaine.Append(Vendredi_Matin & VI.Tild)
                Chaine.Append(Vendredi_ApresMidi & VI.Tild)
                Chaine.Append(Samedi_Matin & VI.Tild)
                Chaine.Append(Samedi_ApresMidi & VI.Tild)
                Chaine.Append(Dimanche_Matin & VI.Tild)
                Chaine.Append(Dimanche_ApresMidi & VI.Tild)
                Chaine.Append(CycledeTravail & VI.Tild)
                Chaine.Append(HoraireJournalier & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(SiJFTravailles & VI.Tild)
                Select Case SiHoraireVariable
                    Case True
                        Chaine.Append("OUI" & VI.Tild)
                    Case False
                        Select Case SiHoraireMixte
                            Case True
                                Chaine.Append("MIX")
                            Case False
                                Chaine.Append("NON")
                        End Select
                End Select

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 21 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_d_effet = TableauData(1)
                Lundi_Matin = TableauData(2)
                Lundi_ApresMidi = TableauData(3)
                Mardi_Matin = TableauData(4)
                Mardi_ApresMidi = TableauData(5)
                Mercredi_Matin = TableauData(6)
                Mercredi_ApresMidi = TableauData(7)
                Jeudi_Matin = TableauData(8)
                Jeudi_ApresMidi = TableauData(9)
                Vendredi_Matin = TableauData(10)
                Vendredi_ApresMidi = TableauData(11)
                Samedi_Matin = TableauData(12)
                Samedi_ApresMidi = TableauData(13)
                Dimanche_Matin = TableauData(14)
                Dimanche_ApresMidi = TableauData(15)
                CycledeTravail = TableauData(16)
                HoraireJournalier = TableauData(17)
                Observations = TableauData(18)
                SiJFTravailles = TableauData(19)
                Select Case TableauData(20)
                    Case Is = "MIX", "NON", "OUI"
                        WsTypeHoraire = TableauData(20)
                    Case Else
                        WsTypeHoraire = "OUI"
                End Select

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = ""
                TsTypeCycle = 0
                Select Case CycledeTravail
                    Case Is <> ""
                        TsTypeCycle = VI.TypedeCycleTravail.CycledeTravail
                    Case Else
                        For IndiceI = 1 To 14
                            Select Case TableauData(IndiceI + 1)
                                Case Is <> ""
                                    Select Case TableauData(IndiceI + 1)
                                        Case Is = "OUI", "NON"
                                            TsTypeCycle = VI.TypedeCycleTravail.Administratif
                                            Exit For
                                        Case Else
                                            Select Case InStr(TableauData(IndiceI), VI.PointVirgule)
                                                Case Is = 0
                                                    TsTypeCycle = VI.TypedeCycleTravail.Personnalise
                                                    Exit For
                                                Case Is > 0
                                                    TsTypeCycle = VI.TypedeCycleTravail.EmploiduTemps
                                                    Exit For
                                            End Select
                                    End Select
                            End Select
                        Next IndiceI
                End Select
            End Set
        End Property

        Public ReadOnly Property SiHoraireVariable() As Boolean
            Get
                Select Case TypeHoraire
                    Case Is = "OUI"
                        Return True
                End Select
                Return False
            End Get
        End Property

        Public ReadOnly Property SiHoraireFixe() As Boolean
            Get
                Select Case TypeHoraire
                    Case Is = "NON"
                        Return True
                End Select
                Return False
            End Get
        End Property

        Public ReadOnly Property SiHoraireMixte() As Boolean
            Get
                Select Case TypeHoraire
                    Case Is = "MIX"
                        Return True
                End Select
                Return False
            End Get
        End Property

        Public ReadOnly Property TypeduCycle() As Integer
            Get
                Return TsTypeCycle
            End Get
        End Property

        Public ReadOnly Property IndexDebutRotation() As Integer
            Get
                Return Weekday(DateValue(Date_d_effet), FirstDayOfWeek.Monday)
            End Get
        End Property

        Public ReadOnly Property ChaineCyclePersonnalisee() As String
            Get
                Dim Chaine As System.Text.StringBuilder
                Chaine = New System.Text.StringBuilder
                Chaine.Append(Lundi_Matin & VI.Tild)
                Chaine.Append(Lundi_ApresMidi & VI.Tild)
                Chaine.Append(Mardi_Matin & VI.Tild)
                Chaine.Append(Mardi_ApresMidi & VI.Tild)
                Chaine.Append(Mercredi_Matin & VI.Tild)
                Chaine.Append(Mercredi_ApresMidi & VI.Tild)
                Chaine.Append(Jeudi_Matin & VI.Tild)
                Chaine.Append(Jeudi_ApresMidi & VI.Tild)
                Chaine.Append(Vendredi_Matin & VI.Tild)
                Chaine.Append(Vendredi_ApresMidi & VI.Tild)
                Chaine.Append(Samedi_Matin & VI.Tild)
                Chaine.Append(Samedi_ApresMidi & VI.Tild)
                Chaine.Append(Dimanche_Matin & VI.Tild)
                Chaine.Append(Dimanche_ApresMidi)
                Return Chaine.ToString
            End Get
        End Property

        Public Property VDate_de_Fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_d_effet
                    Case 1
                        Return WsLundi_Matin
                    Case 2
                        Return WsLundi_ApresMidi
                    Case 3
                        Return WsMardi_Matin
                    Case 4
                        Return WsMardi_ApresMidi
                    Case 5
                        Return WsMercredi_Matin
                    Case 6
                        Return WsMercredi_ApresMidi
                    Case 7
                        Return WsJeudi_Matin
                    Case 8
                        Return WsJeudi_ApresMidi
                    Case 9
                        Return WsVendredi_Matin
                    Case 10
                        Return WsVendredi_ApresMidi
                    Case 11
                        Return WsSamedi_Matin
                    Case 12
                        Return WsSamedi_ApresMidi
                    Case 13
                        Return WsDimanche_Matin
                    Case 14
                        Return WsDimanche_ApresMidi
                    Case 15
                        Return WsCycleTravail
                    Case 16
                        Return WsHoraireJournalier
                    Case 17
                        Return WsObservations
                    Case 18
                        Return WsSiJFTravailles
                    Case 19
                        Return WsTypeHoraire
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
