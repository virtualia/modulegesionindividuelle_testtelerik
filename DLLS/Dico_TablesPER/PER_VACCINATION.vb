﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_VACCINATION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_Vaccin As String
        Private WsVaccin As String
        Private WsDate_Rappel As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_VACCINATION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaVaccination
            End Get
        End Property

        Public Property Date_du_Vaccin() As String
            Get
                Return WsDate_Vaccin
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is <> ""
                        WsDate_Vaccin = VirRhDate.DateStandardVirtualia(value)
                    Case Else
                        WsDate_Vaccin = ""
                End Select
                MyBase.Date_de_Valeur = WsDate_Vaccin
            End Set
        End Property

        Public Property Vaccin() As String
            Get
                Return WsVaccin
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsVaccin = value
                    Case Else
                        WsVaccin = Strings.Left(value, 40)
                End Select
                MyBase.Clef = WsVaccin
            End Set
        End Property

        Public Property Date_du_Rappel() As String
            Get
                Return WsDate_Rappel
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is <> ""
                        WsDate_Rappel = VirRhDate.DateStandardVirtualia(value)
                    Case Else
                        WsDate_Rappel = ""
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_du_Vaccin & VI.Tild)
                Chaine.Append(Vaccin & VI.Tild)
                Chaine.Append(Date_du_Rappel)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_du_Vaccin = TableauData(1)
                Vaccin = TableauData(2)
                Date_du_Rappel = TableauData(3)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_Vaccin
                    Case 1
                        Return WsVaccin
                    Case 2
                        Return WsDate_Rappel
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


