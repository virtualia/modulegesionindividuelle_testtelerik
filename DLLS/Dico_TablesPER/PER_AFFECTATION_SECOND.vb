﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_AFFECTATION_SECOND
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsNumObjet As Integer
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsStructure_de_rattachement As String
        Private WsStructure_d_affectation As String
        Private WsStructure_de_3e_niveau As String
        Private WsStructure_de_4e_niveau As String
        Private WsFonction_exercee As String
        Private WsBonification_indiciaire As String
        Private WsFamille_du_metier As String
        Private WsSous_famille_du_metier As String
        Private WsCode_administratif As String
        Private WsObservations As String
        Private WsAnnuaire As String
        Private WsNumero_du_poste As String
        Private WsStructure_de_5e_niveau As String
        Private WsStructure_de_6e_niveau As String
        Private WsSecteureconomique As String
        Private WsPoste_fonctionnel As String
        Private WsFonctionsecondaire As String
        '
        Private TsDate_de_fin As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_AFFECTATION_SECOND"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return WsNumObjet
            End Get
        End Property

        Public Property Structure_de_rattachement() As String
            Get
                Return WsStructure_de_rattachement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsStructure_de_rattachement = value
                    Case Else
                        WsStructure_de_rattachement = Strings.Left(value, 100)
                End Select
                MyBase.Clef = WsStructure_de_rattachement
            End Set
        End Property

        Public Property Structure_de_1er_niveau() As String
            Get
                Return WsStructure_de_rattachement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsStructure_de_rattachement = value
                    Case Else
                        WsStructure_de_rattachement = Strings.Left(value, 100)
                End Select
                MyBase.Clef = WsStructure_de_rattachement
            End Set
        End Property

        Public Property Structure_d_affectation() As String
            Get
                Return WsStructure_d_affectation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsStructure_d_affectation = value
                    Case Else
                        WsStructure_d_affectation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Structure_de_2e_niveau() As String
            Get
                Return WsStructure_d_affectation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsStructure_d_affectation = value
                    Case Else
                        WsStructure_d_affectation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Structure_de_3e_niveau() As String
            Get
                Return WsStructure_de_3e_niveau
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsStructure_de_3e_niveau = value
                    Case Else
                        WsStructure_de_3e_niveau = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Structure_de_4e_niveau() As String
            Get
                Return WsStructure_de_4e_niveau
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsStructure_de_4e_niveau = value
                    Case Else
                        WsStructure_de_4e_niveau = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Fonction_exercee() As String
            Get
                Return WsFonction_exercee
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsFonction_exercee = value
                    Case Else
                        WsFonction_exercee = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Bonification_indiciaire() As String
            Get
                Return WsBonification_indiciaire
            End Get
            Set(ByVal value As String)
                WsBonification_indiciaire = value
            End Set
        End Property

        Public Property Famille_du_metier() As String
            Get
                Return WsFamille_du_metier
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsFamille_du_metier = value
                    Case Else
                        WsFamille_du_metier = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Sous_famille_du_metier() As String
            Get
                Return WsSous_famille_du_metier
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsSous_famille_du_metier = value
                    Case Else
                        WsSous_famille_du_metier = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Code_administratif() As String
            Get
                Return WsCode_administratif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsCode_administratif = value
                    Case Else
                        WsCode_administratif = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property Annuaire() As String
            Get
                Return WsAnnuaire
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsAnnuaire = ""
                    Case Is = "0", "Non"
                        WsAnnuaire = "Non"
                    Case Else
                        WsAnnuaire = "Oui"
                End Select
            End Set
        End Property

        Public Property Numero_du_poste() As String
            Get
                Return WsNumero_du_poste
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNumero_du_poste = value
                    Case Else
                        WsNumero_du_poste = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Structure_de_5e_niveau() As String
            Get
                Return WsStructure_de_5e_niveau
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsStructure_de_5e_niveau = value
                    Case Else
                        WsStructure_de_5e_niveau = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Structure_de_6e_niveau() As String
            Get
                Return WsStructure_de_6e_niveau
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsStructure_de_6e_niveau = value
                    Case Else
                        WsStructure_de_6e_niveau = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Secteureconomique() As String
            Get
                Return WsSecteureconomique
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsSecteureconomique = value
                    Case Else
                        WsSecteureconomique = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Poste_fonctionnel() As String
            Get
                Return WsPoste_fonctionnel
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsPoste_fonctionnel = value
                    Case Else
                        WsPoste_fonctionnel = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Fonctionsecondaire() As String
            Get
                Return WsFonctionsecondaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsFonctionsecondaire = value
                    Case Else
                        WsFonctionsecondaire = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Structure_de_rattachement & VI.Tild)
                Chaine.Append(Structure_d_affectation & VI.Tild)
                Chaine.Append(Structure_de_3e_niveau & VI.Tild)
                Chaine.Append(Structure_de_4e_niveau & VI.Tild)
                Chaine.Append(Fonction_exercee & VI.Tild)
                Chaine.Append(Bonification_indiciaire & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Famille_du_metier & VI.Tild)
                Chaine.Append(Sous_famille_du_metier & VI.Tild)
                Chaine.Append(Code_administratif & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(Annuaire & VI.Tild)
                Chaine.Append(Numero_du_poste & VI.Tild)
                Chaine.Append(Structure_de_5e_niveau & VI.Tild)
                Chaine.Append(Structure_de_6e_niveau & VI.Tild)
                Chaine.Append(Secteureconomique & VI.Tild)
                Chaine.Append(Poste_fonctionnel & VI.Tild)
                Chaine.Append(Fonctionsecondaire)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 20 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Structure_de_rattachement = TableauData(2)
                Structure_d_affectation = TableauData(3)
                Structure_de_3e_niveau = TableauData(4)
                Structure_de_4e_niveau = TableauData(5)
                Fonction_exercee = TableauData(6)
                Bonification_indiciaire = TableauData(7)
                MyBase.Date_de_Fin = TableauData(8)
                Famille_du_metier = TableauData(9)
                Sous_famille_du_metier = TableauData(10)
                Code_administratif = TableauData(11)
                Observations = TableauData(12)
                Annuaire = TableauData(13)
                Numero_du_poste = TableauData(14)
                Structure_de_5e_niveau = TableauData(15)
                Structure_de_6e_niveau = TableauData(16)
                Secteureconomique = TableauData(17)
                Poste_fonctionnel = TableauData(18)
                Fonctionsecondaire = TableauData(19)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = Date_de_fin
            End Set
        End Property

        Public Property vDate_de_fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<DateAffectation>" & MyBase.Date_de_Valeur & "</DateAffectation>" & vbCrLf)
                Chaine.Append("<DateFinAffectation>" & MyBase.Date_de_Fin & "</DateFinAffectation>" & vbCrLf)
                Chaine.Append("<AffectationNiveau1>" & VirRhFonction.ChaineXMLValide(Structure_de_rattachement) & "</AffectationNiveau1>" & vbCrLf)
                Chaine.Append("<AffectationNiveau2>" & VirRhFonction.ChaineXMLValide(Structure_d_affectation) & "</AffectationNiveau2>" & vbCrLf)
                Chaine.Append("<AffectationNiveau3>" & VirRhFonction.ChaineXMLValide(Structure_de_3e_niveau) & "</AffectationNiveau3>" & vbCrLf)
                Chaine.Append("<AffectationNiveau4>" & VirRhFonction.ChaineXMLValide(Structure_de_4e_niveau) & "</AffectationNiveau4>" & vbCrLf)
                Chaine.Append("<AffectationNiveau5>" & VirRhFonction.ChaineXMLValide(Structure_de_5e_niveau) & "</AffectationNiveau5>" & vbCrLf)
                Chaine.Append("<AffectationNiveau6>" & VirRhFonction.ChaineXMLValide(Structure_de_6e_niveau) & "</AffectationNiveau6>" & vbCrLf)
                Chaine.Append("<FonctionExercee>" & VirRhFonction.ChaineXMLValide(Fonction_exercee) & "</FonctionExercee>" & vbCrLf)
                Chaine.Append("<BonificationIndiciaire>" & Bonification_indiciaire & "</BonificationIndiciaire>" & vbCrLf)
                Chaine.Append("<FamilleMetier>" & VirRhFonction.ChaineXMLValide(Famille_du_metier) & "</FamilleMetier>" & vbCrLf)
                Chaine.Append("<SousFamilleMetier>" & VirRhFonction.ChaineXMLValide(Sous_famille_du_metier) & "</SousFamilleMetier>" & vbCrLf)
                Chaine.Append("<CodeAdministratifMetier>" & Code_administratif & "</CodeAdministratifMetier>" & vbCrLf)
                Chaine.Append("<FonctionSecondaire>" & VirRhFonction.ChaineXMLValide(Fonctionsecondaire) & "</FonctionSecondaire>" & vbCrLf)
                Chaine.Append("<SecteurEconomique>" & VirRhFonction.ChaineXMLValide(Secteureconomique) & "</SecteurEconomique>" & vbCrLf)
                Chaine.Append("<SiExclureAnnuaire>" & Annuaire & "</SiExclureAnnuaire>" & vbCrLf)
                Chaine.Append("<NumeroPosteFonctionnel>" & Numero_du_poste & "</NumeroPosteFonctionnel>" & vbCrLf)
                Chaine.Append("<PosteFonctionnel>" & VirRhFonction.ChaineXMLValide(Poste_fonctionnel) & "</PosteFonctionnel>" & vbCrLf)

                Return Chaine.ToString

            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return Date_de_Valeur
                    Case 1
                        Return WsStructure_de_rattachement
                    Case 2
                        Return WsStructure_d_affectation
                    Case 3
                        Return WsStructure_de_3e_niveau
                    Case 4
                        Return WsStructure_de_4e_niveau
                    Case 5
                        Return WsFonction_exercee
                    Case 6
                        Return WsBonification_indiciaire
                    Case 7
                        Return Date_de_Fin
                    Case 8
                        Return WsFamille_du_metier
                    Case 9
                        Return WsSous_famille_du_metier
                    Case 10
                        Return WsCode_administratif
                    Case 11
                        Return WsObservations
                    Case 12
                        Return WsAnnuaire
                    Case 13
                        Return WsNumero_du_poste
                    Case 14
                        Return WsStructure_de_5e_niveau
                    Case 15
                        Return WsStructure_de_6e_niveau
                    Case 16
                        Return WsSecteureconomique
                    Case 17
                        Return WsPoste_fonctionnel
                    Case 18
                        Return WsFonctionsecondaire
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Sub New(ByVal NumObjet As Integer)
            MyBase.New()
            WsNumObjet = NumObjet
        End Sub

        Public Sub New()
            MyBase.New()
            WsNumObjet = VI.ObjetPer.ObaAffectation2nd
        End Sub

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property
    End Class

    Public Class PER_AFFECTATION_TROIS
        Inherits PER_AFFECTATION_SECOND

        Public Sub New()
            MyBase.New(141)
        End Sub

        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "PER_AFFECTATION_TROIS"
            End Get
        End Property
    End Class

    Public Class PER_AFFECTATION_QUATRE
        Inherits PER_AFFECTATION_SECOND

        Public Sub New()
            MyBase.New(142)
        End Sub

        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "PER_AFFECTATION_QUATRE"
            End Get
        End Property
    End Class

    Public Class PER_AFFECTATION_CINQ
        Inherits PER_AFFECTATION_SECOND

        Public Sub New()
            MyBase.New(143)
        End Sub

        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "PER_AFFECTATION_CINQ"
            End Get
        End Property
    End Class

    Public Class PER_AFFECTATION_SIX
        Inherits PER_AFFECTATION_SECOND

        Public Sub New()
            MyBase.New(144)
        End Sub

        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "PER_AFFECTATION_SIX"
            End Get
        End Property
    End Class
End Namespace


