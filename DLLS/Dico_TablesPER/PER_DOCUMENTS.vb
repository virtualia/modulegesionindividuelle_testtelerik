﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_DOCUMENTS
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsChrono As String
        Private WsDatation As String
        Private WsReference As String
        Private WsClassement As String
        Private WsObservations As String
        Private WsSiVisibleParIntranet As Boolean

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_DOCUMENTS"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaDocuments
            End Get
        End Property

        Public Property Chrono() As String
            Get
                Return WsChrono
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsChrono = value
                    Case Else
                        WsChrono = Strings.Left(value, 30)
                End Select
                MyBase.Clef = WsChrono
            End Set
        End Property

        Public Property Datation() As String
            Get
                Return WsDatation
            End Get
            Set(ByVal value As String)
                WsDatation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Reference() As String
            Get
                Return WsReference
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsReference = value
                    Case Else
                        WsReference = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Classement() As String
            Get
                Return WsClassement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsClassement = value
                    Case Else
                        WsClassement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property SiVisibleParIntranet() As Boolean
            Get
                Return WsSiVisibleParIntranet
            End Get
            Set(ByVal value As Boolean)
                WsSiVisibleParIntranet = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Chrono & VI.Tild)
                Chaine.Append(Datation & VI.Tild)
                Chaine.Append(Reference & VI.Tild)
                Chaine.Append(Classement & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Select Case SiVisibleParIntranet
                    Case True
                        Chaine.Append("Oui")
                    Case False
                        Chaine.Append("Non")
                End Select

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Chrono = TableauData(1)
                Datation = TableauData(2)
                Reference = TableauData(3)
                Classement = TableauData(4)
                Observations = TableauData(5)
                Select Case TableauData(6)
                    Case Is = "Oui"
                        SiVisibleParIntranet = True
                    Case Else
                        SiVisibleParIntranet = False
                End Select

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Property NomFichier() As String
            Get
                Return WsReference
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsReference = value
                    Case Else
                        WsReference = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


