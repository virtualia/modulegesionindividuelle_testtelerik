﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_PAIE_BASE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsNumObjet As Integer = VI.ObjetPer.ObaPaieBASE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNoOrdre As Integer
        Private WsTypeFichier As String 'BA, DA, HI, HS, SR
        Private WsNumDos As Integer
        Private WsEnregistrement As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_PAIE_BASE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return WsNumObjet 'Spécifique Secteur Public - Paie DGFIP
            End Get
        End Property

        Public Property Numero_Ordre As Integer
            Get
                Return WsNoOrdre
            End Get
            Set(ByVal value As Integer)
                WsNoOrdre = value
                MyBase.Clef = value.ToString
            End Set
        End Property

        Public Property Type_Fichier() As String
            Get
                Return WsTypeFichier
            End Get
            Set(ByVal value As String)
                WsTypeFichier = F_FormatAlpha(value, 3)
            End Set
        End Property

        Public Property Numero_Dossier As Integer
            Get
                Return WsNumDos
            End Get
            Set(ByVal value As Integer)
                WsNumDos = value
            End Set
        End Property

        Public Property Enregistrement() As String
            Get
                Return WsEnregistrement
            End Get
            Set(ByVal value As String)
                WsEnregistrement = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Numero_Ordre.ToString & VI.Tild)
                Chaine.Append(Type_Fichier & VI.Tild)
                Chaine.Append(Numero_Dossier.ToString & VI.Tild)
                Chaine.Append(Enregistrement & VI.Tild)
                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                MyBase.Ide_Dossier = CInt(TableauData(0))
                If IsNumeric(TableauData(1)) Then
                    Numero_Ordre = CInt(TableauData(1))
                Else
                    Numero_Ordre = 0
                End If
                Type_Fichier = TableauData(2)
                If IsNumeric(TableauData(3)) Then
                    Numero_Dossier = CInt(TableauData(3))
                Else
                    Numero_Dossier = 0
                End If
                Enregistrement = TableauData(4)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 1
                        Return Numero_Ordre.ToString
                    Case 2
                        Return Type_Fichier
                    Case 3
                        Return Numero_Dossier.ToString
                    Case 4
                        Return Enregistrement
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
        Public Sub New(ByVal NoObjet As Integer)
            MyBase.New()
            WsNumObjet = NoObjet
        End Sub
    End Class

    Public Class PER_PAIE_BASE2
        Inherits PER_PAIE_BASE

        Public Sub New()
            MyBase.New(166)
        End Sub

        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "PER_PAIE_BASE2"
            End Get
        End Property
    End Class

    Public Class PER_PAIE_BASE3
        Inherits PER_PAIE_BASE

        Public Sub New()
            MyBase.New(168)
        End Sub

        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "PER_PAIE_BASE3"
            End Get
        End Property
    End Class
End Namespace

