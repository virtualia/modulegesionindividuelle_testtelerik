﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_ENTRETIEN_HONDA_PERFORMANCE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNumero As Integer
        Private WsIntitule_Objectif As String
        Private WsControlItemTarget As String
        Private WsRealisation_Objectif As String
        Private WsPoids_Manager As Integer
        Private WsResultatAtteint_Manager As Integer
        Private WsSiDifficile_Manager As String
        Private WsNoteFinale_Manager As Integer
        Private WsPoids_Self As Integer
        Private WsResultatAtteint_Self As Integer
        Private WsSiDifficile_Self As String
        Private WsNoteFinale_Self As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENTRETIEN_PERFORMANCE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 151
            End Get
        End Property

        Public Property Numero() As Integer
            Get
                Return WsNumero
            End Get
            Set(ByVal value As Integer)
                WsNumero = value
            End Set
        End Property

        Public Property Intitule_Objectif() As String
            Get
                Return WsIntitule_Objectif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsIntitule_Objectif = value
                    Case Else
                        WsIntitule_Objectif = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property ControlItemTarget() As String
            Get
                Return WsControlItemTarget
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsControlItemTarget = value
                    Case Else
                        WsControlItemTarget = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property Realisation_Objectif() As String
            Get
                Return WsRealisation_Objectif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsRealisation_Objectif = value
                    Case Else
                        WsRealisation_Objectif = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property Poids_Manager As Integer
            Get
                Return WsPoids_Manager
            End Get
            Set(value As Integer)
                WsPoids_Manager = value
            End Set
        End Property

        Public Property Resultat_Atteint_Manager As Integer
            Get
                Return WsResultatAtteint_Manager
            End Get
            Set(value As Integer)
                WsResultatAtteint_Manager = value
            End Set
        End Property

        Public Property SiDifficile_Manager() As String
            Get
                Return WsSiDifficile_Manager
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "", "N"
                        WsSiDifficile_Manager = "N"
                    Case Else
                        WsSiDifficile_Manager = "Y"
                End Select
            End Set
        End Property

        Public Property Note_Finale_Manager As Integer
            Get
                Return WsNoteFinale_Manager
            End Get
            Set(value As Integer)
                WsNoteFinale_Manager = value
            End Set
        End Property

        Public Property Poids_Self As Integer
            Get
                Return WsPoids_Self
            End Get
            Set(value As Integer)
                WsPoids_Self = value
            End Set
        End Property

        Public Property Resultat_Atteint_Self As Integer
            Get
                Return WsResultatAtteint_Self
            End Get
            Set(value As Integer)
                WsResultatAtteint_Self = value
            End Set
        End Property

        Public Property SiDifficile_Self() As String
            Get
                Return WsSiDifficile_Self
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "", "N"
                        WsSiDifficile_Self = "N"
                    Case Else
                        WsSiDifficile_Self = "Y"
                End Select
            End Set
        End Property

        Public Property Note_Finale_Self As Integer
            Get
                Return WsNoteFinale_Self
            End Get
            Set(value As Integer)
                WsNoteFinale_Self = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Numero.ToString & VI.Tild)
                Chaine.Append(Intitule_Objectif & VI.Tild)
                Chaine.Append(ControlItemTarget & VI.Tild)
                Chaine.Append(Realisation_Objectif & VI.Tild)
                Chaine.Append(Poids_Manager.ToString & VI.Tild)
                Chaine.Append(Resultat_Atteint_Manager.ToString & VI.Tild)
                Chaine.Append(SiDifficile_Manager & VI.Tild)
                Chaine.Append(Note_Finale_Manager.ToString & VI.Tild)
                Chaine.Append(Poids_Self.ToString & VI.Tild)
                Chaine.Append(Resultat_Atteint_Self.ToString & VI.Tild)
                Chaine.Append(SiDifficile_Self & VI.Tild)
                Chaine.Append(Note_Finale_Self.ToString)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 14 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then Numero = CInt(VirRhFonction.ConversionDouble(TableauData(2)))
                Intitule_Objectif = TableauData(3)
                ControlItemTarget = TableauData(4)
                Realisation_Objectif = TableauData(5)
                Poids_Manager = CInt(VirRhFonction.ConversionDouble(TableauData(6)))
                Resultat_Atteint_Manager = CInt(VirRhFonction.ConversionDouble(TableauData(7)))
                SiDifficile_Manager = TableauData(8)
                Note_Finale_Manager = CInt(VirRhFonction.ConversionDouble(TableauData(9)))
                Poids_Self = CInt(VirRhFonction.ConversionDouble(TableauData(10)))
                Resultat_Atteint_Self = CInt(VirRhFonction.ConversionDouble(TableauData(11)))
                SiDifficile_Self = TableauData(12)
                Note_Finale_Self = CInt(VirRhFonction.ConversionDouble(TableauData(13)))

                If WsFicheLue Is Nothing Then
                    WsFicheLue = New System.Text.StringBuilder
                    For IndiceI = 1 To TableauData.Count - 1
                        WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                    Next IndiceI
                End If
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
