﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_DEMANDE_ACOMPTE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsMoisPaie As String
        Private WsMontant As Double = 0
        Private WsCommentaireDemandeur As String
        Private WsSiValideDRH As String
        Private WsDateValidation As String
        Private WsCommentaireDRH As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_DEMANDE_ACOMPTE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaDemandeAcompte
            End Get
        End Property

        Public Property Mois_Paie() As String
            Get
                Return WsMoisPaie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 7
                        WsMoisPaie = value
                    Case Else
                        WsMoisPaie = Strings.Left(value, 7)
                End Select
            End Set
        End Property

        Public Property Montant() As Double
            Get
                Return WsMontant
            End Get
            Set(ByVal value As Double)
                WsMontant = value
            End Set
        End Property

        Public Property Commentaire_Demandeur() As String
            Get
                Return WsCommentaireDemandeur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsCommentaireDemandeur = value
                    Case Else
                        WsCommentaireDemandeur = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property SiValide_DRH() As String
            Get
                Return WsSiValideDRH
            End Get
            Set(ByVal value As String)
                WsSiValideDRH = value
                Select Case value
                    Case Is <> "Oui"
                        WsSiValideDRH = "Non"
                End Select
            End Set
        End Property

        Public Property Date_Validation() As String
            Get
                Return WsDateValidation
            End Get
            Set(ByVal value As String)
                WsDateValidation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Commentaire_DRH() As String
            Get
                Return WsCommentaireDRH
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsCommentaireDRH = value
                    Case Else
                        WsCommentaireDRH = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Mois_Paie & VI.Tild)
                Select Case Montant
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant.ToString) & VI.Tild)
                End Select
                Chaine.Append(Commentaire_Demandeur & VI.Tild)
                Chaine.Append(SiValide_DRH & VI.Tild)
                Chaine.Append(Date_Validation & VI.Tild)
                Chaine.Append(Commentaire_DRH)
                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Mois_Paie = TableauData(2)
                Montant = VirRhFonction.ConversionDouble(TableauData(3))
                Commentaire_Demandeur = TableauData(4)
                SiValide_DRH = TableauData(5)
                Date_Validation = TableauData(6)
                Commentaire_DRH = TableauData(7)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

