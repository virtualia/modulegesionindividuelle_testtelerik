﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_RETRAITE_CONGE_HT
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_Debut As String
        Private WsDureeRetenue As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_RETRAITE_CONGE_HT"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 98
            End Get
        End Property

        Public Property Date_Debut() As String
            Get
                Return WsDate_Debut
            End Get
            Set(ByVal value As String)
                WsDate_Debut = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_Debut
            End Set
        End Property

        Public Property DureeRetenue() As String
            Get
                Return WsDureeRetenue
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsDureeRetenue = value
                    Case Else
                        WsDureeRetenue = Strings.Left(value, 8)
                End Select
                MyBase.Clef = WsDureeRetenue
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_Debut & VI.Tild)
                Chaine.Append(DureeRetenue & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)

                Chaine.Append(MyBase.Date_de_Fin)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_Debut = TableauData(1)
                DureeRetenue = TableauData(2)
                MyBase.Date_de_Fin = TableauData(3)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_Debut
                    Case 1
                        Return WsDureeRetenue
                    Case 2
                        Return Date_de_Fin
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

