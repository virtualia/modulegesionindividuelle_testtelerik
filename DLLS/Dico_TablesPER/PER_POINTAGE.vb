﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_POINTAGE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsBadgeage_N1 As String
        Private WsLieu_N1 As String
        Private WsSens_N1 As String
        Private WsBadgeage_N2 As String
        Private WsLieu_N2 As String
        Private WsSens_N2 As String
        Private WsBadgeage_N3 As String
        Private WsLieu_N3 As String
        Private WsSens_N3 As String
        Private WsBadgeage_N4 As String
        Private WsLieu_N4 As String
        Private WsSens_N4 As String
        Private WsBadgeage_N5 As String
        Private WsLieu_N5 As String
        Private WsSens_N5 As String
        Private WsBadgeage_N6 As String
        Private WsLieu_N6 As String
        Private WsSens_N6 As String
        Private WsRapportAnomalie As String
        Private WsDateduLissage As String
        Private WsSignataire As String
        Private WsLissage_N1 As String
        Private WsLissage_N2 As String
        Private WsLissage_N3 As String
        Private WsLissage_N4 As String
        Private WsLissage_N5 As String
        Private WsLissage_N6 As String
        Private WsBadgeage_N7 As String
        Private WsLieu_N7 As String
        Private WsSens_N7 As String
        Private WsBadgeage_N8 As String
        Private WsLieu_N8 As String
        Private WsSens_N8 As String
        Private WsBadgeage_N9 As String
        Private WsLieu_N9 As String
        Private WsSens_N9 As String
        Private WsBadgeage_N10 As String
        Private WsLieu_N10 As String
        Private WsSens_N10 As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_POINTAGE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaPointage
            End Get
        End Property

        Public Property Badgeage_N1() As String
            Get
                Return WsBadgeage_N1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsBadgeage_N1 = value
                    Case Else
                        WsBadgeage_N1 = Strings.Left(value, 8)
                End Select
                MyBase.Clef = WsBadgeage_N1
            End Set
        End Property

        Public Property Lieu_N1() As String
            Get
                Return WsLieu_N1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsLieu_N1 = value
                    Case Else
                        WsLieu_N1 = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Sens_N1() As String
            Get
                Return WsSens_N1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsSens_N1 = value
                    Case Else
                        WsSens_N1 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Badgeage_N2() As String
            Get
                Return WsBadgeage_N2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsBadgeage_N2 = value
                    Case Else
                        WsBadgeage_N2 = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Lieu_N2() As String
            Get
                Return WsLieu_N2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsLieu_N2 = value
                    Case Else
                        WsLieu_N2 = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Sens_N2() As String
            Get
                Return WsSens_N2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsSens_N2 = value
                    Case Else
                        WsSens_N2 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Badgeage_N3() As String
            Get
                Return WsBadgeage_N3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsBadgeage_N3 = value
                    Case Else
                        WsBadgeage_N3 = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Lieu_N3() As String
            Get
                Return WsLieu_N3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsLieu_N3 = value
                    Case Else
                        WsLieu_N3 = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Sens_N3() As String
            Get
                Return WsSens_N3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsSens_N3 = value
                    Case Else
                        WsSens_N3 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Badgeage_N4() As String
            Get
                Return WsBadgeage_N4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsBadgeage_N4 = value
                    Case Else
                        WsBadgeage_N4 = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Lieu_N4() As String
            Get
                Return WsLieu_N4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsLieu_N4 = value
                    Case Else
                        WsLieu_N4 = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Sens_N4() As String
            Get
                Return WsSens_N4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsSens_N4 = value
                    Case Else
                        WsSens_N4 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Badgeage_N5() As String
            Get
                Return WsBadgeage_N5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsBadgeage_N5 = value
                    Case Else
                        WsBadgeage_N5 = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Lieu_N5() As String
            Get
                Return WsLieu_N5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsLieu_N5 = value
                    Case Else
                        WsLieu_N5 = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Sens_N5() As String
            Get
                Return WsSens_N5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsSens_N5 = value
                    Case Else
                        WsSens_N5 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Badgeage_N6() As String
            Get
                Return WsBadgeage_N6
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsBadgeage_N6 = value
                    Case Else
                        WsBadgeage_N6 = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Lieu_N6() As String
            Get
                Return WsLieu_N6
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsLieu_N6 = value
                    Case Else
                        WsLieu_N6 = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Sens_N6() As String
            Get
                Return WsSens_N6
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsSens_N6 = value
                    Case Else
                        WsSens_N6 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property RapportdAnomalie() As String
            Get
                Return WsRapportAnomalie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 150
                        WsRapportAnomalie = value
                    Case Else
                        WsRapportAnomalie = Strings.Left(value, 150)
                End Select
            End Set
        End Property

        Public Property Date_du_Lissage() As String
            Get
                Return WsDateduLissage
            End Get
            Set(ByVal value As String)
                WsDateduLissage = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Signataire() As String
            Get
                Return WsSignataire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsSignataire = value
                    Case Else
                        WsSignataire = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Lissage_N1() As String
            Get
                Return WsLissage_N1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsLissage_N1 = value
                    Case Else
                        WsLissage_N1 = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Lissage_N2() As String
            Get
                Return WsLissage_N2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsLissage_N2 = value
                    Case Else
                        WsLissage_N2 = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Lissage_N3() As String
            Get
                Return WsLissage_N3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsLissage_N3 = value
                    Case Else
                        WsLissage_N3 = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Lissage_N4() As String
            Get
                Return WsLissage_N4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsLissage_N4 = value
                    Case Else
                        WsLissage_N4 = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Lissage_N5() As String
            Get
                Return WsLissage_N5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsLissage_N5 = value
                    Case Else
                        WsLissage_N5 = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Lissage_N6() As String
            Get
                Return WsLissage_N6
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsLissage_N6 = value
                    Case Else
                        WsLissage_N6 = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Badgeage_N7() As String
            Get
                Return WsBadgeage_N7
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsBadgeage_N7 = value
                    Case Else
                        WsBadgeage_N7 = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Lieu_N7() As String
            Get
                Return WsLieu_N7
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsLieu_N7 = value
                    Case Else
                        WsLieu_N7 = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Sens_N7() As String
            Get
                Return WsSens_N7
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsSens_N7 = value
                    Case Else
                        WsSens_N7 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Badgeage_N8() As String
            Get
                Return WsBadgeage_N8
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsBadgeage_N8 = value
                    Case Else
                        WsBadgeage_N8 = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Lieu_N8() As String
            Get
                Return WsLieu_N8
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsLieu_N8 = value
                    Case Else
                        WsLieu_N8 = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Sens_N8() As String
            Get
                Return WsSens_N8
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsSens_N8 = value
                    Case Else
                        WsSens_N8 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Badgeage_N9() As String
            Get
                Return WsBadgeage_N9
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsBadgeage_N9 = value
                    Case Else
                        WsBadgeage_N9 = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Lieu_N9() As String
            Get
                Return WsLieu_N9
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsLieu_N9 = value
                    Case Else
                        WsLieu_N9 = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Sens_N9() As String
            Get
                Return WsSens_N9
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsSens_N9 = value
                    Case Else
                        WsSens_N9 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Badgeage_N10() As String
            Get
                Return WsBadgeage_N10
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsBadgeage_N10 = value
                    Case Else
                        WsBadgeage_N10 = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Lieu_N10() As String
            Get
                Return WsLieu_N10
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsLieu_N10 = value
                    Case Else
                        WsLieu_N10 = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Sens_N10() As String
            Get
                Return WsSens_N10
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsSens_N10 = value
                    Case Else
                        WsSens_N10 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Badgeage_N1 & VI.Tild)
                Chaine.Append(Lieu_N1 & VI.Tild)
                Chaine.Append(Sens_N1 & VI.Tild)
                Chaine.Append(Badgeage_N2 & VI.Tild)
                Chaine.Append(Lieu_N2 & VI.Tild)
                Chaine.Append(Sens_N2 & VI.Tild)
                Chaine.Append(Badgeage_N3 & VI.Tild)
                Chaine.Append(Lieu_N3 & VI.Tild)
                Chaine.Append(Sens_N3 & VI.Tild)
                Chaine.Append(Badgeage_N4 & VI.Tild)
                Chaine.Append(Lieu_N4 & VI.Tild)
                Chaine.Append(Sens_N4 & VI.Tild)
                Chaine.Append(Badgeage_N5 & VI.Tild)
                Chaine.Append(Lieu_N5 & VI.Tild)
                Chaine.Append(Sens_N5 & VI.Tild)
                Chaine.Append(Badgeage_N6 & VI.Tild)
                Chaine.Append(Lieu_N6 & VI.Tild)
                Chaine.Append(Sens_N6 & VI.Tild)
                Chaine.Append(RapportdAnomalie & VI.Tild)
                Chaine.Append(Date_du_Lissage & VI.Tild)
                Chaine.Append(Signataire & VI.Tild)
                Chaine.Append(Lissage_N1 & VI.Tild)
                Chaine.Append(Lissage_N2 & VI.Tild)
                Chaine.Append(Lissage_N3 & VI.Tild)
                Chaine.Append(Lissage_N4 & VI.Tild)
                Chaine.Append(Lissage_N5 & VI.Tild)
                Chaine.Append(Lissage_N6 & VI.Tild)
                Chaine.Append(Badgeage_N7 & VI.Tild)
                Chaine.Append(Lieu_N7 & VI.Tild)
                Chaine.Append(Sens_N7 & VI.Tild)
                Chaine.Append(Badgeage_N8 & VI.Tild)
                Chaine.Append(Lieu_N8 & VI.Tild)
                Chaine.Append(Sens_N8 & VI.Tild)
                Chaine.Append(Badgeage_N9 & VI.Tild)
                Chaine.Append(Lieu_N9 & VI.Tild)
                Chaine.Append(Sens_N9 & VI.Tild)
                Chaine.Append(Badgeage_N10 & VI.Tild)
                Chaine.Append(Lieu_N10 & VI.Tild)
                Chaine.Append(Sens_N10)
                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 41 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Badgeage_N1 = TableauData(2)
                Lieu_N1 = TableauData(3)
                Sens_N1 = TableauData(4)
                Badgeage_N2 = TableauData(5)
                Lieu_N2 = TableauData(6)
                Sens_N2 = TableauData(7)
                Badgeage_N3 = TableauData(8)
                Lieu_N3 = TableauData(9)
                Sens_N3 = TableauData(10)
                Badgeage_N4 = TableauData(11)
                Lieu_N4 = TableauData(12)
                Sens_N4 = TableauData(13)
                Badgeage_N5 = TableauData(14)
                Lieu_N5 = TableauData(15)
                Sens_N5 = TableauData(16)
                Badgeage_N6 = TableauData(17)
                Lieu_N6 = TableauData(18)
                Sens_N6 = TableauData(19)
                RapportdAnomalie = TableauData(20)
                Date_du_Lissage = TableauData(21)
                Signataire = TableauData(22)
                Lissage_N1 = TableauData(23)
                Lissage_N2 = TableauData(24)
                Lissage_N3 = TableauData(25)
                Lissage_N4 = TableauData(26)
                Lissage_N5 = TableauData(27)
                Lissage_N6 = TableauData(28)
                Badgeage_N7 = TableauData(29)
                Lieu_N7 = TableauData(30)
                Sens_N7 = TableauData(31)
                Badgeage_N8 = TableauData(32)
                Lieu_N8 = TableauData(33)
                Sens_N8 = TableauData(34)
                Badgeage_N9 = TableauData(35)
                Lieu_N9 = TableauData(36)
                Sens_N9 = TableauData(37)
                Badgeage_N10 = TableauData(38)
                Lieu_N10 = TableauData(39)
                Sens_N10 = TableauData(40)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_HeureBadgee(ByVal Index As Integer) As System.DateTime
            Get
                Dim HeureLue As String = ""
                Select Case Index
                    Case 1
                        HeureLue = WsBadgeage_N1
                    Case 2
                        HeureLue = WsBadgeage_N2
                    Case 3
                        HeureLue = WsBadgeage_N3
                    Case 4
                        HeureLue = WsBadgeage_N4
                    Case 5
                        HeureLue = WsBadgeage_N5
                    Case 6
                        HeureLue = WsBadgeage_N6
                    Case 7
                        HeureLue = WsBadgeage_N7
                    Case 8
                        HeureLue = WsBadgeage_N8
                    Case 9
                        HeureLue = WsBadgeage_N9
                    Case 10
                        HeureLue = WsBadgeage_N10
                End Select
                Return VirRhDate.HeureTypee(MyBase.Date_de_Valeur, HeureLue)
            End Get
        End Property

        Public ReadOnly Property V_HeureLissee(ByVal Index As Integer) As System.DateTime
            Get
                Dim HeureLue As String = ""
                Select Case Index
                    Case 1
                        HeureLue = WsLissage_N1
                    Case 2
                        HeureLue = WsLissage_N2
                    Case 3
                        HeureLue = WsLissage_N3
                    Case 4
                        HeureLue = WsLissage_N4
                    Case 5
                        HeureLue = WsLissage_N5
                    Case 6
                        HeureLue = WsLissage_N6
                End Select
                Return VirRhDate.HeureTypee(MyBase.Date_de_Valeur, HeureLue)
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace



