﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_MESSAGERIE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsRang As Integer = 0
        Private WsDateMessage As String
        Private WsHeureMessage As String
        Private WsModuleOrigine As String
        Private WsEmetteur As String
        Private WsObjetMessage As String
        Private WsMessage As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_MESSAGERIE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaMessageIntranet
            End Get
        End Property

        Public Property Rang() As Integer
            Get
                Return WsRang
            End Get
            Set(ByVal value As Integer)
                WsRang = value
                MyBase.Clef = value.ToString
            End Set
        End Property

        Public Property Date_Message() As String
            Get
                Return WsDateMessage
            End Get
            Set(ByVal value As String)
                WsDateMessage = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Heure_Message() As String
            Get
                Return WsHeureMessage
            End Get
            Set(ByVal value As String)
                WsHeureMessage = value
            End Set
        End Property
        Public Property ModuleOrigine() As String
            Get
                Return WsModuleOrigine
            End Get
            Set(ByVal value As String)
                WsModuleOrigine = F_FormatAlpha(value, 50)
            End Set
        End Property
        Public Property Emetteur() As String
            Get
                Return WsEmetteur
            End Get
            Set(ByVal value As String)
                WsEmetteur = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Objet_Message() As String
            Get
                Return WsObjetMessage
            End Get
            Set(ByVal value As String)
                WsObjetMessage = F_FormatAlpha(value, 250)
            End Set
        End Property

        Public Property Contenu_Message() As String
            Get
                Return WsMessage
            End Get
            Set(ByVal value As String)
                WsMessage = F_FormatAlpha(value, 2000)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Rang.ToString & VI.Tild)
                Chaine.Append(Date_Message & VI.Tild)
                Chaine.Append(Heure_Message & VI.Tild)
                Chaine.Append(ModuleOrigine & VI.Tild)
                Chaine.Append(Emetteur & VI.Tild)
                Chaine.Append(Objet_Message & VI.Tild)
                Chaine.Append(Contenu_Message)
                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Rang = CInt(F_FormatNumerique(TableauData(1)))
                Date_Message = TableauData(2)
                Heure_Message = TableauData(3)
                ModuleOrigine = F_FormatAlpha(TableauData(4), 50)
                Emetteur = F_FormatAlpha(TableauData(5), 50)
                Objet_Message = F_FormatAlpha(TableauData(6), 250)
                Contenu_Message = F_FormatAlpha(TableauData(7), 2000)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
