Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_NOTATION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_d_effet As String
        Private WsNote As Single = 0
        Private WsEtatNote As String
        Private WsNotateur As String
        Private WsCritereDecompose(4) As String
        Private WsNoteDecompose(4) As Single
        Private WsTypeNotation As String
        Private WsNoteAlpha As String
        Private WsReliquat As Integer = 0
        Private WsSensReliquat As String
        Private WsNoteReference As Single = 0
        Private WsAppreciation As String
        Private WsVoeux As String
        Private WsRapport As String
        Private WsNoteAlphaDecompose(4) As String
        Private WsCorpsReference As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_NOTATION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaNotation
            End Get
        End Property

        Public Property Date_d_effet() As String
            Get
                Return WsDate_d_effet
            End Get
            Set(ByVal value As String)
                WsDate_d_effet = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_d_effet
            End Set
        End Property

        Public Property Note() As Single
            Get
                Return VirRhFonction.ConversionDouble(Strings.Format(WsNote, "#0.00"))
            End Get
            Set(ByVal value As Single)
                WsNote = value
                MyBase.Clef = value.ToString
            End Set
        End Property

        Public Property EtatdelaNote() As String
            Get
                Return WsEtatNote
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsEtatNote = value
                    Case Else
                        WsEtatNote = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Notateur() As String
            Get
                Return WsNotateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsNotateur = value
                    Case Else
                        WsNotateur = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property CritereEvaluation(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To 4
                        Return WsCritereDecompose(Index)
                    Case Else
                        Return Nothing
                End Select
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 4
                        Select Case value.Length
                            Case Is <= 60
                                WsCritereDecompose(Index) = value
                            Case Else
                                WsCritereDecompose(Index) = Strings.Left(value, 60)
                        End Select
                End Select
            End Set
        End Property

        Public Property NoteDecomposee(ByVal Index As Integer) As Single
            Get
                Select Case Index
                    Case 0 To 4
                        Return VirRhFonction.ConversionDouble(Strings.Format(WsNoteDecompose(Index), "#0.00"))
                    Case Else
                        Return 0
                End Select
            End Get
            Set(ByVal value As Single)
                Select Case Index
                    Case 0 To 4
                        WsNoteDecompose(Index) = value
                End Select
            End Set
        End Property

        Public Property TypedeNotation() As String
            Get
                Return WsTypeNotation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsTypeNotation = value
                    Case Else
                        WsTypeNotation = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property NoteAlphabetique() As String
            Get
                Return WsNoteAlpha
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsNoteAlpha = value
                    Case Else
                        WsNoteAlpha = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Reliquat() As Integer
            Get
                Return WsReliquat
            End Get
            Set(ByVal value As Integer)
                WsReliquat = value
            End Set
        End Property

        Public Property SensduReliquat() As String
            Get
                Return WsSensReliquat
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsSensReliquat = value
                    Case Else
                        WsSensReliquat = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property NotedeReference() As Single
            Get
                Return VirRhFonction.ConversionDouble(Strings.Format(WsNoteReference, "#0.00"))
            End Get
            Set(ByVal value As Single)
                WsNoteReference = value
            End Set
        End Property

        Public Property Appreciation() As String
            Get
                Return WsAppreciation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsAppreciation = value
                    Case Else
                        WsAppreciation = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property Voeux() As String
            Get
                Return WsVoeux
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsVoeux = value
                    Case Else
                        WsVoeux = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Rapport() As String
            Get
                Return WsRapport
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsRapport = value
                    Case Else
                        WsRapport = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property NoteAlphaDecomposee(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To 4
                        Return WsNoteAlphaDecompose(Index)
                    Case Else
                        Return Nothing
                End Select
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 4
                        Select Case value.Length
                            Case Is <= 3
                                WsNoteAlphaDecompose(Index) = value
                            Case Else
                                WsNoteAlphaDecompose(Index) = Strings.Left(value, 3)
                        End Select
                End Select
            End Set
        End Property

        Public Property CorpsdeReference() As String
            Get
                Return WsCorpsReference
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsCorpsReference = value
                    Case Else
                        WsCorpsReference = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceA As Integer

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_d_effet & VI.Tild)
                Chaine.Append(Note.ToString & VI.Tild)
                Chaine.Append(EtatdelaNote & VI.Tild)
                Chaine.Append(Notateur & VI.Tild)
                For IndiceA = 0 To 4
                    Chaine.Append(CritereEvaluation(IndiceA) & VI.Tild)
                    Chaine.Append(NoteDecomposee(IndiceA).ToString & VI.Tild)
                Next IndiceA
                Chaine.Append(TypedeNotation & VI.Tild)
                Chaine.Append(NoteAlphabetique & VI.Tild)
                Chaine.Append(Reliquat.ToString & VI.Tild)
                Chaine.Append(SensduReliquat & VI.Tild)
                Chaine.Append(NotedeReference.ToString & VI.Tild)
                Chaine.Append(Appreciation & VI.Tild)
                Chaine.Append(Voeux & VI.Tild)
                Chaine.Append(Rapport & VI.Tild)
                For IndiceA = 0 To 4
                    Chaine.Append(NoteAlphaDecomposee(IndiceA) & VI.Tild)
                Next IndiceA
                Chaine.Append(CorpsdeReference)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 29 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_d_effet = TableauData(1)
                If TableauData(2) = "" Then TableauData(2) = "0"
                Note = CSng(VirRhFonction.ConversionDouble(TableauData(2)))
                EtatdelaNote = TableauData(3)
                Notateur = TableauData(4)
                For IndiceI = 0 To 4
                    CritereEvaluation(IndiceI) = TableauData((IndiceI * 2) + 5)
                    If TableauData((IndiceI * 2) + 6) = "" Then TableauData((IndiceI * 2) + 6) = "0"
                    NoteDecomposee(IndiceI) = CSng(VirRhFonction.ConversionDouble(TableauData((IndiceI * 2) + 6)))
                Next IndiceI
                TypedeNotation = TableauData(15)
                NoteAlphabetique = TableauData(16)
                If TableauData(17) = "" Then TableauData(17) = "0"
                Reliquat = CInt(TableauData(17))
                SensduReliquat = TableauData(18)
                If TableauData(19) = "" Then TableauData(19) = "0"
                NotedeReference = CSng(VirRhFonction.ConversionDouble(TableauData(19)))
                Appreciation = TableauData(20)
                Voeux = TableauData(21)
                Rapport = TableauData(22)
                For IndiceI = 0 To 4
                    NoteAlphaDecomposee(IndiceI) = TableauData(IndiceI + 23)
                Next IndiceI
                CorpsdeReference = TableauData(28)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_d_effet
                    Case 1
                        Return WsNote.ToString
                    Case 2
                        Return WsEtatNote
                    Case 3
                        Return WsNotateur
                    Case 4
                        Return CritereEvaluation(0)
                    Case 5
                        Return NoteDecomposee(0).ToString
                    Case 6
                        Return CritereEvaluation(1)
                    Case 7
                        Return NoteDecomposee(1).ToString
                    Case 8
                        Return CritereEvaluation(2)
                    Case 9
                        Return NoteDecomposee(2).ToString
                    Case 10
                        Return CritereEvaluation(3)
                    Case 11
                        Return NoteDecomposee(3).ToString
                    Case 12
                        Return CritereEvaluation(4)
                    Case 13
                        Return NoteDecomposee(4).ToString
                    Case 14
                        Return WsTypeNotation
                    Case 15
                        Return WsNoteAlpha
                    Case 16
                        Return WsReliquat.ToString
                    Case 17
                        Return WsSensReliquat
                    Case 18
                        Return WsNoteReference.ToString
                    Case 19
                        Return WsAppreciation
                    Case 20
                        Return WsVoeux
                    Case 21
                        Return WsRapport
                    Case 22
                        Return NoteAlphaDecomposee(0)
                    Case 23
                        Return NoteAlphaDecomposee(1)
                    Case 24
                        Return NoteAlphaDecomposee(2)
                    Case 25
                        Return NoteAlphaDecomposee(3)
                    Case 26
                        Return NoteAlphaDecomposee(4)
                    Case 27
                        Return WsCorpsReference
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
