﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_ENTRETIEN_CITE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsResponsable As String
        Private WsLibAptitude(3) As String
        Private WsBilanAptitude(3) As Integer
        Private WsLibOrientation As String
        Private WsNiveauOrientation As Integer
        Private WsLibAnalyseFormation(3) As String
        Private WsNiveauFormation(3) As Integer
        Private WsLibDemandeFormation(3) As String
        Private WsStatutDemandeFormation(3) As Integer
        Private WsObservationDemandeFormation(3) As String
        Private WsSiDemandeAugmentation As String
        Private WsStatutAugmentation As Integer
        Private WsObservationAugmentation As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENTRETIEN"
            End Get
        End Property

        Public ReadOnly Property NomClient() As String
            Get
                Return "Cité de la Musique"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaEntretien
            End Get
        End Property

        Public Property ResponsableEntretien() As String
            Get
                Return WsResponsable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsResponsable = value
                    Case Else
                        WsResponsable = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Libelle_AptitudeTechnique(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To 3
                        Return WsLibAptitude(Index)
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 3
                        Select Case value.Length
                            Case Is <= 80
                                WsLibAptitude(Index) = value
                            Case Else
                                WsLibAptitude(Index) = Strings.Left(value, 80)
                        End Select
                End Select
            End Set
        End Property

        Public Property Bilan_AptitudeTechnique(ByVal Index As Integer) As Integer
            Get
                Select Case Index
                    Case 0 To 3
                        Return WsBilanAptitude(Index)
                End Select
                Return 0
            End Get
            Set(ByVal value As Integer)
                Select Case Index
                    Case 0 To 3
                        WsBilanAptitude(Index) = value
                End Select
            End Set
        End Property

        Public Property Libelle_OrientationAnnuelle() As String
            Get
                Return WsLibOrientation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsLibOrientation = value
                    Case Else
                        WsLibOrientation = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Niveau_OrientationAnnuelle() As Integer
            Get
                Return WsNiveauOrientation
            End Get
            Set(ByVal value As Integer)
                WsNiveauOrientation = value
            End Set
        End Property

        Public Property Libelle_AnalyseFormation(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To 3
                        Return WsLibAnalyseFormation(Index)
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 3
                        Select Case value.Length
                            Case Is <= 80
                                WsLibAnalyseFormation(Index) = value
                            Case Else
                                WsLibAnalyseFormation(Index) = Strings.Left(value, 80)
                        End Select
                End Select
            End Set
        End Property

        Public Property Niveau_AnalyseFormation(ByVal Index As Integer) As Integer
            Get
                Select Case Index
                    Case 0 To 3
                        Return WsNiveauFormation(Index)
                End Select
                Return 0
            End Get
            Set(ByVal value As Integer)
                Select Case Index
                    Case 0 To 3
                        WsNiveauFormation(Index) = value
                End Select
            End Set
        End Property

        Public Property Libelle_DemandeFormation(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To 3
                        Return WsLibDemandeFormation(Index)
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 3
                        Select Case value.Length
                            Case Is <= 80
                                WsLibDemandeFormation(Index) = value
                            Case Else
                                WsLibDemandeFormation(Index) = Strings.Left(value, 80)
                        End Select
                End Select
            End Set
        End Property

        Public Property Statut_DemandeFormation(ByVal Index As Integer) As Integer
            Get
                Select Case Index
                    Case 0 To 3
                        Return WsStatutDemandeFormation(Index)
                End Select
                Return 0
            End Get
            Set(ByVal value As Integer)
                Select Case Index
                    Case 0 To 3
                        WsStatutDemandeFormation(Index) = value
                End Select
            End Set
        End Property

        Public Property Observation_DemandeFormation(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To 3
                        Return WsObservationDemandeFormation(Index)
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 3
                        Select Case value.Length
                            Case Is <= 80
                                WsObservationDemandeFormation(Index) = value
                            Case Else
                                WsObservationDemandeFormation(Index) = Strings.Left(value, 80)
                        End Select
                End Select
            End Set
        End Property

        Public Property SiDemande_Augmentation() As String
            Get
                Return WsSiDemandeAugmentation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiDemandeAugmentation = value
                    Case Else
                        WsSiDemandeAugmentation = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Statut_DemandeAugmentation() As Integer
            Get
                Return WsStatutAugmentation
            End Get
            Set(ByVal value As Integer)
                WsStatutAugmentation = value
            End Set
        End Property

        Public Property Observation_DemandeAugmentation() As String
            Get
                Return WsObservationAugmentation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsObservationAugmentation = value
                    Case Else
                        WsObservationAugmentation = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(ResponsableEntretien & VI.Tild)
                For IndiceI = 0 To 3
                    Chaine.Append(Libelle_AptitudeTechnique(IndiceI) & VI.Tild)
                    Chaine.Append(Bilan_AptitudeTechnique(IndiceI).ToString & VI.Tild)
                Next IndiceI
                Chaine.Append(Libelle_OrientationAnnuelle & VI.Tild)
                Chaine.Append(Niveau_OrientationAnnuelle.ToString & VI.Tild)
                For IndiceI = 0 To 3
                    Chaine.Append(Libelle_AnalyseFormation(IndiceI) & VI.Tild)
                    Chaine.Append(Niveau_AnalyseFormation(IndiceI).ToString & VI.Tild)
                Next IndiceI
                For IndiceI = 0 To 3
                    Chaine.Append(Libelle_DemandeFormation(IndiceI) & VI.Tild)
                    Chaine.Append(Statut_DemandeFormation(IndiceI).ToString & VI.Tild)
                    Chaine.Append(Observation_DemandeFormation(IndiceI) & VI.Tild)
                Next IndiceI
                Chaine.Append(SiDemande_Augmentation & VI.Tild)
                Chaine.Append(Statut_DemandeAugmentation.ToString & VI.Tild)
                Chaine.Append(Observation_DemandeAugmentation)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer
                Dim IndiceK As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 36 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                ResponsableEntretien = TableauData(2)
                IndiceK = 3
                For IndiceI = 0 To 3
                    Libelle_AptitudeTechnique(IndiceI) = TableauData(IndiceK)
                    IndiceK += 1
                    If TableauData(IndiceK) <> "" Then Bilan_AptitudeTechnique(IndiceI) = CInt(Val(TableauData(IndiceK)))
                    IndiceK += 1
                Next IndiceI
                Libelle_OrientationAnnuelle = TableauData(11)
                If TableauData(12) <> "" Then Niveau_OrientationAnnuelle = CInt(Val(TableauData(12)))
                IndiceK = 13
                For IndiceI = 0 To 3
                    Libelle_AnalyseFormation(IndiceI) = TableauData(IndiceK)
                    IndiceK += 1
                    If TableauData(IndiceK) <> "" Then Niveau_AnalyseFormation(IndiceI) = CInt(Val(TableauData(IndiceK)))
                    IndiceK += 1
                Next IndiceI
                IndiceK = 21
                For IndiceI = 0 To 3
                    Libelle_DemandeFormation(IndiceI) = TableauData(IndiceK)
                    IndiceK += 1
                    If TableauData(IndiceK) <> "" Then Statut_DemandeFormation(IndiceI) = CInt(Val(TableauData(IndiceK)))
                    IndiceK += 1
                    Observation_DemandeFormation(IndiceI) = TableauData(IndiceK)
                    IndiceK += 1
                Next IndiceI
                SiDemande_Augmentation = TableauData(33)
                If TableauData(34) <> "" Then Statut_DemandeAugmentation = CInt(Val(TableauData(34)))
                Observation_DemandeAugmentation = TableauData(35)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace





