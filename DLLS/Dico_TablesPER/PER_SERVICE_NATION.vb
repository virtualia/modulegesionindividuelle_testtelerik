Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_SERVICE_NATION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsSituation_militaire As String
        Private WsPosition As String
        Private WsArme As String
        Private WsDate_d_incorporation As String
        Private WsFin_d_incorporation As String
        Private WsAnnees As Integer = 0
        Private WsMois As Integer = 0
        Private WsJours As Integer = 0
        Private WsMatricule As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_SERVICE_NATION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaArmee
            End Get
        End Property

        Public Property Situation_militaire() As String
            Get
                Return WsSituation_militaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsSituation_militaire = value
                    Case Else
                        WsSituation_militaire = Strings.Left(value, 40)
                End Select
                MyBase.Clef = WsSituation_militaire
            End Set
        End Property

        Public Property Position() As String
            Get
                Return WsPosition
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsPosition = value
                    Case Else
                        WsPosition = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Arme() As String
            Get
                Return WsArme
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsArme = value
                    Case Else
                        WsArme = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Date_d_incorporation() As String
            Get
                Return WsDate_d_incorporation
            End Get
            Set(ByVal value As String)
                WsDate_d_incorporation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Fin_d_incorporation() As String
            Get
                Return WsFin_d_incorporation
            End Get
            Set(ByVal value As String)
                WsFin_d_incorporation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Annees() As Integer
            Get
                Return WsAnnees
            End Get
            Set(ByVal value As Integer)
                WsAnnees = value
            End Set
        End Property

        Public Property Mois() As Integer
            Get
                Return WsMois
            End Get
            Set(ByVal value As Integer)
                WsMois = value
            End Set
        End Property

        Public Property Jours() As Integer
            Get
                Return WsJours
            End Get
            Set(ByVal value As Integer)
                WsJours = value
            End Set
        End Property

        Public Property Matricule() As String
            Get
                Return WsMatricule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsMatricule = value
                    Case Else
                        WsMatricule = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Situation_militaire & VI.Tild)
                Chaine.Append(Position & VI.Tild)
                Chaine.Append(Arme & VI.Tild)
                Chaine.Append(Date_d_incorporation & VI.Tild)
                Chaine.Append(Fin_d_incorporation & VI.Tild)
                Chaine.Append(Annees.ToString & VI.Tild)
                Chaine.Append(Mois.ToString & VI.Tild)
                Chaine.Append(Jours.ToString & VI.Tild)
                Chaine.Append(Matricule)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 10 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Situation_militaire = TableauData(1)
                Position = TableauData(2)
                Arme = TableauData(3)
                Date_d_incorporation = TableauData(4)
                Fin_d_incorporation = TableauData(5)
                If TableauData(6) = "" Then TableauData(6) = "0"
                Annees = CInt(TableauData(6))
                If TableauData(7) = "" Then TableauData(7) = "0"
                Mois = CInt(TableauData(7))
                If TableauData(8) = "" Then TableauData(8) = "0"
                Jours = CInt(TableauData(8))
                Matricule = TableauData(9)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Private ReadOnly Property VDureeServiceNational(ByVal Methode As Integer) As String
            Get
                Return MiseEnFormeAnciennete(Methode, "00" & Strings.Format(Annees, "00") & Strings.Format(Mois, "00") & Strings.Format(Jours, "00"))
            End Get
        End Property


        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return ""
                    Case 1
                        Return WsSituation_militaire
                    Case 2
                        Return WsPosition
                    Case 3
                        Return WsArme
                    Case 4
                        Return WsDate_d_incorporation
                    Case 5
                        Return WsFin_d_incorporation
                    Case 6
                        Return WsAnnees.ToString
                    Case 7
                        Return WsMois.ToString
                    Case 8
                        Return WsJours.ToString
                    Case 9
                        Return WsMatricule
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


