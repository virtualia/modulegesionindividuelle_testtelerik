﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_BANQUE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsCodeBanque As String
        Private WsCodeGuichet As String
        Private WsNumeroCompte As String
        Private WsCleRIB As String
        Private WsDomiciliation As String
        Private WsModeReglement As String
        Private WsIban As String = ""
        Private WsBIC As String = ""

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_BANQUE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaBanque
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 9
            End Get
        End Property

        Public Property Code_Banque() As String
            Get
                If WsCodeBanque <> "" Then
                    Return Strings.Format(CInt(WsCodeBanque), "00000")
                Else
                    Return ""
                End If
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsCodeBanque = value
                    Case Else
                        WsCodeBanque = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Code_Guichet() As String
            Get
                If WsCodeGuichet <> "" Then
                    Return Strings.Format(CInt(WsCodeGuichet), "00000")
                Else
                    Return ""
                End If
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsCodeGuichet = value
                    Case Else
                        WsCodeGuichet = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property NumeroduCompte() As String
            Get
                Return WsNumeroCompte
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 15
                        WsNumeroCompte = value
                    Case Else
                        WsNumeroCompte = Strings.Left(value, 15)
                End Select
            End Set
        End Property

        Public Property CleRIB() As String
            Get
                If WsCleRIB <> "" Then
                    Return Strings.Format(CInt(WsCleRIB), "00")
                Else
                    Return ""
                End If
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsCleRIB = value
                    Case Else
                        WsCleRIB = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Domiciliation() As String
            Get
                Return WsDomiciliation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsDomiciliation = value
                    Case Else
                        WsDomiciliation = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property ModedeReglement() As String
            Get
                Return WsModeReglement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsModeReglement = value
                    Case Else
                        WsModeReglement = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property IBAN() As String
            Get
                Return WsIban
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsIban = value
                    Case Else
                        WsIban = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property BIC() As String
            Get
                Return WsBIC
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 11
                        WsBIC = value
                    Case Else
                        WsBIC = Strings.Left(value, 11)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Code_Banque & VI.Tild)
                Chaine.Append(Code_Guichet & VI.Tild)
                Chaine.Append(NumeroduCompte & VI.Tild)
                Chaine.Append(CleRIB & VI.Tild)
                Chaine.Append(Domiciliation & VI.Tild)
                Chaine.Append(ModedeReglement & VI.Tild)
                Chaine.Append(IBAN & VI.Tild)
                Chaine.Append(BIC & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 10 Then
                    Exit Property
                End If
                Dim iban_se = TableauData(7).Replace(" ", "")
                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) <> "" Then
                Else
                    Code_Banque = iban_se.Substring(4, 5)
                End If
                TableauData(1) = Code_Banque

                If TableauData(2) <> "" Then
                Else
                    Code_Guichet = iban_se.Substring(11, 3)
                End If
                TableauData(2) = Code_Guichet

                If TableauData(3) <> "" Then
                Else
                    NumeroduCompte = iban_se.Substring(14, 11)
                End If
                TableauData(3) = NumeroduCompte

                If TableauData(4) <> "" Then
                Else
                    CleRIB = iban_se.Substring(25, 2)
                End If
                TableauData(4) = CleRIB

                Domiciliation = TableauData(5)
                ModedeReglement = TableauData(6)
                IBAN = TableauData(7)
                If TableauData.Count > 8 Then
                    BIC = TableauData(8)
                End If
                MyBase.Certification = TableauData(9)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                If IBAN = "" Then
                    Call CalculerIBAN_FR()
                ElseIf SiOK_IBAN = False Then
                    IBAN = ""
                End If
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_RIB(ByVal Methode As Integer) As String
            Get
                Dim Chaine As New System.Text.StringBuilder
                Select Case NumeroduCompte
                    Case Is <> ""
                        Chaine.Append(Code_Banque & Strings.Space(1))
                        Chaine.Append(Code_Guichet & Strings.Space(1))
                        Chaine.Append(NumeroduCompte & Strings.Space(1))
                        If Methode = VI.OptionInfo.DicoRecherche Then
                            Chaine.Append(CleRIB & Strings.Chr(13) & Strings.Chr(10))
                        Else
                            Chaine.Append(CleRIB & Strings.Space(1))
                        End If
                        Chaine.Append(Domiciliation)
                End Select
                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<IBAN>" & IBAN & "</IBAN>" & vbCrLf)
                Chaine.Append("<BIC>" & BIC & "</BIC>" & vbCrLf)
                Chaine.Append("<CodeBanque>" & Code_Banque & "</CodeBanque>" & vbCrLf)
                Chaine.Append("<CodeGuichet>" & Code_Guichet & "</CodeGuichet>" & vbCrLf)
                Chaine.Append("<NumeroCompte>" & NumeroduCompte & "</NumeroCompte>" & vbCrLf)
                Chaine.Append("<CleRIB>" & CleRIB & "</CleRIB>" & vbCrLf)
                Chaine.Append("<DomiciliationBancaire>" & VirRhFonction.ChaineXMLValide(Domiciliation) & "</DomiciliationBancaire>" & vbCrLf)
                Chaine.Append("<ModeReglement>" & ModedeReglement & "</ModeReglement>" & vbCrLf)

                Return Chaine.ToString

            End Get
        End Property

        Public ReadOnly Property SiOK_RIB() As Boolean
            Get
                Return VirRhFonction.VerificationRib(Code_Banque, Code_Guichet, NumeroduCompte, CleRIB)
            End Get
        End Property

        Public ReadOnly Property SiOK_IBAN() As Boolean
            Get
                Dim ChaineW As String
                Dim ChaineCalcul As String
                Dim IbanNum As Integer
                Dim Reste As String = ""
                Dim CleIban As Integer
                Dim ChaineConvertie As New System.Text.StringBuilder
                Dim I As Integer

                If IBAN = "" Then
                    Return False
                End If
                ChaineW = IBAN.Replace(Strings.Space(1), "")
                For I = 5 To ChaineW.Length
                    ChaineConvertie.Append(ConversionLettreIban(Strings.Mid(ChaineW, I, 1)))
                Next I
                For I = 1 To 4
                    ChaineConvertie.Append(ConversionLettreIban(Strings.Mid(ChaineW, I, 1)))
                Next I
                ChaineW = ChaineConvertie.ToString
                Do
                    If ChaineW.Length >= 9 Then
                        ChaineCalcul = Strings.Left(ChaineW, 9)
                    Else
                        ChaineCalcul = ChaineW
                    End If
                    Try
                        IbanNum = Int(VirRhFonction.ConversionDouble(ChaineCalcul, 0) \ 97)
                        CleIban = CInt((VirRhFonction.ConversionDouble(ChaineCalcul, 0) - (IbanNum * 97)))
                        Reste = CleIban.ToString
                        If ChaineCalcul = ChaineW Then
                            Exit Do
                        End If
                    Catch ex As Exception
                        Return False
                    End Try
                    ChaineCalcul = Strings.Right(ChaineW, ChaineW.Length - 9)
                    ChaineW = Reste & ChaineCalcul
                Loop
                If CleIban = 1 Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property

        Public ReadOnly Property V_IBAN_Edite As String
            Get
                If IBAN = "" Then
                    Return ""
                End If
                Dim I As Integer
                Dim ChaineW As String
                Dim ChaineEditee As New System.Text.StringBuilder

                ChaineW = Strings.Replace(WsIban, Strings.Space(1), "")
                For I = 1 To ChaineW.Length Step 4
                    ChaineEditee.Append(Strings.Mid(ChaineW, I, 4))
                    ChaineEditee.Append(Strings.Space(1))
                Next I
                Return ChaineEditee.ToString.TrimEnd
            End Get
        End Property

        Public Sub CalculerIBAN_FR()
            If SiOK_RIB = False Then
                Exit Sub
            End If
            Dim IRib As String
            Dim IbanFrance As New System.Text.StringBuilder
            Dim ChaineW As String
            Dim ChaineCalcul As String
            Dim IbanNum As Integer
            Dim Reste As String
            Dim CleIban As Integer
            Dim ICompte As String = NumeroduCompte.Trim
            Dim I As Integer

            If ICompte.Length > 11 Then
                ICompte = Strings.Left(NumeroduCompte.Trim, 11)
            End If
            If ICompte.Length < 11 Then
                ICompte = Strings.StrDup(11 - ICompte.Length, "0") & NumeroduCompte.Trim
            End If
            IRib = Code_Banque & Code_Guichet & ICompte & CleRIB
            For I = 1 To IRib.Length
                IbanFrance.Append(ConversionLettreIban(Strings.Mid(IRib, I, 1)))
            Next I
            IbanFrance.Append("152700") 'FR00 = 152700
            ChaineW = IbanFrance.ToString
            Do
                If ChaineW.Length >= 9 Then
                    ChaineCalcul = Strings.Left(ChaineW, 9)
                Else
                    ChaineCalcul = ChaineW
                End If
                IbanNum = Int(VirRhFonction.ConversionDouble(ChaineCalcul, 0) \ 97)
                CleIban = CInt((VirRhFonction.ConversionDouble(ChaineCalcul, 0) - (IbanNum * 97)))
                Reste = CleIban.ToString
                If ChaineCalcul = ChaineW Then
                    Exit Do
                End If
                ChaineCalcul = Strings.Right(ChaineW, ChaineW.Length - 9)
                ChaineW = Reste & ChaineCalcul
            Loop
            WsIban = "FR" & Strings.Format(98 - CleIban, "00") & IRib
            ChaineW = V_IBAN_Edite
            WsIban = ChaineW
        End Sub

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Return IBAN & VI.PointVirgule & BIC & VI.PointVirgule & Domiciliation
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

