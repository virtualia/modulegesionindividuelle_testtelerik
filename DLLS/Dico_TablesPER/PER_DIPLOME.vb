Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_DIPLOME
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDiplome As String
        Private WsNiveau As String
        Private WsSpecialite As String
        Private WsPromotion As String
        Private WsNiveau_en As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_DIPLOME"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaDiplome
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 6
            End Get
        End Property

        Public Property Diplome() As String
            Get
                Return WsDiplome
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsDiplome = value
                    Case Else
                        WsDiplome = Strings.Left(value, 100)
                End Select
                MyBase.Clef = WsDiplome
            End Set
        End Property

        Public Property Niveau() As String
            Get
                Return WsNiveau
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsNiveau = value
                    Case Else
                        WsNiveau = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Specialite() As String
            Get
                Return WsSpecialite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsSpecialite = value
                    Case Else
                        WsSpecialite = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Promotion() As String
            Get
                Return WsPromotion
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsPromotion = value
                    Case Else
                        WsPromotion = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Niveau_EN() As String
            Get
                Return WsNiveau_en
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsNiveau_en = value
                    Case Else
                        WsNiveau_en = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Diplome & VI.Tild)
                Chaine.Append(Niveau & VI.Tild)
                Chaine.Append(Specialite & VI.Tild)
                Chaine.Append(Promotion & VI.Tild)
                Chaine.Append(Niveau_EN & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Diplome = TableauData(2)
                Niveau = TableauData(3)
                Specialite = TableauData(4)
                Promotion = TableauData(5)
                Niveau_EN = TableauData(6)
                MyBase.Certification = TableauData(7)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public ReadOnly Property AnneeAcquisition() As String
            Get
                If Date_de_valeur <> "" Then
                    Return Strings.Right(Date_de_valeur, 4)
                Else
                    Return ""
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<DateEffetDiplome>" & MyBase.Date_de_Valeur & "</DateEffetDiplome>" & vbCrLf)
                Chaine.Append("<Diplome>" & Diplome & "</Diplome>" & vbCrLf)
                Chaine.Append("<NiveauDiplome>" & VirRhFonction.ChaineXMLValide(Niveau) & "</NiveauDiplome>" & vbCrLf)
                Chaine.Append("<SpecialiteDiplome>" & VirRhFonction.ChaineXMLValide(Specialite) & "</SpecialiteDiplome>" & vbCrLf)
                Chaine.Append("<PromotionDiplome>" & VirRhFonction.ChaineXMLValide(Promotion) & "</PromotionDiplome>" & vbCrLf)
                Chaine.Append("<NiveauENDiplome>" & VirRhFonction.ChaineXMLValide(Niveau_EN) & "</NiveauENDiplome>" & vbCrLf)

                Return Chaine.ToString

            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

