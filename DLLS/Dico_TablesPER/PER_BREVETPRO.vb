Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_BREVETPRO
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsCompetence As String
        Private WsDelivre_par As String
        Private WsNumero As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_BREVETPRO"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaCompetence
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 5
            End Get
        End Property

        Public Property Competence() As String
            Get
                Return WsCompetence
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsCompetence = value
                    Case Else
                        WsCompetence = Strings.Left(value, 100)
                End Select
                MyBase.Clef = WsCompetence
            End Set
        End Property

        Public Property Delivre_par() As String
            Get
                Return WsDelivre_par
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsDelivre_par = value
                    Case Else
                        WsDelivre_par = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Numero() As String
            Get
                Return WsNumero
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNumero = value
                    Case Else
                        WsNumero = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Competence & VI.Tild)
                Chaine.Append(Delivre_par & VI.Tild)
                Chaine.Append(Numero & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Competence = TableauData(2)
                Delivre_par = TableauData(3)
                Numero = TableauData(4)
                MyBase.Date_de_Fin = TableauData(5)
                MyBase.Certification = TableauData(6)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Property V_GrilleVirtuelle As List(Of String)
            Get
                Dim LstGrid As New List(Of String)
                LstGrid.Add(MyBase.Date_de_Valeur)
                LstGrid.Add(MyBase.Date_de_Fin)
                LstGrid.Add(Competence)
                LstGrid.Add(Delivre_par)
                LstGrid.Add(Numero)
                LstGrid.Add(MyBase.Ide_Dossier.ToString & "_" & MyBase.Date_de_Valeur)
                Return LstGrid
            End Get
            Set(ByVal value As List(Of String))
                If value Is Nothing Then
                    Exit Property
                End If
                MyBase.Date_de_Valeur = value(0)
                MyBase.Date_de_Fin = value(1)
                Competence = value(2)
                Delivre_par = value(3)
                Numero = value(4)
            End Set
        End Property

        Public Sub FaireDicoVirtuel()
            Dim InfoBase As Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Date de valeur"
            InfoBase.Longueur = 10
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Date de Fin"
            InfoBase.Longueur = 10
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Comp�tence ou qualification"
            InfoBase.Longueur = 100
            InfoBase.NatureDonnee = "Table"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = VI.PointdeVue.PVueGeneral
            InfoBase.NomTable = "Comp�tence"
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Organisme ayant d�livr� la qualification"
            InfoBase.Longueur = 80
            InfoBase.NatureDonnee = "Alpha"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Num�ro"
            InfoBase.Longueur = 20
            InfoBase.NatureDonnee = "Alpha"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)
        End Sub

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

