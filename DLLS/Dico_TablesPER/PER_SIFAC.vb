﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_SIFAC
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNumDos As Integer
        Private WsTypeVb As String
        Private WsNoOrdre As Integer
        Private WsObjetCout As String
        Private WsCentreFinancier As String
        Private WsCentreCouts As String
        Private WsLibelCentreCouts As String
        Private WsDomaineFonctionnel As String
        Private WsIntituleDomaine As String
        Private WsElementOTP As String
        Private WsDesignationEOTP As String
        Private WsCodePrime As String
        Private WsPourcentage As Double
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_SIFAC"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaSIFAC
            End Get
        End Property

        Public Property NumDos_DGFIP() As Integer
            Get
                Return WsNumDos
            End Get
            Set(ByVal value As Integer)
                WsNumDos = value
            End Set
        End Property

        Public Property Type_VB() As String
            Get
                Return WsTypeVb
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1
                        WsTypeVb = value
                    Case Else
                        WsTypeVb = Strings.Left(value, 1)
                End Select
            End Set
        End Property

        Public Property NoOrdre() As Integer
            Get
                If WsNoOrdre = 0 Then
                    WsNoOrdre = 1
                End If
                Return WsNoOrdre
            End Get
            Set(ByVal value As Integer)
                If value <= 0 Or value > 9 Then
                    value = 1
                End If
                WsNoOrdre = value
            End Set
        End Property

        Public Property Objet_Cout() As String
            Get
                Return WsObjetCout
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsObjetCout = value
                    Case Else
                        WsObjetCout = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property CentreFinancier() As String
            Get
                Return WsCentreFinancier
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 7
                        WsCentreFinancier = value
                    Case Else
                        WsCentreFinancier = Strings.Left(value, 7)
                End Select
            End Set
        End Property
        Public Property Centre_Couts() As String
            Get
                Return WsCentreCouts
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsCentreCouts = value
                    Case Else
                        WsCentreCouts = Strings.Left(value, 10)
                End Select
                If WsCentreCouts.Length = 10 Then
                    WsObjetCout = Strings.Left(WsCentreCouts, 3)
                    WsCentreFinancier = Strings.Right(WsCentreCouts, 7)
                End If
            End Set
        End Property
        Public Property Libelle_CentreCouts() As String
            Get
                Return WsLibelCentreCouts
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsLibelCentreCouts = value
                    Case Else
                        WsLibelCentreCouts = Strings.Left(value, 80)
                End Select
            End Set
        End Property
        Public Property Domaine_Fonctionnel() As String
            Get
                Return WsDomaineFonctionnel
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 15
                        WsDomaineFonctionnel = value
                    Case Else
                        WsDomaineFonctionnel = Strings.Left(value, 15)
                End Select
            End Set
        End Property
        Public Property Intitule_Domaine() As String
            Get
                Return WsIntituleDomaine
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsIntituleDomaine = value
                    Case Else
                        WsIntituleDomaine = Strings.Left(value, 80)
                End Select
            End Set
        End Property
        Public Property Element_OTP() As String
            Get
                Return WsElementOTP
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 24
                        WsElementOTP = value
                    Case Else
                        WsElementOTP = Strings.Left(value, 24)
                End Select
            End Set
        End Property
        Public Property Designation_EOTP() As String
            Get
                Return WsDesignationEOTP
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsDesignationEOTP = value
                    Case Else
                        WsDesignationEOTP = Strings.Left(value, 80)
                End Select
            End Set
        End Property
        Public Property CodePrime() As String
            Get
                Return WsCodePrime
            End Get
            Set(ByVal value As String)
                If IsNumeric(value) Then
                    WsCodePrime = Strings.Format(CInt(value), "0000")
                Else
                    Select Case value.Length
                        Case Is <= 4
                            WsCodePrime = value
                        Case Else
                            WsCodePrime = Strings.Left(value, 4)
                    End Select
                End If
            End Set
        End Property
        Public Property Pourcentage() As Double
            Get
                Return WsPourcentage
            End Get
            Set(ByVal value As Double)
                WsPourcentage = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(MyBase.Clef & VI.Tild)
                Chaine.Append(NumDos_DGFIP & VI.Tild)
                Chaine.Append(Type_VB & VI.Tild)
                Chaine.Append(NoOrdre & VI.Tild)
                Chaine.Append(Objet_Cout & VI.Tild)
                Chaine.Append(CentreFinancier & VI.Tild)
                If Centre_Couts = "" Then
                    WsCentreCouts = Objet_Cout & CentreFinancier
                End If
                Chaine.Append(Centre_Couts & VI.Tild)
                Chaine.Append(Libelle_CentreCouts & VI.Tild)
                Chaine.Append(Domaine_Fonctionnel & VI.Tild)
                Chaine.Append(Intitule_Domaine & VI.Tild)
                Chaine.Append(Element_OTP & VI.Tild)
                Chaine.Append(Designation_EOTP & VI.Tild)
                Chaine.Append(CodePrime & VI.Tild)
                Chaine.Append(Pourcentage.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 15 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                MyBase.Clef = TableauData(2)
                NumDos_DGFIP = VirRhFonction.ConversionDouble(TableauData(3), 0)
                Type_VB = TableauData(4)
                NoOrdre = VirRhFonction.ConversionDouble(TableauData(5), 0)
                Objet_Cout = TableauData(6)
                CentreFinancier = TableauData(7)
                Centre_Couts = TableauData(8)
                Libelle_CentreCouts = TableauData(9)
                Domaine_Fonctionnel = TableauData(10)
                Intitule_Domaine = TableauData(11)
                Element_OTP = TableauData(12)
                Designation_EOTP = TableauData(13)
                CodePrime = TableauData(14)
                Pourcentage = VirRhFonction.ConversionDouble(TableauData(15))

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
