﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_ACTI_ANNEXES
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsActiviteAnnexe As String
        Private WsTypeActivite As String
        Private WsFonction As String
        Private WsSecteur As String
        Private WsPresence As String
        Private WsOrganisme As String
        Private WsTextesApplicables As String
        Private WsDuree As Integer
        Private WsNumero As String
        Private WsDescriptif As String
        Private WsSiRenouvelable As String
        Private WsChargeTravail As String
        Private WsRemuneration As String
        Private WsDate_Decret As String
        Private WsDate_ParutionJO As String
        '
        Private TsRangFonction As Integer = 999 'Issu du Point de vue Commission

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ACTI_ANNEXES"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaActiAnnexes
            End Get
        End Property

        Public Property ActiviteAnnexe() As String
            Get
                Return WsActiviteAnnexe
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsActiviteAnnexe = value
                    Case Else
                        WsActiviteAnnexe = Strings.Left(value, 200)
                End Select
                MyBase.Clef = WsActiviteAnnexe
            End Set
        End Property

        Public Property TypeActivite() As String
            Get
                Return WsTypeActivite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsTypeActivite = value
                    Case Else
                        WsTypeActivite = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Fonction() As String
            Get
                Return WsFonction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsFonction = value
                    Case Else
                        WsFonction = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property Secteur() As String
            Get
                Return WsSecteur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsSecteur = value
                    Case Else
                        WsSecteur = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Presence() As String
            Get
                Return WsPresence
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsPresence = value
                    Case Else
                        WsPresence = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Organisme() As String
            Get
                Return WsOrganisme
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsOrganisme = value
                    Case Else
                        WsOrganisme = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Textes_Applicables() As String
            Get
                Return WsTextesApplicables
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsTextesApplicables = value
                    Case Else
                        WsTextesApplicables = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Duree() As Integer
            Get
                Return WsDuree
            End Get
            Set(ByVal value As Integer)
                WsDuree = value
            End Set
        End Property

        Public Property Numero() As String
            Get
                Return WsNumero
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsNumero = value
                    Case Else
                        WsNumero = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsDescriptif = value
                    Case Else
                        WsDescriptif = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property SiRenouvelable() As String
            Get
                Return WsSiRenouvelable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiRenouvelable = value
                    Case Else
                        WsSiRenouvelable = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Charge_de_travail() As String
            Get
                Return WsChargeTravail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsChargeTravail = value
                    Case Else
                        WsChargeTravail = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Remuneration() As String
            Get
                Return WsRemuneration
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsRemuneration = value
                    Case Else
                        WsRemuneration = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Date_du_Decret() As String
            Get
                Return WsDate_Decret
            End Get
            Set(ByVal value As String)
                WsDate_Decret = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_ParutionJO() As String
            Get
                Return WsDate_ParutionJO
            End Get
            Set(ByVal value As String)
                WsDate_ParutionJO = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(ActiviteAnnexe & VI.Tild)
                Chaine.Append(TypeActivite & VI.Tild)
                Chaine.Append(Fonction & VI.Tild)
                Chaine.Append(Secteur & VI.Tild)
                Chaine.Append(Presence & VI.Tild)
                Chaine.Append(Organisme & VI.Tild)
                Chaine.Append(Textes_Applicables & VI.Tild)
                Chaine.Append(Duree.ToString & VI.Tild)
                Chaine.Append(Numero & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Descriptif & VI.Tild)
                Chaine.Append(SiRenouvelable & VI.Tild)
                Chaine.Append(Charge_de_travail & VI.Tild)
                Chaine.Append(Remuneration & VI.Tild)
                Chaine.Append(Date_du_Decret & VI.Tild)
                Chaine.Append(Date_de_ParutionJO)
                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 18 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                ActiviteAnnexe = TableauData(2)
                TypeActivite = TableauData(3)
                Fonction = TableauData(4)
                Secteur = TableauData(5)
                Presence = TableauData(6)
                Organisme = TableauData(7)
                Textes_Applicables = TableauData(8)
                If TableauData(9) <> "" Then Duree = CInt(TableauData(9))
                Numero = TableauData(10)
                MyBase.Date_de_Fin = TableauData(11)
                Descriptif = TableauData(12)
                SiRenouvelable = TableauData(13)
                Charge_de_travail = TableauData(14)
                Remuneration = TableauData(15)
                Date_du_Decret = TableauData(16)
                Date_de_ParutionJO = TableauData(17)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Property DynaRang_Fonction() As Integer
            Get
                Return TsRangFonction
            End Get
            Set(ByVal value As Integer)
                TsRangFonction = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return Date_de_Valeur
                    Case 1
                        Return WsActiviteAnnexe
                    Case 2
                        Return WsTypeActivite
                    Case 3
                        Return WsFonction
                    Case 4
                        Return WsSecteur
                    Case 5
                        Return WsPresence
                    Case 6
                        Return WsOrganisme
                    Case 7
                        Return WsTextesApplicables
                    Case 8
                        Return WsDuree.ToString
                    Case 9
                        Return WsNumero
                    Case 10
                        Return Date_de_Fin
                    Case 11
                        Return WsDescriptif
                    Case 12
                        Return WsSiRenouvelable
                    Case 13
                        Return WsChargeTravail
                    Case 14
                        Return WsRemuneration
                    Case 15
                        Return WsDate_Decret
                    Case 16
                        Return WsDate_ParutionJO
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

