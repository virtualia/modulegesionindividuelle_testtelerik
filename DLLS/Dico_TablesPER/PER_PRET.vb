﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_PRET
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_d_effet As String
        Private WsMontant As Double
        Private WsTaux As Double
        Private WsDuree As Integer
        Private WsMotif As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_PRET"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaPrets
            End Get
        End Property

        Public Property Date_Effet() As String
            Get
                Return WsDate_d_effet
            End Get
            Set(ByVal value As String)
                WsDate_d_effet = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_d_effet
            End Set
        End Property

        Public Property Date_du_Pret() As String
            Get
                Return WsDate_d_effet
            End Get
            Set(ByVal value As String)
                WsDate_d_effet = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Montant() As Double
            Get
                Return WsMontant
            End Get
            Set(ByVal value As Double)
                WsMontant = value
                MyBase.Clef = WsMontant.ToString
            End Set
        End Property

        Public Property Taux() As Double
            Get
                Return WsTaux
            End Get
            Set(ByVal value As Double)
                WsTaux = value
            End Set
        End Property

        Public Property Duree() As Integer
            Get
                Return WsDuree
            End Get
            Set(ByVal value As Integer)
                WsDuree = value
            End Set
        End Property

        Public Property Motif() As String
            Get
                Return WsMotif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsMotif = value
                    Case Else
                        WsMotif = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_du_Pret & VI.Tild)
                Chaine.Append(Montant.ToString & VI.Tild)
                Chaine.Append(Taux.ToString & VI.Tild)
                Chaine.Append(Duree.ToString & VI.Tild)
                Chaine.Append(Motif & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_du_Pret = TableauData(1)
                If TableauData(2) = "" Then TableauData(2) = "0"
                Montant = VirRhFonction.ConversionDouble(TableauData(2))
                If TableauData(3) = "" Then TableauData(3) = "0"
                Taux = VirRhFonction.ConversionDouble(TableauData(3))
                If TableauData(4) = "" Then TableauData(4) = "0"
                Duree = CInt(TableauData(4))
                Motif = TableauData(5)
                MyBase.Date_de_Fin = TableauData(6)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_d_effet
                    Case 1
                        Return WsMontant.ToString
                    Case 2
                        Return WsTaux.ToString
                    Case 3
                        Return WsDuree.ToString
                    Case 4
                        Return WsMotif
                    Case 5
                        Return Date_de_Fin
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


