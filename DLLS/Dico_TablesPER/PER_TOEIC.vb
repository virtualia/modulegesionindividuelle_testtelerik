﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_TOEIC
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_Test As String
        Private WsNombrePoints As Integer
        Private WsCategorie As String
        Private WsAction As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_TOEIC"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaTOEIC
            End Get
        End Property

        Public Property Date_du_Test() As String
            Get
                Return WsDate_Test
            End Get
            Set(ByVal value As String)
                WsDate_Test = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_Test
            End Set
        End Property

        Public Property NombrePoints() As Integer
            Get
                Return WsNombrePoints
            End Get
            Set(ByVal value As Integer)
                WsNombrePoints = value
            End Set
        End Property

        Public Property Categorie_Toeic() As String
            Get
                Return WsCategorie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsCategorie = value
                    Case Else
                        WsCategorie = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Action_Toeic() As String
            Get
                Return WsAction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsAction = value
                    Case Else
                        WsAction = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_du_Test & VI.Tild)
                Chaine.Append(NombrePoints.ToString & VI.Tild)
                Chaine.Append(Categorie_Toeic & VI.Tild)
                Chaine.Append(Action_Toeic)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 5 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_du_Test = TableauData(1)
                If TableauData(2) = "" Then TableauData(2) = "0"
                NombrePoints = CInt(TableauData(2))
                Categorie_Toeic = TableauData(3)
                Action_Toeic = TableauData(4)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_Test
                    Case 1
                        Return WsNombrePoints.ToString
                    Case 2
                        Return WsCategorie
                    Case 3
                        Return WsAction
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

