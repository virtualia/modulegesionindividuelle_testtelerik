﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_EXPERIENCE_CV
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsExpPro As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_EXPERIENCE_CV"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaExperience
            End Get
        End Property

        Public Property ExperienceProfessionnelle() As String
            Get
                Return WsExpPro
            End Get
            Set(ByVal value As String)
                WsExpPro = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder
                Chaine.Append(VI.Tild)
                Chaine.Append(ExperienceProfessionnelle)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 2 Then
                    Exit Property
                End If
                Ide_Dossier = CInt(TableauData(0))
                WsExpPro = TableauData(1)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                WsFicheLue.Append(ExperienceProfessionnelle)
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return ""
                    Case 1
                        Return WsExpPro
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace



