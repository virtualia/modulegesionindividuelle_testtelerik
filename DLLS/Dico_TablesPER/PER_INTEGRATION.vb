﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_INTEGRATION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_d_effet As String
        Private WsCorps As String
        Private WsAncienneteCorps As Integer
        Private WsGrade As String
        Private WsAncienneteGrade As Integer
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_INTEGRATION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaIntegrationCorps
            End Get
        End Property

        Public Property Date_d_effet() As String
            Get
                Return WsDate_d_effet
            End Get
            Set(ByVal value As String)
                WsDate_d_effet = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_d_effet
            End Set
        End Property

        Public Property Corps() As String
            Get
                Return WsCorps
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsCorps = value
                    Case Else
                        WsCorps = Strings.Left(value, 120)
                End Select
                MyBase.Clef = WsCorps
            End Set
        End Property

        Public Property AncienneteConservee_Corps() As Integer
            Get
                Return WsAncienneteCorps
            End Get
            Set(ByVal value As Integer)
                WsAncienneteCorps = value
            End Set
        End Property

        Public Property Grade() As String
            Get
                Return WsGrade
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsGrade = value
                    Case Else
                        WsGrade = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property AncienneteConservee_Grade() As Integer
            Get
                Return WsAncienneteGrade
            End Get
            Set(ByVal value As Integer)
                WsAncienneteGrade = value
            End Set
        End Property


        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_d_effet & VI.Tild)
                Chaine.Append(Corps & VI.Tild)
                Chaine.Append(AncienneteConservee_Corps.ToString & VI.Tild)
                Chaine.Append(Grade & VI.Tild)
                Chaine.Append(AncienneteConservee_Grade.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 6 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_d_effet = TableauData(1)
                Corps = TableauData(2)
                If TableauData(3) = "" Then TableauData(3) = 0
                AncienneteConservee_Corps = CInt(TableauData(3))
                Grade = TableauData(4)
                If TableauData(5) = "" Then TableauData(5) = 0
                AncienneteConservee_Grade = CInt(TableauData(5))

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_d_effet
                    Case 1
                        Return WsCorps
                    Case 2
                        Return WsAncienneteCorps.ToString
                    Case 3
                        Return WsGrade
                    Case 4
                        Return WsAncienneteGrade.ToString
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


