﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_MODIF_PLANNING
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_de_Debut As String
        Private WsCycleTravail As String
        Private WsHoraireJournalier As String
        Private WsSiJFTravailles As String
        Private WsTypeHoraire As String
        Private WsObservations As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_MODIF_PLANNING"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 91
            End Get
        End Property

        Public Property Date_de_Debut() As String
            Get
                Return WsDate_de_Debut
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is <> ""
                        WsDate_de_Debut = VirRhDate.DateStandardVirtualia(value)
                    Case Else
                        WsDate_de_Debut = ""
                End Select
                MyBase.Date_de_Valeur = WsDate_de_Debut
            End Set
        End Property

        Public Property CycledeTravail() As String
            Get
                Return WsCycleTravail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsCycleTravail = value
                    Case Else
                        WsCycleTravail = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsCycleTravail
            End Set
        End Property

        Public Property HoraireJournalier() As String
            Get
                Return WsHoraireJournalier
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHoraireJournalier = value
                    Case Else
                        WsHoraireJournalier = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property SiJFTravailles() As String
            Get
                Return WsSiJFTravailles
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsSiJFTravailles = value
                    Case Is = "0", "Non"
                        WsSiJFTravailles = "Non"
                    Case Else
                        WsSiJFTravailles = "Oui"
                End Select
            End Set
        End Property

        Public Property TypeHoraire() As String
            Get
                Return WsTypeHoraire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsTypeHoraire = value
                    Case Else
                        WsTypeHoraire = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_de_Debut & VI.Tild)
                Chaine.Append(CycledeTravail & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(HoraireJournalier & VI.Tild)
                Chaine.Append(SiJFTravailles & VI.Tild)
                Select Case SiHoraireVariable
                    Case True
                        Chaine.Append("Oui" & VI.Tild)
                    Case False
                        Select Case SiHoraireMixte
                            Case True
                                Chaine.Append("Mix" & VI.Tild)
                            Case False
                                Chaine.Append("Non" & VI.Tild)
                        End Select
                End Select
                Chaine.Append(Observations)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_de_Debut = TableauData(1)
                CycledeTravail = TableauData(2)
                MyBase.Date_de_Fin = TableauData(3)
                HoraireJournalier = TableauData(4)
                SiJFTravailles = TableauData(5)
                Select Case TableauData(6)
                    Case Is = "Mix", "Non", "Oui"
                        WsTypeHoraire = TableauData(6)
                    Case Else
                        WsTypeHoraire = "Oui"
                End Select
                Observations = TableauData(7)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property SiHoraireVariable() As Boolean
            Get
                Select Case WsTypeHoraire
                    Case Is = "Oui"
                        Return True
                End Select
                Return False
            End Get
        End Property

        Public ReadOnly Property SiHoraireFixe() As Boolean
            Get
                Select Case WsTypeHoraire
                    Case Is = "Non"
                        Return True
                End Select
                Return False
            End Get
        End Property

        Public ReadOnly Property SiHoraireMixte() As Boolean
            Get
                Select Case WsTypeHoraire
                    Case Is = "Mix"
                        Return True
                End Select
                Return False
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_de_Debut
                    Case 1
                        Return WsCycleTravail
                    Case 2
                        Return Date_de_Fin
                    Case 3
                        Return WsHoraireJournalier
                    Case 4
                        Return WsSiJFTravailles
                    Case 5
                        Return WsTypeHoraire
                    Case 6
                        Return WsObservations
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

