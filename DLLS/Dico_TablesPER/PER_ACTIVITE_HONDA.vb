﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_ACTIVITE_HONDA
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNatureActivite As String
        Private WsNatureOperation As String
        Private WsNumeroRingi As String
        Private WsLieuOperation As String
        Private WsCommentaireSalarie As String
        Private WsDateDeclaration As String
        Private WsIdeManager As Integer
        Private WsValidationManager As String
        Private WsCommentaireManager As String
        Private WsDateValidationManager As String
        Private WsNaturePeriode As String
        Private WsMontantPrime As Double
        Private WsSiRecuperation As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ACTIVITE_HONDA"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaActiviteHonda
            End Get
        End Property

        Public Property Nature_Activite() As String
            Get
                Return WsNatureActivite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsNatureActivite = value
                    Case Else
                        WsNatureActivite = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsNatureActivite
            End Set
        End Property

        Public Property Nature_Operation() As String
            Get
                Return WsNatureOperation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsNatureOperation = value
                    Case Else
                        WsNatureOperation = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Numero_Ringi() As String
            Get
                Return WsNumeroRingi
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNumeroRingi = value
                    Case Else
                        WsNumeroRingi = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Lieu_Operation() As String
            Get
                Return WsLieuOperation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsLieuOperation = value
                    Case Else
                        WsLieuOperation = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Commentaire_Salarie() As String
            Get
                Return WsCommentaireSalarie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsCommentaireSalarie = value
                    Case Else
                        WsCommentaireSalarie = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Date_Declaration() As String
            Get
                Return WsDateDeclaration
            End Get
            Set(ByVal value As String)
                WsDateDeclaration = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Identifiant_Manager As Integer
            Get
                Return WsIdeManager
            End Get
            Set(value As Integer)
                WsIdeManager = value
            End Set
        End Property

        Public Property Validation_Manager() As String
            Get
                Return WsValidationManager
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsValidationManager = value
                    Case Else
                        WsValidationManager = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Commentaire_Manager() As String
            Get
                Return WsCommentaireManager
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsCommentaireManager = value
                    Case Else
                        WsCommentaireManager = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Date_Validation() As String
            Get
                Return WsDateValidationManager
            End Get
            Set(ByVal value As String)
                WsDateValidationManager = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Nature_Periode() As String
            Get
                Return WsNaturePeriode
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsNaturePeriode = value
                    Case Else
                        WsNaturePeriode = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Montant_Prime() As Double
            Get
                Return WsMontantPrime
            End Get
            Set(ByVal value As Double)
                WsMontantPrime = value
            End Set
        End Property

        Public Property SiDroit_Recuperation() As String
            Get
                Return WsSiRecuperation
            End Get
            Set(ByVal value As String)
                WsSiRecuperation = value
                Select Case value
                    Case Is <> "Oui"
                        WsSiRecuperation = "Non"
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Nature_Activite & VI.Tild)
                Chaine.Append(Nature_Operation & VI.Tild)
                Chaine.Append(Numero_Ringi & VI.Tild)
                Chaine.Append(Lieu_Operation & VI.Tild)
                Chaine.Append(Commentaire_Salarie & VI.Tild)
                Chaine.Append(Date_Declaration & VI.Tild)
                Chaine.Append(Identifiant_Manager.ToString & VI.Tild)
                Chaine.Append(Validation_Manager & VI.Tild)
                Chaine.Append(Commentaire_Manager & VI.Tild)
                Chaine.Append(Date_Validation & VI.Tild)
                Chaine.Append(Nature_Periode & VI.Tild)
                Chaine.Append(Montant_Prime.ToString & VI.Tild)
                Chaine.Append(SiDroit_Recuperation)
                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 15 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Nature_Activite = TableauData(2)
                Nature_Operation = TableauData(3)
                Numero_Ringi = TableauData(4)
                Lieu_Operation = TableauData(5)
                Commentaire_Salarie = TableauData(6)
                Date_Declaration = TableauData(7)
                Identifiant_Manager = VirRhFonction.ConversionDouble(TableauData(8))
                Validation_Manager = TableauData(9)
                Commentaire_Manager = TableauData(10)
                Date_Validation = TableauData(11)
                Nature_Periode = TableauData(12)
                Montant_Prime = VirRhFonction.ConversionDouble(TableauData(13))
                SiDroit_Recuperation = TableauData(14)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
