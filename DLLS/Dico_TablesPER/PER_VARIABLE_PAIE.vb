﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_VARIABLE_PAIE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsRang As Integer
        Private WsElement As String
        Private WsMontant As Double
        Private WsNombre As Double
        Private WsTaux As Double
        Private WsDate_Paie As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_VARIABLE_PAIE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaVariablePaie
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 9
            End Get
        End Property

        Public Property Rang() As Integer
            Get
                Return WsRang
            End Get
            Set(ByVal value As Integer)
                WsRang = value
                MyBase.Clef = value.ToString
            End Set
        End Property

        Public Property Date_Debut() As String
            Get
                Return MyBase.Date_de_Valeur
            End Get
            Set(ByVal value As String)
                MyBase.Date_de_Valeur = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Fin() As String
            Get
                Return MyBase.Date_de_Fin
            End Get
            Set(ByVal value As String)
                MyBase.Date_de_Fin = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Element() As String
            Get
                Return WsElement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsElement = value
                    Case Else
                        WsElement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Montant() As Double
            Get
                Return WsMontant
            End Get
            Set(ByVal value As Double)
                WsMontant = value
            End Set
        End Property

        Public Property Nombre() As Double
            Get
                Return WsNombre
            End Get
            Set(ByVal value As Double)
                WsNombre = value
            End Set
        End Property

        Public Property Taux() As Double
            Get
                Return WsTaux
            End Get
            Set(ByVal value As Double)
                WsTaux = value
            End Set
        End Property

        Public Property Date_Paie() As String
            Get
                Return WsDate_Paie
            End Get
            Set(ByVal value As String)
                WsDate_Paie = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Rang.ToString & VI.Tild)
                Chaine.Append(Date_Debut & VI.Tild)
                Chaine.Append(Date_Fin & VI.Tild)
                Chaine.Append(Element & VI.Tild)
                Chaine.Append(Montant.ToString & VI.Tild)
                Chaine.Append(Nombre.ToString & VI.Tild)
                Chaine.Append(Taux.ToString & VI.Tild)
                Chaine.Append(Date_Paie & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 10 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) = "" Then TableauData(1) = "0"
                Rang = CInt(TableauData(1))
                Date_Debut = TableauData(2)
                Date_Fin = TableauData(3)
                Element = TableauData(4)
                If TableauData(5) = "" Then TableauData(5) = "0"
                Montant = VirRhFonction.ConversionDouble(TableauData(5))
                If TableauData(6) = "" Then TableauData(6) = "0"
                Nombre = VirRhFonction.ConversionDouble(TableauData(6))
                If TableauData(7) = "" Then TableauData(7) = "0"
                Taux = VirRhFonction.ConversionDouble(TableauData(7))
                Date_Paie = TableauData(8)
                MyBase.Certification = TableauData(9)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


