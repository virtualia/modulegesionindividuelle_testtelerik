﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_AFFILIATION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNoAffiliationRetraite As String
        Private WsCaisseRetraite As String
        Private WsNoAffiliationPrevoyance As String
        Private WsCaissePrevoyance As String
        Private WsNoAffiliationComplementaire As String
        Private WsCaisseComplementaire As String
        Private WsCaisseAssuranceMaladie As String
        Private WsCaisseAssuranceChomage As String
        Private WsCaisseAssuranceAT As String
        Private WsBaseCotisationMaladie As Double

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_AFFILIATION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaCaisse
            End Get
        End Property

        Public Property Numero_AffiliationRetraite() As String
            Get
                Return WsNoAffiliationRetraite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNoAffiliationRetraite = value
                    Case Else
                        WsNoAffiliationRetraite = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property NomCaisse_Retraite() As String
            Get
                Return WsCaisseRetraite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsCaisseRetraite = value
                    Case Else
                        WsCaisseRetraite = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Numero_AffiliationPrevoyance() As String
            Get
                Return WsNoAffiliationPrevoyance
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNoAffiliationPrevoyance = value
                    Case Else
                        WsNoAffiliationPrevoyance = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property NomCaisse_Prevoyance() As String
            Get
                Return WsCaissePrevoyance
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsCaissePrevoyance = value
                    Case Else
                        WsCaissePrevoyance = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Numero_AffiliationComplementaire() As String
            Get
                Return WsNoAffiliationComplementaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNoAffiliationComplementaire = value
                    Case Else
                        WsNoAffiliationComplementaire = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property NomCaisse_Complementaire() As String
            Get
                Return WsCaisseComplementaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsCaisseComplementaire = value
                    Case Else
                        WsCaisseComplementaire = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property NomCaisse_AssuranceMaladie() As String
            Get
                Return WsCaisseAssuranceMaladie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsCaisseAssuranceMaladie = value
                    Case Else
                        WsCaisseAssuranceMaladie = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property NomCaisse_AssuranceChomage() As String
            Get
                Return WsCaisseAssuranceChomage
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsCaisseAssuranceChomage = value
                    Case Else
                        WsCaisseAssuranceChomage = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property NomCaisse_AssuranceAT() As String
            Get
                Return WsCaisseAssuranceAT
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsCaisseAssuranceAT = value
                    Case Else
                        WsCaisseAssuranceAT = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property BaseCotisation_AssuranceMaladie() As Double
            Get
                Return WsBaseCotisationMaladie
            End Get
            Set(ByVal value As Double)
                WsBaseCotisationMaladie = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Numero_AffiliationRetraite & VI.Tild)
                Chaine.Append(NomCaisse_Retraite & VI.Tild)
                Chaine.Append(Numero_AffiliationPrevoyance & VI.Tild)
                Chaine.Append(NomCaisse_Prevoyance & VI.Tild)
                Chaine.Append(Numero_AffiliationComplementaire & VI.Tild)
                Chaine.Append(NomCaisse_Complementaire & VI.Tild)
                Chaine.Append(NomCaisse_AssuranceMaladie & VI.Tild)
                Chaine.Append(NomCaisse_AssuranceChomage & VI.Tild)
                Chaine.Append(NomCaisse_AssuranceAT & VI.Tild)
                Chaine.Append(BaseCotisation_AssuranceMaladie.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Numero_AffiliationRetraite = TableauData(1)
                NomCaisse_Retraite = TableauData(2)
                Numero_AffiliationPrevoyance = TableauData(3)
                NomCaisse_Prevoyance = TableauData(4)
                Numero_AffiliationComplementaire = TableauData(5)
                NomCaisse_Complementaire = TableauData(6)
                NomCaisse_AssuranceMaladie = TableauData(7)
                NomCaisse_AssuranceChomage = TableauData(8)
                NomCaisse_AssuranceAT = TableauData(9)
                If TableauData(10) = "" Then
                    TableauData(10) = "0"
                End If
                BaseCotisation_AssuranceMaladie = VirRhFonction.ConversionDouble(TableauData(10), 2)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return ""
                    Case 1
                        Return WsNoAffiliationRetraite
                    Case 2
                        Return WsCaisseRetraite
                    Case 3
                        Return WsNoAffiliationPrevoyance
                    Case 4
                        Return WsCaissePrevoyance
                    Case 5
                        Return WsNoAffiliationComplementaire
                    Case 6
                        Return WsCaisseComplementaire
                    Case 7
                        Return WsCaisseAssuranceMaladie
                    Case 8
                        Return WsCaisseAssuranceChomage
                    Case 9
                        Return WsCaisseAssuranceAT
                    Case 10
                        Return WsBaseCotisationMaladie.ToString
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


