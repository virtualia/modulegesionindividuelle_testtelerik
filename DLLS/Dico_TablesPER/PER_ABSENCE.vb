﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_ABSENCE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNature As String
        Private WsCalendaires As Double = 0
        Private WsOuvres As Double = 0
        Private WsOuvrables As Double = 0
        Private WsObservations As String
        Private WsPleinTraitement As Double = 0
        Private WsDemiTraitement As Double = 0
        Private WsTravailles As Double = 0
        Private WsCadre_Accident As String
        Private WsNature_Accident As String
        Private WsDate_Commission As String
        Private WsTaux_Invalidite As Double = 0
        Private WsCaracteristique As String
        Private WsDate_Accident As String
        Private WsSi_Cas_de_Rechute As String
        Private WsSi_Arret_Travail As String
        Private WsDate_Saisie As String
        'Parametres absences
        Private Const DATABS_DEBUTINCLUS As String = "Date de début incluse"
        Private Const DATABS_DEBUTAPRESMIDI As String = "Date de début après-midi"
        Private Const DATABS_FININCLUS As String = "Date de fin incluse"
        Private Const DATABS_FINMATIN As String = "Date de fin matin"

        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ABSENCE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaAbsence
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return False
            End Get
        End Property

        Public Property Nature() As String
            Get
                Return WsNature
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsNature = value
                    Case Else
                        WsNature = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsNature
            End Set
        End Property

        Public Property JoursCalendaires() As Double
            Get
                Return Math.Round(WsCalendaires, 1)
            End Get
            Set(ByVal value As Double)
                WsCalendaires = value
            End Set
        End Property

        Public Property JoursOuvres() As Double
            Get
                Return Math.Round(WsOuvres, 1)
            End Get
            Set(ByVal value As Double)
                WsOuvres = value
            End Set
        End Property

        Public Property JoursOuvrables() As Double
            Get
                Return Math.Round(WsOuvrables, 1)
            End Get
            Set(ByVal value As Double)
                WsOuvrables = value
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property JoursPleinTraitement() As Double
            Get
                Return Math.Round(WsPleinTraitement, 1)
            End Get
            Set(ByVal value As Double)
                WsPleinTraitement = value
            End Set
        End Property

        Public Property JoursDemiTraitement() As Double
            Get
                Return Math.Round(WsDemiTraitement, 1)
            End Get
            Set(ByVal value As Double)
                WsDemiTraitement = value
            End Set
        End Property

        Public Property JoursTravailles() As Double
            Get
                Return Math.Round(WsTravailles, 1)
            End Get
            Set(ByVal value As Double)
                WsTravailles = value
            End Set
        End Property

        Public Property Cadre_Accident() As String
            Get
                Return WsCadre_Accident
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsCadre_Accident = value
                    Case Else
                        WsCadre_Accident = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Nature_Accident() As String
            Get
                Return WsNature_Accident
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsNature_Accident = value
                    Case Else
                        WsNature_Accident = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Date_Commission() As String
            Get
                Return WsDate_Commission
            End Get
            Set(ByVal value As String)
                WsDate_Commission = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Taux_Invalidite() As Double
            Get
                Return WsTaux_Invalidite
            End Get
            Set(ByVal value As Double)
                WsTaux_Invalidite = value
            End Set
        End Property

        Public Property Caracteristique() As String
            Get
                Return WsCaracteristique
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsCaracteristique = value
                    Case Else
                        WsCaracteristique = Strings.Left(value, 60)
                End Select
                Select Case WsCaracteristique
                    Case Is = ""
                        Select Case Strings.InStr(VirRhFonction.VirgulePoint(JoursCalendaires.ToString), ",5")
                            Case Is > 0
                                If MyBase.Date_de_Valeur = Date_de_Fin Then
                                    WsCaracteristique = DATABS_DEBUTINCLUS & " - " & DATABS_FINMATIN
                                Else
                                    MyBase.Date_de_Fin = VirRhDate.CalcDateMoinsJour(Date_de_Fin, "1", "0")
                                    WsCaracteristique = DATABS_DEBUTINCLUS & " - " & DATABS_FINMATIN
                                End If
                            Case Else
                                WsCaracteristique = DATABS_DEBUTINCLUS & " - " & DATABS_FININCLUS
                        End Select
                End Select
            End Set
        End Property

        Public Property Date_Accident() As String
            Get
                Return WsDate_Accident
            End Get
            Set(ByVal value As String)
                WsDate_Accident = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Si_Cas_de_Rechute() As String
            Get
                Return WsSi_Cas_de_Rechute
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsSi_Cas_de_Rechute = value
                    Case Is = "0", "Non"
                        WsSi_Cas_de_Rechute = "Non"
                    Case Else
                        WsSi_Cas_de_Rechute = "Oui"
                End Select
            End Set
        End Property

        Public Property Si_Arret_de_Travail() As String
            Get
                Return WsSi_Arret_Travail
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsSi_Arret_Travail = value
                    Case Is = "0", "Non"
                        WsSi_Arret_Travail = "Non"
                    Case Else
                        WsSi_Arret_Travail = "Oui"
                End Select
            End Set
        End Property

        Public Property Date_de_Saisie() As String
            Get
                Return WsDate_Saisie
            End Get
            Set(ByVal value As String)
                WsDate_Saisie = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Nature & VI.Tild)
                Select Case JoursCalendaires
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(JoursCalendaires.ToString & VI.Tild)
                End Select
                Select Case JoursOuvres
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(JoursOuvres.ToString & VI.Tild)
                End Select
                Select Case JoursOuvrables
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(JoursOuvrables.ToString & VI.Tild)
                End Select
                Chaine.Append(Observations & VI.Tild)
                Select Case JoursPleinTraitement
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(JoursPleinTraitement.ToString & VI.Tild)
                End Select
                Select Case JoursDemiTraitement
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(JoursDemiTraitement.ToString & VI.Tild)
                End Select
                Select Case JoursTravailles
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(JoursTravailles.ToString & VI.Tild)
                End Select
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Cadre_Accident & VI.Tild)
                Chaine.Append(Nature_Accident & VI.Tild)
                Chaine.Append(Date_Commission & VI.Tild)
                Select Case Taux_Invalidite
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Taux_Invalidite.ToString) & VI.Tild)
                End Select
                Chaine.Append(Caracteristique & VI.Tild)
                Chaine.Append(Date_Accident & VI.Tild)
                Chaine.Append(Si_Cas_de_Rechute & VI.Tild)
                Chaine.Append(Si_Arret_de_Travail & VI.Tild)
                Chaine.Append(Date_de_Saisie)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 20 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Nature = TableauData(2)
                If TableauData(3) <> "" Then JoursCalendaires = VirRhFonction.ConversionDouble(TableauData(3))
                If TableauData(4) <> "" Then JoursOuvres = VirRhFonction.ConversionDouble(TableauData(4))
                If TableauData(5) <> "" Then JoursOuvrables = VirRhFonction.ConversionDouble(TableauData(5))
                Observations = TableauData(6)
                If TableauData(7) <> "" Then JoursPleinTraitement = VirRhFonction.ConversionDouble(TableauData(7))
                If TableauData(8) <> "" Then JoursDemiTraitement = VirRhFonction.ConversionDouble(TableauData(8))
                If TableauData(9) <> "" Then JoursTravailles = VirRhFonction.ConversionDouble(TableauData(9))
                MyBase.Date_de_Fin = TableauData(10)
                Cadre_Accident = TableauData(11)
                Nature_Accident = TableauData(12)
                Date_Commission = TableauData(13)
                If TableauData(14) <> "" Then Taux_Invalidite = VirRhFonction.ConversionDouble(TableauData(14))
                Caracteristique = TableauData(15)
                Date_Accident = TableauData(16)
                Si_Cas_de_Rechute = TableauData(17)
                Si_Arret_de_Travail = TableauData(18)
                Date_de_Saisie = TableauData(19)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public ReadOnly Property VTypeJour(ByVal ArgumentDate As String) As Integer
            Get
                Select Case VirRhDate.ComparerDates(Date_de_Valeur, ArgumentDate)
                    Case VI.ComparaisonDates.PlusGrand
                        Return 0
                End Select
                Select Case VirRhDate.ComparerDates(Date_de_Fin, ArgumentDate)
                    Case VI.ComparaisonDates.PlusPetit
                        Return 0
                End Select
                Select Case VirRhDate.ComparerDates(Date_de_Valeur, ArgumentDate)
                    Case VI.ComparaisonDates.Egalite
                        Select Case Strings.InStr(Caracteristique, DATABS_DEBUTAPRESMIDI)
                            Case Is > 0
                                Return VI.NumeroPlage.Plage2_Absence
                        End Select
                End Select
                Select Case VirRhDate.ComparerDates(Date_de_Fin, ArgumentDate)
                    Case VI.ComparaisonDates.Egalite
                        Select Case Strings.InStr(Caracteristique, DATABS_FINMATIN)
                            Case Is > 0
                                Return VI.NumeroPlage.Plage1_Absence
                        End Select
                End Select
                Return VI.NumeroPlage.Jour_Absence
            End Get
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<DateDebutAbsence>" & MyBase.Date_de_Valeur & "</DateDebutAbsence>" & vbCrLf)
                Chaine.Append("<NatureAbsence>" & VirRhFonction.ChaineXMLValide(Nature) & "</NatureAbsence>" & vbCrLf)
                Select Case JoursCalendaires
                    Case Is = 0
                        Chaine.Append("<JoursCalendaires>" & "0" & "</JoursCalendaires>" & vbCrLf)
                    Case Else
                        Chaine.Append("<JoursCalendaires>" & VirRhFonction.VirgulePoint(JoursCalendaires.ToString) & "</JoursCalendaires>" & vbCrLf)
                End Select
                Chaine.Append("<DateFinAbsence>" & MyBase.Date_de_Fin & "</DateFinAbsence>" & vbCrLf)
                Chaine.Append("<CadreAccident>" & VirRhFonction.ChaineXMLValide(Cadre_Accident) & "</CadreAccident>" & vbCrLf)
                Chaine.Append("<NatureAccident>" & VirRhFonction.ChaineXMLValide(Nature_Accident) & "</NatureAccident>" & vbCrLf)
                Chaine.Append("<DateCommission>" & Date_Commission & "</DateCommission>" & vbCrLf)
                Select Case Taux_Invalidite
                    Case Is = 0
                        Chaine.Append("<TauxInvalidite>" & "0" & "</TauxInvalidite>" & vbCrLf)
                    Case Else
                        Chaine.Append("<TauxInvalidite>" & VirRhFonction.MontantStocke(Taux_Invalidite.ToString) & "</TauxInvalidite>" & vbCrLf)
                End Select
                Chaine.Append("<Caracteristique>" & Caracteristique & "</Caracteristique>" & vbCrLf)
                Chaine.Append("<DateAccident>" & Date_Accident & "</DateAccident>" & vbCrLf)
                Chaine.Append("<SiCasRechute>" & Si_Cas_de_Rechute & "</SiCasRechute>" & vbCrLf)
                Chaine.Append("<SiArretTravail>" & Si_Arret_de_Travail & "</SiArretTravail>" & vbCrLf)

                Return Chaine.ToString

            End Get
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Property V_GrilleVirtuelle As List(Of String)
            Get
                Dim LstGrid As New List(Of String)
                LstGrid.Add(Nature)
                LstGrid.Add(MyBase.Date_de_Valeur)
                LstGrid.Add(MyBase.Date_de_Fin)
                LstGrid.Add(JoursCalendaires.ToString)
                LstGrid.Add(JoursOuvres.ToString)
                LstGrid.Add(JoursOuvrables.ToString)
                LstGrid.Add(MyBase.Ide_Dossier.ToString & "_" & MyBase.Date_de_Valeur)
                Return LstGrid
            End Get
            Set(ByVal value As List(Of String))
                If value Is Nothing Then
                    Exit Property
                End If
                Nature = value(0)
                MyBase.Date_de_Valeur = value(1)
                MyBase.Date_de_Fin = value(2)
                JoursCalendaires = VirRhFonction.ConversionDouble(value(3))
                JoursOuvres = VirRhFonction.ConversionDouble(value(4))
                JoursOuvrables = VirRhFonction.ConversionDouble(value(5))

            End Set
        End Property

        Public Sub FaireDicoVirtuel()
            Dim InfoBase As Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Intitule"
            InfoBase.Longueur = 80
            InfoBase.NatureDonnee = "Table"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = VI.PointdeVue.PVueAbsences
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Date de valeur"
            InfoBase.Longueur = 10
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Date de Fin"
            InfoBase.Longueur = 10
            InfoBase.NatureDonnee = "Date"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Nombre de jours calendaires"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal1"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Nombre de jours ouvrés"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal1"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Nombre de jours ouvrables"
            InfoBase.Longueur = 5
            InfoBase.NatureDonnee = "Numerique"
            InfoBase.FormatDonnee = "Decimal1"
            InfoBase.IdentifiantTable = 0
            MyBase.V_ListeDicoVirtuel.Add(InfoBase)
        End Sub

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace



