Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_PIECE_IDENTITE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        Private WsSiMajuscule As Boolean = False
        '
        Private WsTitre_de_la_carte_de_travail As String
        Private WsDate_carte_de_travail As String
        Private WsNumero_carte_de_travail As String
        Private WsTitre_de_la_carte_de_sejour As String
        Private WsDate_carte_de_sejour As String
        Private WsNumero_carte_de_sejour As String
        Private WsNom_du_pere As String
        Private WsPrenom_du_pere As String
        Private WsNom_de_la_mere As String
        Private WsPrenom_de_la_mere As String
        Private WsImmatriculation_cee As String
        Private WsPasseport As String
        Private WsDate_passeport As String
        Private WsDatefincartetravail As String
        Private WsDatefincartesejour As String
        Private WsDatedudeces As String
        Private WsLieududeces As String
        Private WsNomUsage As String
        Private WsPrenomUsage As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_PIECE_IDENTITE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaEtranger
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 20
            End Get
        End Property

        Public Property Titre_de_la_carte_de_travail() As String
            Get
                Return WsTitre_de_la_carte_de_travail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsTitre_de_la_carte_de_travail = value
                    Case Else
                        WsTitre_de_la_carte_de_travail = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Date_carte_de_travail() As String
            Get
                Return WsDate_carte_de_travail
            End Get
            Set(ByVal value As String)
                WsDate_carte_de_travail = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Numero_carte_de_travail() As String
            Get
                Return WsNumero_carte_de_travail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNumero_carte_de_travail = value
                    Case Else
                        WsNumero_carte_de_travail = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Titre_de_la_carte_de_sejour() As String
            Get
                Return WsTitre_de_la_carte_de_sejour
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsTitre_de_la_carte_de_sejour = value
                    Case Else
                        WsTitre_de_la_carte_de_sejour = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Date_carte_de_sejour() As String
            Get
                Return WsDate_carte_de_sejour
            End Get
            Set(ByVal value As String)
                WsDate_carte_de_sejour = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Numero_carte_de_sejour() As String
            Get
                Return WsNumero_carte_de_sejour
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNumero_carte_de_sejour = value
                    Case Else
                        WsNumero_carte_de_sejour = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Nom_du_pere() As String
            Get
                Return WsNom_du_pere
            End Get
            Set(ByVal value As String)
                If WsSiMajuscule = True Then
                    Select Case value.Length
                        Case Is <= 35
                            WsNom_du_pere = value.ToUpper
                        Case Else
                            WsNom_du_pere = Strings.Left(value.ToUpper, 35)
                    End Select
                Else
                    Select Case value.Length
                        Case Is <= 35
                            WsNom_du_pere = VirRhFonction.Lettre1Capi(value, 2)
                        Case Else
                            WsNom_du_pere = VirRhFonction.Lettre1Capi(Strings.Left(value, 35), 2)
                    End Select
                End If
            End Set
        End Property

        Public Property Prenom_du_pere() As String
            Get
                Return WsPrenom_du_pere
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsPrenom_du_pere = VirRhFonction.Lettre1Capi(value, 1)
                    Case Else
                        WsPrenom_du_pere = VirRhFonction.Lettre1Capi(Strings.Left(value, 35), 1)
                End Select
            End Set
        End Property

        Public Property Nom_de_la_mere() As String
            Get
                Return WsNom_de_la_mere
            End Get
            Set(ByVal value As String)
                If WsSiMajuscule = True Then
                    Select Case value.Length
                        Case Is <= 35
                            WsNom_de_la_mere = value.ToUpper
                        Case Else
                            WsNom_de_la_mere = Strings.Left(value.ToUpper, 35)
                    End Select
                Else
                    Select Case value.Length
                        Case Is <= 35
                            WsNom_de_la_mere = VirRhFonction.Lettre1Capi(value, 2)
                        Case Else
                            WsNom_de_la_mere = VirRhFonction.Lettre1Capi(Strings.Left(value, 35), 2)
                    End Select
                End If
            End Set
        End Property

        Public Property Prenom_de_la_mere() As String
            Get
                Return WsPrenom_de_la_mere
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsPrenom_de_la_mere = VirRhFonction.Lettre1Capi(value, 1)
                    Case Else
                        WsPrenom_de_la_mere = VirRhFonction.Lettre1Capi(Strings.Left(value, 35), 1)
                End Select
            End Set
        End Property

        Public Property Immatriculation_cee() As String
            Get
                Return WsImmatriculation_cee
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsImmatriculation_cee = value
                    Case Else
                        WsImmatriculation_cee = Strings.Left(value, 35)
                End Select
            End Set
        End Property

        Public Property Passeport() As String
            Get
                Return WsPasseport
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsPasseport = value
                    Case Else
                        WsPasseport = Strings.Left(value, 35)
                End Select
            End Set
        End Property

        Public Property Date_passeport() As String
            Get
                Return WsDate_passeport
            End Get
            Set(ByVal value As String)
                WsDate_passeport = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Datefincartetravail() As String
            Get
                Return WsDatefincartetravail
            End Get
            Set(ByVal value As String)
                WsDatefincartetravail = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Datefincartesejour() As String
            Get
                Return WsDatefincartesejour
            End Get
            Set(ByVal value As String)
                WsDatefincartesejour = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Datedudeces() As String
            Get
                Return WsDatedudeces
            End Get
            Set(ByVal value As String)
                WsDatedudeces = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Lieududeces() As String
            Get
                Return WsLieududeces
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsLieududeces = value
                    Case Else
                        WsLieududeces = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Nom_D_Usage() As String
            Get
                Return WsNomUsage
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsNomUsage = VirRhFonction.Lettre1Capi(value, 1)
                    Case Else
                        WsNomUsage = VirRhFonction.Lettre1Capi(Strings.Left(value, 40), 1)
                End Select
            End Set
        End Property

        Public Property Prenom_D_Usage() As String
            Get
                Return WsPrenomUsage
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsPrenomUsage = VirRhFonction.Lettre1Capi(value, 1)
                    Case Else
                        WsPrenomUsage = VirRhFonction.Lettre1Capi(Strings.Left(value, 40), 1)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Titre_de_la_carte_de_travail & VI.Tild)
                Chaine.Append(Date_carte_de_travail & VI.Tild)
                Chaine.Append(Numero_carte_de_travail & VI.Tild)
                Chaine.Append(Titre_de_la_carte_de_sejour & VI.Tild)
                Chaine.Append(Date_carte_de_sejour & VI.Tild)
                Chaine.Append(Numero_carte_de_sejour & VI.Tild)
                Chaine.Append(Nom_du_pere & VI.Tild)
                Chaine.Append(Prenom_du_pere & VI.Tild)
                Chaine.Append(Nom_de_la_mere & VI.Tild)
                Chaine.Append(Prenom_de_la_mere & VI.Tild)
                Chaine.Append(Immatriculation_cee & VI.Tild)
                Chaine.Append(Passeport & VI.Tild)
                Chaine.Append(Date_passeport & VI.Tild)
                Chaine.Append(Datefincartetravail & VI.Tild)
                Chaine.Append(Datefincartesejour & VI.Tild)
                Chaine.Append(Datedudeces & VI.Tild)
                Chaine.Append(Lieududeces & VI.Tild)
                Chaine.Append(Nom_D_Usage & VI.Tild)
                Chaine.Append(Prenom_D_Usage & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 21 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Titre_de_la_carte_de_travail = TableauData(1)
                Date_carte_de_travail = TableauData(2)
                Numero_carte_de_travail = TableauData(3)
                Titre_de_la_carte_de_sejour = TableauData(4)
                Date_carte_de_sejour = TableauData(5)
                Numero_carte_de_sejour = TableauData(6)
                Nom_du_pere = TableauData(7)
                Prenom_du_pere = TableauData(8)
                Nom_de_la_mere = TableauData(9)
                Prenom_de_la_mere = TableauData(10)
                Immatriculation_cee = TableauData(11)
                Passeport = TableauData(12)
                Date_passeport = TableauData(13)
                Datefincartetravail = TableauData(14)
                Datefincartesejour = TableauData(15)
                Datedudeces = TableauData(16)
                Lieududeces = TableauData(17)
                Nom_D_Usage = TableauData(18)
                Prenom_D_Usage = TableauData(19)
                MyBase.Certification = TableauData(21)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property
        Public Property SiNom_En_Majuscule() As Boolean
            Get
                Return WsSiMajuscule
            End Get
            Set(ByVal value As Boolean)
                WsSiMajuscule = value
            End Set
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Sub New(Optional ByVal SiMajuscule As Boolean = False)
            MyBase.New()
            WsSiMajuscule = SiMajuscule
        End Sub

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property
    End Class
End Namespace

