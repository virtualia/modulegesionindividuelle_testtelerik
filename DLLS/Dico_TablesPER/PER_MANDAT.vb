﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_MANDAT
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_d_effet As String
        Private WsMandat As String
        Private WsNbHeuresAnnuel As Integer
        Private WsTauxDecharge As Integer
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_MANDAT"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaEligible
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 5
            End Get
        End Property

        Public Property Date_d_effet() As String
            Get
                Return WsDate_d_effet
            End Get
            Set(ByVal value As String)
                WsDate_d_effet = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_d_effet
            End Set
        End Property

        Public Property Mandat_Delegation() As String
            Get
                Return WsMandat
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsMandat = value
                    Case Else
                        WsMandat = Strings.Left(value, 120)
                End Select
                MyBase.Clef = WsMandat
            End Set
        End Property

        Public Property NombreHeures_Annuel() As Integer
            Get
                Return WsNbHeuresAnnuel
            End Get
            Set(ByVal value As Integer)
                WsNbHeuresAnnuel = value
            End Set
        End Property

        Public Property Taux_Decharge() As Integer
            Get
                Return WsTauxDecharge
            End Get
            Set(ByVal value As Integer)
                WsTauxDecharge = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_d_effet & VI.Tild)
                Chaine.Append(Mandat_Delegation & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(NombreHeures_Annuel.ToString & VI.Tild)
                Chaine.Append(Taux_Decharge.ToString & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_d_effet = TableauData(1)
                Mandat_Delegation = TableauData(2)
                MyBase.Date_de_Fin = TableauData(3)
                If TableauData(4) = "" Then TableauData(4) = 0
                NombreHeures_Annuel = CInt(TableauData(4))
                If TableauData(5) = "" Then TableauData(5) = 0
                Taux_Decharge = CInt(TableauData(5))
                MyBase.Certification = TableauData(6)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


