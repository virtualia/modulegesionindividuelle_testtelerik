Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_APREVENIR
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNom As String
        Private WsNom_de_la_rue As String
        Private WsComplement_d_adresse As String
        Private WsCode_postal As String
        Private WsVille As String
        Private WsTelephone As String
        Private WsCode_pays As String
        Private WsPays As String
        Private WsFax As String
        Private WsEmail As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_APREVENIR"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaPrevenir
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 11
            End Get
        End Property

        Public Property Nom() As String
            Get
                Return WsNom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsNom = value
                    Case Else
                        WsNom = Strings.Left(value, 35)
                End Select
                MyBase.Clef = WsNom
            End Set
        End Property

        Public Property Nom_de_la_rue() As String
            Get
                Return WsNom_de_la_rue
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsNom_de_la_rue = value
                    Case Else
                        WsNom_de_la_rue = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property Complement_d_adresse() As String
            Get
                Return WsComplement_d_adresse
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsComplement_d_adresse = value
                    Case Else
                        WsComplement_d_adresse = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property Code_postal() As String
            Get
                Return WsCode_postal
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsCode_postal = value
                    Case Else
                        WsCode_postal = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Ville() As String
            Get
                Return WsVille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsVille = value
                    Case Else
                        WsVille = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Telephone() As String
            Get
                Return WsTelephone
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsTelephone = value
                    Case Else
                        WsTelephone = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Code_Pays() As String
            Get
                Return WsCode_pays
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsCode_pays = value
                    Case Else
                        WsCode_pays = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Pays() As String
            Get
                Return WsPays
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsPays = value
                    Case Else
                        WsPays = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Fax() As String
            Get
                Return WsFax
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsFax = value
                    Case Else
                        WsFax = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Email() As String
            Get
                Return WsEmail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 150
                        WsEmail = value
                    Case Else
                        WsEmail = Strings.Left(value, 150)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Nom & VI.Tild)
                Chaine.Append(Nom_de_la_rue & VI.Tild)
                Chaine.Append(Complement_d_adresse & VI.Tild)
                Chaine.Append(Code_postal & VI.Tild)
                Chaine.Append(Ville & VI.Tild)
                Chaine.Append(Telephone & VI.Tild)
                Chaine.Append(Code_Pays & VI.Tild)
                Chaine.Append(Pays & VI.Tild)
                Chaine.Append(Fax & VI.Tild)
                Chaine.Append(Email & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 12 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Nom = TableauData(1)
                Nom_de_la_rue = TableauData(2)
                Complement_d_adresse = TableauData(3)
                Code_postal = TableauData(4)
                Ville = TableauData(5)
                Telephone = TableauData(6)
                Code_Pays = TableauData(7)
                Pays = TableauData(8)
                Fax = TableauData(9)
                Email = TableauData(10)
                MyBase.Certification = TableauData(11)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


