﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_ENTRETIEN_SYNTHESE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsIntituleFamille As String
        Private WsNumeroFamille As String
        Private WsCritereNumerique As Integer
        Private WsCritereLitteral As String
        Private WsNumeroTri As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENTRETIEN_SYNTHESE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaEntretienSynthese
            End Get
        End Property

        Public Property Intitule_Famille() As String
            Get
                Return WsIntituleFamille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntituleFamille = value
                    Case Else
                        WsIntituleFamille = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Numero_Famille() As String
            Get
                Return WsNumeroFamille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsNumeroFamille = value
                    Case Else
                        WsNumeroFamille = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Critere_Numerique() As Integer
            Get
                Return WsCritereNumerique
            End Get
            Set(ByVal value As Integer)
                WsCritereNumerique = value
            End Set
        End Property

        Public Property Critere_Litteral() As String
            Get
                Return WsCritereLitteral
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsCritereLitteral = value
                    Case Else
                        WsCritereLitteral = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property NumeroTri() As Integer
            Get
                Return WsNumeroTri
            End Get
            Set(ByVal value As Integer)
                WsNumeroTri = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Intitule_Famille & VI.Tild)
                Chaine.Append(Numero_Famille & VI.Tild)
                Chaine.Append(Critere_Numerique.ToString & VI.Tild)
                Chaine.Append(WsCritereLitteral & VI.Tild)
                Chaine.Append(NumeroTri.ToString)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 6 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Intitule_Famille = TableauData(2)
                Numero_Famille = TableauData(3)
                If TableauData(4) <> "" Then Critere_Numerique = CInt(VirRhFonction.ConversionDouble(TableauData(4)))
                Critere_Litteral = TableauData(5)
                If TableauData(6) <> "" Then NumeroTri = CInt(VirRhFonction.ConversionDouble(TableauData(6)))

                If WsFicheLue Is Nothing Then
                    WsFicheLue = New System.Text.StringBuilder
                    For IndiceI = 1 To TableauData.Count - 1
                        WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                    Next IndiceI
                End If
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
