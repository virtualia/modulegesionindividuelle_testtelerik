﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_MAJINTRANET
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsRang As Integer = 0
        Private WsDateDemande As String
        Private WsHeureDemande As String
        Private WsDateVisa As String
        Private WsHeureVisa As String
        Private WsNatureVisa As Integer = 0
        Private WsSignataire As String
        Private WsDateGestion As String
        Private WsHeureGestion As String
        Private WsNatureGestion As Integer = 0
        Private WsGestionnaire As String
        Private WsObservationDemandeur As String
        Private WsObservationResponsable As String
        Private WsObservationGestionnaire As String
        Private WsNumObjetPer As Integer = 0
        Private WsContenu As String
        '
        Private TsFicheAbsence As Virtualia.TablesObjet.ShemaPER.PER_ABSENCE
        '
        'Parametres absences
        Private Const DATABS_DEBUTINCLUS As String = "Date de début incluse"
        Private Const DATABS_DEBUTAPRESMIDI As String = "Date de début après-midi"
        Private Const DATABS_FININCLUS As String = "Date de fin incluse"
        Private Const DATABS_FINMATIN As String = "Date de fin matin"
        '
        Private WsObjetCalcul As Object
        '
        Private TsFicheEtatCivil As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
        Private TsFicheConjoint As Virtualia.TablesObjet.ShemaPER.PER_CONJOINT
        Private TsFicheEnfant As Virtualia.TablesObjet.ShemaPER.PER_ENFANT
        Private TsFicheDomicile As Virtualia.TablesObjet.ShemaPER.PER_DOMICILE
        Private TsFicheBanque As Virtualia.TablesObjet.ShemaPER.PER_BANQUE
        Private TsFicheDiplome As Virtualia.TablesObjet.ShemaPER.PER_DIPLOME
        Private TsFicheAdressePro As Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO
        Private TsFichePersonneAPrevenir As Virtualia.TablesObjet.ShemaPER.PER_APREVENIR
        Private TsFicheDemandeFormation As Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_MAJINTRANET"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaSaisieIntranet
            End Get
        End Property

        Public Property Rang() As Integer
            Get
                Return WsRang
            End Get
            Set(ByVal value As Integer)
                WsRang = value
                MyBase.Clef = value.ToString
            End Set
        End Property

        Public Property Date_Demande() As String
            Get
                Return WsDateDemande
            End Get
            Set(ByVal value As String)
                WsDateDemande = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Heure_Demande() As String
            Get
                Return WsHeureDemande
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHeureDemande = value
                    Case Else
                        WsHeureDemande = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Date_Visa() As String
            Get
                Return WsDateVisa
            End Get
            Set(ByVal value As String)
                WsDateVisa = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Heure_Visa() As String
            Get
                Return WsHeureVisa
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHeureVisa = value
                    Case Else
                        WsHeureVisa = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Nature_Visa() As Integer
            Get
                Return WsNatureVisa
            End Get
            Set(ByVal value As Integer)
                WsNatureVisa = value
            End Set
        End Property

        Public Property Signataire() As String
            Get
                Return WsSignataire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsSignataire = value
                    Case Else
                        WsSignataire = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Date_Gestion() As String
            Get
                Return WsDateGestion
            End Get
            Set(ByVal value As String)
                WsDateGestion = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Heure_Gestion() As String
            Get
                Return WsHeureGestion
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHeureGestion = value
                    Case Else
                        WsHeureGestion = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Nature_Gestion() As Integer
            Get
                Return WsNatureGestion
            End Get
            Set(ByVal value As Integer)
                WsNatureGestion = value
            End Set
        End Property

        Public Property Gestionnaire() As String
            Get
                Return WsGestionnaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsGestionnaire = value
                    Case Else
                        WsGestionnaire = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Observations_Demandeur() As String
            Get
                Return WsObservationDemandeur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservationDemandeur = value
                    Case Else
                        WsObservationDemandeur = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Observations_Responsable() As String
            Get
                Return WsObservationResponsable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservationResponsable = value
                    Case Else
                        WsObservationResponsable = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Observations_Gestionnaire() As String
            Get
                Return WsObservationGestionnaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservationGestionnaire = value
                    Case Else
                        WsObservationGestionnaire = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property NumeroObjetPER() As Integer
            Get
                Return WsNumObjetPer
            End Get
            Set(ByVal value As Integer)
                WsNumObjetPer = value
            End Set
        End Property

        Public Property Contenu() As String
            Get
                Return WsContenu
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsContenu = value
                    Case Else
                        WsContenu = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Rang.ToString & VI.Tild)
                Chaine.Append(Date_Demande & VI.Tild)
                Chaine.Append(Heure_Demande & VI.Tild)
                Chaine.Append(Date_Visa & VI.Tild)
                Chaine.Append(Heure_Visa & VI.Tild)
                Chaine.Append(Nature_Visa.ToString & VI.Tild)
                Chaine.Append(Signataire & VI.Tild)
                Chaine.Append(Date_Gestion & VI.Tild)
                Chaine.Append(Heure_Gestion & VI.Tild)
                Chaine.Append(Nature_Gestion.ToString & VI.Tild)
                Chaine.Append(Gestionnaire & VI.Tild)
                Chaine.Append(Observations_Demandeur & VI.Tild)
                Chaine.Append(Observations_Responsable & VI.Tild)
                Chaine.Append(Observations_Gestionnaire & VI.Tild)
                Chaine.Append(NumeroObjetPER.ToString & VI.Tild)
                Chaine.Append(Contenu)
                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 17 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) <> "" Then Rang = CInt(TableauData(1))
                Date_Demande = TableauData(2)
                Heure_Demande = TableauData(3)
                Date_Visa = TableauData(4)
                Heure_Visa = TableauData(5)
                If TableauData(6) <> "" Then Nature_Visa = CInt(TableauData(6))
                Signataire = TableauData(7)
                Date_Gestion = TableauData(8)
                Heure_Gestion = TableauData(9)
                If TableauData(10) <> "" Then Nature_Gestion = CInt(TableauData(10))
                Gestionnaire = TableauData(11)
                Observations_Demandeur = TableauData(12)
                Observations_Responsable = TableauData(13)
                Observations_Gestionnaire = TableauData(14)
                If TableauData(15) <> "" Then NumeroObjetPER = CInt(TableauData(15))
                Contenu = TableauData(16)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                Select Case NumeroObjetPER
                    Case VI.ObjetPer.ObaCivil
                        TsFicheEtatCivil = New Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
                        Try
                            TsFicheEtatCivil.ContenuTable = Ide_Dossier.ToString & VI.Tild & Contenu.Replace(VI.PointVirgule, VI.Tild)
                        Catch ex As Exception
                            TsFicheEtatCivil = Nothing
                        End Try
                    Case VI.ObjetPer.ObaConjoint
                        TsFicheConjoint = New Virtualia.TablesObjet.ShemaPER.PER_CONJOINT
                        Try
                            TsFicheConjoint.ContenuTable = Ide_Dossier.ToString & VI.Tild & Contenu.Replace(VI.PointVirgule, VI.Tild)
                        Catch ex As Exception
                            TsFicheConjoint = Nothing
                        End Try
                    Case VI.ObjetPer.ObaEnfant
                        TsFicheEnfant = New Virtualia.TablesObjet.ShemaPER.PER_ENFANT
                        Try
                            TsFicheEnfant.ContenuTable = Ide_Dossier.ToString & VI.Tild & Contenu.Replace(VI.PointVirgule, VI.Tild)
                        Catch ex As Exception
                            TsFicheEnfant = Nothing
                        End Try
                    Case VI.ObjetPer.ObaAdresse
                        TsFicheDomicile = New Virtualia.TablesObjet.ShemaPER.PER_DOMICILE
                        Try
                            TsFicheDomicile.ContenuTable = Ide_Dossier.ToString & VI.Tild & Contenu.Replace(VI.PointVirgule, VI.Tild)
                        Catch ex As Exception
                            TsFicheDomicile = Nothing
                        End Try
                    Case VI.ObjetPer.ObaBanque
                        TsFicheBanque = New Virtualia.TablesObjet.ShemaPER.PER_BANQUE
                        Try
                            TsFicheBanque.ContenuTable = Ide_Dossier.ToString & VI.Tild & Contenu.Replace(VI.PointVirgule, VI.Tild)
                        Catch ex As Exception
                            TsFicheBanque = Nothing
                        End Try
                    Case VI.ObjetPer.ObaDiplome
                        TsFicheDiplome = New Virtualia.TablesObjet.ShemaPER.PER_DIPLOME
                        Try
                            TsFicheDiplome.ContenuTable = Ide_Dossier.ToString & VI.Tild & Contenu.Replace(VI.PointVirgule, VI.Tild)
                        Catch ex As Exception
                            TsFicheDiplome = Nothing
                        End Try
                    Case VI.ObjetPer.ObaAbsence
                        TsFicheAbsence = New Virtualia.TablesObjet.ShemaPER.PER_ABSENCE
                        Try
                            TsFicheAbsence.ContenuTable = Ide_Dossier.ToString & VI.Tild & ObjetDemandeAbsence
                        Catch ex As Exception
                            TsFicheAbsence = Nothing
                        End Try
                    Case VI.ObjetPer.ObaAdrPro
                        TsFicheAdressePro = New Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO
                        Try
                            TsFicheAdressePro.ContenuTable = Ide_Dossier.ToString & VI.Tild & Contenu.Replace(VI.PointVirgule, VI.Tild)
                        Catch ex As Exception
                            TsFicheAdressePro = Nothing
                        End Try
                    Case VI.ObjetPer.ObaPrevenir
                        TsFichePersonneAPrevenir = New Virtualia.TablesObjet.ShemaPER.PER_APREVENIR
                        Try
                            TsFichePersonneAPrevenir.ContenuTable = Ide_Dossier.ToString & VI.Tild & Contenu.Replace(VI.PointVirgule, VI.Tild)
                        Catch ex As Exception
                            TsFichePersonneAPrevenir = Nothing
                        End Try
                    Case VI.ObjetPer.ObaDemandeFormation
                        TsFicheDemandeFormation = New Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION
                        Try
                            TsFicheDemandeFormation.ContenuTable = Ide_Dossier.ToString & VI.Tild & Contenu.Replace(VI.PointVirgule, VI.Tild)
                        Catch ex As Exception
                            TsFicheDemandeFormation = Nothing
                        End Try

                End Select
            End Set
        End Property

        Public ReadOnly Property PointeurAbsence() As Virtualia.TablesObjet.ShemaPER.PER_ABSENCE
            Get
                Return TsFicheAbsence
            End Get
        End Property
        Public ReadOnly Property PointeurEtatCivil() As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
            Get
                Return TsFicheEtatCivil
            End Get
        End Property
        Public ReadOnly Property PointeurConjoint() As Virtualia.TablesObjet.ShemaPER.PER_CONJOINT
            Get
                Return TsFicheConjoint
            End Get
        End Property
        Public ReadOnly Property PointeurEnfant() As Virtualia.TablesObjet.ShemaPER.PER_ENFANT
            Get
                Return TsFicheEnfant
            End Get
        End Property
        Public ReadOnly Property PointeurDomicile() As Virtualia.TablesObjet.ShemaPER.PER_DOMICILE
            Get
                Return TsFicheDomicile
            End Get
        End Property
        Public ReadOnly Property PointeurBanque() As Virtualia.TablesObjet.ShemaPER.PER_BANQUE
            Get
                Return TsFicheBanque
            End Get
        End Property
        Public ReadOnly Property PointeurDiplome() As Virtualia.TablesObjet.ShemaPER.PER_DIPLOME
            Get
                Return TsFicheDiplome
            End Get
        End Property
        Public ReadOnly Property PointeurAsdressePro() As Virtualia.TablesObjet.ShemaPER.PER_ADRESSEPRO
            Get
                Return TsFicheAdressePro
            End Get
        End Property
        Public ReadOnly Property PointeurPersonneAprevenir() As Virtualia.TablesObjet.ShemaPER.PER_APREVENIR
            Get
                Return TsFichePersonneAPrevenir
            End Get
        End Property
        Public ReadOnly Property PointeurDemandeFormation() As Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_FORMATION
            Get
                Return TsFicheDemandeFormation
            End Get
        End Property
        Private ReadOnly Property ObjetDemandeAbsence() As String
            Get
                Dim TableauLu(0) As String
                Dim TableauData(18) As String
                Dim DemiJour As String = ""
                Dim QuantDebut As Integer
                Dim QuantFin As Integer
                Dim NombreJours As Integer
                Dim Caracteristique As String
                Dim NbJours As String
                Dim CarMM As String

                Select Case Contenu
                    Case Is = ""
                        Return ""
                End Select

                TableauLu = Strings.Split(Contenu, VI.PointVirgule, -1)
                If TableauLu.Count > 10 Then 'Nouvelle formule V4
                    Return Contenu.Replace(VI.PointVirgule, VI.Tild)
                End If
                'Ancienne formule V3
                TableauData(0) = VirRhDate.DateStandardVirtualia(TableauLu(0))
                TableauData(9) = VirRhDate.DateStandardVirtualia(TableauLu(1))
                TableauData(1) = TableauLu(2)
                TableauData(18) = Date_Demande

                Caracteristique = DATABS_DEBUTINCLUS & " - " & DATABS_FININCLUS
                CarMM = "0"
                Select Case TableauData(0)
                    Case Is = TableauData(9)
                        Select Case TableauLu(3)
                            Case Is = "1"
                                Caracteristique = DATABS_DEBUTINCLUS & " - " & DATABS_FINMATIN
                                CarMM = "1"
                        End Select
                        Select Case TableauLu(4)
                            Case Is = "1"
                                Caracteristique = DATABS_DEBUTAPRESMIDI & " - " & DATABS_FININCLUS
                                CarMM = "2"
                        End Select
                    Case Else
                        Select Case TableauLu(3)
                            Case Is = "1"
                                Select Case TableauLu(4)
                                    Case Is = "1"
                                        Caracteristique = DATABS_DEBUTINCLUS & " - " & DATABS_FININCLUS
                                    Case Is = "0"
                                        Caracteristique = DATABS_DEBUTINCLUS & " - " & DATABS_FINMATIN
                                End Select
                            Case Is = "0"
                                Select Case TableauLu(4)
                                    Case Is = "1"
                                        Caracteristique = DATABS_DEBUTAPRESMIDI & " - " & DATABS_FININCLUS
                                    Case Is = 0
                                        Caracteristique = DATABS_DEBUTAPRESMIDI & " - " & DATABS_FINMATIN
                                End Select
                        End Select
                End Select
                TableauData(14) = Caracteristique

                QuantDebut = VirRhDate.CalcQuantieme(TableauData(0))
                QuantFin = VirRhDate.CalcQuantieme(TableauData(9))
                NombreJours = QuantFin - QuantDebut + 1
                Select Case Caracteristique
                    Case DATABS_DEBUTINCLUS & " - " & DATABS_FININCLUS
                        DemiJour = ""
                    Case DATABS_DEBUTAPRESMIDI & " - " & DATABS_FININCLUS, DATABS_DEBUTINCLUS & " - " & DATABS_FINMATIN
                        DemiJour = ",5"
                        NombreJours = NombreJours - 1
                    Case DATABS_DEBUTAPRESMIDI & " - " & DATABS_FINMATIN
                        DemiJour = ""
                        NombreJours = NombreJours - 1
                End Select
                TableauData(2) = NombreJours.ToString & DemiJour
                If WsObjetCalcul IsNot Nothing Then
                    If WsObjetCalcul.GetType.Name = "RegleCalculJours" Then
                        NbJours = Strings.Format(WsObjetCalcul.CalcJoursOuvres(TableauData(0), TableauData(9), "OUVRES", Caracteristique), "##0.0")
                        TableauData(3) = NbJours
                        If Strings.InStr(NbJours, ",0") > 0 Then
                            TableauData(3) = Strings.Left(NbJours, NbJours.Length - 2)
                        End If
                        NbJours = Strings.Format(WsObjetCalcul.CalcJoursOuvres(TableauData(0), TableauData(9), "OUVRABLES", Caracteristique), "##0.0")
                        TableauData(4) = NbJours
                        If InStr(NbJours, ",0") > 0 Then
                            TableauData(4) = Strings.Left(NbJours, NbJours.Length - 2)
                        End If
                    End If
                End If

                TableauData(5) = "Intranet - " & Signataire

                Return Strings.Join(TableauData, VI.Tild)
            End Get
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return ""
                    Case 1
                        Return WsRang.ToString
                    Case 2
                        Return WsDateDemande
                    Case 3
                        Return WsHeureDemande
                    Case 4
                        Return WsDateVisa
                    Case 5
                        Return WsHeureVisa
                    Case 6
                        Return WsNatureVisa.ToString
                    Case 7
                        Return WsSignataire
                    Case 8
                        Return WsDateGestion
                    Case 9
                        Return WsHeureGestion
                    Case 10
                        Return WsNatureGestion.ToString
                    Case 11
                        Return WsGestionnaire
                    Case 12
                        Return WsObservationDemandeur
                    Case 13
                        Return WsObservationResponsable
                    Case 14
                        Return WsObservationGestionnaire
                    Case 15
                        Return WsNumObjetPer.ToString
                    Case 16
                        Return WsContenu
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Sub New(Optional ByVal InstanceCalcul As Object = Nothing)
            MyBase.New()
            WsObjetCalcul = InstanceCalcul
        End Sub

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property
    End Class
End Namespace



