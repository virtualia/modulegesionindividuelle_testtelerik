﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_HORSFRANCE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNumero_et_nom_de_la_rue As String
        Private WsComplement_d_adresse As String
        Private WsVille As String
        Private WsNumero_de_telephone As String
        Private WsPays As String
        Private WsCode_pays As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_HORSFRANCE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaAdresseHorsFr
            End Get
        End Property

        Public Property Numero_et_nom_de_la_rue() As String
            Get
                Return WsNumero_et_nom_de_la_rue
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsNumero_et_nom_de_la_rue = value
                    Case Else
                        WsNumero_et_nom_de_la_rue = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property Complement_d_adresse() As String
            Get
                Return WsComplement_d_adresse
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsComplement_d_adresse = value
                    Case Else
                        WsComplement_d_adresse = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property Ville() As String
            Get
                Return WsVille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsVille = value
                    Case Else
                        WsVille = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Pays() As String
            Get
                Return WsPays
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsPays = value
                    Case Else
                        WsPays = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Property Numero_de_telephone() As String
            Get
                Return WsNumero_de_telephone
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsNumero_de_telephone = value
                    Case Else
                        WsNumero_de_telephone = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Code_Pays() As String
            Get
                Return WsCode_pays
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsCode_pays = value
                    Case Else
                        WsCode_pays = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Numero_et_nom_de_la_rue & VI.Tild)
                Chaine.Append(Complement_d_adresse & VI.Tild)
                Chaine.Append(Ville & VI.Tild)
                Chaine.Append(Pays & VI.Tild)
                Chaine.Append(Numero_de_telephone & VI.Tild)
                Chaine.Append(Code_Pays)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Numero_et_nom_de_la_rue = TableauData(1)
                Complement_d_adresse = TableauData(2)
                Ville = TableauData(3)
                Pays = TableauData(4)
                Numero_de_telephone = TableauData(5)
                Code_Pays = TableauData(6)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return ""
                    Case 1
                        Return WsNumero_et_nom_de_la_rue
                    Case 2
                        Return WsComplement_d_adresse
                    Case 3
                        Return WsVille
                    Case 4
                        Return WsNumero_de_telephone
                    Case 5
                        Return WsPays
                    Case 6
                        Return WsCode_pays
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<NumeroNomRue>" & VirRhFonction.ChaineXMLValide(Numero_et_nom_de_la_rue) & "</NumeroNomRue>" & vbCrLf)
                Chaine.Append("<ComplementAdresse>" & VirRhFonction.ChaineXMLValide(Complement_d_adresse) & "</ComplementAdresse>" & vbCrLf)
                Chaine.Append("<Ville>" & VirRhFonction.ChaineXMLValide(Ville) & "</Ville>" & vbCrLf)
                Chaine.Append("<Pays>" & VirRhFonction.ChaineXMLValide(Pays) & "</Pays>" & vbCrLf)
                Chaine.Append("<NumeroTelephonePerso>" & Numero_de_telephone & "</NumeroTelephonePerso>" & vbCrLf)
                Chaine.Append("<CodePays>" & Code_Pays & "</CodePays>" & vbCrLf)

                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

