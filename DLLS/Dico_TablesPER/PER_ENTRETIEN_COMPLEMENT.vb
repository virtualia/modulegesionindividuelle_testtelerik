﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_ENTRETIEN_COMPLEMENT
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsRang As Integer
        Private WsIntitule As String = ""
        Private WsNature As Integer
        Private WsValeur As String = ""

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENTRETIEN_COMPLEMENT"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaEntretienComplement
            End Get
        End Property

        Public Property Rang() As Integer
            Get
                Return WsRang
            End Get
            Set(ByVal value As Integer)
                WsRang = value
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Nature() As Integer
            Get
                Return WsNature
            End Get
            Set(ByVal value As Integer)
                WsNature = value
            End Set
        End Property

        Public Property Valeur() As String
            Get
                Return WsValeur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsValeur = value
                    Case Else
                        WsValeur = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder
                Chaine = New System.Text.StringBuilder
                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Rang & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Nature & VI.Tild)
                Chaine.Append(Valeur)
                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 5 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Rang = VirRhFonction.ConversionDouble(TableauData(2), 0)
                Intitule = TableauData(3)
                Nature = VirRhFonction.ConversionDouble(TableauData(4), 0)
                Valeur = TableauData(5)

                If WsFicheLue Is Nothing Then
                    WsFicheLue = New System.Text.StringBuilder
                    For IndiceI = 1 To TableauData.Count - 1
                        WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                    Next IndiceI
                End If
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

