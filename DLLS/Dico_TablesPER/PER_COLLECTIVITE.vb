Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_COLLECTIVITE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsAdministration As String
        Private WsRegion As String
        Private WsDepartement As String
        Private WsArrondissement As String
        Private WsCanton As String
        Private WsPopulation As String
        Private WsEffectif As String
        Private WsType As String
        Private WsCode_paie As String
        Private WsMode_de_recrutement As String
        Private WsMotif_de_depart As String
        Private WsDate_de_depart As String
        Private WsObservations As String
        '
        Private TsDate_de_fin As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_COLLECTIVITE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaSociete 'Spécifique Secteur Public
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 14
            End Get
        End Property

        Public Property Date_d_effet() As String
            Get
                Return MyBase.Date_de_Valeur
            End Get
            Set(ByVal value As String)
                MyBase.Date_de_Valeur = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Administration() As String
            Get
                Return WsAdministration
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsAdministration = value
                    Case Else
                        WsAdministration = Strings.Left(value, 120)
                End Select
                MyBase.Clef = WsAdministration
            End Set
        End Property

        Public Property Region() As String
            Get
                Return WsRegion
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsRegion = value
                    Case Else
                        WsRegion = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Departement() As String
            Get
                Return WsDepartement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsDepartement = value
                    Case Else
                        WsDepartement = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Arrondissement() As String
            Get
                Return WsArrondissement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsArrondissement = value
                    Case Else
                        WsArrondissement = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Canton() As String
            Get
                Return WsCanton
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsCanton = value
                    Case Else
                        WsCanton = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Population() As String
            Get
                Return WsPopulation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsPopulation = value
                    Case Else
                        WsPopulation = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Effectif() As String
            Get
                Return WsEffectif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsEffectif = value
                    Case Else
                        WsEffectif = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property VType() As String
            Get
                Return WsType
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsType = value
                    Case Else
                        WsType = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Code_paie() As String
            Get
                Return WsCode_paie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 6
                        WsCode_paie = value
                    Case Else
                        WsCode_paie = Strings.Left(value, 6)
                End Select
            End Set
        End Property

        Public Property Mode_de_recrutement() As String
            Get
                Return WsMode_de_recrutement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsMode_de_recrutement = value
                    Case Else
                        WsMode_de_recrutement = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property Motif_de_depart() As String
            Get
                Return WsMotif_de_depart
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsMotif_de_depart = value
                    Case Else
                        WsMotif_de_depart = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property Date_de_depart() As String
            Get
                Return WsDate_de_depart
            End Get
            Set(ByVal value As String)
                WsDate_de_depart = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Fin = WsDate_de_depart
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Administration & VI.Tild)
                Chaine.Append(Region & VI.Tild)
                Chaine.Append(Departement & VI.Tild)
                Chaine.Append(Arrondissement & VI.Tild)
                Chaine.Append(Canton & VI.Tild)
                Chaine.Append(Population & VI.Tild)
                Chaine.Append(Effectif & VI.Tild)
                Chaine.Append(VType & VI.Tild)
                Chaine.Append(Code_paie & VI.Tild)
                Chaine.Append(Mode_de_recrutement & VI.Tild)
                Chaine.Append(Motif_de_depart & VI.Tild)
                Chaine.Append(Date_de_depart & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 16 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_d_effet = TableauData(1)
                Administration = TableauData(2)
                Region = TableauData(3)
                Departement = TableauData(4)
                Arrondissement = TableauData(5)
                Canton = TableauData(6)
                Population = TableauData(7)
                Effectif = TableauData(8)
                VType = TableauData(9)
                Code_paie = TableauData(10)
                Mode_de_recrutement = TableauData(11)
                Motif_de_depart = TableauData(12)
                Date_de_depart = TableauData(13)
                Observations = TableauData(14)
                MyBase.Certification = TableauData(15)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = Date_de_depart
            End Set
        End Property

        Public Property VDate_de_Fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<DateEntreeEtablissement>" & MyBase.Date_de_Valeur & "</DateEntreeEtablissement>" & vbCrLf)
                Chaine.Append("<Etablissement>" & VirRhFonction.ChaineXMLValide(Administration) & "</Etablissement>" & vbCrLf)
                Chaine.Append("<CodePaieEtablissement>" & Code_paie & "</CodePaieEtablissement>" & vbCrLf)
                Chaine.Append("<ModeRecrutement>" & VirRhFonction.ChaineXMLValide(Mode_de_recrutement) & "</ModeRecrutement>" & vbCrLf)
                Chaine.Append("<DateSortieEtablissement>" & Date_de_depart & "</DateSortieEtablissement>" & vbCrLf)
                Chaine.Append("<MotifDepart>" & VirRhFonction.ChaineXMLValide(Motif_de_depart) & "</MotifDepart>" & vbCrLf)

                Return Chaine.ToString

            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification Then
                        Select Case IndiceI
                            Case TableauData.Count - 1
                                Chaine.Append(TableauData(IndiceI))
                            Case Else
                                If TableauData(IndiceI) <> "" Then
                                    Chaine.Append(TableauData(IndiceI) & VI.PointVirgule)
                                End If
                        End Select
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


