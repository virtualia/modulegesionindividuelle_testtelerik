﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_REGIME_AFFECTATION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNatureRegime As String
        Private WsLettreRegime As String
        Private WsFonction As String
        Private WsObservations As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_REGIME_AFFECTATION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 182
            End Get
        End Property

        Public Property NatureRegime() As String
            Get
                Return WsNatureRegime
            End Get
            Set(ByVal value As String)
                WsNatureRegime = F_FormatAlpha(value, 100)
                MyBase.Clef = WsNatureRegime
            End Set
        End Property

        Public Property LettreRegime() As String
            Get
                Return WsLettreRegime
            End Get
            Set(ByVal value As String)
                WsLettreRegime = F_FormatAlpha(value, 10)
            End Set
        End Property

        Public Property Fonction() As String
            Get
                Return WsFonction
            End Get
            Set(ByVal value As String)
                WsFonction = F_FormatAlpha(value, 120)
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                WsObservations = F_FormatAlpha(value, 250)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(NatureRegime & VI.Tild)
                Chaine.Append(LettreRegime & VI.Tild)
                Chaine.Append(Fonction & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 6 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                NatureRegime = TableauData(2)
                LettreRegime = TableauData(3)
                Fonction = TableauData(4)
                Observations = TableauData(5)
                MyBase.Date_de_Fin = TableauData(6)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
