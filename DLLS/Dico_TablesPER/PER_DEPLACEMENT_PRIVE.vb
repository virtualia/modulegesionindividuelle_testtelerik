﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_DEPLACEMENT_PRIVE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_de_Debut As String
        Private WsLibelleObjet As String
        Private WsNatureFrais As String
        Private WsMontant As Double = 0
        Private WsObservations As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_DEPLACEMENT"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaFrais 'Spécifique Secteur Privé
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 6
            End Get
        End Property

        Public Property Date_de_Debut() As String
            Get
                Return WsDate_de_Debut
            End Get
            Set(ByVal value As String)
                WsDate_de_Debut = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property LibelleObjet() As String
            Get
                Return WsLibelleObjet
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsLibelleObjet = value
                    Case Else
                        WsLibelleObjet = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsLibelleObjet
            End Set
        End Property

        Public Property NatureFrais() As String
            Get
                Return WsNatureFrais
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsNatureFrais = value
                    Case Else
                        WsNatureFrais = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Montant() As Double
            Get
                Return WsMontant
            End Get
            Set(ByVal value As Double)
                WsMontant = value
            End Set
        End Property

        Public Property Observation() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_de_Debut & VI.Tild)
                Chaine.Append(LibelleObjet & VI.Tild)
                Chaine.Append(NatureFrais & VI.Tild)
                Select Case Montant
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant.ToString) & VI.Tild)
                End Select
                Chaine.Append(Observation & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_de_Debut = TableauData(1)
                LibelleObjet = TableauData(2)
                NatureFrais = TableauData(3)
                If TableauData(4) = "" Then
                    TableauData(4) = "0"
                Else
                    TableauData(4) = VirRhFonction.MontantStocke(TableauData(4))
                End If
                Montant = CInt(TableauData(4))
                Observation = TableauData(5)
                MyBase.Date_de_Fin = TableauData(6)
                MyBase.Certification = TableauData(7)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
