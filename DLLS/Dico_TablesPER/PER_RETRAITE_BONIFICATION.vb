﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_RETRAITE_BONIFICATION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_Debut As String
        Private WsMotif As String
        Private WsPosition As String
        Private WsTerritoire As String
        Private WsSiOriginaireTerritoire As String
        Private WsTauxPrisEnCompte As Double
        Private WsDureeRetenue As String
        Private WsTypeTauxBonification As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_RETRAITE_BONIFICATION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 96
            End Get
        End Property

        Public Property Date_Debut() As String
            Get
                Return WsDate_Debut
            End Get
            Set(ByVal value As String)
                WsDate_Debut = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_Debut
            End Set
        End Property

        Public Property Motif() As String
            Get
                Return WsMotif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsMotif = value
                    Case Else
                        WsMotif = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsMotif
            End Set
        End Property

        Public Property Position() As String
            Get
                Return WsPosition
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsPosition = value
                    Case Else
                        WsPosition = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Territoire() As String
            Get
                Return WsTerritoire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsTerritoire = value
                    Case Else
                        WsTerritoire = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property SiOriginaireTerritoire() As String
            Get
                Return WsSiOriginaireTerritoire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiOriginaireTerritoire = value
                    Case Else
                        WsSiOriginaireTerritoire = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Taux_de_PriseEnCompte() As Double
            Get
                Return WsTauxPrisEnCompte
            End Get
            Set(ByVal value As Double)
                WsTauxPrisEnCompte = value
            End Set
        End Property

        Public Property DureeRetenue() As String
            Get
                Return WsDureeRetenue
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsDureeRetenue = value
                    Case Else
                        WsDureeRetenue = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Type_TauxBonification() As String
            Get
                Return WsTypeTauxBonification
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsTypeTauxBonification = value
                    Case Else
                        WsTypeTauxBonification = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_Debut & VI.Tild)
                Chaine.Append(Motif & VI.Tild)
                Chaine.Append(Position & VI.Tild)
                Chaine.Append(Territoire & VI.Tild)
                Chaine.Append(SiOriginaireTerritoire & VI.Tild)
                Chaine.Append(Taux_de_PriseEnCompte.ToString & VI.Tild)
                Chaine.Append(DureeRetenue & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Type_TauxBonification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 10 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_Debut = TableauData(1)
                Motif = TableauData(2)
                Position = TableauData(3)
                Territoire = TableauData(4)
                SiOriginaireTerritoire = TableauData(5)
                If TableauData(6) = "" Then TableauData(6) = "0"
                Taux_de_PriseEnCompte = VirRhFonction.ConversionDouble(TableauData(6))
                DureeRetenue = TableauData(7)
                MyBase.Date_de_Fin = TableauData(8)
                Type_TauxBonification = TableauData(9)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_Debut
                    Case 1
                        Return WsMotif
                    Case 2
                        Return WsPosition
                    Case 3
                        Return WsTerritoire
                    Case 4
                        Return WsSiOriginaireTerritoire
                    Case 5
                        Return WsTauxPrisEnCompte.ToString
                    Case 6
                        Return WsDureeRetenue
                    Case 7
                        Return Date_de_Fin
                    Case 8
                        Return WsTypeTauxBonification
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

