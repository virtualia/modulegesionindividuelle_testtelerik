Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_DOMICILE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNumero_et_nom_de_la_rue As String
        Private WsComplement_d_adresse As String
        Private WsLieu_dit As String
        Private WsCode_postal As String
        Private WsVille As String
        Private WsNumero_de_telephone As String
        Private WsPays As String
        Private WsCode_pays As String
        Private WsFax As String
        Private WsEmail As String
        Private WsNumportable As String
        Private WsTransport As String
        Private WsServicedistributeur As String
        Private WsSiAdresseNC As String
        Private WsSiTelephoneNC As String
        Private WsSiPortableNC As String
        Private WsSiEmailNC As String
        Private WsCompte_Twitter As String
        Private WsCompte_Facebook As String
        Private WsUrlBlog As String
        Private WsCompte_Viadeo As String
        Private WsCompte_Linkeldin As String
        Private WsTemps_Trajet As String
        '
        Private TsDADSU_Complement As String = ""
        Private TsDADSU_NoVoie As String = ""
        Private TsDADSU_Extension_Voie As String = ""
        Private TsDADSU_Nature_Voie As String = ""
        Private TsDADSU_Nom_Voie As String = ""
        Private TsDADSU_Commune As String = ""
        Private TsDADSU_CodePostal As String = ""
        Private TsDADSU_BureauDistributeur As String = ""
        Private TsDADSU_CodePays As String = ""

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_DOMICILE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaAdresse
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 24
            End Get
        End Property

        Public Property Numero_et_nom_de_la_rue() As String
            Get
                Return WsNumero_et_nom_de_la_rue
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsNumero_et_nom_de_la_rue = value
                    Case Else
                        WsNumero_et_nom_de_la_rue = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property Complement_d_adresse() As String
            Get
                Return WsComplement_d_adresse
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsComplement_d_adresse = value
                    Case Else
                        WsComplement_d_adresse = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property Lieu_dit() As String
            Get
                Return WsLieu_dit
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsLieu_dit = value
                    Case Else
                        WsLieu_dit = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Code_postal() As String
            Get
                Return WsCode_postal
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsCode_postal = value
                    Case Else
                        WsCode_postal = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Ville() As String
            Get
                Return WsVille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsVille = value
                    Case Else
                        WsVille = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Numero_de_telephone() As String
            Get
                Return WsNumero_de_telephone
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsNumero_de_telephone = value
                    Case Else
                        WsNumero_de_telephone = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Pays() As String
            Get
                Return WsPays
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsPays = value
                    Case Else
                        WsPays = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Property Code_Pays() As String
            Get
                Return WsCode_pays
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsCode_pays = value
                    Case Else
                        WsCode_pays = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Fax() As String
            Get
                Return WsFax
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsFax = value
                    Case Else
                        WsFax = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Email() As String
            Get
                Return WsEmail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 150
                        WsEmail = value
                    Case Else
                        WsEmail = Strings.Left(value, 150)
                End Select
            End Set
        End Property

        Public Property Numportable() As String
            Get
                Return WsNumportable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsNumportable = value
                    Case Else
                        WsNumportable = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Transport() As String
            Get
                Return WsTransport
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsTransport = value
                    Case Else
                        WsTransport = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Servicedistributeur() As String
            Get
                Return WsServicedistributeur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsServicedistributeur = value
                    Case Else
                        WsServicedistributeur = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property SiAdresseNC() As String
            Get
                Return WsSiAdresseNC
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "Oui"
                        WsSiAdresseNC = "Oui"
                    Case Else
                        WsSiAdresseNC = "Non"
                End Select
            End Set
        End Property

        Public Property SiTelephoneNC() As String
            Get
                Return WsSiTelephoneNC
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "Oui"
                        WsSiTelephoneNC = "Oui"
                    Case Else
                        WsSiTelephoneNC = "Non"
                End Select
            End Set
        End Property

        Public Property SiPortableNC() As String
            Get
                Return WsSiPortableNC
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "Oui"
                        WsSiPortableNC = "Oui"
                    Case Else
                        WsSiPortableNC = "Non"
                End Select
            End Set
        End Property

        Public Property SiEmailNC() As String
            Get
                Return WsSiEmailNC
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "Oui"
                        WsSiEmailNC = "Oui"
                    Case Else
                        WsSiEmailNC = "Non"
                End Select
            End Set
        End Property

        Public Property Compte_Twitter() As String
            Get
                Return WsCompte_Twitter
            End Get
            Set(ByVal value As String)
                WsCompte_Twitter = F_FormatAlpha(value, 150)
            End Set
        End Property

        Public Property Compte_Facebook() As String
            Get
                Return WsCompte_Facebook
            End Get
            Set(ByVal value As String)
                WsCompte_Facebook = F_FormatAlpha(value, 150)
            End Set
        End Property

        Public Property URL_Blog() As String
            Get
                Return WsUrlBlog
            End Get
            Set(ByVal value As String)
                WsUrlBlog = F_FormatAlpha(value, 250)
            End Set
        End Property

        Public Property Compte_Viadeo() As String
            Get
                Return WsCompte_Viadeo
            End Get
            Set(ByVal value As String)
                WsCompte_Viadeo = F_FormatAlpha(value, 250)
            End Set
        End Property

        Public Property Compte_Linkeldin() As String
            Get
                Return WsCompte_Linkeldin
            End Get
            Set(ByVal value As String)
                WsCompte_Linkeldin = F_FormatAlpha(value, 250)
            End Set
        End Property

        Public Property Temps_Trajet() As String
            Get
                Return WsTemps_Trajet
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsTemps_Trajet = value
                    Case Else
                        WsTemps_Trajet = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Numero_et_nom_de_la_rue & VI.Tild)
                Chaine.Append(Complement_d_adresse & VI.Tild)
                Chaine.Append(Lieu_dit & VI.Tild)
                Chaine.Append(Code_postal & VI.Tild)
                Chaine.Append(Ville & VI.Tild)
                Chaine.Append(Numero_de_telephone & VI.Tild)
                Chaine.Append(Pays & VI.Tild)
                Chaine.Append(Code_Pays & VI.Tild)
                Chaine.Append(Fax & VI.Tild)
                Chaine.Append(Email & VI.Tild)
                Chaine.Append(Numportable & VI.Tild)
                Chaine.Append(Transport & VI.Tild)
                Chaine.Append(Servicedistributeur & VI.Tild)
                Chaine.Append(SiAdresseNC & VI.Tild)
                Chaine.Append(SiTelephoneNC & VI.Tild)
                Chaine.Append(SiPortableNC & VI.Tild)
                Chaine.Append(SiEmailNC & VI.Tild)
                Chaine.Append(Compte_Twitter & VI.Tild)
                Chaine.Append(Compte_Facebook & VI.Tild)
                Chaine.Append(URL_Blog & VI.Tild)
                Chaine.Append(Compte_Viadeo & VI.Tild)
                Chaine.Append(Compte_Linkeldin & VI.Tild)
                Chaine.Append(Temps_Trajet & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 25 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Numero_et_nom_de_la_rue = TableauData(1)
                Complement_d_adresse = TableauData(2)
                Lieu_dit = TableauData(3)
                Code_postal = TableauData(4)
                Ville = TableauData(5)
                Numero_de_telephone = TableauData(6)
                Pays = TableauData(7)
                Code_Pays = TableauData(8)
                Fax = TableauData(9)
                Email = TableauData(10)
                Numportable = TableauData(11)
                Transport = TableauData(12)
                Servicedistributeur = TableauData(13)
                SiAdresseNC = TableauData(14)
                SiTelephoneNC = TableauData(15)
                SiPortableNC = TableauData(16)
                SiEmailNC = TableauData(17)
                Compte_Twitter = TableauData(18)
                Compte_Facebook = TableauData(19)
                URL_Blog = TableauData(20)
                Compte_Viadeo = TableauData(21)
                Compte_Linkeldin = TableauData(22)
                Temps_Trajet = TableauData(23)
                MyBase.Certification = TableauData(24)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property VAdresseDomicile(ByVal Methode As Integer) As String
            Get
                Dim Chaine As System.Text.StringBuilder
                Dim Saut As String

                Select Case Methode
                    Case VI.OptionInfo.DicoRecherche
                        Saut = Strings.Chr(13) & Strings.Chr(10)
                    Case Else
                        Saut = Strings.Space(1)
                End Select
                Chaine = New System.Text.StringBuilder
                Chaine.Append(Numero_et_nom_de_la_rue & Strings.Space(1) & Complement_d_adresse & Saut)
                Select Case Ville
                    Case ""
                        Chaine.Append(Code_postal & Strings.Space(1) & Lieu_dit)
                    Case Else
                        Chaine.Append(Lieu_dit & Saut)
                        Chaine.Append(Code_postal & Strings.Space(1) & Ville)
                End Select
                Select Case Pays
                    Case Is <> ""
                        Chaine.Append(" (" & Pays & ")")
                End Select
                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<NumeroNomRue>" & VirRhFonction.ChaineXMLValide(Numero_et_nom_de_la_rue) & "</NumeroNomRue>" & vbCrLf)
                Chaine.Append("<ComplementAdresse>" & VirRhFonction.ChaineXMLValide(Complement_d_adresse) & "</ComplementAdresse>" & vbCrLf)
                Chaine.Append("<LieuDit>" & VirRhFonction.ChaineXMLValide(Lieu_dit) & "</LieuDit>" & vbCrLf)
                Chaine.Append("<CodePostal>" & Code_postal & "</CodePostal>" & vbCrLf)
                Chaine.Append("<Ville>" & VirRhFonction.ChaineXMLValide(Ville) & "</Ville>" & vbCrLf)
                Chaine.Append("<Pays>" & VirRhFonction.ChaineXMLValide(Pays) & "</Pays>" & vbCrLf)
                Chaine.Append("<NumeroTelephonePerso>" & Numero_de_telephone & "</NumeroTelephonePerso>" & vbCrLf)
                Chaine.Append("<NumeroPortablePerso>" & Numportable & "</NumeroPortablePerso>" & vbCrLf)
                Chaine.Append("<EmailPerso>" & Email & "</EmailPerso>" & vbCrLf)
                Chaine.Append("<FaxPerso>" & Fax & "</FaxPerso>" & vbCrLf)
                Chaine.Append("<Transport>" & VirRhFonction.ChaineXMLValide(Transport) & "</Transport>" & vbCrLf)

                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property DADSU_Complement_Adresse As String
            Get
                Return VirRhFonction.ChaineSansAccent(TsDADSU_Complement, True)
            End Get
        End Property

        Public ReadOnly Property DADSU_Numero_Voie As String
            Get
                Return TsDADSU_NoVoie.ToUpper
            End Get
        End Property

        Public ReadOnly Property DADSU_Extension_Voie As String
            Get
                Return TsDADSU_Extension_Voie.ToUpper
            End Get
        End Property

        Public ReadOnly Property DADSU_Nature_Voie As String
            Get
                Return TsDADSU_Nature_Voie.ToUpper
            End Get
        End Property

        Public ReadOnly Property DADSU_Libelle_Voie As String
            Get
                Return VirRhFonction.ChaineSansAccent(TsDADSU_Nom_Voie, True)
            End Get
        End Property

        Public ReadOnly Property DADSU_Commune As String
            Get
                Return VirRhFonction.ChaineSansAccent(TsDADSU_Commune, True)
            End Get
        End Property

        Public ReadOnly Property DADSU_CodePostal As String
            Get
                Return TsDADSU_CodePostal.ToUpper
            End Get
        End Property

        Public ReadOnly Property DADSU_Bureau_Distributeur As String
            Get
                Return VirRhFonction.ChaineSansAccent(TsDADSU_BureauDistributeur, True)
            End Get
        End Property

        Public Sub FaireAdresseNormalisee()
            Dim Chaine As String
            Dim ChaineW As String
            Dim TableauTmp(0) As String
            Dim IndiceI As Integer
            Dim ListeCodeBTQ As New List(Of String)
            Dim ListeValeurBTQ As New List(Of String)
            Dim IndiceK As Integer

            ListeCodeBTQ.Add("B")
            ListeCodeBTQ.Add("T")
            ListeCodeBTQ.Add("Q")
            ListeValeurBTQ.Add("Bis")
            ListeValeurBTQ.Add("Ter")
            ListeValeurBTQ.Add("Quater")

            Chaine = Numero_et_nom_de_la_rue.Replace(",", Strings.Space(1))
            Chaine = Chaine.Replace(Strings.Space(2), Strings.Space(1))
            TableauTmp = Strings.Split(Chaine, Strings.Space(1), -1)

            TsDADSU_NoVoie = ""
            TsDADSU_Extension_Voie = ""
            TsDADSU_Nature_Voie = ""
            TsDADSU_Nom_Voie = ""
            If IsNumeric(TableauTmp(0).Trim) Then
                TsDADSU_NoVoie = TableauTmp(0).Trim
                IndiceK = 1
            Else
                IndiceK = 0
            End If
            If TableauTmp.Count > IndiceK Then
                ChaineW = TableauTmp(IndiceK).Trim.ToUpper
                For IndiceI = 0 To ListeValeurBTQ.Count - 1
                    If ListeValeurBTQ.Item(IndiceI).ToUpper = ChaineW Then
                        TsDADSU_Extension_Voie = ListeCodeBTQ.Item(IndiceI)
                        IndiceK += 1
                        Exit For
                    End If
                Next IndiceI
            End If
            If TableauTmp.Count > IndiceK Then
                TsDADSU_Nature_Voie = VirRhAnnexe12.Code_Voie(TableauTmp(IndiceK))
                For IndiceI = IndiceK + 1 To TableauTmp.Count - 1
                    TsDADSU_Nom_Voie &= TableauTmp(IndiceI) & Strings.Space(1)
                Next IndiceI
            End If
            TsDADSU_Commune = Lieu_dit
            TsDADSU_CodePostal = Code_postal
            TsDADSU_BureauDistributeur = Ville
            If TsDADSU_Commune.ToUpper = TsDADSU_BureauDistributeur.ToUpper Then
                TsDADSU_Commune = ""
            End If

            TsDADSU_Complement = Complement_d_adresse
            TsDADSU_CodePays = "FR"

        End Sub

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Call FaireAdresseNormalisee()
                Dim Chaine As String
                Chaine = DADSU_Numero_Voie & VI.PointVirgule & DADSU_Extension_Voie & VI.PointVirgule & DADSU_Nature_Voie & VI.PointVirgule
                Chaine &= DADSU_Libelle_Voie & VI.PointVirgule & DADSU_Complement_Adresse & VI.PointVirgule & DADSU_Bureau_Distributeur & VI.PointVirgule
                Chaine &= DADSU_CodePostal & VI.PointVirgule & DADSU_Commune & VI.PointVirgule
                Chaine &= Numero_de_telephone & VI.PointVirgule & Email
                Return Chaine
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

