﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_ENTRETIEN_HONDA_GENERAL
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsKI As String
        Private WsTypeFormulaire As String
        Private WsDate_Entretien As String
        Private WsDate_Signature_Manager_KI1 As String
        Private WsDate_Signature_Manager_KI2 As String
        Private WsDate_Signature_Manager_KI3 As String
        Private WsDate_Signature_Salarie_KI1 As String
        Private WsDate_Signature_Salarie_KI2 As String
        Private WsDate_Signature_Salarie_KI3 As String
        Private WsSiRefus_Signature As String
        Private WsRole_Dans_Organisation As String
        Private WsCible_Dans_Organisation As String
        Private WsRole_Individuel As String
        Private WsExigence_Comportement As String
        Private WsSiBesoin_Formation As String
        Private WsIdeManager As Integer
        Private WsNote_Performance_Self As Double
        Private WsNote_Performance_Manager As Double
        Private WsDifficultes_Performance As String
        Private WsNote_Comportement_Self As Double
        Private WsNote_Comportement_Manager As Double
        Private WsObservations_Comportement As String
        Private WsNote_Globale_Self As Double
        Private WsNote_Globale_Manager As Double
        Private WsNote_Lettre_Manager As String
        Private WsNote_Lettre_Revisee As String
        Private WsResume_Developpement As String
        Private WsCommentaires_Self As String
        Private WsCommentaires_Manager As String
        Private WsChargesTravail_Self As String
        Private WsChargesTravail_Manager As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENTRETIEN_GENERAL"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 150
            End Get
        End Property

        Public Property KI() As String
            Get
                Return WsKI
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsKI = value
                    Case Else
                        WsKI = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Type_Formulaire() As String
            Get
                Return WsTypeFormulaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsTypeFormulaire = value
                    Case Else
                        WsTypeFormulaire = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Date_Entretien() As String
            Get
                Return WsDate_Entretien
            End Get
            Set(ByVal value As String)
                WsDate_Entretien = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_Signature_Manager_KI1() As String
            Get
                Return WsDate_Signature_Manager_KI1
            End Get
            Set(ByVal value As String)
                WsDate_Signature_Manager_KI1 = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_Signature_Manager_KI2() As String
            Get
                Return WsDate_Signature_Manager_KI2
            End Get
            Set(ByVal value As String)
                WsDate_Signature_Manager_KI2 = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_Signature_Manager_KI3() As String
            Get
                Return WsDate_Signature_Manager_KI3
            End Get
            Set(ByVal value As String)
                WsDate_Signature_Manager_KI3 = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_Signature_Evalue_KI1() As String
            Get
                Return WsDate_Signature_Salarie_KI1
            End Get
            Set(ByVal value As String)
                WsDate_Signature_Salarie_KI1 = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_Signature_Evalue_KI2() As String
            Get
                Return WsDate_Signature_Salarie_KI2
            End Get
            Set(ByVal value As String)
                WsDate_Signature_Salarie_KI2 = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_Signature_Evalue_KI3() As String
            Get
                Return WsDate_Signature_Salarie_KI3
            End Get
            Set(ByVal value As String)
                WsDate_Signature_Salarie_KI3 = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property SiRefus_Signature() As String
            Get
                Return WsSiRefus_Signature
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case "", "Non"
                        WsSiRefus_Signature = "Non"
                    Case Else
                        WsSiRefus_Signature = "Oui"
                End Select
            End Set
        End Property

        Public Property Role_Dans_Organisation() As String
            Get
                Return WsRole_Dans_Organisation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsRole_Dans_Organisation = value
                    Case Else
                        WsRole_Dans_Organisation = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property Cible_Dans_Organisation() As String
            Get
                Return WsCible_Dans_Organisation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsCible_Dans_Organisation = value
                    Case Else
                        WsCible_Dans_Organisation = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property Role_Individuel() As String
            Get
                Return WsRole_Individuel
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsRole_Individuel = value
                    Case Else
                        WsRole_Individuel = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property Exigence_Comportement() As String
            Get
                Return WsExigence_Comportement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsExigence_Comportement = value
                    Case Else
                        WsExigence_Comportement = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property SiBesoin_Formation() As String
            Get
                Return WsSiBesoin_Formation
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case "", "Non"
                        WsSiBesoin_Formation = "Non"
                    Case Else
                        WsSiBesoin_Formation = "Oui"
                End Select
            End Set
        End Property

        Public Property Identifiant_Manager As Integer
            Get
                Return WsIdeManager
            End Get
            Set(value As Integer)
                WsIdeManager = value
            End Set
        End Property

        Public Property Note_Performance_Self() As Double
            Get
                Return WsNote_Performance_Self
            End Get
            Set(ByVal value As Double)
                WsNote_Performance_Self = value
            End Set
        End Property

        Public Property Note_Performance_Manager() As Double
            Get
                Return WsNote_Performance_Manager
            End Get
            Set(ByVal value As Double)
                WsNote_Performance_Manager = value
            End Set
        End Property

        Public Property Difficultes_Performance() As String
            Get
                Return WsDifficultes_Performance
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsDifficultes_Performance = value
                    Case Else
                        WsDifficultes_Performance = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property Note_Comportement_Self() As Double
            Get
                Return WsNote_Comportement_Self
            End Get
            Set(ByVal value As Double)
                WsNote_Comportement_Self = value
            End Set
        End Property

        Public Property Note_Comportement_Manager() As Double
            Get
                Return WsNote_Comportement_Manager
            End Get
            Set(ByVal value As Double)
                WsNote_Comportement_Manager = value
            End Set
        End Property

        Public Property Observations_Comportement() As String
            Get
                Return WsObservations_Comportement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsObservations_Comportement = value
                    Case Else
                        WsObservations_Comportement = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property Note_Globale_Self() As Double
            Get
                Return WsNote_Globale_Self
            End Get
            Set(ByVal value As Double)
                WsNote_Globale_Self = value
            End Set
        End Property

        Public Property Note_Globale_Manager() As Double
            Get
                Return WsNote_Globale_Manager
            End Get
            Set(ByVal value As Double)
                WsNote_Globale_Manager = value
            End Set
        End Property

        Public Property Note_Lettre_Manager() As String
            Get
                Return WsNote_Lettre_Manager
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsNote_Lettre_Manager = value
                    Case Else
                        WsNote_Lettre_Manager = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Note_Lettre_Revisee() As String
            Get
                Return WsNote_Lettre_Revisee
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsNote_Lettre_Revisee = value
                    Case Else
                        WsNote_Lettre_Revisee = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Resume_Developpement() As String
            Get
                Return WsResume_Developpement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsResume_Developpement = value
                    Case Else
                        WsResume_Developpement = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property Commentaires_Self() As String
            Get
                Return WsCommentaires_Self
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsCommentaires_Self = value
                    Case Else
                        WsCommentaires_Self = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property Commentaires_Manager() As String
            Get
                Return WsCommentaires_Manager
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsCommentaires_Manager = value
                    Case Else
                        WsCommentaires_Manager = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property ChargesTravail_Self() As String
            Get
                Return WsChargesTravail_Self
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsChargesTravail_Self = value
                    Case Else
                        WsChargesTravail_Self = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property ChargesTravail_Manager() As String
            Get
                Return WsChargesTravail_Manager
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsChargesTravail_Manager = value
                    Case Else
                        WsChargesTravail_Manager = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(KI & VI.Tild)
                Chaine.Append(Type_Formulaire & VI.Tild)
                Chaine.Append(Date_Entretien & VI.Tild)
                Chaine.Append(Date_de_Signature_Manager_KI1 & VI.Tild)
                Chaine.Append(Date_de_Signature_Manager_KI2 & VI.Tild)
                Chaine.Append(Date_de_Signature_Manager_KI3 & VI.Tild)
                Chaine.Append(Date_de_Signature_Evalue_KI1 & VI.Tild)
                Chaine.Append(Date_de_Signature_Evalue_KI2 & VI.Tild)
                Chaine.Append(Date_de_Signature_Evalue_KI3 & VI.Tild)
                Chaine.Append(SiRefus_Signature & VI.Tild)
                Chaine.Append(Role_Dans_Organisation & VI.Tild)
                Chaine.Append(Cible_Dans_Organisation & VI.Tild)
                Chaine.Append(Role_Individuel & VI.Tild)
                Chaine.Append(Exigence_Comportement & VI.Tild)
                Chaine.Append(SiBesoin_Formation & VI.Tild)
                Chaine.Append(Identifiant_Manager.ToString & VI.Tild)
                Chaine.Append(Note_Performance_Self.ToString & VI.Tild)
                Chaine.Append(Note_Performance_Manager.ToString & VI.Tild)
                Chaine.Append(Difficultes_Performance & VI.Tild)
                Chaine.Append(Note_Comportement_Self.ToString & VI.Tild)
                Chaine.Append(Note_Comportement_Manager.ToString & VI.Tild)
                Chaine.Append(Observations_Comportement & VI.Tild)
                Chaine.Append(Note_Globale_Self.ToString & VI.Tild)
                Chaine.Append(Note_Globale_Manager.ToString & VI.Tild)
                Chaine.Append(Note_Lettre_Manager & VI.Tild)
                Chaine.Append(Note_Lettre_Revisee & VI.Tild)
                Chaine.Append(Resume_Developpement & VI.Tild)
                Chaine.Append(Commentaires_Self & VI.Tild)
                Chaine.Append(Commentaires_Manager & VI.Tild)
                Chaine.Append(ChargesTravail_Self & VI.Tild)
                Chaine.Append(ChargesTravail_Manager)
                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 33 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                KI = TableauData(2)
                Type_Formulaire = TableauData(3)
                Date_Entretien = TableauData(4)
                Date_de_Signature_Manager_KI1 = TableauData(5)
                Date_de_Signature_Manager_KI2 = TableauData(6)
                Date_de_Signature_Manager_KI3 = TableauData(7)
                Date_de_Signature_Evalue_KI1 = TableauData(8)
                Date_de_Signature_Evalue_KI2 = TableauData(9)
                Date_de_Signature_Evalue_KI3 = TableauData(10)
                SiRefus_Signature = TableauData(11)
                Role_Dans_Organisation = TableauData(12)
                Cible_Dans_Organisation = TableauData(13)
                Role_Individuel = TableauData(14)
                Exigence_Comportement = TableauData(15)
                SiBesoin_Formation = TableauData(16)
                If TableauData(17) <> "" Then Identifiant_Manager = CInt(VirRhFonction.ConversionDouble(TableauData(17)))
                Note_Performance_Self = VirRhFonction.ConversionDouble(TableauData(18))
                Note_Performance_Manager = VirRhFonction.ConversionDouble(TableauData(19))
                Difficultes_Performance = TableauData(20)
                Note_Comportement_Self = VirRhFonction.ConversionDouble(TableauData(21))
                Note_Performance_Manager = VirRhFonction.ConversionDouble(TableauData(22))
                Observations_Comportement = TableauData(23)
                Note_Globale_Self = VirRhFonction.ConversionDouble(TableauData(24))
                Note_Globale_Manager = VirRhFonction.ConversionDouble(TableauData(25))
                Note_Lettre_Manager = TableauData(26)
                Note_Lettre_Revisee = TableauData(27)
                Resume_Developpement = TableauData(28)
                Commentaires_Self = TableauData(29)
                Commentaires_Manager = TableauData(30)
                ChargesTravail_Self = TableauData(31)
                ChargesTravail_Manager = TableauData(32)

                If WsFicheLue Is Nothing Then
                    WsFicheLue = New System.Text.StringBuilder
                    For IndiceI = 1 To TableauData.Count - 1
                        WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                    Next IndiceI
                End If
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
