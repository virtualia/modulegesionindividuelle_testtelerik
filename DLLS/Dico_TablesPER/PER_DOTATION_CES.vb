﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_DOTATION_CES
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_Valeur As String
        Private WsCreditDeplacement As Double = 0
        Private WsCreditWagonLit As Double = 0
        Private WsCreditComplementaire As Double = 0
        Private WsCreditTotal As Double = 0
        Private WsObservations As String
        Private WsAbonnement(3) As String
        Private WsDateValiditeAbonnement(3) As String
        Private WsCoutAbonnement(3) As Double
        Private WsCoutTotalAbonnement As Double = 0
        Private WsTotalDotation As Double = 0
        Private WsDate_Fin As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_DOTATION_CES"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 185 'Spécifique Conseil Economique et Social
            End Get
        End Property

        Public Property Credit_Deplacements() As Double
            Get
                Return WsCreditDeplacement
            End Get
            Set(ByVal value As Double)
                WsCreditDeplacement = value
            End Set
        End Property

        Public Property Credit_Wagon_Lit() As Double
            Get
                Return WsCreditWagonLit
            End Get
            Set(ByVal value As Double)
                WsCreditWagonLit = value
            End Set
        End Property

        Public Property Credit_Complementaire() As Double
            Get
                Return WsCreditComplementaire
            End Get
            Set(ByVal value As Double)
                WsCreditComplementaire = value
            End Set
        End Property

        Public Property Credit_Total() As Double
            Get
                Return WsCreditTotal
            End Get
            Set(ByVal value As Double)
                WsCreditTotal = value
            End Set
        End Property

        Public Property Obsevations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property Nature_Abonnement(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To 3
                        Return WsAbonnement(Index)
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 3
                        WsAbonnement(Index) = value
                End Select
            End Set
        End Property

        Public Property DateValidite_Abonnement(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To 3
                        Return WsDateValiditeAbonnement(Index)
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 3
                        WsDateValiditeAbonnement(Index) = VirRhDate.DateStandardVirtualia(value)
                End Select
            End Set
        End Property

        Public Property Cout_Abonnement(ByVal Index As Integer) As Double
            Get
                Select Case Index
                    Case 0 To 3
                        Return WsCoutAbonnement(Index)
                End Select
                Return 0
            End Get
            Set(ByVal value As Double)
                Select Case Index
                    Case 0 To 3
                        WsCoutAbonnement(Index) = value
                End Select
            End Set
        End Property

        Public Property Total_Abonnements() As Double
            Get
                Return WsCoutTotalAbonnement
            End Get
            Set(ByVal value As Double)
                WsCoutTotalAbonnement = value
            End Set
        End Property

        Public Property Total_Dotation() As Double
            Get
                Return WsTotalDotation
            End Get
            Set(ByVal value As Double)
                WsTotalDotation = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder
                Dim I As Integer

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(VirRhFonction.MontantStocke(Credit_Deplacements.ToString) & VI.Tild)
                Chaine.Append(VirRhFonction.MontantStocke(Credit_Wagon_Lit.ToString) & VI.Tild)
                Chaine.Append(VirRhFonction.MontantStocke(Credit_Complementaire.ToString) & VI.Tild)
                Chaine.Append(VirRhFonction.MontantStocke(Credit_Total.ToString) & VI.Tild)
                Chaine.Append(Obsevations & VI.Tild)
                For I = 0 To 3
                    Chaine.Append(Nature_Abonnement(I) & VI.Tild)
                    Chaine.Append(DateValidite_Abonnement(I) & VI.Tild)
                    Chaine.Append(VirRhFonction.MontantStocke(Cout_Abonnement(I).ToString) & VI.Tild)
                Next I
                Chaine.Append(VirRhFonction.MontantStocke(Total_Abonnements.ToString) & VI.Tild)
                Chaine.Append(VirRhFonction.MontantStocke(Total_Dotation.ToString) & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer
                Dim IndiceK As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 22 Then
                    Exit Property
                End If
                For IndiceI = 0 To TableauData.Count - 1
                    Select Case IndiceI
                        Case 0, 2, 3, 4, 9, 12, 15, 18, 19, 20
                            If TableauData(IndiceI) = "" Then
                                TableauData(IndiceI) = "0"
                            End If
                    End Select
                Next IndiceI

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Credit_Deplacements = VirRhFonction.ConversionDouble(VirRhFonction.MontantStocke(TableauData(2)))
                Credit_Wagon_Lit = VirRhFonction.ConversionDouble(VirRhFonction.MontantStocke(TableauData(3)))
                Credit_Complementaire = VirRhFonction.ConversionDouble(VirRhFonction.MontantStocke(TableauData(4)))
                Credit_Total = VirRhFonction.ConversionDouble(VirRhFonction.MontantStocke(TableauData(5)))
                Obsevations = TableauData(6)
                IndiceK = 0
                For IndiceI = 7 To 16 Step 3
                    Nature_Abonnement(IndiceK) = TableauData(IndiceI)
                    DateValidite_Abonnement(IndiceK) = TableauData(IndiceI + 1)
                    Cout_Abonnement(IndiceK) = VirRhFonction.ConversionDouble(VirRhFonction.MontantStocke(TableauData(IndiceI + 2)))
                    IndiceK += 1
                Next IndiceI
                Total_Abonnements = VirRhFonction.ConversionDouble(VirRhFonction.MontantStocke(TableauData(19)))
                Total_Dotation = VirRhFonction.ConversionDouble(VirRhFonction.MontantStocke(TableauData(20)))
                MyBase.Date_de_Fin = TableauData(21)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

