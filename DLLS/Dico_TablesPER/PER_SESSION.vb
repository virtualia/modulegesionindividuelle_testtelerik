Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_SESSION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_du_stage As String
        Private WsOrganisme As String
        Private WsDomaine As String
        Private WsTheme As String
        Private WsIntitule As String
        Private WsDate_de_l_inscription As String
        Private WsSuite_donnee As String
        Private WsQualification As String
        Private WsDuree As Double = 0
        Private WsAction_de_formation As String
        Private WsDuree_en_heures As Double = 0
        Private WsCout_de_l_inscription As Double = 0
        Private WsFrais_de_deplacement As Double = 0
        Private WsFrais_d_hebergement As Double = 0
        Private WsHeures_de_deplacement As Integer = 0
        Private WsFrais_de_restauration As Double = 0
        Private WsReference_de_la_session As String
        Private WsMotif_de_non_participation As String
        Private WsCursus As String
        Private WsVModule As String
        Private WsOrdre_de_priorite_a_l_inscript As String
        Private WsDemande_d_hebergement As String
        Private WsSuivi_de_la_presence_au_stage As String
        Private WsIntitule_de_la_session As String
        Private WsReference_du_stage As String
        Private WsContexte_du_stage As String
        Private WsType_de_stage As String
        Private WsClassement_du_stage As String
        Private WsPlan_de_formation As String
        Private WsCout_annexe_de_l_inscription As Double = 0
        Private WsContexte As String
        Private WsFiliere As String
        Private WsSidif As String
        Private WsSihorstempstravail As String
        Private WsRubrique_formation As String 'Sous-Th�me
        Private WsNbheures_DIF As Double = 0
        Private WsRangSession As Integer = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_SESSION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaFormation
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 38
            End Get
        End Property

        Public Property Date_du_stage() As String
            Get
                Return WsDate_du_stage
            End Get
            Set(ByVal value As String)
                WsDate_du_stage = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = value
            End Set
        End Property

        Public Property Organisme() As String
            Get
                Return WsOrganisme
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsOrganisme = value
                    Case Else
                        WsOrganisme = Strings.Left(value, 100)
                End Select
                MyBase.Clef = WsOrganisme
            End Set
        End Property

        Public Property Domaine() As String
            Get
                Return WsDomaine
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsDomaine = value
                    Case Else
                        WsDomaine = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Theme() As String
            Get
                Return WsTheme
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsTheme = value
                    Case Else
                        WsTheme = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Date_de_l_inscription() As String
            Get
                Return WsDate_de_l_inscription
            End Get
            Set(ByVal value As String)
                WsDate_de_l_inscription = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Suite_donnee() As String
            Get
                Return WsSuite_donnee
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsSuite_donnee = value
                    Case Else
                        WsSuite_donnee = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Qualification() As String
            Get
                Return WsQualification
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsQualification = value
                    Case Else
                        WsQualification = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Duree() As Double
            Get
                Return WsDuree
            End Get
            Set(ByVal value As Double)
                WsDuree = value
            End Set
        End Property

        Public Property Action_de_formation() As String
            Get
                Return WsAction_de_formation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsAction_de_formation = value
                    Case Else
                        WsAction_de_formation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Duree_en_heures() As Double
            Get
                Return WsDuree_en_heures
            End Get
            Set(ByVal value As Double)
                WsDuree_en_heures = value
            End Set
        End Property

        Public Property Cout_de_l_inscription() As Double
            Get
                Return WsCout_de_l_inscription
            End Get
            Set(ByVal value As Double)
                WsCout_de_l_inscription = value
            End Set
        End Property

        Public Property Frais_de_deplacement() As Double
            Get
                Return WsFrais_de_deplacement
            End Get
            Set(ByVal value As Double)
                WsFrais_de_deplacement = value
            End Set
        End Property

        Public Property Frais_d_hebergement() As Double
            Get
                Return WsFrais_d_hebergement
            End Get
            Set(ByVal value As Double)
                WsFrais_d_hebergement = value
            End Set
        End Property

        Public Property Heures_de_deplacement() As Integer
            Get
                Return WsHeures_de_deplacement
            End Get
            Set(ByVal value As Integer)
                WsHeures_de_deplacement = value
            End Set
        End Property

        Public Property Frais_de_restauration() As Double
            Get
                Return WsFrais_de_restauration
            End Get
            Set(ByVal value As Double)
                WsFrais_de_restauration = value
            End Set
        End Property

        Public Property Reference_de_la_session() As String
            Get
                Return WsReference_de_la_session
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsReference_de_la_session = value
                    Case Else
                        WsReference_de_la_session = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Motif_de_non_participation() As String
            Get
                Return WsMotif_de_non_participation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsMotif_de_non_participation = value
                    Case Else
                        WsMotif_de_non_participation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Cursus() As String
            Get
                Return WsCursus
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsCursus = value
                    Case Else
                        WsCursus = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property VModule() As String
            Get
                Return WsVModule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsVModule = value
                    Case Else
                        WsVModule = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Ordre_de_priorite_a_l_inscript() As String
            Get
                Return WsOrdre_de_priorite_a_l_inscript
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsOrdre_de_priorite_a_l_inscript = value
                    Case Else
                        WsOrdre_de_priorite_a_l_inscript = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Demande_d_hebergement() As String
            Get
                Return WsDemande_d_hebergement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsDemande_d_hebergement = value
                    Case Else
                        WsDemande_d_hebergement = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Suivi_de_la_presence_au_stage() As String
            Get
                Return WsSuivi_de_la_presence_au_stage
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsSuivi_de_la_presence_au_stage = value
                    Case Else
                        WsSuivi_de_la_presence_au_stage = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Intitule_de_la_session() As String
            Get
                Return WsIntitule_de_la_session
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule_de_la_session = value
                    Case Else
                        WsIntitule_de_la_session = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Reference_du_stage() As String
            Get
                Return WsReference_du_stage
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsReference_du_stage = value
                    Case Else
                        WsReference_du_stage = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Contexte_du_stage() As String
            Get
                Return WsContexte_du_stage
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsContexte_du_stage = value
                    Case Else
                        WsContexte_du_stage = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Type_de_stage() As String
            Get
                Return WsType_de_stage
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsType_de_stage = value
                    Case Else
                        WsType_de_stage = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Classement_du_stage() As String
            Get
                Return WsClassement_du_stage
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsClassement_du_stage = value
                    Case Else
                        WsClassement_du_stage = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Plan_de_formation() As String
            Get
                Return WsPlan_de_formation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsPlan_de_formation = value
                    Case Else
                        WsPlan_de_formation = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Cout_annexe_de_l_inscription() As Double
            Get
                Return WsCout_annexe_de_l_inscription
            End Get
            Set(ByVal value As Double)
                WsCout_annexe_de_l_inscription = value
            End Set
        End Property

        Public Property Contexte() As String
            Get
                Return WsContexte
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsContexte = value
                    Case Else
                        WsContexte = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Filiere() As String
            Get
                Return WsFiliere
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsFiliere = value
                    Case Else
                        WsFiliere = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Sidif() As String
            Get
                Return WsSidif
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsSidif = value
                    Case Is = "0", "Non"
                        WsSidif = "Non"
                    Case Else
                        WsSidif = "Oui"
                End Select
            End Set
        End Property

        Public Property Sihorstempstravail() As String
            Get
                Return WsSihorstempstravail
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsSihorstempstravail = value
                    Case Is = "0", "Non"
                        WsSihorstempstravail = "Non"
                    Case Else
                        WsSihorstempstravail = "Oui"
                End Select
            End Set
        End Property

        Public Property Rubrique() As String
            Get
                Return WsRubrique_formation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsRubrique_formation = value
                    Case Else
                        WsRubrique_formation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property DureeDIF_en_heures() As Double
            Get
                Return WsNbheures_DIF
            End Get
            Set(ByVal value As Double)
                WsNbheures_DIF = value
            End Set
        End Property

        Public Property Rang_Session() As Integer
            Get
                Return WsRangSession
            End Get
            Set(ByVal value As Integer)
                WsRangSession = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_du_stage & VI.Tild)
                Chaine.Append(Organisme & VI.Tild)
                Chaine.Append(Domaine & VI.Tild)
                Chaine.Append(Theme & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Date_de_l_inscription & VI.Tild)
                Chaine.Append(Suite_donnee & VI.Tild)
                Chaine.Append(Qualification & VI.Tild)
                Chaine.Append(Duree.ToString & VI.Tild)
                Chaine.Append(Action_de_formation & VI.Tild)
                Chaine.Append(Duree_en_heures.ToString & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Select Case Cout_de_l_inscription
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Cout_de_l_inscription.ToString) & VI.Tild)
                End Select
                Select Case Frais_de_deplacement
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Frais_de_deplacement.ToString) & VI.Tild)
                End Select
                Select Case Frais_d_hebergement
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Frais_d_hebergement.ToString) & VI.Tild)
                End Select
                Select Case Heures_de_deplacement
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(Heures_de_deplacement.ToString & VI.Tild)
                End Select
                Select Case Frais_de_restauration
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Frais_de_restauration.ToString) & VI.Tild)
                End Select
                Chaine.Append(Reference_de_la_session & VI.Tild)
                Chaine.Append(Motif_de_non_participation & VI.Tild)
                Chaine.Append(Cursus & VI.Tild)
                Chaine.Append(VModule & VI.Tild)
                Chaine.Append(Ordre_de_priorite_a_l_inscript & VI.Tild)
                Chaine.Append(Demande_d_hebergement & VI.Tild)
                Chaine.Append(Suivi_de_la_presence_au_stage & VI.Tild)
                Chaine.Append(Intitule_de_la_session & VI.Tild)
                Chaine.Append(Reference_du_stage & VI.Tild)
                Chaine.Append(Contexte_du_stage & VI.Tild)
                Chaine.Append(Type_de_stage & VI.Tild)
                Chaine.Append(Classement_du_stage & VI.Tild)
                Chaine.Append(Plan_de_formation & VI.Tild)
                Select Case Cout_annexe_de_l_inscription
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Cout_annexe_de_l_inscription.ToString) & VI.Tild)
                End Select
                Chaine.Append(Contexte & VI.Tild)
                Chaine.Append(Filiere & VI.Tild)
                Chaine.Append(Sidif & VI.Tild)
                Chaine.Append(Sihorstempstravail & VI.Tild)
                Chaine.Append(Rubrique & VI.Tild)
                Chaine.Append(DureeDIF_en_heures.ToString & VI.Tild)
                Chaine.Append(Rang_Session.ToString & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 40 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_du_stage = TableauData(1)
                Organisme = TableauData(2)
                Domaine = TableauData(3)
                Theme = TableauData(4)
                Intitule = TableauData(5)
                Date_de_l_inscription = TableauData(6)
                Suite_donnee = TableauData(7)
                Qualification = TableauData(8)
                If TableauData(9) = "" Then TableauData(9) = "0"
                Duree = VirRhFonction.ConversionDouble(TableauData(9))
                Action_de_formation = TableauData(10)
                If TableauData(11) = "" Then TableauData(11) = "0"
                Duree_en_heures = VirRhFonction.ConversionDouble(TableauData(11))
                MyBase.Date_de_Fin = TableauData(12)
                For IndiceI = 13 To 17
                    If TableauData(IndiceI) = "" Then TableauData(IndiceI) = "0"
                Next IndiceI
                Cout_de_l_inscription = VirRhFonction.ConversionDouble(TableauData(13))
                Frais_de_deplacement = VirRhFonction.ConversionDouble(TableauData(14))
                Frais_d_hebergement = VirRhFonction.ConversionDouble(TableauData(15))
                Heures_de_deplacement = CInt(TableauData(16))
                Frais_de_restauration = VirRhFonction.ConversionDouble(TableauData(17))
                Reference_de_la_session = TableauData(18)
                Motif_de_non_participation = TableauData(19)
                Cursus = TableauData(20)
                VModule = TableauData(21)
                Ordre_de_priorite_a_l_inscript = TableauData(22)
                Demande_d_hebergement = TableauData(23)
                Suivi_de_la_presence_au_stage = TableauData(24)
                Intitule_de_la_session = TableauData(25)
                Reference_du_stage = TableauData(26)
                Contexte_du_stage = TableauData(27)
                Type_de_stage = TableauData(28)
                Classement_du_stage = TableauData(29)
                Plan_de_formation = TableauData(30)
                If TableauData(31) = "" Then TableauData(31) = "0"
                Cout_annexe_de_l_inscription = VirRhFonction.ConversionDouble(TableauData(31))
                Contexte = TableauData(32)
                Filiere = TableauData(33)
                Sidif = TableauData(34)
                Sihorstempstravail = TableauData(35)
                Rubrique = TableauData(36)
                If TableauData(37) <> "" Then DureeDIF_en_heures = VirRhFonction.ConversionDouble(TableauData(37))
                If TableauData(38) <> "" Then Rang_Session = CInt(TableauData(38))
                MyBase.Certification = TableauData(39)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<DateDebutFormation>" & MyBase.Date_de_Valeur & "</DateDebutFormation>" & vbCrLf)
                Chaine.Append("<DateFinFormation>" & MyBase.Date_de_Fin & "</DateFinFormation>" & vbCrLf)
                Chaine.Append("<IntituleFormation>" & VirRhFonction.ChaineXMLValide(Intitule) & "</IntituleFormation>" & vbCrLf)
                Chaine.Append("<ReferenceStage>" & Reference_du_stage & "</ReferenceStage>" & vbCrLf)
                Chaine.Append("<ReferenceSession>" & Reference_de_la_session & "</ReferenceSession>" & vbCrLf)
                Chaine.Append("<IntituleSession>" & VirRhFonction.ChaineXMLValide(Intitule_de_la_session) & "</IntituleSession>" & vbCrLf)
                Chaine.Append("<PlanFormation>" & VirRhFonction.ChaineXMLValide(Plan_de_formation) & "</PlanFormation>" & vbCrLf)
                Chaine.Append("<DomaineFormation>" & VirRhFonction.ChaineXMLValide(Domaine) & "</DomaineFormation>" & vbCrLf)
                Chaine.Append("<ThemeFormation>" & VirRhFonction.ChaineXMLValide(Theme) & "</ThemeFormation>" & vbCrLf)
                Chaine.Append("<OrganismeFormation>" & VirRhFonction.ChaineXMLValide(Organisme) & "</OrganismeFormation>" & vbCrLf)
                Chaine.Append("<ActionFormation>" & VirRhFonction.ChaineXMLValide(Action_de_formation) & "</ActionFormation>" & vbCrLf)
                Chaine.Append("<ContexteFormation>" & VirRhFonction.ChaineXMLValide(Contexte_du_stage) & "</ContexteFormation>" & vbCrLf)
                Chaine.Append("<TypeFormation>" & VirRhFonction.ChaineXMLValide(Type_de_stage) & "</TypeFormation>" & vbCrLf)
                Chaine.Append("<ClassementFormation>" & VirRhFonction.ChaineXMLValide(Classement_du_stage) & "</ClassementFormation>" & vbCrLf)
                Chaine.Append("<FiliereFormation>" & VirRhFonction.ChaineXMLValide(Filiere) & "</FiliereFormation>" & vbCrLf)
                Chaine.Append("<CursusFormation>" & VirRhFonction.ChaineXMLValide(Cursus) & "</CursusFormation>" & vbCrLf)
                Chaine.Append("<ModuleFormation>" & VirRhFonction.ChaineXMLValide(VModule) & "</ModuleFormation>" & vbCrLf)
                Chaine.Append("<SuiteDonnee>" & VirRhFonction.ChaineXMLValide(Suite_donnee) & "</SuiteDonnee>" & vbCrLf)
                Chaine.Append("<MotifNonParticipation>" & VirRhFonction.ChaineXMLValide(Motif_de_non_participation) & "</MotifNonParticipation>" & vbCrLf)
                Chaine.Append("<PresenceAuStage>" & VirRhFonction.ChaineXMLValide(Suivi_de_la_presence_au_stage) & "</PresenceAuStage>" & vbCrLf)
                Chaine.Append("<DureeJours>" & Duree.ToString & "</DureeJours>" & vbCrLf)
                Chaine.Append("<DureeHeures>" & Duree_en_heures.ToString & "</DureeHeures>" & vbCrLf)
                Chaine.Append("<DateInscription>" & Date_de_l_inscription & "</DateInscription>" & vbCrLf)
                Chaine.Append("<OrdrePriorite>" & Ordre_de_priorite_a_l_inscript & "</OrdrePriorite>" & vbCrLf)
                Select Case Cout_de_l_inscription
                    Case Is = 0
                        Chaine.Append("<CoutInscription>" & "0" & "</CoutInscription>" & vbCrLf)
                    Case Else
                        Chaine.Append("<CoutInscription>" & VirRhFonction.VirgulePoint(Cout_de_l_inscription.ToString) & "</CoutInscription>" & vbCrLf)
                End Select
                Select Case Cout_annexe_de_l_inscription
                    Case Is = 0
                        Chaine.Append("<CoutAnnexeInscription>" & "0" & "</CoutAnnexeInscription>" & vbCrLf)
                    Case Else
                        Chaine.Append("<CoutAnnexeInscription>" & VirRhFonction.VirgulePoint(Cout_annexe_de_l_inscription.ToString) & "</CoutAnnexeInscription>" & vbCrLf)
                End Select
                Select Case Frais_de_deplacement
                    Case Is = 0
                        Chaine.Append("<FraisDeplacement>" & "0" & "</FraisDeplacement>" & vbCrLf)
                    Case Else
                        Chaine.Append("<FraisDeplacement>" & VirRhFonction.VirgulePoint(Frais_de_deplacement.ToString) & "</FraisDeplacement>" & vbCrLf)
                End Select
                Chaine.Append("<DemandeHebergement>" & Demande_d_hebergement & "</DemandeHebergement>" & vbCrLf)
                Select Case Frais_d_hebergement
                    Case Is = 0
                        Chaine.Append("<FraisHebergement>" & "0" & "</FraisHebergement>" & vbCrLf)
                    Case Else
                        Chaine.Append("<FraisHebergement>" & VirRhFonction.VirgulePoint(Frais_d_hebergement.ToString) & "</FraisHebergement>" & vbCrLf)
                End Select
                Select Case Heures_de_deplacement
                    Case Is = 0
                        Chaine.Append("<HeuresDeplacement>" & "0" & "</HeuresDeplacement>" & vbCrLf)
                    Case Else
                        Chaine.Append("<HeuresDeplacement>" & VirRhFonction.VirgulePoint(Heures_de_deplacement.ToString) & "</HeuresDeplacement>" & vbCrLf)
                End Select
                Select Case Frais_de_restauration
                    Case Is = 0
                        Chaine.Append("<FraisRestauration>" & "0" & "</FraisRestauration>" & vbCrLf)
                    Case Else
                        Chaine.Append("<FraisRestauration>" & VirRhFonction.VirgulePoint(Frais_de_restauration.ToString) & "</FraisRestauration>" & vbCrLf)
                End Select
                Chaine.Append("<SiAuTitreDuDIF>" & Sidif & "</SiAuTitreDuDIF>" & vbCrLf)
                Chaine.Append("<SihorstempstravailDIF>" & Sihorstempstravail & "</SihorstempstravailDIF>" & vbCrLf)

                Return Chaine.ToString

            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace



