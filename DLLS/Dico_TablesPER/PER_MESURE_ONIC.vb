﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_MESURE_ONIC
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_Effet As String
        Private WsNumeroOrdre As Integer
        Private WsMission As String
        Private WsActivite As String
        Private WsMesure As String
        Private WsFonction As String
        Private WsPourcentageRepartition As Integer
        Private WsVisa As String
        Private WsCommentaire As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_MESURE_ONIC"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 110
            End Get
        End Property

        Public Property Date_Effet() As String
            Get
                Return WsDate_Effet
            End Get
            Set(ByVal value As String)
                WsDate_Effet = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_Effet
            End Set
        End Property

        Public Property Numero_Ordre() As Integer
            Get
                Return WsNumeroOrdre
            End Get
            Set(ByVal value As Integer)
                WsNumeroOrdre = value
                MyBase.Clef = value.ToString
            End Set
        End Property

        Public Property Mission() As String
            Get
                Return WsMission
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsMission = value
                    Case Else
                        WsMission = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Activite() As String
            Get
                Return WsActivite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsActivite = value
                    Case Else
                        WsActivite = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Mesure() As String
            Get
                Return WsMesure
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsMesure = value
                    Case Else
                        WsMesure = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Fonction() As String
            Get
                Return WsFonction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsFonction = value
                    Case Else
                        WsFonction = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Pourcentage_Repartition() As Double
            Get
                Return WsPourcentageRepartition
            End Get
            Set(ByVal value As Double)
                WsPourcentageRepartition = value
            End Set
        End Property

        Public Property Visa() As String
            Get
                Return WsVisa
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsVisa = value
                    Case Else
                        WsVisa = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Commentaire() As String
            Get
                Return WsCommentaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsCommentaire = value
                    Case Else
                        WsCommentaire = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Numero_Ordre.ToString & VI.Tild)
                Chaine.Append(Mission & VI.Tild)
                Chaine.Append(Activite & VI.Tild)
                Chaine.Append(Mesure & VI.Tild)
                Chaine.Append(Fonction & VI.Tild)
                Chaine.Append(Pourcentage_Repartition.ToString & VI.Tild)
                Chaine.Append(Visa & VI.Tild)
                Chaine.Append(Commentaire)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 10 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) = "" Then TableauData(2) = "0"
                Numero_Ordre = VirRhFonction.ConversionDouble(TableauData(2))
                Mission = TableauData(3)
                Activite = TableauData(4)
                Mesure = TableauData(5)
                Fonction = TableauData(6)
                If TableauData(7) = "" Then TableauData(7) = "0"
                Pourcentage_Repartition = VirRhFonction.ConversionDouble(TableauData(7))
                Visa = TableauData(8)
                Commentaire = TableauData(9)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_Effet
                    Case 1
                        Return WsNumeroOrdre.ToString
                    Case 2
                        Return WsMission
                    Case 3
                        Return WsActivite
                    Case 4
                        Return WsMesure
                    Case 5
                        Return WsFonction.ToString
                    Case 6
                        Return WsPourcentageRepartition.ToString
                    Case 7
                        Return WsVisa
                    Case 8
                        Return WsCommentaire
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
