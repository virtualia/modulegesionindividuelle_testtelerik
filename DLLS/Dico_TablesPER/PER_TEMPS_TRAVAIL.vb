﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_TEMPS_TRAVAIL
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_Valorisation As String
        Private WsRegleValo As String
        Private WsObservations As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_TEMPS_TRAVAIL"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaValtemps
            End Get
        End Property

        Public Property Date_Valorisation() As String
            Get
                Return WsDate_Valorisation
            End Get
            Set(ByVal value As String)
                WsDate_Valorisation = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_Valorisation
            End Set
        End Property

        Public Property Regle_Valorisation() As String
            Get
                Return WsRegleValo
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsRegleValo = value
                    Case Else
                        WsRegleValo = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsRegleValo
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_Valorisation & VI.Tild)
                Chaine.Append(Regle_Valorisation & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Observations)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 5 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_Valorisation = TableauData(1)
                Regle_Valorisation = TableauData(2)
                MyBase.Date_de_Fin = TableauData(3)
                Observations = TableauData(4)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_Valorisation
                    Case 1
                        Return WsRegleValo
                    Case 2
                        Return Date_de_Fin
                    Case 3
                        Return WsObservations
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


