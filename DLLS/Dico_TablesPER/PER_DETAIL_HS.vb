﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_DETAIL_HS
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsNature As String = ""
        Private WsFaitGenerateur As String = ""
        Private WsHeureBorne As String = ""
        Private WsHeureFin As String = ""
        Private WsNbheures_Jusqua20h As Double
        Private WsNbheures_De20ha22h As Double
        Private WsNbheures_Nuit As Double
        Private WsNbheures_Trajet As Double
        Private WsObservations As String = ""
        Private WsDate_Validation As String = ""
        Private WsModuleOrigine As String = ""
        Private WsSignataire As String = ""
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_DETAIL_HS"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaDetailHS
            End Get
        End Property

        Public Property Nature() As String
            Get
                Return WsNature
            End Get
            Set(ByVal value As String)
                WsNature = F_FormatAlpha(value, 30)
                MyBase.Clef = WsNature
            End Set
        End Property

        Public Property FaitGenerateur() As String
            Get
                Return WsFaitGenerateur
            End Get
            Set(ByVal value As String)
                WsFaitGenerateur = F_FormatAlpha(value, 80)
            End Set
        End Property

        Public Property Heure_Borne() As String
            Get
                Return WsHeureBorne
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureBorne = value
                    Case Else
                        WsHeureBorne = Strings.Left(value, 5)
                End Select
                If WsHeureFin <> "" Then
                    Call Actualiser()
                End If
            End Set
        End Property

        Public Property Heure_Fin() As String
            Get
                Return WsHeureFin
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureFin = value
                    Case Else
                        WsHeureFin = Strings.Left(value, 5)
                End Select
                If WsHeureBorne <> "" Then
                    Call Actualiser()
                End If
            End Set
        End Property

        Public Property Nbheures_Jusqua20h() As Double
            Get
                Return WsNbheures_Jusqua20h
            End Get
            Set(ByVal value As Double)
                WsNbheures_Jusqua20h = Math.Round(value, 2)
            End Set
        End Property

        Public Property Nbheures_De20ha22h() As Double
            Get
                Return WsNbheures_De20ha22h
            End Get
            Set(ByVal value As Double)
                WsNbheures_De20ha22h = Math.Round(value, 2)
            End Set
        End Property

        Public Property Nbheures_Nuit() As Double
            Get
                Return WsNbheures_Nuit
            End Get
            Set(ByVal value As Double)
                WsNbheures_Nuit = value
            End Set
        End Property

        Public Property Nbheures_Trajet() As Double
            Get
                Return WsNbheures_Trajet
            End Get
            Set(ByVal value As Double)
                WsNbheures_Trajet = Math.Round(value, 2)
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                F_FormatAlpha(value, 250)
            End Set
        End Property

        Public Property Date_Validation() As String
            Get
                Return WsDate_Validation
            End Get
            Set(ByVal value As String)
                WsDate_Validation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property ModuleOrigine() As String
            Get
                Return WsModuleOrigine
            End Get
            Set(ByVal value As String)
                WsModuleOrigine = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Signataire() As String
            Get
                Return WsSignataire
            End Get
            Set(ByVal value As String)
                WsSignataire = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Nature & VI.Tild)
                Chaine.Append(FaitGenerateur & VI.Tild)
                Chaine.Append(Heure_Borne & VI.Tild)
                Chaine.Append(Heure_Fin & VI.Tild)
                Chaine.Append(Nbheures_Jusqua20h.ToString & VI.Tild)
                Chaine.Append(Nbheures_De20ha22h.ToString & VI.Tild)
                Chaine.Append(Nbheures_Nuit.ToString & VI.Tild)
                Chaine.Append(Nbheures_Trajet.ToString & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(Date_Validation & VI.Tild)
                Chaine.Append(ModuleOrigine & VI.Tild)
                Chaine.Append(Signataire)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 13 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Nature = TableauData(2)
                FaitGenerateur = TableauData(3)
                Heure_Borne = TableauData(4)
                Heure_Fin = TableauData(5)
                Nbheures_Jusqua20h = VirRhFonction.ConversionDouble(TableauData(6), 2)
                Nbheures_De20ha22h = VirRhFonction.ConversionDouble(TableauData(7), 2)
                Nbheures_Nuit = VirRhFonction.ConversionDouble(TableauData(8), 2)
                Nbheures_Trajet = VirRhFonction.ConversionDouble(TableauData(9), 2)
                Observations = TableauData(10)
                Date_Validation = TableauData(11)
                ModuleOrigine = TableauData(12)
                Signataire = TableauData(13)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property SiHeureSupplementaire As Boolean
            Get
                If Nature = "Heure supplémentaire" Then
                    Return True
                End If
                Return False
            End Get
        End Property

        Public ReadOnly Property SiAstreinte As Boolean
            Get
                If Nature = "Astreinte" Then
                    Return True
                End If
                Return False
            End Get
        End Property

        Public ReadOnly Property SiJourOuvrable As Boolean
            Get
                Return VirRhDate.SiJourOuvrable(MyBase.Date_de_Valeur, True)
            End Get
        End Property
        Public ReadOnly Property SiValide_Par_Manager As Boolean
            Get
                If ModuleOrigine = "ManagerV4" Then
                    Return True
                End If
                Return False
            End Get
        End Property

        Public ReadOnly Property V_NbHeures_Jusqua20h As String
            Get
                If WsHeureBorne = "" Or WsHeureFin = "" Then
                    Return ""
                End If
                Dim HTypeeBorne As System.DateTime
                Dim HTypeeFin As System.DateTime
                Dim HTypee20h As System.DateTime
                Dim HCalc As System.TimeSpan

                HTypee20h = VirRhDate.HeureTypee(MyBase.Date_de_Valeur, "20:00")
                HTypeeBorne = VirRhDate.HeureTypee(MyBase.Date_de_Valeur, WsHeureBorne)
                Select Case VirRhDate.ComparerHeures(WsHeureFin, WsHeureBorne)
                    Case VI.ComparaisonDates.PlusPetit
                        HTypeeFin = VirRhDate.HeureTypee(MyBase.Date_Valeur_ToDate.AddDays(1).ToShortDateString, WsHeureFin)
                    Case Else
                        HTypeeFin = VirRhDate.HeureTypee(MyBase.Date_de_Valeur, WsHeureFin)
                End Select
                If HTypeeFin <= HTypee20h Then
                    HCalc = HTypeeFin.Subtract(HTypeeBorne)
                Else
                    HCalc = HTypee20h.Subtract(HTypeeBorne)
                End If
                Return Strings.Format(HCalc.Hours, "00") & " h " & Strings.Format(HCalc.Minutes, "00")
            End Get
        End Property

        Public ReadOnly Property V_NbHeures_de20ha22h As String
            Get
                If WsHeureBorne = "" Or WsHeureFin = "" Then
                    Return ""
                End If
                Dim HTypeeBorne As System.DateTime
                Dim HTypeeFin As System.DateTime
                Dim HTypee20h As System.DateTime
                Dim HTypee22h As System.DateTime
                Dim HCalc As System.TimeSpan

                HTypee20h = VirRhDate.HeureTypee(MyBase.Date_de_Valeur, "20:00")
                HTypee22h = VirRhDate.HeureTypee(MyBase.Date_de_Valeur, "22:00")
                HTypeeBorne = VirRhDate.HeureTypee(MyBase.Date_de_Valeur, WsHeureBorne)
                Select Case VirRhDate.ComparerHeures(WsHeureFin, WsHeureBorne)
                    Case VI.ComparaisonDates.PlusPetit
                        HTypeeFin = VirRhDate.HeureTypee(MyBase.Date_Valeur_ToDate.AddDays(1).ToShortDateString, WsHeureFin)
                    Case Else
                        HTypeeFin = VirRhDate.HeureTypee(MyBase.Date_de_Valeur, WsHeureFin)
                End Select
                If HTypeeFin <= HTypee20h Then
                    Return ""
                End If
                If HTypeeFin <= HTypee22h Then
                    HCalc = HTypeeFin.Subtract(HTypee20h)
                    Return Strings.Format(HCalc.Hours, "00") & " h " & Strings.Format(HCalc.Minutes, "00")
                Else
                    Return "02 h 00"
                End If
            End Get
        End Property

        Public ReadOnly Property V_NbHeures_deNuit As String
            Get
                If WsHeureBorne = "" Or WsHeureFin = "" Then
                    Return ""
                End If
                Dim HTypeeBorne As System.DateTime
                Dim HTypeeFin As System.DateTime
                Dim HTypee22h As System.DateTime
                Dim HCalc As System.TimeSpan

                HTypee22h = VirRhDate.HeureTypee(MyBase.Date_de_Valeur, "22:00")
                HTypeeBorne = VirRhDate.HeureTypee(MyBase.Date_de_Valeur, WsHeureBorne)
                Select Case VirRhDate.ComparerHeures(WsHeureFin, WsHeureBorne)
                    Case VI.ComparaisonDates.PlusPetit
                        HTypeeFin = VirRhDate.HeureTypee(MyBase.Date_Valeur_ToDate.AddDays(1).ToShortDateString, WsHeureFin)
                    Case Else
                        HTypeeFin = VirRhDate.HeureTypee(MyBase.Date_de_Valeur, WsHeureFin)
                End Select
                If HTypeeFin <= HTypee22h Then
                    Return ""
                End If
                HCalc = HTypeeFin.Subtract(HTypee22h)
                Return Strings.Format(HCalc.Hours, "00") & " h " & Strings.Format(HCalc.Minutes, "00")
            End Get
        End Property

        Public ReadOnly Property V_NbHeures_Trajet As String
            Get
                If WsHeureBorne = "" Or WsHeureFin = "" Then
                    Return ""
                End If
                Dim HTypeeTrajet As System.DateTime
                Dim HCalc As System.DateTime

                HTypeeTrajet = VirRhDate.HeureTypee(MyBase.Date_de_Valeur, "00:00")
                HCalc = HTypeeTrajet.AddMinutes(Math.Round(Nbheures_Trajet * 60))
                Return Strings.Format(HCalc.Hour, "00") & " h " & Strings.Format(HCalc.Minute, "00")
            End Get
        End Property

        Public Sub Actualiser()
            Nbheures_Jusqua20h = 0
            Nbheures_De20ha22h = 0
            Nbheures_Nuit = 0
            If WsHeureBorne = "" Or WsHeureFin = "" Then
                Exit Sub
            End If
            If WsFaitGenerateur <> "" And WsDate_Validation <> "" Then
                Exit Sub
            End If
            If V_NbHeures_Jusqua20h <> "" Then
                Nbheures_Jusqua20h = VirRhDate.ConvHeuresEnCentieme(V_NbHeures_Jusqua20h)
            End If
            If V_NbHeures_de20ha22h <> "" Then
                Nbheures_De20ha22h = VirRhDate.ConvHeuresEnCentieme(V_NbHeures_de20ha22h)
            End If
            If V_NbHeures_deNuit <> "" Then
                Nbheures_Nuit = VirRhDate.ConvHeuresEnCentieme(V_NbHeures_deNuit)
            End If
        End Sub

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
