Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_DECORATION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_d_effet As String
        Private WsDecoration As String
        Private WsObservations As String
        Private WsGrade As String
        Private WsDate_de_remise As String
        Private WsDate_du_decret As String
        Private WsDate_de_parution As String
        Private WsMinistere As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_DECORATION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaDecoration
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 8
            End Get
        End Property

        Public Property Date_d_effet() As String
            Get
                Return WsDate_d_effet
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is <> ""
                        WsDate_d_effet = VirRhDate.DateStandardVirtualia(value)
                    Case Else
                        WsDate_d_effet = ""
                End Select
                MyBase.Date_de_Valeur = WsDate_d_effet
            End Set
        End Property

        Public Property Decoration() As String
            Get
                Return WsDecoration
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsDecoration = value
                    Case Else
                        WsDecoration = Strings.Left(value, 100)
                End Select
                MyBase.Clef = WsDecoration
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Grade() As String
            Get
                Return WsGrade
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsGrade = value
                    Case Else
                        WsGrade = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Date_de_remise() As String
            Get
                Return WsDate_de_remise
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is <> ""
                        WsDate_de_remise = VirRhDate.DateStandardVirtualia(value)
                    Case Else
                        WsDate_de_remise = ""
                End Select
            End Set
        End Property

        Public Property Date_du_decret() As String
            Get
                Return WsDate_du_decret
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is <> ""
                        WsDate_du_decret = VirRhDate.DateStandardVirtualia(value)
                    Case Else
                        WsDate_du_decret = ""
                End Select
            End Set
        End Property

        Public Property Date_de_parution() As String
            Get
                Return WsDate_de_parution
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is <> ""
                        WsDate_de_parution = VirRhDate.DateStandardVirtualia(value)
                    Case Else
                        WsDate_de_parution = ""
                End Select
            End Set
        End Property

        Public Property Ministere() As String
            Get
                Return WsMinistere
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsMinistere = value
                    Case Else
                        WsMinistere = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_d_effet & VI.Tild)
                Chaine.Append(Decoration & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(Grade & VI.Tild)
                Chaine.Append(Date_de_remise & VI.Tild)
                Chaine.Append(Date_du_decret & VI.Tild)
                Chaine.Append(Date_de_parution & VI.Tild)
                Chaine.Append(Ministere & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 10 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_d_effet = TableauData(1)
                Decoration = TableauData(2)
                Observations = TableauData(3)
                Grade = TableauData(4)
                Date_de_remise = TableauData(5)
                Date_du_decret = TableauData(6)
                Date_de_parution = TableauData(7)
                Ministere = TableauData(8)
                MyBase.Certification = TableauData(9)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

