﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_RETRAITE_ENFANTS
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        Private WsSiMajuscule As Boolean
        '
        Private WsDate_Naissance As String
        Private WsPrenom As String
        Private WsNom As String
        Private WsNoInsee As String
        Private WsSexe As String
        Private WsDeptPaysNaissance As String
        Private WsVilleNaissance As String
        Private WsTauxInfirmite As Double
        Private WsDate_Adoption As String
        Private WsDate_Debut_PriseEnCharge As String
        Private WsDate_Fin_PriseEnCharge As String
        Private WsDuree_PriseEnCharge As String
        Private WsDate_Debut_Filiation As String
        Private WsDate_Fin_Filiation As String
        Private WsLienAgent As String
        Private WsSiArticleL12B As String
        Private WsSiArticleL12BBIS As String
        Private WsMajorationL12BIS As Integer
        Private WsDate_Deces As String
        Private WsSiDecesFaitdeGuerre As String
        Private WsMajorationL12TER As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_RETRAITE_ENFANTS"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 66
            End Get
        End Property

        Public Property Date_Naissance() As String
            Get
                Return WsDate_Naissance
            End Get
            Set(ByVal value As String)
                WsDate_Naissance = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_Naissance
            End Set
        End Property

        Public Property Prenom() As String
            Get
                Return WsPrenom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsPrenom = value
                    Case Else
                        WsPrenom = Strings.Left(value, 35)
                End Select
                MyBase.Clef = WsPrenom
            End Set
        End Property

        Public Property Nom() As String
            Get
                Return WsNom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsNom = value
                    Case Else
                        WsNom = Strings.Left(value, 35)
                End Select
            End Set
        End Property

        Public Property Numero_INSEE() As String
            Get
                Return WsNoInsee
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 13
                        WsNoInsee = value
                    Case Else
                        WsNoInsee = Strings.Left(value, 13)
                End Select
            End Set
        End Property

        Public Property Sexe() As String
            Get
                Return WsSexe
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsSexe = value
                    Case Else
                        WsSexe = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Departement_Pays_Naissance() As String
            Get
                Return WsDeptPaysNaissance
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsDeptPaysNaissance = value
                    Case Else
                        WsDeptPaysNaissance = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Ville_Naissance() As String
            Get
                Return WsVilleNaissance
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsVilleNaissance = value
                    Case Else
                        WsVilleNaissance = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Property Taux_Infirmite() As Double
            Get
                Return WsTauxInfirmite
            End Get
            Set(ByVal value As Double)
                WsTauxInfirmite = value
            End Set
        End Property

        Public Property Date_Adoption() As String
            Get
                Return WsDate_Adoption
            End Get
            Set(ByVal value As String)
                WsDate_Adoption = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Debut_PriseEnCharge() As String
            Get
                Return WsDate_Debut_PriseEnCharge
            End Get
            Set(ByVal value As String)
                WsDate_Debut_PriseEnCharge = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Fin_PriseEnCharge() As String
            Get
                Return WsDate_Fin_PriseEnCharge
            End Get
            Set(ByVal value As String)
                WsDate_Fin_PriseEnCharge = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Duree_PriseEnCharge() As String
            Get
                Return WsDuree_PriseEnCharge
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 6
                        WsDuree_PriseEnCharge = value
                    Case Else
                        WsDuree_PriseEnCharge = Strings.Left(value, 6)
                End Select
            End Set
        End Property

        Public Property Date_Debut_Filiation() As String
            Get
                Return WsDate_Debut_Filiation
            End Get
            Set(ByVal value As String)
                WsDate_Debut_Filiation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_Fin_Filiation() As String
            Get
                Return WsDate_Fin_Filiation
            End Get
            Set(ByVal value As String)
                WsDate_Fin_Filiation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Lien_Avec_Agent() As String
            Get
                Return WsLienAgent
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsLienAgent = value
                    Case Else
                        WsLienAgent = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property SiArticle_L12B() As String
            Get
                Return WsSiArticleL12B
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiArticleL12B = value
                    Case Else
                        WsSiArticleL12B = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property SiArticle_L12BBIS() As String
            Get
                Return WsSiArticleL12BBIS
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiArticleL12BBIS = value
                    Case Else
                        WsSiArticleL12BBIS = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Majoration_L12BBIS() As Integer
            Get
                Return WsMajorationL12BIS
            End Get
            Set(ByVal value As Integer)
                WsMajorationL12BIS = value
            End Set
        End Property

        Public Property Date_Deces() As String
            Get
                Return WsDate_Deces
            End Get
            Set(ByVal value As String)
                WsDate_Deces = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property SiDeces_FaitdeGuerre() As String
            Get
                Return WsSiDecesFaitdeGuerre
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiDecesFaitdeGuerre = value
                    Case Else
                        WsSiDecesFaitdeGuerre = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Majoration_L12TER() As Integer
            Get
                Return WsMajorationL12TER
            End Get
            Set(ByVal value As Integer)
                WsMajorationL12TER = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_Naissance & VI.Tild)
                Chaine.Append(Prenom & VI.Tild)
                Chaine.Append(Nom & VI.Tild)
                Chaine.Append(Numero_INSEE & VI.Tild)
                Chaine.Append(Sexe & VI.Tild)
                Chaine.Append(Departement_Pays_Naissance & VI.Tild)
                Chaine.Append(Ville_Naissance & VI.Tild)
                Chaine.Append(Taux_Infirmite.ToString & VI.Tild)
                Chaine.Append(Date_Adoption & VI.Tild)
                Chaine.Append(Date_Debut_PriseEnCharge & VI.Tild)
                Chaine.Append(Date_Fin_PriseEnCharge & VI.Tild)
                Chaine.Append(Duree_PriseEnCharge & VI.Tild)
                Chaine.Append(Date_Debut_Filiation & VI.Tild)
                Chaine.Append(Date_Fin_Filiation & VI.Tild)
                Chaine.Append(Lien_Avec_Agent & VI.Tild)
                Chaine.Append(SiArticle_L12B & VI.Tild)
                Chaine.Append(SiArticle_L12BBIS & VI.Tild)
                Chaine.Append(Majoration_L12BBIS.ToString & VI.Tild)
                Chaine.Append(Date_Deces & VI.Tild)
                Chaine.Append(SiDeces_FaitdeGuerre & VI.Tild)
                Chaine.Append(Majoration_L12TER.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 22 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_Naissance = TableauData(1)
                Prenom = TableauData(2)
                Nom = TableauData(3)
                Numero_INSEE = TableauData(4)
                Sexe = TableauData(5)
                Departement_Pays_Naissance = TableauData(6)
                Ville_Naissance = TableauData(7)
                If TableauData(8) = "" Then TableauData(8) = "0"
                Taux_Infirmite = VirRhFonction.ConversionDouble(TableauData(8))
                Date_Adoption = TableauData(9)
                Date_Debut_PriseEnCharge = TableauData(10)
                Date_Fin_PriseEnCharge = TableauData(11)
                Duree_PriseEnCharge = TableauData(12)
                Date_Debut_Filiation = TableauData(13)
                Date_Fin_Filiation = TableauData(14)
                Lien_Avec_Agent = TableauData(15)
                SiArticle_L12B = TableauData(16)
                SiArticle_L12BBIS = TableauData(17)
                If TableauData(18) = "" Then TableauData(18) = "0"
                Majoration_L12BBIS = CInt(TableauData(18))
                Date_Deces = TableauData(19)
                SiDeces_FaitdeGuerre = TableauData(20)
                If TableauData(21) = "" Then TableauData(21) = "0"
                Majoration_L12BBIS = CInt(TableauData(21))

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_Naissance
                    Case 1
                        Return WsPrenom
                    Case 2
                        Return WsNom
                    Case 3
                        Return WsNoInsee
                    Case 4
                        Return WsSexe
                    Case 5
                        Return WsDeptPaysNaissance
                    Case 6
                        Return WsVilleNaissance
                    Case 7
                        Return WsTauxInfirmite.ToString
                    Case 8
                        Return WsDate_Adoption
                    Case 9
                        Return WsDate_Debut_PriseEnCharge
                    Case 10
                        Return WsDate_Fin_PriseEnCharge
                    Case 11
                        Return WsDuree_PriseEnCharge
                    Case 12
                        Return WsDate_Debut_Filiation
                    Case 13
                        Return WsDate_Fin_Filiation
                    Case 14
                        Return WsLienAgent
                    Case 15
                        Return WsSiArticleL12B
                    Case 16
                        Return WsSiArticleL12BBIS
                    Case 17
                        Return WsMajorationL12BIS.ToString
                    Case 18
                        Return WsDate_Deces
                    Case 19
                        Return WsSiDecesFaitdeGuerre
                    Case 20
                        Return WsMajorationL12TER.ToString
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

