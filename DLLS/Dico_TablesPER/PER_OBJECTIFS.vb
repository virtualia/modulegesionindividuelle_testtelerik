﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_OBJECTIFS
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsAnnee_Valeur As String
        Private WsDefinition As String
        Private WsPourcentage_SalaireAnnuel As Double = 0
        Private WsPourcentage_ObjectifsQualitatifs As Double = 0
        Private WsPourcentage_ObjectifsQuantitatifs As Double = 0
        Private WsRealisation_ObjectifsQualitatifs As Double = 0
        Private WsRealisation_ObjectifsQuantitatifs As Double = 0
        Private WsRealisation_TotalObjectifs As Double = 0
        Private WsMontantAttribue As Double = 0
        Private WsBonusExceptionnel As Double = 0
        Private WsObservations As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_OBJECTIFS"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaObjectifs 'Spécifique Secteur Privé
            End Get
        End Property

        Public Property Annee_Valeur() As String
            Get
                Return WsAnnee_Valeur
            End Get
            Set(ByVal value As String)
                WsAnnee_Valeur = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Definition_Objectif() As String
            Get
                Return WsDefinition
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsDefinition = value
                    Case Else
                        WsDefinition = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsDefinition
            End Set
        End Property

        Public Property Pourcentage_SalaireAnnuel() As Double
            Get
                Return WsPourcentage_SalaireAnnuel
            End Get
            Set(ByVal value As Double)
                WsPourcentage_SalaireAnnuel = value
            End Set
        End Property

        Public Property Pourcentage_ObjectifsQualitatifs() As Double
            Get
                Return WsPourcentage_ObjectifsQualitatifs
            End Get
            Set(ByVal value As Double)
                WsPourcentage_ObjectifsQualitatifs = value
            End Set
        End Property

        Public Property Pourcentage_ObjectifsQuantitatifs() As Double
            Get
                Return WsPourcentage_ObjectifsQuantitatifs
            End Get
            Set(ByVal value As Double)
                WsPourcentage_ObjectifsQuantitatifs = value
            End Set
        End Property

        Public Property Realisation_ObjectifsQualitatifs() As Double
            Get
                Return WsRealisation_ObjectifsQualitatifs
            End Get
            Set(ByVal value As Double)
                WsRealisation_ObjectifsQualitatifs = value
            End Set
        End Property

        Public Property Realisation_ObjectifsQuantitatifs() As Double
            Get
                Return WsRealisation_ObjectifsQuantitatifs
            End Get
            Set(ByVal value As Double)
                WsRealisation_ObjectifsQuantitatifs = value
            End Set
        End Property

        Public Property Realisation_Total() As Double
            Get
                Return WsRealisation_TotalObjectifs
            End Get
            Set(ByVal value As Double)
                WsRealisation_TotalObjectifs = value
            End Set
        End Property

        Public Property MontantAttribue() As Double
            Get
                Return WsMontantAttribue
            End Get
            Set(ByVal value As Double)
                WsMontantAttribue = value
            End Set
        End Property

        Public Property BonusExceptionnel() As Double
            Get
                Return WsBonusExceptionnel
            End Get
            Set(ByVal value As Double)
                WsBonusExceptionnel = value
            End Set
        End Property

        Public Property Observation() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Annee_Valeur & VI.Tild)
                Chaine.Append(Definition_Objectif & VI.Tild)
                Select Case Pourcentage_SalaireAnnuel
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Pourcentage_SalaireAnnuel.ToString) & VI.Tild)
                End Select
                Select Case Pourcentage_ObjectifsQualitatifs
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Pourcentage_ObjectifsQualitatifs.ToString) & VI.Tild)
                End Select
                Select Case Pourcentage_ObjectifsQuantitatifs
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Pourcentage_ObjectifsQuantitatifs.ToString) & VI.Tild)
                End Select
                Select Case Realisation_ObjectifsQualitatifs
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Realisation_ObjectifsQualitatifs.ToString) & VI.Tild)
                End Select
                Select Case Realisation_ObjectifsQuantitatifs
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Realisation_ObjectifsQuantitatifs.ToString) & VI.Tild)
                End Select
                Select Case Realisation_Total
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Realisation_Total.ToString) & VI.Tild)
                End Select
                Select Case MontantAttribue
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(MontantAttribue.ToString) & VI.Tild)
                End Select
                Select Case BonusExceptionnel
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(BonusExceptionnel.ToString) & VI.Tild)
                End Select
                Chaine.Append(Observation)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 12 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Annee_Valeur = TableauData(1)
                Definition_Objectif = TableauData(2)
                For IndiceI = 3 To 10
                    If TableauData(IndiceI) = "" Then
                        TableauData(IndiceI) = "0"
                    End If
                Next IndiceI
                Pourcentage_SalaireAnnuel = Val(VirRhFonction.MontantStocke(TableauData(3)))
                Pourcentage_ObjectifsQualitatifs = Val(VirRhFonction.MontantStocke(TableauData(4)))
                Pourcentage_ObjectifsQuantitatifs = Val(VirRhFonction.MontantStocke(TableauData(5)))
                Realisation_ObjectifsQualitatifs = Val(VirRhFonction.MontantStocke(TableauData(6)))
                Realisation_ObjectifsQuantitatifs = Val(VirRhFonction.MontantStocke(TableauData(7)))
                Realisation_Total = Val(VirRhFonction.MontantStocke(TableauData(8)))
                MontantAttribue = Val(VirRhFonction.MontantStocke(TableauData(9)))
                BonusExceptionnel = Val(VirRhFonction.MontantStocke(TableauData(10)))
                Observation = TableauData(11)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsAnnee_Valeur
                    Case 1
                        Return WsDefinition
                    Case 2
                        Select Case WsPourcentage_SalaireAnnuel
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(WsPourcentage_SalaireAnnuel.ToString)
                        End Select
                    Case 3
                        Select Case WsPourcentage_ObjectifsQualitatifs
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(WsPourcentage_ObjectifsQualitatifs.ToString)
                        End Select
                    Case 4
                        Select Case WsPourcentage_ObjectifsQuantitatifs
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(WsPourcentage_ObjectifsQuantitatifs.ToString)
                        End Select
                    Case 5
                        Select Case WsRealisation_ObjectifsQualitatifs
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(WsRealisation_ObjectifsQualitatifs.ToString)
                        End Select
                    Case 6
                        Select Case WsRealisation_ObjectifsQuantitatifs
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(WsRealisation_ObjectifsQuantitatifs.ToString)
                        End Select
                    Case 7
                        Select Case WsRealisation_TotalObjectifs
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(WsRealisation_TotalObjectifs.ToString)
                        End Select
                    Case 8
                        Select Case WsMontantAttribue
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(WsMontantAttribue.ToString)
                        End Select
                    Case 9
                        Select Case WsBonusExceptionnel
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(WsBonusExceptionnel.ToString)
                        End Select
                    Case 10
                        Return WsObservations
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


