﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class VConstructeur
        Implements Virtualia.Systeme.IVConstructeur
        Private WsTypeModele As Integer
        Private WsPointdeVue As Integer = 1
        Private WsTypeParametre As Integer = 0

        Private Enum SensParametre As Integer
            EntretienMagi = 1
            EntretienCite = 2
            FraisCES = 3
            EntretienHonda = 4
        End Enum

        Public Function V_NouvelleFiche(ByVal NumObjet As Integer, ByVal IdeDossier As Integer, ByVal LstDatas As List(Of String)) As Virtualia.Systeme.MetaModele.VIR_FICHE Implements Systeme.IVConstructeur.V_NouvelleFiche

            Dim FichePER As Virtualia.Systeme.MetaModele.VIR_FICHE = Nothing
            Select Case NumObjet
                Case VI.ObjetPer.ObaCivil
                    FichePER = New PER_ETATCIVIL
                Case VI.ObjetPer.ObaConjoint
                    FichePER = New PER_CONJOINT
                Case VI.ObjetPer.ObaEnfant
                    FichePER = New PER_ENFANT
                Case VI.ObjetPer.ObaAdresse
                    FichePER = New PER_DOMICILE
                Case VI.ObjetPer.ObaBanque
                    FichePER = New PER_BANQUE
                Case VI.ObjetPer.ObaDiplome
                    FichePER = New PER_DIPLOME
                Case VI.ObjetPer.ObaExperience
                    FichePER = New PER_EXPERIENCE_CV
                Case VI.ObjetPer.ObaAcquis
                    FichePER = New PER_ACQUIS_CV
                Case VI.ObjetPer.ObaLangue
                    FichePER = New PER_LANGUE_CV
                Case VI.ObjetPer.ObaLoisir
                    FichePER = New PER_LOISIR_CV
                Case VI.ObjetPer.ObaStageCv
                    FichePER = New PER_STAGE_CV
                Case VI.ObjetPer.ObaStatut
                    Select Case WsTypeModele
                        Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                            FichePER = New PER_STATUT
                        Case Else
                            FichePER = New PER_CONTRAT
                    End Select
                Case VI.ObjetPer.ObaActivite
                    Select Case WsTypeModele
                        Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                            FichePER = New PER_POSITION
                        Case Else
                            FichePER = New PER_ACTIVITE
                    End Select
                Case VI.ObjetPer.ObaGrade
                    Select Case WsTypeModele
                        Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                            FichePER = New PER_GRADE
                        Case Else
                            FichePER = New PER_EMPLOI
                    End Select
                Case VI.ObjetPer.ObaSociete
                    Select Case WsTypeModele
                        Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                            FichePER = New PER_COLLECTIVITE
                        Case Else
                            FichePER = New PER_SOCIETE
                    End Select
                Case VI.ObjetPer.ObaAbsence
                    FichePER = New PER_ABSENCE
                Case VI.ObjetPer.ObaDroits
                    FichePER = New PER_DROIT_CONGES
                Case VI.ObjetPer.ObaOrganigramme
                    FichePER = New PER_AFFECTATION
                Case VI.ObjetPer.ObaPostebud
                    Select Case WsTypeModele
                        Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                            FichePER = New PER_EMPLOI_BUDGET
                        Case Else
                            FichePER = New PER_AFFECTATION_BUDGET
                    End Select
                Case VI.ObjetPer.ObaNotation
                    FichePER = New PER_NOTATION
                Case VI.ObjetPer.ObaMemento
                    FichePER = New PER_ANNOTATION
                Case VI.ObjetPer.ObaIndemnite
                    FichePER = New PER_INDEMNITE
                Case VI.ObjetPer.ObaArmee
                    FichePER = New PER_SERVICE_NATION
                Case VI.ObjetPer.ObaExterne
                    FichePER = New PER_REFEXTERNE
                Case VI.ObjetPer.ObaAdrPro
                    FichePER = New PER_ADRESSEPRO
                Case VI.ObjetPer.ObaOrigine
                    Select Case WsTypeModele
                        Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                            FichePER = New PER_ADMORIGINE
                        Case Else
                            FichePER = New PER_GROUPE
                    End Select
                Case VI.ObjetPer.ObaSociete
                    Select Case WsTypeModele
                        Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                            FichePER = New PER_COLLECTIVITE
                        Case Else
                            FichePER = New PER_SOCIETE
                    End Select
                Case VI.ObjetPer.ObaCaisse
                    FichePER = New PER_AFFILIATION
                Case VI.ObjetPer.ObaAgenda
                    FichePER = New PER_NOTEAGENDA
                Case VI.ObjetPer.ObaFormation
                    FichePER = New PER_SESSION
                Case VI.ObjetPer.ObaPrevenir
                    FichePER = New PER_APREVENIR
                Case VI.ObjetPer.ObaEtranger
                    FichePER = New PER_PIECE_IDENTITE
                Case VI.ObjetPer.ObaMedical
                    FichePER = New PER_VISITE_MEDICA
                Case VI.ObjetPer.ObaPresence
                    FichePER = New PER_TRAVAIL
                Case VI.ObjetPer.ObaCompetence
                    FichePER = New PER_BREVETPRO
                Case VI.ObjetPer.ObaSanction
                    FichePER = New PER_SANCTION
                Case VI.ObjetPer.ObaDecoration
                    FichePER = New PER_DECORATION
                Case VI.ObjetPer.ObaEligible
                    FichePER = New PER_MANDAT
                Case VI.ObjetPer.ObaRangement
                    FichePER = Nothing
                Case VI.ObjetPer.ObaHandicap
                    FichePER = New PER_HANDICAP
                Case VI.ObjetPer.ObaJournee
                    FichePER = New PER_EVENEMENTJOUR
                Case VI.ObjetPer.ObaValtemps
                    FichePER = New PER_TEMPS_TRAVAIL
                Case VI.ObjetPer.ObaAnnualisation
                    FichePER = New PER_ANNUALISATION
                Case VI.ObjetPer.ObaHebdoCET
                    FichePER = New PER_CET_HEBDO
                Case VI.ObjetPer.ObaAdresseHorsFr
                    FichePER = New PER_HORSFRANCE
                Case VI.ObjetPer.ObaAutAdm
                    FichePER = New PER_AUTORISATION
                Case VI.ObjetPer.ObaOrdreMission
                    FichePER = New PER_ORDREMISSION
                Case VI.ObjetPer.ObaFrais
                    Select Case WsTypeModele
                        Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                            If WsTypeParametre = SensParametre.FraisCES Then
                                FichePER = New PER_DEPLACEMENT_CES
                            Else
                                FichePER = New PER_DEPLACEMENT
                            End If
                        Case Else
                            FichePER = New PER_DEPLACEMENT_PRIVE
                    End Select
                Case VI.ObjetPer.ObaActiAnnexes
                    FichePER = New PER_ACTI_ANNEXES
                Case VI.ObjetPer.ObaAvantage
                    FichePER = New PER_AVANTAGES
                Case VI.ObjetPer.ObaEntretien
                    Select Case WsTypeParametre
                        Case SensParametre.EntretienMagi
                            FichePER = New PER_ENTRETIEN_MAGI
                        Case SensParametre.EntretienCite
                            FichePER = New PER_ENTRETIEN_CITE
                        Case Else
                            FichePER = New PER_ENTRETIEN
                    End Select
                Case VI.ObjetPer.ObaDocuments
                    FichePER = New PER_DOCUMENTS
                Case VI.ObjetPer.ObaAppreciation
                    FichePER = Nothing
                Case VI.ObjetPer.ObaSpecialite
                    FichePER = New PER_SPECIALITE
                Case VI.ObjetPer.ObaStatutDetache
                    Select Case WsTypeModele
                        Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                            FichePER = New PER_STATUT_DETACHE
                        Case Else
                            FichePER = New PER_INTERESSEMENT
                    End Select
                Case VI.ObjetPer.ObaPositionDetache
                    Select Case WsTypeModele
                        Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                            FichePER = New PER_POSITION_DETACHE
                        Case Else
                            FichePER = New PER_STOCKOPTION
                    End Select
                Case VI.ObjetPer.ObaGradeDetache
                    Select Case WsTypeModele
                        Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                            FichePER = New PER_GRADE_DETACHE
                        Case Else
                            FichePER = New PER_PARTICIPATION
                    End Select
                Case VI.ObjetPer.ObaBulletin
                    FichePER = New PER_BULLETIN
                Case VI.ObjetPer.ObaVaccination
                    FichePER = New PER_VACCINATION
                Case VI.ObjetPer.ObaMutuelle
                    FichePER = New PER_MUTUELLE
                Case VI.ObjetPer.ObaNotationDetache
                    FichePER = New PER_NOTATION_DETA
                Case VI.ObjetPer.ObaHeuresSup
                    FichePER = New PER_HEURESUP
                Case VI.ObjetPer.ObaVirtualia
                    FichePER = New PER_COMPTE_RENDU
                Case VI.ObjetPer.ObaLoginIntranet
                    FichePER = New PER_LOGIN
                Case Is = VI.ObjetPer.ObaMessageIntranet
                    FichePER = New PER_MESSAGERIE
                Case VI.ObjetPer.ObaSaisieIntranet
                    FichePER = New PER_MAJINTRANET
                Case 66 'VI.ObjetPer.ObaJourFerie
                    FichePER = New PER_RETRAITE_ENFANTS
                Case VI.ObjetPer.ObaVariablePaie
                    FichePER = New PER_VARIABLE_PAIE
                Case VI.ObjetPer.ObaIntegrationCorps
                    FichePER = New PER_INTEGRATION
                Case VI.ObjetPer.ObaCumulRem
                    FichePER = New PER_CUMUL_EMPLOI
                Case VI.ObjetPer.ObaFIA
                    FichePER = New PER_FIA
                Case VI.ObjetPer.ObaAccesIntranet
                    FichePER = Nothing
                Case VI.ObjetPer.ObaAdresse2
                    FichePER = New PER_DOMICILE_SECOND
                Case VI.ObjetPer.ObaAdresseConge
                    FichePER = New PER_DOMICILE_CONGE
                Case VI.ObjetPer.ObaMateriel
                    FichePER = New PER_MATERIEL
                Case VI.ObjetPer.ObaActiInternes
                    FichePER = New PER_ACTI_INTERNES
                Case VI.ObjetPer.ObaPrevention
                    FichePER = New PER_PREVENTION_RISQUE
                Case VI.ObjetPer.ObaRetraite
                    FichePER = New PER_RETRAITE
                Case VI.ObjetPer.ObaAffectation2nd
                    FichePER = New PER_AFFECTATION_SECOND(NumObjet)
                Case VI.ObjetPer.ObaPointage
                    FichePER = New PER_POINTAGE
                Case VI.ObjetPer.ObaEpargneTemps
                    FichePER = New PER_CET
                Case VI.ObjetPer.ObaDemandeFormation
                    FichePER = New PER_DEMANDE_FORMATION
                Case VI.ObjetPer.ObaPrets
                    FichePER = New PER_PRET
                Case VI.ObjetPer.ObaDecRecrutement
                    Select Case WsTypeModele
                        Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                            FichePER = New PER_DECRECRUTER
                        Case Else
                            FichePER = Nothing
                    End Select
                Case VI.ObjetPer.ObaPaieVacation
                    Select Case WsTypeModele
                        Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                            FichePER = New PER_PAIE_VACATION
                        Case Else
                            FichePER = Nothing
                    End Select
                Case 85
                    Select Case WsTypeModele
                        Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                            FichePER = New PER_BASE_COTISATION
                        Case Else
                            FichePER = New PER_OBJECTIFS
                    End Select
                Case VI.ObjetPer.ObaThese
                    Select Case WsTypeModele
                        Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                            FichePER = New PER_THESE
                        Case Else
                            FichePER = Nothing
                    End Select
                Case 88
                    Select Case WsTypeParametre
                        Case SensParametre.EntretienMagi
                            FichePER = New PER_ENTRETIEN_MAGI2
                    End Select
                Case 89
                    Select Case WsTypeParametre
                        Case SensParametre.EntretienMagi
                            FichePER = New PER_ENTRETIEN_MAGI3
                    End Select
                Case VI.ObjetPer.ObaModifPlanning
                    FichePER = New PER_MODIF_PLANNING
                Case 92 'LOLF - Public
                    Select Case WsTypeModele
                        Case VI.ModeleRh.FPCollectivite, VI.ModeleRh.FPPompier, VI.ModeleRh.FPEtat, VI.ModeleRh.AfriquePublic
                            FichePER = New PER_LOLF
                        Case Else
                            FichePER = Nothing
                    End Select
                Case 93 'DIF
                    FichePER = New PER_DIF
                Case 94
                    FichePER = New PER_CONTEXTEPAIE
                Case 96
                    FichePER = New PER_RETRAITE_BONIFICATION
                Case 97
                    FichePER = New PER_RETRAITE_CATEGORIE
                Case 98
                    FichePER = New PER_RETRAITE_CONGE_HT
                Case 99
                    FichePER = New PER_RETRAITE_SERVICES
                Case VI.ObjetPer.ObaAnnuaire
                    FichePER = New PER_ANNUAIRE
                Case VI.ObjetPer.ObaActiviteMensuelle
                    Select Case WsTypeParametre
                        Case SensParametre.EntretienHonda
                            FichePER = New PER_ACTIVITE_HONDA
                        Case Else
                            FichePER = New PER_MESURE_ONIC
                    End Select
                Case 111
                    FichePER = New PER_EVAL_ACTIVITE
                Case VI.ObjetPer.ObaActiviteJour
                    FichePER = New PER_ACTIVITE_JOUR
                Case VI.ObjetPer.ObaLanguePratiquee
                    FichePER = New PER_LANGUE_PRATIQUEE
                Case VI.ObjetPer.ObaTOEIC
                    FichePER = New PER_TOEIC
                Case VI.ObjetPer.ObaDetailHS
                    FichePER = New PER_DETAIL_HS
                Case VI.ObjetPer.ObaMensuelHS
                    FichePER = New PER_MENSUEL_HS
                Case 78
                    FichePER = New PER_AFFECTATION_SECOND
                Case 141
                    FichePER = New PER_AFFECTATION_TROIS
                Case 142
                    FichePER = New PER_AFFECTATION_QUATRE
                Case 143
                    FichePER = New PER_AFFECTATION_CINQ
                Case 144
                    FichePER = New PER_AFFECTATION_SIX
                Case VI.ObjetPer.ObaEntretienPro
                    Select Case WsTypeParametre
                        Case SensParametre.EntretienHonda
                            FichePER = New PER_ENTRETIEN_HONDA_GENERAL
                        Case Else
                            FichePER = New PER_ENTRETIEN_GENERAL
                    End Select
                Case VI.ObjetPer.ObaEntretienResultat
                    Select Case WsTypeParametre
                        Case SensParametre.EntretienHonda
                            FichePER = New PER_ENTRETIEN_HONDA_PERFORMANCE
                        Case Else
                            FichePER = New PER_ENTRETIEN_RESULTAT
                    End Select
                Case VI.ObjetPer.ObaEntretienObjectif
                    Select Case WsTypeParametre
                        Case SensParametre.EntretienHonda
                            FichePER = New PER_ENTRETIEN_HONDA_COMPORTEMENT
                        Case Else
                            FichePER = New PER_ENTRETIEN_OBJECTIF
                    End Select
                Case VI.ObjetPer.ObaEntretienServir
                    Select Case WsTypeParametre
                        Case SensParametre.EntretienHonda
                            FichePER = New PER_ENTRETIEN_HONDA_FORMATION
                        Case Else
                            FichePER = New PER_ENTRETIEN_COMPETENCE
                    End Select
                Case VI.ObjetPer.ObaEntretienObs
                    FichePER = New PER_ENTRETIEN_OBSERVATION
                Case VI.ObjetPer.ObaEntretienFormation
                    FichePER = New PER_ENTRETIEN_FORMATION
                Case VI.ObjetPer.ObaEntretienSynthese
                    FichePER = New PER_ENTRETIEN_SYNTHESE
                Case VI.ObjetPer.ObaEntretienComplement
                    FichePER = New PER_ENTRETIEN_COMPLEMENT
                Case VI.ObjetPer.ObaPaieGEST
                    FichePER = New PER_PAIE_GEST
                Case VI.ObjetPer.ObaCIR
                    FichePER = New PER_CIR
                Case VI.ObjetPer.ObaPaieBASE
                    FichePER = New PER_PAIE_BASE
                Case 165
                    FichePER = New PER_PAIE_GEST2
                Case 166
                    FichePER = New PER_PAIE_BASE2
                Case 167
                    FichePER = New PER_PAIE_GEST3
                Case 168
                    FichePER = New PER_PAIE_BASE3
                Case VI.ObjetPer.ObaDemandeAcompte
                    FichePER = New PER_DEMANDE_ACOMPTE
                Case 171
                    FichePER = New PER_JOUR_ASTREINTE
                Case VI.ObjetPer.ObaSIFAC
                    FichePER = New PER_SIFAC
                Case 182
                    FichePER = New PER_REGIME_AFFECTATION
                Case 185
                    FichePER = New PER_DOTATION_CES
            End Select

            If FichePER Is Nothing Then
                Return Nothing
            End If
            If LstDatas Is Nothing Then
                FichePER.Ide_Dossier = IdeDossier
                Return FichePER
            End If
            Dim IndiceI As Integer
            Dim ChaineDatas As New System.Text.StringBuilder
            ChaineDatas.Append(IdeDossier.ToString & VI.Tild)
            For IndiceI = 0 To LstDatas.Count - 1
                If IndiceI = LstDatas.Count - 1 Then
                    ChaineDatas.Append(LstDatas(IndiceI))
                Else
                    ChaineDatas.Append(LstDatas(IndiceI) & VI.Tild)
                End If
            Next IndiceI
            FichePER.ContenuTable = ChaineDatas.ToString
            Return FichePER
        End Function

        Public Sub New(ByVal Modele As Integer)
            WsTypeModele = Modele
        End Sub

        Public Function V_NouvelleFiche(NumObjet As Integer, IdeDossier As Integer) As Systeme.MetaModele.VIR_FICHE Implements Systeme.IVConstructeur.V_NouvelleFiche
            Return V_NouvelleFiche(NumObjet, IdeDossier, Nothing)
        End Function

        Public WriteOnly Property V_Parametre As Integer Implements Systeme.IVConstructeur.V_Parametre
            Set(value As Integer)
                WsTypeParametre = value
            End Set
        End Property

        Public WriteOnly Property V_PointdeVue As Integer Implements Systeme.IVConstructeur.V_PointdeVue
            Set(value As Integer)
                WsPointdeVue = 1
            End Set
        End Property

        Public WriteOnly Property V_TypeduModele As Integer Implements Systeme.IVConstructeur.V_TypeduModele
            Set(value As Integer)
                WsTypeModele = value
            End Set
        End Property
    End Class

End Namespace

