Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_ADMORIGINE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsAdministration As String
        Private WsDate_d_entree_fonction_publiqu As String
        Private WsDate_d_entree_fp_etat As String
        Private WsDate_d_entree_fp_territoriale As String
        Private WsDate_d_entree_fp_hospitaliere As String
        Private WsAnneevalideeeffectif As Integer = 0
        Private WsMoisvalideseffectif As Integer = 0
        Private WsJoursvalideseffectif As Integer = 0
        Private WsSens_validation As String
        Private WsCorrespondances As String
        Private WsEcolefonctionnaire As String
        Private WsDate_d_entree_ecole As String
        Private WsDate_de_sortie_ecole As String
        Private WsAnneevalideepublic As Integer = 0
        Private WsMoisvalidespublic As Integer = 0
        Private WsJoursvalidespublic As Integer = 0
        Private WsAnneevalideeprive As Integer = 0
        Private WsMoisvalidesprive As Integer = 0
        Private WsJoursvalidesprive As Integer = 0
        Private WsAnneevalideeretraite As Integer = 0
        Private WsMoisvalidesretraite As Integer = 0
        Private WsJoursvalidesretraite As Integer = 0
        Private WsCorps_d_origine As String
        Private WsSimobiliteaccomplie As String
        Private WsDatearretevalidationmob As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ADMORIGINE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaOrigine
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 26
            End Get
        End Property

        Public Property Administration() As String
            Get
                Return WsAdministration
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsAdministration = value
                    Case Else
                        WsAdministration = Strings.Left(value, 120)
                End Select
                MyBase.Clef = WsAdministration
            End Set
        End Property

        Public Property Date_d_entree_fonction_publiqu() As String
            Get
                Return WsDate_d_entree_fonction_publiqu
            End Get
            Set(ByVal value As String)
                WsDate_d_entree_fonction_publiqu = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_d_entree_fp_etat() As String
            Get
                Return WsDate_d_entree_fp_etat
            End Get
            Set(ByVal value As String)
                WsDate_d_entree_fp_etat = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_d_entree_fp_territoriale() As String
            Get
                Return WsDate_d_entree_fp_territoriale
            End Get
            Set(ByVal value As String)
                WsDate_d_entree_fp_territoriale = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_d_entree_fp_hospitaliere() As String
            Get
                Return WsDate_d_entree_fp_hospitaliere
            End Get
            Set(ByVal value As String)
                WsDate_d_entree_fp_hospitaliere = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Anneevalideeeffectif() As Integer
            Get
                Return WsAnneevalideeeffectif
            End Get
            Set(ByVal value As Integer)
                WsAnneevalideeeffectif = value
            End Set
        End Property

        Public Property Moisvalideseffectif() As Integer
            Get
                Return WsMoisvalideseffectif
            End Get
            Set(ByVal value As Integer)
                WsMoisvalideseffectif = value
            End Set
        End Property

        Public Property Joursvalideseffectif() As Integer
            Get
                Return WsJoursvalideseffectif
            End Get
            Set(ByVal value As Integer)
                WsJoursvalideseffectif = value
            End Set
        End Property

        Public Property Sens_validation() As String
            Get
                Return WsSens_validation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsSens_validation = value
                    Case Else
                        WsSens_validation = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Correspondances() As String
            Get
                Return WsCorrespondances
            End Get
            Set(ByVal value As String)
                WsCorrespondances = value
            End Set
        End Property

        Public Property Ecolefonctionnaire() As String
            Get
                Return WsEcolefonctionnaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsEcolefonctionnaire = value
                    Case Else
                        WsEcolefonctionnaire = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Date_d_entree_ecole() As String
            Get
                Return WsDate_d_entree_ecole
            End Get
            Set(ByVal value As String)
                WsDate_d_entree_ecole = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_sortie_ecole() As String
            Get
                Return WsDate_de_sortie_ecole
            End Get
            Set(ByVal value As String)
                WsDate_de_sortie_ecole = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Anneevalideepublic() As Integer
            Get
                Return WsAnneevalideepublic
            End Get
            Set(ByVal value As Integer)
                WsAnneevalideepublic = value
            End Set
        End Property

        Public Property Moisvalidespublic() As Integer
            Get
                Return WsMoisvalidespublic
            End Get
            Set(ByVal value As Integer)
                WsMoisvalidespublic = value
            End Set
        End Property

        Public Property Joursvalidespublic() As Integer
            Get
                Return WsJoursvalidespublic
            End Get
            Set(ByVal value As Integer)
                WsJoursvalidespublic = value
            End Set
        End Property

        Public Property Anneevalideeprive() As Integer
            Get
                Return WsAnneevalideeprive
            End Get
            Set(ByVal value As Integer)
                WsAnneevalideeprive = value
            End Set
        End Property

        Public Property Moisvalidesprive() As Integer
            Get
                Return WsMoisvalidesprive
            End Get
            Set(ByVal value As Integer)
                WsMoisvalidesprive = value
            End Set
        End Property

        Public Property Joursvalidesprive() As Integer
            Get
                Return WsJoursvalidesprive
            End Get
            Set(ByVal value As Integer)
                WsJoursvalidesprive = value
            End Set
        End Property

        Public Property Anneevalideeretraite() As Integer
            Get
                Return WsAnneevalideeretraite
            End Get
            Set(ByVal value As Integer)
                WsAnneevalideeretraite = value
            End Set
        End Property

        Public Property Moisvalidesretraite() As Integer
            Get
                Return WsMoisvalidesretraite
            End Get
            Set(ByVal value As Integer)
                WsMoisvalidesretraite = value
            End Set
        End Property

        Public Property Joursvalidesretraite() As Integer
            Get
                Return WsJoursvalidesretraite
            End Get
            Set(ByVal value As Integer)
                WsJoursvalidesretraite = value
            End Set
        End Property

        Public Property Corps_d_origine() As String
            Get
                Return WsCorps_d_origine
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsCorps_d_origine = value
                    Case Else
                        WsCorps_d_origine = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Simobiliteaccomplie() As String
            Get
                Return WsSimobiliteaccomplie
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsSimobiliteaccomplie = ""
                    Case Is = "0", "Non"
                        WsSimobiliteaccomplie = "Non"
                    Case Else
                        WsSimobiliteaccomplie = "Oui"
                End Select
            End Set
        End Property

        Public Property Datearretevalidationmob() As String
            Get
                Return WsDatearretevalidationmob
            End Get
            Set(ByVal value As String)
                WsDatearretevalidationmob = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Administration & VI.Tild)
                Chaine.Append(Date_d_entree_fonction_publiqu & VI.Tild)
                Chaine.Append(Date_d_entree_fp_etat & VI.Tild)
                Chaine.Append(Date_d_entree_fp_territoriale & VI.Tild)
                Chaine.Append(Date_d_entree_fp_hospitaliere & VI.Tild)
                Chaine.Append(Anneevalideeeffectif.ToString & VI.Tild)
                Chaine.Append(Moisvalideseffectif.ToString & VI.Tild)
                Chaine.Append(Joursvalideseffectif.ToString & VI.Tild)
                Chaine.Append(Sens_validation & VI.Tild)
                Chaine.Append(Correspondances & VI.Tild)
                Chaine.Append(Ecolefonctionnaire & VI.Tild)
                Chaine.Append(Date_d_entree_ecole & VI.Tild)
                Chaine.Append(Date_de_sortie_ecole & VI.Tild)
                Chaine.Append(Anneevalideepublic.ToString & VI.Tild)
                Chaine.Append(Moisvalidespublic.ToString & VI.Tild)
                Chaine.Append(Joursvalidespublic.ToString & VI.Tild)
                Chaine.Append(Anneevalideeprive.ToString & VI.Tild)
                Chaine.Append(Moisvalidesprive.ToString & VI.Tild)
                Chaine.Append(Joursvalidesprive.ToString & VI.Tild)
                Chaine.Append(Anneevalideeretraite.ToString & VI.Tild)
                Chaine.Append(Moisvalidesretraite.ToString & VI.Tild)
                Chaine.Append(Joursvalidesretraite.ToString & VI.Tild)
                Chaine.Append(Corps_d_origine & VI.Tild)
                Chaine.Append(Simobiliteaccomplie & VI.Tild)
                Chaine.Append(Datearretevalidationmob & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 27 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Administration = TableauData(1)
                Date_d_entree_fonction_publiqu = TableauData(2)
                Date_d_entree_fp_etat = TableauData(3)
                Date_d_entree_fp_territoriale = TableauData(4)
                Date_d_entree_fp_hospitaliere = TableauData(5)
                If TableauData(6) = "" Then TableauData(6) = "0"
                Anneevalideeeffectif = CInt(TableauData(6))
                If TableauData(7) = "" Then TableauData(7) = "0"
                Moisvalideseffectif = CInt(TableauData(7))
                If TableauData(8) = "" Then TableauData(8) = "0"
                Joursvalideseffectif = CInt(TableauData(8))
                Sens_validation = TableauData(9)
                Correspondances = TableauData(10)
                Ecolefonctionnaire = TableauData(11)
                Date_d_entree_ecole = TableauData(12)
                Date_de_sortie_ecole = TableauData(13)
                For IndiceI = 14 To 22
                    If TableauData(IndiceI) = "" Then TableauData(IndiceI) = "0"
                Next IndiceI
                Anneevalideepublic = CInt(TableauData(14))
                Moisvalidespublic = CInt(TableauData(15))
                Joursvalidespublic = CInt(TableauData(16))
                Anneevalideeprive = CInt(TableauData(17))
                Moisvalidesprive = CInt(TableauData(18))
                Joursvalidesprive = CInt(TableauData(19))
                Anneevalideeretraite = CInt(TableauData(20))
                Moisvalidesretraite = CInt(TableauData(21))
                Joursvalidesretraite = CInt(TableauData(22))
                Corps_d_origine = TableauData(23)
                Simobiliteaccomplie = TableauData(24)
                Datearretevalidationmob = TableauData(25)
                MyBase.Certification = TableauData(26)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<Administration>" & Administration & "</Administration>" & vbCrLf)
                Chaine.Append("<DateEntreeFP>" & Date_d_entree_fonction_publiqu & "</DateEntreeFP>" & vbCrLf)
                Chaine.Append("<DateEntreeFPEtat>" & Date_d_entree_fp_etat & "</DateEntreeFPEtat>" & vbCrLf)
                Chaine.Append("<DateEntreeFPTerritoriale>" & Date_d_entree_fp_territoriale & "</DateEntreeFPTerritoriale>" & vbCrLf)
                Chaine.Append("<DateEntreeFPHospitaliere>" & Date_d_entree_fp_hospitaliere & "</DateEntreeFPHospitaliere>" & vbCrLf)

                Return Chaine.ToString
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

