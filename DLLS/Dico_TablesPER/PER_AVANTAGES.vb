﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_AVANTAGES
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_d_effet As String
        Private WsNature As String
        Private WsMontantImposable As Double
        Private WsObservations As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_AVANTAGES"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaAvantage
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 5
            End Get
        End Property

        Public Property Date_d_effet() As String
            Get
                Return WsDate_d_effet
            End Get
            Set(ByVal value As String)
                WsDate_d_effet = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDate_d_effet
            End Set
        End Property

        Public Property NatureAvantage() As String
            Get
                Return WsNature
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsNature = value
                    Case Else
                        WsNature = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsNature
            End Set
        End Property

        Public Property MontantImposable() As Double
            Get
                Return VirRhFonction.ConversionDouble(Strings.Format(WsMontantImposable, "#0.00"))
            End Get
            Set(ByVal value As Double)
                WsMontantImposable = value
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_d_effet & VI.Tild)
                Chaine.Append(NatureAvantage & VI.Tild)
                Chaine.Append(MontantImposable.ToString & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_d_effet = TableauData(1)
                NatureAvantage = TableauData(2)
                If TableauData(3) = "" Then TableauData(3) = "0"
                MontantImposable = VirRhFonction.ConversionDouble(TableauData(3))
                Observations = TableauData(4)
                MyBase.Date_de_Fin = TableauData(5)
                MyBase.Certification = TableauData(6)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace


