﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_THESE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        Private WsSiMajuscule As Boolean = False
        '
        Private WsSujet As String
        Private WsConseillerEtudesN1 As String
        Private WsConseillerEtudesN2 As String
        Private WsDirecteurThese As String
        Private WsCoordonnees As String
        Private WsEcoleDoctorale As String
        Private WsOrganismeInscription As String
        Private WsDate_Soutenance As String
        Private WsCompositionJury As String
        Private WsDate_Interruption As String
        Private WsMotif_Interruption As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_THESE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaThese
            End Get
        End Property

        Public Property Sujet_These() As String
            Get
                Return WsSujet
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 255
                        WsSujet = value
                    Case Else
                        WsSujet = Strings.Left(value, 255)
                End Select
            End Set
        End Property

        Public Property Conseiller_Etudes_N1() As String
            Get
                Return WsConseillerEtudesN1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsConseillerEtudesN1 = value
                    Case Else
                        WsConseillerEtudesN1 = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Conseiller_Etudes_N2() As String
            Get
                Return WsConseillerEtudesN2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsConseillerEtudesN2 = value
                    Case Else
                        WsConseillerEtudesN2 = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Directeur_These() As String
            Get
                Return WsDirecteurThese
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsDirecteurThese = value
                    Case Else
                        WsDirecteurThese = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Coordonnees() As String
            Get
                Return WsCoordonnees
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 255
                        WsCoordonnees = value
                    Case Else
                        WsCoordonnees = Strings.Left(value, 255)
                End Select
            End Set
        End Property

        Public Property EcoleDoctorale() As String
            Get
                Return WsEcoleDoctorale
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsEcoleDoctorale = value
                    Case Else
                        WsEcoleDoctorale = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Organisme_Inscription() As String
            Get
                Return WsOrganismeInscription
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsOrganismeInscription = value
                    Case Else
                        WsOrganismeInscription = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Date_Soutenance() As String
            Get
                Return WsDate_Soutenance
            End Get
            Set(ByVal value As String)
                WsDate_Soutenance = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Composition_Jury() As String
            Get
                Return WsCompositionJury
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 255
                        WsCompositionJury = value
                    Case Else
                        WsCompositionJury = Strings.Left(value, 255)
                End Select
            End Set
        End Property

        Public Property Date_Interruption() As String
            Get
                Return WsDate_Interruption
            End Get
            Set(ByVal value As String)
                WsDate_Interruption = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Motif_Interruption() As String
            Get
                Return WsMotif_Interruption
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsMotif_Interruption = value
                    Case Else
                        WsMotif_Interruption = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Sujet_These & VI.Tild)
                Chaine.Append(Conseiller_Etudes_N1 & VI.Tild)
                Chaine.Append(Conseiller_Etudes_N2 & VI.Tild)
                Chaine.Append(Directeur_These & VI.Tild)
                Chaine.Append(Coordonnees & VI.Tild)
                Chaine.Append(EcoleDoctorale & VI.Tild)
                Chaine.Append(Organisme_Inscription & VI.Tild)
                Chaine.Append(Date_Soutenance & VI.Tild)
                Chaine.Append(Composition_Jury & VI.Tild)
                Chaine.Append(Date_Interruption & VI.Tild)
                Chaine.Append(Motif_Interruption)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 12 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Sujet_These = TableauData(1)
                Conseiller_Etudes_N1 = TableauData(2)
                Conseiller_Etudes_N2 = TableauData(3)
                Directeur_These = TableauData(4)
                Coordonnees = TableauData(5)
                EcoleDoctorale = TableauData(6)
                Organisme_Inscription = TableauData(7)
                Date_Soutenance = TableauData(8)
                Composition_Jury = TableauData(9)
                Date_Interruption = TableauData(10)
                Motif_Interruption = TableauData(11)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return ""
                    Case 1
                        Return WsSujet
                    Case 2
                        Return WsConseillerEtudesN1
                    Case 3
                        Return WsConseillerEtudesN2
                    Case 4
                        Return WsDirecteurThese
                    Case 5
                        Return WsCoordonnees
                    Case 6
                        Return WsEcoleDoctorale
                    Case 7
                        Return WsOrganismeInscription
                    Case 8
                        Return WsDate_Soutenance
                    Case 9
                        Return WsCompositionJury
                    Case 10
                        Return WsDate_Interruption
                    Case 11
                        Return WsMotif_Interruption
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Sub New(Optional ByVal SiMajuscule As Boolean = False)
            MyBase.New()
            WsSiMajuscule = SiMajuscule
        End Sub

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property
    End Class
End Namespace

