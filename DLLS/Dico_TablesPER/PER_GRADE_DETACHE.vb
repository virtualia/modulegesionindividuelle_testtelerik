﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_GRADE_DETACHE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsGrade As String
        Private WsEchelon As String
        Private WsIndice_brut As String
        Private WsIndice_majore As String
        Private WsCategorie As String
        Private WsCorps As String
        Private WsEchelle_de_remuneration As String
        Private WsReliquat_valorise As Integer = 0
        Private WsDate_de_l_arrete As String
        Private WsReference_de_l_arrete As String
        Private WsMode_d_acces As String
        Private WsFiliere As String
        Private WsGroupe_hierarchique As String
        Private WsDuree As String
        Private WsBase_de_remuneration As String
        Private WsTaux_de_remuneration As String
        Private WsSecteur As String
        Private WsIndice_assimilation As String
        Private WsIndice_remuneration As String
        Private WsNombre10000 As String
        Private WsDate_du_decret As String
        Private WsDate_de_parution As String
        Private WsImpersonnel As String
        Private WsMonnaieCompte As String
        Private WsMonnaiePaie As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_GRADE_DETACHE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaGradeDetache  'Spécifique Secteur Public
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 26
            End Get
        End Property

        Public Property Grade() As String
            Get
                Return WsGrade
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsGrade = value
                    Case Else
                        WsGrade = Strings.Left(value, 120)
                End Select
                MyBase.Clef = WsGrade
            End Set
        End Property

        Public Property Echelon() As String
            Get
                Return WsEchelon
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsEchelon = value
                    Case Else
                        WsEchelon = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Indice_brut() As String
            Get
                Return WsIndice_brut
            End Get
            Set(ByVal value As String)
                WsIndice_brut = value
            End Set
        End Property

        Public Property Indice_majore() As String
            Get
                Return WsIndice_majore
            End Get
            Set(ByVal value As String)
                WsIndice_majore = value
            End Set
        End Property

        Public Property Categorie() As String
            Get
                Return WsCategorie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsCategorie = value
                    Case Else
                        WsCategorie = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Corps() As String
            Get
                Return WsCorps
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsCorps = value
                    Case Else
                        WsCorps = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Echelle_de_remuneration() As String
            Get
                Return WsEchelle_de_remuneration
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsEchelle_de_remuneration = value
                    Case Else
                        WsEchelle_de_remuneration = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Reliquat_valorise() As Integer
            Get
                Return WsReliquat_valorise
            End Get
            Set(ByVal value As Integer)
                WsReliquat_valorise = value
            End Set
        End Property

        Public Property Date_de_l_arrete() As String
            Get
                Return WsDate_de_l_arrete
            End Get
            Set(ByVal value As String)
                WsDate_de_l_arrete = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Reference_de_l_arrete() As String
            Get
                Return WsReference_de_l_arrete
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsReference_de_l_arrete = value
                    Case Else
                        WsReference_de_l_arrete = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Mode_d_acces() As String
            Get
                Return WsMode_d_acces
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsMode_d_acces = value
                    Case Else
                        WsMode_d_acces = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property Filiere() As String
            Get
                Return WsFiliere
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsFiliere = value
                    Case Else
                        WsFiliere = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Groupe_hierarchique() As String
            Get
                Return WsGroupe_hierarchique
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsGroupe_hierarchique = value
                    Case Else
                        WsGroupe_hierarchique = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Duree() As String
            Get
                Return WsDuree
            End Get
            Set(ByVal value As String)
                WsDuree = value
            End Set
        End Property

        Public Property Base_de_remuneration() As String
            Get
                Return WsBase_de_remuneration
            End Get
            Set(ByVal value As String)
                WsBase_de_remuneration = VirRhFonction.VirgulePoint(value)
            End Set
        End Property

        Public Property Taux_de_remuneration() As String
            Get
                Return WsTaux_de_remuneration
            End Get
            Set(ByVal value As String)
                WsTaux_de_remuneration = VirRhFonction.VirgulePoint(value)
            End Set
        End Property

        Public Property Secteur() As String
            Get
                Return WsSecteur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsSecteur = value
                    Case Else
                        WsSecteur = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Indice_assimilation() As String
            Get
                Return WsIndice_assimilation
            End Get
            Set(ByVal value As String)
                WsIndice_assimilation = value
            End Set
        End Property

        Public Property Indice_remuneration() As String
            Get
                Return WsIndice_remuneration
            End Get
            Set(ByVal value As String)
                WsIndice_remuneration = value
            End Set
        End Property

        Public Property Nombre10000() As String
            Get
                Return WsNombre10000
            End Get
            Set(ByVal value As String)
                WsNombre10000 = value
            End Set
        End Property

        Public Property Date_du_decret() As String
            Get
                Return WsDate_du_decret
            End Get
            Set(ByVal value As String)
                WsDate_du_decret = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_parution() As String
            Get
                Return WsDate_de_parution
            End Get
            Set(ByVal value As String)
                WsDate_de_parution = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Impersonnel() As String
            Get
                Return WsImpersonnel
            End Get
            Set(ByVal value As String)
                WsImpersonnel = value
            End Set
        End Property

        Public Property Monnaie_de_Compte() As String
            Get
                Return WsMonnaieCompte
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsMonnaieCompte = value
                    Case Else
                        WsMonnaieCompte = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Monnaie_de_Paiement() As String
            Get
                Return WsMonnaiePaie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsMonnaiePaie = value
                    Case Else
                        WsMonnaiePaie = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Grade & VI.Tild)
                Chaine.Append(Echelon & VI.Tild)
                Chaine.Append(Indice_brut & VI.Tild)
                Chaine.Append(Indice_majore & VI.Tild)
                Chaine.Append(Categorie & VI.Tild)
                Chaine.Append(Corps & VI.Tild)
                Chaine.Append(Echelle_de_remuneration & VI.Tild)
                Chaine.Append(Reliquat_valorise.ToString & VI.Tild)
                Chaine.Append(Date_de_l_arrete & VI.Tild)
                Chaine.Append(Reference_de_l_arrete & VI.Tild)
                Chaine.Append(Mode_d_acces & VI.Tild)
                Chaine.Append(Filiere & VI.Tild)
                Chaine.Append(Groupe_hierarchique & VI.Tild)
                Chaine.Append(VI.Tild) 'Durée
                Chaine.Append(Base_de_remuneration & VI.Tild)
                Chaine.Append(Taux_de_remuneration & VI.Tild)
                Chaine.Append(Secteur & VI.Tild)
                Chaine.Append(Indice_assimilation & VI.Tild)
                Chaine.Append(Indice_remuneration & VI.Tild)
                Chaine.Append(Nombre10000 & VI.Tild)
                Chaine.Append(Date_du_decret & VI.Tild)
                Chaine.Append(Date_de_parution & VI.Tild)
                Chaine.Append(Impersonnel & VI.Tild)
                Chaine.Append(Monnaie_de_Compte & VI.Tild)
                Chaine.Append(Monnaie_de_Paiement & VI.Tild)
                Chaine.Append(MyBase.Certification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 28 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Grade = TableauData(2)
                Echelon = TableauData(3)
                Indice_brut = TableauData(4)
                Indice_majore = TableauData(5)
                Categorie = TableauData(6)
                Corps = TableauData(7)
                Echelle_de_remuneration = TableauData(8)
                If TableauData(9) = "" Then TableauData(9) = "0"
                Reliquat_valorise = CInt(TableauData(9))
                Date_de_l_arrete = TableauData(10)
                Reference_de_l_arrete = TableauData(11)
                Mode_d_acces = TableauData(12)
                Filiere = TableauData(13)
                Groupe_hierarchique = TableauData(14)
                Duree = TableauData(15)
                Base_de_remuneration = TableauData(16)
                Taux_de_remuneration = TableauData(17)
                Secteur = TableauData(18)
                Indice_assimilation = TableauData(19)
                Indice_remuneration = TableauData(20)
                Nombre10000 = TableauData(21)
                Date_du_decret = TableauData(22)
                Date_de_parution = TableauData(23)
                Impersonnel = TableauData(24)
                Monnaie_de_Compte = TableauData(25)
                Monnaie_de_Paiement = TableauData(26)
                MyBase.Certification = TableauData(27)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

