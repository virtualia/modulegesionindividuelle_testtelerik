﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_CONTEXTEPAIE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsContexte01 As String
        Private WsContexte02 As String
        Private WsContexte03 As String
        Private WsContexte04 As String
        Private WsContexte05 As String
        Private WsContexte06 As String
        Private WsContexte07 As String
        Private WsContexte08 As String
        Private WsContexte09 As String
        Private WsContexte10 As String
        Private WsContexte11 As String
        Private WsContexte12 As String
        Private WsContexte13 As String
        Private WsContexte14 As String
        Private WsContexte15 As String
        Private WsContexte16 As String
        Private WsContexte17 As String
        Private WsContexte18 As String
        Private WsContexte19 As String
        Private WsContexte20 As String
        Private WsContexte21 As String
        Private WsContexte22 As String
        Private WsContexte23 As String
        Private WsContexte24 As String
        Private WsContexte25 As String
        Private WsIde_paie As Integer = 0
        Private WsBudget As String
        Private WsEmployeur As String
        Private WsEtablissement As String
        Private WsNoUrssaf As String
        Private WsNoSiret As String
        Private WsCumulBrut As Double = 0
        Private WsCumulImposable As Double = 0
        Private WsCumulBaseSS As Double = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_CONTEXTEPAIE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaContextePaie
            End Get
        End Property

        Public Property Contexte01() As String
            Get
                Return WsContexte01
            End Get
            Set(ByVal value As String)
                WsContexte01 = F_FormatAlpha(value, 50)
                If WsContexte01 = "" Then
                    WsContexte01 = "(Non renseigné)"
                End If
                MyBase.Clef = WsContexte01
            End Set
        End Property

        Public Property Contexte02() As String
            Get
                Return WsContexte02
            End Get
            Set(ByVal value As String)
                If IsNumeric(value) Then
                    value = CInt(value).ToString
                End If
                WsContexte02 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte03() As String
            Get
                Return WsContexte03
            End Get
            Set(ByVal value As String)
                WsContexte03 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte04() As String
            Get
                Return WsContexte04
            End Get
            Set(ByVal value As String)
                WsContexte04 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte05() As String
            Get
                Return WsContexte05
            End Get
            Set(ByVal value As String)
                WsContexte05 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte06() As String
            Get
                Return WsContexte06
            End Get
            Set(ByVal value As String)
                WsContexte06 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte07() As String
            Get
                Return WsContexte07
            End Get
            Set(ByVal value As String)
                WsContexte07 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte08() As String
            Get
                Return WsContexte08
            End Get
            Set(ByVal value As String)
                WsContexte08 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte09() As String
            Get
                Return WsContexte09
            End Get
            Set(ByVal value As String)
                WsContexte09 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte10() As String
            Get
                Return WsContexte10
            End Get
            Set(ByVal value As String)
                WsContexte10 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte11() As String
            Get
                Return WsContexte11
            End Get
            Set(ByVal value As String)
                WsContexte11 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte12() As String
            Get
                Return WsContexte12
            End Get
            Set(ByVal value As String)
                WsContexte12 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte13() As String
            Get
                Return WsContexte13
            End Get
            Set(ByVal value As String)
                WsContexte13 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte14() As String
            Get
                Return WsContexte14
            End Get
            Set(ByVal value As String)
                WsContexte14 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte15() As String
            Get
                Return WsContexte15
            End Get
            Set(ByVal value As String)
                WsContexte15 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte16() As String
            Get
                Return WsContexte16
            End Get
            Set(ByVal value As String)
                WsContexte16 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte17() As String
            Get
                Return WsContexte17
            End Get
            Set(ByVal value As String)
                WsContexte17 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte18() As String
            Get
                Return WsContexte18
            End Get
            Set(ByVal value As String)
                WsContexte18 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte19() As String
            Get
                Return WsContexte19
            End Get
            Set(ByVal value As String)
                WsContexte19 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte20() As String
            Get
                Return WsContexte20
            End Get
            Set(ByVal value As String)
                WsContexte20 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte21() As String
            Get
                Return WsContexte21
            End Get
            Set(ByVal value As String)
                WsContexte21 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte22() As String
            Get
                Return WsContexte22
            End Get
            Set(ByVal value As String)
                WsContexte22 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte23() As String
            Get
                Return WsContexte23
            End Get
            Set(ByVal value As String)
                WsContexte23 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte24() As String
            Get
                Return WsContexte24
            End Get
            Set(ByVal value As String)
                WsContexte24 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Contexte25() As String
            Get
                Return WsContexte25
            End Get
            Set(ByVal value As String)
                WsContexte25 = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Ide_paie() As Integer
            Get
                Return WsIde_paie
            End Get
            Set(ByVal value As Integer)
                WsIde_paie = value
            End Set
        End Property

        Public Property Budget() As String
            Get
                Return WsBudget
            End Get
            Set(ByVal value As String)
                WsBudget = F_FormatAlpha(value, 50)
                If WsBudget = "" Then
                    WsBudget = Contexte21
                End If
            End Set
        End Property

        Public Property Employeur() As String
            Get
                Return WsEmployeur
            End Get
            Set(ByVal value As String)
                WsEmployeur = F_FormatAlpha(value, 50)
                If WsEmployeur = "" Then
                    WsEmployeur = Contexte22
                End If
            End Set
        End Property

        Public Property Etablissement() As String
            Get
                Return WsEtablissement
            End Get
            Set(ByVal value As String)
                WsEtablissement = F_FormatAlpha(value, 50)
                If WsEtablissement = "" Then
                    WsEtablissement = Contexte21
                End If
            End Set
        End Property

        Public Property Numero_Siret() As String
            Get
                Return WsNoSiret
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNoSiret = value
                    Case Else
                        WsNoSiret = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Numero_Urssaf() As String
            Get
                Return WsNoUrssaf
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNoUrssaf = value
                    Case Else
                        WsNoUrssaf = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Cumul_Brut() As Double
            Get
                Return WsCumulBrut
            End Get
            Set(ByVal value As Double)
                WsCumulBrut = value
            End Set
        End Property

        Public Property Cumul_Imposable() As Double
            Get
                Return WsCumulImposable
            End Get
            Set(ByVal value As Double)
                WsCumulImposable = value
            End Set
        End Property

        Public Property Cumul_BaseSS() As Double
            Get
                Return WsCumulBaseSS
            End Get
            Set(ByVal value As Double)
                WsCumulBaseSS = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Contexte01 & VI.Tild)
                Chaine.Append(Contexte02 & VI.Tild)
                Chaine.Append(Contexte03 & VI.Tild)
                Chaine.Append(Contexte04 & VI.Tild)
                Chaine.Append(Contexte05 & VI.Tild)
                Chaine.Append(Contexte06 & VI.Tild)
                Chaine.Append(Contexte07 & VI.Tild)
                Chaine.Append(Contexte08 & VI.Tild)
                Chaine.Append(Contexte09 & VI.Tild)
                Chaine.Append(Contexte10 & VI.Tild)
                Chaine.Append(Contexte11 & VI.Tild)
                Chaine.Append(Contexte12 & VI.Tild)
                Chaine.Append(Contexte13 & VI.Tild)
                Chaine.Append(Contexte14 & VI.Tild)
                Chaine.Append(Contexte15 & VI.Tild)
                Chaine.Append(Contexte16 & VI.Tild)
                Chaine.Append(Contexte17 & VI.Tild)
                Chaine.Append(Contexte18 & VI.Tild)
                Chaine.Append(Contexte19 & VI.Tild)
                Chaine.Append(Contexte20 & VI.Tild)
                Chaine.Append(Contexte21 & VI.Tild)
                Chaine.Append(Contexte22 & VI.Tild)
                Chaine.Append(Contexte23 & VI.Tild)
                Chaine.Append(Contexte24 & VI.Tild)
                Chaine.Append(Contexte25 & VI.Tild)
                Select Case Ide_paie
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(Ide_paie.ToString & VI.Tild)
                End Select
                Chaine.Append(Budget & VI.Tild)
                Chaine.Append(Employeur & VI.Tild)
                Chaine.Append(Etablissement & VI.Tild)
                Chaine.Append(Numero_Siret & VI.Tild)
                Chaine.Append(Numero_Urssaf & VI.Tild)
                Select Case Cumul_Brut
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Cumul_Brut.ToString) & VI.Tild)
                End Select
                Select Case Cumul_Imposable
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Cumul_Imposable.ToString) & VI.Tild)
                End Select
                Select Case Cumul_BaseSS
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Cumul_BaseSS.ToString))
                End Select
                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer
                Dim ChaineW As String

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 28 Then
                    Exit Property
                End If

                MyBase.Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Contexte01 = TableauData(2)
                Contexte02 = TableauData(3)
                Contexte03 = TableauData(4)
                Contexte04 = TableauData(5)
                Contexte05 = TableauData(6)
                Contexte06 = TableauData(7)
                Contexte07 = TableauData(8)
                ChaineW = TableauData(8).Replace("0", "")
                ChaineW = ChaineW.Replace(",", "")
                If ChaineW.Trim = "" Then
                    Contexte07 = ""
                End If
                Contexte08 = TableauData(9)
                Contexte09 = TableauData(10)
                Contexte10 = TableauData(11)
                Contexte11 = TableauData(12)
                Contexte12 = TableauData(13)
                Contexte13 = TableauData(14)
                Contexte14 = TableauData(15)
                Contexte15 = TableauData(16)
                Contexte16 = TableauData(17)
                Contexte17 = TableauData(18)
                Contexte18 = TableauData(19)
                Contexte19 = TableauData(20)
                Contexte20 = TableauData(21)
                Contexte21 = TableauData(22)
                Contexte22 = TableauData(23)
                Contexte23 = TableauData(24)
                Contexte24 = TableauData(25)
                Contexte25 = TableauData(26)
                If TableauData(27) <> "" Then Ide_paie = VirRhFonction.ConversionDouble(TableauData(27))
                If TableauData.Count > 34 Then
                    Budget = TableauData(28)
                    Employeur = TableauData(29)
                    Etablissement = TableauData(30)
                    Numero_Siret = TableauData(31)
                    Numero_Urssaf = TableauData(32)
                    If TableauData(33) <> "" Then Cumul_Brut = VirRhFonction.ConversionDouble(TableauData(33))
                    If TableauData(34) <> "" Then Cumul_Imposable = VirRhFonction.ConversionDouble(TableauData(34))
                    If TableauData(35) <> "" Then Cumul_BaseSS = VirRhFonction.ConversionDouble(TableauData(35))
                End If

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_NumDos As String
            Get
                If Contexte23.Length = 2 AndAlso IsNumeric(Contexte23) Then
                    Return Contexte23
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property V_QualiteNometPrenom As String
            Get
                Return Contexte24
            End Get
        End Property

        Public ReadOnly Property V_NIRComplet As String
            Get
                Dim Chaine As String
                Chaine = Contexte25.Replace(Strings.Space(1), "")
                Return Chaine
            End Get
        End Property

        Public ReadOnly Property V_NumeroetNomRue As String
            Get
                Return Contexte10
            End Get
        End Property

        Public ReadOnly Property V_ComplementAdresse As String
            Get
                Select Case Ide_paie
                    Case VI.IdentifiantPaie.GAPAIE
                        If Contexte11 = "" Then
                            Contexte11 = Contexte12
                            Contexte12 = ""
                        End If
                End Select
                Return Contexte11
            End Get
        End Property

        Public ReadOnly Property V_CodePostal_Ville As String
            Get
                Select Case Ide_paie
                    Case VI.IdentifiantPaie.GAPAIE
                        Return Contexte13
                End Select
                If Contexte12 = "" Then
                    Return Contexte13
                Else
                    Return Contexte12 & Strings.Space(1) & Contexte13
                End If
            End Get
        End Property

        Public ReadOnly Property V_NbEnfants_ACharge As String
            Get
                If IsNumeric(Contexte05) Then
                    Return Contexte05
                End If
                Return ""
            End Get
        End Property

        Public ReadOnly Property V_RIB As String
            Get
                Select Case Ide_paie
                    Case VI.IdentifiantPaie.GAPAIE
                        Dim Chaine As String
                        Chaine = Contexte15 & Strings.Space(1) 'Code Banque
                        Chaine &= Contexte16 & Strings.Space(1) 'Code Guichet
                        Chaine &= Contexte18 'NoCompte
                        Return Chaine
                    Case Else
                        Return Contexte14
                End Select
            End Get
        End Property

        Public ReadOnly Property V_Domiciliation_Bancaire As String
            Get
                Select Case Ide_paie
                    Case VI.IdentifiantPaie.GAPAIE
                        Return Contexte17
                    Case Else
                        Return Contexte15
                End Select
            End Get
        End Property

        Public ReadOnly Property V_NoCompte_Bancaire As String
            Get
                Select Case Ide_paie
                    Case VI.IdentifiantPaie.GAPAIE
                        Return Contexte18
                    Case Else
                        Dim TableauTmp(0) As String
                        TableauTmp = Strings.Split(Contexte14, Strings.Space(1))
                        Select Case TableauTmp.Count
                            Case Is = 4
                                Return TableauTmp(2)
                            Case Else
                                Try
                                    Return Strings.Left(Contexte18, Contexte18.Length - 2)
                                Catch ex As Exception
                                    Return ""
                                End Try
                        End Select
                End Select
            End Get
        End Property

        Public ReadOnly Property V_BIC As String
            Get
                Select Case Ide_paie
                    Case VI.IdentifiantPaie.GAPAIE
                        Return ""
                End Select
                Dim TableauTmp(0) As String
                TableauTmp = Strings.Split(Contexte16, Strings.Space(1))
                Select Case TableauTmp.Count
                    Case Is > 1
                        Return TableauTmp(1)
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public ReadOnly Property V_IBAN As String
            Get
                Select Case Ide_paie
                    Case VI.IdentifiantPaie.GAPAIE
                        Return ""
                End Select
                Dim TableauTmp(0) As String
                TableauTmp = Strings.Split(Contexte16, Strings.Space(1))
                Select Case TableauTmp.Count
                    Case Is > 0
                        Return TableauTmp(0)
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public ReadOnly Property V_IndiceTraitement As Integer
            Get
                If IsNumeric(Contexte03) Then
                    Return CInt(Contexte03)
                Else
                    Return 0
                End If
                'Dim TabEtp(0) As String
                'TabEtp = Strings.Split(Contexte07, Strings.Space(1))
                'If IsNumeric(TabEtp(0)) Then
                '    Return CInt(TabEtp(0))
                'End If
                'Return 0
            End Get
        End Property

        Public ReadOnly Property V_Statut As String
            Get
                Select Case Ide_paie
                    Case Is <> VI.IdentifiantPaie.KA_KX
                        Return Contexte19
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public ReadOnly Property V_SitAdm As String
            Get
                Return Contexte21
            End Get
        End Property

        Public ReadOnly Property V_Grade As String
            Get
                Return Contexte01
            End Get
        End Property

        Public ReadOnly Property V_Echelon As String
            Get
                Dim Pos As Integer

                If Strings.InStr(Contexte02, " - ") > 0 Then
                    Pos = Strings.InStr(Contexte02, " - ")
                    Return Strings.Left(Contexte02, Pos - 1)
                Else
                    Return Contexte02
                End If
            End Get
        End Property

        Public ReadOnly Property V_Chevron As String
            Get
                Dim Pos As Integer

                If Strings.InStr(Contexte02, " - ") > 0 Then
                    Pos = Strings.InStr(Contexte02, " - ")
                    Return Strings.Right(Contexte02, Contexte02.Length - Pos - 2)
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property V_NBI As String
            Get
                Return Contexte04
            End Get
        End Property

        Public ReadOnly Property V_Service_Affectation As String
            Get
                Return Contexte22
            End Get
        End Property

        Public ReadOnly Property V_Emploi_Poste As String
            Get
                If Cumul_Imposable = 0 Then ' Ancienne mouture
                    Return ""
                End If
                Return Contexte17
            End Get
        End Property

        Public ReadOnly Property V_Taux_Activite As Single
            Get
                If Contexte07 = "" Then
                    If IsNumeric(Contexte08.Replace(".", ",")) Then
                        Select Case CSng(Contexte08)
                            Case Is = 0
                                Return 100
                            Case Else
                                Return Math.Round(CSng(Contexte08.Replace(".", ",")), 3)
                        End Select
                    Else
                        Return 100
                    End If
                End If
                Dim TabEtp(0) As String
                TabEtp = Strings.Split(Contexte07, Strings.Space(1))
                If TabEtp.Count > 1 Then
                    If IsNumeric(TabEtp(1)) Then
                        Return Math.Round(CSng(TabEtp(1)) / 10, 3)
                    End If
                End If
                Return 100
            End Get
        End Property

        Public ReadOnly Property V_Taux_Remuneration As Single
            Get
                If Contexte07 = "" Then
                    If IsNumeric(Contexte08.Replace(".", ",")) Then
                        Select Case CSng(Contexte08)
                            Case 0
                                Return 100
                            Case 80
                                Return Math.Round(6 / 7 * 100, 3)
                            Case 90
                                Return Math.Round(32 / 35 * 100, 3)
                            Case Else
                                Return Math.Round(CSng(Contexte08.Replace(".", ",")), 3)
                        End Select
                    Else
                        Return 100
                    End If
                End If
                Dim TabEtp(0) As String
                TabEtp = Strings.Split(Contexte07, Strings.Space(1))
                If TabEtp.Count > 2 Then
                    If IsNumeric(TabEtp(2)) Then
                        Return Math.Round(CSng(TabEtp(2)) / 10, 3)
                    End If
                End If
                Return 100
            End Get
        End Property

        Public ReadOnly Property V_KAKX_CodeVba As String
            Get
                If Contexte19 = "" Then
                    Return ""
                End If
                Dim TableauData(0) As String
                TableauData = Strings.Split(Contexte19, VI.Tiret, -1)
                Return TableauData(0)
            End Get
        End Property

        Public ReadOnly Property V_KAKX_IndicePC As String
            Get
                If Contexte19 = "" Then
                    Return ""
                End If
                Dim TableauData(0) As String
                TableauData = Strings.Split(Contexte19, VI.Tiret, -1)
                If TableauData.Count > 1 Then
                    If TableauData(1) = "0000" Then
                        Return ""
                    Else
                        Return TableauData(1)
                    End If
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property V_KAKX_RegimeRemu As String
            Get
                If Contexte19 = "" Then
                    Return ""
                End If
                Dim TableauData(0) As String
                TableauData = Strings.Split(Contexte19, VI.Tiret, -1)
                If TableauData.Count > 2 Then
                    Return TableauData(2)
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property V_KAKX_CodeSS As String
            Get
                If Contexte19 = "" Then
                    Return ""
                End If
                Dim TableauData(0) As String
                TableauData = Strings.Split(Contexte19, VI.Tiret, -1)
                If TableauData.Count > 3 Then
                    Return TableauData(3)
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property V_KAKX_CodeRC As String
            Get
                If Contexte19 = "" Then
                    Return ""
                End If
                Dim TableauData(0) As String
                TableauData = Strings.Split(Contexte19, VI.Tiret, -1)
                If TableauData.Count > 4 Then
                    Return TableauData(4)
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property V_KAKX_CodeMutuelle As String
            Get
                If Contexte19 = "" Then
                    Return ""
                End If
                Dim TableauData(0) As String
                TableauData = Strings.Split(Contexte19, VI.Tiret, -1)
                If TableauData.Count > 5 Then
                    If TableauData(5) = "00" Then
                        Return ""
                    Else
                        Return TableauData(5)
                    End If
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property V_KAKX_GarantieMutuelle As String
            Get
                If Contexte19 = "" Then
                    Return ""
                End If
                Dim TableauData(0) As String
                TableauData = Strings.Split(Contexte19, VI.Tiret, -1)
                If TableauData.Count > 6 Then
                    If TableauData(5) = "00" Then
                        If TableauData(6) = "00" Then
                            Return ""
                        Else
                            Return TableauData(6)
                        End If
                    Else
                        Return TableauData(6)
                    End If
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property V_KAKX_ZoneResidence As String
            Get
                If Contexte19 = "" Then
                    Return ""
                End If
                Dim TableauData(0) As String
                TableauData = Strings.Split(Contexte19, VI.Tiret, -1)
                If TableauData.Count > 7 Then
                    Return TableauData(7)
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property V_KAKX_CDOM_IDA As String
            Get
                If Contexte19 = "" Then
                    Return ""
                End If
                Dim TableauData(0) As String
                TableauData = Strings.Split(Contexte19, VI.Tiret, -1)
                If TableauData.Count > 8 Then
                    Return TableauData(8)
                Else
                    Return ""
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return Date_de_Valeur
                    Case 1
                        Return WsContexte01
                    Case 2
                        Return WsContexte02
                    Case 3
                        Return WsContexte03
                    Case 4
                        Return WsContexte04
                    Case 5
                        Return WsContexte05
                    Case 6
                        Return WsContexte06
                    Case 7
                        Return WsContexte07
                    Case 8
                        Return WsContexte08
                    Case 9
                        Return WsContexte09
                    Case 10
                        Return WsContexte10
                    Case 11
                        Return WsContexte11
                    Case 12
                        Return WsContexte12
                    Case 13
                        Return WsContexte13
                    Case 14
                        Return WsContexte14
                    Case 15
                        Return WsContexte15
                    Case 16
                        Return WsContexte16
                    Case 17
                        Return WsContexte17
                    Case 18
                        Return WsContexte18
                    Case 19
                        Return WsContexte19
                    Case 20
                        Return WsContexte20
                    Case 21
                        Return WsContexte21
                    Case 22
                        Return WsContexte22
                    Case 23
                        Return WsContexte23
                    Case 24
                        Return WsContexte24
                    Case 25
                        Return WsContexte25
                    Case 26
                        Return WsIde_paie.ToString
                    Case 27
                        Return WsBudget
                    Case 28
                        Return WsEmployeur
                    Case 29
                        Return WsEtablissement
                    Case 30
                        Return WsNoSiret
                    Case 31
                        Return WsNoUrssaf
                    Case 32
                        Select Case Cumul_Brut
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Cumul_Brut.ToString)
                        End Select
                    Case 33
                        Select Case Cumul_Imposable
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Cumul_Imposable.ToString)
                        End Select
                    Case 34
                        Select Case Cumul_BaseSS
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Cumul_BaseSS.ToString)
                        End Select
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

