﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_ACTIVITE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsActivite As String
        Private WsHoraireMensuel As Double = 0
        Private WsParticularite As String
        Private WsDuree As String
        Private WsHoraireHebdo As Double = 0
        '
        Private TsDate_de_fin As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ACTIVITE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaActivite 'Spécifique Secteur Privé
            End Get
        End Property

        Public Overrides ReadOnly Property SiACertifier() As Boolean
            Get
                Return True
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroInfoCertification() As Integer
            Get
                Return 7
            End Get
        End Property

        Public Property Activite() As String
            Get
                Return WsActivite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsActivite = value
                    Case Else
                        WsActivite = Strings.Left(value, 80)
                End Select
                MyBase.Clef = WsActivite
            End Set
        End Property

        Public Property HoraireMensuel() As Double
            Get
                Return WsHoraireMensuel
            End Get
            Set(ByVal value As Double)
                WsHoraireMensuel = value
            End Set
        End Property

        Public Property Particularites() As String
            Get
                Return WsParticularite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsParticularite = value
                    Case Else
                        WsParticularite = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Duree() As String
            Get
                Return WsDuree
            End Get
            Set(ByVal value As String)
                WsDuree = value
            End Set
        End Property

        Public Property HoraireHebdomadaire() As Double
            Get
                Return WsHoraireHebdo
            End Get
            Set(ByVal value As Double)
                WsHoraireHebdo = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Activite & VI.Tild)
                Chaine.Append(HoraireMensuel.ToString & VI.Tild)
                Chaine.Append(Particularites & VI.Tild)
                Chaine.Append(Duree & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(HoraireHebdomadaire.ToString & VI.Tild)
                Chaine.Append(MyBase.Certification)
                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 9 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Activite = TableauData(2)
                If TableauData(3) = "" Then TableauData(3) = "0"
                HoraireMensuel = VirRhFonction.ConversionDouble(TableauData(3))
                Particularites = TableauData(4)
                Duree = TableauData(5)
                MyBase.Date_de_Fin = TableauData(6)
                If TableauData(7) = "" Then TableauData(7) = "0"
                HoraireHebdomadaire = VirRhFonction.ConversionDouble(TableauData(7))
                MyBase.Certification = TableauData(8)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = Date_de_fin
            End Set
        End Property

        Public Property VDate_de_fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public ReadOnly Property HoraireMensuel_En_Minutes() As Integer
            Get
                Dim TableauData(0) As String
                TableauData = Strings.Split(VirRhFonction.VirgulePoint(HoraireMensuel), ".", -1)
                If TableauData(0) = "" Then
                    Return 0
                End If
                If TableauData(1) = "" Then
                    TableauData(1) = 0
                End If
                Return (CInt(TableauData(0)) * 60) + (CInt(TableauData(1)) * 6 / 10)
            End Get
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Donnees_Significatives As String
            Get
                Dim TableauData(0) As String
                Dim Chaine As System.Text.StringBuilder
                Dim IndiceI As Integer

                TableauData = Strings.Split(ContenuTable.Replace(VI.PointVirgule, VI.Tiret), VI.Tild)
                Chaine = New System.Text.StringBuilder
                For IndiceI = 0 To TableauData.Count - 1
                    If IndiceI <> NumeroInfoCertification AndAlso TableauData(IndiceI) <> "" Then
                        Select Case Chaine.Length
                            Case > 0
                                Chaine.Append(VI.PointVirgule)
                        End Select
                        Chaine.Append(TableauData(IndiceI))
                    End If
                Next IndiceI
                Return Chaine.ToString
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

