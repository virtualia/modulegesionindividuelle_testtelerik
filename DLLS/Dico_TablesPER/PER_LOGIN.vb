﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_LOGIN
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsIde_Interne As Integer = 0
        Private WsMotdePasse As String
        Private WsDate_Maj_PW As String
        Private WsDate_Acces As String
        Private WsHeure_Acces As String
        Private WsNombre_Acces As Integer = 0
        Private WsSiActif As String
        Private WsLangue As Integer = 0
        Private WsMonnaie As Integer = 0
        Private WsNatureUti As Integer = 0
        Private WsPerimetreUti As String
        Private WsFiltre As String
        Private WsCleLDAP As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_LOGIN"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaLoginIntranet
            End Get
        End Property

        Public Property Ide_Interne() As Integer
            Get
                Return MyBase.Ide_Dossier
            End Get
            Set(ByVal value As Integer)
                MyBase.Ide_Dossier = value
                MyBase.Clef = value.ToString
            End Set
        End Property

        Public Property MotdePasse() As String
            Get
                Return WsMotdePasse
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 48
                        WsMotdePasse = value
                    Case Else
                        WsMotdePasse = Strings.Left(value, 48)
                End Select
            End Set
        End Property

        Public Property DateMaj_PW() As String
            Get
                Return WsDate_Maj_PW
            End Get
            Set(ByVal value As String)
                WsDate_Maj_PW = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property DateAcces() As String
            Get
                Return WsDate_Acces
            End Get
            Set(ByVal value As String)
                WsDate_Acces = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Heure_Acces() As String
            Get
                Return WsHeure_Acces
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHeure_Acces = value
                    Case Else
                        WsHeure_Acces = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Nombre_Acces() As Integer
            Get
                Return WsNombre_Acces
            End Get
            Set(ByVal value As Integer)
                WsNombre_Acces = value
            End Set
        End Property

        Public Property SiActif() As String
            Get
                Return WsSiActif
            End Get
            Set(ByVal value As String)
                WsSiActif = value
            End Set
        End Property

        Public Property Langue() As Integer
            Get
                Return WsLangue
            End Get
            Set(ByVal value As Integer)
                WsLangue = value
            End Set
        End Property

        Public Property Monnaie() As Integer
            Get
                Return WsMonnaie
            End Get
            Set(ByVal value As Integer)
                WsMonnaie = value
            End Set
        End Property

        Public Property Nature_Utilisateur() As Integer
            Get
                Return WsNatureUti
            End Get
            Set(ByVal value As Integer)
                WsNatureUti = value
            End Set
        End Property

        Public Property Perimetre_Utilisateur() As String
            Get
                Return WsPerimetreUti
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 32
                        WsPerimetreUti = value
                    Case Else
                        WsPerimetreUti = Strings.Left(value, 32)
                End Select
            End Set
        End Property

        Public Property Filtre() As String
            Get
                Return WsFiltre
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsFiltre = value
                    Case Else
                        WsFiltre = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Cle_LDAP() As String
            Get
                Return WsCleLDAP
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsCleLDAP = value
                    Case Else
                        WsCleLDAP = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Ide_Interne & VI.Tild)
                Chaine.Append(MotdePasse & VI.Tild)
                Chaine.Append(DateMaj_PW & VI.Tild)
                Chaine.Append(DateAcces & VI.Tild)
                Chaine.Append(Heure_Acces & VI.Tild)
                Chaine.Append(Nombre_Acces.ToString & VI.Tild)
                Chaine.Append(SiActif & VI.Tild)
                Chaine.Append(Langue.ToString & VI.Tild)
                Chaine.Append(Monnaie.ToString & VI.Tild)
                Chaine.Append(Nature_Utilisateur.ToString & VI.Tild)
                Chaine.Append(Perimetre_Utilisateur & VI.Tild)
                Chaine.Append(Filtre & VI.Tild)
                Chaine.Append(Cle_LDAP)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 14 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Ide_Interne = Ide_Dossier
                MotdePasse = TableauData(2)
                DateMaj_PW = TableauData(3)
                DateAcces = TableauData(4)
                Heure_Acces = TableauData(5)
                If TableauData(6) <> "" Then Nombre_Acces = CInt(TableauData(6))
                SiActif = TableauData(7)
                If TableauData(8) <> "" Then Langue = CInt(TableauData(8))
                If TableauData(9) <> "" Then Monnaie = CInt(TableauData(9))
                If TableauData(10) <> "" Then Nature_Utilisateur = CInt(TableauData(10))
                Perimetre_Utilisateur = TableauData(11)
                Filtre = TableauData(12)
                Cle_LDAP = TableauData(13)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return ""
                    Case 1
                        Return WsIde_Interne.ToString
                    Case 2
                        Return WsMotdePasse
                    Case 3
                        Return WsDate_Maj_PW
                    Case 4
                        Return WsDate_Acces
                    Case 5
                        Return WsHeure_Acces
                    Case 6
                        Return WsNombre_Acces.ToString
                    Case 7
                        Return WsSiActif
                    Case 8
                        Return WsLangue.ToString
                    Case 9
                        Return WsMonnaie.ToString
                    Case 10
                        Return WsNatureUti.ToString
                    Case 11
                        Return WsPerimetreUti
                    Case 12
                        Return WsFiltre
                    Case 13
                        Return WsCleLDAP
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

