﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_DECRECRUTER
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsDate_Validite As String
        Private WsNumeroDecision As String
        Private WsDate_Fin_Validite As String
        Private WsAnneeUniversitaire As String
        Private WsDate_Decision As String
        Private WsEtat_Decision As String
        Private WsDate_Envoi_Dossier As String
        Private WsSiEdition As String
        Private WsSiIrrecevabilite As String
        Private WsMotifIrrecevabilite As String
        Private WsMontantNet As Double
        Private WsVolumeHoraire_EquiHED As Double
        Private WsVolumeHoraire_CoursMagistraux As Double
        Private WsVolumeHoraire_TD As Double
        Private WsVolumeHoraire_TP As Double
        Private WsEnseignantResponsable As String
        Private WsAffectationPrincipale As String
        Private WsAffectationSecondaire As String
        Private WsCodeAnalytique As String
        Private WsActivite As String
        Private WsCodeActivite As String
        Private WsCodeAction As String
        Private WsNumeroCommission As String
        Private WsCommission As String
        Private WsDateCommission As String
        Private WsAvisCommission As String
        Private WsObservations As String
        '
        Private TsDate_de_fin As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_DECRECRUTER"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaDecRecrutement 'Spécifique Secteur Public
            End Get
        End Property

        Public Property Date_Validite() As String
            Get
                Return WsDate_Validite
            End Get
            Set(ByVal value As String)
                WsDate_Validite = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Numero_Decision() As String
            Get
                Return WsNumeroDecision
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 14
                        WsNumeroDecision = value
                    Case Else
                        WsNumeroDecision = Strings.Left(value, 14)
                End Select
                MyBase.Clef = WsNumeroDecision
            End Set
        End Property

        Public Property Date_Fin_Validite() As String
            Get
                Return WsDate_Fin_Validite
            End Get
            Set(ByVal value As String)
                WsDate_Fin_Validite = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property AnneeUniversitaire() As String
            Get
                Return WsAnneeUniversitaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 12
                        WsAnneeUniversitaire = value
                    Case Else
                        WsAnneeUniversitaire = Strings.Left(value, 12)
                End Select
            End Set
        End Property

        Public Property Date_Decision() As String
            Get
                Return WsDate_Decision
            End Get
            Set(ByVal value As String)
                WsDate_Decision = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Etat_Decision() As String
            Get
                Return WsEtat_Decision
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsEtat_Decision = value
                    Case Else
                        WsEtat_Decision = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Date_Envoi_Dossier() As String
            Get
                Return WsDate_Envoi_Dossier
            End Get
            Set(ByVal value As String)
                WsDate_Envoi_Dossier = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property SiEdition() As String
            Get
                Return WsSiEdition
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiEdition = value
                    Case Else
                        WsSiEdition = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property SiIrrecevabilite() As String
            Get
                Return WsSiIrrecevabilite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiIrrecevabilite = value
                    Case Else
                        WsSiIrrecevabilite = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Motif_Irrecevabilite() As String
            Get
                Return WsMotifIrrecevabilite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsMotifIrrecevabilite = value
                    Case Else
                        WsMotifIrrecevabilite = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property VolumeHoraire_EquivalentHED() As Double
            Get
                Return WsVolumeHoraire_EquiHED
            End Get
            Set(ByVal value As Double)
                WsVolumeHoraire_EquiHED = value
            End Set
        End Property

        Public Property VolumeHoraire_CoursMagistraux() As Double
            Get
                Return WsVolumeHoraire_CoursMagistraux
            End Get
            Set(ByVal value As Double)
                WsVolumeHoraire_CoursMagistraux = value
            End Set
        End Property

        Public Property VolumeHoraire_TravauxDiriges() As Double
            Get
                Return WsVolumeHoraire_TD
            End Get
            Set(ByVal value As Double)
                WsVolumeHoraire_TD = value
            End Set
        End Property

        Public Property VolumeHoraire_TravauxPratiques() As Double
            Get
                Return WsVolumeHoraire_TP
            End Get
            Set(ByVal value As Double)
                WsVolumeHoraire_TP = value
            End Set
        End Property

        Public Property EnseignantResponsable() As String
            Get
                Return WsEnseignantResponsable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsEnseignantResponsable = value
                    Case Else
                        WsEnseignantResponsable = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property AffectationPrincipale() As String
            Get
                Return WsAffectationPrincipale
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsAffectationPrincipale = value
                    Case Else
                        WsAffectationPrincipale = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property AffectationSecondaire() As String
            Get
                Return WsAffectationSecondaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsAffectationSecondaire = value
                    Case Else
                        WsAffectationSecondaire = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Code_Analytique() As String
            Get
                Return WsCodeAnalytique
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 6
                        WsCodeAnalytique = value
                    Case Else
                        WsCodeAnalytique = Strings.Left(value, 6)
                End Select
            End Set
        End Property

        Public Property Activite() As String
            Get
                Return WsActivite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsActivite = value
                    Case Else
                        WsActivite = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Code_Activite() As String
            Get
                Return WsCodeActivite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsCodeActivite = value
                    Case Else
                        WsCodeActivite = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Code_Action() As String
            Get
                Return WsCodeAction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 6
                        WsCodeAction = value
                    Case Else
                        WsCodeAction = Strings.Left(value, 6)
                End Select
            End Set
        End Property

        Public Property Numero_Commission() As String
            Get
                Return WsNumeroCommission
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsNumeroCommission = value
                    Case Else
                        WsNumeroCommission = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Commission() As String
            Get
                Return WsCommission
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsCommission = value
                    Case Else
                        WsCommission = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Date_Commission() As String
            Get
                Return WsDateCommission
            End Get
            Set(ByVal value As String)
                WsDateCommission = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Avis_Commission() As String
            Get
                Return WsAvisCommission
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsAvisCommission = value
                    Case Else
                        WsAvisCommission = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(Date_Validite & VI.Tild)
                Chaine.Append(Numero_Decision & VI.Tild)
                Chaine.Append(Date_Fin_Validite & VI.Tild)
                Chaine.Append(AnneeUniversitaire & VI.Tild)
                Chaine.Append(Date_Decision & VI.Tild)
                Chaine.Append(Etat_Decision & VI.Tild)
                Chaine.Append(Date_Envoi_Dossier & VI.Tild)
                Chaine.Append(SiEdition & VI.Tild)
                Chaine.Append(SiIrrecevabilite & VI.Tild)
                Chaine.Append(Motif_Irrecevabilite & VI.Tild)
                Chaine.Append(VolumeHoraire_EquivalentHED.ToString & VI.Tild)
                Chaine.Append(VolumeHoraire_CoursMagistraux.ToString & VI.Tild)
                Chaine.Append(VolumeHoraire_TravauxDiriges.ToString & VI.Tild)
                Chaine.Append(VolumeHoraire_TravauxPratiques.ToString & VI.Tild)
                Chaine.Append(EnseignantResponsable & VI.Tild)
                Chaine.Append(AffectationPrincipale & VI.Tild)
                Chaine.Append(AffectationSecondaire & VI.Tild)
                Chaine.Append(Code_Analytique & VI.Tild)
                Chaine.Append(Activite & VI.Tild)
                Chaine.Append(Code_Activite & VI.Tild)
                Chaine.Append(Code_Action & VI.Tild)
                Chaine.Append(Numero_Commission & VI.Tild)
                Chaine.Append(Commission & VI.Tild)
                Chaine.Append(Date_Commission & VI.Tild)
                Chaine.Append(Avis_Commission & VI.Tild)
                Chaine.Append(Observations)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 27 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Date_Validite = TableauData(1)
                Numero_Decision = TableauData(2)
                Date_Fin_Validite = TableauData(3)
                AnneeUniversitaire = TableauData(4)
                Date_Decision = TableauData(5)
                Etat_Decision = TableauData(6)
                Date_Envoi_Dossier = TableauData(7)
                SiEdition = TableauData(8)
                SiIrrecevabilite = TableauData(9)
                Motif_Irrecevabilite = TableauData(10)
                If TableauData(11) = "" Then TableauData(11) = "0"
                VolumeHoraire_EquivalentHED = VirRhFonction.ConversionDouble(TableauData(11))
                If TableauData(12) = "" Then TableauData(12) = "0"
                VolumeHoraire_CoursMagistraux = VirRhFonction.ConversionDouble(TableauData(12))
                If TableauData(13) = "" Then TableauData(13) = "0"
                VolumeHoraire_TravauxDiriges = VirRhFonction.ConversionDouble(TableauData(13))
                If TableauData(14) = "" Then TableauData(14) = "0"
                VolumeHoraire_TravauxPratiques = VirRhFonction.ConversionDouble(TableauData(14))
                EnseignantResponsable = TableauData(15)
                AffectationPrincipale = TableauData(16)
                AffectationSecondaire = TableauData(17)
                Code_Analytique = TableauData(18)
                Activite = TableauData(19)
                Code_Activite = TableauData(20)
                Code_Action = TableauData(21)
                Numero_Commission = TableauData(22)
                Commission = TableauData(23)
                Date_Commission = TableauData(24)
                Avis_Commission = TableauData(25)
                Observations = TableauData(26)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = Date_Fin_Validite
            End Set
        End Property

        Public Property VDate_de_Fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return WsDate_Validite
                    Case 1
                        Return WsNumeroDecision
                    Case 2
                        Return WsDate_Fin_Validite
                    Case 3
                        Return WsAnneeUniversitaire
                    Case 4
                        Return WsDate_Decision
                    Case 5
                        Return WsEtat_Decision
                    Case 6
                        Return WsDate_Envoi_Dossier
                    Case 7
                        Return WsSiEdition
                    Case 8
                        Return WsSiIrrecevabilite
                    Case 9
                        Return WsMotifIrrecevabilite
                    Case 10
                        Return WsVolumeHoraire_EquiHED.ToString
                    Case 11
                        Return WsVolumeHoraire_CoursMagistraux.ToString
                    Case 12
                        Return WsVolumeHoraire_TD.ToString
                    Case 13
                        Return WsVolumeHoraire_TP.ToString
                    Case 14
                        Return WsEnseignantResponsable
                    Case 15
                        Return WsAffectationPrincipale
                    Case 16
                        Return WsAffectationSecondaire
                    Case 17
                        Return WsCodeAnalytique
                    Case 18
                        Return WsActivite
                    Case 19
                        Return WsCodeActivite
                    Case 20
                        Return WsCodeAction
                    Case 21
                        Return WsNumeroCommission
                    Case 22
                        Return WsCommission
                    Case 23
                        Return WsDateCommission
                    Case 24
                        Return WsAvisCommission
                    Case 25
                        Return WsObservations
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
