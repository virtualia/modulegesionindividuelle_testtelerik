﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_ENTRETIEN_HONDA_FORMATION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsIntitule_Besoin As String
        Private WsReference_Besoin As String
        Private WsDomaine As String
        Private WsPrecisions_Contenu As String
        Private WsPeriode As String
        Private WsDuree_Jours As Integer
        Private WsDuree_Heures As String
        Private WsCout_HT As Double
        Private WsObjectif As String
        Private WsDate_Saisie_Manager As String
        Private WsIdentifiant_N1 As Integer
        Private WsValidation_N1 As String
        Private WsDate_Validation_N1 As String
        Private WsSiValidation_DRH As String
        Private WsDate_Validation_DRH As String
        Private WsCommentaire As String
        Private WsType_Formation As String
        Private WsNumeroTri As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENTRETIEN_FORMATION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 153
            End Get
        End Property

        Public Property Intitule_Besoin() As String
            Get
                Return WsIntitule_Besoin
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsIntitule_Besoin = value
                    Case Else
                        WsIntitule_Besoin = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Reference_Besoin() As String
            Get
                Return WsReference_Besoin
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsReference_Besoin = value
                    Case Else
                        WsReference_Besoin = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Domaine() As String
            Get
                Return WsDomaine
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsDomaine = value
                    Case Else
                        WsDomaine = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Precisions_Sur_Contenu() As String
            Get
                Return WsPrecisions_Contenu
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsPrecisions_Contenu = value
                    Case Else
                        WsPrecisions_Contenu = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property Periode() As String
            Get
                Return WsPeriode
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsPeriode = value
                    Case Else
                        WsPeriode = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Duree_En_Jours As Double
            Get
                Return WsDuree_Jours
            End Get
            Set(value As Double)
                WsDuree_Jours = value
            End Set
        End Property

        Public Property Duree_En_Heures() As String
            Get
                Return WsDuree_Heures
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsDuree_Heures = value
                    Case Else
                        WsDuree_Heures = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Cout_HT As Double
            Get
                Return WsCout_HT
            End Get
            Set(value As Double)
                WsCout_HT = value
            End Set
        End Property

        Public Property Objectif() As String
            Get
                Return WsObjectif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsObjectif = value
                    Case Else
                        WsObjectif = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property Date_Saisie_Manager() As String
            Get
                Return WsDate_Saisie_Manager
            End Get
            Set(ByVal value As String)
                WsDate_Saisie_Manager = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Identifiant_N1 As Integer
            Get
                Return WsIdentifiant_N1
            End Get
            Set(value As Integer)
                WsIdentifiant_N1 = value
            End Set
        End Property

        Public Property Validation_N1() As String
            Get
                Return WsValidation_N1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsValidation_N1 = value
                    Case Else
                        WsValidation_N1 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Date_Validation_N1() As String
            Get
                Return WsDate_Validation_N1
            End Get
            Set(ByVal value As String)
                WsDate_Validation_N1 = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property SiValidation_DRH() As String
            Get
                Return WsSiValidation_DRH
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = "", "Non"
                        WsSiValidation_DRH = "Non"
                    Case Else
                        WsSiValidation_DRH = "Oui"
                End Select
            End Set
        End Property

        Public Property Date_Validation_DRH() As String
            Get
                Return WsDate_Validation_DRH
            End Get
            Set(ByVal value As String)
                WsDate_Validation_DRH = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Commentaire() As String
            Get
                Return WsCommentaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1000
                        WsCommentaire = value
                    Case Else
                        WsCommentaire = Strings.Left(value, 1000)
                End Select
            End Set
        End Property

        Public Property Type_Formation() As String
            Get
                Return WsType_Formation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsType_Formation = value
                    Case Else
                        WsType_Formation = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property NumeroTri() As Integer
            Get
                Return WsNumeroTri
            End Get
            Set(ByVal value As Integer)
                WsNumeroTri = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Intitule_Besoin & VI.Tild)
                Chaine.Append(Reference_Besoin & VI.Tild)
                Chaine.Append(Domaine & VI.Tild)
                Chaine.Append(Precisions_Sur_Contenu & VI.Tild)
                Chaine.Append(Periode & VI.Tild)
                Chaine.Append(Duree_En_Jours.ToString & VI.Tild)
                Chaine.Append(Duree_En_Heures & VI.Tild)
                Chaine.Append(Cout_HT.ToString & VI.Tild)
                Chaine.Append(Objectif & VI.Tild)
                Chaine.Append(Date_Saisie_Manager & VI.Tild)
                Chaine.Append(Identifiant_N1.ToString & VI.Tild)
                Chaine.Append(Validation_N1 & VI.Tild)
                Chaine.Append(Date_Validation_N1 & VI.Tild)
                Chaine.Append(SiValidation_DRH & VI.Tild)
                Chaine.Append(Date_Validation_DRH & VI.Tild)
                Chaine.Append(Commentaire & VI.Tild)
                Chaine.Append(Type_Formation & VI.Tild)
                Chaine.Append(NumeroTri.ToString)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 20 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Intitule_Besoin = TableauData(2)
                Reference_Besoin = TableauData(3)
                Domaine = TableauData(4)
                Precisions_Sur_Contenu = TableauData(5)
                Periode = TableauData(6)
                Duree_En_Jours = CInt(VirRhFonction.ConversionDouble(TableauData(7)))
                Duree_En_Heures = TableauData(8)
                Cout_HT = VirRhFonction.ConversionDouble(TableauData(9))
                Objectif = TableauData(10)
                Date_Saisie_Manager = TableauData(11)
                Identifiant_N1 = CInt(VirRhFonction.ConversionDouble(TableauData(12)))
                Validation_N1 = TableauData(13)
                Date_Validation_N1 = TableauData(14)
                SiValidation_DRH = TableauData(15)
                Date_Validation_DRH = TableauData(16)
                Commentaire = TableauData(17)
                Type_Formation = TableauData(18)
                NumeroTri = CInt(VirRhFonction.ConversionDouble(TableauData(19)))

                If WsFicheLue Is Nothing Then
                    WsFicheLue = New System.Text.StringBuilder
                    For IndiceI = 1 To TableauData.Count - 1
                        WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                    Next IndiceI
                End If
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace

