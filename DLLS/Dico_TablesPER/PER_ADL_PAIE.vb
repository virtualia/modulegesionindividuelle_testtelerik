﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_ADL_PAIE
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsCodePCE As String
        Private WsMontantMonnaieCompte As Double = 0
        Private WsMontantEuro As Double = 0
        Private WsTauxChancellerie As Double = 0
        Private WsDestinataire As String
        Private WsDateSaisie As String
        Private WsCommentaire As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ADL_PAIE"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 90
            End Get
        End Property

        Public Property Code_PCE() As String
            Get
                Return WsCodePCE
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsCodePCE = value
                    Case Else
                        WsCodePCE = Strings.Left(value, 10)
                End Select
                MyBase.Clef = WsCodePCE
            End Set
        End Property

        Public Property Montant_MonnaiedeCompte() As Double
            Get
                Return WsMontantMonnaieCompte
            End Get
            Set(ByVal value As Double)
                WsMontantMonnaieCompte = value
            End Set
        End Property

        Public Property Montant_Euro() As Double
            Get
                Return WsMontantEuro
            End Get
            Set(ByVal value As Double)
                WsMontantEuro = value
            End Set
        End Property

        Public Property Taux_Chancellerie() As Double
            Get
                Return WsTauxChancellerie
            End Get
            Set(ByVal value As Double)
                WsTauxChancellerie = value
            End Set
        End Property

        Public Property Destinataire() As String
            Get
                Return WsDestinataire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsDestinataire = value
                    Case Else
                        WsDestinataire = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Date_de_Saisie() As String
            Get
                Return WsDateSaisie
            End Get
            Set(ByVal value As String)
                WsDateSaisie = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Commentaire() As String
            Get
                Return WsCommentaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsCommentaire = value
                    Case Else
                        WsCommentaire = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Code_PCE & VI.Tild)
                Select Case Montant_MonnaiedeCompte
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_MonnaiedeCompte.ToString) & VI.Tild)
                End Select
                Select Case Montant_Euro
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant_Euro.ToString) & VI.Tild)
                End Select
                Select Case Taux_Chancellerie
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Taux_Chancellerie.ToString) & VI.Tild)
                End Select
                Chaine.Append(Destinataire & VI.Tild)
                Chaine.Append(Date_de_Saisie & VI.Tild)
                Chaine.Append(Commentaire)
                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 9 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Code_PCE = TableauData(2)
                If TableauData(3) <> "" Then Montant_MonnaiedeCompte = VirRhFonction.ConversionDouble(TableauData(3))
                If TableauData(4) <> "" Then Montant_Euro = VirRhFonction.ConversionDouble(TableauData(4))
                If TableauData(5) <> "" Then Taux_Chancellerie = VirRhFonction.ConversionDouble(TableauData(5))
                Destinataire = TableauData(6)
                Date_de_Saisie = TableauData(7)
                Commentaire = TableauData(8)

                WsFicheLue = New System.Text.StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return Date_de_Valeur
                    Case 1
                        Return WsCodePCE
                    Case 2
                        Select Case Montant_MonnaiedeCompte
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Montant_MonnaiedeCompte.ToString)
                        End Select
                    Case 3
                        Select Case Montant_Euro
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Montant_Euro.ToString)
                        End Select
                    Case 4
                        Select Case Taux_Chancellerie
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Taux_Chancellerie.ToString)
                        End Select
                    Case 5
                        Return WsDestinataire
                    Case 6
                        Return WsDateSaisie
                    Case 7
                        Return WsCommentaire
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
