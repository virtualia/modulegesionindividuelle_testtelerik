﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_ORDREMISSION
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsType As String
        Private WsEtendue As String
        Private WsMoyenTransport As String
        Private WsMotifDeplacement As String
        Private WsLieuMission As String
        '
        Private TsDate_de_fin As String
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ORDREMISSION"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return VI.ObjetPer.ObaOrdreMission 'Idem Secteur Public et Privé
            End Get
        End Property

        Public Property TypeOrdre() As String
            Get
                Return WsType
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsType = value
                    Case Else
                        WsType = Strings.Left(value, 30)
                End Select
                MyBase.Clef = WsType
            End Set
        End Property

        Public Property Etendue() As String
            Get
                Return WsEtendue
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsEtendue = value
                    Case Else
                        WsEtendue = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property MoyendeTransport() As String
            Get
                Return WsMoyenTransport
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsMoyenTransport = value
                    Case Else
                        WsMoyenTransport = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property MotifDeplacement() As String
            Get
                Return WsMotifDeplacement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsMotifDeplacement = value
                    Case Else
                        WsMotifDeplacement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property LieuMission() As String
            Get
                Return WsLieuMission
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsLieuMission = value
                    Case Else
                        WsLieuMission = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(TypeOrdre & VI.Tild)
                Chaine.Append(Etendue & VI.Tild)
                Chaine.Append(MoyendeTransport & VI.Tild)
                Chaine.Append(MotifDeplacement & VI.Tild)
                Chaine.Append(LieuMission & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                TypeOrdre = TableauData(2)
                Etendue = TableauData(3)
                MoyendeTransport = TableauData(4)
                MotifDeplacement = TableauData(5)
                LieuMission = TableauData(6)
                MyBase.Date_de_Fin = TableauData(7)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = Date_de_Fin
            End Set
        End Property

        Public Property VDate_de_Fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return Date_de_Valeur
                    Case 1
                        Return WsType
                    Case 2
                        Return WsEtendue
                    Case 3
                        Return WsMoyenTransport
                    Case 4
                        Return WsMotifDeplacement
                    Case 5
                        Return WsLieuMission
                    Case 6
                        Return Date_de_Fin
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
