﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace ShemaPER
    Public Class PER_LOLF
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsIdentAutorisation As String
        Private WsQuotiteAE As Double = 0
        Private WsIdentUO As String
        Private WsQuotiteUO As Double = 0
        Private WsIntituleMission As String
        Private WsIntituleProgramme As String
        Private WsIntituleAction As String
        Private WsIntituleBOP As String
        Private WsIntituleUO As String
        Private WsIntituleFonction As String
        '
        Private TsIdentMission As Integer
        Private TsIdentProgramme As Integer
        Private TsIdentAction As String
        Private TsIdentAE As Integer
        Private TsIdentBOP As Integer
        Private TsIdentUO As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_LOLF"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 92
            End Get
        End Property

        Public Property Identification_AE() As String
            Get
                Return WsIdentAutorisation
            End Get
            Set(ByVal value As String)
                Dim TableauIdent(0) As String
                Select Case value.Length
                    Case Is <= 20
                        WsIdentAutorisation = value
                    Case Else
                        WsIdentAutorisation = Strings.Left(value, 20)
                End Select
                MyBase.Clef = WsIdentAutorisation
                TableauIdent = Strings.Split(WsIdentAutorisation, "-", -1)
                If TableauIdent.Count >= 4 Then
                    If IsNumeric(TableauIdent(0)) Then
                        TsIdentMission = CInt(TableauIdent(0))
                    End If
                    If IsNumeric(TableauIdent(1)) Then
                        TsIdentProgramme = CInt(TableauIdent(1))
                    End If
                    Select Case TableauIdent.Count
                        Case Is = 4
                            TsIdentAction = TableauIdent(2)
                            If IsNumeric(TableauIdent(3)) Then
                                TsIdentAE = CInt(TableauIdent(3))
                            End If
                        Case Else
                            TsIdentAction = TableauIdent(2) & "-" & TableauIdent(3)
                            If IsNumeric(TableauIdent(4)) Then
                                TsIdentAE = CInt(TableauIdent(4))
                            End If
                    End Select
                End If
            End Set
        End Property

        Public Property Quotite_AE() As Double
            Get
                Return WsQuotiteAE
            End Get
            Set(ByVal value As Double)
                WsQuotiteAE = value
                If value > 100 Then
                    WsQuotiteAE = 100
                End If
            End Set
        End Property

        Public Property Identification_UO() As String
            Get
                Return WsIdentUO
            End Get
            Set(ByVal value As String)
                Dim TableauIdent(0) As String
                Select Case value.Length
                    Case Is <= 20
                        WsIdentUO = value
                    Case Else
                        WsIdentUO = Strings.Left(value, 20)
                End Select
                TableauIdent = Strings.Split(WsIdentUO, "-", -1)
                Select Case TableauIdent.Count
                    Case Is = 4
                        If IsNumeric(TableauIdent(2)) Then
                            TsIdentBOP = CInt(TableauIdent(2))
                        End If
                        If IsNumeric(TableauIdent(3)) Then
                            TsIdentUO = CInt(TableauIdent(3))
                        End If
                End Select
            End Set
        End Property

        Public Property Quotite_UO() As Double
            Get
                Return WsQuotiteUO
            End Get
            Set(ByVal value As Double)
                WsQuotiteUO = value
                If value > 100 Then
                    WsQuotiteUO = 100
                End If
            End Set
        End Property

        Public Property Intitule_Mission As String
            Get
                Return WsIntituleMission
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntituleMission = value
                    Case Else
                        WsIntituleMission = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Intitule_Programme As String
            Get
                Return WsIntituleProgramme
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntituleProgramme = value
                    Case Else
                        WsIntituleProgramme = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Intitule_Action As String
            Get
                Return WsIntituleAction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntituleAction = value
                    Case Else
                        WsIntituleAction = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Intitule_Fonction As String
            Get
                Return WsIntituleFonction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntituleFonction = value
                    Case Else
                        WsIntituleFonction = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Intitule_BOP As String
            Get
                Return WsIntituleBOP
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntituleBOP = value
                    Case Else
                        WsIntituleBOP = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Intitule_UO As String
            Get
                Return WsIntituleUO
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntituleUO = value
                    Case Else
                        WsIntituleUO = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Identification_AE & VI.Tild)
                Select Case Quotite_AE
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Quotite_AE.ToString) & VI.Tild)
                End Select
                Chaine.Append(Identification_UO & VI.Tild)
                Select Case Quotite_UO
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Quotite_UO.ToString) & VI.Tild)
                End Select
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Intitule_Mission & VI.Tild)
                Chaine.Append(Intitule_Programme & VI.Tild)
                Chaine.Append(Intitule_Action & VI.Tild)
                Chaine.Append(Intitule_BOP & VI.Tild)
                Chaine.Append(Intitule_UO & VI.Tild)
                Chaine.Append(Intitule_Fonction)
                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 12 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Identification_AE = TableauData(2)
                If TableauData(3) <> "" Then Quotite_AE = VirRhFonction.ConversionDouble(TableauData(3))
                Identification_UO = TableauData(4)
                If TableauData(5) <> "" Then Quotite_UO = VirRhFonction.ConversionDouble(TableauData(5))
                MyBase.Date_de_Fin = TableauData(6)
                Intitule_Mission = TableauData(7)
                Intitule_Programme = TableauData(8)
                Intitule_Action = TableauData(9)
                Intitule_BOP = TableauData(10)
                Intitule_UO = TableauData(11)
                Intitule_Fonction = TableauData(12)

                WsFicheLue = New System.Text.StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_Identification_Mission As Integer
            Get
                Return TsIdentMission
            End Get
        End Property

        Public ReadOnly Property V_Mission As String
            Get
                Return TsIdentMission.ToString & " - " & Intitule_Mission
            End Get
        End Property

        Public ReadOnly Property V_Identification_Programme As Integer
            Get
                Return TsIdentProgramme
            End Get
        End Property

        Public ReadOnly Property V_Programme As String
            Get
                Return TsIdentProgramme.ToString & " - " & Intitule_Programme
            End Get
        End Property

        Public ReadOnly Property V_Identification_Action As String
            Get
                Return TsIdentAction
            End Get
        End Property

        Public ReadOnly Property V_Action As String
            Get
                Return TsIdentAction & " - " & Intitule_Action
            End Get
        End Property

        Public ReadOnly Property V_Identification_AE As Integer
            Get
                Return TsIdentAE
            End Get
        End Property

        Public ReadOnly Property V_AutorisationEmploi As String
            Get
                Return TsIdentAE.ToString & " - " & Intitule_Fonction
            End Get
        End Property

        Public ReadOnly Property V_Identification_BOP As Integer
            Get
                Return TsIdentBOP
            End Get
        End Property

        Public ReadOnly Property V_BOP As String
            Get
                Return TsIdentBOP.ToString & " - " & Intitule_BOP
            End Get
        End Property

        Public ReadOnly Property V_Identification_UO As Integer
            Get
                Return TsIdentUO
            End Get
        End Property

        Public ReadOnly Property V_UniteOperationnelle As String
            Get
                Return TsIdentUO.ToString & " - " & Intitule_UO
            End Get
        End Property

        Public ReadOnly Property V_XML As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append("<DateDebutLOLF>" & MyBase.Date_de_Valeur & "</DateDebutLOLF>" & vbCrLf)
                Chaine.Append("<DateFinLOLF>" & MyBase.Date_de_Fin & "</DateFinLOLF>" & vbCrLf)
                Chaine.Append("<CodeAE>" & Identification_AE & "</CodeAE>" & vbCrLf)
                Chaine.Append("<QuotiteAE>" & VirRhFonction.MontantStocke(Quotite_AE.ToString) & "</QuotiteAE>" & vbCrLf)
                Chaine.Append("<CodeUO>" & Identification_UO & "</CodeUO>" & vbCrLf)
                Chaine.Append("<QuotiteUO>" & VirRhFonction.MontantStocke(Quotite_UO.ToString) & "</QuotiteUO>" & vbCrLf)
                Chaine.Append("<IntituleMission>" & VirRhFonction.ChaineXMLValide(Intitule_Mission) & "</IntituleMission>" & vbCrLf)
                Chaine.Append("<IntituleProgramme>" & VirRhFonction.ChaineXMLValide(Intitule_Programme) & "</IntituleProgramme>" & vbCrLf)
                Chaine.Append("<IntituleAction>" & VirRhFonction.ChaineXMLValide(Intitule_Action) & "</IntituleAction>" & vbCrLf)
                Chaine.Append("<IntituleBOP>" & VirRhFonction.ChaineXMLValide(Intitule_BOP) & "</IntituleBOP>" & vbCrLf)
                Chaine.Append("<IntituleUO>" & VirRhFonction.ChaineXMLValide(Intitule_UO) & "</IntituleUO>" & vbCrLf)
                Chaine.Append("<IntituleFonction>" & VirRhFonction.ChaineXMLValide(Intitule_Fonction) & "</IntituleFonction>" & vbCrLf)

                Return Chaine.ToString

            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return Date_de_Valeur
                    Case 1
                        Return WsIdentAutorisation
                    Case 2
                        Select Case Quotite_AE
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Quotite_AE.ToString)
                        End Select
                    Case 3
                        Return WsIdentUO
                    Case 4
                        Select Case Quotite_UO
                            Case 0
                                Return "0"
                            Case Else
                                Return VirRhFonction.MontantStocke(Quotite_UO.ToString)
                        End Select
                    Case 5
                        Return Date_de_Fin
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace



