﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace ShemaPER
    Public Class PER_ENTRETIEN_HONDA_COMPORTEMENT
        Inherits Virtualia.Systeme.MetaModele.VIR_FICHE
        Private WsFicheLue As System.Text.StringBuilder
        '
        Private WsIntitule As String
        Private WsNumeroTri As Integer
        Private WsNote_Manager As Double
        Private WsNote_Self As Double

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PER_ENTRETIEN_COMPORTEMENT"
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 152
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Numero_Tri() As Integer
            Get
                Return WsNumeroTri
            End Get
            Set(ByVal value As Integer)
                WsNumeroTri = value
            End Set
        End Property

        Public Property Note_Manager() As Double
            Get
                Return WsNote_Manager
            End Get
            Set(ByVal value As Double)
                WsNote_Manager = value
            End Set
        End Property

        Public Property Note_Self() As Double
            Get
                Return WsNote_Self
            End Get
            Set(ByVal value As Double)
                WsNote_Self = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As System.Text.StringBuilder

                Chaine = New System.Text.StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Numero_Tri.ToString & VI.Tild)
                Chaine.Append(Note_Manager.ToString & VI.Tild)
                Chaine.Append(Note_Self.ToString)

                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 6 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Intitule = TableauData(2)
                If TableauData(3) <> "" Then Numero_Tri = CInt(VirRhFonction.ConversionDouble(TableauData(3)))
                Note_Manager = VirRhFonction.ConversionDouble(TableauData(4))
                Note_Self = VirRhFonction.ConversionDouble(TableauData(5))

                If WsFicheLue Is Nothing Then
                    WsFicheLue = New System.Text.StringBuilder
                    For IndiceI = 1 To TableauData.Count - 1
                        WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                    Next IndiceI
                End If
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace
