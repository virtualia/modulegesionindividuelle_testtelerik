﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Virtualia.TablesObjet.ShemaVUE

Namespace Datas
    Public Class ObjetJournal
        Private WsListeEnreg As List(Of VUE_Journal)
        Private WsNomUtilisateur As String
        Private WsNoBd As Integer = 1

        Public WriteOnly Property Ajouter_FicheEnreg() As VUE_Journal
            Set(ByVal value As VUE_Journal)
                WsListeEnreg.Add(value)
            End Set
        End Property

        Public WriteOnly Property Ajouter_Enregistrement() As String
            Set(ByVal value As String)
                Dim FicheJrn As VUE_Journal
                FicheJrn = New VUE_Journal(value)
                WsListeEnreg.Add(FicheJrn)
            End Set
        End Property

        Public ReadOnly Property NombreEnregs() As Integer
            Get
                Return WsListeEnreg.Count
            End Get
        End Property

        Public Function ListeEnreg(ByVal PtdeVue As Integer, ByVal NumObjet As Integer, ByVal IdeDossier As Integer) As List(Of VUE_Journal)

            Dim LstRes As List(Of VUE_Journal)
            LstRes = (From instance In WsListeEnreg Select instance _
                            Where instance.PointdeVue = PtdeVue And instance.NumeroObjet = NumObjet And instance.Ide_Dossier = IdeDossier _
                            Order By instance.Date_Valeur_ToDate Ascending).ToList
            Return LstRes

        End Function

        Public Function ListeEnreg(ByVal PtdeVue As Integer, ByVal IdeDossier As Integer) As List(Of VUE_Journal)

            Dim LstRes As List(Of VUE_Journal)
            LstRes = (From instance In WsListeEnreg Select instance _
                            Where instance.PointdeVue = PtdeVue And instance.Ide_Dossier = IdeDossier _
                            Order By instance.NumeroObjet Ascending, instance.Date_Valeur_ToDate Ascending).ToList
            Return LstRes

        End Function

        Public Function ListeEnreg(ByVal Datedebut As String, ByVal Datefin As String) As List(Of VUE_Journal)

            Dim Datedebut_todate = DateValue(Datedebut)
            Dim Datefin_todate = DateValue(Datefin)
            Dim LstRes As List(Of VUE_Journal)
            LstRes = (From instance In WsListeEnreg Select instance _
                            Where instance.Date_Valeur_ToDate >= Datedebut_todate _
                            And instance.Date_Valeur_ToDate <= Datefin_todate _
                            Order By instance.NumeroObjet Ascending, instance.Date_Valeur_ToDate Ascending).ToList
            Return LstRes

        End Function

        Public Sub New(ByVal NoBd As Integer, ByVal NomUti As String)
            WsNoBd = NoBd
            WsNomUtilisateur = NomUti
        End Sub

    End Class
End Namespace