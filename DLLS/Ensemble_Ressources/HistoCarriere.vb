﻿Option Strict Off
Option Explicit On
Option Compare Text

Namespace Datas
    Public Class HistoCarriere
        Private WsParent As ObjetDossierPER
        Private WsListeHisto As List(Of LigneCarriere)
        Private WsTableauActivite As ArrayList

        Public ReadOnly Property ListeHisto() As List(Of LigneCarriere)
            Get
                Return WsListeHisto
            End Get
        End Property

        Public ReadOnly Property NombredeLignes() As Integer
            Get
                Return WsListeHisto.Count
            End Get
        End Property

        Private Sub ConstituerHistorique_Principal()
            Dim IndiceI As Integer
            Dim IndiceK As Integer
            Dim IndiceR As Integer
            Dim FicheStatut As Virtualia.TablesObjet.ShemaPER.PER_STATUT
            Dim FichePosition As Virtualia.TablesObjet.ShemaPER.PER_POSITION
            Dim FicheGrade As Virtualia.TablesObjet.ShemaPER.PER_GRADE
            Dim ListeBrute As New List(Of LigneCarriere)
            Dim LigneHisto As LigneCarriere

            IndiceK = 0
            Do
                IndiceI = WsParent.V_IndexFiche(IndiceK, "PER_STATUT")
                If IndiceI = -1 Then
                    Exit Do
                End If
                FicheStatut = CType(WsParent.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_STATUT)
                LigneHisto = New LigneCarriere(FicheStatut.Date_de_Valeur)
                LigneHisto.Statut = FicheStatut.Statut
                LigneHisto.SituationAdministrative = FicheStatut.Situation
                ListeBrute.Add(LigneHisto)
                IndiceK += 1
            Loop

            IndiceK = 0
            Do
                IndiceI = WsParent.V_IndexFiche(IndiceK, "PER_POSITION")
                If IndiceI = -1 Then
                    Exit Do
                End If
                FichePosition = CType(WsParent.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_POSITION)
                LigneHisto = ListeBrute.Find(Function(Recherche) Recherche.DateValeur = FichePosition.Date_de_Valeur)
                If LigneHisto Is Nothing Then
                    LigneHisto = New LigneCarriere(FichePosition.Date_de_Valeur)
                    ListeBrute.Add(LigneHisto)
                End If
                LigneHisto.PositionActivite = FichePosition.Position
                LigneHisto.ModaliteService = FichePosition.Modaliteservice
                LigneHisto.TauxActivite = FichePosition.Taux_d_activite
                IndiceK += 1
            Loop

            IndiceK = 0
            Do
                IndiceI = WsParent.V_IndexFiche(IndiceK, "PER_GRADE")
                If IndiceI = -1 Then
                    Exit Do
                End If
                FicheGrade = CType(WsParent.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_GRADE)
                LigneHisto = ListeBrute.Find(Function(Recherche) Recherche.DateValeur = FicheGrade.Date_de_Valeur)
                If LigneHisto Is Nothing Then
                    LigneHisto = New LigneCarriere(FicheGrade.Date_de_Valeur)
                    ListeBrute.Add(LigneHisto)
                End If
                LigneHisto.Grade = FicheGrade.Grade
                LigneHisto.Echelon = FicheGrade.Echelon
                LigneHisto.IndiceMajore = FicheGrade.Indice_majore
                IndiceK += 1
            Loop

            WsListeHisto = New List(Of LigneCarriere)
            If ListeBrute.Count = 0 Then
                Exit Sub
            End If

            WsListeHisto = ListeBrute.OrderBy(Function(Tri) Tri.ValeurDate).ToList

            For IndiceK = WsListeHisto.Count - 1 To 1 Step -1
                LigneHisto = WsListeHisto.Item(IndiceK)
                If LigneHisto.PositionActivite = "" Then
                    For IndiceR = IndiceK - 1 To 0 Step -1
                        If WsListeHisto.Item(IndiceR).PositionActivite <> "" Then
                            LigneHisto.PositionActivite = WsListeHisto.Item(IndiceR).PositionActivite
                            LigneHisto.ModaliteService = WsListeHisto.Item(IndiceR).ModaliteService
                            LigneHisto.TauxActivite = WsListeHisto.Item(IndiceR).TauxActivite
                            Exit For
                        End If
                    Next IndiceR
                End If
            Next IndiceK

            For IndiceK = WsListeHisto.Count - 1 To 1 Step -1
                LigneHisto = WsListeHisto.Item(IndiceK)
                If LigneHisto.Statut = "" Then
                    For IndiceR = IndiceK - 1 To 0 Step -1
                        If WsListeHisto.Item(IndiceR).Statut <> "" Then
                            LigneHisto.Statut = WsListeHisto.Item(IndiceR).Statut
                            LigneHisto.SituationAdministrative = WsListeHisto.Item(IndiceR).SituationAdministrative
                            Exit For
                        End If
                    Next IndiceR
                End If
            Next IndiceK

            For IndiceK = WsListeHisto.Count - 1 To 1 Step -1
                LigneHisto = WsListeHisto.Item(IndiceK)
                If LigneHisto.Grade = "" And SiPositiondActivite(LigneHisto.PositionActivite) = True Then
                    For IndiceR = IndiceK - 1 To 0 Step -1
                        If WsListeHisto.Item(IndiceR).Grade <> "" Then
                            LigneHisto.Grade = WsListeHisto.Item(IndiceR).Grade
                            LigneHisto.Echelon = WsListeHisto.Item(IndiceR).Echelon
                            LigneHisto.IndiceMajore = WsListeHisto.Item(IndiceR).IndiceMajore
                            Exit For
                        End If
                    Next IndiceR
                End If
            Next IndiceK
        End Sub

        Private Sub ConstituerHistorique_DoubleCarriere()
            Dim IndiceI As Integer
            Dim IndiceK As Integer
            Dim IndiceR As Integer
            Dim FicheStatut As Virtualia.TablesObjet.ShemaPER.PER_STATUT_DETACHE
            Dim FichePosition As Virtualia.TablesObjet.ShemaPER.PER_POSITION_DETACHE
            Dim FicheGrade As Virtualia.TablesObjet.ShemaPER.PER_GRADE_DETACHE
            Dim ListeBrute As New List(Of LigneCarriere)
            Dim LigneHisto As LigneCarriere

            IndiceK = 0
            Do
                IndiceI = WsParent.V_IndexFiche(IndiceK, "PER_STATUT_DETACHE")
                If IndiceI = -1 Then
                    Exit Do
                End If
                FicheStatut = CType(WsParent.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_STATUT_DETACHE)
                LigneHisto = New LigneCarriere(FicheStatut.Date_de_Valeur)
                LigneHisto.Statut = FicheStatut.Statut
                LigneHisto.Statut = FicheStatut.Situation
                ListeBrute.Add(LigneHisto)
                IndiceK += 1
            Loop

            IndiceK = 0
            Do
                IndiceI = WsParent.V_IndexFiche(IndiceK, "PER_POSITION_DETACHE")
                If IndiceI = -1 Then
                    Exit Do
                End If
                FichePosition = CType(WsParent.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_POSITION_DETACHE)
                LigneHisto = ListeBrute.Find(Function(Recherche) Recherche.DateValeur = FichePosition.Date_de_Valeur)
                If LigneHisto Is Nothing Then
                    LigneHisto = New LigneCarriere(FichePosition.Date_de_Valeur)
                    ListeBrute.Add(LigneHisto)
                End If
                LigneHisto.PositionActivite = FichePosition.Position
                LigneHisto.ModaliteService = FichePosition.Modaliteservice
                LigneHisto.TauxActivite = FichePosition.Taux_d_activite
                IndiceK += 1
            Loop

            IndiceK = 0
            Do
                IndiceI = WsParent.V_IndexFiche(IndiceK, "PER_GRADE_DETACHE")
                If IndiceI = -1 Then
                    Exit Do
                End If
                FicheGrade = CType(WsParent.Item(IndiceI), Virtualia.TablesObjet.ShemaPER.PER_GRADE_DETACHE)
                LigneHisto = ListeBrute.Find(Function(Recherche) Recherche.DateValeur = FicheGrade.Date_de_Valeur)
                If LigneHisto Is Nothing Then
                    LigneHisto = New LigneCarriere(FicheGrade.Date_de_Valeur)
                    ListeBrute.Add(LigneHisto)
                End If
                LigneHisto.Grade = FicheGrade.Grade
                LigneHisto.Echelon = FicheGrade.Echelon
                LigneHisto.IndiceMajore = FicheGrade.Indice_majore
                IndiceK += 1
            Loop

            WsListeHisto = New List(Of LigneCarriere)
            If ListeBrute.Count = 0 Then
                Exit Sub
            End If

            WsListeHisto = ListeBrute.OrderBy(Function(Tri) Tri.ValeurDate).ToList

            For IndiceK = WsListeHisto.Count - 1 To 1 Step -1
                LigneHisto = WsListeHisto.Item(IndiceK)
                If LigneHisto.PositionActivite = "" Then
                    For IndiceR = IndiceK - 1 To 0 Step -1
                        If WsListeHisto.Item(IndiceR).PositionActivite <> "" Then
                            LigneHisto.PositionActivite = WsListeHisto.Item(IndiceR).PositionActivite
                            LigneHisto.ModaliteService = WsListeHisto.Item(IndiceR).ModaliteService
                            LigneHisto.TauxActivite = WsListeHisto.Item(IndiceR).TauxActivite
                            Exit For
                        End If
                    Next IndiceR
                End If
            Next IndiceK

            For IndiceK = WsListeHisto.Count - 1 To 1 Step -1
                LigneHisto = WsListeHisto.Item(IndiceK)
                If LigneHisto.Statut = "" Then
                    For IndiceR = IndiceK - 1 To 0 Step -1
                        If WsListeHisto.Item(IndiceR).Statut <> "" Then
                            LigneHisto.Statut = WsListeHisto.Item(IndiceR).Statut
                            LigneHisto.SituationAdministrative = WsListeHisto.Item(IndiceR).SituationAdministrative
                            Exit For
                        End If
                    Next IndiceR
                End If
            Next IndiceK

            For IndiceK = WsListeHisto.Count - 1 To 1 Step -1
                LigneHisto = WsListeHisto.Item(IndiceK)
                If LigneHisto.Grade = "" And SiPositiondActivite(LigneHisto.PositionActivite) Then
                    For IndiceR = IndiceK - 1 To 0 Step -1
                        If WsListeHisto.Item(IndiceR).Grade <> "" Then
                            LigneHisto.Grade = WsListeHisto.Item(IndiceR).Grade
                            LigneHisto.Echelon = WsListeHisto.Item(IndiceR).Echelon
                            LigneHisto.IndiceMajore = WsListeHisto.Item(IndiceR).IndiceMajore
                            Exit For
                        End If
                    Next IndiceR
                End If
            Next IndiceK

        End Sub

        Private Function SiPositiondActivite(ByVal Valeur As String) As Boolean

            For Each s As String In WsTableauActivite
                If (s = Valeur) Then
                    Return True
                End If
            Next

            Return False

        End Function

        Public Sub New(ByVal NomUtilisateur As String, ByVal Host As ObjetDossierPER, ByVal TabActivites As ArrayList, ByVal SiDoubleCarriere As Boolean)
            WsParent = Host
            WsTableauActivite = TabActivites
            If SiDoubleCarriere Then
                ConstituerHistorique_DoubleCarriere()
            Else
                ConstituerHistorique_Principal()
            End If
        End Sub

    End Class
End Namespace