﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace Datas
    Public Class ObjetDiscriminants
        Private WsNomUtilisateur As String
        Private WsLstPlanFormation As List(Of String)
        Private WsLstGestionGrade As List(Of String)
        Private WsLstEtaUniteCycle As List(Of String)
        Private WsLstEtaCycleTravail As List(Of String)
        Private WsLstEtaMesureActivite As List(Of String)
        Private WsLstFilierePosteBud As List(Of String)
        Private WsLstNiveau1PosteFct As List(Of String)

        Public ReadOnly Property ItemPlanFormation(ByVal Index As Integer) As String
            Get
                If WsLstPlanFormation Is Nothing Then
                    Return ""
                End If
                Select Case Index
                    Case 0 To WsLstPlanFormation.Count - 1
                        Return WsLstPlanFormation(Index).ToString
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property ItemGestionGrade(ByVal Index As Integer) As String
            Get
                If WsLstGestionGrade Is Nothing Then
                    Return ""
                End If
                Select Case Index
                    Case 0 To WsLstGestionGrade.Count - 1
                        Return WsLstGestionGrade(Index).ToString
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property ItemEtablissementUnite(ByVal Index As Integer) As String
            Get
                If WsLstEtaUniteCycle Is Nothing Then
                    Return ""
                End If
                Select Case Index
                    Case 0 To WsLstEtaUniteCycle.Count - 1
                        Return WsLstEtaUniteCycle(Index).ToString
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property ItemEtablissementCycle(ByVal Index As Integer) As String
            Get
                If WsLstEtaCycleTravail Is Nothing Then
                    Return ""
                End If
                Select Case Index
                    Case 0 To WsLstEtaCycleTravail.Count - 1
                        Return WsLstEtaCycleTravail(Index).ToString
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property ItemEtablissementActivites(ByVal Index As Integer) As String
            Get
                If WsLstEtaMesureActivite Is Nothing Then
                    Return ""
                End If
                Select Case Index
                    Case 0 To WsLstEtaMesureActivite.Count - 1
                        Return WsLstEtaMesureActivite(Index).ToString
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property ItemFilierePosteBud(ByVal Index As Integer) As String
            Get
                If WsLstFilierePosteBud Is Nothing Then
                    Return ""
                End If
                Select Case Index
                    Case 0 To WsLstFilierePosteBud.Count - 1
                        Return WsLstFilierePosteBud(Index).ToString
                End Select
                Return ""
            End Get
        End Property

        Public ReadOnly Property ItemNiveau1PosteFct(ByVal Index As Integer) As String
            Get
                If WsLstNiveau1PosteFct Is Nothing Then
                    Return ""
                End If
                Select Case Index
                    Case 0 To WsLstNiveau1PosteFct.Count - 1
                        Return WsLstNiveau1PosteFct(Index).ToString
                End Select
                Return ""
            End Get
        End Property

        Private Sub ConstituerListe()
            Dim ClauseSql As String
            Dim LstResultat As List(Of String)
            Dim IndiceA As Integer
            Dim TableauData(0) As String

            '** 1  Liste des Plans de formation
            WsLstPlanFormation = New List(Of String)
            ClauseSql = "SELECT DISTINCT Plan_de_formation FROM FOR_IDENTIFICATION ORDER BY Plan_de_formation"

            LstResultat = VirServiceServeur.RequeteSql_ToListeChar(WsNomUtilisateur, VI.PointdeVue.PVueFormation, 1, ClauseSql)
            For IndiceA = 0 To LstResultat.Count - 1
                If LstResultat(IndiceA) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(LstResultat(IndiceA), VI.Tild, -1)
                If TableauData(0) = "" Then
                    TableauData(0) = "Non défini"
                End If
                WsLstPlanFormation.Add(TableauData(0))
            Next IndiceA

            '** 2  Liste des codes gestion du grade
            WsLstGestionGrade = New List(Of String)
            ClauseSql = "SELECT DISTINCT Gestion FROM GRD_GRADE ORDER BY Gestion"

            LstResultat = VirServiceServeur.RequeteSql_ToListeChar(WsNomUtilisateur, VI.PointdeVue.PVueGrades, 1, ClauseSql)
            For IndiceA = 0 To LstResultat.Count - 1
                If LstResultat(IndiceA) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(LstResultat(IndiceA), VI.Tild, -1)
                If TableauData(0) = "" Then
                    TableauData(0) = "Non défini"
                End If
                WsLstGestionGrade.Add(TableauData(0))
            Next IndiceA

            '** 3  Liste des Etablissements Unités de cycle 
            WsLstEtaUniteCycle = New List(Of String)
            ClauseSql = "SELECT DISTINCT FiltreEtablissement FROM CYC_IDENTIFICATION ORDER BY FiltreEtablissement"

            LstResultat = VirServiceServeur.RequeteSql_ToListeChar(WsNomUtilisateur, VI.PointdeVue.PVueBaseHebdo, 1, ClauseSql)
            For IndiceA = 0 To LstResultat.Count - 1
                If LstResultat(IndiceA) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(LstResultat(IndiceA), VI.Tild, -1)
                If TableauData(0) = "" Then
                    TableauData(0) = "Commun"
                End If
                WsLstEtaUniteCycle.Add(TableauData(0))
            Next IndiceA

            '** 4  Liste des Etablissements Cycles de travail 
            WsLstEtaCycleTravail = New List(Of String)
            ClauseSql = "SELECT DISTINCT FiltreEtablissement FROM TRA_IDENTIFICATION ORDER BY FiltreEtablissement"

            LstResultat = VirServiceServeur.RequeteSql_ToListeChar(WsNomUtilisateur, VI.PointdeVue.PVueBaseHebdo, 1, ClauseSql)
            For IndiceA = 0 To LstResultat.Count - 1
                If LstResultat(IndiceA) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(LstResultat(IndiceA), VI.Tild, -1)
                If TableauData(0) = "" Then
                    TableauData(0) = "Commun"
                End If
                WsLstEtaCycleTravail.Add(TableauData(0))
            Next IndiceA

            '** 5  Liste des Etablissements Fonction Support et Mesures 
            WsLstEtaMesureActivite = New List(Of String)
            ClauseSql = "SELECT DISTINCT FiltreEtablissement FROM ONIC_MISSION ORDER BY FiltreEtablissement"

            LstResultat = VirServiceServeur.RequeteSql_ToListeChar(WsNomUtilisateur, VI.PointdeVue.PVueActivite, 1, ClauseSql)
            For IndiceA = 0 To LstResultat.Count - 1
                If LstResultat(IndiceA) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(LstResultat(IndiceA), VI.Tild, -1)
                If TableauData(0) = "" Then
                    TableauData(0) = "Commun"
                End If
                WsLstEtaMesureActivite.Add(TableauData(0))
            Next IndiceA

            '** 6  Liste des Filières Emplois budgétaires 
            WsLstFilierePosteBud = New List(Of String)
            ClauseSql = "SELECT DISTINCT Filiere FROM ORG_DEFINITION ORDER BY Filiere"

            LstResultat = VirServiceServeur.RequeteSql_ToListeChar(WsNomUtilisateur, VI.PointdeVue.PVuePosteBud, 1, ClauseSql)
            For IndiceA = 0 To LstResultat.Count - 1
                If LstResultat(IndiceA) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(LstResultat(IndiceA), VI.Tild, -1)
                If TableauData(0) = "" Then
                    TableauData(0) = "Commun"
                End If
                WsLstFilierePosteBud.Add(TableauData(0))
            Next IndiceA

            '** 7  Liste des postes fonctionnels
            WsLstNiveau1PosteFct = New List(Of String)
            ClauseSql = "SELECT DISTINCT Niveau1 FROM PST_ORGANIGRAMME ORDER BY Niveau1"

            LstResultat = VirServiceServeur.RequeteSql_ToListeChar(WsNomUtilisateur, VI.PointdeVue.PVuePosteFct, 1, ClauseSql)
            For IndiceA = 0 To LstResultat.Count - 1
                If LstResultat(IndiceA) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(LstResultat(IndiceA), VI.Tild, -1)
                If TableauData(0) <> "" Then
                    WsLstNiveau1PosteFct.Add(TableauData(0))
                End If
            Next IndiceA
        End Sub

        Public Sub New(ByVal NomUtilisateur As String)
            WsNomUtilisateur = NomUtilisateur
            Call ConstituerListe()
        End Sub

    End Class
End Namespace
