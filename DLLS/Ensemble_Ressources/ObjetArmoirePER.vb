﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes

Namespace Datas
    Public Class ObjetArmoirePER
        Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates
        Private WsInstanceBd As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdDatabase
        Private WsNomUtilisateur As String
        '
        Private WsFiltreEtablissement As String
        Private WsFiltreUti As String
        Private WsFiltreActivite As System.Text.StringBuilder = Nothing

        Private WsListeAffectations As List(Of Virtualia.Net.ServiceServeur.VirRequeteType) = Nothing
        Private WsListeCarrieres As List(Of Virtualia.Net.ServiceServeur.VirRequeteType) = Nothing

        Public ReadOnly Property AffectationsFonctionnelles(ByVal Etablissement As String, ByVal N1 As String, ByVal N2 As String, ByVal N3 As String, _
                                    ByVal N4 As String, ByVal N5 As String, ByVal N6 As String) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Get
                If WsListeAffectations Is Nothing Then
                    Call Faire_Affectations()
                End If
                If WsListeAffectations Is Nothing Then
                    Return Nothing
                End If
                If Etablissement = "" And N1 = "" And N2 = "" And N3 = "" And N4 = "" And N5 = "" And N6 = "" Then
                    Return (From Ligne In WsListeAffectations Select Ligne Where Ligne.Valeurs(0) <> "" _
                            Order By Ligne.Valeurs(0), Ligne.Valeurs(1), Ligne.Valeurs(2), Ligne.Valeurs(3), Ligne.Valeurs(4), _
                            Ligne.Valeurs(5), Ligne.Valeurs(6), Ligne.Valeurs(7), Ligne.Valeurs(8)).ToList
                End If
                If Etablissement <> "" And N1 = "" And N2 = "" And N3 = "" And N4 = "" And N5 = "" And N6 = "" Then
                    Return (From Ligne In WsListeAffectations Select Ligne Where Ligne.Valeurs(0) = Etablissement _
                            Order By Ligne.Valeurs(0), Ligne.Valeurs(1), Ligne.Valeurs(2), Ligne.Valeurs(3), Ligne.Valeurs(4), _
                            Ligne.Valeurs(5), Ligne.Valeurs(6), Ligne.Valeurs(7), Ligne.Valeurs(8)).ToList
                End If
                If Etablissement <> "" And N1 <> "" And N2 = "" And N3 = "" And N4 = "" And N5 = "" And N6 = "" Then
                    Return (From Ligne In WsListeAffectations Select Ligne Where Ligne.Valeurs(0) = Etablissement And Ligne.Valeurs(1) = N1 _
                            Order By Ligne.Valeurs(0), Ligne.Valeurs(1), Ligne.Valeurs(2), Ligne.Valeurs(3), Ligne.Valeurs(4), _
                            Ligne.Valeurs(5), Ligne.Valeurs(6), Ligne.Valeurs(7), Ligne.Valeurs(8)).ToList
                End If
                If Etablissement <> "" And N1 <> "" And N2 <> "" And N3 = "" And N4 = "" And N5 = "" And N6 = "" Then
                    Return (From Ligne In WsListeAffectations Select Ligne Where Ligne.Valeurs(0) = Etablissement And Ligne.Valeurs(1) = N1 _
                        And Ligne.Valeurs(2) = N2 _
                            Order By Ligne.Valeurs(0), Ligne.Valeurs(1), Ligne.Valeurs(2), Ligne.Valeurs(3), Ligne.Valeurs(4), _
                            Ligne.Valeurs(5), Ligne.Valeurs(6), Ligne.Valeurs(7), Ligne.Valeurs(8)).ToList
                End If
                If Etablissement <> "" And N1 <> "" And N2 <> "" And N3 <> "" And N4 = "" And N5 = "" And N6 = "" Then
                    Return (From Ligne In WsListeAffectations Select Ligne Where Ligne.Valeurs(0) = Etablissement And Ligne.Valeurs(1) = N1 _
                        And Ligne.Valeurs(2) = N2 And Ligne.Valeurs(3) = N3 _
                            Order By Ligne.Valeurs(0), Ligne.Valeurs(1), Ligne.Valeurs(2), Ligne.Valeurs(3), Ligne.Valeurs(4), _
                            Ligne.Valeurs(5), Ligne.Valeurs(6), Ligne.Valeurs(7), Ligne.Valeurs(8)).ToList
                End If
                If Etablissement <> "" And N1 <> "" And N2 <> "" And N3 <> "" And N4 <> "" And N5 = "" And N6 = "" Then
                    Return (From Ligne In WsListeAffectations Select Ligne Where Ligne.Valeurs(0) = Etablissement And Ligne.Valeurs(1) = N1 _
                        And Ligne.Valeurs(2) = N2 And Ligne.Valeurs(3) = N3 And Ligne.Valeurs(4) = N4 _
                            Order By Ligne.Valeurs(0), Ligne.Valeurs(1), Ligne.Valeurs(2), Ligne.Valeurs(3), Ligne.Valeurs(4), _
                            Ligne.Valeurs(5), Ligne.Valeurs(6), Ligne.Valeurs(7), Ligne.Valeurs(8)).ToList
                End If
                If Etablissement <> "" And N1 <> "" And N2 <> "" And N3 <> "" And N4 <> "" And N5 <> "" And N6 = "" Then
                    Return (From Ligne In WsListeAffectations Select Ligne Where Ligne.Valeurs(0) = Etablissement And Ligne.Valeurs(1) = N1 _
                        And Ligne.Valeurs(2) = N2 And Ligne.Valeurs(3) = N3 And Ligne.Valeurs(4) = N4 And Ligne.Valeurs(5) = N5 _
                            Order By Ligne.Valeurs(0), Ligne.Valeurs(1), Ligne.Valeurs(2), Ligne.Valeurs(3), Ligne.Valeurs(4), _
                            Ligne.Valeurs(5), Ligne.Valeurs(6), Ligne.Valeurs(7), Ligne.Valeurs(8)).ToList
                End If
                If Etablissement <> "" And N1 <> "" And N2 <> "" And N3 <> "" And N4 <> "" And N5 <> "" And N6 <> "" Then
                    Return (From Ligne In WsListeAffectations Select Ligne Where Ligne.Valeurs(0) = Etablissement And Ligne.Valeurs(1) = N1 _
                        And Ligne.Valeurs(2) = N2 And Ligne.Valeurs(3) = N3 And Ligne.Valeurs(4) = N4 And Ligne.Valeurs(5) = N5 And Ligne.Valeurs(6) = N6 _
                            Order By Ligne.Valeurs(0), Ligne.Valeurs(1), Ligne.Valeurs(2), Ligne.Valeurs(3), Ligne.Valeurs(4), _
                            Ligne.Valeurs(5), Ligne.Valeurs(6), Ligne.Valeurs(7), Ligne.Valeurs(8)).ToList
                End If
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property SituationsAdministratives(ByVal Etablissement As String, ByVal Statut As String, ByVal Categorie As String, _
                                                           ByVal Corps As String, ByVal Grade As String) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Get
                If WsListeCarrieres Is Nothing Then
                    Call Faire_Carrieres()
                End If
                If WsListeCarrieres Is Nothing Then
                    Return Nothing
                End If
                If Etablissement = "" And Statut = "" And Categorie = "" And Corps = "" And Grade = "" Then
                    Return (From Ligne In WsListeCarrieres Select Ligne Where Ligne.Valeurs(0) <> "" _
                            Order By Ligne.Valeurs(0), Ligne.Valeurs(1), Ligne.Valeurs(2), Ligne.Valeurs(3), Ligne.Valeurs(4)).ToList
                End If
                If Etablissement <> "" And Statut = "" And Categorie = "" And Corps = "" And Grade = "" Then
                    Return (From Ligne In WsListeCarrieres Select Ligne Where Ligne.Valeurs(0) = Etablissement _
                            Order By Ligne.Valeurs(0), Ligne.Valeurs(1), Ligne.Valeurs(2), Ligne.Valeurs(3), Ligne.Valeurs(4)).ToList
                End If
                If Etablissement <> "" And Statut <> "" And Categorie = "" And Corps = "" And Grade = "" Then
                    Return (From Ligne In WsListeCarrieres Select Ligne Where Ligne.Valeurs(0) = Etablissement And Ligne.Valeurs(1) = Statut _
                            Order By Ligne.Valeurs(0), Ligne.Valeurs(1), Ligne.Valeurs(2), Ligne.Valeurs(3), Ligne.Valeurs(4)).ToList
                End If
                If Etablissement <> "" And Statut <> "" And Categorie <> "" And Corps = "" And Grade = "" Then
                    Return (From Ligne In WsListeCarrieres Select Ligne Where Ligne.Valeurs(0) = Etablissement And Ligne.Valeurs(1) = Statut _
                        And Ligne.Valeurs(2) = Categorie _
                            Order By Ligne.Valeurs(0), Ligne.Valeurs(1), Ligne.Valeurs(2), Ligne.Valeurs(3), Ligne.Valeurs(4)).ToList
                End If
                If Etablissement <> "" And Statut <> "" And Categorie <> "" And Corps <> "" And Grade = "" Then
                    Return (From Ligne In WsListeCarrieres Select Ligne Where Ligne.Valeurs(0) = Etablissement And Ligne.Valeurs(1) = Statut _
                        And Ligne.Valeurs(2) = Categorie And Ligne.Valeurs(3) = Corps _
                            Order By Ligne.Valeurs(0), Ligne.Valeurs(1), Ligne.Valeurs(2), Ligne.Valeurs(3), Ligne.Valeurs(4)).ToList
                End If
                If Etablissement <> "" And Statut <> "" And Categorie <> "" And Corps <> "" And Grade <> "" Then
                    Return (From Ligne In WsListeCarrieres Select Ligne Where Ligne.Valeurs(0) = Etablissement And Ligne.Valeurs(1) = Statut _
                        And Ligne.Valeurs(2) = Categorie And Ligne.Valeurs(3) = Corps And Ligne.Valeurs(4) = Grade _
                            Order By Ligne.Valeurs(0), Ligne.Valeurs(1), Ligne.Valeurs(2), Ligne.Valeurs(3), Ligne.Valeurs(4)).ToList
                End If
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property ListeAlphabetique(ByVal FiltreSurNom As String, ByVal SiEnActivite As Boolean) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim IndiceK As Integer = 0

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModeleRh, WsInstanceBd)
                If SiEnActivite = True Then
                    If WsFiltreActivite Is Nothing Then
                        Call Faire_Filtre_PositionsdActivite()
                    End If
                    Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", WsRhDates.DateduJour, VI.Operateurs.ET) = 2
                Else
                    Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", WsRhDates.DateduJour, VI.Operateurs.ET) = 1
                End If
                Constructeur.SiPasdeTriSurIdeDossier = True
                If SiEnActivite = True Then
                    Constructeur.NoInfoSelection(IndiceK, VI.ObjetPer.ObaActivite) = 1
                    Constructeur.ValeuraComparer(IndiceK, VI.Operateurs.OU, VI.Operateurs.Inclu, False) = WsFiltreActivite.ToString
                    IndiceK += 1
                End If
                Constructeur.NoInfoSelection(IndiceK, VI.ObjetPer.ObaCivil) = 2
                Select Case FiltreSurNom
                    Case Is <> ""
                        Constructeur.ValeuraComparer(IndiceK, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = FiltreSurNom & "%;" & FiltreSurNom.ToLower & "%"
                End Select
                Constructeur.InfoExtraite(0, VI.ObjetPer.ObaCivil, 1) = 2
                Constructeur.InfoExtraite(1, VI.ObjetPer.ObaCivil, 2) = 3
                If WsFiltreEtablissement <> "" Then
                    Constructeur.ClauseFiltreIN = ClauseFiltre_Etablissement()
                End If
                If WsFiltreUti <> "" Then
                    Constructeur.ClauseFiltreIN = WsFiltreUti
                End If
                Return VirServiceServeur.RequeteSql_ToListeType(WsNomUtilisateur, VI.PointdeVue.PVueApplicatif, 1, Constructeur.OrdreSqlDynamique)
            End Get
        End Property

        Private Sub Faire_Affectations()
            Dim LstEtablissements As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim LstAffectations As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim Ligne As Virtualia.Net.ServiceServeur.VirRequeteType
            Dim LstRes As List(Of String)
            Dim IndiceI As Integer

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModeleRh, WsInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", WsRhDates.DateduJour, VI.Operateurs.ET) = 1
            Constructeur.NoInfoSelection(0, VI.ObjetPer.ObaOrganigramme) = 1
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.SiHistoriquedeSituation = False

            Constructeur.InfoExtraite(0, VI.ObjetPer.ObaOrganigramme, 0) = 1
            Constructeur.InfoExtraite(1, VI.ObjetPer.ObaOrganigramme, 0) = 2
            Constructeur.InfoExtraite(2, VI.ObjetPer.ObaOrganigramme, 0) = 3
            Constructeur.InfoExtraite(3, VI.ObjetPer.ObaOrganigramme, 0) = 4
            Constructeur.InfoExtraite(4, VI.ObjetPer.ObaOrganigramme, 0) = 14
            Constructeur.InfoExtraite(5, VI.ObjetPer.ObaOrganigramme, 0) = 15
            Constructeur.InfoExtraite(6, VI.ObjetPer.ObaCivil, 0) = 2
            Constructeur.InfoExtraite(7, VI.ObjetPer.ObaCivil, 0) = 3
            If WsFiltreUti <> "" Then
                Constructeur.ClauseFiltreIN = WsFiltreUti
            End If

            LstAffectations = VirServiceServeur.RequeteSql_ToListeType(WsNomUtilisateur, VI.PointdeVue.PVueApplicatif, 1, Constructeur.OrdreSqlDynamique)
            If LstAffectations Is Nothing Then
                Exit Sub
            End If
            LstEtablissements = Faire_Etablissements()
            If lstEtablissements Is Nothing Then
                Exit Sub
            End If
            WsListeAffectations = New List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            For Each Affectation As Virtualia.Net.ServiceServeur.VirRequeteType In LstAffectations
                Ligne = LstEtablissements.Find(Function(Recherche) Recherche.Ide_Dossier = Affectation.Ide_Dossier)
                If Ligne IsNot Nothing Then
                    If Ligne.Valeurs.Count = 2 Then
                        Ligne.Valeurs.RemoveAt(1)
                    End If
                    If Ligne IsNot Nothing AndAlso Affectation.Valeurs.Count > 7 Then
                        LstRes = New List(Of String)
                        For IndiceI = 0 To 7
                            LstRes.Add(Affectation.Valeurs(IndiceI))
                        Next IndiceI
                        Ligne.Valeurs.AddRange(LstRes)
                        WsListeAffectations.Add(Ligne)
                    End If
                End If
            Next
        End Sub

        Private Sub Faire_Carrieres()
            Dim LstEtablissements As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim LstStatuts As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim LstGrades As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim LigneA As Virtualia.Net.ServiceServeur.VirRequeteType
            Dim LigneB As Virtualia.Net.ServiceServeur.VirRequeteType
            Dim LstRes As List(Of String)
            Dim IndiceI As Integer

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModeleRh, WsInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", WsRhDates.DateduJour, VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.SiHistoriquedeSituation = False
            Constructeur.NoInfoSelection(0, VI.ObjetPer.ObaStatut) = 1
            Constructeur.InfoExtraite(0, VI.ObjetPer.ObaStatut, 0) = 1

            LstStatuts = VirServiceServeur.RequeteSql_ToListeType(WsNomUtilisateur, VI.PointdeVue.PVueApplicatif, 1, Constructeur.OrdreSqlDynamique)
            If LstStatuts Is Nothing Then
                Exit Sub
            End If

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModeleRh, WsInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", WsRhDates.DateduJour, VI.Operateurs.ET) = 1
            Constructeur.NoInfoSelection(0, VI.ObjetPer.ObaGrade) = 1
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.SiHistoriquedeSituation = False
            Constructeur.InfoExtraite(0, VI.ObjetPer.ObaGrade, 0) = 5
            Constructeur.InfoExtraite(1, VI.ObjetPer.ObaGrade, 0) = 6
            Constructeur.InfoExtraite(2, VI.ObjetPer.ObaGrade, 0) = 1
            Constructeur.InfoExtraite(3, VI.ObjetPer.ObaCivil, 0) = 2
            Constructeur.InfoExtraite(4, VI.ObjetPer.ObaCivil, 0) = 3
            If WsFiltreUti <> "" Then
                Constructeur.ClauseFiltreIN = WsFiltreUti
            End If

            LstGrades = VirServiceServeur.RequeteSql_ToListeType(WsNomUtilisateur, VI.PointdeVue.PVueApplicatif, 1, Constructeur.OrdreSqlDynamique)
            If LstGrades Is Nothing Then
                Exit Sub
            End If
            For Each Grade As Virtualia.Net.ServiceServeur.VirRequeteType In LstGrades
                If Grade.Valeurs(0) = "" Then
                    Grade.Valeurs(0) = "(Non renseigné)"
                End If
                If Grade.Valeurs(1) = "" Then
                    Grade.Valeurs(1) = "(Non renseigné)"
                End If
            Next
            LstEtablissements = Faire_Etablissements()
            If LstEtablissements Is Nothing Then
                Exit Sub
            End If

            WsListeCarrieres = New List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            For Each Statut As Virtualia.Net.ServiceServeur.VirRequeteType In LstStatuts
                LigneA = LstEtablissements.Find(Function(Recherche) Recherche.Ide_Dossier = Statut.Ide_Dossier)
                If LigneA IsNot Nothing Then
                    If LigneA.Valeurs.Count = 2 Then
                        LigneA.Valeurs.RemoveAt(1)
                    End If
                    LigneA.Valeurs.Add(Statut.Valeurs(0))

                    LigneB = LstGrades.Find(Function(Recherche) Recherche.Ide_Dossier = Statut.Ide_Dossier)
                    If LigneB IsNot Nothing AndAlso LigneB.Valeurs.Count > 4 Then
                        LstRes = New List(Of String)
                        For IndiceI = 0 To 4
                            LstRes.Add(LigneB.Valeurs(IndiceI))
                        Next IndiceI
                        LigneA.Valeurs.AddRange(LstRes)
                        WsListeCarrieres.Add(LigneA)
                    End If
                End If
            Next
        End Sub

        Private Function Faire_Etablissements() As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModeleRh, WsInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", WsRhDates.DateduJour, VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.SiHistoriquedeSituation = False

            Constructeur.NoInfoSelection(0, VI.ObjetPer.ObaSociete) = 1
            If WsFiltreEtablissement <> "" Then
                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Inclu, False) = WsFiltreEtablissement
            End If
            Constructeur.InfoExtraite(0, VI.ObjetPer.ObaSociete, 0) = 1

            Return VirServiceServeur.RequeteSql_ToListeType(WsNomUtilisateur, VI.PointdeVue.PVueApplicatif, 1, Constructeur.OrdreSqlDynamique)
        End Function

        Private Sub Faire_Filtre_PositionsdActivite()
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim LstRes As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModeleRh, WsInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVuePosition, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.NoInfoSelection(0, 1) = 8
            Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = "Oui;"
            Constructeur.InfoExtraite(0, 1, 0) = 1

            LstRes = VirServiceServeur.RequeteSql_ToListeType(WsNomUtilisateur, VI.PointdeVue.PVuePosition, 1, Constructeur.OrdreSqlDynamique)

            WsFiltreActivite = New System.Text.StringBuilder
            If LstRes Is Nothing Then
                WsFiltreActivite.Append("")
                Exit Sub
            End If
            For Each Position As Virtualia.Net.ServiceServeur.VirRequeteType In LstRes
                WsFiltreActivite.Append(Position.Valeurs(0) & VI.PointVirgule)
            Next
        End Sub
        Private ReadOnly Property ClauseFiltre_Etablissement() As String
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim ChaineSql As String

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModeleRh, WsInstanceBd)
                Constructeur.LettreAlias = "eta"
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "01/01/" & Year(Now), "31/12/" & Year(Now), VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True
                Constructeur.SiHistoriquedeSituation = False
                Constructeur.SiForcerClauseDistinct = True
                Constructeur.NoInfoSelection(0, VI.ObjetPer.ObaSociete) = 1
                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = WsFiltreEtablissement

                ChaineSql = Constructeur.OrdreSqlDynamique
                Constructeur = Nothing
                Return ChaineSql
            End Get
        End Property

        Public Sub New(ByVal NomUtilisateur As String, ByVal Modele As Virtualia.Systeme.MetaModele.Donnees.ModeleRH, _
                         ByVal Sgbd As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdDatabase, ByVal FiltreEtablissement As String, ByVal FiltreUti As String)
            MyBase.New()
            WsNomUtilisateur = NomUtilisateur
            VirModeleRh = Modele
            WsInstanceBd = Sgbd
            WsFiltreUti = FiltreUti
            WsFiltreEtablissement = FiltreEtablissement
            WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates
        End Sub
    End Class
End Namespace

