﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetCommission
        Implements IGestionVirFicheModif(Of ACT_COMMISSION)

        Private WsListeCommissions As List(Of ACT_COMMISSION)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesCommissions(ByVal SiInterne As Boolean) As List(Of ACT_COMMISSION)
            Get
                Dim LstRes As List(Of ACT_COMMISSION)
                LstRes = (From instance In WsListeCommissions Select instance _
                                Where instance.SiInterne = SiInterne _
                                Order By instance.Intitule Ascending).ToList
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property ListeDesCommissions(ByVal SiInterne As Boolean, ByVal Lettre As String) As List(Of ACT_COMMISSION)
            Get
                If WsListeCommissions Is Nothing Then
                    Return Nothing
                End If
                Dim Predicat As New Virtualia.Ressources.Predicats.PredicateUniversel(Lettre, VI.PointdeVue.PVueCommission)
                Dim LstRes As List(Of ACT_COMMISSION)
                Dim LstTri As List(Of ACT_COMMISSION) = Nothing

                LstRes = WsListeCommissions.FindAll(AddressOf Predicat.RechercherLettre)
                If LstRes Is Nothing Then
                    Return Nothing
                End If
                LstTri = (From instance In LstRes Select instance _
                       Where instance.SiInterne = SiInterne _
                       Order By instance.Intitule Ascending).ToList

                Return LstTri
            End Get
        End Property

        Public ReadOnly Property ListeDesCommissions(ByVal SiInterne As Boolean, ByVal CritereSel As String, ByVal Valeur As String, Optional ByVal CritereTri As String = "") As List(Of ACT_COMMISSION)
            Get
                If WsListeCommissions Is Nothing Then
                    Return Nothing
                End If
                Dim LstRes As List(Of ACT_COMMISSION) = Nothing

                If CritereSel = "" Then
                    Select Case CritereTri
                        Case Is = "Activité", ""
                            LstRes = (From instance In WsListeCommissions Select instance _
                                               Where instance.SiInterne = SiInterne _
                                               Order By instance.Secteur_Activite Ascending, _
                                               instance.Organisme_Competent Ascending, _
                                               instance.Intitule Ascending).ToList
                        Case "Organisme"
                            LstRes = (From instance In WsListeCommissions Select instance _
                                                Where instance.SiInterne = SiInterne _
                                                Order By instance.Organisme_Competent Ascending, _
                                                instance.Secteur_Activite Ascending, _
                                                instance.Intitule Ascending).ToList
                    End Select
                    Return LstRes
                End If

                Select Case CritereSel
                    Case Is = "Activité"
                        Select Case CritereTri
                            Case Is = "" 'Intitulé
                                LstRes = (From instance In WsListeCommissions Select instance _
                                                Where instance.SiInterne = SiInterne _
                                                       And instance.Secteur_Activite = Valeur _
                                                Order By instance.Intitule Ascending).ToList

                            Case Is = "Organisme"
                                LstRes = (From instance In WsListeCommissions Select instance _
                                        Where instance.SiInterne = SiInterne _
                                                And instance.Secteur_Activite = Valeur _
                                        Order By instance.Organisme_Competent Ascending, _
                                       instance.Intitule Ascending).ToList
                        End Select

                    Case Is = "Numéro"
                        Select Case CritereTri
                            Case Is = "" 'Intitulé
                                LstRes = (From instance In WsListeCommissions Select instance _
                                                Where instance.SiInterne = SiInterne _
                                                       And instance.Numero <> "" _
                                                Order By instance.Intitule Ascending).ToList

                        End Select

                    Case Is = "Organisme"
                        Select Case CritereTri
                            Case Is = "" 'Intitulé
                                LstRes = (From instance In WsListeCommissions Select instance _
                                                Where instance.SiInterne = SiInterne _
                                                    And instance.Organisme_Competent = Valeur _
                                                Order By instance.Intitule Ascending).ToList

                            Case Is = "Activité"
                                LstRes = (From instance In WsListeCommissions Select instance _
                                        Where instance.SiInterne = SiInterne _
                                                And instance.Organisme_Competent = Valeur _
                                        Order By instance.Secteur_Activite Ascending, _
                                       instance.Intitule Ascending).ToList
                        End Select

                End Select
                If LstRes Is Nothing Then
                    Return WsListeCommissions
                End If
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredeCommissions() As Integer Implements IGestionVirFiche(Of ACT_COMMISSION).NombredItems
            Get
                Return WsListeCommissions.Count
            End Get
        End Property

        Public Function Fiche_Commission(ByVal Ide_Dossier As Integer) As ACT_COMMISSION Implements IGestionVirFiche(Of ACT_COMMISSION).GetFiche
            Return WsListeCommissions.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Commission(ByVal Intitule As String) As ACT_COMMISSION Implements IGestionVirFiche(Of ACT_COMMISSION).GetFiche
            Return WsListeCommissions.Find(Function(m) m.Intitule = Intitule)
        End Function

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of ACT_COMMISSION).SupprimerItem
            Dim FicheCommission As ACT_COMMISSION
            FicheCommission = Fiche_Commission(Ide)
            If FicheCommission IsNot Nothing Then
                WsListeCommissions.Remove(FicheCommission)
            End If
        End Sub

        Public Sub AjouterItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of ACT_COMMISSION).AjouterItem
            SupprimerItem(Ide)
            ConstituerListe(Ide)
        End Sub

        Public Sub Actualiser() Implements IGestionVirFiche(Of ACT_COMMISSION).Actualiser
            WsListeCommissions.Clear()
            ConstituerListe(0)
        End Sub

        Private Sub ConstituerListe(ByVal Ide As Integer)
            WsListeCommissions.Clear()
            '** 1  Liste de toutes les Commissions
            WsListeCommissions.AddRange(CreationCollectionVIRFICHE(Of ACT_COMMISSION).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide))
            If (WsListeCommissions.Count <= 0) Then
                Exit Sub
            End If

            '** 2  Liste de toutes les fonctions
            Dim FicheCommission As ACT_COMMISSION
            Dim lstfonc As List(Of ACT_FONCTIONS) = CreationCollectionVIRFICHE(Of ACT_FONCTIONS).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstfonc.ForEach(Sub(f)
                                FicheCommission = Fiche_Commission(f.Ide_Dossier)
                                If (FicheCommission Is Nothing) Then
                                    Exit Sub
                                End If
                                FicheCommission.Ajouter_Fonction(f)
                            End Sub)

        End Sub

        Public Sub New(ByVal NomUtilisateur As String, ByVal Ide As Integer)
            WsNomUtilisateur = NomUtilisateur
            WsListeCommissions = New List(Of ACT_COMMISSION)
            ConstituerListe(Ide)
        End Sub

    End Class
End Namespace
