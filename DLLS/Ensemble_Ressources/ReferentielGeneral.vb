Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Namespace General
        Public Class Referentiel
            Private WsNomUtilisateur As String
            '*************Position **************************
            Private WsListePositions As List(Of POS_POSITION) = Nothing
            Private WsSiPositionActivite As Boolean
            Private WsCarrierePosition As String
            Private WsPourcentagePosition As String
            Private WsSiAdeduireETP_Activite As Boolean
            Private WsSiAdeduireETP_financier As Boolean
            '************** Grade ***************************
            Private WsListeGrades As List(Of GRD_GRADE) = Nothing
            Private WsGradeCategorie As String
            Private WsGradeGrille As String
            Private WsGradeCadreEmploi As String
            Private WsGradeGroupe As String
            Private WsGradeFiliere As String
            Private WsGradeSecteur As String
            Private WsGradeCorpsEna As Boolean
            Private WsGradeCorpsParticulier As Boolean
            Private WsGradeTriduCorps As String
            Private WsGradeTriduGrade As String
            Private WsCodeNomenclatureGrade As String
            Private WsCodeNomenclatureCorps As String
            '************* Conditions d'avancement de grade **********
            Private WsListeAvancementGrade As List(Of GRD_AVANCEMENT_GRADE)
            Private WsAvaGrade(2) As String
            Private WsAvaAge(2) As Integer
            Private WsAvaEchelonAtteint(2) As Integer
            Private WsAvaEchelonAnciennete(2) As Integer
            Private WsAvaAncienneteEchelon(2) As String
            Private WsAvaAncienneteGrade(2) As String
            Private WsAvaAncienneteCorps(2) As String
            Private WsAvaAncienneteCategorie(2) As String
            Private WsAvaAncienneteServices(2) As String
            '************** Absences *********************************
            Private WsListeREFAbsence As List(Of ABS_ABSENCE) = Nothing
            Private WsIntituleAbsence As String = ""
            Private WsTypeAbsence As String
            Private WsMnemoAbsence As String
            Private WsDebitCreditAbsence As String
            Private WsRegroupementAbsence As String
            Private WsDeduireRepasAbsence As String
            '************** Pr�sences ********************************
            Private WsListeREFPresence As List(Of PRE_IDENTIFICATION) = Nothing
            Private WsIntitulePresence As String = ""
            Private WsMnemoPresence As String
            Private WsRegroupementPresence As String
            Private WsBadgeagePresence As String
            '************** Sites G�ographiques **********************
            Private WslisteSiteGeo As List(Of GEO_DESCRIPTIF) = Nothing
            Private WsAdresseSiteGeo As String
            Private WsTelephoneSiteGeo As String
            '*************** Valeurs Paie ****************************
            Private WsListeREFPaie As List(Of VIR_FICHE) = Nothing
            Private WsPlancherResidence As Double
            Private WsTauxResidence As Double
            Private WsListePaieARTT As List(Of PAI_TRAVAIL) = Nothing
            Private WsAgeDepartRetraite As Integer
            Private WsAgeMiseRetraite As Integer
            Private WsListePaieMisFrais As List(Of PAI_MISSION) = Nothing
            Private WsListePaieMisEtranger As List(Of PAI_MIS_ETRANGER) = Nothing
            Private WsListePaieMisKm As List(Of PAI_INDEMNITE_KM) = Nothing
            '******************* Etablisements ***********************
            Private WsListeEtablissement As List(Of VIR_FICHE) = Nothing
            Private WsListeHoraireEtab As List(Of ETA_HORAIRE) = Nothing
            Private WsEtablissementCourant As String = ""

            Public WriteOnly Property Position() As String
                Set(ByVal value As String)
                    Call ChargerPositions(value)
                End Set
            End Property

            Public ReadOnly Property SiPositiondActivite() As Boolean
                Get
                    Return WsSiPositionActivite
                End Get
            End Property

            Public ReadOnly Property CarrierePosition() As String
                Get
                    Return WsCarrierePosition
                End Get
            End Property

            Public ReadOnly Property PourcentagePosition() As String
                Get
                    Return WsPourcentagePosition
                End Get
            End Property

            Public ReadOnly Property VacanceETPActivite() As Boolean
                Get
                    Return WsSiAdeduireETP_Activite
                End Get
            End Property

            Public ReadOnly Property VacanceETPFinancier() As Boolean
                Get
                    Return WsSiAdeduireETP_financier
                End Get
            End Property

            Public ReadOnly Property AdresseduSiteGeo(ByVal Valeur As String) As String
                Get
                    Call ChargerSitesGeo(Valeur)
                    Return WsAdresseSiteGeo
                End Get
            End Property

            Public ReadOnly Property TelephoneduSiteGeo(ByVal Valeur As String) As String
                Get
                    Call ChargerSitesGeo(Valeur)
                    Return WsTelephoneSiteGeo
                End Get
            End Property

            Public WriteOnly Property Grade() As String
                Set(ByVal value As String)
                    Call ChargerGrades(value)
                End Set
            End Property

            Public ReadOnly Property GradeCategorie() As String
                Get
                    Return WsGradeCategorie
                End Get
            End Property

            Public ReadOnly Property GradeGrille() As String
                Get
                    Return WsGradeGrille
                End Get
            End Property

            Public ReadOnly Property GradeCadreEmploi() As String
                Get
                    Return WsGradeCadreEmploi
                End Get
            End Property

            Public ReadOnly Property GradeGroupe() As String
                Get
                    Return WsGradeGroupe
                End Get
            End Property

            Public ReadOnly Property GradeFiliere() As String
                Get
                    Return WsGradeFiliere
                End Get
            End Property

            Public ReadOnly Property GradeSecteur() As String
                Get
                    Return WsGradeSecteur
                End Get
            End Property

            Public ReadOnly Property CodeNomenclatureGrade() As String
                Get
                    Return WsCodeNomenclatureGrade
                End Get
            End Property

            Public ReadOnly Property CodeNomenclatureCorps() As String
                Get
                    Return WsCodeNomenclatureCorps
                End Get
            End Property

            Public ReadOnly Property SiAppartientaCorpsEna() As Boolean
                Get
                    Return WsGradeCorpsEna
                End Get
            End Property

            Public ReadOnly Property SiAppartientaCorpsParticulier() As Boolean
                Get
                    Return WsGradeCorpsParticulier
                End Get
            End Property

            Public ReadOnly Property OrdreTriduCorps() As String
                Get
                    Return WsGradeTriduCorps
                End Get
            End Property

            Public ReadOnly Property OrdreTriduGrade() As String
                Get
                    Return WsGradeTriduGrade
                End Get
            End Property

            Public ReadOnly Property GradedeVocation(ByVal Index As Integer) As String
                Get
                    Select Case Index
                        Case 1, 2
                            Return WsAvaGrade(Index)
                        Case Else
                            Return ""
                    End Select
                End Get
            End Property

            Public ReadOnly Property ConditiondAge(ByVal Index As Integer) As Integer
                Get
                    Select Case Index
                        Case 1, 2
                            Return WsAvaAge(Index)
                        Case Else
                            Return 0
                    End Select
                End Get
            End Property

            Public ReadOnly Property ConditiondEchelonAtteint(ByVal Index As Integer) As Integer
                Get
                    Select Case Index
                        Case 1, 2
                            Return WsAvaEchelonAtteint(Index)
                        Case Else
                            Return 0
                    End Select
                End Get
            End Property

            Public ReadOnly Property ConditiondEchelonRequis(ByVal Index As Integer) As Integer
                Get
                    Select Case Index
                        Case 1, 2
                            Return WsAvaEchelonAnciennete(Index)
                        Case Else
                            Return 0
                    End Select
                End Get
            End Property

            Public ReadOnly Property ConditiondAncienneteEchelon(ByVal Index As Integer) As String
                Get
                    Select Case Index
                        Case 1, 2
                            Return WsAvaAncienneteEchelon(Index)
                        Case Else
                            Return ""
                    End Select
                End Get
            End Property

            Public ReadOnly Property ConditiondAncienneteGrade(ByVal Index As Integer) As String
                Get
                    Select Case Index
                        Case 1, 2
                            Return WsAvaAncienneteGrade(Index)
                        Case Else
                            Return ""
                    End Select
                End Get
            End Property

            Public ReadOnly Property ConditiondAncienneteCorps(ByVal Index As Integer) As String
                Get
                    Select Case Index
                        Case 1, 2
                            Return WsAvaAncienneteCorps(Index)
                        Case Else
                            Return ""
                    End Select
                End Get
            End Property

            Public ReadOnly Property ConditiondAncienneteCategorie(ByVal Index As Integer) As String
                Get
                    Select Case Index
                        Case 1, 2
                            Return WsAvaAncienneteCategorie(Index)
                        Case Else
                            Return ""
                    End Select
                End Get
            End Property

            Public ReadOnly Property ConditiondAncienneteServices(ByVal Index As Integer) As String
                Get
                    Select Case Index
                        Case 1, 2
                            Return WsAvaAncienneteServices(Index)
                        Case Else
                            Return ""
                    End Select
                End Get
            End Property

            Public ReadOnly Property PlancherResidence() As Double
                Get
                    If WsListeREFPaie Is Nothing Then
                        Call ChargerValeursPaie()
                    End If
                    Return WsPlancherResidence
                End Get
            End Property

            Public ReadOnly Property TauxResidence() As Double
                Get
                    If WsListeREFPaie Is Nothing Then
                        Call ChargerValeursPaie()
                    End If
                    Return WsTauxResidence
                End Get
            End Property

            Public ReadOnly Property ValeurAnnuellePoint(ByVal DateValeur As String) As Double
                Get
                    Dim I As Integer
                    Dim Tableaudata(0) As String

                    If WsListeREFPaie Is Nothing Then
                        Call ChargerValeursPaie()
                    End If
                    Select Case DateValeur
                        Case Is = ""
                            DateValeur = VirRhDates.DateduJour
                    End Select
                    For I = 0 To WsListeREFPaie.Count - 1
                        If WsListeREFPaie.Item(I) Is Nothing Then
                            Exit For
                        End If
                        Tableaudata = Strings.Split(WsListeREFPaie.Item(I).ContenuTable, VI.Tild, -1)
                        Select Case VirRhDates.ComparerDates(Tableaudata(0), DateValeur)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                Return VirRhFonction.ConversionDouble(Tableaudata(1)) / 100
                        End Select
                    Next I
                    Return 0
                End Get
            End Property

            Public ReadOnly Property HoraireEtablissement(ByVal DateValeur As String, ByVal Etablissement As String) As String
                Get
                    'Pr�cision sur temps de travail
                    Select Case Etablissement
                        Case Is <> WsEtablissementCourant
                            Call ChargerEtablissement(Etablissement)
                    End Select
                    For Each Fiche As ETA_HORAIRE In WsListeHoraireEtab
                        Select Case VirRhDates.ComparerDates(Fiche.Date_de_Valeur, DateValeur)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                Return Strings.Trim(Str(Val(VirRhDates.CalcHeure(Fiche.HoraireJournalier, "", 1)) / 60))
                        End Select
                    Next
                    Return Strings.Trim(Str(DateValeur))
                End Get
            End Property

            Public ReadOnly Property TypedelAbsence(ByVal Valeur As String) As String
                Get
                    Select Case Valeur
                        Case Is <> WsIntituleAbsence
                            Call ChargerAbsences(Valeur)
                    End Select
                    Return WsTypeAbsence
                End Get
            End Property

            Public ReadOnly Property MnemodelAbsence(ByVal Valeur As String) As String
                Get
                    Select Case Valeur
                        Case Is <> WsIntituleAbsence
                            Call ChargerAbsences(Valeur)
                    End Select
                    Return WsMnemoAbsence
                End Get
            End Property

            Public ReadOnly Property InfluencedelAbsence(ByVal Valeur As String) As String
                Get
                    Select Case Valeur
                        Case Is <> WsIntituleAbsence
                            Call ChargerAbsences(Valeur)
                    End Select
                    Return WsDebitCreditAbsence
                End Get
            End Property

            Public ReadOnly Property ReroupementdelAbsence(ByVal Valeur As String) As String
                Get
                    Select Case Valeur
                        Case Is <> WsIntituleAbsence
                            Call ChargerAbsences(Valeur)
                    End Select
                    Return WsRegroupementAbsence
                End Get
            End Property

            Public ReadOnly Property SiAbsenceADeduireTitreRepas(ByVal Valeur As String) As Boolean
                Get
                    Select Case Valeur
                        Case Is <> WsIntituleAbsence
                            Call ChargerAbsences(Valeur)
                    End Select
                    Select Case WsDeduireRepasAbsence
                        Case Is = "Oui"
                            Return True
                        Case Is = "Non"
                            Return False
                        Case Else
                            Return True
                    End Select
                End Get
            End Property

            Public ReadOnly Property MnemodelaPresence(ByVal Valeur As String) As String
                Get
                    Select Case Valeur
                        Case Is <> WsIntitulePresence
                            Call ChargerPresences(Valeur)
                    End Select
                    Return WsMnemoPresence
                End Get
            End Property

            Public ReadOnly Property RegroupementdelaPresence(ByVal Valeur As String) As String
                Get
                    Select Case Valeur
                        Case Is <> WsIntitulePresence
                            Call ChargerPresences(Valeur)
                    End Select
                    Return WsRegroupementPresence
                End Get
            End Property

            Public ReadOnly Property InfluenceBadgeagedelaPresence(ByVal Valeur As String) As String
                Get
                    Select Case Valeur
                        Case Is <> WsIntitulePresence
                            Call ChargerPresences(Valeur)
                    End Select
                    Return WsBadgeagePresence
                End Get
            End Property

            Private Sub ChargerGrades(ByVal Valeur As String)
                Dim IdeGrade As Integer
                Dim IndexAva As Integer

                IdeGrade = 0
                WsGradeCategorie = ""
                WsGradeGrille = ""
                WsGradeCadreEmploi = ""
                WsGradeGroupe = ""
                WsGradeFiliere = ""
                WsGradeSecteur = ""
                WsGradeCorpsEna = False
                WsGradeCorpsParticulier = False
                WsCodeNomenclatureGrade = ""
                WsCodeNomenclatureCorps = ""
                '
                For IndexAva = 0 To WsAvaGrade.Count - 1
                    WsAvaGrade(IndexAva) = ""
                    WsAvaAge(IndexAva) = 0
                    WsAvaEchelonAtteint(IndexAva) = 0
                    WsAvaEchelonAnciennete(IndexAva) = 0
                    WsAvaAncienneteEchelon(IndexAva) = "000000"
                    WsAvaAncienneteGrade(IndexAva) = "000000"
                    WsAvaAncienneteCorps(IndexAva) = "000000"
                    WsAvaAncienneteCategorie(IndexAva) = "000000"
                    WsAvaAncienneteServices(IndexAva) = "000000"
                Next IndexAva

                If WsListeGrades Is Nothing Then
                    WsListeGrades = VirRhFonction.ConvertisseurListeFiches(Of GRD_GRADE)(UnObjetEntier(VI.PointdeVue.PVueGrades, 1, 0))
                    WsListeAvancementGrade = VirRhFonction.ConvertisseurListeFiches(Of GRD_AVANCEMENT_GRADE)(UnObjetEntier(VI.PointdeVue.PVueGrades, 2, 0))
                End If

                For Each FicheGrade As GRD_GRADE In WsListeGrades
                    Select Case FicheGrade.Intitule
                        Case Is = Valeur
                            IdeGrade = FicheGrade.Ide_Dossier
                            WsGradeCategorie = FicheGrade.Categorie
                            WsGradeGrille = FicheGrade.Echelle_de_remuneration
                            WsGradeCadreEmploi = FicheGrade.Corps
                            WsGradeGroupe = FicheGrade.Groupe
                            WsGradeFiliere = FicheGrade.Filiere
                            WsGradeSecteur = FicheGrade.Secteur
                            WsGradeTriduCorps = CStr(FicheGrade.Tri_du_corps)
                            WsGradeTriduGrade = CStr(FicheGrade.Tri_du_grade)
                            Select Case FicheGrade.Ena
                                Case Is = "OUI"
                                    WsGradeCorpsEna = True
                            End Select
                            Select Case FicheGrade.Corpsparticulier
                                Case Is = "OUI"
                                    WsGradeCorpsParticulier = True
                            End Select
                            '
                            WsCodeNomenclatureGrade = FicheGrade.Nomenclature
                            WsCodeNomenclatureCorps = FicheGrade.Codecorps
                            Exit For
                    End Select
                Next

                Select Case IdeGrade
                    Case Is > 0
                        If WsListeAvancementGrade IsNot Nothing Then
                            For Each FicheAva As GRD_AVANCEMENT_GRADE In WsListeAvancementGrade
                                Select Case FicheAva.Ide_Dossier
                                    Case Is = IdeGrade
                                        IndexAva = FicheAva.Numero_ordre
                                        Select Case IndexAva
                                            Case 1 To 2
                                                Try
                                                    WsAvaGrade(IndexAva) = FicheAva.Grade_avancement
                                                    WsAvaAge(IndexAva) = FicheAva.Condition_age
                                                    If IsNumeric(FicheAva.Condition_echelonatteint) Then
                                                        WsAvaEchelonAtteint(IndexAva) = CInt(FicheAva.Condition_echelonatteint)
                                                    End If
                                                    If IsNumeric(FicheAva.Condition_echelonanciennete) Then
                                                        WsAvaEchelonAnciennete(IndexAva) = CInt(FicheAva.Condition_echelonanciennete)
                                                    End If
                                                    WsAvaAncienneteEchelon(IndexAva) = Strings.Format(FicheAva.Ancienneterequise_echelon, "000000")
                                                    WsAvaAncienneteGrade(IndexAva) = Strings.Format(FicheAva.Ancienneterequise_grade, "000000")
                                                    WsAvaAncienneteCorps(IndexAva) = Strings.Format(FicheAva.Ancienneterequise_corps, "000000")
                                                    WsAvaAncienneteCategorie(IndexAva) = Strings.Format(FicheAva.Ancienneterequise_categorie, "000000")
                                                    WsAvaAncienneteServices(IndexAva) = Strings.Format(FicheAva.Ancienneterequise_serviceseffectifs, "000000")
                                                Catch ex As Exception
                                                    Exit Select
                                                End Try
                                        End Select
                                    Case Is > IdeGrade
                                        Exit For
                                End Select
                            Next
                        End If
                End Select
            End Sub

            Private Sub ChargerPositions(ByVal Valeur As String)
                WsCarrierePosition = ""
                WsSiPositionActivite = True
                WsPourcentagePosition = ""
                If WsListePositions Is Nothing Then
                    WsListePositions = VirRhFonction.ConvertisseurListeFiches(Of POS_POSITION)(UnObjetEntier(VI.PointdeVue.PVuePosition, 1, 0))
                End If
                For Each FichePos In WsListePositions
                    Select Case FichePos.Intitule
                        Case Is = Valeur
                            WsCarrierePosition = FichePos.Interruption_de_carriere
                            WsPourcentagePosition = CStr(FichePos.Pourcentage_d_interruption)
                            WsSiAdeduireETP_Activite = FichePos.SiAdeduire_ETPActivite
                            WsSiAdeduireETP_financier = FichePos.SiAdeduire_ETPFinancier
                            Select Case FichePos.Notion_d_activite
                                Case Is <> "Oui"
                                    WsSiPositionActivite = False
                            End Select
                            Exit For
                    End Select
                Next
            End Sub

            Private Sub ChargerAbsences(ByVal Valeur As String)
                WsIntituleAbsence = ""
                WsTypeAbsence = ""
                WsMnemoAbsence = ""
                WsDebitCreditAbsence = ""
                WsRegroupementAbsence = ""
                WsDeduireRepasAbsence = ""
                If WsListeREFAbsence Is Nothing Then
                    WsListeREFAbsence = VirRhFonction.ConvertisseurListeFiches(Of ABS_ABSENCE)(UnObjetEntier(VI.PointdeVue.PVueAbsences, 1, 0))
                End If
                For Each FicheAbs As ABS_ABSENCE In WsListeREFAbsence
                    Select Case FicheAbs.Intitule
                        Case Is = Valeur
                            WsIntituleAbsence = FicheAbs.Intitule
                            WsMnemoAbsence = FicheAbs.Mnemonique 'Code mn�monique
                            WsDebitCreditAbsence = FicheAbs.Influence_DebitCredit 'Influence d�bit-Cr�dit
                            WsRegroupementAbsence = FicheAbs.Regroupement 'Regroupement
                            WsDeduireRepasAbsence = FicheAbs.SiaDeduire_TitreRepas 'A d�duire des titres repas
                            WsTypeAbsence = FicheAbs.Categorie 'Cat�gorie
                            Select Case Strings.InStr(Valeur, "Accid")
                                Case Is > 0
                                    WsTypeAbsence = "Accident"
                            End Select
                            Select Case Strings.InStr(Valeur, "Maladie")
                                Case Is > 0
                                    WsTypeAbsence = "Maladie"
                            End Select
                            Select Case Strings.InStr(Valeur, "Cong�s")
                                Case Is > 0
                                    WsTypeAbsence = "Cong�s"
                            End Select
                            Exit For
                    End Select
                Next
            End Sub

            Private Sub ChargerPresences(ByVal Valeur As String)
                WsIntitulePresence = ""
                If WsListeREFPresence Is Nothing Then
                    WsListeREFPresence = VirRhFonction.ConvertisseurListeFiches(Of PRE_IDENTIFICATION)(UnObjetEntier(VI.PointdeVue.PVuePresences, 1, 0))
                End If
                For Each FicheREF As PRE_IDENTIFICATION In WsListeREFPresence
                    Select Case FicheREF.Intitule
                        Case Is = Valeur
                            WsIntitulePresence = Valeur
                            WsMnemoPresence = FicheREF.Mnemonique 'Code mn�monique
                            WsBadgeagePresence = FicheREF.Influence_Badgeage 'Influence sur badgeage
                            WsRegroupementPresence = FicheREF.Regroupement 'Regroupement
                            Exit For
                    End Select
                Next
            End Sub

            Private Sub ChargerSitesGeo(ByVal Valeur As String)
                WsAdresseSiteGeo = ""
                WsTelephoneSiteGeo = ""
                If WslisteSiteGeo Is Nothing Then
                    WslisteSiteGeo = VirRhFonction.ConvertisseurListeFiches(Of GEO_DESCRIPTIF)(UnObjetEntier(VI.PointdeVue.PVueSiteGeo, 1, 0))
                End If
                For Each Fiche As GEO_DESCRIPTIF In WslisteSiteGeo
                    Select Case Fiche.Identification
                        Case Is = Valeur
                            WsAdresseSiteGeo = Fiche.Adresse
                            WsTelephoneSiteGeo = Fiche.Telephone
                            Exit For
                    End Select
                Next
            End Sub

            Private Sub ChargerValeursPaie()
                Dim ZoneResidence As Integer
                Dim TauxResidence As Double
                Dim IndiceO As Integer
                Dim TableauData(0) As String

                ZoneResidence = 1
                WsPlancherResidence = 289
                TauxResidence = 3
                'Zone de r�sidence
                WsListeREFPaie = UnObjetEntier(VI.PointdeVue.PVuePaie, 4, 0)
                If WsListeREFPaie IsNot Nothing Then
                    For IndiceO = 0 To WsListeREFPaie.Count - 1
                        If WsListeREFPaie.Item(IndiceO) Is Nothing Then
                            Exit For
                        End If
                        TableauData = Strings.Split(WsListeREFPaie.Item(IndiceO).ContenuTable, VI.Tild, -1)
                        Select Case Val(TableauData(1))
                            Case 1 To 3
                                ZoneResidence = CInt(TableauData(1))
                                Exit For
                            Case Else
                                ZoneResidence = 1
                        End Select
                    Next IndiceO
                End If

                'Indemnit� de r�sidence
                WsListeREFPaie = UnObjetEntier(VI.PointdeVue.PVuePaie, 9, 0)
                If WsListeREFPaie IsNot Nothing Then
                    For IndiceO = 0 To WsListeREFPaie.Count - 1
                        If WsListeREFPaie.Item(IndiceO) Is Nothing Then
                            Exit For
                        End If
                        TableauData = Strings.Split(WsListeREFPaie.Item(IndiceO).ContenuTable, VI.Tild, -1)
                        WsPlancherResidence = Val(TableauData(2))
                        WsTauxResidence = VirRhFonction.ConversionDouble(TableauData(ZoneResidence + 2)) / 100
                        Exit For
                    Next IndiceO
                End If

                'Temps de travail L�gal
                WsListePaieARTT = VirRhFonction.ConvertisseurListeFiches(Of PAI_TRAVAIL)(UnObjetEntier(VI.PointdeVue.PVuePaie, 15, 0))
                If WsListePaieARTT IsNot Nothing Then
                    For IndiceO = 0 To WsListePaieARTT.Count - 1
                        TableauData = Strings.Split(WsListePaieARTT.Item(IndiceO).ContenuTable, VI.Tild, -1)
                        WsAgeDepartRetraite = CInt(TableauData(4))
                        WsAgeMiseRetraite = CInt(TableauData(5))
                        Exit For
                    Next IndiceO
                End If
                'Valeur du point
                WsListeREFPaie = UnObjetEntier(VI.PointdeVue.PVuePaie, 6, 0)

            End Sub

            Public ReadOnly Property HoraireMensuelLegal(ByVal DateValeur As String) As Double
                Get
                    If WsListeREFPaie Is Nothing Then
                        Call ChargerValeursPaie()
                    End If
                    Select Case DateValeur
                        Case Is = ""
                            DateValeur = VirRhDates.DateduJour
                    End Select
                    For Each Fiche As PAI_TRAVAIL In WsListePaieARTT
                        Select Case VirRhDates.ComparerDates(Fiche.Date_de_Valeur, DateValeur)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                Return Fiche.Horaire_Mensuel
                        End Select
                    Next
                    Return 0
                End Get
            End Property

            Public ReadOnly Property HoraireHebdomadadaireLegal(ByVal DateValeur As String) As Double
                Get
                    If WsListeREFPaie Is Nothing Then
                        Call ChargerValeursPaie()
                    End If
                    Select Case DateValeur
                        Case Is = ""
                            DateValeur = VirRhDates.DateduJour
                    End Select
                    For Each Fiche As PAI_TRAVAIL In WsListePaieARTT
                        Select Case VirRhDates.ComparerDates(Fiche.Date_de_Valeur, DateValeur)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                Return Fiche.Horaire_Hebdomadaire
                        End Select
                    Next
                    Return 0
                End Get
            End Property

            Public ReadOnly Property ListeMissionFrais As List(Of PAI_MISSION)
                Get
                    If WsListePaieMisFrais Is Nothing Then
                        WsListePaieMisFrais = VirRhFonction.ConvertisseurListeFiches(Of PAI_MISSION)(UnObjetEntier(VI.PointdeVue.PVuePaie, 18, 0))
                    End If
                    Return WsListePaieMisFrais
                End Get
            End Property

            Public ReadOnly Property ListeMissionKm As List(Of PAI_INDEMNITE_KM)
                Get
                    If WsListePaieMisKm Is Nothing Then
                        WsListePaieMisKm = VirRhFonction.ConvertisseurListeFiches(Of PAI_INDEMNITE_KM)(UnObjetEntier(VI.PointdeVue.PVuePaie, 12, 0))
                    End If
                    Return WsListePaieMisKm
                End Get
            End Property

            Public ReadOnly Property ListeMissionPays As List(Of PAI_MIS_ETRANGER)
                Get
                    If WsListePaieMisEtranger Is Nothing Then
                        WsListePaieMisEtranger = VirRhFonction.ConvertisseurListeFiches(Of PAI_MIS_ETRANGER)(UnObjetEntier(VI.PointdeVue.PVuePaie, 22, 0))
                    End If
                    Return WsListePaieMisEtranger
                End Get
            End Property

            Private Sub ChargerEtablissement(ByVal Valeur As String)
                WsEtablissementCourant = ""
                If WsListeEtablissement Is Nothing Then
                    WsListeEtablissement = UnObjetEntier(VI.PointdeVue.PVueEtablissement, 1, 0)
                    WsListeHoraireEtab = VirRhFonction.ConvertisseurListeFiches(Of ETA_HORAIRE)(UnObjetEntier(VI.PointdeVue.PVueEtablissement, 2, 0))
                End If
                For Each FicheEtab In WsListeEtablissement
                    Select Case FicheEtab.V_TableauData(1).ToString
                        Case Is = Valeur
                            WsEtablissementCourant = Valeur
                            Exit For
                    End Select
                Next
            End Sub

            Private ReadOnly Property UnObjetEntier(ByVal PointdeVue As Integer, ByVal NoObjet As Integer, ByVal Identifiant As Integer) As List(Of VIR_FICHE)
                Get
                    Dim LstResultat As List(Of VIR_FICHE)
                    Select Case Identifiant
                        Case Is > 0
                            LstResultat = VirServiceServeur.LectureObjet_ToFiches(WsNomUtilisateur, PointdeVue, NoObjet, Identifiant, True)
                        Case Else
                            LstResultat = VirServiceServeur.LectureObjet_ToFiches(WsNomUtilisateur, PointdeVue, NoObjet, 0, False)
                    End Select
                    Return LstResultat
                End Get
            End Property

            Public Sub New(ByVal NomUtilisateur As String)
                WsNomUtilisateur = NomUtilisateur
            End Sub
        End Class
    End Namespace
End Namespace
