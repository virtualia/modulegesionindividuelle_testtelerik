﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetCorrespondance_CIR
        Implements IGestionVirFiche(Of TAB_CORRES_VIR_CIR_GRD)

        Private WsNomUtilisateur As String
        Private WsIdeDossier As Integer = 35
        '** Ide = 30 = ADAGE, Ide = 35 = NNE
        Private WsListeCorrespondances As List(Of TAB_CORRES_VIR_CIR_GRD) = New List(Of TAB_CORRES_VIR_CIR_GRD)

        Public ReadOnly Property NombredItems As Integer Implements IGestionVirFiche(Of TAB_CORRES_VIR_CIR_GRD).NombredItems
            Get
                If (WsListeCorrespondances Is Nothing) Then
                    Return 0
                End If

                Return WsListeCorrespondances.Count
            End Get
        End Property

        Public ReadOnly Property ListeCorrespondances As List(Of TAB_CORRES_VIR_CIR_GRD)
            Get
                Return WsListeCorrespondances
            End Get
        End Property

        Public Function FicheCorrespExterne(ByVal CodeNomenclature As String) As TAB_CORRES_VIR_CIR_GRD
            If Not (IsNumeric(CodeNomenclature)) Then
                Return Nothing
            End If
            Return WsListeCorrespondances.Find(Function(m) m.Numero_Dossier_GRD = CInt(CodeNomenclature))
        End Function

        Public Function FicheCorrespInterne(ByVal IntituleVirtualia As String) As TAB_CORRES_VIR_CIR_GRD Implements IGestionVirFiche(Of TAB_CORRES_VIR_CIR_GRD).GetFiche
            If (WsListeCorrespondances Is Nothing) Then
                Return Nothing
            End If
            Return WsListeCorrespondances.Find(Function(m) m.Valeur_Virtualia = IntituleVirtualia)
        End Function

        Public Function FicheCorresp(ByVal Ide_Dossier As Integer) As TAB_CORRES_VIR_CIR_GRD Implements IGestionVirFiche(Of TAB_CORRES_VIR_CIR_GRD).GetFiche
            If (WsListeCorrespondances Is Nothing) Then
                Return Nothing
            End If
            Return (From it As TAB_CORRES_VIR_CIR_GRD In WsListeCorrespondances Where it.Ide_Dossier = Ide_Dossier Select it).FirstOrDefault()
        End Function

        Public Sub Actualiser() Implements IGestionVirFiche(Of TAB_CORRES_VIR_CIR_GRD).Actualiser
            WsListeCorrespondances.Clear()
            ConstituerListe(WsIdeDossier)
        End Sub

        Private Sub ConstituerListe(ByVal Ide As Integer)
            WsListeCorrespondances.Clear()
            WsListeCorrespondances.AddRange(CreationCollectionVIRFICHE(Of TAB_CORRES_VIR_CIR_GRD).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide))
        End Sub

        Public Sub New(ByVal NomUtilisateur As String, ByVal SiAdage As Boolean)
            WsNomUtilisateur = NomUtilisateur
            Select Case SiAdage
                Case True
                    WsIdeDossier = 30
                Case False 'NNE par défaut
                    WsIdeDossier = 35
            End Select
            Call ConstituerListe(WsIdeDossier)
        End Sub
    End Class
End Namespace

