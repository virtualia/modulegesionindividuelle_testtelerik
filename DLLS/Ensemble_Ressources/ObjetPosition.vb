﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetPosition
        Implements IGestionVirFicheModif(Of POS_POSITION)

        Private WsListePosition As List(Of POS_POSITION)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesPositions() As List(Of POS_POSITION)
            Get
                Dim LstRes As List(Of POS_POSITION)
                LstRes = (From instance In WsListePosition Select instance _
                                Where instance.Ide_Dossier > 0 _
                                Order By instance.Intitule Ascending).ToList
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredItems() As Integer Implements IGestionVirFicheModif(Of POS_POSITION).NombredItems
            Get
                Return WsListePosition.Count
            End Get
        End Property

        Public Function Fiche_Position(ByVal Ide_Dossier As Integer) As POS_POSITION Implements IGestionVirFicheModif(Of POS_POSITION).GetFiche
            Return WsListePosition.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Position(ByVal Intitule As String) As POS_POSITION Implements IGestionVirFicheModif(Of POS_POSITION).GetFiche
            Return WsListePosition.Find(Function(m) m.Intitule = Intitule)
        End Function

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of POS_POSITION).SupprimerItem
            Dim FichePos As POS_POSITION
            FichePos = Fiche_Position(Ide)
            If FichePos IsNot Nothing Then
                WsListePosition.Remove(FichePos)
            End If
        End Sub

        Public Sub AjouterItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of POS_POSITION).AjouterItem
            SupprimerItem(Ide)
            Actualiser()
        End Sub

        Public Sub Actualiser() Implements IGestionVirFicheModif(Of POS_POSITION).Actualiser
            WsListePosition.Clear()
            ConstituerListe()
        End Sub

        Private Sub ConstituerListe()
            WsListePosition.Clear()
            WsListePosition.AddRange(CreationCollectionVIRFICHE(Of POS_POSITION).GetCollection(WsNomUtilisateur, VirServiceServeur))
        End Sub

        Public Sub New(ByVal NomUtilisateur As String)
            WsNomUtilisateur = NomUtilisateur
            WsListePosition = New List(Of POS_POSITION)
            ConstituerListe()
        End Sub

    End Class
End Namespace
