﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.TablesObjet.ShemaREF
Imports Virtualia.Systeme.MetaModele

Namespace Datas
    Public Class ObjetAbsence
        Implements IGestionVirFicheModif(Of ABS_ABSENCE)

        Private WsListeAbsence As List(Of ABS_ABSENCE)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesAbsences() As List(Of ABS_ABSENCE)
            Get
                Dim LstRes As List(Of ABS_ABSENCE)
                LstRes = (From instance In WsListeAbsence Select instance Where instance.Ide_Dossier > 0
                          Order By instance.Intitule Ascending).ToList
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredItems() As Integer Implements IGestionVirFicheModif(Of ABS_ABSENCE).NombredItems
            Get
                Return WsListeAbsence.Count
            End Get
        End Property

        Public Function Fiche_Absence(ByVal Ide_Dossier As Integer) As ABS_ABSENCE Implements IGestionVirFicheModif(Of ABS_ABSENCE).GetFiche

            Return WsListeAbsence.Find(Function(m) m.Ide_Dossier = Ide_Dossier)

        End Function

        Public Function Fiche_Absence(ByVal Intitule As String) As ABS_ABSENCE Implements IGestionVirFicheModif(Of ABS_ABSENCE).GetFiche

            Return WsListeAbsence.Find(Function(m) m.Intitule = Intitule)

        End Function

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of ABS_ABSENCE).SupprimerItem
            Dim FicheAbs As ABS_ABSENCE
            FicheAbs = Fiche_Absence(Ide)
            If FicheAbs IsNot Nothing Then
                WsListeAbsence.Remove(FicheAbs)
            End If
        End Sub

        Public Sub AjouterItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of ABS_ABSENCE).AjouterItem
            SupprimerItem(Ide)
            Actualiser()
        End Sub

        Public Sub Actualiser() Implements IGestionVirFicheModif(Of ABS_ABSENCE).Actualiser
            WsListeAbsence.Clear()
            ConstituerListe()
        End Sub

        Private Sub ConstituerListe()
            WsListeAbsence.Clear()
            WsListeAbsence.AddRange(CreationCollectionVIRFICHE(Of ABS_ABSENCE).GetCollection(WsNomUtilisateur, VirServiceServeur))
        End Sub

        Public Sub New(ByVal NomUtilisateur As String)
            WsNomUtilisateur = NomUtilisateur
            WsListeAbsence = New List(Of ABS_ABSENCE)
            ConstituerListe()
        End Sub

    End Class
End Namespace


