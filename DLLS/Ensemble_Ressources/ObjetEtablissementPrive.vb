﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.TablesObjet.ShemaREF
Imports Virtualia.Systeme.MetaModele

Namespace Datas
    Public Class ObjetEtablissementPrive
        Implements IGestionVirFiche(Of ETA_IDENTITE_PRIVE)

        Private WsListeEtablissements As List(Of ETA_IDENTITE_PRIVE) = New List(Of ETA_IDENTITE_PRIVE)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesEtablissements(ByVal Lettre As String) As List(Of ETA_IDENTITE_PRIVE)
            Get
                If WsListeEtablissements Is Nothing Then
                    Return Nothing
                End If
                Dim Predicat As New Virtualia.Ressources.Predicats.PredicateUniversel(Lettre, VI.PointdeVue.PVueEtablissement, True)
                Dim LstRes As List(Of ETA_IDENTITE_PRIVE)
                Dim LstTri As List(Of ETA_IDENTITE_PRIVE) = Nothing

                LstRes = WsListeEtablissements.FindAll(AddressOf Predicat.RechercherLettre)
                If LstRes Is Nothing Then
                    Return Nothing
                End If
                LstTri = (From instance In LstRes Select instance _
                       Where instance.Ide_Dossier > 0 _
                       Order By instance.Denomination Ascending).ToList

                Return LstTri
            End Get
        End Property

        Public ReadOnly Property ListeDesEtablissements() As List(Of ETA_IDENTITE_PRIVE)
            Get
                Dim LstRes As List(Of ETA_IDENTITE_PRIVE)
                LstRes = (From instance In WsListeEtablissements Select instance _
                                Where instance.Ide_Dossier > 0 _
                                Order By instance.Denomination Ascending).ToList
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredEtablissements() As Integer Implements IGestionVirFiche(Of ETA_IDENTITE_PRIVE).NombredItems
            Get
                Return WsListeEtablissements.Count
            End Get
        End Property

        Public Function Fiche_Etablissement(ByVal IdeDossier As Integer) As ETA_IDENTITE_PRIVE Implements IGestionVirFiche(Of ETA_IDENTITE_PRIVE).GetFiche
            Return WsListeEtablissements.Find(Function(m) m.Ide_Dossier = IdeDossier)
        End Function

        Public Function Fiche_Etablissement(ByVal Intitule As String) As ETA_IDENTITE_PRIVE Implements IGestionVirFiche(Of ETA_IDENTITE_PRIVE).GetFiche
            Return WsListeEtablissements.Find(Function(m) m.Denomination = Intitule)
        End Function

        Public Sub Actualiser() Implements IGestionVirFiche(Of ETA_IDENTITE_PRIVE).Actualiser
            WsListeEtablissements.Clear()
            WsListeEtablissements = Nothing
            ConstituerListe()
        End Sub

        Private Sub ConstituerListe()
            WsListeEtablissements.Clear()
            Dim FicheIdentite As ETA_IDENTITE_PRIVE

            '** 1  Liste de tous les établissements
            WsListeEtablissements.AddRange(CreationCollectionVIRFICHE(Of ETA_IDENTITE_PRIVE).GetCollection(WsNomUtilisateur, VirServiceServeur))
            If (WsListeEtablissements.Count <= 0) Then
                Return
            End If

            '** 2  Liste de tous les horaires
            Dim lsthor As List(Of ETA_HORAIRE) = CreationCollectionVIRFICHE(Of ETA_HORAIRE).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lsthor.ForEach(Sub(f)
                               FicheIdentite = Fiche_Etablissement(f.Ide_Dossier)
                               If FicheIdentite Is Nothing Then
                                   Return
                               End If
                               FicheIdentite.Ajouter_Horaire(f)
                           End Sub)

            '** 3  Liste de tous les accords RTT
            Dim lstacc As List(Of ETA_ACCORD_RTT) = CreationCollectionVIRFICHE(Of ETA_ACCORD_RTT).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstacc.ForEach(Sub(f)
                               FicheIdentite = Fiche_Etablissement(f.Ide_Dossier)
                               If FicheIdentite Is Nothing Then
                                   Return
                               End If
                               FicheIdentite.Ajouter_Accord(f)
                           End Sub)

            '** 4  Liste de toutes les règles de valorisation
            Dim lstvalo As List(Of ETA_REGLE_VALTEMPS) = CreationCollectionVIRFICHE(Of ETA_REGLE_VALTEMPS).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstvalo.ForEach(Sub(f)
                                FicheIdentite = Fiche_Etablissement(f.Ide_Dossier)
                                If FicheIdentite Is Nothing Then
                                    Return
                                End If
                                FicheIdentite.Ajouter_RegleValo(f)
                            End Sub)

            '** 5  Liste de toutes les règles des droits à congé
            Dim lstdc As List(Of DROITS_CONGE) = CreationCollectionVIRFICHE(Of DROITS_CONGE).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstdc.ForEach(Sub(f)
                              FicheIdentite = Fiche_Etablissement(f.Ide_Dossier)

                              If (FicheIdentite Is Nothing) Then
                                  Return
                              End If

                              FicheIdentite.Ajouter_RegleConge(f)
                          End Sub)

            '** 7  Liste de toutes les règles Débit-Crédit
            Dim lstdeb As List(Of ETA_DEBIT_CREDIT) = CreationCollectionVIRFICHE(Of ETA_DEBIT_CREDIT).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstdeb.ForEach(Sub(f)
                               FicheIdentite = Fiche_Etablissement(f.Ide_Dossier)
                               If FicheIdentite Is Nothing Then
                                   Return
                               End If
                               FicheIdentite.Ajouter_RegleDC(f)
                           End Sub)

            '** 8  Liste de toutes les structures
            Dim lststruct As List(Of ETA_STRUCTURE) = CreationCollectionVIRFICHE(Of ETA_STRUCTURE).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lststruct.ForEach(Sub(f)
                                  FicheIdentite = Fiche_Etablissement(f.Ide_Dossier)
                                  If FicheIdentite Is Nothing Then
                                      Return
                                  End If
                                  FicheIdentite.Ajouter_Structure(f)
                              End Sub)

            '** 9  Liste de tous les paramètres
            Dim lstparam As List(Of ETA_PARAMETRES) = CreationCollectionVIRFICHE(Of ETA_PARAMETRES).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstparam.ForEach(Sub(f)
                                 FicheIdentite = Fiche_Etablissement(f.Ide_Dossier)
                                 If FicheIdentite Is Nothing Then
                                     Return
                                 End If
                                 FicheIdentite.Ajouter_Parametre(f)
                             End Sub)

            '** 10  Liste des règles de répartition des effectifs
            Dim lsteff As List(Of ETA_REGLE_EFFECTIF) = CreationCollectionVIRFICHE(Of ETA_REGLE_EFFECTIF).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lsteff.ForEach(Sub(f)
                               FicheIdentite = Fiche_Etablissement(f.Ide_Dossier)
                               If FicheIdentite Is Nothing Then
                                   Return
                               End If
                               FicheIdentite.Ajouter_RegleEffectif(f)
                           End Sub)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String)
            WsNomUtilisateur = NomUtilisateur
            Call ConstituerListe()
        End Sub

    End Class
End Namespace
