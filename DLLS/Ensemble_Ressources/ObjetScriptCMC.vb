﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF
Imports Virtualia.Systeme.MetaModele.Donnees
Imports Virtualia.Systeme.Parametrage.BasesdeDonnees

Namespace Datas
    Public Class ObjetScriptCMC
        Implements IGestionVirFiche(Of CMC_DESCRIPTIF)
        Private WsInstanceSgBd As ParamSgbdDatabase
        Private WsListeScript As List(Of CMC_DESCRIPTIF)
        Private WsNomUtilisateur As String
        Private WsPtDevue As Integer

        Public ReadOnly Property ListeDesScripts(Optional ByVal CritereSel As String = "", Optional ByVal CritereTri As String = "") As List(Of CMC_DESCRIPTIF)
            Get
                If WsListeScript Is Nothing Then
                    Return Nothing
                End If

                If CritereSel = "" Then
                    Select Case CritereTri
                        Case Is = ""
                            Return WsListeScript
                        Case "Catégorie"
                            Return WsListeScript.OrderBy(Function(m) m.CategorieRH).ToList
                        Case "Intitulé"
                            Return WsListeScript.OrderBy(Function(m) m.Intitule).ToList
                    End Select
                End If
                Dim LstRes As List(Of CMC_DESCRIPTIF) = Nothing
                Select Case CritereSel
                    Case Is = "Catégorie"
                        Select Case CritereTri
                            Case Is = ""
                                Return WsListeScript.FindAll(Function(m) m.CategorieRH = CInt(CritereSel)).ToList
                            Case Else
                                Select Case CritereTri
                                    Case "Catégorie"
                                        LstRes = (From instance In WsListeScript Select instance _
                                                Where instance.CategorieRH = CInt(CritereSel) _
                                                Order By instance.CategorieRH Ascending).ToList
                                    Case "Intitulé"
                                        LstRes = (From instance In WsListeScript Select instance _
                                                Where instance.CategorieRH = CInt(CritereSel) _
                                                Order By instance.Intitule Ascending).ToList
                                End Select
                        End Select
                    Case Is = "Intitulé"
                        Select Case CritereTri
                            Case Is = ""
                                Return WsListeScript.FindAll(Function(m) m.Intitule = CritereSel).ToList
                            Case Else
                                Select Case CritereTri
                                    Case "Catégorie"
                                        LstRes = (From instance In WsListeScript Select instance _
                                                Where instance.Intitule = CritereSel _
                                                Order By instance.CategorieRH Ascending).ToList
                                    Case "Intitulé"
                                        LstRes = (From instance In WsListeScript Select instance _
                                                Where instance.Intitule = CritereSel _
                                                Order By instance.Intitule Ascending).ToList
                                End Select
                        End Select
                End Select
                If LstRes Is Nothing Then
                    Return WsListeScript
                End If
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredeScripts() As Integer Implements IGestionVirFiche(Of CMC_DESCRIPTIF).NombredItems
            Get
                Return WsListeScript.Count
            End Get
        End Property

        Private Sub LireDossier(ByVal Ide As Integer)
            Dim IndiceO As Integer
            Dim IndiceNb As Integer
            Dim NumObjet As Integer = 0
            Dim TableauData(0) As String
            Dim TableauObjet As List(Of String)
            Dim FicheScript As CMC_DESCRIPTIF = Nothing
            Dim FicheSelection As CMC_SELECTION = Nothing
            Dim FicheValeur As CMC_VALEURS = Nothing
            Dim FicheSql As CMC_SQL = Nothing
            Dim LstNoObjets As New List(Of Integer)

            For IndiceO = 1 To 4
                LstNoObjets.Add(CInt(IndiceO))
            Next IndiceO
            TableauObjet = VirServiceServeur.LectureDossier_ToListeChar(WsNomUtilisateur, VI.PointdeVue.PVueScriptCmc, Ide, False, LstNoObjets)

            For IndiceO = 0 To TableauObjet.Count - 1
                If TableauObjet(IndiceO) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(TableauObjet(IndiceO), VI.Tild, -1)
                Select Case Strings.Left(TableauObjet(IndiceO), 6)
                    Case Is = "Objet="
                        NumObjet = CInt(Strings.Right(TableauData(0), TableauData(0).Length - 6))
                    Case Else
                        Select Case NumObjet
                            Case 1
                                FicheScript = New CMC_DESCRIPTIF
                                FicheScript.ContenuTable = TableauObjet(IndiceO)
                                WsListeScript.Add(FicheScript)
                            Case 2
                                FicheSelection = New CMC_SELECTION
                                FicheSelection.ContenuTable = TableauObjet(IndiceO)
                                IndiceNb = FicheScript.Ajouter_Selection(FicheSelection)
                            Case 3
                                FicheValeur = New CMC_VALEURS
                                FicheValeur.ContenuTable = TableauObjet(IndiceO)
                                FicheSelection = FicheScript.Fiche_Selection(FicheValeur.Numero_LigneIndice)
                                If FicheSelection IsNot Nothing Then
                                    IndiceNb = FicheSelection.Ajouter_Valeur(FicheValeur)
                                End If
                            Case 4
                                FicheSql = New CMC_SQL
                                FicheSql.ContenuTable = TableauObjet(IndiceO)
                                FicheScript.Fiche_Sql = FicheSql
                        End Select
                End Select
            Next IndiceO
        End Sub

        Public Sub Actualiser() Implements IGestionVirFiche(Of CMC_DESCRIPTIF).Actualiser
            WsListeScript.Clear()
            WsListeScript = Nothing
            ConstituerListe()
        End Sub

        Public Function GetFiche(ByVal Ide_Dossier As Integer) As CMC_DESCRIPTIF Implements IGestionVirFiche(Of CMC_DESCRIPTIF).GetFiche
            Return (From it As CMC_DESCRIPTIF In WsListeScript Where it.Ide_Dossier = Ide_Dossier Select it).FirstOrDefault()
        End Function

        Public Function GetFiche(ByVal Intitule As String) As CMC_DESCRIPTIF Implements IGestionVirFiche(Of CMC_DESCRIPTIF).GetFiche
            Return (From it As CMC_DESCRIPTIF In WsListeScript Where it.Intitule = Intitule Select it).FirstOrDefault()
        End Function

        Private Sub ConstituerListe()
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim LstResultat As List(Of String)
            Dim IndiceA As Integer
            Dim TableauData(0) As String
            Dim Ide As Integer

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModeleRh, WsInstanceSgBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueScriptCmc, "", "", VI.Operateurs.ET) = 2
            Constructeur.SiPasdeTriSurIdeDossier = True
            Constructeur.NoInfoSelection(0, 1) = 1
            Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = WsPtDevue.ToString
            Constructeur.NoInfoSelection(1, 1) = 3
            Select Case WsNomUtilisateur
                Case Is = "Virtualia"
                    Constructeur.ValeuraComparer(1, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = "Virtualia" & VI.PointVirgule & "VStandard"
                Case Else
                    Constructeur.ValeuraComparer(1, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = "VStandard" & VI.PointVirgule & WsNomUtilisateur
            End Select
            Constructeur.InfoExtraite(0, 1, 0) = 1
            Constructeur.InfoExtraite(1, 1, 2) = 2
            Constructeur.InfoExtraite(2, 1, 0) = 3
            Constructeur.InfoExtraite(3, 1, 1) = 8

            LstResultat = VirServiceServeur.RequeteSql_ToListeChar(WsNomUtilisateur, VI.PointdeVue.PVueConfiguration, 1, Constructeur.OrdreSqlDynamique)
            Constructeur = Nothing

            WsListeScript = New List(Of CMC_DESCRIPTIF)

            For IndiceA = 0 To LstResultat.Count - 1
                If LstResultat(IndiceA) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(LstResultat(IndiceA), VI.Tild, -1)
                If Not (IsNumeric(TableauData(0))) Then
                    Exit For
                End If
                Ide = CInt(TableauData(0))
                Call LireDossier(Ide)
            Next IndiceA
        End Sub

        Public Sub New(ByVal RhDico As ModeleRH, ByVal Sgbd As ParamSgbdDatabase, ByVal NomUtilisateur As String, ByVal PtdeVue As Integer)
            VirModeleRh = RhDico
            WsNomUtilisateur = NomUtilisateur
            WsInstanceSgBd = Sgbd
            WsPtDevue = PtdeVue
            ConstituerListe()
        End Sub

        Public Sub New(ByVal RhDico As ModeleRH, ByVal Sgbd As ParamSgbdDatabase, ByVal NomUtilisateur As String)
            Me.New(RhDico, Sgbd, NomUtilisateur, 1)
        End Sub

    End Class
End Namespace



