﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Module ModuleRessources
    Private WsClientServiceWcf As Virtualia.Net.WebService.ServiceWcfServeur = Nothing
    Private WsClientServiceWeb As Virtualia.Net.WebService.ServiceWebServeur = Nothing
    Private ModeFonctionnement As String = VI.ParametreApplication("ModeVirtualia")

    Private WsRhModele As Virtualia.Systeme.MetaModele.Donnees.ModeleRH
    Private WsRhFonction As Virtualia.Systeme.Fonctions.Generales
    Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates

    Public ReadOnly Property VirServiceServeur As Virtualia.Net.WebService.IServiceServeur
        Get
            Select Case ModeFonctionnement
                Case "WCF"
                    If WsClientServiceWcf Is Nothing Then
                        If WsRhModele Is Nothing Then
                            WsClientServiceWcf = New Virtualia.Net.WebService.ServiceWcfServeur
                            WsRhModele = WsClientServiceWcf.Instance_ModeleRH
                            WsClientServiceWcf = Nothing
                        End If
                        If IsNumeric(VI.ParametreApplication("Particularite")) Then
                            WsClientServiceWcf = New Virtualia.Net.WebService.ServiceWcfServeur(WsRhModele.InstanceProduit.ClefModele, _
                                                                                                CInt(VI.ParametreApplication("Particularite")))
                        Else
                            WsClientServiceWcf = New Virtualia.Net.WebService.ServiceWcfServeur(WsRhModele.InstanceProduit.ClefModele)
                        End If
                    End If
                    Return WsClientServiceWcf
                Case Else
                    If WsClientServiceWeb Is Nothing Then
                        If WsRhModele Is Nothing Then
                            WsClientServiceWeb = New Virtualia.Net.WebService.ServiceWebServeur(VI.ParametreApplication("WebService.VirtualiaServeur"))
                            WsRhModele = WsClientServiceWeb.Instance_ModeleRH
                            WsClientServiceWeb = Nothing
                        End If
                        If IsNumeric(VI.ParametreApplication("Particularite")) Then
                            WsClientServiceWeb = New Virtualia.Net.WebService.ServiceWebServeur(VI.ParametreApplication("WebService.VirtualiaServeur"), _
                                                            WsRhModele.InstanceProduit.ClefModele, CInt(VI.ParametreApplication("Particularite")))
                        Else
                            WsClientServiceWeb = New Virtualia.Net.WebService.ServiceWebServeur(VI.ParametreApplication("WebService.VirtualiaServeur"), WsRhModele.InstanceProduit.ClefModele)
                        End If
                    End If
                    Return WsClientServiceWeb
            End Select
        End Get
    End Property

    Public Property VirModeleRh() As Virtualia.Systeme.MetaModele.Donnees.ModeleRH
        Get
            If WsRhModele Is Nothing Then
                WsRhModele = VirServiceServeur.Instance_ModeleRH
                WsClientServiceWcf = Nothing
                WsClientServiceWeb = Nothing
            End If
            Return WsRhModele
        End Get
        Set(value As Virtualia.Systeme.MetaModele.Donnees.ModeleRH)
            WsRhModele = value
        End Set
    End Property

    Public ReadOnly Property VirRhFonction As Virtualia.Systeme.Fonctions.Generales
        Get
            If WsRhFonction Is Nothing Then
                WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
            End If
            Return WsRhFonction
        End Get
    End Property

    Public ReadOnly Property VirRhDates As Virtualia.Systeme.Fonctions.CalculDates
        Get
            If WsRhDates Is Nothing Then
                WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates
            End If
            Return WsRhDates
        End Get
    End Property
End Module
