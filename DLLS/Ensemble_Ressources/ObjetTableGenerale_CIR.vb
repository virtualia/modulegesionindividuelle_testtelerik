﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetTableGenerale_CIR
        Implements IGestionVirFiche(Of TAB_DESCRIPTION_CIR)

        Private WsListeTables As List(Of TAB_DESCRIPTION_CIR)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesTables() As List(Of TAB_DESCRIPTION_CIR)
            Get
                Dim LstRes As List(Of TAB_DESCRIPTION_CIR)
                LstRes = (From instance In WsListeTables Select instance _
                                Where instance.Ide_Dossier > 0 _
                                Order By instance.Intitule_CIR Ascending).ToList
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredeTables() As Integer Implements IGestionVirFiche(Of TAB_DESCRIPTION_CIR).NombredItems
            Get
                Return WsListeTables.Count
            End Get
        End Property

        Public Function Fiche_Table(ByVal Ide_Dossier As Integer) As TAB_DESCRIPTION_CIR Implements IGestionVirFiche(Of TAB_DESCRIPTION_CIR).GetFiche
            Return WsListeTables.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Table(ByVal Intitule As String) As TAB_DESCRIPTION_CIR Implements IGestionVirFiche(Of TAB_DESCRIPTION_CIR).GetFiche
            Return WsListeTables.Find(Function(m) m.Intitule_CIR = Intitule)
        End Function

        Public Sub Actualiser() Implements IGestionVirFiche(Of TAB_DESCRIPTION_CIR).Actualiser
            WsListeTables.Clear()
            ConstituerListe()
        End Sub

        Private Sub ConstituerListe()
            WsListeTables.Clear()
            Dim FicheTable As TAB_DESCRIPTION_CIR

            '** 1  Liste de toutes les tables
            WsListeTables.AddRange(CreationCollectionVIRFICHE(Of TAB_DESCRIPTION_CIR).GetCollection(WsNomUtilisateur, VirServiceServeur))
            If (WsListeTables.Count <= 0) Then
                Return
            End If

            '** 2  Liste de toutes les valeurs
            Dim lstval As List(Of TAB_CATALOGUE_CIR) = CreationCollectionVIRFICHE(Of TAB_CATALOGUE_CIR).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstval.ForEach(Sub(f)
                               FicheTable = Fiche_Table(f.Ide_Dossier)

                               If (FicheTable Is Nothing) Then
                                   Return
                               End If

                               FicheTable.Ajouter_Valeur(f)
                           End Sub)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String)
            WsNomUtilisateur = NomUtilisateur
            WsListeTables = New List(Of TAB_DESCRIPTION_CIR)
            ConstituerListe()
        End Sub

    End Class
End Namespace

