﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace Datas
    Public Class ItemSelection
        Private WsIdentifiant As Integer
        Private WsChampExtrait As List(Of String)
        Private WsCritereTri As String = "" 'En général Nom et Prénom et/ou Premières colonnes
        Private WsClef As String            'A priori Identifiant Et/ou Identifiant + 1er Champ
        Private SiDimensionne As Boolean
        Private SiTriParDefaut As Boolean = False

        Public ReadOnly Property Identifiant() As Integer
            Get
                Return WsIdentifiant
            End Get
        End Property

        Public Property Clef() As String
            Get
                Return WsClef
            End Get
            Set(ByVal value As String)
                WsClef = value
            End Set
        End Property

        Public WriteOnly Property AjouterUneClef() As String
            Set(ByVal value As String)
                WsClef &= value
            End Set
        End Property

        Public ReadOnly Property LigneExtraite() As String
            Get
                Dim K As Integer = 0
                Dim Ligne As New System.Text.StringBuilder
                For K = 0 To WsChampExtrait.Count - 1
                    Ligne.Append(WsChampExtrait(K).ToString & VI.Tild)
                Next K
                Ligne.Append(WsClef & VI.SigneBarre)
                Return Ligne.ToString
            End Get
        End Property

        Public Property ChampExtrait(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To WsChampExtrait.Count - 1
                        Return WsChampExtrait(Index).ToString
                    Case Else
                        Return ""
                End Select
            End Get
            Set(ByVal value As String)
                If SiDimensionne Then
                    Select Case Index
                        Case 0 To WsChampExtrait.Count - 1
                            WsChampExtrait(Index) = value
                    End Select
                Else
                    If Index = 0 Then
                        WsChampExtrait.Add(value)
                        Return
                    End If
                    If Index <= WsChampExtrait.Count - 1 Then
                        WsChampExtrait(Index) = value
                    Else
                        WsChampExtrait.Add(value)
                    End If
                End If
            End Set
        End Property

        Public ReadOnly Property NombredeChamps() As Integer
            Get
                Return WsChampExtrait.Count
            End Get
        End Property

        Public Property TriParDefaut() As Boolean
            Get
                Return SiTriParDefaut
            End Get
            Set(ByVal value As Boolean)
                SiTriParDefaut = value
            End Set
        End Property

        Public Property CritereTri() As String
            Get
                Select Case SiTriParDefaut
                    Case True
                        Dim I As Integer
                        For I = 0 To WsChampExtrait.Count - 1
                            WsCritereTri &= WsChampExtrait(I).ToString
                            If I > 6 Then
                                Exit For
                            End If
                        Next I
                End Select
                Return WsCritereTri
            End Get
            Set(ByVal value As String)
                WsCritereTri = value
            End Set
        End Property

        Public Sub New(ByVal Ide As Integer, ByVal TailleArea As Integer)
            WsIdentifiant = Ide
            WsClef = Ide.ToString
            WsChampExtrait = New List(Of String)(TailleArea)
            Dim I As Integer
            For I = 0 To TailleArea - 1
                WsChampExtrait.Add("")
            Next
            SiDimensionne = True
        End Sub

        Public Sub New(ByVal Ide As Integer)
            WsIdentifiant = Ide
            WsClef = Ide.ToString
            WsChampExtrait = New List(Of String)()
            SiDimensionne = False
        End Sub

    End Class
End Namespace
