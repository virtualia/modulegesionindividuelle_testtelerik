﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes

Namespace Comparaison
    Public Class AlphaComparaison
        Implements IComparer(Of String)

        Public Function Compare(ByVal X As String, ByVal Y As String) As Integer Implements IComparer(Of String).Compare

            If X Is Nothing Then
                If Y Is Nothing Then  '** Egalité
                    Return 0
                Else                  '** Y est plus grand
                    Return -1
                End If
            Else
                If Y Is Nothing Then   '** X est plus grand
                    Return 1
                Else
                    Return X.CompareTo(Y)
                End If
            End If
        End Function
    End Class

    Public Class NumeriqueComparaison
        Implements IComparer(Of Integer)

        Public Function Compare(ByVal X As Integer, ByVal Y As Integer) As Integer Implements IComparer(Of Integer).Compare
            Return X.CompareTo(Y)
        End Function
    End Class
End Namespace