﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace Datas
    Public Class ObjetVisualiserTout
        Private WsNomObjet As String = ""
        Private WsEtiDateDebut As String = ""
        Private WsEtiDateFin As String = ""
        Private WsEtiquette_1 As String = ""
        Private WsEtiquette_2 As String = ""
        Private WsEtiquette_3 As String = ""
        Private WsEtiquette_4 As String = ""
        Private WsEtiquette_5 As String = ""
        Private WsEtiquette_6 As String = ""
        Private WsEtiquette_7 As String = ""
        Private WsEtiquette_8 As String = ""
        Private WsEtiquette_9 As String = ""
        Private WsEtiquette_10 As String = ""
        Private WsEtiquette_11 As String = ""
        Private WsEtiquette_12 As String = ""
        Private WsEtiquette_13 As String = ""
        Private WsEtiquette_14 As String = ""
        Private WsEtiquette_15 As String = ""
        Private WsEtiquette_16 As String = ""
        Private WsEtiquette_17 As String = ""
        Private WsEtiquette_18 As String = ""
        Private WsEtiquette_19 As String = ""
        Private WsEtiquette_20 As String = ""
        Private WsEtiquette_21 As String = ""
        Private WsEtiquette_22 As String = ""
        Private WsEtiquette_23 As String = ""
        Private WsEtiquette_24 As String = ""
        Private WsEtiquette_25 As String = ""
        Private WsEtiquette_26 As String = ""
        Private WsEtiquette_27 As String = ""
        Private WsEtiquette_28 As String = ""
        Private WsEtiquette_29 As String = ""
        Private WsEtiquette_30 As String = ""
        Private WsEtiquette_31 As String = ""
        Private WsEtiquette_32 As String = ""
        Private WsEtiquette_33 As String = ""
        Private WsEtiquette_34 As String = ""
        Private WsEtiquette_35 As String = ""
        Private WsEtiquette_36 As String = ""
        Private WsEtiquette_37 As String = ""
        Private WsEtiquette_38 As String = ""
        Private WsEtiquette_39 As String = ""
        Private WsEtiquette_40 As String = ""
        Private WsEtiquette_41 As String = ""
        Private WsEtiquette_42 As String = ""
        Private WsEtiquette_43 As String = ""
        Private WsEtiquette_44 As String = ""
        Private WsEtiquette_45 As String = ""
        Private WsEtiquette_46 As String = ""
        Private WsEtiquette_47 As String = ""
        Private WsEtiquette_48 As String = ""
        Private WsEtiquette_49 As String = ""
        Private WsEtiquette_50 As String = ""
        '
        Private WsDonDateDebut As String = ""
        Private WsDonDateFin As String = ""
        Private WsColonne_1 As String = ""
        Private WsColonne_2 As String = ""
        Private WsColonne_3 As String = ""
        Private WsColonne_4 As String = ""
        Private WsColonne_5 As String = ""
        Private WsColonne_6 As String = ""
        Private WsColonne_7 As String = ""
        Private WsColonne_8 As String = ""
        Private WsColonne_9 As String = ""
        Private WsColonne_10 As String = ""
        Private WsColonne_11 As String = ""
        Private WsColonne_12 As String = ""
        Private WsColonne_13 As String = ""
        Private WsColonne_14 As String = ""
        Private WsColonne_15 As String = ""
        Private WsColonne_16 As String = ""
        Private WsColonne_17 As String = ""
        Private WsColonne_18 As String = ""
        Private WsColonne_19 As String = ""
        Private WsColonne_20 As String = ""
        Private WsColonne_21 As String = ""
        Private WsColonne_22 As String = ""
        Private WsColonne_23 As String = ""
        Private WsColonne_24 As String = ""
        Private WsColonne_25 As String = ""
        Private WsColonne_26 As String = ""
        Private WsColonne_27 As String = ""
        Private WsColonne_28 As String = ""
        Private WsColonne_29 As String = ""
        Private WsColonne_30 As String = ""
        Private WsColonne_31 As String = ""
        Private WsColonne_32 As String = ""
        Private WsColonne_33 As String = ""
        Private WsColonne_34 As String = ""
        Private WsColonne_35 As String = ""
        Private WsColonne_36 As String = ""
        Private WsColonne_37 As String = ""
        Private WsColonne_38 As String = ""
        Private WsColonne_39 As String = ""
        Private WsColonne_40 As String = ""
        Private WsColonne_41 As String = ""
        Private WsColonne_42 As String = ""
        Private WsColonne_43 As String = ""
        Private WsColonne_44 As String = ""
        Private WsColonne_45 As String = ""
        Private WsColonne_46 As String = ""
        Private WsColonne_47 As String = ""
        Private WsColonne_48 As String = ""
        Private WsColonne_49 As String = ""
        Private WsColonne_50 As String = ""

        Public ReadOnly Property NomObjet() As String
            Get
                Return WsNomObjet
            End Get
        End Property

        Public Property Etiquette_Debut() As String
            Get
                Return WsEtiDateDebut
            End Get
            Set(ByVal value As String)
                WsEtiDateDebut = value
            End Set
        End Property

        Public Property Etiquette_Fin() As String
            Get
                Return WsEtiDateFin
            End Get
            Set(ByVal value As String)
                WsEtiDateFin = value
            End Set
        End Property

        Public Property Etiquette(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0
                        Return Etiquette_1
                    Case 1
                        Return Etiquette_2
                    Case 2
                        Return Etiquette_3
                    Case 3
                        Return Etiquette_4
                    Case 4
                        Return Etiquette_5
                    Case 5
                        Return Etiquette_6
                    Case 6
                        Return Etiquette_7
                    Case 7
                        Return Etiquette_8
                    Case 8
                        Return Etiquette_9
                    Case 9
                        Return Etiquette_10
                    Case 10
                        Return Etiquette_11
                    Case 11
                        Return Etiquette_12
                    Case 12
                        Return Etiquette_13
                    Case 13
                        Return Etiquette_14
                    Case 14
                        Return Etiquette_15
                    Case 15
                        Return Etiquette_16
                    Case 16
                        Return Etiquette_17
                    Case 17
                        Return Etiquette_18
                    Case 18
                        Return Etiquette_19
                    Case 19
                        Return Etiquette_20
                    Case 20
                        Return Etiquette_21
                    Case 21
                        Return Etiquette_22
                    Case 22
                        Return Etiquette_23
                    Case 23
                        Return Etiquette_24
                    Case 24
                        Return Etiquette_25
                    Case 25
                        Return Etiquette_26
                    Case 26
                        Return Etiquette_27
                    Case 27
                        Return Etiquette_28
                    Case 28
                        Return Etiquette_29
                    Case 29
                        Return Etiquette_30
                    Case 30
                        Return Etiquette_31
                    Case 31
                        Return Etiquette_32
                    Case 32
                        Return Etiquette_33
                    Case 33
                        Return Etiquette_34
                    Case 34
                        Return Etiquette_35
                    Case 35
                        Return Etiquette_36
                    Case 36
                        Return Etiquette_37
                    Case 37
                        Return Etiquette_38
                    Case 38
                        Return Etiquette_39
                    Case 39
                        Return Etiquette_40
                    Case 40
                        Return Etiquette_41
                    Case 41
                        Return Etiquette_42
                    Case 42
                        Return Etiquette_43
                    Case 43
                        Return Etiquette_44
                    Case 44
                        Return Etiquette_45
                    Case 45
                        Return Etiquette_46
                    Case 46
                        Return Etiquette_47
                    Case 47
                        Return Etiquette_48
                    Case 48
                        Return Etiquette_49
                    Case 49
                        Return Etiquette_50
                    Case Else
                        Return ""
                End Select
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0
                        WsEtiquette_1 = value
                    Case 1
                        WsEtiquette_2 = value
                    Case 2
                        WsEtiquette_3 = value
                    Case 3
                        WsEtiquette_4 = value
                    Case 4
                        WsEtiquette_5 = value
                    Case 5
                        WsEtiquette_6 = value
                    Case 6
                        WsEtiquette_7 = value
                    Case 7
                        WsEtiquette_8 = value
                    Case 8
                        WsEtiquette_9 = value
                    Case 9
                        WsEtiquette_10 = value
                    Case 10
                        WsEtiquette_11 = value
                    Case 11
                        WsEtiquette_12 = value
                    Case 12
                        WsEtiquette_13 = value
                    Case 13
                        WsEtiquette_14 = value
                    Case 14
                        WsEtiquette_15 = value
                    Case 15
                        WsEtiquette_16 = value
                    Case 16
                        WsEtiquette_17 = value
                    Case 17
                        WsEtiquette_18 = value
                    Case 18
                        WsEtiquette_19 = value
                    Case 19
                        WsEtiquette_20 = value
                    Case 20
                        WsEtiquette_21 = value
                    Case 21
                        WsEtiquette_22 = value
                    Case 22
                        WsEtiquette_23 = value
                    Case 23
                        WsEtiquette_24 = value
                    Case 24
                        WsEtiquette_25 = value
                    Case 25
                        WsEtiquette_26 = value
                    Case 26
                        WsEtiquette_27 = value
                    Case 27
                        WsEtiquette_28 = value
                    Case 28
                        WsEtiquette_29 = value
                    Case 29
                        WsEtiquette_30 = value
                    Case 30
                        WsEtiquette_31 = value
                    Case 31
                        WsEtiquette_32 = value
                    Case 32
                        WsEtiquette_33 = value
                    Case 33
                        WsEtiquette_34 = value
                    Case 34
                        WsEtiquette_35 = value
                    Case 35
                        WsEtiquette_36 = value
                    Case 36
                        WsEtiquette_37 = value
                    Case 37
                        WsEtiquette_38 = value
                    Case 38
                        WsEtiquette_39 = value
                    Case 39
                        WsEtiquette_40 = value
                    Case 40
                        WsEtiquette_41 = value
                    Case 41
                        WsEtiquette_42 = value
                    Case 42
                        WsEtiquette_43 = value
                    Case 43
                        WsEtiquette_44 = value
                    Case 44
                        WsEtiquette_45 = value
                    Case 45
                        WsEtiquette_46 = value
                    Case 46
                        WsEtiquette_47 = value
                    Case 47
                        WsEtiquette_48 = value
                    Case 48
                        WsEtiquette_49 = value
                    Case 49
                        WsEtiquette_50 = value
                End Select
            End Set
        End Property

        Public Property Colonne_Debut() As String
            Get
                Return WsDonDateDebut
            End Get
            Set(ByVal value As String)
                WsDonDateDebut = value
            End Set
        End Property

        Public Property Colonne_Fin() As String
            Get
                Return WsDonDateFin
            End Get
            Set(ByVal value As String)
                WsDonDateFin = value
            End Set
        End Property

        Public Property Donnee(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0
                        Return Colonne_1
                    Case 1
                        Return Colonne_2
                    Case 2
                        Return Colonne_3
                    Case 3
                        Return Colonne_4
                    Case 4
                        Return Colonne_5
                    Case 5
                        Return Colonne_6
                    Case 6
                        Return Colonne_7
                    Case 7
                        Return Colonne_8
                    Case 8
                        Return Colonne_9
                    Case 9
                        Return Colonne_10
                    Case 10
                        Return Colonne_11
                    Case 11
                        Return Colonne_12
                    Case 12
                        Return Colonne_13
                    Case 13
                        Return Colonne_14
                    Case 14
                        Return Colonne_15
                    Case 15
                        Return Colonne_16
                    Case 16
                        Return Colonne_17
                    Case 17
                        Return Colonne_18
                    Case 18
                        Return Colonne_19
                    Case 19
                        Return Colonne_20
                    Case 20
                        Return Colonne_21
                    Case 21
                        Return Colonne_22
                    Case 22
                        Return Colonne_23
                    Case 23
                        Return Colonne_24
                    Case 24
                        Return Colonne_25
                    Case 25
                        Return Colonne_26
                    Case 26
                        Return Colonne_27
                    Case 27
                        Return Colonne_28
                    Case 28
                        Return Colonne_29
                    Case 29
                        Return Colonne_30
                    Case 30
                        Return Colonne_31
                    Case 31
                        Return Colonne_32
                    Case 32
                        Return Colonne_33
                    Case 33
                        Return Colonne_34
                    Case 34
                        Return Colonne_35
                    Case 35
                        Return Colonne_36
                    Case 36
                        Return Colonne_37
                    Case 37
                        Return Colonne_38
                    Case 38
                        Return Colonne_39
                    Case 39
                        Return Colonne_40
                    Case 40
                        Return Colonne_41
                    Case 41
                        Return Colonne_42
                    Case 42
                        Return Colonne_43
                    Case 43
                        Return Colonne_44
                    Case 44
                        Return Colonne_45
                    Case 45
                        Return Colonne_46
                    Case 46
                        Return Colonne_47
                    Case 47
                        Return Colonne_48
                    Case 48
                        Return Colonne_49
                    Case 49
                        Return Colonne_50
                    Case Else
                        Return ""
                End Select
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0
                        WsColonne_1 = value
                    Case 1
                        WsColonne_2 = value
                    Case 2
                        WsColonne_3 = value
                    Case 3
                        WsColonne_4 = value
                    Case 4
                        WsColonne_5 = value
                    Case 5
                        WsColonne_6 = value
                    Case 6
                        WsColonne_7 = value
                    Case 7
                        WsColonne_8 = value
                    Case 8
                        WsColonne_9 = value
                    Case 9
                        WsColonne_10 = value
                    Case 10
                        WsColonne_11 = value
                    Case 11
                        WsColonne_12 = value
                    Case 12
                        WsColonne_13 = value
                    Case 13
                        WsColonne_14 = value
                    Case 14
                        WsColonne_15 = value
                    Case 15
                        WsColonne_16 = value
                    Case 16
                        WsColonne_17 = value
                    Case 17
                        WsColonne_18 = value
                    Case 18
                        WsColonne_19 = value
                    Case 19
                        WsColonne_20 = value
                    Case 20
                        WsColonne_21 = value
                    Case 21
                        WsColonne_22 = value
                    Case 22
                        WsColonne_23 = value
                    Case 23
                        WsColonne_24 = value
                    Case 24
                        WsColonne_25 = value
                    Case 25
                        WsColonne_26 = value
                    Case 26
                        WsColonne_27 = value
                    Case 27
                        WsColonne_28 = value
                    Case 28
                        WsColonne_29 = value
                    Case 29
                        WsColonne_30 = value
                    Case 30
                        WsColonne_31 = value
                    Case 31
                        WsColonne_32 = value
                    Case 32
                        WsColonne_33 = value
                    Case 33
                        WsColonne_34 = value
                    Case 34
                        WsColonne_35 = value
                    Case 35
                        WsColonne_36 = value
                    Case 36
                        WsColonne_37 = value
                    Case 37
                        WsColonne_38 = value
                    Case 38
                        WsColonne_39 = value
                    Case 39
                        WsColonne_40 = value
                    Case 40
                        WsColonne_41 = value
                    Case 41
                        WsColonne_42 = value
                    Case 42
                        WsColonne_43 = value
                    Case 43
                        WsColonne_44 = value
                    Case 44
                        WsColonne_45 = value
                    Case 45
                        WsColonne_46 = value
                    Case 46
                        WsColonne_47 = value
                    Case 47
                        WsColonne_48 = value
                    Case 48
                        WsColonne_49 = value
                    Case 49
                        WsColonne_50 = value
                End Select
            End Set
        End Property

        Public Property Etiquette_1() As String
            Get
                Return WsEtiquette_1
            End Get
            Set(ByVal value As String)
                WsEtiquette_1 = value
            End Set
        End Property

        Public Property Etiquette_2() As String
            Get
                Return WsEtiquette_2
            End Get
            Set(ByVal value As String)
                WsEtiquette_2 = value
            End Set
        End Property

        Public Property Etiquette_3() As String
            Get
                Return WsEtiquette_3
            End Get
            Set(ByVal value As String)
                WsEtiquette_3 = value
            End Set
        End Property

        Public Property Etiquette_4() As String
            Get
                Return WsEtiquette_4
            End Get
            Set(ByVal value As String)
                WsEtiquette_4 = value
            End Set
        End Property

        Public Property Etiquette_5() As String
            Get
                Return WsEtiquette_5
            End Get
            Set(ByVal value As String)
                WsEtiquette_5 = value
            End Set
        End Property

        Public Property Etiquette_6() As String
            Get
                Return WsEtiquette_6
            End Get
            Set(ByVal value As String)
                WsEtiquette_6 = value
            End Set
        End Property

        Public Property Etiquette_7() As String
            Get
                Return WsEtiquette_7
            End Get
            Set(ByVal value As String)
                WsEtiquette_7 = value
            End Set
        End Property

        Public Property Etiquette_8() As String
            Get
                Return WsEtiquette_8
            End Get
            Set(ByVal value As String)
                WsEtiquette_8 = value
            End Set
        End Property

        Public Property Etiquette_9() As String
            Get
                Return WsEtiquette_9
            End Get
            Set(ByVal value As String)
                WsEtiquette_9 = value
            End Set
        End Property

        Public Property Etiquette_10() As String
            Get
                Return WsEtiquette_10
            End Get
            Set(ByVal value As String)
                WsEtiquette_10 = value
            End Set
        End Property

        Public Property Etiquette_11() As String
            Get
                Return WsEtiquette_11
            End Get
            Set(ByVal value As String)
                WsEtiquette_11 = value
            End Set
        End Property

        Public Property Etiquette_12() As String
            Get
                Return WsEtiquette_12
            End Get
            Set(ByVal value As String)
                WsEtiquette_12 = value
            End Set
        End Property

        Public Property Etiquette_13() As String
            Get
                Return WsEtiquette_13
            End Get
            Set(ByVal value As String)
                WsEtiquette_13 = value
            End Set
        End Property

        Public Property Etiquette_14() As String
            Get
                Return WsEtiquette_14
            End Get
            Set(ByVal value As String)
                WsEtiquette_14 = value
            End Set
        End Property

        Public Property Etiquette_15() As String
            Get
                Return WsEtiquette_15
            End Get
            Set(ByVal value As String)
                WsEtiquette_15 = value
            End Set
        End Property

        Public Property Etiquette_16() As String
            Get
                Return WsEtiquette_16
            End Get
            Set(ByVal value As String)
                WsEtiquette_16 = value
            End Set
        End Property

        Public Property Etiquette_17() As String
            Get
                Return WsEtiquette_17
            End Get
            Set(ByVal value As String)
                WsEtiquette_17 = value
            End Set
        End Property

        Public Property Etiquette_18() As String
            Get
                Return WsEtiquette_18
            End Get
            Set(ByVal value As String)
                WsEtiquette_18 = value
            End Set
        End Property

        Public Property Etiquette_19() As String
            Get
                Return WsEtiquette_19
            End Get
            Set(ByVal value As String)
                WsEtiquette_19 = value
            End Set
        End Property

        Public Property Etiquette_20() As String
            Get
                Return WsEtiquette_20
            End Get
            Set(ByVal value As String)
                WsEtiquette_20 = value
            End Set
        End Property

        Public Property Etiquette_21() As String
            Get
                Return WsEtiquette_21
            End Get
            Set(ByVal value As String)
                WsEtiquette_21 = value
            End Set
        End Property

        Public Property Etiquette_22() As String
            Get
                Return WsEtiquette_22
            End Get
            Set(ByVal value As String)
                WsEtiquette_22 = value
            End Set
        End Property

        Public Property Etiquette_23() As String
            Get
                Return WsEtiquette_23
            End Get
            Set(ByVal value As String)
                WsEtiquette_23 = value
            End Set
        End Property

        Public Property Etiquette_24() As String
            Get
                Return WsEtiquette_24
            End Get
            Set(ByVal value As String)
                WsEtiquette_24 = value
            End Set
        End Property

        Public Property Etiquette_25() As String
            Get
                Return WsEtiquette_25
            End Get
            Set(ByVal value As String)
                WsEtiquette_25 = value
            End Set
        End Property

        Public Property Etiquette_26() As String
            Get
                Return WsEtiquette_26
            End Get
            Set(ByVal value As String)
                WsEtiquette_26 = value
            End Set
        End Property

        Public Property Etiquette_27() As String
            Get
                Return WsEtiquette_27
            End Get
            Set(ByVal value As String)
                WsEtiquette_27 = value
            End Set
        End Property

        Public Property Etiquette_28() As String
            Get
                Return WsEtiquette_28
            End Get
            Set(ByVal value As String)
                WsEtiquette_28 = value
            End Set
        End Property

        Public Property Etiquette_29() As String
            Get
                Return WsEtiquette_29
            End Get
            Set(ByVal value As String)
                WsEtiquette_29 = value
            End Set
        End Property

        Public Property Etiquette_30() As String
            Get
                Return WsEtiquette_30
            End Get
            Set(ByVal value As String)
                WsEtiquette_30 = value
            End Set
        End Property

        Public Property Etiquette_31() As String
            Get
                Return WsEtiquette_31
            End Get
            Set(ByVal value As String)
                WsEtiquette_31 = value
            End Set
        End Property

        Public Property Etiquette_32() As String
            Get
                Return WsEtiquette_32
            End Get
            Set(ByVal value As String)
                WsEtiquette_32 = value
            End Set
        End Property

        Public Property Etiquette_33() As String
            Get
                Return WsEtiquette_33
            End Get
            Set(ByVal value As String)
                WsEtiquette_33 = value
            End Set
        End Property

        Public Property Etiquette_34() As String
            Get
                Return WsEtiquette_34
            End Get
            Set(ByVal value As String)
                WsEtiquette_34 = value
            End Set
        End Property

        Public Property Etiquette_35() As String
            Get
                Return WsEtiquette_35
            End Get
            Set(ByVal value As String)
                WsEtiquette_35 = value
            End Set
        End Property

        Public Property Etiquette_36() As String
            Get
                Return WsEtiquette_36
            End Get
            Set(ByVal value As String)
                WsEtiquette_36 = value
            End Set
        End Property

        Public Property Etiquette_37() As String
            Get
                Return WsEtiquette_37
            End Get
            Set(ByVal value As String)
                WsEtiquette_37 = value
            End Set
        End Property

        Public Property Etiquette_38() As String
            Get
                Return WsEtiquette_38
            End Get
            Set(ByVal value As String)
                WsEtiquette_38 = value
            End Set
        End Property

        Public Property Etiquette_39() As String
            Get
                Return WsEtiquette_39
            End Get
            Set(ByVal value As String)
                WsEtiquette_39 = value
            End Set
        End Property

        Public Property Etiquette_40() As String
            Get
                Return WsEtiquette_40
            End Get
            Set(ByVal value As String)
                WsEtiquette_40 = value
            End Set
        End Property

        Public Property Etiquette_41() As String
            Get
                Return WsEtiquette_41
            End Get
            Set(ByVal value As String)
                WsEtiquette_41 = value
            End Set
        End Property

        Public Property Etiquette_42() As String
            Get
                Return WsEtiquette_42
            End Get
            Set(ByVal value As String)
                WsEtiquette_42 = value
            End Set
        End Property

        Public Property Etiquette_43() As String
            Get
                Return WsEtiquette_43
            End Get
            Set(ByVal value As String)
                WsEtiquette_43 = value
            End Set
        End Property

        Public Property Etiquette_44() As String
            Get
                Return WsEtiquette_44
            End Get
            Set(ByVal value As String)
                WsEtiquette_44 = value
            End Set
        End Property

        Public Property Etiquette_45() As String
            Get
                Return WsEtiquette_45
            End Get
            Set(ByVal value As String)
                WsEtiquette_45 = value
            End Set
        End Property

        Public Property Etiquette_46() As String
            Get
                Return WsEtiquette_46
            End Get
            Set(ByVal value As String)
                WsEtiquette_46 = value
            End Set
        End Property

        Public Property Etiquette_47() As String
            Get
                Return WsEtiquette_47
            End Get
            Set(ByVal value As String)
                WsEtiquette_47 = value
            End Set
        End Property

        Public Property Etiquette_48() As String
            Get
                Return WsEtiquette_48
            End Get
            Set(ByVal value As String)
                WsEtiquette_48 = value
            End Set
        End Property

        Public Property Etiquette_49() As String
            Get
                Return WsEtiquette_49
            End Get
            Set(ByVal value As String)
                WsEtiquette_49 = value
            End Set
        End Property

        Public Property Etiquette_50() As String
            Get
                Return WsEtiquette_50
            End Get
            Set(ByVal value As String)
                WsEtiquette_50 = value
            End Set
        End Property

        Public Property Colonne_1() As String
            Get
                Return WsColonne_1
            End Get
            Set(ByVal value As String)
                WsColonne_1 = value
            End Set
        End Property

        Public Property Colonne_2() As String
            Get
                Return WsColonne_2
            End Get
            Set(ByVal value As String)
                WsColonne_2 = value
            End Set
        End Property

        Public Property Colonne_3() As String
            Get
                Return WsColonne_3
            End Get
            Set(ByVal value As String)
                WsColonne_3 = value
            End Set
        End Property

        Public Property Colonne_4() As String
            Get
                Return WsColonne_4
            End Get
            Set(ByVal value As String)
                WsColonne_4 = value
            End Set
        End Property

        Public Property Colonne_5() As String
            Get
                Return WsColonne_5
            End Get
            Set(ByVal value As String)
                WsColonne_5 = value
            End Set
        End Property

        Public Property Colonne_6() As String
            Get
                Return WsColonne_6
            End Get
            Set(ByVal value As String)
                WsColonne_6 = value
            End Set
        End Property

        Public Property Colonne_7() As String
            Get
                Return WsColonne_7
            End Get
            Set(ByVal value As String)
                WsColonne_7 = value
            End Set
        End Property

        Public Property Colonne_8() As String
            Get
                Return WsColonne_8
            End Get
            Set(ByVal value As String)
                WsColonne_8 = value
            End Set
        End Property

        Public Property Colonne_9() As String
            Get
                Return WsColonne_9
            End Get
            Set(ByVal value As String)
                WsColonne_9 = value
            End Set
        End Property

        Public Property Colonne_10() As String
            Get
                Return WsColonne_10
            End Get
            Set(ByVal value As String)
                WsColonne_10 = value
            End Set
        End Property

        Public Property Colonne_11() As String
            Get
                Return WsColonne_11
            End Get
            Set(ByVal value As String)
                WsColonne_11 = value
            End Set
        End Property

        Public Property Colonne_12() As String
            Get
                Return WsColonne_12
            End Get
            Set(ByVal value As String)
                WsColonne_12 = value
            End Set
        End Property

        Public Property Colonne_13() As String
            Get
                Return WsColonne_13
            End Get
            Set(ByVal value As String)
                WsColonne_13 = value
            End Set
        End Property

        Public Property Colonne_14() As String
            Get
                Return WsColonne_14
            End Get
            Set(ByVal value As String)
                WsColonne_14 = value
            End Set
        End Property

        Public Property Colonne_15() As String
            Get
                Return WsColonne_15
            End Get
            Set(ByVal value As String)
                WsColonne_15 = value
            End Set
        End Property

        Public Property Colonne_16() As String
            Get
                Return WsColonne_16
            End Get
            Set(ByVal value As String)
                WsColonne_16 = value
            End Set
        End Property

        Public Property Colonne_17() As String
            Get
                Return WsColonne_17
            End Get
            Set(ByVal value As String)
                WsColonne_17 = value
            End Set
        End Property

        Public Property Colonne_18() As String
            Get
                Return WsColonne_18
            End Get
            Set(ByVal value As String)
                WsColonne_18 = value
            End Set
        End Property

        Public Property Colonne_19() As String
            Get
                Return WsColonne_19
            End Get
            Set(ByVal value As String)
                WsColonne_19 = value
            End Set
        End Property

        Public Property Colonne_20() As String
            Get
                Return WsColonne_20
            End Get
            Set(ByVal value As String)
                WsColonne_20 = value
            End Set
        End Property

        Public Property Colonne_21() As String
            Get
                Return WsColonne_21
            End Get
            Set(ByVal value As String)
                WsColonne_21 = value
            End Set
        End Property

        Public Property Colonne_22() As String
            Get
                Return WsColonne_22
            End Get
            Set(ByVal value As String)
                WsColonne_22 = value
            End Set
        End Property

        Public Property Colonne_23() As String
            Get
                Return WsColonne_23
            End Get
            Set(ByVal value As String)
                WsColonne_23 = value
            End Set
        End Property

        Public Property Colonne_24() As String
            Get
                Return WsColonne_24
            End Get
            Set(ByVal value As String)
                WsColonne_24 = value
            End Set
        End Property

        Public Property Colonne_25() As String
            Get
                Return WsColonne_25
            End Get
            Set(ByVal value As String)
                WsColonne_25 = value
            End Set
        End Property

        Public Property Colonne_26() As String
            Get
                Return WsColonne_26
            End Get
            Set(ByVal value As String)
                WsColonne_26 = value
            End Set
        End Property

        Public Property Colonne_27() As String
            Get
                Return WsColonne_27
            End Get
            Set(ByVal value As String)
                WsColonne_27 = value
            End Set
        End Property

        Public Property Colonne_28() As String
            Get
                Return WsColonne_28
            End Get
            Set(ByVal value As String)
                WsColonne_28 = value
            End Set
        End Property

        Public Property Colonne_29() As String
            Get
                Return WsColonne_29
            End Get
            Set(ByVal value As String)
                WsColonne_29 = value
            End Set
        End Property

        Public Property Colonne_30() As String
            Get
                Return WsColonne_30
            End Get
            Set(ByVal value As String)
                WsColonne_30 = value
            End Set
        End Property

        Public Property Colonne_31() As String
            Get
                Return WsColonne_31
            End Get
            Set(ByVal value As String)
                WsColonne_31 = value
            End Set
        End Property

        Public Property Colonne_32() As String
            Get
                Return WsColonne_32
            End Get
            Set(ByVal value As String)
                WsColonne_32 = value
            End Set
        End Property

        Public Property Colonne_33() As String
            Get
                Return WsColonne_33
            End Get
            Set(ByVal value As String)
                WsColonne_33 = value
            End Set
        End Property

        Public Property Colonne_34() As String
            Get
                Return WsColonne_34
            End Get
            Set(ByVal value As String)
                WsColonne_34 = value
            End Set
        End Property

        Public Property Colonne_35() As String
            Get
                Return WsColonne_35
            End Get
            Set(ByVal value As String)
                WsColonne_35 = value
            End Set
        End Property

        Public Property Colonne_36() As String
            Get
                Return WsColonne_36
            End Get
            Set(ByVal value As String)
                WsColonne_36 = value
            End Set
        End Property

        Public Property Colonne_37() As String
            Get
                Return WsColonne_37
            End Get
            Set(ByVal value As String)
                WsColonne_37 = value
            End Set
        End Property

        Public Property Colonne_38() As String
            Get
                Return WsColonne_38
            End Get
            Set(ByVal value As String)
                WsColonne_38 = value
            End Set
        End Property

        Public Property Colonne_39() As String
            Get
                Return WsColonne_39
            End Get
            Set(ByVal value As String)
                WsColonne_39 = value
            End Set
        End Property

        Public Property Colonne_40() As String
            Get
                Return WsColonne_40
            End Get
            Set(ByVal value As String)
                WsColonne_40 = value
            End Set
        End Property

        Public Property Colonne_41() As String
            Get
                Return WsColonne_41
            End Get
            Set(ByVal value As String)
                WsColonne_41 = value
            End Set
        End Property

        Public Property Colonne_42() As String
            Get
                Return WsColonne_42
            End Get
            Set(ByVal value As String)
                WsColonne_42 = value
            End Set
        End Property

        Public Property Colonne_43() As String
            Get
                Return WsColonne_43
            End Get
            Set(ByVal value As String)
                WsColonne_43 = value
            End Set
        End Property

        Public Property Colonne_44() As String
            Get
                Return WsColonne_44
            End Get
            Set(ByVal value As String)
                WsColonne_44 = value
            End Set
        End Property

        Public Property Colonne_45() As String
            Get
                Return WsColonne_45
            End Get
            Set(ByVal value As String)
                WsColonne_45 = value
            End Set
        End Property

        Public Property Colonne_46() As String
            Get
                Return WsColonne_46
            End Get
            Set(ByVal value As String)
                WsColonne_46 = value
            End Set
        End Property

        Public Property Colonne_47() As String
            Get
                Return WsColonne_47
            End Get
            Set(ByVal value As String)
                WsColonne_47 = value
            End Set
        End Property

        Public Property Colonne_48() As String
            Get
                Return WsColonne_48
            End Get
            Set(ByVal value As String)
                WsColonne_48 = value
            End Set
        End Property

        Public Property Colonne_49() As String
            Get
                Return WsColonne_49
            End Get
            Set(ByVal value As String)
                WsColonne_49 = value
            End Set
        End Property

        Public Property Colonne_50() As String
            Get
                Return WsColonne_50
            End Get
            Set(ByVal value As String)
                WsColonne_50 = value
            End Set
        End Property

        Public Sub New(ByVal IntituleObjet As String)
            MyBase.New()
            WsNomObjet = IntituleObjet
        End Sub
    End Class
End Namespace