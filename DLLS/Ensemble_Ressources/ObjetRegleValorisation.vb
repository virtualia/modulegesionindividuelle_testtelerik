﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetRegleValorisation
        Implements IGestionVirFicheModif(Of HOR_REGLE)

        Private WsListeRegles As List(Of HOR_REGLE)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesRegles() As List(Of HOR_REGLE)
            Get
                If WsListeRegles Is Nothing Then
                    Return Nothing
                End If
                Dim LstRes As List(Of HOR_REGLE)
                LstRes = (From instance In WsListeRegles Select instance _
                                Where instance.Ide_Dossier > 0 _
                                Order By instance.Intitule Ascending).ToList

                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredeRegles() As Integer Implements IGestionVirFicheModif(Of HOR_REGLE).NombredItems
            Get
                Return WsListeRegles.Count
            End Get
        End Property

        Public Function Fiche_Regle(ByVal Ide_Dossier As Integer) As HOR_REGLE Implements IGestionVirFicheModif(Of HOR_REGLE).GetFiche
            Return WsListeRegles.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Regle(ByVal Intitule As String) As HOR_REGLE Implements IGestionVirFicheModif(Of HOR_REGLE).GetFiche
            Return WsListeRegles.Find(Function(m) m.Intitule = Intitule)
        End Function

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of HOR_REGLE).SupprimerItem
            Dim FicheRegle As HOR_REGLE
            FicheRegle = Fiche_Regle(Ide)
            If FicheRegle IsNot Nothing Then
                WsListeRegles.Remove(FicheRegle)
            End If
        End Sub

        Public Sub Actualiser() Implements IGestionVirFicheModif(Of HOR_REGLE).Actualiser
            WsListeRegles.Clear()
            ConstituerListe()
        End Sub

        Public Sub AjouterItem(Ide As Integer) Implements IGestionVirFicheModif(Of HOR_REGLE).AjouterItem
            Return
        End Sub

        Private Sub ConstituerListe()
            WsListeRegles.Clear()
            Dim FicheRegle As HOR_REGLE

            '** 1  Liste de toutes les règles
            WsListeRegles.AddRange(CreationCollectionVIRFICHE(Of HOR_REGLE).GetCollection(WsNomUtilisateur, VirServiceServeur))
            If (WsListeRegles.Count <= 0) Then
                Return
            End If

            '** 2  Liste de toutes les variables
            Dim lstvar As List(Of HOR_VARIABLES) = CreationCollectionVIRFICHE(Of HOR_VARIABLES).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstvar.ForEach(Sub(f)
                               FicheRegle = Fiche_Regle(f.Ide_Dossier)
                               If FicheRegle Is Nothing Then
                                   Return
                               End If
                               FicheRegle.Ajouter_Variable(f)
                           End Sub)

            '** 3  Liste de toutes les conditions de présence
            Dim lstcondpres As List(Of HOR_CONDITIONS_PRESENCE) = CreationCollectionVIRFICHE(Of HOR_CONDITIONS_PRESENCE).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstcondpres.ForEach(Sub(f)
                                    FicheRegle = Fiche_Regle(f.Ide_Dossier)
                                    If FicheRegle Is Nothing Then
                                        Return
                                    End If
                                    FicheRegle.Ajouter_ConditionPresence(f)
                                End Sub)

            '** 4  Liste de toutes les conditions d'absence
            Dim lstcondabs As List(Of HOR_CONDITIONS_ABSENCE) = CreationCollectionVIRFICHE(Of HOR_CONDITIONS_ABSENCE).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstcondabs.ForEach(Sub(f)
                                   FicheRegle = Fiche_Regle(f.Ide_Dossier)
                                   If FicheRegle Is Nothing Then
                                       Return
                                   End If
                                   FicheRegle.Ajouter_ConditionAbsence(f)
                               End Sub)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String)
            WsNomUtilisateur = NomUtilisateur
            WsListeRegles = New List(Of HOR_REGLE)
            ConstituerListe()
        End Sub

    End Class
End Namespace
