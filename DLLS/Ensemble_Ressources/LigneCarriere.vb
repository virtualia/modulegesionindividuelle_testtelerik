﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes

Namespace Datas
    Public Class LigneCarriere
        Private WsDateValeur As String = ""
        Private WsStatut As String = ""
        Private WsSitAdm As String = ""
        Private WsPosition As String = ""
        Private WsModaliteService As String = ""
        Private WsTauxActivite As String = ""
        Private WsGrade As String = ""
        Private WsEchelon As String = ""
        Private WsIndiceMajore As String = ""
        Private WsChevron As String = ""

        Public ReadOnly Property LigneEditee() As String
            Get
                Dim LigneComplete As New System.Text.StringBuilder
                LigneComplete.Append(DateValeur & VI.Tild)
                LigneComplete.Append(Statut & VI.Tild)
                LigneComplete.Append(SituationAdministrative & VI.Tild)
                LigneComplete.Append(PositionActivite & VI.Tild)
                LigneComplete.Append(ModaliteService & VI.Tild)
                LigneComplete.Append(TauxActivite & VI.Tild)
                LigneComplete.Append(Grade & VI.Tild)
                LigneComplete.Append(Echelon & VI.Tild)
                LigneComplete.Append(IndiceMajore & VI.Tild)
                LigneComplete.Append(DateValeur & VI.SigneBarre)
                Return LigneComplete.ToString
            End Get
        End Property

        Public ReadOnly Property LigneEditee(ByVal SiAvecChevron As Boolean) As String
            Get
                Dim LigneComplete As New System.Text.StringBuilder
                LigneComplete.Append(DateValeur & VI.Tild)
                LigneComplete.Append(Statut & VI.Tild)
                LigneComplete.Append(SituationAdministrative & VI.Tild)
                LigneComplete.Append(PositionActivite & VI.Tild)
                LigneComplete.Append(ModaliteService & VI.Tild)
                LigneComplete.Append(TauxActivite & VI.Tild)
                LigneComplete.Append(Grade & VI.Tild)
                LigneComplete.Append(Echelon & VI.Tild)
                LigneComplete.Append(Chevron & VI.Tild)
                LigneComplete.Append(IndiceMajore & VI.Tild)
                LigneComplete.Append(DateValeur & VI.SigneBarre)
                Return LigneComplete.ToString
            End Get
        End Property

        Public ReadOnly Property ValeurDate() As Date
            Get
                If WsDateValeur IsNot Nothing Then
                    If WsDateValeur <> "" Then
                        Return CDate(WsDateValeur)
                    End If
                End If
                Return Nothing
            End Get
        End Property

        Public Property DateValeur() As String
            Get
                Return WsDateValeur
            End Get
            Set(ByVal value As String)
                WsDateValeur = value
            End Set
        End Property

        Public ReadOnly Property Date_Valeur_ToDate() As Date
            Get
                If WsDateValeur = "" Then
                    Return DateValue("01/01/1950")
                Else
                    Try
                        Return DateValue(WsDateValeur)
                    Catch ex As Exception
                        Return DateValue("01/01/1950")
                    End Try
                End If
            End Get
        End Property

        Public Property Statut() As String
            Get
                Return WsStatut
            End Get
            Set(ByVal value As String)
                WsStatut = value
            End Set
        End Property

        Public Property SituationAdministrative() As String
            Get
                Return WsSitAdm
            End Get
            Set(ByVal value As String)
                WsSitAdm = value
            End Set
        End Property

        Public Property PositionActivite() As String
            Get
                Return WsPosition
            End Get
            Set(ByVal value As String)
                WsPosition = value
            End Set
        End Property

        Public Property ModaliteService() As String
            Get
                Return WsModaliteService
            End Get
            Set(ByVal value As String)
                WsModaliteService = value
            End Set
        End Property

        Public Property TauxActivite() As String
            Get
                Return WsTauxActivite
            End Get
            Set(ByVal value As String)
                WsTauxActivite = value
            End Set
        End Property

        Public Property Grade() As String
            Get
                Return WsGrade
            End Get
            Set(ByVal value As String)
                WsGrade = value
            End Set
        End Property

        Public Property Echelon() As String
            Get
                Return WsEchelon
            End Get
            Set(ByVal value As String)
                WsEchelon = value
            End Set
        End Property

        Public Property Chevron() As String
            Get
                Return WsChevron
            End Get
            Set(ByVal value As String)
                WsChevron = value
            End Set
        End Property

        Public Property IndiceMajore() As String
            Get
                Return WsIndiceMajore
            End Get
            Set(ByVal value As String)
                WsIndiceMajore = value
            End Set
        End Property

        Public Sub New(ByVal DateEffet As String)
            WsDateValeur = DateEffet
        End Sub

    End Class
End Namespace