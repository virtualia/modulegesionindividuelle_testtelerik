﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Ressources.Datas
Imports Virtualia.TablesObjet.ShemaREF
Imports Virtualia.Systeme.MetaModele

Namespace Calculs
    Public Class ObjetCalculJour
        Private WsEnsemblePays As ObjetPays
        Private WsNomUtilisateur As String
        '
        Private WsFicheJourOuv As PAI_OUVRABLE
        Private WsListeJoursFeries As List(Of PAI_FERIE)
        '
        Private WsSiGestionEtranger As Boolean
        Private WsPaysdesConges As String
        '
        'Parametres absences
        Private Const DATABS_DEBUTINCLUS As String = "Date de début incluse"
        Private Const DATABS_DEBUTAPRESMIDI As String = "Date de début après-midi"
        Private Const DATABS_FININCLUS As String = "Date de fin incluse"
        Private Const DATABS_FINMATIN As String = "Date de fin matin"

        Public ReadOnly Property ListeDesJoursFeries As List(Of PAI_FERIE)
            Get
                Return WsListeJoursFeries
            End Get
        End Property

        Public Property PaysdesConges() As String
            Get
                Return WsPaysdesConges
            End Get
            Set(ByVal value As String)
                WsPaysdesConges = value
            End Set
        End Property

        Public ReadOnly Property JourOuvrableNumeric() As Integer
            Get
                Return WsFicheJourOuv.JourOuvrableNumeric
            End Get
        End Property

        Public ReadOnly Property JourOuvrableNumeric(ByVal Pays As String) As Integer
            Get
                If Pays = "" Or Pays = "France" Then
                    Return WsFicheJourOuv.JourOuvrableNumeric
                End If
                If WsEnsemblePays Is Nothing Then
                    Return VI.Jours.Samedi
                End If
                Dim FichePays As PAYS_DESCRIPTION
                FichePays = WsEnsemblePays.Fiche_Pays(Pays)
                If FichePays Is Nothing Then
                    Return VI.Jours.Samedi
                End If
                Return FichePays.JourOuvrableNumeric
            End Get
        End Property

        Public ReadOnly Property JourReposNumeric() As Integer
            Get
                Return WsFicheJourOuv.JourReposNumeric
            End Get
        End Property

        Public ReadOnly Property JourReposNumeric(ByVal Pays As String) As Integer
            Get
                If Pays = "" Or Pays = "France" Then
                    Return WsFicheJourOuv.JourReposNumeric
                End If
                If WsEnsemblePays Is Nothing Then
                    Return VI.Jours.Dimanche
                End If
                Dim FichePays As PAYS_DESCRIPTION
                FichePays = WsEnsemblePays.Fiche_Pays(Pays)
                If FichePays Is Nothing Then
                    Return VI.Jours.Dimanche
                End If
                Return FichePays.JourReposNumeric
            End Get
        End Property

        Public ReadOnly Property SiEstUnJourFerie(ByVal ArgumentDate As String) As Boolean
            Get
                Dim IndiceI As Integer
                Dim FicheVirt As PAI_FERIE

                For IndiceI = 0 To WsListeJoursFeries.Count - 1
                    FicheVirt = WsListeJoursFeries.Item(IndiceI)
                    Select Case FicheVirt.Annee.ToString
                        Case Is = Strings.Right(ArgumentDate, 4)
                            If VirRhDates.ComparerDates(FicheVirt.Date_de_Valeur, ArgumentDate) = VI.ComparaisonDates.Egalite Then
                                If FicheVirt.Si_Plus_JourFerie = "Oui" Then
                                    Return False
                                Else
                                    Return True
                                End If
                            End If
                    End Select
                Next IndiceI
                Return VirRhDates.SiJourFerie(ArgumentDate)
            End Get
        End Property

        Public ReadOnly Property SiEstUnJourFerie(ByVal ArgumentDate As String, ByVal Pays As String) As Boolean
            Get
                If Pays = "" Or Pays = "France" Then
                    Return SiEstUnJourFerie(ArgumentDate)
                End If
                If WsEnsemblePays Is Nothing Then
                    Return False
                End If
                Dim FichePays As PAYS_DESCRIPTION
                FichePays = WsEnsemblePays.Fiche_Pays(Pays)
                If FichePays Is Nothing Then
                    Return False
                End If
                If FichePays.Fiche_JF_Fixe(ArgumentDate) Is Nothing Then
                    If FichePays.Fiche_JF_Mobile(ArgumentDate) Is Nothing Then
                        Return False
                    Else
                        Return True
                    End If
                Else
                    Return True
                End If
                Return False
            End Get
        End Property

        Public ReadOnly Property SiJourOuvre(ByVal ArgumentDate As String, ByVal SiTenirCompteJourFerie As Boolean) As Boolean
            Get
                Dim Wd As Integer
                Wd = Weekday(DateValue(ArgumentDate), FirstDayOfWeek.Monday)
                Select Case WsSiGestionEtranger
                    Case False
                        Select Case Wd
                            Case Is = JourReposNumeric
                                Return False
                            Case Is = JourOuvrableNumeric
                                Return False
                            Case Else
                                Select Case SiTenirCompteJourFerie
                                    Case False
                                        Return True
                                    Case True
                                        Select Case SiEstUnJourFerie(ArgumentDate)
                                            Case True
                                                Return False
                                            Case False
                                                Return True
                                        End Select
                                End Select
                        End Select
                    Case True
                        Select Case Wd
                            Case Is = JourReposNumeric(WsPaysdesConges)
                                Return False
                            Case Is = JourOuvrableNumeric(WsPaysdesConges)
                                Return False
                            Case Else
                                Select Case SiTenirCompteJourFerie
                                    Case False
                                        Return True
                                    Case True
                                        Select Case SiEstUnJourFerie(ArgumentDate, (WsPaysdesConges))
                                            Case True
                                                Return False
                                            Case False
                                                Return True
                                        End Select
                                End Select
                        End Select
                End Select
                Return False
            End Get
        End Property

        Public ReadOnly Property SiJourOuvrable(ByVal ArgumentDate As String, ByVal SiTenirCompteJourFerie As Boolean) As Boolean
            Get
                Dim Wd As Integer
                Wd = Weekday(DateValue(ArgumentDate), FirstDayOfWeek.Monday)
                Select Case WsSiGestionEtranger
                    Case False
                        Select Case Wd
                            Case Is = JourOuvrableNumeric
                                Return True
                                Select Case SiTenirCompteJourFerie
                                    Case True
                                        Select Case SiEstUnJourFerie(ArgumentDate)
                                            Case True
                                                Return False
                                            Case False
                                                Return True
                                        End Select
                                End Select
                            Case Else
                                Return False
                        End Select
                    Case True
                        Select Case Wd
                            Case Is = JourOuvrableNumeric(WsPaysdesConges)
                                Return True
                                Select Case SiTenirCompteJourFerie
                                    Case True
                                        Select Case SiEstUnJourFerie(ArgumentDate, WsPaysdesConges)
                                            Case True
                                                Return False
                                            Case False
                                                Return True
                                        End Select
                                End Select
                            Case Else
                                Return False
                        End Select
                End Select
                Return False
            End Get
        End Property

        Public ReadOnly Property NombredeJoursOuvres(ByVal Annee As String, ByVal Mois As Integer) As Integer
            Get
                Dim TableauData(0) As String
                TableauData = Strings.Split(ListeDesJoursOuvres(Annee, Mois, False), VI.Tild, -1)
                Return UBound(TableauData)
            End Get
        End Property

        Public ReadOnly Property NombredeJoursTravailles(ByVal Annee As String, ByVal Mois As Integer) As Integer
            Get
                Dim TableauData(0) As String
                TableauData = Strings.Split(ListeDesJoursOuvres(Annee, Mois, True), VI.Tild, -1)
                Return UBound(TableauData)
            End Get
        End Property

        Public ReadOnly Property NombredeJoursOuvrables(ByVal Annee As String, ByVal Mois As Integer) As Integer
            Get
                Dim TableauData(0) As String
                TableauData = Strings.Split(ListeDesJoursOuvrables(Annee, Mois, False), VI.Tild, -1)
                Return UBound(TableauData)
            End Get
        End Property

        Public Function CalcJoursOuvres(ByVal DateDebut As String, ByVal DateFin As String, ByVal Nature As String, ByVal TypeEcartDate As String) As Double
            Dim IndiceJ As Integer
            Dim IndiceP As Integer
            Dim IndicePos As Integer
            Dim TypeEcartDateDebut As String
            Dim TypeEcartDateFin As String
            Dim QuantDebut As Integer
            Dim QuantFin As Integer
            Dim NbjCalend As Double
            Dim NbjCorrCalend As Double
            Dim NbjCorrDim As Double
            Dim NbjCorrOuvrable As Double
            Dim Wd As Integer
            Dim WdDebut As Integer
            Dim WdFin As Integer
            Dim NbrDimanches As Integer
            Dim NbrOuvrable As Integer
            Dim Reste As Double
            Dim Ecart As Double
            Dim NbrFeries As Double
            Dim NbrFeriesDim As Double
            Dim NbrFeriesOuv As Double
            Dim NbrCorrFeries As Double
            Dim NbrCorrFeriesDim As Double
            Dim NbrCorrFeriesOuv As Double
            Dim TotalJoursOuvres As Double
            Dim DateW As String
            Dim UnDemi As Double
            Dim SiFinVendredi As Integer
            Dim SiJF As Boolean
            '
            SiFinVendredi = 0
            UnDemi = 1 / 2
            Select Case DateDebut
                Case Is = ""
                    Return 0
            End Select
            Select Case DateFin
                Case Is = ""
                    Return 0
            End Select
            '
            'TypeEcartDate correspond au type d'ecart entre DateDebut et DateFin
            'et peut prendre les valeurs suivantes :
            'DATABS_DEBUTINCLUS & " - " & DATABS_FININCLUS
            'DATABS_DEBUTAPRESMIDI & " - " & DATABS_FININCLUS
            'DATABS_DEBUTAPRESMIDI & " - " & DATABS_FINMATIN
            'DATABS_DEBUTINCLUS & " - " & DATABS_FINMATIN
            'Par defaut : le calcul s'effectue dans le cas : DATABS_DEBUTINCLUS & " - " & DATABS_FININCLUS
            Select Case TypeEcartDate
                Case DATABS_DEBUTINCLUS & " - " & DATABS_FININCLUS
                    TypeEcartDateDebut = DATABS_DEBUTINCLUS
                    TypeEcartDateFin = DATABS_FININCLUS
                Case DATABS_DEBUTAPRESMIDI & " - " & DATABS_FININCLUS
                    TypeEcartDateDebut = DATABS_DEBUTAPRESMIDI
                    TypeEcartDateFin = DATABS_FININCLUS
                Case DATABS_DEBUTAPRESMIDI & " - " & DATABS_FINMATIN
                    TypeEcartDateDebut = DATABS_DEBUTAPRESMIDI
                    TypeEcartDateFin = DATABS_FINMATIN
                Case DATABS_DEBUTINCLUS & " - " & DATABS_FINMATIN
                    TypeEcartDateDebut = DATABS_DEBUTINCLUS
                    TypeEcartDateFin = DATABS_FINMATIN
                Case Else
                    TypeEcartDateDebut = DATABS_DEBUTINCLUS
                    TypeEcartDateFin = DATABS_FININCLUS
            End Select
            '
            '***
            QuantDebut = VirRhDates.CalcQuantieme(DateDebut)
            QuantFin = VirRhDates.CalcQuantieme(DateFin)
            NbjCalend = QuantFin - QuantDebut + 1
            '
            '1ere correction / TypeEcartDate
            NbjCorrCalend = 0
            Select Case TypeEcartDateDebut
                Case DATABS_DEBUTAPRESMIDI
                    NbjCorrCalend += UnDemi
            End Select
            Select Case TypeEcartDateFin
                Case DATABS_FINMATIN
                    NbjCorrCalend += UnDemi
            End Select
            '********************************************************************
            Wd = Weekday(DateValue(DateDebut), FirstDayOfWeek.Monday)
            WdDebut = Wd
            NbjCorrDim = 0
            Select Case Wd
                Case Is = JourReposNumeric
                    IndiceP = 7
                    Select Case TypeEcartDateDebut
                        Case DATABS_DEBUTAPRESMIDI
                            NbjCorrDim += UnDemi
                    End Select
                Case Else
                    IndiceP = Wd - 1
            End Select
            '*******************************************************************
            NbrDimanches = 0
            Ecart = 7 - IndiceP
            Reste = NbjCalend - Ecart
            Do While Reste > 0
                Select Case Reste
                    Case Is > 0
                        NbrDimanches += 1
                End Select
                Reste = Reste - 7
            Loop
            '
            Wd = Weekday(DateValue(DateFin), FirstDayOfWeek.Monday)
            WdFin = Wd
            Select Case Wd
                Case Is = JourReposNumeric
                    Select Case TypeEcartDateFin
                        Case DATABS_FINMATIN
                            NbjCorrDim += UnDemi
                    End Select
                Case Is = VI.Jours.Vendredi
                    Select Case TypeEcartDate
                        Case DATABS_DEBUTINCLUS & " - " & DATABS_FININCLUS, DATABS_DEBUTAPRESMIDI & " - " & DATABS_FININCLUS
                            SiFinVendredi = 1
                    End Select
            End Select
            '********************************************************************
            Select Case WsSiGestionEtranger
                Case False
                    IndicePos = JourOuvrableNumeric
                Case True
                    IndicePos = JourOuvrableNumeric(WsPaysdesConges)
            End Select
            NbrOuvrable = 0
            Ecart = IndicePos - IndiceP
            Reste = NbjCalend - Ecart
            Do While Reste > 0
                If Reste > 0 And Reste <= NbjCalend Then
                    NbrOuvrable += 1
                End If
                Reste = Reste - 7
            Loop
            '
            NbjCorrOuvrable = 0
            Select Case TypeEcartDateDebut
                Case DATABS_DEBUTAPRESMIDI
                    Select Case IndicePos + 1
                        Case WdDebut
                            NbjCorrOuvrable += UnDemi
                    End Select
            End Select
            Select Case TypeEcartDateFin
                Case DATABS_FINMATIN
                    Select Case IndicePos + 1
                        Case WdFin
                            NbjCorrOuvrable += UnDemi
                    End Select
            End Select
            '
            '*********************************************************************************
            NbrFeries = 0
            NbrFeriesDim = 0
            NbrFeriesOuv = 0
            '
            NbrCorrFeries = 0
            NbrCorrFeriesDim = 0
            NbrCorrFeriesOuv = 0
            '
            'Jours Feries
            IndiceJ = 0
            Do
                DateW = VirRhDates.CalcDateMoinsJour(DateDebut, LTrim(Str(IndiceJ)), "0")
                Select Case DateW
                    Case Is = ""
                        Exit Do
                End Select
                Select Case VirRhDates.ComparerDates(DateW, DateFin)
                    Case VI.ComparaisonDates.PlusGrand
                        Exit Do
                End Select
                Select Case WsSiGestionEtranger
                    Case False
                        SiJF = SiEstUnJourFerie(DateW)
                    Case True
                        SiJF = SiEstUnJourFerie(DateW, WsPaysdesConges)
                End Select
                Select Case SiJF
                    Case True
                        NbrFeries += 1
                        Select Case WsSiGestionEtranger
                            Case False
                                Select Case Weekday(DateValue(DateW), FirstDayOfWeek.Monday)
                                    Case Is = JourReposNumeric
                                        NbrFeriesDim += 1
                                    Case Is = JourOuvrableNumeric
                                        NbrFeriesOuv += 1
                                End Select
                            Case True
                                Select Case Weekday(DateValue(DateW), FirstDayOfWeek.Monday)
                                    Case Is = JourReposNumeric(WsPaysdesConges)
                                        NbrFeriesDim += 1
                                    Case Is = JourOuvrableNumeric(WsPaysdesConges)
                                        NbrFeriesOuv += 1
                                End Select
                        End Select
                        Select Case DateW
                            Case Is = DateDebut
                                Select Case TypeEcartDateDebut
                                    Case Is = DATABS_DEBUTAPRESMIDI
                                        NbrCorrFeries += UnDemi
                                        Select Case WsSiGestionEtranger
                                            Case False
                                                Select Case Weekday(DateValue(DateW), FirstDayOfWeek.Monday)
                                                    Case Is = JourReposNumeric
                                                        NbrCorrFeriesDim += UnDemi
                                                    Case Is = JourOuvrableNumeric
                                                        NbrCorrFeriesOuv += UnDemi
                                                End Select
                                            Case True
                                                Select Case Weekday(DateValue(DateW), FirstDayOfWeek.Monday)
                                                    Case Is = JourReposNumeric(WsPaysdesConges)
                                                        NbrCorrFeriesDim += UnDemi
                                                    Case Is = JourOuvrableNumeric(WsPaysdesConges)
                                                        NbrCorrFeriesOuv += UnDemi
                                                End Select
                                        End Select
                                End Select
                        End Select
                        Select Case DateW
                            Case Is = DateFin
                                Select Case TypeEcartDateFin
                                    Case Is = DATABS_FINMATIN
                                        NbrCorrFeries += UnDemi
                                        Select Case WsSiGestionEtranger
                                            Case False
                                                Select Case Weekday(DateValue(DateW), FirstDayOfWeek.Monday)
                                                    Case Is = JourReposNumeric
                                                        NbrCorrFeriesDim += UnDemi
                                                    Case Is = JourOuvrableNumeric
                                                        NbrCorrFeriesOuv += UnDemi
                                                End Select
                                            Case True
                                                Select Case Weekday(DateValue(DateW), FirstDayOfWeek.Monday)
                                                    Case Is = JourReposNumeric(WsPaysdesConges)
                                                        NbrCorrFeriesDim += UnDemi
                                                    Case Is = JourOuvrableNumeric(WsPaysdesConges)
                                                        NbrCorrFeriesOuv += UnDemi
                                                End Select
                                        End Select
                                End Select
                        End Select
                End Select
                IndiceJ = IndiceJ + 1
            Loop
            '***
            TotalJoursOuvres = 0
            Select Case Nature
                Case Is = "OUVRES"
                    TotalJoursOuvres = (NbjCalend - NbjCorrCalend) - (NbrDimanches - NbjCorrDim) - (NbrOuvrable - NbjCorrOuvrable) - (NbrFeries - NbrCorrFeries) + (NbrFeriesDim - NbrCorrFeriesDim) + (NbrFeriesOuv - NbrCorrFeriesOuv)
                Case Is = "OUVRABLES"
                    TotalJoursOuvres = (NbjCalend - NbjCorrCalend) - (NbrDimanches - NbjCorrDim) - (NbrFeries - NbrCorrFeries) + (NbrFeriesDim - NbrCorrFeriesDim) + SiFinVendredi
            End Select
            Select Case TotalJoursOuvres
                Case Is <= 0
                    Return 0
                Case Else
                    Return TotalJoursOuvres
            End Select
        End Function

        Private Function ListeDesJoursOuvres(ByVal Annee As String, ByVal Mois As Integer, ByVal SiTravaille As Boolean) As String

            Dim IMois As Integer
            Dim IDebut As Integer
            Dim IFin As Integer
            Dim IJour As Integer
            Dim INombre As Integer
            Dim Chaine As System.Text.StringBuilder
            Select Case Mois
                Case 1 To 12
                    IDebut = Mois
                    IFin = Mois
                Case Else
                    IDebut = 1
                    IFin = 12
            End Select
            Chaine = New System.Text.StringBuilder
            For IMois = IDebut To IFin
                Select Case IMois
                    Case 2
                        INombre = 28
                    Case 1, 3, 5, 7, 8, 10, 12
                        INombre = 31
                    Case Else
                        INombre = 30
                End Select
                For IJour = 1 To INombre
                    Select Case SiJourOuvre(Strings.Format(IJour, "00") & VI.Slash & Strings.Format(IMois, "00") & VI.Slash & Annee, SiTravaille)
                        Case True
                            Chaine.Append(Strings.Format(IJour, "00") & VI.Slash & Strings.Format(IMois, "00") & VI.Slash & Annee & VI.Tild)
                    End Select
                Next IJour
            Next IMois
            Return Chaine.ToString

        End Function

        Private Function ListeDesJoursOuvrables(ByVal Annee As String, ByVal Mois As Integer, ByVal SiTravaille As Boolean) As String

            Dim IMois As Integer
            Dim IDebut As Integer
            Dim IFin As Integer
            Dim IJour As Integer
            Dim INombre As Integer
            Dim Chaine As System.Text.StringBuilder
            Select Case Mois
                Case 1 To 12
                    IDebut = Mois
                    IFin = Mois
                Case Else
                    IDebut = 1
                    IFin = 12
            End Select
            Chaine = New System.Text.StringBuilder
            For IMois = IDebut To IFin
                Select Case IMois
                    Case 2
                        INombre = 28
                    Case 1, 3, 5, 7, 8, 10, 12
                        INombre = 31
                    Case Else
                        INombre = 30
                End Select
                For IJour = 1 To INombre
                    Select Case SiJourOuvrable(Strings.Format(IJour, "00") & VI.Slash & Strings.Format(IMois, "00") & VI.Slash & Annee, SiTravaille)
                        Case True
                            Chaine.Append(Strings.Format(IJour, "00") & VI.Slash & Strings.Format(IMois, "00") & VI.Slash & Annee & VI.Tild)
                    End Select
                Next IJour
            Next IMois
            Return Chaine.ToString

        End Function

        Private Sub LireReferentiel()
            Dim LstResultat As List(Of VIR_FICHE)
            Dim TableauData(0) As String

            WsListeJoursFeries = New List(Of PAI_FERIE)
            '** 1  Jour Ouvrable et Repos
            LstResultat = VirServiceServeur.LectureObjet_ToFiches(WsNomUtilisateur, VI.PointdeVue.PVuePaie, 3, 0, False)
            If LstResultat Is Nothing Then
                LstResultat = New List(Of VIR_FICHE)
                LstResultat.Add(New PAI_OUVRABLE() With {.ContenuTable = "" & 0 & VI.Tild & "Samedi" & VI.Tild})
            End If
            WsFicheJourOuv = DirectCast(LstResultat(0), PAI_OUVRABLE)

            '** 2  Liste de tous les Jours fériés
            LstResultat = VirServiceServeur.LectureObjet_ToFiches(WsNomUtilisateur, VI.PointdeVue.PVuePaie, 2, 0, False)
            If LstResultat IsNot Nothing Then
                LstResultat.ForEach(Sub(It)
                                        WsListeJoursFeries.Add(DirectCast(It, PAI_FERIE))
                                    End Sub)
            End If
        End Sub

        Public Sub New(ByVal NomUtilisateur As String, ByVal ObjetPays As ObjetPays)
            WsNomUtilisateur = NomUtilisateur
            WsEnsemblePays = ObjetPays
            Call LireReferentiel()
        End Sub

    End Class
End Namespace

