﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.TablesObjet.ShemaREF
Imports Virtualia.Systeme.MetaModele

Namespace Datas
    Public Class ObjetDirections
        Implements IGestionVirFiche(Of ORG_DIRECTION)
        Private WsListeDirections As List(Of ORG_DIRECTION) = New List(Of ORG_DIRECTION)()
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesDirections(ByVal Lettre As String) As List(Of ORG_DIRECTION)
            Get
                If WsListeDirections Is Nothing Then
                    Return Nothing
                End If
                Dim Predicat As New Virtualia.Ressources.Predicats.PredicateUniversel(Lettre, VI.PointdeVue.PVueDirection)
                Dim LstRes As List(Of ORG_DIRECTION)
                Dim LstTri As List(Of ORG_DIRECTION) = Nothing

                LstRes = WsListeDirections.FindAll(AddressOf Predicat.RechercherLettre)
                If LstRes Is Nothing Then
                    Return Nothing
                End If
                LstTri = (From instance In LstRes Select instance
                          Where instance.Ide_Dossier > 0
                          Order By instance.Denomination Ascending).ToList

                Return LstTri
            End Get
        End Property

        Public ReadOnly Property ListeDesDirections() As List(Of ORG_DIRECTION)
            Get
                Dim LstRes As List(Of ORG_DIRECTION)
                LstRes = (From instance In WsListeDirections Select instance
                          Where instance.Ide_Dossier > 0 Order By instance.Denomination Ascending).ToList
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property Nombreddirection() As Integer Implements IGestionVirFiche(Of ORG_DIRECTION).NombredItems
            Get
                Return WsListeDirections.Count
            End Get
        End Property

        Public Function Fiche_Direction(ByVal IdeDossier As Integer) As ORG_DIRECTION Implements IGestionVirFiche(Of ORG_DIRECTION).GetFiche
            Return WsListeDirections.Find(Function(m) m.Ide_Dossier = IdeDossier)
        End Function

        Public Function Fiche_Direction(ByVal Intitule As String) As ORG_DIRECTION Implements IGestionVirFiche(Of ORG_DIRECTION).GetFiche
            Return WsListeDirections.Find(Function(m) m.Denomination = Intitule)
        End Function

        Public Sub Actualiser() Implements IGestionVirFiche(Of ORG_DIRECTION).Actualiser
            WsListeDirections.Clear()
            WsListeDirections = Nothing
            ConstituerListe()
        End Sub

        Private Sub ConstituerListe()
            WsListeDirections.Clear()

            ''** 1  Liste de tous les directions
            WsListeDirections.AddRange(CreationCollectionVIRFICHE(Of ORG_DIRECTION).GetCollection(WsNomUtilisateur, VirServiceServeur))
            If (WsListeDirections.Count <= 0) Then
                Return
            End If

        End Sub

        Public Sub New(ByVal NomUtilisateur As String)
            WsNomUtilisateur = NomUtilisateur
            ConstituerListe()
        End Sub

    End Class
End Namespace
