﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.MetaModele

Namespace Datas
    Public Class CreationCollectionVIRFICHE(Of T As {VIR_FICHE, New})

        Public Shared Function GetCollection(ByVal NomUtilisateur As String, ByVal WcfWebService As Virtualia.Net.WebService.IServiceServeur) As List(Of T)
            Return GetCollection(NomUtilisateur, WcfWebService, 0)
        End Function

        Public Shared Function GetCollection(ByVal NomUtilisateur As String, ByVal WcfWebService As Virtualia.Net.WebService.IServiceServeur, ByVal Ide As Integer) As List(Of T)
            Dim LstResultat As List(Of VIR_FICHE)
            Dim Poindevue As Integer = GetPointDeVue()
            Dim Numobjet As Integer = GetNumObjet()

            LstResultat = WcfWebService.LectureObjet_ToFiches(NomUtilisateur, Poindevue, Numobjet, Ide, False)

            Dim LstTypee As List(Of T) = New List(Of T)()
            If (LstResultat Is Nothing) Then
                Return LstTypee
            End If
            LstResultat.ForEach(Sub(it)
                                    LstTypee.Add(DirectCast(it, T))
                                End Sub)
            Return LstTypee
        End Function

        Private Shared Function GetPointDeVue() As Integer
            Return New T().PointdeVue
        End Function

        Private Shared Function GetNumObjet() As Integer
            Return New T().NumeroObjet
        End Function

    End Class
End Namespace