﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetMesureActivite
        Implements IGestionVirFicheModif(Of ONIC_MISSION)

        Private WsListeFctSupport As List(Of FCT_SUPPORT)
        Private WsListeMissions As List(Of ONIC_MISSION)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeFonctionsSupport(ByVal Etablissement As String) As List(Of FCT_SUPPORT)
            Get
                If WsListeFctSupport Is Nothing Then
                    Return Nothing
                End If
                Dim LstRes As List(Of FCT_SUPPORT)
                Select Case Etablissement
                    Case Is = ""
                        LstRes = (From instance In WsListeFctSupport Select instance _
                                        Where instance.Ide_Dossier > 0 _
                                        Order By instance.Etablissement Ascending, instance.Intitule Ascending).ToList
                    Case Else
                        LstRes = (From instance In WsListeFctSupport Select instance _
                                        Where instance.Etablissement = Etablissement _
                                        Order By instance.Intitule Ascending).ToList
                End Select
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property ListeFonctionsSupport(ByVal Lettre As String, ByVal Etablissement As String) As List(Of FCT_SUPPORT)
            Get
                If WsListeFctSupport Is Nothing Then
                    Return Nothing
                End If
                Dim Predicat As New Virtualia.Ressources.Predicats.PredicateUniversel(Lettre, VI.PointdeVue.PVueFonctionSupport)
                Dim LstRes As List(Of FCT_SUPPORT)
                Dim LstTri As List(Of FCT_SUPPORT) = Nothing

                LstRes = WsListeFctSupport.FindAll(AddressOf Predicat.RechercherLettre)
                If LstRes Is Nothing Then
                    Return Nothing
                End If
                Select Case Etablissement
                    Case Is = ""
                        LstTri = (From instance In LstRes Select instance _
                               Where instance.Ide_Dossier > 0 _
                               Order By instance.Intitule Ascending).ToList

                    Case Else
                        LstTri = (From instance In LstRes Select instance _
                               Where instance.Etablissement = Etablissement _
                               Order By instance.Intitule Ascending).ToList

                End Select
                Return LstTri
            End Get
        End Property

        Public ReadOnly Property ListeDesMisions(ByVal Etablissement As String) As List(Of ONIC_MISSION)
            Get
                If WsListeMissions Is Nothing Then
                    Return Nothing
                End If
                Dim LstRes As List(Of ONIC_MISSION)
                Select Case Etablissement
                    Case Is = ""
                        LstRes = (From instance In WsListeMissions Select instance _
                                        Where instance.Ide_Dossier > 0 _
                                        Order By instance.Etablissement Ascending, instance.Intitule Ascending).ToList
                    Case Else
                        LstRes = (From instance In WsListeMissions Select instance _
                                        Where instance.Etablissement = Etablissement _
                                        Order By instance.Intitule Ascending).ToList
                End Select
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property ListeDesMissions(ByVal Lettre As String, ByVal Etablissement As String) As List(Of ONIC_MISSION)
            Get
                If WsListeMissions Is Nothing Then
                    Return Nothing
                End If
                Dim Predicat As New Virtualia.Ressources.Predicats.PredicateUniversel(Lettre, VI.PointdeVue.PVueActivite)
                Dim LstRes As List(Of ONIC_MISSION)
                Dim LstTri As List(Of ONIC_MISSION) = Nothing

                LstRes = WsListeMissions.FindAll(AddressOf Predicat.RechercherLettre)
                If LstRes Is Nothing Then
                    Return Nothing
                End If
                Select Case Etablissement
                    Case Is = ""
                        LstTri = (From instance In LstRes Select instance _
                               Where instance.Ide_Dossier > 0 _
                               Order By instance.Intitule Ascending).ToList

                    Case Else
                        LstTri = (From instance In LstRes Select instance _
                               Where instance.Etablissement = Etablissement _
                               Order By instance.Intitule Ascending).ToList

                End Select
                Return LstTri
            End Get
        End Property

        Public ReadOnly Property NombredeFonctionsSupport() As Integer
            Get
                Return WsListeFctSupport.Count
            End Get
        End Property

        Public ReadOnly Property NombredeMissions() As Integer Implements IGestionVirFicheModif(Of ONIC_MISSION).NombredItems
            Get
                Return WsListeMissions.Count
            End Get
        End Property

        Public Function Fiche_Mission(ByVal Ide_Dossier As Integer) As ONIC_MISSION Implements IGestionVirFicheModif(Of ONIC_MISSION).GetFiche
            Return WsListeMissions.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Mission(ByVal Intitule As String) As ONIC_MISSION Implements IGestionVirFicheModif(Of ONIC_MISSION).GetFiche
            Return WsListeMissions.Find(Function(m) m.Intitule = Intitule)
        End Function

        Public Function Fiche_FonctionSupport(ByVal Ide_Dossier As Integer) As FCT_SUPPORT

            Return WsListeFctSupport.Find(Function(m) m.Ide_Dossier = Ide_Dossier)

        End Function

        Public Function Fiche_FonctionSupport(ByVal Intitule As String) As FCT_SUPPORT

            Return WsListeFctSupport.Find(Function(m) m.Intitule = Intitule)

        End Function

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of ONIC_MISSION).SupprimerItem
            Dim FicheMission As ONIC_MISSION
            FicheMission = Fiche_Mission(Ide)
            If FicheMission IsNot Nothing Then
                WsListeMissions.Remove(FicheMission)
            End If
        End Sub

        Public Sub AjouterItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of ONIC_MISSION).AjouterItem
            SupprimerItem(Ide)
            ConstituerListe(Ide)
        End Sub

        Public Sub Actualiser() Implements IGestionVirFicheModif(Of ONIC_MISSION).Actualiser
            WsListeMissions.Clear()
            ConstituerListe(0)
        End Sub

        Private Sub ConstituerListe(ByVal Ide As Integer)
            WsListeFctSupport.Clear()
            WsListeMissions.Clear()
            Dim FicheMission As ONIC_MISSION

            '** 1.1  Liste de toutes les fonctions support
            WsListeFctSupport.AddRange(CreationCollectionVIRFICHE(Of FCT_SUPPORT).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide))

            '** 1.2  Liste de toutes les missions
            WsListeMissions.AddRange(CreationCollectionVIRFICHE(Of ONIC_MISSION).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide))

            '** 2  Liste de toutes les mesures d'activité
            Dim lstmes As List(Of ONIC_MESURE) = CreationCollectionVIRFICHE(Of ONIC_MESURE).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstmes.ForEach(Sub(f)
                               FicheMission = Fiche_Mission(f.Ide_Dossier)
                               If FicheMission Is Nothing Then
                                   Return
                               End If
                               FicheMission.Ajouter_Mesure(f)
                           End Sub)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String, ByVal Ide As Integer)
            WsNomUtilisateur = NomUtilisateur
            WsListeFctSupport = New List(Of FCT_SUPPORT)
            WsListeMissions = New List(Of ONIC_MISSION)
            ConstituerListe(Ide)
        End Sub

    End Class
End Namespace
