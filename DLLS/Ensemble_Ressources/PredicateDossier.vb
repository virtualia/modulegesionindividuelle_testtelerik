﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Ressources.Datas

Namespace Predicats
    Public Class PredicateDossier
        Private WsIdentifiant As Integer
        Private WsLettre As String
        Private WsCritereDyna(0) As String

        Public Function RechercherIdentifiant(ByVal Source As ObjetDossierPER) As Boolean
            Return Source.V_Identifiant = WsIdentifiant
        End Function

        Public Function RechercherNomEtCritere(ByVal Source As ObjetDossierPER) As Boolean

            If WsLettre = "" Then

                If (WsCritereDyna(0) = "") Then
                    Return True
                End If

                For Each s As String In WsCritereDyna
                    If (s = Source.CritereIcone) Then
                        Return True
                    End If
                Next

                Return False

            End If

            If Strings.Left(Source.Nom, WsLettre.Length) = WsLettre Then
                If WsCritereDyna(0) = "" Then
                    Return True
                End If

                For Each s As String In WsCritereDyna
                    If (s = Source.CritereIcone) Then
                        Return True
                    End If
                Next
                Return False
            End If

            Return False
        End Function

        Public Function RechercherNom(ByVal Source As ObjetDossierPER) As Boolean

            If (WsLettre = "") Then
                Return True
            End If

            If (Strings.Left(WsLettre, 1) = "*") Then
                Return Source.Nom.Contains(Strings.Right(WsLettre, WsLettre.Length - 1))
            End If

            Return Strings.Left(Source.Nom, WsLettre.Length) = WsLettre

        End Function

        Public Function RechercherCritere(ByVal Source As ObjetDossierPER) As Boolean

            If WsCritereDyna(0) = "" Then
                Return True
            End If

            For Each s As String In WsCritereDyna
                If (s = Source.CritereIcone) Then
                    Return True
                End If
            Next

            Return False

        End Function

        Public Function RechercheIdeSurExtraction(ByVal Source As ItemSelection) As Boolean
            Return Source.Identifiant = WsIdentifiant
        End Function

        Private Sub New(ByVal Ide As Integer, ByVal LettreAlpha As String, ByVal Criteres As String)

            WsIdentifiant = Ide
            WsLettre = LettreAlpha

            If (Criteres = "") Then
                WsCritereDyna(0) = ""
                Return
            End If

            WsCritereDyna = Strings.Split(Criteres, ",", -1)
        End Sub

        Public Sub New(ByVal Ide As Integer)
            Me.New(Ide, "", "")
        End Sub

        Public Sub New(ByVal LettreAlpha As String)
            Me.New(0, LettreAlpha, "")
        End Sub

        Public Sub New(ByVal LettreAlpha As String, ByVal Criteres As String)
            Me.New(0, LettreAlpha, Criteres)
        End Sub

    End Class
End Namespace
