Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele.Donnees
Imports Virtualia.Systeme.Parametrage.BasesdeDonnees
Imports Virtualia.Systeme.Sgbd.Sql
Namespace Datas
    Namespace PosteBudgetaire
        Public Class EmploisBudgetaires
            Inherits List(Of ElementDetaille)

            Private WsDateValeur As String
            Private WsInstanceBd As ParamSgbdDatabase
            Private WsNomUtilisateur As String
            '
            Private WsEtatCivil As List(Of String)
            Private WsPosteBud As List(Of String)
            Private WsStatut As List(Of String)
            Private WsPosition As List(Of String)
            Private WsPositionDeta As List(Of String)
            Private WsGrade As List(Of String)
            Private WsGradeDeta As List(Of String)
            Private WsOrganigramme As List(Of String)
            Private WsAbsence As List(Of String)
            '
            Private WsNbAgents As Integer

            Public ReadOnly Property DatedEffet() As String
                Get
                    Return WsDateValeur
                End Get
            End Property

            Public Overloads ReadOnly Property Item(ByVal Index As Integer) As ElementDetaille
                Get
                    If (Index < 0 Or Index >= Count) Then
                        Return Nothing
                    End If

                    Return Me(Index)
                End Get
            End Property

            Public WriteOnly Property NomUtilisateur(ByVal Sgbd As ParamSgbdDatabase) As String
                Set(ByVal value As String)
                    WsNomUtilisateur = value
                    WsInstanceBd = Sgbd
                End Set
            End Property

            Public Sub ChargerLesDonnees(ByVal DatedeValeur As String)

                Dim Enregistrement As ElementDetaille
                Dim TableauData As String()
                Dim IdeDossier As Integer
                Dim AFaire As Boolean

                WsDateValeur = DatedeValeur
                Clear()

                LireLesDonnees()

                For Each s As String In WsPosteBud
                    If (s = "") Then
                        Exit For
                    End If

                    TableauData = Strings.Split(s, VI.Tild, -1)

                    Enregistrement = New ElementDetaille(Me)

                    IdeDossier = CInt(TableauData(0))

                    Enregistrement.IdentifiantDossier = IdeDossier
                    Enregistrement.DateEmploi = TableauData(1)
                    Enregistrement.NumeroEmploi = TableauData(2)
                    Enregistrement.DateFinEmploi = TableauData(3)
                    Enregistrement.GradedelEmploi = TableauData(4)

                    For Each ec As String In WsEtatCivil
                        If (ec = "") Then
                            Exit For
                        End If

                        TableauData = Split(ec, VI.Tild, -1)

                        If (Integer.Parse(TableauData(0)) = IdeDossier) Then
                            Enregistrement.Qualite = TableauData(1)
                            Enregistrement.Nom = TableauData(2)
                            Enregistrement.Prenom = TableauData(3)
                            Enregistrement.Patronyme = TableauData(4)
                            Exit For
                        End If
                    Next

                    For Each st As String In WsStatut
                        If (st = "") Then
                            Exit For
                        End If

                        TableauData = Split(st, VI.Tild, -1)

                        If (Integer.Parse(TableauData(0)) = IdeDossier) Then
                            Enregistrement.DateduStatut = TableauData(1)
                            Enregistrement.Statut = TableauData(2)
                            Enregistrement.ModedeRemuneration = TableauData(3)
                            Exit For
                        End If
                    Next

                    For Each p As String In WsPosition
                        If (p = "") Then
                            Exit For
                        End If

                        TableauData = Split(p, VI.Tild, -1)

                        If (Integer.Parse(TableauData(0)) = IdeDossier) Then
                            Enregistrement.DatedelaPosition = TableauData(1)
                            Enregistrement.Position = TableauData(2)
                            Enregistrement.TauxdActivite = Val(TableauData(3))
                            Enregistrement.DatedeFinPosition = TableauData(4)
                            Exit For
                        End If
                    Next

                    For Each g As String In WsGrade
                        If (g = "") Then
                            Exit For
                        End If

                        TableauData = Split(g, VI.Tild, -1)

                        If (Integer.Parse(TableauData(0)) = IdeDossier) Then
                            Enregistrement.DateduGrade = TableauData(1)
                            Enregistrement.Grade = TableauData(2)
                            Enregistrement.Echelon = TableauData(3)
                            Enregistrement.IndiceBrut = Val(TableauData(4))
                            Enregistrement.IndiceMajore = Val(TableauData(5))
                            Enregistrement.Categorie = TableauData(6)
                            Enregistrement.Corps = TableauData(7)
                            Enregistrement.ForfaitMensuel = Val(TableauData(8))
                            Enregistrement.TauxHoraire = Val(TableauData(9))
                            Exit For
                        End If
                    Next

                    For Each gd As String In WsGradeDeta
                        If (gd = "") Then
                            Exit For
                        End If

                        TableauData = Split(gd, VI.Tild, -1)

                        If (Integer.Parse(TableauData(0)) = IdeDossier) Then
                            Enregistrement.DateGradedeDetachement = TableauData(1)
                            Enregistrement.GradedeDetachement = TableauData(2)
                            Enregistrement.EchelondeDetachement = TableauData(3)
                            Enregistrement.IndiceBrutdeDetachement = Val(TableauData(4))
                            Enregistrement.IndiceMajoredeDetachement = Val(TableauData(5))
                            Enregistrement.CategoriedeDetachement = TableauData(6)
                            Enregistrement.CorpsdeDetachement = TableauData(7)
                            Enregistrement.ForfaitMensueldeDetachement = Val(TableauData(8))
                            Enregistrement.TauxHorairedeDetachement = Val(TableauData(9))
                            Exit For
                        End If
                    Next

                    For Each org As String In WsOrganigramme
                        If (org = "") Then
                            Exit For
                        End If

                        TableauData = Split(org, VI.Tild, -1)

                        If (Integer.Parse(TableauData(0)) = IdeDossier) Then
                            Enregistrement.DatedelAffectationFonctionnelle = TableauData(1)
                            Enregistrement.Direction = TableauData(2)
                            Enregistrement.Service = TableauData(3)
                            Enregistrement.Niveau3 = TableauData(4)
                            Enregistrement.Niveau4 = TableauData(5)
                            Enregistrement.FonctionExercee = TableauData(6)
                            Exit For
                        End If
                    Next

                    For Each abs As String In WsAbsence
                        If (abs = "") Then
                            Exit For
                        End If

                        TableauData = Split(abs, VI.Tild, -1)

                        If (Integer.Parse(TableauData(0)) = IdeDossier) Then
                            Enregistrement.NaturedelAbsence = TableauData(1)
                            Exit For
                        End If
                    Next

                    For Each pd As String In WsPositionDeta
                        If (pd = "") Then
                            Exit For
                        End If

                        TableauData = Split(pd, VI.Tild, -1)

                        If (Integer.Parse(TableauData(0)) = IdeDossier) Then
                            Enregistrement.DatePositiondeDetachement = TableauData(1)
                            Enregistrement.PositiondeDetachement = TableauData(2)
                            Enregistrement.TauxdActivitedeDetachement = Val(TableauData(3))
                            Enregistrement.DateFinPositiondeDetachement = TableauData(4)
                            Exit For
                        End If
                    Next

                    AFaire = True
                    If (Enregistrement.NumeroEmploi = "") Then
                        AFaire = False
                    End If

                    If (Enregistrement.Nom = "") Then
                        AFaire = False
                    End If

                    If (AFaire) Then
                        Add(Enregistrement)
                    End If

                Next
            End Sub

            Private Sub LireLesDonnees()
                Dim LstResultat As List(Of String)
                Dim NumObjet As Integer = 0
                Do
                    Select Case NumObjet
                        Case 0
                            NumObjet = VI.ObjetPer.ObaCivil
                        Case VI.ObjetPer.ObaCivil
                            NumObjet = VI.ObjetPer.ObaStatut
                        Case VI.ObjetPer.ObaStatut
                            NumObjet = VI.ObjetPer.ObaActivite
                        Case VI.ObjetPer.ObaActivite
                            NumObjet = VI.ObjetPer.ObaPositionDetache
                        Case VI.ObjetPer.ObaPositionDetache
                            NumObjet = VI.ObjetPer.ObaGrade
                        Case VI.ObjetPer.ObaGrade
                            NumObjet = VI.ObjetPer.ObaGradeDetache
                        Case VI.ObjetPer.ObaGradeDetache
                            NumObjet = VI.ObjetPer.ObaPostebud
                        Case VI.ObjetPer.ObaPostebud
                            NumObjet = VI.ObjetPer.ObaOrganigramme
                        Case VI.ObjetPer.ObaOrganigramme
                            NumObjet = VI.ObjetPer.ObaAbsence
                        Case VI.ObjetPer.ObaAbsence
                            Exit Do
                    End Select
                    LstResultat = VirServiceServeur.RequeteSql_ToListeChar(WsNomUtilisateur, VI.PointdeVue.PVueApplicatif, NumObjet, OrdreSqlParticulier(NumObjet))

                    Select Case NumObjet
                        Case VI.ObjetPer.ObaCivil
                            WsEtatCivil = LstResultat
                            WsNbAgents = WsEtatCivil.Count
                        Case VI.ObjetPer.ObaStatut
                            WsStatut = LstResultat
                        Case VI.ObjetPer.ObaActivite
                            WsPosition = LstResultat
                        Case VI.ObjetPer.ObaPositionDetache
                            WsPositionDeta = LstResultat
                        Case VI.ObjetPer.ObaGrade
                            WsGrade = LstResultat
                        Case VI.ObjetPer.ObaGradeDetache
                            WsGradeDeta = LstResultat
                        Case VI.ObjetPer.ObaPostebud
                            WsPosteBud = LstResultat
                        Case VI.ObjetPer.ObaOrganigramme
                            WsOrganigramme = LstResultat
                        Case VI.ObjetPer.ObaAbsence
                            WsAbsence = LstResultat
                            Exit Do
                    End Select
                    LstResultat.Clear()
                Loop

            End Sub

            Private Function OrdreSqlParticulier(ByVal NoObjet As Integer) As String

                Dim Constructeur As SqlInterne
                Dim IndiceI As Integer
                Dim IndiceK As Integer = 0
                Dim Debut As Integer
                Dim Fin As Integer

                Constructeur = New SqlInterne(VirModeleRh, WsInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueMetier, "", WsDateValeur, VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.SiHistoriquedeSituation = True

                Constructeur.NoInfoSelection(0, NoObjet) = 1
                Select Case NoObjet
                    Case VI.ObjetPer.ObaCivil
                        Debut = 1
                        Fin = 3
                    Case VI.ObjetPer.ObaStatut
                        Debut = 0
                        Fin = 1
                    Case VI.ObjetPer.ObaActivite, VI.ObjetPer.ObaPositionDetache
                        Debut = 0
                        Fin = 2
                    Case VI.ObjetPer.ObaGrade, VI.ObjetPer.ObaGradeDetache
                        Debut = 0
                        Fin = 6
                    Case VI.ObjetPer.ObaPostebud
                        Constructeur.NoInfoSelection(0, NoObjet) = 4
                        Constructeur.InfoExtraite(IndiceK, NoObjet, 0) = 0
                        IndiceK += 1
                        Debut = 4
                        Fin = 5
                    Case VI.ObjetPer.ObaOrganigramme
                        Debut = 0
                        Fin = 5
                    Case VI.ObjetPer.ObaAbsence
                        Debut = 1
                        Fin = 1
                End Select

                For IndiceI = Debut To Fin
                    Constructeur.InfoExtraite(IndiceK, NoObjet, 0) = CInt(IndiceI)
                    IndiceK += 1
                Next IndiceI

                Select Case NoObjet
                    Case VI.ObjetPer.ObaCivil
                        Constructeur.InfoExtraite(IndiceK, NoObjet, 0) = 13
                        IndiceK += 1
                    Case VI.ObjetPer.ObaStatut
                        Constructeur.InfoExtraite(IndiceK, NoObjet, 0) = 5
                        IndiceK += 1
                    Case VI.ObjetPer.ObaActivite, VI.ObjetPer.ObaPositionDetache
                        Constructeur.InfoExtraite(IndiceK, NoObjet, 0) = 7
                        IndiceK += 1
                    Case VI.ObjetPer.ObaGrade, VI.ObjetPer.ObaGradeDetache
                        Constructeur.InfoExtraite(IndiceK, NoObjet, 0) = 15
                        IndiceK += 1
                        Constructeur.InfoExtraite(IndiceK, NoObjet, 0) = 16
                        IndiceK += 1
                    Case VI.ObjetPer.ObaPostebud
                        Constructeur.InfoExtraite(IndiceK, NoObjet, 0) = 12
                        IndiceK += 1
                        Constructeur.InfoExtraite(IndiceK, NoObjet, 0) = 6
                        IndiceK += 1
                    Case VI.ObjetPer.ObaOrganigramme
                        Constructeur.InfoExtraite(IndiceK, NoObjet, 0) = 14
                        IndiceK += 1
                End Select

                Return Constructeur.OrdreSqlDynamique

            End Function

            Public Sub New(ByVal Modele As ModeleRH)
                MyBase.New()
                VirModeleRh = Modele
            End Sub

        End Class
    End Namespace
End Namespace