﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Datas
    Public Class ObjetDossierREF
        Inherits Virtualia.Ressources.Datas.GenericDossier
        Private WsNomTableReference As String
        Private WsValeur As String = ""
        Private WsCode As String = ""
        '
        Public ReadOnly Property NomTable_Reference() As String
            Get
                Return WsNomTableReference
            End Get
        End Property

        Public ReadOnly Property Valeur() As String
            Get
                Return WsValeur
            End Get
        End Property

        Public ReadOnly Property Code() As String
            Get
                If WsCode = "" Then
                    Return WsValeur
                Else
                    Return WsCode
                End If
            End Get
        End Property

        Public Sub New(ByVal Modele As Virtualia.Systeme.MetaModele.Donnees.ModeleRH, ByVal Pvue As Integer, ByVal Ide As Integer)
            MyBase.New(Modele, Pvue, Ide)
        End Sub

        Public Sub New(ByVal Modele As Virtualia.Systeme.MetaModele.Donnees.ModeleRH, ByVal Pvue As Integer, ByVal NomdelaTable As String, ByVal ChaineLue As String)
            MyBase.New(Modele, Pvue, ChaineLue)
            Dim TableauData(0) As String
            WsNomTableReference = NomdelaTable
            If ChaineLue = "" Then
                Exit Sub
            End If
            TableauData = Strings.Split(ChaineLue, VI.Tild, -1)
            WsValeur = TableauData(1)
            WsCode = TableauData(2)
        End Sub
    End Class
End Namespace