﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetItineraire
        Implements IGestionVirFiche(Of ITI_DISTANCE)

        Private WsListeIti As List(Of ITI_DISTANCE) = New List(Of ITI_DISTANCE)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesVilles(ByVal Lettre As String) As List(Of ITI_DISTANCE)
            Get
                If WsListeIti Is Nothing Then
                    Return Nothing
                End If
                Dim Predicat As New Virtualia.Ressources.Predicats.PredicateUniversel(Lettre, VI.PointdeVue.PVueItineraire)
                Dim LstRes As List(Of ITI_DISTANCE)
                Dim LstTri As List(Of ITI_DISTANCE) = Nothing

                LstRes = WsListeIti.FindAll(AddressOf Predicat.RechercherLettre)
                If LstRes Is Nothing Then
                    Return Nothing
                End If
                LstTri = (From instance In LstRes Select instance _
                       Where instance.Ide_Dossier > 0 _
                       Order By instance.Villedepart Ascending).ToList

                Return LstTri
            End Get
        End Property

        Public ReadOnly Property ListeDesVilles() As List(Of ITI_DISTANCE)
            Get
                Dim LstRes As List(Of ITI_DISTANCE)
                LstRes = (From instance In WsListeIti Select instance _
                                Where instance.Ide_Dossier > 0 _
                                Order By instance.Villedepart Ascending, instance.Villearrivee Ascending).ToList
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredeVilles() As Integer Implements IGestionVirFiche(Of ITI_DISTANCE).NombredItems
            Get
                Return WsListeIti.Count
            End Get
        End Property

        Public Function Fiche_Itineraire(ByVal Ide_Dossier As Integer) As ITI_DISTANCE Implements IGestionVirFiche(Of ITI_DISTANCE).GetFiche
            Return WsListeIti.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Itineraire(ByVal Ville_de_Depart As String) As ITI_DISTANCE Implements IGestionVirFiche(Of ITI_DISTANCE).GetFiche
            Return WsListeIti.Find(Function(m) m.Departement_villedepart = Ville_de_Depart)
        End Function

        Public Sub Actualiser() Implements IGestionVirFiche(Of ITI_DISTANCE).Actualiser
            WsListeIti.Clear()
            ConstituerListe()
        End Sub

        Private Sub ConstituerListe()
            WsListeIti.Clear()
            WsListeIti.AddRange(CreationCollectionVIRFICHE(Of ITI_DISTANCE).GetCollection(WsNomUtilisateur, VirServiceServeur))
        End Sub

        Public Sub New(ByVal NomUtilisateur As String)
            WsNomUtilisateur = NomUtilisateur
            Call ConstituerListe()
        End Sub

    End Class
End Namespace

