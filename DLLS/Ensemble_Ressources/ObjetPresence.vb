﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetPresence
        Implements IGestionVirFiche(Of PRE_IDENTIFICATION)

        Private WsListePresences As List(Of PRE_IDENTIFICATION) = New List(Of PRE_IDENTIFICATION)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesPresences(ByVal Lettre As String) As List(Of PRE_IDENTIFICATION)
            Get
                If WsListePresences Is Nothing Then
                    Return Nothing
                End If
                Dim Predicat As New Virtualia.Ressources.Predicats.PredicateUniversel(Lettre, VI.PointdeVue.PVuePresences)
                Dim LstRes As List(Of PRE_IDENTIFICATION)
                Dim LstTri As List(Of PRE_IDENTIFICATION) = Nothing

                LstRes = WsListePresences.FindAll(AddressOf Predicat.RechercherLettre)
                If LstRes Is Nothing Then
                    Return Nothing
                End If
                LstTri = (From instance In LstRes Select instance _
                       Where instance.Ide_Dossier > 0 _
                       Order By instance.Intitule Ascending).ToList

                Return LstTri
            End Get
        End Property

        Public ReadOnly Property ListeDesPresences() As List(Of PRE_IDENTIFICATION)
            Get
                Dim LstRes As List(Of PRE_IDENTIFICATION)
                LstRes = (From instance In WsListePresences Select instance _
                                Where instance.Ide_Dossier > 0 _
                                Order By instance.Intitule Ascending).ToList
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredItems() As Integer Implements IGestionVirFiche(Of PRE_IDENTIFICATION).NombredItems
            Get
                Return WsListePresences.Count
            End Get
        End Property

        Public Function Fiche_Presence(ByVal IdeDossier As Integer) As PRE_IDENTIFICATION Implements IGestionVirFiche(Of PRE_IDENTIFICATION).GetFiche
            Return WsListePresences.Find(Function(m) m.Ide_Dossier = IdeDossier)
        End Function

        Public Function Fiche_Presence(ByVal Intitule As String) As PRE_IDENTIFICATION Implements IGestionVirFiche(Of PRE_IDENTIFICATION).GetFiche
            Return WsListePresences.Find(Function(m) m.Intitule = Intitule)
        End Function

        Public Sub Actualiser() Implements IGestionVirFiche(Of PRE_IDENTIFICATION).Actualiser
            WsListePresences.Clear()
            WsListePresences = Nothing
            ConstituerListe()
        End Sub

        Private Sub ConstituerListe()
            WsListePresences.Clear()
            Dim FicheIdentite As PRE_IDENTIFICATION

            '** 1  Liste de toutes les présences
            WsListePresences.AddRange(CreationCollectionVIRFICHE(Of PRE_IDENTIFICATION).GetCollection(WsNomUtilisateur, VirServiceServeur))
            If (WsListePresences.Count <= 0) Then
                Return
            End If

            '** 2  Liste de tous les accords RTT
            Dim lstacc As List(Of PRE_ACCORD_RTT) = CreationCollectionVIRFICHE(Of PRE_ACCORD_RTT).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstacc.ForEach(Sub(f)
                               FicheIdentite = Fiche_Presence(f.Ide_Dossier)
                               If FicheIdentite Is Nothing Then
                                   Return
                               End If
                               FicheIdentite.Ajouter_Accord(f)
                           End Sub)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String)
            WsNomUtilisateur = NomUtilisateur
            Call ConstituerListe()
        End Sub

    End Class
End Namespace
