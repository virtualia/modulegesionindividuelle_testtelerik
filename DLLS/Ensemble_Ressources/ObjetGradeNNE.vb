﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetGradeNNE
        Implements IGestionVirFicheModif(Of GRD_GRADE_NNE)

        Private WsListeComplete As List(Of GRD_GRADE_NNE) = Nothing
        Private WsListePartielle As List(Of GRD_GRADE_NNE) = Nothing
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesGrades() As List(Of GRD_GRADE_NNE)
            Get
                If WsListeComplete Is Nothing Then
                    Return Nothing
                End If
                Return (From grade In WsListeComplete Order By grade.Intitule Ascending).ToList

            End Get
        End Property

        Public ReadOnly Property ListeDesGrades(ByVal LstCodes As List(Of String)) As List(Of GRD_GRADE_NNE)
            Get
                If LstCodes Is Nothing Then
                    Return WsListePartielle
                End If
                If WsListeComplete Is Nothing Then
                    Return Nothing
                End If
                Dim IndiceI As Integer
                Dim FicheREF As GRD_GRADE_NNE
                WsListePartielle = New List(Of GRD_GRADE_NNE)

                For IndiceI = 0 To LstCodes.Count - 1
                    FicheREF = WsListeComplete.Find(Function(m) m.CodeNomenclature = LstCodes.Item(IndiceI))
                    If FicheREF IsNot Nothing Then
                        WsListePartielle.Add(FicheREF)
                    End If
                Next IndiceI

                Return WsListePartielle

            End Get
        End Property

        Public ReadOnly Property ListeDesGrades(ByVal NoInfoTri As Integer) As List(Of GRD_GRADE_NNE)
            Get
                If WsListeComplete Is Nothing Then
                    Return Nothing
                End If
                Select Case NoInfoTri
                    Case 0
                        Return (From grade In WsListeComplete Order By grade.Ide_Dossier Ascending).ToList
                    Case 1
                        Return (From grade In WsListeComplete Order By grade.CodeNomenclature Ascending).ToList
                    Case Else
                        Return (From grade In WsListeComplete Order By grade.Intitule Ascending).ToList
                End Select
            End Get
        End Property

        Public ReadOnly Property NombredeGrades() As Integer Implements IGestionVirFicheModif(Of GRD_GRADE_NNE).NombredItems
            Get
                Return WsListeComplete.Count
            End Get
        End Property

        Public Function Fiche_Grade(ByVal Ide_Dossier As Integer) As GRD_GRADE_NNE Implements IGestionVirFicheModif(Of GRD_GRADE_NNE).GetFiche
            If WsListeComplete Is Nothing Then
                Return Nothing
            End If
            Return WsListeComplete.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Grade(ByVal CodeNomenclature As String) As GRD_GRADE_NNE

            If WsListeComplete Is Nothing Then
                Return Nothing
            End If
            Return WsListeComplete.Find(Function(m) m.CodeNomenclature = CodeNomenclature)

        End Function

        Public Function Fiche_GradeInverse(ByVal Intitule As String) As GRD_GRADE_NNE Implements IGestionVirFicheModif(Of GRD_GRADE_NNE).GetFiche
            Return WsListeComplete.Find(Function(m) m.Intitule = Intitule)
        End Function

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of GRD_GRADE_NNE).SupprimerItem
            Dim FicheGrade As GRD_GRADE_NNE
            FicheGrade = Fiche_Grade(Ide)
            If FicheGrade IsNot Nothing Then
                WsListeComplete.Remove(FicheGrade)
            End If
        End Sub

        Public Sub AjouterItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of GRD_GRADE_NNE).AjouterItem
            SupprimerItem(Ide)
            ConstituerListe(Ide)
        End Sub

        Public Sub Actualiser() Implements IGestionVirFicheModif(Of GRD_GRADE_NNE).Actualiser
            WsListeComplete.Clear()
            ConstituerListe(0)
        End Sub

        Private Sub ConstituerListe(ByVal Ide As Integer)
            WsListeComplete.Clear()
            Dim FicheGrade As GRD_GRADE_NNE

            '** 1  Liste de tous les établissements
            WsListeComplete.AddRange(CreationCollectionVIRFICHE(Of GRD_GRADE_NNE).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide))
            If (WsListeComplete.Count <= 0) Then
                Return
            End If

            '** 2  Liste de tous les echelons
            Dim lstech As List(Of GRD_GRILLE_NNE) = CreationCollectionVIRFICHE(Of GRD_GRILLE_NNE).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstech.ForEach(Sub(f)
                               FicheGrade = Fiche_Grade(f.Ide_Dossier)

                               If (FicheGrade Is Nothing) Then
                                   Return
                               End If

                               FicheGrade.Ajouter_Echelon(f)
                           End Sub)

            '** 3  Liste de toutes les Catégories
            Dim lstcat As List(Of GRD_CATEGORIE_NNE) = CreationCollectionVIRFICHE(Of GRD_CATEGORIE_NNE).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstcat.ForEach(Sub(f)
                               FicheGrade = Fiche_Grade(f.Ide_Dossier)

                               If (FicheGrade Is Nothing) Then
                                   Return
                               End If

                               FicheGrade.Ajouter_Categorie(f)
                           End Sub)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String, ByVal Ide As Integer)
            WsNomUtilisateur = NomUtilisateur
            WsListeComplete = New List(Of GRD_GRADE_NNE)
            ConstituerListe(Ide)
        End Sub

    End Class
End Namespace

