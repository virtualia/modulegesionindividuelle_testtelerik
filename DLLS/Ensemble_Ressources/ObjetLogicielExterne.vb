﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetLogicielExterne
        Implements IGestionVirFicheModif(Of OUT_LOGICIEL)

        Private WsListeInterface As List(Of OUT_LOGICIEL)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesLogiciels() As List(Of OUT_LOGICIEL)
            Get
                Dim LstRes As List(Of OUT_LOGICIEL)

                LstRes = (From instance In WsListeInterface Select instance _
                                    Where instance.Ide_Dossier > 0 _
                                    Order By instance.Nom Ascending).ToList

                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredeLogiciels() As Integer Implements IGestionVirFicheModif(Of OUT_LOGICIEL).NombredItems
            Get
                Return WsListeInterface.Count
            End Get
        End Property

        Public Function Fiche_Logiciel(ByVal Ide_Dossier As Integer) As OUT_LOGICIEL Implements IGestionVirFicheModif(Of OUT_LOGICIEL).GetFiche
            Return WsListeInterface.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Logiciel(ByVal Intitule As String) As OUT_LOGICIEL Implements IGestionVirFicheModif(Of OUT_LOGICIEL).GetFiche
            Return WsListeInterface.Find(Function(m) m.Nom = Intitule)
        End Function

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of OUT_LOGICIEL).SupprimerItem
            Dim FicheInterface As OUT_LOGICIEL
            FicheInterface = Fiche_Logiciel(Ide)
            If FicheInterface IsNot Nothing Then
                WsListeInterface.Remove(FicheInterface)
            End If
        End Sub

        Public Sub AjouterItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of OUT_LOGICIEL).AjouterItem
            SupprimerItem(Ide)
            ConstituerListe(Ide)
        End Sub

        Public Sub Actualiser() Implements IGestionVirFicheModif(Of OUT_LOGICIEL).Actualiser
            WsListeInterface.Clear()
            ConstituerListe(0)
        End Sub

        Private Sub ConstituerListe(ByVal Ide As Integer)
            WsListeInterface.Clear()
            Dim FicheLogiciel As OUT_LOGICIEL

            '** 1  Liste de tous les logiciels
            WsListeInterface.AddRange(CreationCollectionVIRFICHE(Of OUT_LOGICIEL).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide))
            If (WsListeInterface.Count <= 0) Then
                Return
            End If

            '** 2  Liste de toutes les rubriques
            Dim lstrub As List(Of OUT_RUBRIQUES) = CreationCollectionVIRFICHE(Of OUT_RUBRIQUES).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstrub.ForEach(Sub(f)
                               FicheLogiciel = Fiche_Logiciel(f.Ide_Dossier)

                               If (FicheLogiciel Is Nothing) Then
                                   Return
                               End If

                               FicheLogiciel.Ajouter_Rubrique(f)
                           End Sub)

            '** 3  Liste de tous les contextes
            Dim lstcont As List(Of OUT_CONTEXTE) = CreationCollectionVIRFICHE(Of OUT_CONTEXTE).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstcont.ForEach(Sub(f)
                                FicheLogiciel = Fiche_Logiciel(f.Ide_Dossier)

                                If (FicheLogiciel Is Nothing) Then
                                    Return
                                End If

                                FicheLogiciel.Fiche_Contexte = f
                            End Sub)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String, ByVal Ide As Integer)
            WsNomUtilisateur = NomUtilisateur
            WsListeInterface = New List(Of OUT_LOGICIEL)
            ConstituerListe(Ide)
        End Sub

    End Class
End Namespace
