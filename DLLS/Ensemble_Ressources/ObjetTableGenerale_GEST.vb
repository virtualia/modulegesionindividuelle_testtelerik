﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.TablesObjet.ShemaREF
Imports Virtualia.Systeme.MetaModele

Namespace Datas
    Public Class ObjetTableGenerale_GEST
        Implements IGestionVirFicheModif(Of TAB_DESCRIPTION_GEST)

        Private WsListeTables As List(Of TAB_DESCRIPTION_GEST)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesTables() As List(Of TAB_DESCRIPTION_GEST)
            Get
                Dim LstRes As List(Of TAB_DESCRIPTION_GEST)
                LstRes = (From instance In WsListeTables Select instance Where instance.Ide_Dossier > 0 Order By instance.Intitule_GEST Ascending).ToList
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredeTables() As Integer Implements IGestionVirFicheModif(Of TAB_DESCRIPTION_GEST).NombredItems
            Get
                Return WsListeTables.Count
            End Get
        End Property

        Public Function Fiche_Table(ByVal Ide_Dossier As Integer) As TAB_DESCRIPTION_GEST Implements IGestionVirFicheModif(Of TAB_DESCRIPTION_GEST).GetFiche
            Return WsListeTables.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Table(ByVal Intitule As String) As TAB_DESCRIPTION_GEST Implements IGestionVirFicheModif(Of TAB_DESCRIPTION_GEST).GetFiche
            Return WsListeTables.Find(Function(m) m.Intitule_GEST = Intitule)
        End Function

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of TAB_DESCRIPTION_GEST).SupprimerItem
            Dim FicheTable As TAB_DESCRIPTION_GEST
            FicheTable = Fiche_Table(Ide)
            If FicheTable IsNot Nothing Then
                WsListeTables.Remove(FicheTable)
            End If
        End Sub

        Public Sub AjouterItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of TAB_DESCRIPTION_GEST).AjouterItem
            SupprimerItem(Ide)
            ConstituerListe()
        End Sub

        Public Sub Actualiser() Implements IGestionVirFicheModif(Of TAB_DESCRIPTION_GEST).Actualiser
            WsListeTables.Clear()
            ConstituerListe()
        End Sub

        Private Sub ConstituerListe()
            WsListeTables.Clear()
            Dim FicheTable As TAB_DESCRIPTION_GEST

            '** 1  Liste de toutes les tables
            WsListeTables.AddRange(CreationCollectionVIRFICHE(Of TAB_DESCRIPTION_GEST).GetCollection(WsNomUtilisateur, VirServiceServeur))
            If (WsListeTables.Count <= 0) Then
                Return
            End If

            '** 2  Liste de toutes les valeurs
            Dim lstval As List(Of TAB_LISTE_GEST) = CreationCollectionVIRFICHE(Of TAB_LISTE_GEST).GetCollection(WsNomUtilisateur, VirServiceServeur)
            lstval.ForEach(Sub(f)
                               FicheTable = Fiche_Table(f.Ide_Dossier)
                               If (FicheTable Is Nothing) Then
                                   Return
                               End If
                               FicheTable.Ajouter_Valeur(f)
                           End Sub)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String)
            WsNomUtilisateur = NomUtilisateur
            WsListeTables = New List(Of TAB_DESCRIPTION_GEST)
            ConstituerListe()
        End Sub

    End Class
End Namespace
