﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetMission
        Implements IGestionVirFicheModif(Of MIS_CARACTERISTIC)

        Private WsListeMissions As List(Of MIS_CARACTERISTIC)
        Private WsNomUtilisateur As String
        Private WsTabAnnees As ArrayList

        Public ReadOnly Property ListeDesMissions_Alpha(ByVal Lettre As String, ByVal Annee As String) As List(Of MIS_CARACTERISTIC)
            Get
                If WsListeMissions Is Nothing Then
                    Return Nothing
                End If
                Dim Predicat As New Virtualia.Ressources.Predicats.PredicateUniversel(Lettre, VI.PointdeVue.PVueMission)
                Dim LstRes As List(Of MIS_CARACTERISTIC)
                Dim LstTri As List(Of MIS_CARACTERISTIC) = Nothing

                LstRes = WsListeMissions.FindAll(AddressOf Predicat.RechercherLettre)
                If LstRes Is Nothing Then
                    Return Nothing
                End If
                Select Case Annee
                    Case Is = ""
                        LstTri = (From instance In LstRes Select instance _
                               Where instance.Ide_Dossier > 0 _
                               Order By instance.Intitule Ascending).ToList

                    Case Else
                        LstTri = (From instance In LstRes Select instance _
                               Where instance.AnneeMission = Annee _
                               Order By instance.Intitule Ascending).ToList

                End Select
                Return LstTri
            End Get
        End Property

        Public ReadOnly Property ListeDesMissions(ByVal Annee As String, Optional ByVal Mois As Integer = 0) As List(Of MIS_CARACTERISTIC)
            Get
                If WsListeMissions Is Nothing Then
                    Return Nothing
                End If
                Dim LstRes As List(Of MIS_CARACTERISTIC) = Nothing

                If Annee <> "" Then
                    Select Case Mois
                        Case Is = 0
                            LstRes = (From instance In WsListeMissions Select instance _
                               Where instance.AnneeMission = Annee _
                                Order By instance.MoisMission Ascending, instance.ObjetdelaMission Ascending, _
                                         instance.Intitule Ascending).ToList
                        Case Else
                            LstRes = (From instance In WsListeMissions Select instance _
                               Where instance.AnneeMission = Annee And instance.MoisMission = Mois _
                                Order By instance.ObjetdelaMission Ascending, _
                                         instance.Intitule Ascending).ToList
                    End Select
                End If
                If LstRes Is Nothing Then
                    Return Nothing
                End If
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredeMissions() As Integer Implements IGestionVirFiche(Of MIS_CARACTERISTIC).NombredItems
            Get
                Return WsListeMissions.Count
            End Get
        End Property

        Public Function ItemSelectionAnnee(ByVal Index As Integer) As String

            If WsTabAnnees Is Nothing Then
                Return ""
            End If
            Select Case Index
                Case 0 To WsTabAnnees.Count - 1
                    Return WsTabAnnees(Index).ToString
            End Select
            Return ""

        End Function

        Public Function Fiche_Mission(ByVal Ide_Dossier As Integer) As MIS_CARACTERISTIC Implements IGestionVirFicheModif(Of MIS_CARACTERISTIC).GetFiche
            Return WsListeMissions.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Mission(ByVal Intitule As String) As MIS_CARACTERISTIC Implements IGestionVirFicheModif(Of MIS_CARACTERISTIC).GetFiche
            Return WsListeMissions.Find(Function(m) m.Intitule = Intitule)
        End Function

        Public Sub EnrichirItem(ByVal Ide As Integer)
            EnrichirListe(Ide)
        End Sub

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of MIS_CARACTERISTIC).SupprimerItem
            Dim FicheMission As MIS_CARACTERISTIC
            FicheMission = Fiche_Mission(Ide)
            If FicheMission IsNot Nothing Then
                WsListeMissions.Remove(FicheMission)
            End If
        End Sub

        Public Sub AjouterItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of MIS_CARACTERISTIC).AjouterItem
            SupprimerItem(Ide)
            ConstituerListe(Ide)
        End Sub

        Public Sub Actualiser() Implements IGestionVirFicheModif(Of MIS_CARACTERISTIC).Actualiser
            WsListeMissions.Clear()
            ConstituerListe(0)
        End Sub

        Private Sub EnrichirListe(ByVal Ide As Integer)
            If WsListeMissions.Count <= 0 Then
                Exit Sub
            End If
            Dim FicheMission As MIS_CARACTERISTIC

            '** 2  Liste de toutes les affectations
            Dim lstaff As List(Of MIS_AFFECTATION) = CreationCollectionVIRFICHE(Of MIS_AFFECTATION).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstaff.ForEach(Sub(f)
                               FicheMission = Fiche_Mission(f.Ide_Dossier)
                               If FicheMission Is Nothing Then
                                   Exit Sub
                               End If
                               FicheMission.Ajouter_Affectation(f)
                           End Sub)

            '** 3  Liste de tous les bons de transport
            Dim lsttrans As List(Of MIS_TRANSPORT) = CreationCollectionVIRFICHE(Of MIS_TRANSPORT).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lsttrans.ForEach(Sub(f)
                                 FicheMission = Fiche_Mission(f.Ide_Dossier)
                                 If FicheMission Is Nothing Then
                                     Exit Sub
                                 End If
                                 FicheMission.Ajouter_Transport(f)
                             End Sub)
        End Sub

        Private Sub ConstituerListe(ByVal Ide As Integer)
            WsListeMissions.Clear()
            If Ide = 0 Then
                WsTabAnnees = New ArrayList
            End If

            '** 1  Liste de toutes les missions
            Dim lstmiss As List(Of MIS_CARACTERISTIC) = CreationCollectionVIRFICHE(Of MIS_CARACTERISTIC).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            lstmiss.ForEach(Sub(f)

                                WsListeMissions.Add(f)

                                If (Ide <> 0) Then
                                    Return
                                End If

                                Dim SiAfaire As Boolean = (f.AnneeMission <> "")

                                For Each s As Object In WsTabAnnees
                                    If (f.AnneeMission = s.ToString()) Then
                                        SiAfaire = False
                                        Exit For
                                    End If
                                Next

                                If Not (SiAfaire) Then
                                    Return
                                End If

                                If WsTabAnnees.Count = 0 Then
                                    WsTabAnnees.Add(f.AnneeMission)
                                    Return
                                End If

                                For cpt As Integer = 0 To WsTabAnnees.Count - 1
                                    If Integer.Parse(f.AnneeMission) < Integer.Parse(WsTabAnnees(cpt).ToString()) Then
                                        WsTabAnnees.Insert(cpt, f.AnneeMission)
                                        SiAfaire = False
                                        Exit For
                                    End If
                                Next
                                If SiAfaire Then
                                    WsTabAnnees.Add(f.AnneeMission)
                                End If

                            End Sub)
        End Sub

        Public Sub New(ByVal NomUtilisateur As String, ByVal Ide As Integer)
            WsNomUtilisateur = NomUtilisateur
            WsListeMissions = New List(Of MIS_CARACTERISTIC)
            Call ConstituerListe(Ide)
        End Sub

    End Class
End Namespace

