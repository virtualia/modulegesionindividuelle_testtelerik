﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.Systeme.MetaModele.Donnees
Imports Virtualia.Systeme.MetaModele.Predicats
Imports Virtualia.TablesObjet.ShemaVUE
Imports Virtualia.Systeme.Sgbd.Sql
Imports Virtualia.Systeme.Parametrage.BasesdeDonnees

Namespace Datas
    Public Class GenericDossier
        Inherits List(Of VIR_FICHE)
        Implements IGenericDossier

        Private WsRhModele As ModeleRH
        Private WsPointdeVue As Integer = VI.PointdeVue.PVueApplicatif
        Private WsIdentifiant As Integer

        Public ReadOnly Property V_Modele() As Integer
            Get
                Return WsRhModele.InstanceProduit.ClefModele
            End Get
        End Property

        Public ReadOnly Property V_PointdeVue() As Integer Implements IGenericDossier.V_PointdeVue
            Get
                Return WsPointdeVue
            End Get
        End Property

        Public ReadOnly Property V_Identifiant() As Integer Implements IGenericDossier.V_Identifiant
            Get
                Return WsIdentifiant
            End Get
        End Property

        Public ReadOnly Property V_Identifiant(ByVal NomUtilisateur As String, ByVal SiActualiser As Boolean) As Integer
            Get
                If NomUtilisateur = "" Then
                    Return 0
                End If
                If Not (SiActualiser) And Me.Count > 0 Then
                    Return WsIdentifiant
                End If
                Me.Clear()

                Dim RhPvue As PointdeVue = (From it As PointdeVue In WsRhModele Where it.Numero = WsPointdeVue Select it).FirstOrDefault()
                If RhPvue Is Nothing Then
                    Return 0
                End If

                Dim LstObjets As New List(Of Integer)
                RhPvue.ForEach(Sub(obj As Objet)
                                   LstObjets.Add(obj.Numero)
                               End Sub)

                V_LireDossier(NomUtilisateur, LstObjets)

                Return WsIdentifiant
            End Get
        End Property

        Public WriteOnly Property V_ActualiserUnObjet(ByVal NomUtilisateur As String, ByVal NomTable As String) As Integer
            Set(ByVal value As Integer)
                Dim IndiceV As Integer = 0
                Do
                    If IndiceV > Me.Count - 1 Then
                        Exit Do
                    End If
                    If Me(IndiceV).GetType().Name = NomTable Then
                        RemoveAt(IndiceV)
                    Else
                        IndiceV += 1
                    End If
                Loop
                Call LectureObjet(NomUtilisateur, value)
            End Set
        End Property

        Public Property V_ListeDesFiches() As List(Of VIR_FICHE) Implements IGenericDossier.V_ListeDesFiches
            Get
                Return Me
            End Get
            Set(ByVal value As List(Of VIR_FICHE))
                Me.Clear()
                value.ForEach(Sub(fiche)
                                  Add(fiche)
                              End Sub)
            End Set
        End Property

        Public ReadOnly Property V_ListeDesFiches(ByVal NumObjet As Integer) As List(Of VIR_FICHE)
            Get
                Return (From Instance In Me Select Instance Where Instance.PointdeVue = WsPointdeVue And Instance.NumeroObjet = NumObjet).ToList
            End Get
        End Property

        Public ReadOnly Property V_ListeDesFiches(ByVal LstObjets As List(Of Integer)) As List(Of VIR_FICHE)
            Get
                Dim Predicat As New PredicateFiche(WsPointdeVue, LstObjets)
                Return Me.FindAll(AddressOf Predicat.SiFicheDansListeObjets)
            End Get
        End Property

        Public ReadOnly Property V_UnObjetLu(ByVal NomUtilisateur As String, ByVal NomTable As String, ByVal NumObjet As Integer) As Integer
            Get
                Dim IndiceF As Integer
                IndiceF = V_IndexTableDescendant(NomTable, NumObjet)
                If IndiceF <> -1 Then
                    Return IndiceF
                End If
                Call LectureObjet(NomUtilisateur, NumObjet)
                Return V_IndexTableDescendant(NomTable, NumObjet)
            End Get
        End Property

        Public ReadOnly Property V_NombredeFiches() As Integer
            Get
                Return Me.Count
            End Get
        End Property

        Public ReadOnly Property V_IndexFiche(ByVal Index As Integer, ByVal NomTable As String) As Integer
            Get
                Dim IndiceV As Integer
                Dim IndexTable As Integer = 0

                If Me.Count = 0 Then
                    Return -1
                End If
                For IndiceV = Me.Count - 1 To 0 Step -1
                    Select Case Me(IndiceV).GetType().Name
                        Case NomTable
                            If IndexTable = Index Then
                                Return IndiceV
                            End If
                            IndexTable += 1
                    End Select
                Next IndiceV

                Return -1
            End Get
        End Property

        Public ReadOnly Property V_IndexFicheAlpha(ByVal Index As Integer, ByVal NomTable As String) As Integer
            Get
                Dim IndiceV As Integer
                Dim IndexTable As Integer = 0

                If Me.Count = 0 Then
                    Return -1
                End If
                For IndiceV = 0 To Me.Count - 1
                    Select Case Me(IndiceV).GetType.Name
                        Case NomTable
                            If IndexTable = Index Then
                                Return IndiceV
                            End If
                            IndexTable += 1
                    End Select
                Next IndiceV
                Return -1
            End Get
        End Property

        Public ReadOnly Property V_NomdelaTable(ByVal NumObjet As Integer) As String
            Get
                Dim FicheGeneric As Object
                Dim IndiceI As Integer
                For IndiceI = 0 To Me.Count - 1
                    FicheGeneric = Me(IndiceI)
                    If FicheGeneric.NumeroObjet = NumObjet Then
                        Return Me(IndiceI).GetType.Name
                    End If
                Next IndiceI
                Return ""
            End Get
        End Property

        Public ReadOnly Property V_IndexTableDescendant(ByVal NomTable As String) As Integer
            Get
                Dim IndiceV As Integer
                Dim FicheVirt As Object
                If Me.Count = 0 Then
                    Return -1
                End If
                For IndiceV = 0 To Me.Count - 1
                    FicheVirt = Me(IndiceV)
                    Select Case FicheVirt.GetType.Name
                        Case Is = NomTable
                            Return IndiceV
                    End Select
                Next IndiceV
                Return -1
            End Get
        End Property

        Public ReadOnly Property V_IndexTableDescendant(ByVal NomTable As String, ByVal NumObjet As Integer) As Integer
            Get
                Dim IndiceV As Integer
                If Me.Count = 0 Then
                    Return -1
                End If
                Dim Fiche As Virtualia.Systeme.MetaModele.VIR_FICHE
                For IndiceV = 0 To Me.Count - 1
                    Fiche = Me(IndiceV)
                    Select Case Fiche.GetType.Name
                        Case Is = NomTable
                            If Fiche.NumeroObjet = NumObjet Then
                                Return IndiceV
                            End If
                    End Select
                Next IndiceV
                Return -1
            End Get
        End Property

        Public ReadOnly Property V_IndexTableAscendant(ByVal NomTable As String) As Integer
            Get
                Dim IndiceV As Integer
                Dim FicheVirt As Object
                If Me.Count = 0 Then
                    Return -1
                End If
                For IndiceV = Me.Count - 1 To 0 Step -1
                    FicheVirt = Me(IndiceV)
                    Select Case FicheVirt.GetType.Name
                        Case Is = NomTable
                            Return IndiceV
                    End Select
                Next IndiceV
                Return -1
            End Get
        End Property

        Public Sub V_RemoveFiche(ByVal Index As Integer) Implements IGenericDossier.V_RemoveFiche
            RemoveAt(Index)
        End Sub

        Public Sub V_RemoveFiche(ByVal Fiche As VIR_FICHE) Implements IGenericDossier.V_RemoveFiche
            Remove(Fiche)
        End Sub

        Public Function V_LireDossier(ByVal NomUtilisateur As String, ByVal ListeObjets As List(Of Integer)) As Boolean Implements IGenericDossier.V_LireDossier
            Dim IndiceO As Integer
            Dim NumObjet As Integer = 0
            Dim TableauData As String()
            Dim TableauObjet As List(Of String)
            Dim Nomtable As String = ""
            Dim fiche As VIR_FICHE = Nothing

            Me.Clear()

            TableauObjet = VirServiceServeur.LectureDossier_ToListeChar(NomUtilisateur, WsPointdeVue, WsIdentifiant, False, ListeObjets)

            For IndiceO = 0 To TableauObjet.Count - 1

                If TableauObjet(IndiceO) = "" Then
                    Exit For
                End If
                TableauData = Strings.Split(TableauObjet(IndiceO), VI.Tild, -1)

                If Strings.Left(TableauObjet(IndiceO), 6) = "Objet=" Then
                    NumObjet = CInt(Strings.Right(TableauData(0), TableauData(0).Length - 6))
                    Nomtable = V_NomdelaTable(NumObjet)
                    Continue For
                Else
                    If Nomtable <> "" Then
                        Continue For
                    End If
                    If TableauData(0) = "" Then
                        Continue For
                    End If

                    Try
                        Dim IConstructeur As Virtualia.Systeme.IVConstructeur
                        Select Case WsPointdeVue
                            Case VI.PointdeVue.PVueApplicatif
                                IConstructeur = New Virtualia.TablesObjet.ShemaPER.VConstructeur(WsRhModele.InstanceProduit.ClefModele)
                            Case Else
                                IConstructeur = New Virtualia.TablesObjet.ShemaREF.VConstructeur(WsRhModele.InstanceProduit.ClefModele, WsPointdeVue)
                        End Select
                        fiche = IConstructeur.V_NouvelleFiche(NumObjet, 0)

                        If fiche IsNot Nothing Then
                            fiche.ContenuTable = TableauObjet(IndiceO)
                            Add(fiche)
                        End If

                    Catch Ex As Exception
                        Exit Try
                    End Try

                End If

            Next IndiceO

            Return True

        End Function

        Public Function V_MettreAJourFiche(ByVal NomUtiSgbd As String, ByVal NomUtiSession As String, ByVal NumObjet As Integer,
                                           ByVal Ancienne As String, ByVal Nouvelle As String) As Boolean Implements IGenericDossier.V_MettreAJourFiche
            If NomUtiSgbd = "" Then
                Return False
            End If
            If Nouvelle = "" Or (Ancienne = Nouvelle) Then
                Return False
            End If
            If NomUtiSession = "" Then
                NomUtiSession = NomUtiSgbd
            End If
            Dim Cretour As Boolean = False
            If Ancienne = "" And Nouvelle <> "" Then
                Cretour = VirServiceServeur.MiseAjour_Fiche(NomUtiSgbd, WsPointdeVue, NumObjet, WsIdentifiant, "C", "", Nouvelle)
            Else
                Cretour = VirServiceServeur.MiseAjour_Fiche(NomUtiSgbd, WsPointdeVue, NumObjet, WsIdentifiant, "M", Ancienne, Nouvelle)
            End If
            Return Cretour
        End Function

        Public Function V_SupprimerFiche(ByVal NomUtiSgbd As String, ByVal NomUtiSession As String, ByVal NumObjet As Integer, ByVal Contenu As String) As Boolean Implements IGenericDossier.V_SupprimerFiche
            If NomUtiSgbd = "" Then
                Return False
            End If
            If Contenu = "" Then
                Return False
            End If
            If NomUtiSession = "" Then
                NomUtiSession = NomUtiSgbd
            End If
            Dim Cretour As Boolean = False
            Cretour = VirServiceServeur.MiseAjour_Fiche(NomUtiSgbd, WsPointdeVue, NumObjet, WsIdentifiant, "S", Contenu, "")
            Return Cretour
        End Function

        Public Function V_SupprimerDossier(ByVal NomUtiSgbd As String, ByVal NomUtiSession As String, ByVal InstanceBD As ParamSgbdDatabase) As Boolean Implements IGenericDossier.V_SupprimerDossier
            If NomUtiSgbd = "" Then
                Return False
            End If
            If NomUtiSession = "" Then
                NomUtiSession = NomUtiSgbd
            End If
            Dim Cretour As Boolean
            Dim Cpt As Integer = 0
            Dim Jrn As VUE_Journal
            Dim IndiceI As Integer
            Dim RhPvue As PointdeVue = (From it As PointdeVue In WsRhModele Where it.Numero = WsPointdeVue Select it).FirstOrDefault()
            If RhPvue Is Nothing Then
                Return False
            End If
            Dim TableauSql As List(Of String) = New List(Of String)
            Dim Constructeur As SqlInterne = New SqlInterne(WsRhModele, InstanceBD)

            RhPvue.ForEach(Sub(obj As Objet)
                               TableauSql.Add(Constructeur.SqlDeleteObjet(WsPointdeVue, RhPvue.Item(IndiceI).Numero, WsIdentifiant, "", "D"))
                           End Sub)
            Constructeur = Nothing

            For Each req As String In TableauSql
                If VirServiceServeur.MajSqlDirecte(NomUtiSgbd, req) Then
                    Cpt += 1
                End If
            Next
            If Cpt > 0 Then
                Jrn = New VUE_Journal(NomUtiSession, "D", VI.PointdeVue.PVueApplicatif, 1, WsIdentifiant)
                Jrn.ContenuTable = ""
                Cretour = VirServiceServeur.Journaliser_Maj(NomUtiSgbd, VI.PointdeVue.PVueApplicatif, 1, WsIdentifiant, "D", Jrn.Enregistrement)
            End If

            Return Cretour
        End Function

        Private Sub LectureObjet(ByVal NomUtilisateur As String, ByVal NumObjet As Integer)
            Dim TableauObjet As List(Of VIR_FICHE) = VirServiceServeur.LectureObjet_ToFiches(NomUtilisateur, WsPointdeVue, NumObjet, WsIdentifiant, True)
            If TableauObjet Is Nothing Then
                Exit Sub
            End If
            Me.AddRange(TableauObjet)
        End Sub

        Public Sub New(ByVal Modele As ModeleRH, ByVal PtdeVue As Integer, ByVal ChaineLue As String)
            MyBase.New()
            WsPointdeVue = PtdeVue
            WsRhModele = Modele

            If ChaineLue = "" Or ChaineLue = VI.Tild Then
                Exit Sub
            End If
            Dim TableauData As String() = Strings.Split(ChaineLue, VI.Tild, -1)
            WsIdentifiant = Integer.Parse(TableauData(0))
        End Sub

        Public Sub New(ByVal Modele As ModeleRH, ByVal PtdeVue As Integer, ByVal Ide As Integer)
            MyBase.New()
            WsPointdeVue = PtdeVue
            WsRhModele = Modele
            WsIdentifiant = Ide
        End Sub

    End Class

    Public Interface IGenericDossier
        ReadOnly Property V_PointdeVue As Integer
        ReadOnly Property V_Identifiant As Integer
        Property V_ListeDesFiches As List(Of VIR_FICHE)
        Sub V_RemoveFiche(ByVal fiche As VIR_FICHE)
        Sub V_RemoveFiche(ByVal index As Integer)
        Function V_LireDossier(ByVal NomUtilisateur As String, ByVal ListeObjets As List(Of Integer)) As Boolean
        Function V_MettreAJourFiche(ByVal NomUtiSgbd As String, ByVal NomUtiSession As String, ByVal NumObjet As Integer, ByVal Ancienne As String, ByVal Nouvelle As String) As Boolean
        Function V_SupprimerFiche(ByVal NomUtiSgbd As String, ByVal NomUtiSession As String, ByVal NumObjet As Integer, ByVal Contenu As String) As Boolean
        Function V_SupprimerDossier(ByVal NomUtiSgbd As String, ByVal NomUtiSession As String, ByVal InstanceBD As ParamSgbdDatabase) As Boolean
    End Interface
End Namespace

