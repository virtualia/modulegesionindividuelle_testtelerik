﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Virtualia.Systeme.MetaModele
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.Fonctions
Imports Virtualia.Systeme.MetaModele.Donnees
Imports Virtualia.Systeme.Parametrage.BasesdeDonnees
Imports Virtualia.Systeme.MetaModele.Expertes
Imports Virtualia.Ressources.Registre
Imports Virtualia.Ressources.Calculs
Imports Virtualia.Systeme.MetaModele.Outils
Imports Virtualia.Ressources.Datas
Imports Virtualia.Systeme.Sgbd.Sql
Imports System.Text
Imports Virtualia.TablesObjet.ShemaREF
Imports System.IO

Namespace Session
    Public Class GenericGlobal
        Private WsRhDates As CalculDates
        Private WsRhFct As Generales
        Private WsRhModele As ModeleRH
        Private WsInstanceSgbd As ParamSgbd
        Private WsRhExpertes As ExpertesRH
        Private WsRegistreVirtualia As ObjetConfiguration
        Private WsObjetCalcJour As ObjetCalculJour

        Private WsModeConnexion As String = "V4" 'V4=Via New Uti gestion, VI=Intranet Standard
        Private WsUrlWebServiceSession As String
        Private WsNomUtilisateurBd As String
        Private WsNoBd As Integer
        Private WsRepertoireVirtualia As String

        Private WsListeObjetsDico As List(Of FicheObjetDictionnaire) = Nothing
        Private WsListeInfosDico As List(Of FicheInfoDictionnaire) = Nothing
        Private WsListeExpertesDico As List(Of FicheInfoExperte) = Nothing

        Private WsArmoireComplete As List(Of ObjetDossierPER)

        Public ReadOnly Property PointeurRegistre As ObjetConfiguration
            Get
                Return WsRegistreVirtualia
            End Get
        End Property

        Public ReadOnly Property ObjetCalcJour As ObjetCalculJour
            Get
                If WsObjetCalcJour Is Nothing Then
                    WsObjetCalcJour = New ObjetCalculJour(WsNomUtilisateurBd, Nothing)
                End If
                Return WsObjetCalcJour
            End Get
        End Property

        Public ReadOnly Property UrlDestination(ByVal NoSession As String, ByVal Index As Integer, ByVal Param As String) As String
            Get
                Dim WseSession As Virtualia.Net.WebService.ServiceWcfSessions = New Virtualia.Net.WebService.ServiceWcfSessions
                Dim UrlAppli As String = WseSession.Url_Applicative(NoSession, Index, Param)
                WseSession = Nothing

                Return UrlAppli
            End Get
        End Property

        Public ReadOnly Property IDUtilisateur(ByVal NoSession As String) As String
            Get
                Dim WseSession As Virtualia.Net.WebService.ServiceWcfSessions = New Virtualia.Net.WebService.ServiceWcfSessions
                Dim CnxUti As Virtualia.Net.ServiceSessions.SessionUtilisateurType = WseSession.Utilisateur(NoSession)
                WseSession = Nothing
                If CnxUti Is Nothing Then
                    Return ""
                Else
                    Return CnxUti.ID_NomConnexion
                End If

            End Get
        End Property

        Public ReadOnly Property SiConnexionAutorisee(ByVal ChaineCnx As String, ByVal Identite As String, ByVal Password As String) As Boolean
            Get
                Dim WseSession As Virtualia.Net.WebService.ServiceWcfSessions = New Virtualia.Net.WebService.ServiceWcfSessions
                Dim SiOk As Boolean = WseSession.Si_ConnexionEstAutorisee(WsNoBd, _
                                                                      WsModeConnexion, _
                                                                      ChaineCnx, _
                                                                      Identite, _
                                                                      WsRhFct.DecryptageVirtualia(Password))
                WseSession = Nothing
                Return SiOk
            End Get
        End Property

        Public ReadOnly Property SiModificationPWOK(ByVal Identite As String, ByVal Password As String) As Boolean
            Get
                Dim WseSession As Virtualia.Net.WebService.ServiceWcfSessions = New Virtualia.Net.WebService.ServiceWcfSessions
                Dim SiOk As Boolean = WseSession.Si_ModificatonPasswordOK(WsNoBd, _
                                                                          WsModeConnexion, _
                                                                          Identite, _
                                                                          WsRhFct.DecryptageVirtualia(Password))
                WseSession = Nothing
                Return SiOk
            End Get
        End Property

        Public ReadOnly Property NouveauMotdePasse(ByVal Identite As String, ByVal Email As String) As String
            Get
                Dim WseSession As Virtualia.Net.WebService.ServiceWcfSessions = New Virtualia.Net.WebService.ServiceWcfSessions
                Dim NewPw As String = WseSession.Nouveau_Password(WsNoBd, WsModeConnexion, Identite, Email)
                WseSession = Nothing
                Return NewPw
            End Get
        End Property

        Public ReadOnly Property Identifiant(ByVal Ide As Integer) As Integer
            Get

                If WsArmoireComplete Is Nothing Then
                    FaireArmoireComplete()
                End If
                Dim PerDossier As ObjetDossierPER = WsArmoireComplete.Find(Function(Recherche) Recherche.V_Identifiant = Ide)
                If PerDossier IsNot Nothing Then
                    Return PerDossier.V_Identifiant
                End If
                Return 0
            End Get
        End Property

        Public ReadOnly Property Identifiant(ByVal Nom As String, ByVal Prenom As String) As Integer
            Get
                If WsArmoireComplete Is Nothing Then
                    FaireArmoireComplete()
                End If

                Dim ListePER As List(Of ObjetDossierPER) = (From instance In WsArmoireComplete Select instance Where instance.Nom = Nom And instance.Prenom = Prenom).ToList
                If ListePER Is Nothing Then
                    Return 0
                End If
                Return ListePER.Item(0).V_Identifiant
            End Get
        End Property

        Public ReadOnly Property NouvelIdentifiant(ByVal NomTableSgbd As String) As Integer
            Get
                Return VirServiceServeur.ObtenirUnCompteur(VirNomUtilisateur, NomTableSgbd, "Max") + 1
            End Get
        End Property

        Public ReadOnly Property SelectionDistincte(ByVal Pvue As Integer, ByVal NoObjet As Integer, ByVal NomTable As String, ByVal Champ As String) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Get
                Dim OrdreSql As String = "SELECT DISTINCT " & Champ & " FROM " & NomTable & " ORDER BY " & Champ
                Return VirServiceServeur.RequeteSql_ToListeType(VirNomUtilisateur, Pvue, NoObjet, OrdreSql)
            End Get
        End Property

        Public ReadOnly Property RequeteSql_ListeType(ByVal Pvue As Integer, ByVal NoObjet As Integer, ByVal OrdreSql As String) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Get
                Return VirServiceServeur.RequeteSql_ToListeType(VirNomUtilisateur, Pvue, NoObjet, OrdreSql)
            End Get
        End Property

        Public ReadOnly Property RequeteSql_ListeFiches(ByVal Pvue As Integer, ByVal NoObjet As Integer, ByVal OrdreSql As String) As List(Of VIR_FICHE)
            Get
                Return VirServiceServeur.RequeteSql_ToFiches(VirNomUtilisateur, Pvue, NoObjet, OrdreSql)
            End Get
        End Property

        Public ReadOnly Property Lecture_UnObjet(ByVal Pvue As Integer, ByVal NoObjet As Integer, ByVal Ide As Integer) As List(Of VIR_FICHE)
            Get
                Return VirServiceServeur.LectureObjet_ToFiches(VirNomUtilisateur, Pvue, NoObjet, Ide, True)
            End Get
        End Property

        Public ReadOnly Property SelectionDynamique(ByVal Pvue As Integer, ByVal NoObjet As Integer, ByVal NoInfo As Integer, _
                                                    ByVal OpeLiaison As Integer, ByVal OpeComparaison As Integer, _
                                                    ByVal ConditionValeurs As String, ByVal DateDebut As String, ByVal DateFin As String) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Get
                Dim Constructeur As SqlInterne = New SqlInterne(VirModele, VirInstanceBd)
                Dim ChaineSql As String

                Constructeur.NombredeRequetes(Pvue, DateDebut, DateFin, VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.NoInfoSelection(0, NoObjet) = NoInfo

                If ConditionValeurs <> "" Then
                    Constructeur.ValeuraComparer(0, OpeLiaison, OpeComparaison, False) = ConditionValeurs
                End If
                Constructeur.InfoExtraite(0, NoObjet, 0) = NoInfo
                ChaineSql = Constructeur.OrdreSqlDynamique
                Constructeur = Nothing

                Return RequeteSql_ListeType(Pvue, NoObjet, ChaineSql)
            End Get
        End Property

        Public ReadOnly Property VirNomUtilisateur As String
            Get
                Return WsNomUtilisateurBd
            End Get
        End Property

        Public ReadOnly Property VirUrlWebSession As String
            Get
                Return WsUrlWebServiceSession
            End Get
        End Property

        Public ReadOnly Property VirModeConnexion As String
            Get
                Return WsModeConnexion
            End Get
        End Property

        Public ReadOnly Property VirModele As ModeleRH
            Get
                Return WsRhModele
            End Get
        End Property

        Public ReadOnly Property VirExpertes() As ExpertesRH
            Get
                Return WsRhExpertes
            End Get
        End Property

        Public ReadOnly Property VirRepertoire As String
            Get
                Return WsRepertoireVirtualia
            End Get
        End Property

        Public ReadOnly Property VirNoBd As Integer
            Get
                Return WsNoBd
            End Get
        End Property

        Public ReadOnly Property VirSgbd As ParamSgbd
            Get
                Return WsInstanceSgbd
            End Get
        End Property

        Public ReadOnly Property VirInstanceBd As ParamSgbdDatabase
            Get
                Return (From it As ParamSgbdDatabase In VirSgbd Where it.Numero = WsNoBd Select it).FirstOrDefault()
            End Get
        End Property

        Public ReadOnly Property VirRhDates As CalculDates
            Get
                Return WsRhDates
            End Get
        End Property

        Public ReadOnly Property VirRhFonction As Generales
            Get
                Return WsRhFct
            End Get
        End Property

        Public ReadOnly Property TabledesCategories() As String()
            Get
                Dim Tabliste(12) As String
                Dim I As Integer
                For I = 0 To Tabliste.Count - 1
                    Tabliste(I) = "9999" & VI.Tild & LibelleCategorie(I) & VI.Tild & I
                Next I
                Return Tabliste
            End Get
        End Property

        Public ReadOnly Property CategorieNum(ByVal Valeur As String) As Integer
            Get
                Dim I As Integer
                For I = 0 To 12
                    If LibelleCategorie(I) = Valeur Then
                        Return CInt(I)
                    End If
                Next I
                Return 0
            End Get
        End Property

        Public ReadOnly Property LibelleCategorie(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case VI.CategorieRH.InfosPersonnelles
                        Return "Informations personnelles"
                    Case VI.CategorieRH.Diplomes_Qualification
                        Return "Diplômes et qualificatons"
                    Case VI.CategorieRH.SituationAdministrative
                        Return "Situation administrative"
                    Case VI.CategorieRH.AffectationsFonctionnelles
                        Return "Affectations fonctionnelles"
                    Case VI.CategorieRH.AffectationsBudgetaires
                        Return "Affectations budgétaires"
                    Case VI.CategorieRH.TempsdeTravail
                        Return "Temps de travail, congés et absences"
                    Case VI.CategorieRH.Formation
                        Return "Formation continue"
                    Case VI.CategorieRH.Evaluation_Gpec
                        Return "Entretiens et évaluations "
                    Case VI.CategorieRH.Remuneration
                        Return "Rémunération"
                    Case VI.CategorieRH.Frais_Deplacement
                        Return "Frais de déplacement"
                    Case VI.CategorieRH.Social
                        Return "Informations sociales"
                    Case VI.CategorieRH.Retraite
                        Return "Retraite"
                    Case Else
                        Return "(Non défini)"
                End Select
            End Get
        End Property

        Public ReadOnly Property ControleDoublon(ByVal PointdeVue As Integer, ByVal Ide As Integer, ByVal NumInfo As Integer, _
                                                 ByVal Valeur_N1 As String, Optional ByVal Valeur_N2 As String = "", _
                                                 Optional ByVal Valeur_N3 As String = "") As List(Of Integer)
            Get

                Dim Constructeur As SqlInterne = New SqlInterne(WsRhModele, VirInstanceBd)
                Constructeur.NombredeRequetes(PointdeVue, "", "", VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True

                Constructeur.NoInfoSelection(0, 1) = NumInfo
                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Valeur_N1
                Constructeur.InfoExtraite(0, 1, 0) = NumInfo
                If Valeur_N2 <> "" Then
                    Constructeur.NoInfoSelection(1, 1) = CInt(NumInfo + 1)
                    Constructeur.ValeuraComparer(1, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Valeur_N2
                    Constructeur.InfoExtraite(1, 1, 0) = CInt(NumInfo + 1)
                End If
                If Valeur_N3 <> "" Then
                    Constructeur.NoInfoSelection(2, 1) = CInt(NumInfo + 2)
                    Constructeur.ValeuraComparer(2, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Valeur_N3
                    Constructeur.InfoExtraite(2, 1, 0) = CInt(NumInfo + 2)
                End If

                Dim LstResultat As List(Of Virtualia.Net.ServiceServeur.VirRequeteType) = VirServiceServeur.RequeteSql_ToListeType(WsNomUtilisateurBd, PointdeVue, 1, Constructeur.OrdreSqlDynamique)
                Constructeur = Nothing

                If LstResultat Is Nothing Then
                    Return Nothing
                End If
                Dim TabRes As New List(Of Integer)
                If LstResultat.Item(0).Ide_Dossier <> Ide Then
                    TabRes.Add(LstResultat.Item(0).Ide_Dossier)
                End If
                If TabRes.Count > 0 Then
                    Return TabRes
                Else
                    Return Nothing
                End If
            End Get
        End Property

        Public ReadOnly Property VirObjet(ByVal NumPointVue As Integer, ByVal NumObjet As Integer) As Objet
            Get

                Dim pv As PointdeVue = (From it As PointdeVue In WsRhModele Where it.Numero = NumPointVue Select it).FirstOrDefault()
                If (pv Is Nothing) Then
                    Return Nothing
                End If

                Return (From it As Objet In pv Where it.Numero = NumObjet Select it).FirstOrDefault()
            End Get
        End Property

        Public ReadOnly Property VirListeObjetsDico() As List(Of FicheObjetDictionnaire)
            Get

                If Not (WsListeObjetsDico Is Nothing) Then
                    Return WsListeObjetsDico
                End If

                WsListeObjetsDico = New List(Of FicheObjetDictionnaire)
                Dim FicheDico As FicheObjetDictionnaire

                For Each pv As PointdeVue In VirModele
                    For Each obj As Objet In pv
                        FicheDico = New FicheObjetDictionnaire(obj)
                        WsListeObjetsDico.Add(FicheDico)
                        FicheDico.VIndex = WsListeObjetsDico.Count
                    Next
                Next

                Return WsListeObjetsDico
            End Get
        End Property

        Public ReadOnly Property VirListeInfosDico() As List(Of FicheInfoDictionnaire)
            Get
                If Not (WsListeInfosDico Is Nothing) Then
                    Return WsListeInfosDico
                End If


                WsListeInfosDico = New List(Of FicheInfoDictionnaire)
                Dim FicheDico As FicheInfoDictionnaire

                For Each pv As PointdeVue In VirModele
                    For Each obj As Objet In pv
                        For Each info As Virtualia.Systeme.MetaModele.Donnees.Information In obj
                            FicheDico = New FicheInfoDictionnaire(info)
                            WsListeInfosDico.Add(FicheDico)
                            FicheDico.VIndex = WsListeInfosDico.Count
                        Next
                    Next
                Next

                Return WsListeInfosDico
            End Get
        End Property

        Public ReadOnly Property VirListeExpertesDico() As List(Of FicheInfoExperte)
            Get
                If Not (WsListeExpertesDico Is Nothing) Then
                    Return WsListeExpertesDico
                End If

                WsListeExpertesDico = New List(Of FicheInfoExperte)
                Dim FicheDico As FicheInfoExperte

                For Each pv As PointdevueExpert In VirExpertes
                    For Each obj As ObjetExpert In pv
                        For Each info As InformationExperte In obj
                            FicheDico = New FicheInfoExperte(info)
                            WsListeExpertesDico.Add(FicheDico)
                            FicheDico.VIndex = WsListeExpertesDico.Count
                        Next
                    Next
                Next

                Return WsListeExpertesDico
            End Get
        End Property

        Public Function LireTableSysReference(ByVal NoPvue As Integer) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim Constructeur As SqlInterne = New SqlInterne(VirModele, VirInstanceBd)

            Constructeur.NombredeRequetes(NoPvue, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = True
            Constructeur.NoInfoSelection(0, 1) = 1
            Constructeur.InfoExtraite(0, 1, 1) = 1

            Dim ChaineSql As String = Constructeur.OrdreSqlDynamique
            Constructeur = Nothing
            Return VirServiceServeur.RequeteSql_ToListeType(VirNomUtilisateur, NoPvue, 1, ChaineSql)
        End Function

        Public Function LireTableGeneraleSimple(ByVal Intitule As String) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim LstResultat As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim Constructeur As SqlInterne = New SqlInterne(VirModele, VirInstanceBd)
            Dim IndiceI As Integer
            Dim cretour As Boolean

            Constructeur.NombredeRequetes(VI.PointdeVue.PVueGeneral, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = True
            Constructeur.NoInfoSelection(0, 1) = 1
            Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Intitule
            Constructeur.InfoExtraite(0, 2, 1) = 1
            Constructeur.InfoExtraite(1, 2, 0) = 2

            LstResultat = VirServiceServeur.RequeteSql_ToListeType(VirNomUtilisateur, VI.PointdeVue.PVueGeneral, 2, Constructeur.OrdreSqlDynamique)
            If LstResultat IsNot Nothing Then
                Constructeur = Nothing
                Return LstResultat
            End If

            '*** Si Aucune valeur et la table générale n'existe pas alors Création de celle-ci. ******
            '*** Si Aucune valeur et que la table générale existe alors initialisation avec (Aucun) ****
            Constructeur = Nothing
            Constructeur = New SqlInterne(VirModele, VirInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueGeneral, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = True
            Constructeur.NoInfoSelection(0, 1) = 1
            Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Intitule
            Constructeur.InfoExtraite(0, 1, 0) = 1

            LstResultat = VirServiceServeur.RequeteSql_ToListeType(VirNomUtilisateur, VI.PointdeVue.PVueGeneral, 1, Constructeur.OrdreSqlDynamique)

            Dim FicheRef As TAB_DESCRIPTION = New TAB_DESCRIPTION

            If LstResultat Is Nothing Then
                IndiceI = VirServiceServeur.ObtenirUnCompteur(VirNomUtilisateur, "TAB_DESCRIPTION", "Max") + 1
                FicheRef.Ide_Dossier = IndiceI
                FicheRef.Intitule = Intitule
                FicheRef.SiModifiable = "Oui"
                cretour = VirServiceServeur.MiseAjour_Fiche(VirNomUtilisateur, VI.PointdeVue.PVueGeneral, 1, IndiceI, "C", "", FicheRef.ContenuTable)
            Else
                FicheRef.ContenuTable = LstResultat.Item(0).Ide_Dossier & VI.Tild & Intitule & VI.Tild
            End If

            Dim FicheVal As New TAB_LISTE
            FicheVal.Ide_Dossier = FicheRef.Ide_Dossier
            FicheVal.Valeur = "(Aucun)"
            FicheVal.Reference = ""
            cretour = VirServiceServeur.MiseAjour_Fiche(VirNomUtilisateur, VI.PointdeVue.PVueGeneral, 2, FicheVal.Ide_Dossier, "C", "", FicheVal.ContenuTable)

            Constructeur = Nothing

            LstResultat = New List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim Aucun As New Virtualia.Net.ServiceServeur.VirRequeteType
            Aucun.Ide_Dossier = FicheVal.Ide_Dossier
            Aucun.Valeurs = New List(Of String)
            Aucun.Valeurs.Add(FicheVal.Valeur)
            LstResultat.Add(Aucun)
            Return LstResultat
        End Function

        Public Sub Deconnexion(ByVal NoSession As String)
            Dim WseSession As Virtualia.Net.WebService.ServiceWcfSessions = New Virtualia.Net.WebService.ServiceWcfSessions
            Dim SiOk As Boolean = WseSession.FinSession(NoSession)
            WseSession = Nothing
        End Sub

        Public Sub EcrireLogTraitement(ByVal Nature As String, ByVal SiDatee As Boolean, ByVal Msg As String)

            Dim SysCodeIso As Encoding = Encoding.GetEncoding(1252)
            Dim NomLog As String = Virtualia.Systeme.Constantes.DossierVirtualiaService("Logs") & Nature & ".log"
            Dim FicStream As FileStream = New FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
            Dim FicWriter As StreamWriter = New StreamWriter(FicStream, SysCodeIso)
            Dim dateValue As Date = System.DateTime.Now

            If (Msg = "") Then
                FicWriter.WriteLine(Strings.StrDup(20, "-") & Strings.Space(1) & dateValue.ToString("dd/MM/yyyy hh:mm:ss.fff tt") & Space(1) & Strings.StrDup(20, "-"))
                FicWriter.Flush()
                FicWriter.Close()
                Return
            End If

            If SiDatee Then
                FicWriter.WriteLine(dateValue.ToString("dd/MM/yyyy hh:mm:ss.fff tt") & Space(1) & Msg)
            Else
                FicWriter.WriteLine(Space(5) & Msg)
            End If

            FicWriter.Flush()
            FicWriter.Close()
        End Sub

        Public Sub Reinitialiser()
            WsRegistreVirtualia = Nothing
            WsObjetCalcJour = Nothing
            Try
                GC.Collect()
                GC.WaitForPendingFinalizers()
                GC.Collect()
            Catch ex As Exception
                Exit Try
            End Try

            ChangerBaseCourante()

            Try
                WsRegistreVirtualia = New ObjetConfiguration(WsNomUtilisateurBd)
                If WsRegistreVirtualia.ListeDesSections.Count = 0 Then
                    WsRegistreVirtualia = Nothing
                End If
            Catch Ex As Exception
                WsRegistreVirtualia = Nothing
            End Try
        End Sub

        Private Sub FaireArmoireComplete()
            Dim LstResultat As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Dim TableauData(0) As String

            'Requete pour être sûr d'avoir tout le monde
            Dim Constructeur As SqlInterne = New SqlInterne(VirModele, VirInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", "", VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.SiHistoriquedeSituation = False
            Constructeur.NoInfoSelection(0, 1) = 2
            Constructeur.InfoExtraite(0, 1, 0) = 2
            Constructeur.InfoExtraite(1, 1, 0) = 3
            Constructeur.InfoExtraite(2, 1, 0) = 4

            LstResultat = VirServiceServeur.RequeteSql_ToListeType(WsNomUtilisateurBd, VI.PointdeVue.PVueApplicatif, 1, Constructeur.OrdreSqlDynamique)
            Constructeur = Nothing

            WsArmoireComplete = New List(Of ObjetDossierPER)
            Dim PerDossier As ObjetDossierPER

            If LstResultat Is Nothing Then
                Exit Sub
            End If
            For Each sobj As Virtualia.Net.ServiceServeur.VirRequeteType In LstResultat
                PerDossier = New ObjetDossierPER(VirModele, sobj.Ide_Dossier)
                PerDossier.Nom = sobj.Valeurs(0)
                PerDossier.Prenom = sobj.Valeurs(1)
                PerDossier.Date_de_Naissance = sobj.Valeurs(2)

                WsArmoireComplete.Add(PerDossier)
            Next
        End Sub

        Private Sub ObtenirModele()
            WsRhModele = VirServiceServeur.Instance_ModeleRH
            WsRhExpertes = VirServiceServeur.Instance_ExperteRH
        End Sub

        Private Sub ObtenirSgbd()
            WsInstanceSgbd = VirServiceServeur.Instance_Database
        End Sub

        Private Sub ChangerBaseCourante()
            Dim Cretour As Boolean = VirServiceServeur.ChangerBasedeDonneesCourante(WsNomUtilisateurBd, WsNoBd)
        End Sub

        Public Sub New(ByVal UrlWebSession As String, ByVal RepertoireVirtualia As String, ByVal NomUti As String, ByVal NoBd As Integer)
            WsUrlWebServiceSession = UrlWebSession
            WsRepertoireVirtualia = RepertoireVirtualia
            WsNomUtilisateurBd = NomUti
            WsNoBd = NoBd
            WsRhDates = New CalculDates
            WsRhFct = New Generales
            ObtenirSgbd()
            ObtenirModele()
            Call ChangerBaseCourante()
            Try
                WsRegistreVirtualia = New ObjetConfiguration(WsNomUtilisateurBd)
                If WsRegistreVirtualia.ListeDesSections.Count = 0 Then
                    WsRegistreVirtualia = Nothing
                End If
            Catch Ex As Exception
                WsRegistreVirtualia = Nothing
            End Try

        End Sub

    End Class
End Namespace

