﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.Fonctions

Namespace Session
    Public Class Utilisateur
        Private WsTypeUti As String '** I = Interne Virtualia, E a= Externe Via Version 3 - Intranet - Nouveaux Modules
        Private WsNomConnexion As String = Nothing
        Private WsPrenomConnexion As String = Nothing
        Private WsIdeVirtualia As Integer
        Private WsIDSession As String 'Attribué par Virtualia au moment de la connexion = 1ere SessionID IIS
        Private WsSgbdUtilisateur As Integer = 0
        Private WsWebServeurUtilisateur As String = Nothing
        Private WsID_Machine As String
        Private WsID_AdresseIP As String
        Private WsID_LogonIdentity As String
        '
        Private WsRhFonction As Generales
        Private WsRhDates As CalculDates
        '
        Public Property V_IDSession As String
            Get
                Return WsIDSession
            End Get
            Set(ByVal value As String)
                WsIDSession = value
            End Set
        End Property

        Public ReadOnly Property V_NomdeConnexion() As String
            Get
                Return WsNomConnexion
            End Get
        End Property

        Public Property V_PrenomdeConnexion As String
            Get
                Return WsPrenomConnexion
            End Get
            Set(ByVal value As String)
                WsPrenomConnexion = value
            End Set
        End Property

        Public Property V_Identifiant As Integer
            Get
                Return WsIdeVirtualia
            End Get
            Set(ByVal value As Integer)
                WsIdeVirtualia = value
            End Set
        End Property

        Public ReadOnly Property V_NomdUtilisateurSgbd() As String
            Get
                Return WsWebServeurUtilisateur
            End Get
        End Property

        Public ReadOnly Property V_NumeroSgbdActif() As Integer
            Get
                Return WsSgbdUtilisateur
            End Get
        End Property

        Public Property ID_Machine As String
            Get
                Return WsID_Machine
            End Get
            Set(ByVal value As String)
                WsID_Machine = value
            End Set
        End Property

        Public Property ID_AdresseIP As String
            Get
                Return WsID_AdresseIP
            End Get
            Set(ByVal value As String)
                WsID_AdresseIP = value
            End Set
        End Property

        Public Property ID_LogonIdentite As String
            Get
                Return WsID_LogonIdentity
            End Get
            Set(ByVal value As String)
                WsID_LogonIdentity = value
            End Set
        End Property

        Public ReadOnly Property V_RhDates() As CalculDates
            Get
                Return WsRhDates
            End Get
        End Property

        Public ReadOnly Property V_RhFonction() As Generales
            Get
                Return WsRhFonction
            End Get
        End Property

        Public Function V_ChangerBaseCourante(ByVal NoBd As Integer) As Boolean
            Dim CodeRetour As Boolean = VirServiceServeur.ChangerBasedeDonneesCourante(WsWebServeurUtilisateur, NoBd)
            If CodeRetour Then
                WsSgbdUtilisateur = NoBd
            End If
            Return CodeRetour
        End Function

        Private Sub LireSessionUti()
            Dim Uti As Virtualia.Net.ServiceServeur.UtilisateurType
            Uti = VirServiceServeur.LireProfilUtilisateurVirtualia(V_NomdeConnexion, V_NomdeConnexion, V_NumeroSgbdActif)

            WsIdeVirtualia = Uti.Identifiant
            WsPrenomConnexion = Uti.Prenom & Strings.Space(1) & Uti.Nom
            WsSgbdUtilisateur = Uti.InstanceBd
        End Sub

        'Connexion via Virtualia Gestion Version 4 - Sgbd = Par défaut
        Public Sub New(ByVal NomUtilisateur As String)
            WsTypeUti = "I"
            WsNomConnexion = NomUtilisateur
            WsWebServeurUtilisateur = NomUtilisateur
            WsRhFonction = New Generales
            WsRhDates = New CalculDates
            LireSessionUti()
        End Sub

        'Connexion via Virtualia Version 4 - Sgbd = WebConfig
        Public Sub New(ByVal NomUtilisateur As String, ByVal NoBd As Integer)
            WsTypeUti = "I"
            WsNomConnexion = NomUtilisateur
            WsWebServeurUtilisateur = NomUtilisateur
            WsSgbdUtilisateur = NoBd
            WsRhFonction = New Generales
            WsRhDates = New CalculDates
        End Sub

        'Connexion Intranet
        Public Sub New(ByVal NomConnexion As String, ByVal NoBd As Integer, ByVal NomUtiWse As String)
            WsTypeUti = "E"
            WsNomConnexion = NomConnexion
            WsSgbdUtilisateur = NoBd
            WsRhFonction = New Generales
            WsRhDates = New CalculDates
            WsWebServeurUtilisateur = NomUtiWse
        End Sub

    End Class
End Namespace