﻿Option Strict On
Option Explicit On
Option Compare Text

Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.TablesObjet.ShemaREF
Imports Virtualia.Systeme.MetaModele

Namespace Datas
    Public Class ObjetGradeGrille
        Implements IGestionVirFicheModif(Of GRD_GRADE)

        Private WsListeGrades As List(Of GRD_GRADE) = New List(Of GRD_GRADE)()
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesGradesTries(ByVal CritereSel As String, ByVal Valeur As String, Optional ByVal CritereTri As String = "") As List(Of GRD_GRADE)
            Get
                If WsListeGrades Is Nothing OrElse WsListeGrades.Count = 0 Then
                    Return Nothing
                End If
                Dim LstRes As List(Of GRD_GRADE) = Nothing

                If CritereSel = "" Then
                    Select Case CritereTri
                        Case Is = "" 'Standard
                            LstRes = (From grade In WsListeGrades Select grade
                                      Order By grade.Gestion Ascending, grade.Categorie Ascending, grade.Filiere Ascending, grade.Corps Ascending, grade.Intitule Ascending).ToList
                            Return LstRes
                        Case Is = "Catégorie"
                            LstRes = (From grade In WsListeGrades Select grade _
                                                Order By grade.Categorie Ascending, grade.Intitule Ascending).ToList
                            Return LstRes
                        Case Is = "Corps"
                            LstRes = (From grade In WsListeGrades Select grade _
                                               Order By grade.Corps Ascending, grade.Intitule Ascending).ToList
                            Return LstRes
                        Case Is = "Intitulé"
                            Return WsListeGrades.OrderBy(Function(m) m.Intitule).ToList
                    End Select
                End If
                Select Case CritereSel
                    Case Is = "Catégorie"
                        Select Case CritereTri
                            Case Is = "" 'Standard
                                LstRes = (From grade In WsListeGrades Select grade Where grade.Categorie = Valeur
                                          Order By grade.Gestion Ascending, grade.Categorie Ascending, grade.Filiere Ascending, grade.Corps Ascending,
                                                grade.Intitule Ascending).ToList
                            Case Else
                                Select Case CritereTri
                                    Case "Catégorie"
                                        LstRes = (From grade In WsListeGrades Select grade Where grade.Categorie = Valeur
                                                  Order By grade.Categorie Ascending, grade.Intitule Ascending).ToList
                                    Case "Corps"
                                        LstRes = (From grade In WsListeGrades Select grade Where grade.Categorie = Valeur
                                                  Order By grade.Corps Ascending, grade.Intitule Ascending).ToList
                                    Case "Intitulé"
                                        LstRes = (From grade In WsListeGrades Select grade Where grade.Categorie = Valeur
                                                  Order By grade.Intitule Ascending).ToList
                                End Select
                        End Select
                    Case Is = "Corps"
                        Select Case CritereTri
                            Case Is = "" 'Standard
                                LstRes = (From grade In WsListeGrades Select grade Where grade.Corps = Valeur
                                          Order By grade.Gestion Ascending, grade.Categorie Ascending, grade.Filiere Ascending, grade.Corps Ascending,
                                                grade.Intitule Ascending).ToList
                            Case Else
                                Select Case CritereTri
                                    Case "Catégorie"
                                        LstRes = (From grade In WsListeGrades Select grade Where grade.Corps = Valeur
                                                  Order By grade.Categorie Ascending, grade.Intitule Ascending).ToList
                                    Case "Corps"
                                        LstRes = (From grade In WsListeGrades Select grade Where grade.Corps = Valeur
                                                  Order By grade.Corps Ascending, grade.Intitule Ascending).ToList
                                    Case "Intitulé"
                                        LstRes = (From grade In WsListeGrades Select grade Where grade.Corps = Valeur
                                                  Order By grade.Intitule Ascending).ToList
                                End Select
                        End Select
                    Case Is = "Gestion" 'Standard
                        Select Case CritereTri
                            Case Is = "" 'Standard
                                LstRes = (From grade In WsListeGrades Select grade Where grade.Gestion = Valeur
                                          Order By grade.Categorie Ascending, grade.Filiere Ascending, grade.Corps Ascending, grade.Intitule Ascending).ToList
                            Case Else
                                Select Case CritereTri
                                    Case "Catégorie"
                                        LstRes = (From grade In WsListeGrades Select grade Where grade.Gestion = Valeur
                                                  Order By grade.Categorie Ascending, grade.Intitule Ascending).ToList
                                    Case "Corps"
                                        LstRes = (From grade In WsListeGrades Select grade Where grade.Gestion = Valeur
                                                  Order By grade.Corps Ascending, grade.Intitule Ascending).ToList
                                    Case "Intitulé"
                                        LstRes = (From grade In WsListeGrades Select grade Where grade.Gestion = Valeur
                                                  Order By grade.Intitule Ascending).ToList
                                End Select
                        End Select
                    Case Is = "Intitulé"
                        Select Case CritereTri
                            Case Is = "" 'Standard
                                LstRes = (From grade In WsListeGrades Select grade Where grade.Intitule = Valeur
                                          Order By grade.Gestion Ascending, grade.Categorie Ascending, grade.Filiere Ascending, grade.Corps Ascending,
                                                grade.Intitule Ascending).ToList
                            Case Else
                                Select Case CritereTri
                                    Case "Catégorie"
                                        LstRes = (From grade In WsListeGrades Select grade Where grade.Intitule = Valeur
                                                  Order By grade.Categorie Ascending, grade.Intitule Ascending).ToList
                                    Case "Corps"
                                        LstRes = (From grade In WsListeGrades Select grade Where grade.Categorie = Valeur
                                                  Order By grade.Corps Ascending, grade.Intitule Ascending).ToList
                                    Case "Intitulé"
                                        LstRes = (From grade In WsListeGrades Select grade Where grade.Intitule = Valeur
                                                  Order By grade.Intitule Ascending).ToList
                                End Select
                        End Select
                End Select
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property ListeDesGrades(ByVal Lettre As String, Optional ByVal CritereSel As String = "") As List(Of GRD_GRADE)
            Get
                If WsListeGrades Is Nothing OrElse WsListeGrades.Count = 0 Then
                    Return Nothing
                End If
                Dim Predicat As New Virtualia.Ressources.Predicats.PredicateUniversel(Lettre, VI.PointdeVue.PVueGrades)
                Dim LstRes As New List(Of GRD_GRADE)
                Dim LstTri As List(Of GRD_GRADE) = Nothing

                LstRes = WsListeGrades.FindAll(AddressOf Predicat.RechercherLettre)
                If LstRes Is Nothing Then
                    Return Nothing
                End If
                Select Case CritereSel
                    Case Is = ""
                        LstTri = (From grade In LstRes Select grade Where grade.Ide_Dossier > 0 Order By grade.Intitule Ascending).ToList
                    Case Else
                        LstTri = (From grade In LstRes Select grade Where grade.Gestion = CritereSel Order By grade.Intitule Ascending).ToList
                End Select
                Return LstTri
            End Get
        End Property

        Public ReadOnly Property ListeDesGradesNNE() As List(Of GRD_GRADE)
            Get
                If WsListeGrades Is Nothing OrElse WsListeGrades.Count = 0 Then
                    Return Nothing
                End If
                Dim LstRes As New List(Of GRD_GRADE)

                LstRes = (From grade In WsListeGrades Select grade Where grade.CodeNNE <> "" Order By grade.Intitule Ascending).ToList

                Return LstRes
            End Get
        End Property

        Public ReadOnly Property ListeDesGradesADAGE() As List(Of GRD_GRADE)
            Get
                If WsListeGrades Is Nothing OrElse WsListeGrades.Count = 0 Then
                    Return Nothing
                End If
                Dim LstRes As New List(Of GRD_GRADE)

                LstRes = (From grade In WsListeGrades Select grade Where grade.CodeADAGE <> "" Order By grade.Intitule Ascending).ToList

                Return LstRes
            End Get
        End Property

        Public ReadOnly Property NombredeGrades() As Integer Implements IGestionVirFicheModif(Of GRD_GRADE).NombredItems
            Get
                If WsListeGrades Is Nothing OrElse WsListeGrades.Count = 0 Then
                    Return 0
                End If
                Return WsListeGrades.Count
            End Get
        End Property

        Public Function Fiche_Grade(ByVal Ide_Dossier As Integer) As GRD_GRADE Implements IGestionVirFicheModif(Of GRD_GRADE).GetFiche
            If WsListeGrades Is Nothing OrElse WsListeGrades.Count = 0 Then
                Return Nothing
            End If
            Return WsListeGrades.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Grade(ByVal Intitule As String) As GRD_GRADE Implements IGestionVirFicheModif(Of GRD_GRADE).GetFiche
            If WsListeGrades Is Nothing OrElse WsListeGrades.Count = 0 Then
                Return Nothing
            End If
            Return WsListeGrades.Find(Function(m) m.Intitule = Intitule)
        End Function

        Public Function Fiche_Grade(ByVal Nature As String, ByVal Code As String) As GRD_GRADE
            Select Case Nature
                Case Is = "ADAGE"
                    Return WsListeGrades.Find(Function(m) m.CodeADAGE = Code)
                Case Is = "NNE"
                    Return WsListeGrades.Find(Function(m) m.CodeNNE = Code)
                Case Else
                    Return WsListeGrades.Find(Function(m) m.Nomenclature = Code)
            End Select
        End Function

        Public Function Fiche_GradeRemu(ByVal EchelleRemu As String) As GRD_GRADE
            If WsListeGrades Is Nothing OrElse WsListeGrades.Count = 0 Then
                Return Nothing
            End If
            Return WsListeGrades.Find(Function(m) m.Echelle_de_remuneration = EchelleRemu)
        End Function

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of GRD_GRADE).SupprimerItem
            Dim FicheGrade As GRD_GRADE
            FicheGrade = Fiche_Grade(Ide)
            If FicheGrade IsNot Nothing Then
                WsListeGrades.Remove(FicheGrade)
            End If
        End Sub

        Public Sub AjouterItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of GRD_GRADE).AjouterItem
            SupprimerItem(Ide)
            ConstituerListe(Ide)
        End Sub

        Public Sub Actualiser() Implements IGestionVirFicheModif(Of GRD_GRADE).Actualiser
            WsListeGrades.Clear()
            ConstituerListe(0)
        End Sub

        Private Sub ConstituerListe(ByVal Ide As Integer)
            WsListeGrades.Clear()
            Dim FicheGrade As GRD_GRADE

            '** 1  Liste de tous les établissements
            WsListeGrades.AddRange(CreationCollectionVIRFICHE(Of GRD_GRADE).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide))
            If WsListeGrades.Count <= 0 Then
                Return
            End If

            '** 2  Liste de toutes les règles d'avancement
            Dim LstAvanc As List(Of GRD_AVANCEMENT_GRADE) = CreationCollectionVIRFICHE(Of GRD_AVANCEMENT_GRADE).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            LstAvanc.ForEach(Sub(f)
                                 FicheGrade = Fiche_Grade(f.Ide_Dossier)
                                 If FicheGrade Is Nothing Then
                                     Return
                                 End If
                                 FicheGrade.Ajouter_RegleAvancement(f)
                             End Sub)

            '** 3  Liste de toutes les grilles
            Dim LstGrilles As List(Of GRD_GRILLE) = CreationCollectionVIRFICHE(Of GRD_GRILLE).GetCollection(WsNomUtilisateur, VirServiceServeur, Ide)
            LstGrilles.ForEach(Sub(f)
                                   If Ide = 0 Then
                                       FicheGrade = Fiche_GradeRemu(f.Intitule)
                                       If FicheGrade Is Nothing Then
                                           FicheGrade = Fiche_Grade(f.Intitule)
                                       End If
                                       If FicheGrade IsNot Nothing Then
                                           FicheGrade.Ajouter_Grille(f)
                                       End If
                                       Return
                                   End If
                                   FicheGrade = Fiche_Grade(Ide)
                                   If FicheGrade IsNot Nothing Then
                                       If FicheGrade.Echelle_de_remuneration = f.Intitule Or FicheGrade.Intitule = f.Intitule Then
                                           FicheGrade.Ajouter_Grille(f)
                                       End If
                                   End If
                               End Sub)

        End Sub

        Public Sub New(ByVal NomUtilisateur As String, ByVal Ide As Integer)
            WsNomUtilisateur = NomUtilisateur
            WsListeGrades = New List(Of GRD_GRADE)
            ConstituerListe(Ide)
        End Sub

    End Class
End Namespace



