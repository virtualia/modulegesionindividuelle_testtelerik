﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace Datas
    Public Class ObjetUniteCycle
        Implements IGestionVirFicheModif(Of CYC_IDENTIFICATION)

        Private WsListeUnites As List(Of CYC_IDENTIFICATION)
        Private WsNomUtilisateur As String

        Public ReadOnly Property ListeDesUnites(ByVal Etablissement As String) As List(Of CYC_IDENTIFICATION)
            Get
                If WsListeUnites Is Nothing Then
                    Return Nothing
                End If
                Dim LstRes As List(Of CYC_IDENTIFICATION)
                Select Case Etablissement
                    Case Is = ""
                        LstRes = (From instance In WsListeUnites Select instance _
                                        Where instance.Ide_Dossier > 0 _
                                        Order By instance.Etablissement, instance.Intitule Ascending).ToList
                    Case Else
                        LstRes = (From instance In WsListeUnites Select instance _
                                        Where instance.Etablissement = Etablissement _
                                        Order By instance.Intitule Ascending).ToList
                End Select
                Return LstRes
            End Get
        End Property

        Public ReadOnly Property ListeDesUnites(ByVal Lettre As String, ByVal Etablissement As String) As List(Of CYC_IDENTIFICATION)
            Get
                If WsListeUnites Is Nothing Then
                    Return Nothing
                End If
                Dim Predicat As New Virtualia.Ressources.Predicats.PredicateUniversel(Lettre, VI.PointdeVue.PVueBaseHebdo)
                Dim LstRes As List(Of CYC_IDENTIFICATION)
                Dim LstTri As List(Of CYC_IDENTIFICATION) = Nothing

                LstRes = WsListeUnites.FindAll(AddressOf Predicat.RechercherLettre)
                If LstRes Is Nothing Then
                    Return Nothing
                End If
                Select Case Etablissement
                    Case Is = ""
                        LstTri = (From instance In LstRes Select instance _
                               Where instance.Ide_Dossier > 0 _
                               Order By instance.Intitule Ascending).ToList

                    Case Else
                        LstTri = (From instance In LstRes Select instance _
                               Where instance.Etablissement = Etablissement _
                               Order By instance.Intitule Ascending).ToList

                End Select
                Return LstTri
            End Get
        End Property

        Public ReadOnly Property NombreUnites() As Integer Implements IGestionVirFicheModif(Of CYC_IDENTIFICATION).NombredItems
            Get
                Return WsListeUnites.Count
            End Get
        End Property

        Public Function Fiche_Unite(ByVal Ide_Dossier As Integer) As CYC_IDENTIFICATION Implements IGestionVirFiche(Of CYC_IDENTIFICATION).GetFiche
            Return WsListeUnites.Find(Function(m) m.Ide_Dossier = Ide_Dossier)
        End Function

        Public Function Fiche_Unite(ByVal Intitule As String) As CYC_IDENTIFICATION Implements IGestionVirFiche(Of CYC_IDENTIFICATION).GetFiche
            Return WsListeUnites.Find(Function(m) m.Intitule = Intitule)
        End Function

        Public Sub SupprimerItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of CYC_IDENTIFICATION).SupprimerItem
            Dim FicheUnite As CYC_IDENTIFICATION
            FicheUnite = Fiche_Unite(Ide)
            If FicheUnite IsNot Nothing Then
                WsListeUnites.Remove(FicheUnite)
            End If
        End Sub

        Public Sub AjouterItem(ByVal Ide As Integer) Implements IGestionVirFicheModif(Of CYC_IDENTIFICATION).AjouterItem
            SupprimerItem(Ide)
            ConstituerListe(Ide)
        End Sub

        Public Sub Actualiser() Implements IGestionVirFicheModif(Of CYC_IDENTIFICATION).Actualiser
            WsListeUnites.Clear()
            ConstituerListe(0)
        End Sub

        Private Sub ConstituerListe(ByVal Ide As Integer)

            Dim FicheUnite As CYC_IDENTIFICATION
            Dim LstResultat As List(Of VIR_FICHE)
            Dim IndiceA As Integer
            Dim IndiceK As Integer
            Dim TableauData(0) As String
            Dim IndiceNB As Integer

            '** 1  Liste de toutes les unités de cycle
            LstResultat = VirServiceServeur.LectureObjet_ToFiches(WsNomUtilisateur, VI.PointdeVue.PVueBaseHebdo, 1, Ide, False)
            WsListeUnites = VirRhFonction.ConvertisseurListeFiches(Of CYC_IDENTIFICATION)(LstResultat)

            '** 2  Liste de tous les jours du cycle
            For IndiceK = 2 To 8
                LstResultat = VirServiceServeur.LectureObjet_ToFiches(WsNomUtilisateur, VI.PointdeVue.PVueBaseHebdo, CInt(IndiceK), Ide, False)
                If LstResultat IsNot Nothing Then
                    For IndiceA = 0 To LstResultat.Count - 1
                        FicheUnite = Fiche_Unite(LstResultat(IndiceA).Ide_Dossier)
                        If FicheUnite IsNot Nothing Then
                            IndiceNB = FicheUnite.Ajouter_Cycle(CType(LstResultat(IndiceA), CYC_UNITE))
                        End If
                    Next IndiceA
                End If
            Next IndiceK
        End Sub

        Public Sub New(ByVal NomUtilisateur As String, ByVal Ide As Integer)
            WsNomUtilisateur = NomUtilisateur
            WsListeUnites = New List(Of CYC_IDENTIFICATION)
            ConstituerListe(Ide)
        End Sub

    End Class
End Namespace

