﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class GRD_EMPLOI
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsDenomination As String
        Private WsRemuMaxi As Double
        Private WsEchelle_de_remuneration As String
        Private WsCategorie As String
        Private WsGroupe As String
        Private WsFiliere As String
        Private WsDuree_PeriodeEssai As Integer
        Private WsDescriptif As String
        Private WsConditions_d_acces As String
        Private WsRemuMini As Double
        Private WsCoutHoraireMoyen As Double

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "GRD_EMPLOI"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueGrades
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Denomination() As String
            Get
                Return WsDenomination
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsDenomination = value
                    Case Else
                        WsDenomination = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property RemunerationMaximum() As Double
            Get
                Return WsRemuMaxi
            End Get
            Set(ByVal value As Double)
                WsRemuMaxi = value
            End Set
        End Property

        Public Property Echelle_de_remuneration() As String
            Get
                Return WsEchelle_de_remuneration
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsEchelle_de_remuneration = value
                    Case Else
                        WsEchelle_de_remuneration = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Categorie() As String
            Get
                Return WsCategorie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsCategorie = value
                    Case Else
                        WsCategorie = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Groupe() As String
            Get
                Return WsGroupe
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsGroupe = value
                    Case Else
                        WsGroupe = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Filiere() As String
            Get
                Return WsFiliere
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsFiliere = value
                    Case Else
                        WsFiliere = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Duree_Periode_Essai() As Integer
            Get
                Return WsDuree_PeriodeEssai
            End Get
            Set(ByVal value As Integer)
                WsDuree_PeriodeEssai = value
            End Set
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsDescriptif = value
                    Case Else
                        WsDescriptif = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Conditions_d_acces() As String
            Get
                Return WsConditions_d_acces
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsConditions_d_acces = value
                    Case Else
                        WsConditions_d_acces = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property RemunerationMinimum() As Double
            Get
                Return WsRemuMini
            End Get
            Set(ByVal value As Double)
                WsRemuMini = value
            End Set
        End Property

        Public Property CoutHoraire_Moyen() As Double
            Get
                Return WsCoutHoraireMoyen
            End Get
            Set(ByVal value As Double)
                WsCoutHoraireMoyen = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(WsDenomination & VI.Tild)
                Chaine.Append(RemunerationMaximum.ToString & VI.Tild)
                Chaine.Append(Echelle_de_remuneration & VI.Tild)
                Chaine.Append(Categorie & VI.Tild)
                Chaine.Append(Groupe & VI.Tild)
                Chaine.Append(Filiere & VI.Tild)
                Chaine.Append(Duree_Periode_Essai.ToString & VI.Tild)
                Chaine.Append(Descriptif & VI.Tild)
                Chaine.Append(Conditions_d_acces & VI.Tild)
                Chaine.Append(RemunerationMinimum.ToString & VI.Tild)
                Chaine.Append(CoutHoraire_Moyen.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 20 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                WsDenomination = TableauData(1)
                If TableauData(2) = "" Then
                    TableauData(2) = "0"
                End If
                RemunerationMaximum = VirRhFonction.ConversionDouble(TableauData(2))
                Echelle_de_remuneration = TableauData(3)
                Categorie = TableauData(4)
                Groupe = TableauData(5)
                Filiere = TableauData(6)
                Select Case TableauData(7)
                    Case Is = ""
                        TableauData(7) = "0"
                End Select
                Duree_Periode_Essai = CInt(TableauData(7))
                Descriptif = TableauData(8)
                Conditions_d_acces = TableauData(9)
                If TableauData(10) = "" Then
                    TableauData(10) = "0"
                End If
                RemunerationMinimum = VirRhFonction.ConversionDouble(TableauData(10))
                If TableauData(11) = "" Then
                    TableauData(11) = "0"
                End If
                CoutHoraire_Moyen = VirRhFonction.ConversionDouble(TableauData(11))

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
