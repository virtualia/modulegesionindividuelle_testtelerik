﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class UTI_CRITERE_DOSSIER
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsObjetPer As Integer
        Private WsInfoPer As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "UTI_CRITERE_DOSSIER"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueUtilisateur
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 4
            End Get
        End Property

        Public Property Numero_ObjetPER() As Integer
            Get
                Return WsObjetPer
            End Get
            Set(ByVal value As Integer)
                WsObjetPer = value
            End Set
        End Property

        Public Property Numero_InfoPER() As Integer
            Get
                Return WsInfoPer
            End Get
            Set(ByVal value As Integer)
                WsInfoPer = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Numero_ObjetPER & VI.Tild)
                Chaine.Append(Numero_InfoPER)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 3 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) = "" Then
                    TableauData(1) = "0"
                End If
                Numero_ObjetPER = CInt(TableauData(1))
                If TableauData(2) = "" Then
                    TableauData(2) = "0"
                End If
                Numero_InfoPER = CInt(TableauData(2))

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
