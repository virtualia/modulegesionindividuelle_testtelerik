﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace ShemaREF
    Partial Public Class FOR_INSCRIPTIONS
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsRang_inscription As Integer
        Private WsDate_inscription As String = ""
        Private WsNom_stagiaire As String = ""
        Private WsPrenom_stagiaire As String = ""
        Private WsDatenaissance_stagiaire As String = ""
        Private WsEtablissement_stagiaire As String = ""
        Private WsAppartenanceaupersonnel As String = ""
        Private WsCoutpedagogique As Double = 0
        Private WsCoutdeplacement As Double = 0
        Private WsCoutrestauration As Double = 0
        Private WsCouthebergement As Double = 0
        Private WsNbheures_formation As Double = 0
        Private WsNbheures_deplacement As Integer = 0
        Private WsDatedebut_formation As String = ""
        Private WsDatefin_formation As String = ""
        Private WsReference_session As String = ""
        Private WsSuivi_inscription As String = ""
        Private WsMotif_nonparticipation As String = ""
        Private WsSuivipresence As String = ""
        Private WsOrdrepriorite_inscription As String = ""
        Private WsDemandehebergement As String = ""
        Private WsCommentaire As String = ""
        Private WsOrganismepayeur_pedagogie As String = ""
        Private WsOrganismepayeur_autrecouts As String = ""
        Private WsCoutforfaitaire As Double = 0
        Private WsCursus_formation As String = ""
        Private WsModule_formation As String = ""
        Private WsInscriptionpersonnalisee As String = ""
        Private WsDirection_stagiaire As String = ""
        Private WsService_stagiaire As String = ""
        Private WsAction_formation As String = ""
        Private WsOrganisme_formation As String = ""
        Private WsContexte_formation As String = ""
        Private WsFiliere_formation As String = ""
        Private WsDIF As String = ""
        Private WsDIF_horstravail As String = ""
        Private WsRubrique_formation As String = "" 'Sous-Thème
        Private WsNbheures_DIF As Double = 0
        Private WsRangSession As Integer = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "FOR_INSCRIPTIONS"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueFormation
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 8
            End Get
        End Property

        Public Property Rang_inscription() As Integer
            Get
                Return WsRang_inscription
            End Get
            Set(ByVal value As Integer)
                WsRang_inscription = value
            End Set
        End Property

        Public Property Date_inscription() As String
            Get
                Return WsDate_inscription
            End Get
            Set(ByVal value As String)
                WsDate_inscription = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Nom_stagiaire() As String
            Get
                Return WsNom_stagiaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsNom_stagiaire = value
                    Case Else
                        WsNom_stagiaire = Strings.Left(value, 35)
                End Select
            End Set
        End Property

        Public Property Prenom_stagiaire() As String
            Get
                Return WsPrenom_stagiaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsPrenom_stagiaire = value
                    Case Else
                        WsPrenom_stagiaire = Strings.Left(value, 35)
                End Select
            End Set
        End Property

        Public Property Datenaissance_stagiaire() As String
            Get
                Return WsDatenaissance_stagiaire
            End Get
            Set(ByVal value As String)
                WsDatenaissance_stagiaire = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Etablissement_stagiaire() As String
            Get
                Return WsEtablissement_stagiaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsEtablissement_stagiaire = value
                    Case Else
                        WsEtablissement_stagiaire = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Appartenanceaupersonnel() As String
            Get
                Select Case WsAppartenanceaupersonnel
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsAppartenanceaupersonnel
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsAppartenanceaupersonnel = value
                    Case Else
                        WsAppartenanceaupersonnel = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Coutpedagogique() As Double
            Get
                Return WsCoutpedagogique
            End Get
            Set(ByVal value As Double)
                WsCoutpedagogique = value
            End Set
        End Property

        Public Property Coutdeplacement() As Double
            Get
                Return WsCoutdeplacement
            End Get
            Set(ByVal value As Double)
                WsCoutdeplacement = value
            End Set
        End Property

        Public Property Coutrestauration() As Double
            Get
                Return WsCoutrestauration
            End Get
            Set(ByVal value As Double)
                WsCoutrestauration = value
            End Set
        End Property

        Public Property Couthebergement() As Double
            Get
                Return WsCouthebergement
            End Get
            Set(ByVal value As Double)
                WsCouthebergement = value
            End Set
        End Property

        Public Property Nbheures_formation() As Double
            Get
                Return WsNbheures_formation
            End Get
            Set(ByVal value As Double)
                WsNbheures_formation = value
            End Set
        End Property

        Public Property Nbheures_deplacement() As Integer
            Get
                Return WsNbheures_deplacement
            End Get
            Set(ByVal value As Integer)
                WsNbheures_deplacement = value
            End Set
        End Property

        Public Property Datedebut_formation() As String
            Get
                Return WsDatedebut_formation
            End Get
            Set(ByVal value As String)
                WsDatedebut_formation = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = value
            End Set
        End Property

        Public Property Datefin_formation() As String
            Get
                Return WsDatefin_formation
            End Get
            Set(ByVal value As String)
                WsDatefin_formation = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Fin = value
            End Set
        End Property

        Public Property Reference_session() As String
            Get
                Return WsReference_session
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsReference_session = value
                    Case Else
                        WsReference_session = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Suivi_inscription() As String
            Get
                Return WsSuivi_inscription
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsSuivi_inscription = value
                    Case Else
                        WsSuivi_inscription = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Motif_nonparticipation() As String
            Get
                Return WsMotif_nonparticipation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsMotif_nonparticipation = value
                    Case Else
                        WsMotif_nonparticipation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Suivipresence() As String
            Get
                Return WsSuivipresence
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsSuivipresence = value
                    Case Else
                        WsSuivipresence = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Ordrepriorite_inscription() As String
            Get
                Return WsOrdrepriorite_inscription
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsOrdrepriorite_inscription = value
                    Case Else
                        WsOrdrepriorite_inscription = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Demandehebergement() As String
            Get
                Select Case WsDemandehebergement
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsDemandehebergement
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsDemandehebergement = value
                    Case Else
                        WsDemandehebergement = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Commentaire() As String
            Get
                Return WsCommentaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsCommentaire = value
                    Case Else
                        WsCommentaire = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property Organismepayeur_pedagogie() As String
            Get
                Return WsOrganismepayeur_pedagogie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsOrganismepayeur_pedagogie = value
                    Case Else
                        WsOrganismepayeur_pedagogie = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Organismepayeur_autrecouts() As String
            Get
                Return WsOrganismepayeur_autrecouts
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsOrganismepayeur_autrecouts = value
                    Case Else
                        WsOrganismepayeur_autrecouts = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Coutforfaitaire() As Double
            Get
                Return WsCoutforfaitaire
            End Get
            Set(ByVal value As Double)
                WsCoutforfaitaire = value
            End Set
        End Property

        Public Property Cursus_formation() As String
            Get
                Return WsCursus_formation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsCursus_formation = value
                    Case Else
                        WsCursus_formation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Module_formation() As String
            Get
                Return WsModule_formation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsModule_formation = value
                    Case Else
                        WsModule_formation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Inscriptionpersonnalisee() As String
            Get
                Select Case WsInscriptionpersonnalisee
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsInscriptionpersonnalisee
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsInscriptionpersonnalisee = value
                    Case Else
                        WsInscriptionpersonnalisee = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Direction_stagiaire() As String
            Get
                Return WsDirection_stagiaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsDirection_stagiaire = value
                    Case Else
                        WsDirection_stagiaire = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Service_stagiaire() As String
            Get
                Return WsService_stagiaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsService_stagiaire = value
                    Case Else
                        WsService_stagiaire = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Action_formation() As String
            Get
                Return WsAction_formation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsAction_formation = value
                    Case Else
                        WsAction_formation = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Organisme_formation() As String
            Get
                Return WsOrganisme_formation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsOrganisme_formation = value
                    Case Else
                        WsOrganisme_formation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Contexte_formation() As String
            Get
                Return WsContexte_formation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsContexte_formation = value
                    Case Else
                        WsContexte_formation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Filiere_formation() As String
            Get
                Return WsFiliere_formation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsFiliere_formation = value
                    Case Else
                        WsFiliere_formation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property DIF() As String
            Get
                Select Case WsDIF
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsDIF
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsDIF = value
                    Case Else
                        WsDIF = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property DIF_horstravail() As String
            Get
                Select Case WsDIF_horstravail
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsDIF_horstravail
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsDIF_horstravail = value
                    Case Else
                        WsDIF_horstravail = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Rubrique_formation() As String
            Get
                Return WsRubrique_formation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsRubrique_formation = value
                    Case Else
                        WsRubrique_formation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Nombreheures_DIF() As Double
            Get
                Return WsNbheures_DIF
            End Get
            Set(ByVal value As Double)
                WsNbheures_DIF = value
            End Set
        End Property

        Public Property Rang_Session() As Integer
            Get
                Return WsRangSession
            End Get
            Set(ByVal value As Integer)
                WsRangSession = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)

                Chaine.Append(Rang_inscription.ToString & VI.Tild)
                Chaine.Append(Date_inscription & VI.Tild)
                Chaine.Append(Nom_stagiaire & VI.Tild)
                Chaine.Append(Prenom_stagiaire & VI.Tild)
                Chaine.Append(Datenaissance_stagiaire & VI.Tild)
                Chaine.Append(Etablissement_stagiaire & VI.Tild)
                Chaine.Append(Appartenanceaupersonnel & VI.Tild)
                Chaine.Append(Coutpedagogique.ToString & VI.Tild)
                Chaine.Append(Coutdeplacement.ToString & VI.Tild)
                Chaine.Append(Coutrestauration.ToString & VI.Tild)
                Chaine.Append(Couthebergement.ToString & VI.Tild)
                Chaine.Append(Nbheures_formation.ToString & VI.Tild)
                Chaine.Append(Nbheures_deplacement.ToString & VI.Tild)
                Chaine.Append(Datedebut_formation & VI.Tild)
                Chaine.Append(Datefin_formation & VI.Tild)
                Chaine.Append(Reference_session & VI.Tild)
                Chaine.Append(Suivi_inscription & VI.Tild)
                Chaine.Append(Motif_nonparticipation & VI.Tild)
                Chaine.Append(Suivipresence & VI.Tild)
                Chaine.Append(Ordrepriorite_inscription & VI.Tild)
                Chaine.Append(Demandehebergement & VI.Tild)
                Chaine.Append(Commentaire & VI.Tild)
                Chaine.Append(Organismepayeur_pedagogie & VI.Tild)
                Chaine.Append(Organismepayeur_autrecouts & VI.Tild)
                Chaine.Append(Coutforfaitaire.ToString & VI.Tild)
                Chaine.Append(Cursus_formation & VI.Tild)
                Chaine.Append(Module_formation & VI.Tild)
                Chaine.Append(Inscriptionpersonnalisee & VI.Tild)
                Chaine.Append(Direction_stagiaire & VI.Tild)
                Chaine.Append(Service_stagiaire & VI.Tild)
                Chaine.Append(Action_formation & VI.Tild)
                Chaine.Append(Organisme_formation & VI.Tild)
                Chaine.Append(Contexte_formation & VI.Tild)
                Chaine.Append(Filiere_formation & VI.Tild)
                Chaine.Append(DIF & VI.Tild)
                Chaine.Append(DIF_horstravail & VI.Tild)
                Chaine.Append(Rubrique_formation & VI.Tild)
                Chaine.Append(Nombreheures_DIF.ToString & VI.Tild)
                Chaine.Append(Rang_Session.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 40 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))

                If TableauData(1) <> "" Then Rang_inscription = CInt(TableauData(1))
                Date_inscription = TableauData(2)
                Nom_stagiaire = TableauData(3)
                Prenom_stagiaire = TableauData(4)
                Datenaissance_stagiaire = TableauData(5)
                Etablissement_stagiaire = TableauData(6)
                Appartenanceaupersonnel = TableauData(7)
                If TableauData(8) <> "" Then Coutpedagogique = VirRhFonction.ConversionDouble(TableauData(8))
                If TableauData(9) <> "" Then Coutdeplacement = VirRhFonction.ConversionDouble(TableauData(9))
                If TableauData(10) <> "" Then Coutrestauration = VirRhFonction.ConversionDouble(TableauData(10))
                If TableauData(11) <> "" Then Couthebergement = VirRhFonction.ConversionDouble(TableauData(11))
                If TableauData(12) <> "" Then Nbheures_formation = VirRhFonction.ConversionDouble(TableauData(12))
                If TableauData(13) <> "" Then Nbheures_deplacement = CInt(TableauData(13))
                Datedebut_formation = TableauData(14) 'Clé
                Datefin_formation = TableauData(15)
                Reference_session = TableauData(16)
                Suivi_inscription = TableauData(17)
                Motif_nonparticipation = TableauData(18)
                Suivipresence = TableauData(19)
                Ordrepriorite_inscription = TableauData(20)
                Demandehebergement = TableauData(21)
                Commentaire = TableauData(22)
                Organismepayeur_pedagogie = TableauData(23)
                Organismepayeur_autrecouts = TableauData(24)
                If TableauData(25) <> "" Then Coutforfaitaire = VirRhFonction.ConversionDouble(TableauData(25))
                Cursus_formation = TableauData(26)
                Module_formation = TableauData(27)
                Inscriptionpersonnalisee = TableauData(28)
                Direction_stagiaire = TableauData(29)
                Service_stagiaire = TableauData(30)
                Action_formation = TableauData(31)
                Organisme_formation = TableauData(32)
                Contexte_formation = TableauData(33)
                Filiere_formation = TableauData(34)
                DIF = TableauData(35)
                DIF_horstravail = TableauData(36)
                Rubrique_formation = TableauData(37)
                If TableauData(38) <> "" Then Nombreheures_DIF = VirRhFonction.ConversionDouble(TableauData(38))
                If TableauData(39) <> "" Then Rang_Session = CInt(TableauData(39))

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace


