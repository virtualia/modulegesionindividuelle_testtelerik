﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class FOR_FACTURE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsReferencefacture As String
        Private WsNaturefacture As String
        Private WsStatutcreance As String
        Private WsMontant_HT As Double = 0
        Private WsMontant_TVA As Double = 0
        Private WsMontant_TTC As Double = 0
        Private WsObservations As String
        Private WsOrganisme_payeur As String
        Private WsOrganisme_prestataire As String
        Private WsDatedebut_periodefacturation As String
        Private WsDatefin_periodefacturation As String
        Private WsDateordonnancement As String
        Private WsDatereglement As String
        Private WsImputationbudgetaire As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "FOR_FACTURE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueFormation
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 7
            End Get
        End Property

        Public Property Datefacture() As String
            Get
                Return MyBase.Date_de_Valeur
            End Get
            Set(ByVal value As String)
                MyBase.Date_de_Valeur = value
            End Set
        End Property

        Public Property Referencefacture() As String
            Get
                Return WsReferencefacture
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsReferencefacture = value
                    Case Else
                        WsReferencefacture = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Naturefacture() As String
            Get
                Return WsNaturefacture
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsNaturefacture = value
                    Case Else
                        WsNaturefacture = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Statutcreance() As String
            Get
                Return WsStatutcreance
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsStatutcreance = value
                    Case Else
                        WsStatutcreance = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Montant_HT() As Double
            Get
                Return WsMontant_HT
            End Get
            Set(ByVal value As Double)
                WsMontant_HT = value
            End Set
        End Property

        Public Property Montant_TVA() As Double
            Get
                Return WsMontant_TVA
            End Get
            Set(ByVal value As Double)
                WsMontant_TVA = value
            End Set
        End Property

        Public Property Montant_TTC() As Double
            Get
                Return WsMontant_TTC
            End Get
            Set(ByVal value As Double)
                WsMontant_TTC = value
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property Organisme_payeur() As String
            Get
                Return WsOrganisme_payeur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsOrganisme_payeur = value
                    Case Else
                        WsOrganisme_payeur = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Organisme_prestataire() As String
            Get
                Return WsOrganisme_prestataire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsOrganisme_prestataire = value
                    Case Else
                        WsOrganisme_prestataire = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Datedebut_periodefacturation() As String
            Get
                Return WsDatedebut_periodefacturation
            End Get
            Set(ByVal value As String)
                WsDatedebut_periodefacturation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Datefin_periodefacturation() As String
            Get
                Return WsDatefin_periodefacturation
            End Get
            Set(ByVal value As String)
                WsDatefin_periodefacturation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Dateordonnancement() As String
            Get
                Return WsDateordonnancement
            End Get
            Set(ByVal value As String)
                WsDateordonnancement = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Datereglement() As String
            Get
                Return WsDatereglement
            End Get
            Set(ByVal value As String)
                WsDatereglement = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Imputationbudgetaire() As String
            Get
                Return WsImputationbudgetaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsImputationbudgetaire = value
                    Case Else
                        WsImputationbudgetaire = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(Datefacture & VI.Tild)
                Chaine.Append(Referencefacture & VI.Tild)
                Chaine.Append(Naturefacture & VI.Tild)
                Chaine.Append(Statutcreance & VI.Tild)
                Chaine.Append(Montant_HT.ToString & VI.Tild)
                Chaine.Append(Montant_TVA.ToString & VI.Tild)
                Chaine.Append(Montant_TTC.ToString & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(Organisme_payeur & VI.Tild)
                Chaine.Append(Organisme_prestataire & VI.Tild)
                Chaine.Append(Datedebut_periodefacturation & VI.Tild)
                Chaine.Append(Datefin_periodefacturation & VI.Tild)
                Chaine.Append(Dateordonnancement & VI.Tild)
                Chaine.Append(Datereglement & VI.Tild)
                Chaine.Append(Imputationbudgetaire)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 16 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Datefacture = TableauData(1)
                Referencefacture = TableauData(2)
                Naturefacture = TableauData(3)
                Statutcreance = TableauData(4)
                If TableauData(5) <> "" Then Montant_HT = VirRhFonction.ConversionDouble(TableauData(5))
                If TableauData(6) <> "" Then Montant_TVA = VirRhFonction.ConversionDouble(TableauData(6))
                If TableauData(7) <> "" Then Montant_TTC = VirRhFonction.ConversionDouble(TableauData(7))
                Observations = TableauData(8)
                Organisme_payeur = TableauData(9)
                Organisme_prestataire = TableauData(10)
                Datedebut_periodefacturation = TableauData(11)
                Datefin_periodefacturation = TableauData(12)
                Dateordonnancement = TableauData(13)
                Datereglement = TableauData(14)
                Imputationbudgetaire = TableauData(15)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace


