﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAI_FERIE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsJourFerie As String
        Private WsSiTravaille As String
        '
        Private TsAnnee As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAI_FERIE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePaie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property JourFerieMobile() As String
            Get
                Return WsJourFerie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsJourFerie = value
                    Case Else
                        WsJourFerie = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Si_Plus_JourFerie() As String
            Get
                Select Case WsSiTravaille
                    Case ""
                        WsSiTravaille = "Non"
                End Select
                Return WsSiTravaille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiTravaille = value
                    Case Else
                        WsSiTravaille = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(JourFerieMobile & VI.Tild)
                Chaine.Append(Si_Plus_JourFerie)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                JourFerieMobile = TableauData(2)
                Si_Plus_JourFerie = TableauData(3)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsAnnee = CInt(Strings.Right(Date_de_Valeur, 4))

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property Annee() As Integer
            Get
                Return TsAnnee
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
