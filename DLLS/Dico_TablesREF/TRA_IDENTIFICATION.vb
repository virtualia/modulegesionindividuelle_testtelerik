﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class TRA_IDENTIFICATION
        Inherits VIR_FICHE

        Private WsLstRegles As List(Of TRA_VALORISATION)
        Private WsLstAccords As List(Of ACCORD_RTT)
        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsUniteCycle_N1 As String
        Private WsUniteCycle_N2 As String
        Private WsUniteCycle_N3 As String
        Private WsUniteCycle_N4 As String
        Private WsQualification As String
        Private WsObservation As String
        Private WsUniteCycle_N5 As String
        Private WsUniteCycle_N6 As String
        Private WsUniteCycle_N7 As String
        Private WsUniteCycle_N8 As String
        Private WsEtablissement As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "TRA_IDENTIFICATION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueCycle
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Unite_de_Cycle_N1() As String
            Get
                Return WsUniteCycle_N1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsUniteCycle_N1 = value
                    Case Else
                        WsUniteCycle_N1 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Unite_de_Cycle_N2() As String
            Get
                Return WsUniteCycle_N2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsUniteCycle_N2 = value
                    Case Else
                        WsUniteCycle_N2 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Unite_de_Cycle_N3() As String
            Get
                Return WsUniteCycle_N3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsUniteCycle_N3 = value
                    Case Else
                        WsUniteCycle_N3 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Unite_de_Cycle_N4() As String
            Get
                Return WsUniteCycle_N4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsUniteCycle_N4 = value
                    Case Else
                        WsUniteCycle_N4 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Qualification() As String
            Get
                Return WsQualification
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsQualification = value
                    Case Else
                        WsQualification = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservation = value
                    Case Else
                        WsObservation = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Unite_de_Cycle_N5() As String
            Get
                Return WsUniteCycle_N5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsUniteCycle_N5 = value
                    Case Else
                        WsUniteCycle_N5 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Unite_de_Cycle_N6() As String
            Get
                Return WsUniteCycle_N6
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsUniteCycle_N6 = value
                    Case Else
                        WsUniteCycle_N6 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Unite_de_Cycle_N7() As String
            Get
                Return WsUniteCycle_N7
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsUniteCycle_N7 = value
                    Case Else
                        WsUniteCycle_N7 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Unite_de_Cycle_N8() As String
            Get
                Return WsUniteCycle_N8
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsUniteCycle_N8 = value
                    Case Else
                        WsUniteCycle_N8 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Etablissement() As String
            Get
                Return WsEtablissement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsEtablissement = value
                    Case Else
                        WsEtablissement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Unite_de_Cycle_N1 & VI.Tild)
                Chaine.Append(Unite_de_Cycle_N2 & VI.Tild)
                Chaine.Append(Unite_de_Cycle_N3 & VI.Tild)
                Chaine.Append(Unite_de_Cycle_N4 & VI.Tild)
                Chaine.Append(Qualification & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(Unite_de_Cycle_N5 & VI.Tild)
                Chaine.Append(Unite_de_Cycle_N6 & VI.Tild)
                Chaine.Append(Unite_de_Cycle_N7 & VI.Tild)
                Chaine.Append(Unite_de_Cycle_N8 & VI.Tild)
                Chaine.Append(Etablissement)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 13 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                Unite_de_Cycle_N1 = TableauData(2)
                Unite_de_Cycle_N2 = TableauData(3)
                Unite_de_Cycle_N3 = TableauData(4)
                Unite_de_Cycle_N4 = TableauData(5)
                Qualification = TableauData(6)
                Observations = TableauData(7)
                Unite_de_Cycle_N5 = TableauData(8)
                Unite_de_Cycle_N6 = TableauData(9)
                Unite_de_Cycle_N7 = TableauData(10)
                Unite_de_Cycle_N8 = TableauData(11)
                If TableauData(12) <> "" Then
                    Etablissement = TableauData(12)
                Else
                    Etablissement = "Commun"
                End If

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesRegles() As List(Of TRA_VALORISATION)
            Get
                If WsLstRegles Is Nothing Then
                    Return Nothing
                End If
                Return WsLstRegles.OrderBy(Function(m) m.Regle_A_Appliquer).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesAccords() As List(Of ACCORD_RTT)
            Get
                If WsLstAccords Is Nothing Then
                    Return Nothing
                End If
                Return WsLstAccords.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Regle(ByVal Fiche As TRA_VALORISATION) As Integer

            If WsLstRegles Is Nothing Then
                WsLstRegles = New List(Of TRA_VALORISATION)
            End If
            WsLstRegles.Add(Fiche)
            Return WsLstRegles.Count

        End Function

        Public Function Ajouter_Accord(ByVal Fiche As ACCORD_RTT) As Integer

            If WsLstAccords Is Nothing Then
                WsLstAccords = New List(Of ACCORD_RTT)
            End If
            WsLstAccords.Add(Fiche)
            Return WsLstAccords.Count

        End Function

        Public ReadOnly Property Fiche_Accord(ByVal DateValeur As String) As ACCORD_RTT
            Get
                If WsLstAccords Is Nothing Then
                    Return Nothing
                End If

                Return (From it As ACCORD_RTT In ListedesAccords Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) Select it).FirstOrDefault()
            End Get
        End Property

    End Class
End Namespace


