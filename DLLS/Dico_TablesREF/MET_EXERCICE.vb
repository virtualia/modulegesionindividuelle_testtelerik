﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class MET_EXERCICE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsConditionexercice_no1 As String
        Private WsConditionexercice_no2 As String
        Private WsConditionexercice_no3 As String
        Private WsConditionexercice_no4 As String
        Private WsConditionexercice_no5 As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "MET_EXERCICE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueMetier
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Conditionexercice_no1() As String
            Get
                Return WsConditionexercice_no1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsConditionexercice_no1 = value
                    Case Else
                        WsConditionexercice_no1 = Strings.Left(value, 120)
                End Select
            End Set
        End Property
        Public Property Conditionexercice_no2() As String
            Get
                Return WsConditionexercice_no2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsConditionexercice_no2 = value
                    Case Else
                        WsConditionexercice_no2 = Strings.Left(value, 120)
                End Select
            End Set
        End Property
        Public Property Conditionexercice_no3() As String
            Get
                Return WsConditionexercice_no3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsConditionexercice_no3 = value
                    Case Else
                        WsConditionexercice_no3 = Strings.Left(value, 120)
                End Select
            End Set
        End Property
        Public Property Conditionexercice_no4() As String
            Get
                Return WsConditionexercice_no4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsConditionexercice_no4 = value
                    Case Else
                        WsConditionexercice_no4 = Strings.Left(value, 120)
                End Select
            End Set
        End Property
        Public Property Conditionexercice_no5() As String
            Get
                Return WsConditionexercice_no5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsConditionexercice_no5 = value
                    Case Else
                        WsConditionexercice_no5 = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Conditionexercice_no1 & VI.Tild)
                Chaine.Append(Conditionexercice_no2 & VI.Tild)
                Chaine.Append(Conditionexercice_no3 & VI.Tild)
                Chaine.Append(Conditionexercice_no4 & VI.Tild)
                Chaine.Append(Conditionexercice_no5)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 6 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Conditionexercice_no1 = TableauData(1)
                Conditionexercice_no2 = TableauData(2)
                Conditionexercice_no3 = TableauData(3)
                Conditionexercice_no4 = TableauData(4)
                Conditionexercice_no5 = TableauData(5)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

