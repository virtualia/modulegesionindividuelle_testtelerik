﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAI_SFT
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsIndicePlancher As Integer = 0
        Private WsIndicePlafonds As Integer = 0
        Private WsElementFixe_1 As Double = 0
        Private WsElementFixe_2 As Double = 0
        Private WsElementFixe_3 As Double = 0
        Private WsElementFixe_Plus_3 As Double = 0
        Private WsElementProportionnel_1 As Double = 0
        Private WsElementProportionnel_2 As Double = 0
        Private WsElementProportionnel_3 As Double = 0
        Private WsElementProportionnel_Plus_3 As Double = 0


        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAI_SFT"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePaie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 10
            End Get
        End Property

        Public Property IndicePlancher() As Integer
            Get
                Return WsIndicePlancher
            End Get
            Set(ByVal value As Integer)
                WsIndicePlancher = value
            End Set
        End Property

        Public Property IndicePlafonds() As Integer
            Get
                Return WsIndicePlafonds
            End Get
            Set(ByVal value As Integer)
                WsIndicePlafonds = value
            End Set
        End Property

        Public Property Element_Fixe_1() As Double
            Get
                Return WsElementFixe_1
            End Get
            Set(ByVal value As Double)
                WsElementFixe_1 = value
            End Set
        End Property

        Public Property Element_Fixe_2() As Double
            Get
                Return WsElementFixe_2
            End Get
            Set(ByVal value As Double)
                WsElementFixe_2 = value
            End Set
        End Property

        Public Property Element_Fixe_3() As Double
            Get
                Return WsElementFixe_3
            End Get
            Set(ByVal value As Double)
                WsElementFixe_3 = value
            End Set
        End Property

        Public Property Element_Fixe_Superieur() As Double
            Get
                Return WsElementFixe_Plus_3
            End Get
            Set(ByVal value As Double)
                WsElementFixe_Plus_3 = value
            End Set
        End Property

        Public Property Element_Proportionnel_1() As Double
            Get
                Return WsElementProportionnel_1
            End Get
            Set(ByVal value As Double)
                WsElementProportionnel_1 = value
            End Set
        End Property

        Public Property Element_Proportionnel_2() As Double
            Get
                Return WsElementProportionnel_2
            End Get
            Set(ByVal value As Double)
                WsElementProportionnel_2 = value
            End Set
        End Property

        Public Property Element_Proportionnel_3() As Double
            Get
                Return WsElementProportionnel_3
            End Get
            Set(ByVal value As Double)
                WsElementProportionnel_3 = value
            End Set
        End Property

        Public Property Element_Proportionnel_Superieur() As Double
            Get
                Return WsElementProportionnel_Plus_3
            End Get
            Set(ByVal value As Double)
                WsElementProportionnel_Plus_3 = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(IndicePlancher.ToString & VI.Tild)
                Chaine.Append(IndicePlafonds.ToString & VI.Tild)
                Chaine.Append(Element_Fixe_1.ToString & VI.Tild)
                Chaine.Append(Element_Fixe_2.ToString & VI.Tild)
                Chaine.Append(Element_Fixe_3.ToString & VI.Tild)
                Chaine.Append(Element_Fixe_Superieur.ToString & VI.Tild)
                Chaine.Append(Element_Proportionnel_1.ToString & VI.Tild)
                Chaine.Append(Element_Proportionnel_2.ToString & VI.Tild)
                Chaine.Append(Element_Proportionnel_3.ToString & VI.Tild)
                Chaine.Append(Element_Proportionnel_Superieur.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 12 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then IndicePlancher = CInt(TableauData(2))
                If TableauData(3) <> "" Then IndicePlafonds = CInt(TableauData(3))
                If TableauData(4) <> "" Then Element_Fixe_1 = VirRhFonction.ConversionDouble(TableauData(4))
                If TableauData(5) <> "" Then Element_Fixe_2 = VirRhFonction.ConversionDouble(TableauData(5))
                If TableauData(6) <> "" Then Element_Fixe_3 = VirRhFonction.ConversionDouble(TableauData(6))
                If TableauData(7) <> "" Then Element_Fixe_Superieur = VirRhFonction.ConversionDouble(TableauData(7))
                If TableauData(8) <> "" Then Element_Proportionnel_1 = VirRhFonction.ConversionDouble(TableauData(8))
                If TableauData(9) <> "" Then Element_Proportionnel_2 = VirRhFonction.ConversionDouble(TableauData(9))
                If TableauData(10) <> "" Then Element_Proportionnel_3 = VirRhFonction.ConversionDouble(TableauData(10))
                If TableauData(11) <> "" Then Element_Proportionnel_Superieur = VirRhFonction.ConversionDouble(TableauData(11))

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
