﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class GRD_GRADE_DGCL
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsCode As String
        Private WsIntitule As String
        Private WsCadreEmploi As String
        Private WsTypeFP As String
        Private WsStatut As String
        Private WsCategorie As String
        Private WsFiliere As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "GRD_GRADE_DGCL"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueGrades
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 4
            End Get
        End Property

        Public Property CodeNomenclature() As String
            Get
                Return WsCode
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsCode = value
                    Case Else
                        WsCode = Strings.Left(value, 4)
                End Select
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Cadre_Emploi() As String
            Get
                Return WsCadreEmploi
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsCadreEmploi = value
                    Case Else
                        WsCadreEmploi = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Type_FP() As String
            Get
                Return WsTypeFP
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsTypeFP = value
                    Case Else
                        WsTypeFP = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Statut() As String
            Get
                Return WsStatut
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsStatut = value
                    Case Else
                        WsStatut = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Categorie() As String
            Get
                If WsCategorie = "" Then
                    Return "Non définie"
                Else
                    Return WsCategorie
                End If
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsCategorie = value
                    Case Else
                        WsCategorie = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Filiere() As String
            Get
                If WsFiliere = "" Then
                    Return "Non définie"
                Else
                    Return WsFiliere
                End If
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsFiliere = value
                    Case Else
                        WsFiliere = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(CodeNomenclature & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Cadre_Emploi & VI.Tild)
                Chaine.Append(Type_FP & VI.Tild)
                Chaine.Append(Statut & VI.Tild)
                Chaine.Append(Categorie & VI.Tild)
                Chaine.Append(Filiere)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                CodeNomenclature = TableauData(1)
                Intitule = TableauData(2)
                Cadre_Emploi = TableauData(3)
                Type_FP = TableauData(4)
                Statut = TableauData(5)
                Categorie = TableauData(6)
                Filiere = TableauData(7)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
