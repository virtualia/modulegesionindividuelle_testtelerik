﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class MET_QUALITE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsDomaine As String
        Private WsNiveau_responsabilite As String
        Private WsRolequalites_no1 As String
        Private WsRolequalites_no2 As String
        Private WsRolequalites_no3 As String
        Private WsRolequalites_no4 As String
        Private WsRolequalites_no5 As String
        Private WsRolequalites_no6 As String
        Private WsRolequalites_no7 As String
        Private WsRolequalites_no8 As String
        Private WsRolequalites_no9 As String
        Private WsRolequalites_no10 As String
        Private WsRolequalites_no11 As String
        Private WsRolequalites_no12 As String
        Private WsRolequalites_no13 As String
        Private WsRolequalites_no14 As String
        Private WsRolequalites_no15 As String
        Private WsRolequalites_no16 As String
        Private WsRolequalites_no17 As String
        Private WsRolequalites_no18 As String
        Private WsCompetence_cle As String
        Private WsContexte As String
        Private WsContribution As String
        Private WsQualites As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "MET_QUALITE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueMetier
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 3
            End Get
        End Property

        Public Property Domaine() As String
            Get
                Return WsDomaine
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsDomaine = value
                    Case Else
                        WsDomaine = Strings.Left(value, 2)
                End Select
            End Set
        End Property
        Public Property Niveau_responsabilite() As String
            Get
                Return WsNiveau_responsabilite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsNiveau_responsabilite = value
                    Case Else
                        WsNiveau_responsabilite = Strings.Left(value, 2)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no1() As String
            Get
                Return WsRolequalites_no1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no1 = value
                    Case Else
                        WsRolequalites_no1 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no2() As String
            Get
                Return WsRolequalites_no2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no2 = value
                    Case Else
                        WsRolequalites_no2 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no3() As String
            Get
                Return WsRolequalites_no3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no3 = value
                    Case Else
                        WsRolequalites_no3 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no4() As String
            Get
                Return WsRolequalites_no4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no4 = value
                    Case Else
                        WsRolequalites_no4 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no5() As String
            Get
                Return WsRolequalites_no5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no5 = value
                    Case Else
                        WsRolequalites_no5 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no6() As String
            Get
                Return WsRolequalites_no6
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no6 = value
                    Case Else
                        WsRolequalites_no6 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no7() As String
            Get
                Return WsRolequalites_no7
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no7 = value
                    Case Else
                        WsRolequalites_no7 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no8() As String
            Get
                Return WsRolequalites_no8
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no8 = value
                    Case Else
                        WsRolequalites_no8 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no9() As String
            Get
                Return WsRolequalites_no9
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no9 = value
                    Case Else
                        WsRolequalites_no9 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no10() As String
            Get
                Return WsRolequalites_no10
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no10 = value
                    Case Else
                        WsRolequalites_no10 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no11() As String
            Get
                Return WsRolequalites_no11
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no11 = value
                    Case Else
                        WsRolequalites_no11 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no12() As String
            Get
                Return WsRolequalites_no12
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no12 = value
                    Case Else
                        WsRolequalites_no12 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no13() As String
            Get
                Return WsRolequalites_no13
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no13 = value
                    Case Else
                        WsRolequalites_no13 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no14() As String
            Get
                Return WsRolequalites_no14
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no14 = value
                    Case Else
                        WsRolequalites_no14 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no15() As String
            Get
                Return WsRolequalites_no15
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no15 = value
                    Case Else
                        WsRolequalites_no15 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no16() As String
            Get
                Return WsRolequalites_no16
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no16 = value
                    Case Else
                        WsRolequalites_no16 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no17() As String
            Get
                Return WsRolequalites_no17
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no17 = value
                    Case Else
                        WsRolequalites_no17 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Rolequalites_no18() As String
            Get
                Return WsRolequalites_no18
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 4
                        WsRolequalites_no18 = value
                    Case Else
                        WsRolequalites_no18 = Strings.Left(value, 4)
                End Select
            End Set
        End Property
        Public Property Competence_cle() As String
            Get
                Select Case WsCompetence_cle
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsCompetence_cle
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsCompetence_cle = value
                    Case Else
                        WsCompetence_cle = Strings.Left(value, 3)
                End Select
            End Set
        End Property
        Public Property Contexte() As String
            Get
                Return WsContexte
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsContexte = value
                    Case Else
                        WsContexte = Strings.Left(value, 2)
                End Select
            End Set
        End Property
        Public Property Contribution() As String
            Get
                Return WsContribution
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 15
                        WsContribution = value
                    Case Else
                        WsContribution = Strings.Left(value, 15)
                End Select
            End Set
        End Property
        Public Property Qualites() As String
            Get
                Return WsQualites
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsQualites = value
                    Case Else
                        WsQualites = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Domaine & VI.Tild)
                Chaine.Append(Niveau_responsabilite & VI.Tild)
                Chaine.Append(Rolequalites_no1 & VI.Tild)
                Chaine.Append(Rolequalites_no2 & VI.Tild)
                Chaine.Append(Rolequalites_no3 & VI.Tild)
                Chaine.Append(Rolequalites_no4 & VI.Tild)
                Chaine.Append(Rolequalites_no5 & VI.Tild)
                Chaine.Append(Rolequalites_no6 & VI.Tild)
                Chaine.Append(Rolequalites_no7 & VI.Tild)
                Chaine.Append(Rolequalites_no8 & VI.Tild)
                Chaine.Append(Rolequalites_no9 & VI.Tild)
                Chaine.Append(Rolequalites_no10 & VI.Tild)
                Chaine.Append(Rolequalites_no11 & VI.Tild)
                Chaine.Append(Rolequalites_no12 & VI.Tild)
                Chaine.Append(Rolequalites_no13 & VI.Tild)
                Chaine.Append(Rolequalites_no14 & VI.Tild)
                Chaine.Append(Rolequalites_no15 & VI.Tild)
                Chaine.Append(Rolequalites_no16 & VI.Tild)
                Chaine.Append(Rolequalites_no17 & VI.Tild)
                Chaine.Append(Rolequalites_no18 & VI.Tild)
                Chaine.Append(Competence_cle & VI.Tild)
                Chaine.Append(Contexte & VI.Tild)
                Chaine.Append(Contribution & VI.Tild)
                Chaine.Append(Qualites)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 25 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Domaine = TableauData(1)
                Niveau_responsabilite = TableauData(2)
                Rolequalites_no1 = TableauData(3)
                Rolequalites_no2 = TableauData(4)
                Rolequalites_no3 = TableauData(5)
                Rolequalites_no4 = TableauData(6)
                Rolequalites_no5 = TableauData(7)
                Rolequalites_no6 = TableauData(8)
                Rolequalites_no7 = TableauData(9)
                Rolequalites_no8 = TableauData(10)
                Rolequalites_no9 = TableauData(11)
                Rolequalites_no10 = TableauData(12)
                Rolequalites_no11 = TableauData(13)
                Rolequalites_no12 = TableauData(14)
                Rolequalites_no13 = TableauData(15)
                Rolequalites_no14 = TableauData(16)
                Rolequalites_no15 = TableauData(17)
                Rolequalites_no16 = TableauData(18)
                Rolequalites_no17 = TableauData(19)
                Rolequalites_no18 = TableauData(20)
                Competence_cle = TableauData(21)
                Contexte = TableauData(22)
                Contribution = TableauData(23)
                Qualites = TableauData(24)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

