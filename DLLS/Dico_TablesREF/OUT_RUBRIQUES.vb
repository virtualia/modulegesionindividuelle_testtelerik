﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class OUT_RUBRIQUES
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsCodeRubrique As String
        Private WsIntitule As String
        Private WsCorrespondance As String
        Private WsRegroupement As String
        Private WsImputation As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "OUT_RUBRIQUES"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueInterface
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property CodeRubrique() As String
            Get
                Return WsCodeRubrique
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsCodeRubrique = value
                    Case Else
                        WsCodeRubrique = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Property Correspondance() As String
            Get
                Return WsCorrespondance
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsCorrespondance = value
                    Case Else
                        WsCorrespondance = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Regroupement() As String
            Get
                Return WsRegroupement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsRegroupement = value
                    Case Else
                        WsRegroupement = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Imputation() As String
            Get
                Return WsImputation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsImputation = value
                    Case Else
                        WsImputation = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(CodeRubrique & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Correspondance & VI.Tild)
                Chaine.Append(Regroupement & VI.Tild)
                Chaine.Append(Imputation)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 6 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                CodeRubrique = TableauData(1)
                Intitule = TableauData(2)
                Correspondance = TableauData(3)
                Regroupement = TableauData(4)
                Imputation = TableauData(5)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
