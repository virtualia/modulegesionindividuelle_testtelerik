﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class GRD_GRILLE_NNE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsRang As Integer
        Private WsSpecificite As String 'E=Echelon, 'S Special
        Private WsIndiceMajore As Integer
        Private WsLettreChevron As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "GRD_GRILLE_NNE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueGrilles
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Rang As Integer
            Get
                Return WsRang
            End Get
            Set(ByVal value As Integer)
                WsRang = value
                MyBase.Clef = value.ToString
            End Set
        End Property

        Public Property Specificite() As String
            Get
                Return WsSpecificite
            End Get
            Set(ByVal value As String)
                WsSpecificite = F_FormatAlpha(value, 1)
            End Set
        End Property

        Public Property IndiceMajore As Integer
            Get
                Return WsIndiceMajore
            End Get
            Set(ByVal value As Integer)
                WsIndiceMajore = value
            End Set
        End Property

        Public Property Lettre_Chevron() As String
            Get
                Return WsLettreChevron
            End Get
            Set(ByVal value As String)
                WsLettreChevron = F_FormatAlpha(value, 2)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Rang.ToString & VI.Tild)
                Chaine.Append(Specificite & VI.Tild)
                Chaine.Append(IndiceMajore.ToString & VI.Tild)
                Chaine.Append(Lettre_Chevron)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 6 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Rang = CInt(F_FormatNumerique(TableauData(2)))
                Specificite = TableauData(3)
                IndiceMajore = CInt(F_FormatNumerique(TableauData(4)))
                Lettre_Chevron = TableauData(5)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_ChaineAComparer As String
            Get
                Dim Chaine As StringBuilder = New StringBuilder()
                Chaine.Append(Rang.ToString & VI.Tild)
                Chaine.Append(Specificite & VI.Tild)
                Chaine.Append(IndiceMajore.ToString & VI.Tild)
                Chaine.Append(Lettre_Chevron)

                Return Chaine.ToString()
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
