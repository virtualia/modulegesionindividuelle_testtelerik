﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class MET_DIPLOME
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsDiplomeobligatoire_no1 As String
        Private WsDiplomeobligatoire_no2 As String
        Private WsDiplomeobligatoire_no3 As String
        Private WsPermisobligatoire As String
        Private WsSalairerecrutement As Double = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "MET_DIPLOME"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueMetier
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 5
            End Get
        End Property

        Public Property Diplomeobligatoire_no1() As String
            Get
                Return WsDiplomeobligatoire_no1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsDiplomeobligatoire_no1 = value
                    Case Else
                        WsDiplomeobligatoire_no1 = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Diplomeobligatoire_no2() As String
            Get
                Return WsDiplomeobligatoire_no2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsDiplomeobligatoire_no2 = value
                    Case Else
                        WsDiplomeobligatoire_no2 = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Diplomeobligatoire_no3() As String
            Get
                Return WsDiplomeobligatoire_no3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsDiplomeobligatoire_no3 = value
                    Case Else
                        WsDiplomeobligatoire_no3 = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Permisobligatoire() As String
            Get
                Return WsPermisobligatoire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsPermisobligatoire = value
                    Case Else
                        WsPermisobligatoire = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Salairerecrutement() As Double
            Get
                Return WsSalairerecrutement
            End Get
            Set(ByVal value As Double)
                WsSalairerecrutement = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Diplomeobligatoire_no1 & VI.Tild)
                Chaine.Append(Diplomeobligatoire_no2 & VI.Tild)
                Chaine.Append(Diplomeobligatoire_no3 & VI.Tild)
                Chaine.Append(Permisobligatoire & VI.Tild)
                Chaine.Append(Salairerecrutement.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 6 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Diplomeobligatoire_no1 = TableauData(1)
                Diplomeobligatoire_no2 = TableauData(2)
                Diplomeobligatoire_no3 = TableauData(3)
                Permisobligatoire = TableauData(4)
                If TableauData(5) <> "" Then Salairerecrutement = VirRhFonction.ConversionDouble(TableauData(5))

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

