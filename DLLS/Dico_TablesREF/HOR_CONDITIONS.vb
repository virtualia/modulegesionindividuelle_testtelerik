﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class HOR_CONDITIONS
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsNatureCondtion As String
        Private WsCodeCondition As String
        '
        Private WsNumObjet As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "HOR_CONDITIONS"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueValTemps
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return WsNumObjet '3 = Présences, 4 = Absences
            End Get
        End Property

        Public Property Nature_Condition() As String
            Get
                Return WsNatureCondtion
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsNatureCondtion = value
                    Case Else
                        WsNatureCondtion = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Code_Condition() As String
            Get
                Return WsCodeCondition
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsCodeCondition = value
                    Case Else
                        WsCodeCondition = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Nature_Condition & VI.Tild)
                Chaine.Append(Code_Condition)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 3 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Nature_Condition = TableauData(1)
                Code_Condition = TableauData(2)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
       
        Public Sub New(ByVal NumObjet As Integer)
            MyBase.New()
            WsNumObjet = NumObjet
        End Sub

    End Class

    Public Class HOR_CONDITIONS_PRESENCE
        Inherits HOR_CONDITIONS
        Public Sub New()
            MyBase.New(Integer.Parse("3"))

        End Sub
    End Class

    Public Class HOR_CONDITIONS_ABSENCE
        Inherits HOR_CONDITIONS
        Public Sub New()
            MyBase.New(Integer.Parse("4"))

        End Sub
    End Class

End Namespace
