﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAI_SOLIDARITE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsTaux As Double = 0
        Private WsPlancher As Integer = 0
        Private WsPlafonds As Double = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAI_SOLIDARITE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePaie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 11
            End Get
        End Property

        Public Property Taux() As Double
            Get
                Return WsTaux
            End Get
            Set(ByVal value As Double)
                WsTaux = value
            End Set
        End Property

        Public Property Plancher() As Integer
            Get
                Return WsPlancher
            End Get
            Set(ByVal value As Integer)
                WsPlancher = value
            End Set
        End Property

        Public Property Plafonds() As Double
            Get
                Return WsPlafonds
            End Get
            Set(ByVal value As Double)
                WsPlafonds = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Taux.ToString & VI.Tild)
                Chaine.Append(Plancher.ToString & VI.Tild)
                Chaine.Append(Plafonds.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 5 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then Taux = VirRhFonction.ConversionDouble(TableauData(2))
                If TableauData(3) <> "" Then Plancher = CInt(TableauData(3))
                If TableauData(4) <> "" Then Plafonds = VirRhFonction.ConversionDouble(TableauData(4))

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
