﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class HOR_REGLE
        Inherits VIR_FICHE

        Private WsLstVariables As List(Of HOR_VARIABLES)
        Private WsLstPresences As List(Of HOR_CONDITIONS)
        Private WsLstAbsences As List(Of HOR_CONDITIONS)
        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsCodePaie As String
        Private WsMnemonique As String
        Private WsPeriodicite As Integer
        Private WsVolumePeriodicite As Integer
        Private WsNbHeures_Minimum As String
        Private WsNbHeures_Maximum As String
        Private WsPlageHoraire_Minimum As String
        Private WsPlageHoraire_Maximum As String
        Private WsJoursFeries As String
        Private WsNbHeures_A_Effectuer As String
        Private WsNbRealisations As Integer
        Private WsCoefficient As Double = 0
        Private WsForfaitHeures As String
        Private WsUniteResultat As Integer
        Private WsInfluenceResultat As Integer
        Private WsObservations As String
        Private WsSiInfluenceSurPrevu As String
        Private WsSiEstDivisible As String = "Non"

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "HOR_REGLE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueValTemps
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Code_Paie() As String
            Get
                Return WsCodePaie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsCodePaie = value
                    Case Else
                        WsCodePaie = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Mnemonique() As String
            Get
                Return WsMnemonique
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsMnemonique = value
                    Case Else
                        WsMnemonique = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Periodicite() As Integer
            Get
                Return WsPeriodicite
            End Get
            Set(ByVal value As Integer)
                WsPeriodicite = value
            End Set
        End Property

        Public Property Volume_Periodicite() As Integer
            Get
                Return WsVolumePeriodicite
            End Get
            Set(ByVal value As Integer)
                WsVolumePeriodicite = value
            End Set
        End Property

        Public Property NombreHeures_Minimum() As String
            Get
                Return WsNbHeures_Minimum
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsNbHeures_Minimum = value
                    Case Else
                        WsNbHeures_Minimum = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property NombreHeures_Maximum() As String
            Get
                Return WsNbHeures_Maximum
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsNbHeures_Maximum = value
                    Case Else
                        WsNbHeures_Maximum = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property PlageHoraire_Minimum() As String
            Get
                Return WsPlageHoraire_Minimum
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsPlageHoraire_Minimum = value
                    Case Else
                        WsPlageHoraire_Minimum = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property PlageHoraire_Maximum() As String
            Get
                Return WsPlageHoraire_Maximum
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsPlageHoraire_Maximum = value
                    Case Else
                        WsPlageHoraire_Maximum = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property JoursFeries() As String
            Get
                Return WsJoursFeries
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsJoursFeries = value
                    Case Else
                        WsJoursFeries = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property NombreHeures_A_Effectuer() As String
            Get
                Return WsNbHeures_A_Effectuer
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsNbHeures_A_Effectuer = value
                    Case Else
                        WsNbHeures_A_Effectuer = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property NombredeRealisations() As Integer
            Get
                Return WsNbRealisations
            End Get
            Set(ByVal value As Integer)
                WsNbRealisations = value
            End Set
        End Property

        Public Property Coefficient() As Double
            Get
                Return WsCoefficient
            End Get
            Set(ByVal value As Double)
                WsCoefficient = value
            End Set
        End Property

        Public Property Forfait_Heures() As String
            Get
                Return WsForfaitHeures
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsForfaitHeures = value
                    Case Else
                        WsForfaitHeures = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property UniteduResultat() As Integer
            Get
                Return WsUniteResultat
            End Get
            Set(ByVal value As Integer)
                WsUniteResultat = value
            End Set
        End Property

        Public Property InfluenceduResultat() As Integer
            Get
                Return WsInfluenceResultat
            End Get
            Set(ByVal value As Integer)
                WsInfluenceResultat = value
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 800
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 800)
                End Select
            End Set
        End Property

        Public Property SiInfluencePrevu() As String
            Get
                Return WsSiInfluenceSurPrevu
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiInfluenceSurPrevu = value
                    Case Else
                        WsSiInfluenceSurPrevu = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property SiEstDivisible() As String
            Get
                Return WsSiEstDivisible
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiEstDivisible = value
                    Case Else
                        WsSiEstDivisible = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Code_Paie & VI.Tild)
                Chaine.Append(Mnemonique & VI.Tild)
                Chaine.Append(Periodicite.ToString & VI.Tild)
                Chaine.Append(Volume_Periodicite.ToString & VI.Tild)
                Chaine.Append(NombreHeures_Minimum & VI.Tild)
                Chaine.Append(NombreHeures_Maximum & VI.Tild)
                Chaine.Append(PlageHoraire_Minimum & VI.Tild)
                Chaine.Append(PlageHoraire_Maximum & VI.Tild)
                Chaine.Append(JoursFeries & VI.Tild)
                Chaine.Append(NombreHeures_A_Effectuer & VI.Tild)
                Chaine.Append(NombredeRealisations.ToString & VI.Tild)
                Chaine.Append(Coefficient.ToString & VI.Tild)
                Chaine.Append(Forfait_Heures & VI.Tild)
                Chaine.Append(UniteduResultat.ToString & VI.Tild)
                Chaine.Append(InfluenceduResultat.ToString & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(SiInfluencePrevu & VI.Tild)
                Chaine.Append(SiEstDivisible)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 19 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                Code_Paie = TableauData(2)
                Mnemonique = TableauData(3)
                If TableauData(4) = "" Then TableauData(4) = "0"
                Periodicite = CInt(TableauData(4))
                If TableauData(5) = "" Then TableauData(5) = "0"
                Volume_Periodicite = CInt(TableauData(5))
                NombreHeures_Minimum = TableauData(6)
                NombreHeures_Maximum = TableauData(7)
                PlageHoraire_Minimum = TableauData(8)
                PlageHoraire_Maximum = TableauData(9)
                JoursFeries = TableauData(10)
                NombreHeures_A_Effectuer = TableauData(11)
                If TableauData(12) = "" Then TableauData(12) = "0"
                NombredeRealisations = CInt(TableauData(12))
                If TableauData(13) <> "" Then Coefficient = VirRhFonction.ConversionDouble(TableauData(13))
                Forfait_Heures = TableauData(14)
                If TableauData(15) = "" Then TableauData(15) = "0"
                UniteduResultat = CInt(TableauData(15))
                If TableauData(16) = "" Then TableauData(16) = "0"
                InfluenceduResultat = CInt(TableauData(16))
                Observations = TableauData(17)
                SiInfluencePrevu = TableauData(18)
                If TableauData.Count > 19 Then
                    SiEstDivisible = TableauData(19)
                End If

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public ReadOnly Property SiSamedi() As Boolean
            Get
                Select Case JoursFeries.Length
                    Case Is = 3
                        If Strings.Left(JoursFeries, 1) = "1" Then
                            Return True
                        End If
                End Select
                Return False
            End Get
        End Property

        Public ReadOnly Property SiDimanche() As Boolean
            Get
                Select Case JoursFeries.Length
                    Case Is = 3
                        If Strings.Mid(JoursFeries, 2, 1) = "1" Then
                            Return True
                        End If
                End Select
                Return False
            End Get
        End Property

        Public ReadOnly Property SiJourFerie() As Boolean
            Get
                Select Case JoursFeries.Length
                    Case Is = 3
                        If Strings.Right(JoursFeries, 1) = "1" Then
                            Return True
                        End If
                End Select
                Return False
            End Get
        End Property

        Public ReadOnly Property PlageHoraireMini_Minutes() As Integer
            Get
                Dim Duree As String
                If PlageHoraire_Minimum <> "" Then
                    Duree = VirRhDate.DureeHeure("00:00", PlageHoraire_Minimum)
                    Return Val(VirRhDate.CalcHeure(Duree, "", 1))
                Else
                    Return 0
                End If
            End Get
        End Property

        Public ReadOnly Property PlageHoraireMaxi_Minutes() As Integer
            Get
                Dim Duree As String
                If PlageHoraire_Maximum <> "" Then
                    Duree = VirRhDate.DureeHeure("00:00", PlageHoraire_Maximum)
                    Return Val(VirRhDate.CalcHeure(Duree, "", 1))
                Else
                    Return 0
                End If
            End Get
        End Property

        Public ReadOnly Property NombreHeuresMinimum_Minutes() As Integer
            Get
                If NombreHeures_Minimum <> "" Then
                    Return Val(VirRhDate.CalcHeure(NombreHeures_Minimum, "", 1))
                Else
                    Return 0
                End If
            End Get
        End Property

        Public ReadOnly Property NombreHeuresMaximum_Minutes() As Integer
            Get
                If NombreHeures_Maximum <> "" Then
                    Return Val(VirRhDate.CalcHeure(NombreHeures_Maximum, "", 1))
                Else
                    Return 0
                End If
            End Get
        End Property

        Public ReadOnly Property NombreHeuresAEffectuer_Minutes() As Integer
            Get
                If NombreHeures_A_Effectuer <> "" Then
                    Return Val(VirRhDate.CalcHeure(NombreHeures_A_Effectuer, "", 1))
                Else
                    Return 0
                End If
            End Get
        End Property

        Public ReadOnly Property ForfaitHeures_Minutes() As Integer
            Get
                If Forfait_Heures <> "" Then
                    Return Val(VirRhDate.CalcHeure(Forfait_Heures, "", 1))
                Else
                    Return 0
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesVariables() As List(Of HOR_VARIABLES)
            Get
                If WsLstVariables Is Nothing Then
                    Return Nothing
                End If
                Return WsLstVariables.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Variable(ByVal Fiche As HOR_VARIABLES) As Integer

            If WsLstVariables Is Nothing Then
                WsLstVariables = New List(Of HOR_VARIABLES)
            End If
            WsLstVariables.Add(Fiche)
            Return WsLstVariables.Count

        End Function

        Public Function Fiche_Variable(ByVal DateValeur As String) As HOR_VARIABLES

            If WsLstVariables Is Nothing Then
                Return Nothing
            End If

            Return (From it As HOR_VARIABLES In ListedesVariables Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_ConditionPresence(ByVal Fiche As HOR_CONDITIONS) As Integer

            If WsLstPresences Is Nothing Then
                WsLstPresences = New List(Of HOR_CONDITIONS)
            End If
            WsLstPresences.Add(Fiche)
            Return WsLstPresences.Count

        End Function

        Public Function Ajouter_ConditionAbsence(ByVal Fiche As HOR_CONDITIONS) As Integer

            If WsLstAbsences Is Nothing Then
                WsLstAbsences = New List(Of HOR_CONDITIONS)
            End If
            WsLstAbsences.Add(Fiche)
            Return WsLstAbsences.Count

        End Function

        Public Function ListedesConditions(ByVal NumObjet As Integer) As List(Of HOR_CONDITIONS)

            Select Case NumObjet
                Case 3
                    If WsLstPresences Is Nothing Then
                        Return Nothing
                    End If
                    Return WsLstPresences.OrderBy(Function(m) m.Nature_Condition).ToList
                Case 4
                    If WsLstAbsences Is Nothing Then
                        Return Nothing
                    End If
                    Return WsLstAbsences.OrderBy(Function(m) m.Nature_Condition).ToList
                Case Else
                    Return Nothing
            End Select

        End Function

        Public Function Fiche_Condition(ByVal NumObjet As Integer, ByVal Intitule As String) As HOR_CONDITIONS

            Dim LstTriee As List(Of HOR_CONDITIONS) = ListedesConditions(NumObjet)
            If LstTriee Is Nothing Then
                Return Nothing
            End If

            Return (From it As HOR_CONDITIONS In LstTriee Where it.Nature_Condition = Intitule Select it).FirstOrDefault()

        End Function

        Public Function Fiche_Condition(ByVal SiPresence As Boolean, ByVal Index As Integer) As HOR_CONDITIONS
            Dim lsttmp As List(Of HOR_CONDITIONS) = Nothing
            If SiPresence = True Then
                lsttmp = WsLstPresences
            Else
                lsttmp = WsLstAbsences
            End If
            If (lsttmp Is Nothing) Then
                Return Nothing
            End If
            If (Index < 0 Or Index >= lsttmp.Count) Then
                Return Nothing
            End If
            Return lsttmp(Index)
        End Function

        Public Function SiConditionPresente(ByVal SiPresence As Boolean, ByVal IntituleItem As String) As Boolean

            Dim IndiceV As Integer = 0
            Do
                If Fiche_Condition(SiPresence, IndiceV) Is Nothing Then
                    Return False
                End If
                If Fiche_Condition(SiPresence, IndiceV).Nature_Condition = IntituleItem Then
                    Return True
                End If
                IndiceV += 1
            Loop
            Return False
        End Function
    End Class
End Namespace


