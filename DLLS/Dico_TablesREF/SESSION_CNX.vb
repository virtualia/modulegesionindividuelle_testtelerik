﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class SESSION_CNX
        Inherits VIR_FICHE
        Private WsFicheLue As StringBuilder
        '
        Private WsNumeroOrdre As Integer
        Private WsIdentification As String
        Private WsNom As String
        Private WsPrenom As String
        Private WsEmail As String
        Private WsModeCnx As String
        Private WsMachine As String
        Private WsAdresseIP As String
        Private WsDomaine As String
        Private WsDateConnexion As String
        Private WsHeureDeConnexion As String
        Private WsHeureFinConnexion As String
        Private WsBaseCourante As Integer
        Private WsFiltreEtablissement As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "SESSION_CNX"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueSession
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Numero_Ordre() As Integer
            Get
                Return WsNumeroOrdre
            End Get
            Set(ByVal value As Integer)
                WsNumeroOrdre = value
            End Set
        End Property

        Public Property Identification() As String
            Get
                Return WsIdentification
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 64
                        WsIdentification = value
                    Case Else
                        WsIdentification = Strings.Left(value, 64)
                End Select
            End Set
        End Property

        Public Property Nom() As String
            Get
                Return WsNom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsNom = value
                    Case Else
                        WsNom = Strings.Left(value, 35)
                End Select
            End Set
        End Property

        Public Property Prenom() As String
            Get
                Return WsPrenom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsPrenom = value
                    Case Else
                        WsPrenom = Strings.Left(value, 35)
                End Select
            End Set
        End Property

        Public Property Email() As String
            Get
                Return WsEmail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsEmail = value
                    Case Else
                        WsEmail = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Mode_Connexion() As String
            Get
                Return WsModeCnx
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsModeCnx = value
                    Case Else
                        WsModeCnx = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Machine() As String
            Get
                Return WsMachine
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 32
                        WsMachine = value
                    Case Else
                        WsMachine = Strings.Left(value, 32)
                End Select
            End Set
        End Property

        Public Property AdresseIP() As String
            Get
                Return WsAdresseIP
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 32
                        WsAdresseIP = value
                    Case Else
                        WsAdresseIP = Strings.Left(value, 32)
                End Select
            End Set
        End Property

        Public Property Domaine() As String
            Get
                Return WsDomaine
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 64
                        WsDomaine = value
                    Case Else
                        WsDomaine = Strings.Left(value, 64)
                End Select
            End Set
        End Property

        Public Property Date_Connexion() As String
            Get
                Return WsDateConnexion
            End Get
            Set(ByVal value As String)
                WsDateConnexion = VirRhDate.DateStandardVirtualia(value)
                If MyBase.Ide_Dossier = 0 Then
                    MyBase.Ide_Dossier = CInt(Strings.Right(WsDateConnexion, 4) & Strings.Mid(WsDateConnexion, 4, 2) & Strings.Left(WsDateConnexion, 2))
                End If
            End Set
        End Property

        Public Property HeureDebut_Connexion() As String
            Get
                Return WsHeureDeConnexion
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHeureDeConnexion = value
                    Case Else
                        WsHeureDeConnexion = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property HeureFin_Connexion() As String
            Get
                Return WsHeureFinConnexion
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHeureFinConnexion = value
                    Case Else
                        WsHeureFinConnexion = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property DatabaseCourante() As Integer
            Get
                Return WsBaseCourante
            End Get
            Set(ByVal value As Integer)
                WsBaseCourante = value
            End Set
        End Property

        Public Property FiltreEtablissement() As String
            Get
                Return WsFiltreEtablissement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsFiltreEtablissement = value
                    Case Else
                        WsFiltreEtablissement = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Numero_Ordre.ToString & VI.Tild)
                Chaine.Append(Identification & VI.Tild)
                Chaine.Append(Nom & VI.Tild)
                Chaine.Append(Prenom & VI.Tild)
                Chaine.Append(Email & VI.Tild)
                Chaine.Append(Mode_Connexion & VI.Tild)
                Chaine.Append(Machine & VI.Tild)
                Chaine.Append(AdresseIP & VI.Tild)
                Chaine.Append(Domaine & VI.Tild)
                Chaine.Append(Date_Connexion & VI.Tild)
                Chaine.Append(HeureDebut_Connexion & VI.Tild)
                Chaine.Append(HeureFin_Connexion & VI.Tild)
                Chaine.Append(DatabaseCourante.ToString & VI.Tild)
                Chaine.Append(FiltreEtablissement)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 15 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If IsNumeric(TableauData(1)) Then
                    Numero_Ordre = CInt(TableauData(1))
                Else
                    Numero_Ordre = 1
                End If
                Identification = TableauData(2)
                Nom = TableauData(3)
                Prenom = TableauData(4)
                Email = TableauData(5)
                Mode_Connexion = TableauData(6)
                Machine = TableauData(7)
                AdresseIP = TableauData(8)
                Domaine = TableauData(9)
                Date_Connexion = TableauData(10)
                HeureDebut_Connexion = TableauData(11)
                HeureFin_Connexion = TableauData(12)
                If TableauData(13) = "" Then
                    TableauData(13) = "0"
                End If
                DatabaseCourante = CInt(TableauData(13))
                FiltreEtablissement = TableauData(14)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace