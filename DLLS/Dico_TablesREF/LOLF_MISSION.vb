﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class LOLF_MISSION
        Inherits VIR_FICHE

        Private WsLstProgrammes As List(Of LOLF_PROGRAMME)
        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsDateCreation As String
        Private WsDescriptif As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "LOLF_MISSION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueLolf
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property DatedeCreation() As String
            Get
                Return WsDateCreation
            End Get
            Set(ByVal value As String)
                WsDateCreation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsDescriptif = value
                    Case Else
                        WsDescriptif = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(DatedeCreation & VI.Tild)
                Chaine.Append(Descriptif)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                DatedeCreation = TableauData(2)
                Descriptif = TableauData(3)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Fiche_Programme(ByVal Intitule As String) As LOLF_PROGRAMME

            If WsLstProgrammes Is Nothing Then
                Return Nothing
            End If

            Return (From it As LOLF_PROGRAMME In WsLstProgrammes Where it.Intitule = Intitule Select it).FirstOrDefault()

        End Function

        Public Function Fiche_Programme(ByVal Numero As Integer) As LOLF_PROGRAMME

            If WsLstProgrammes Is Nothing Then
                Return Nothing
            End If

            Return (From it As LOLF_PROGRAMME In WsLstProgrammes Where it.Numero = Numero Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_Programme(ByVal FichePgm As LOLF_PROGRAMME) As Integer

            If WsLstProgrammes Is Nothing Then
                WsLstProgrammes = New List(Of LOLF_PROGRAMME)
            End If
            WsLstProgrammes.Add(FichePgm)
            Return WsLstProgrammes.Count

        End Function

        Public Function ListedesProgrammes(ByVal CritereTri As String) As List(Of LOLF_PROGRAMME)

            If WsLstProgrammes Is Nothing Then
                Return Nothing
            End If
            Select Case CritereTri
                Case Is = "Intitulé"
                    Return WsLstProgrammes.OrderBy(Function(m) m.Intitule).ToList
                Case Is = "Numero"
                    Return WsLstProgrammes.OrderBy(Function(m) m.Numero).ToList
                Case Else
                    Return WsLstProgrammes
            End Select

        End Function


    End Class
End Namespace

