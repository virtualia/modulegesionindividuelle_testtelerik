﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ETA_IDENTITE
        Inherits VIR_FICHE

        Private WsLstHoraires As List(Of ETA_HORAIRE)
        Private WsLstAccords As List(Of ACCORD_RTT)
        Private WsLstReglesValo As List(Of ETA_REGLE_VALTEMPS)
        Private WsLstReglesConge As List(Of DROITS_CONGE)
        Private WsLstReglesDC As List(Of ETA_DEBIT_CREDIT)
        Private WsLstStructures As List(Of ETA_STRUCTURE)
        Private WsLstParametres As List(Of ETA_PARAMETRES)
        Private WsLstReglesEffectif As List(Of ETA_REGLE_EFFECTIF)
        Private WsLstMandatements As List(Of ETA_MANDATEMENT)
        Private WsLstComptesRendu As List(Of ETA_COMPTE_RENDU)
        Private WsLstCodeFonctions As List(Of ETA_FONCTIONS)
        Private WsLstFichesNavette As List(Of ETA_FICHE_NAVETTE)
        Private WsLstSelfAbsence As List(Of ETA_INTRANET_ABSENCE)

        Private WsFicheLue As StringBuilder
        '
        Private WsNom As String
        Private WsType As String
        Private WsNumeroetRue As String
        Private WsCodePostal As String
        Private WsBureauDistributeur As String
        Private WsNomResponsable As String
        Private WsFonctionResponsable As String
        Private WsTelephoneResponsable As String
        Private WsFaxResponsable As String
        Private WsImmatriculation As String
        Private WsRegion As String
        Private WsDepartement As String
        Private WsArrondissement As String
        Private WsCanton As String
        Private WsPopulation As String
        Private WsSuperficie As String
        Private WsEffectif As String
        Private WsCodePays As String
        Private WsEMailResponsable As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ETA_IDENTITE" 'Secteur Public
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueEtablissement
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Denomination() As String
            Get
                Return WsNom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNom = value
                    Case Else
                        WsNom = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property TypeEtablissement() As String
            Get
                Return WsType
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsType = value
                    Case Else
                        WsType = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Numero_et_NomdelaRue() As String
            Get
                Return WsNumeroetRue
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsNumeroetRue = value
                    Case Else
                        WsNumeroetRue = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property Code_Postal() As String
            Get
                Return WsCodePostal
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsCodePostal = value
                    Case Else
                        WsCodePostal = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property BureauDistributeur() As String
            Get
                Return WsBureauDistributeur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsBureauDistributeur = value
                    Case Else
                        WsBureauDistributeur = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Responsable_Nom() As String
            Get
                Return WsNomResponsable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsNomResponsable = value
                    Case Else
                        WsNomResponsable = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property Responsable_Fonction() As String
            Get
                Return WsFonctionResponsable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsFonctionResponsable = value
                    Case Else
                        WsFonctionResponsable = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Responsable_Telephone() As String
            Get
                Return WsTelephoneResponsable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsTelephoneResponsable = value
                    Case Else
                        WsTelephoneResponsable = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Responsable_Fax() As String
            Get
                Return WsFaxResponsable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsFaxResponsable = value
                    Case Else
                        WsFaxResponsable = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Immatriculation() As String
            Get
                Return WsImmatriculation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsImmatriculation = value
                    Case Else
                        WsImmatriculation = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Region() As String
            Get
                Return WsRegion
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsRegion = value
                    Case Else
                        WsRegion = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Departement() As String
            Get
                Return WsDepartement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsDepartement = value
                    Case Else
                        WsDepartement = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Arrondisement() As String
            Get
                Return WsArrondissement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsArrondissement = value
                    Case Else
                        WsArrondissement = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Canton() As String
            Get
                Return WsCanton
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsCanton = value
                    Case Else
                        WsCanton = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Population() As String
            Get
                Return WsPopulation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsPopulation = value
                    Case Else
                        WsPopulation = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Superficie() As String
            Get
                Return WsSuperficie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 12
                        WsSuperficie = value
                    Case Else
                        WsSuperficie = Strings.Left(value, 12)
                End Select
            End Set
        End Property

        Public Property Tranche_Effectif() As String
            Get
                Return WsEffectif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsEffectif = value
                    Case Else
                        WsEffectif = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Code_Pays() As String
            Get
                Return WsCodePays
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsCodePays = value
                    Case Else
                        WsCodePays = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Responsable_EMail() As String
            Get
                Return WsEMailResponsable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 150
                        WsEMailResponsable = value
                    Case Else
                        WsEMailResponsable = Strings.Left(value, 150)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Denomination & VI.Tild)
                Chaine.Append(TypeEtablissement & VI.Tild)
                Chaine.Append(Numero_et_NomdelaRue & VI.Tild)
                Chaine.Append(Code_Postal & VI.Tild)
                Chaine.Append(BureauDistributeur & VI.Tild)
                Chaine.Append(Responsable_Nom & VI.Tild)
                Chaine.Append(Responsable_Fonction & VI.Tild)
                Chaine.Append(Responsable_Telephone & VI.Tild)
                Chaine.Append(Responsable_Fax & VI.Tild)
                Chaine.Append(Immatriculation & VI.Tild)
                Chaine.Append(Region & VI.Tild)
                Chaine.Append(Departement & VI.Tild)
                Chaine.Append(Arrondisement & VI.Tild)
                Chaine.Append(Canton & VI.Tild)
                Chaine.Append(Population & VI.Tild)
                Chaine.Append(Superficie & VI.Tild)
                Chaine.Append(Tranche_Effectif & VI.Tild)
                Chaine.Append(Code_Pays & VI.Tild)
                Chaine.Append(Responsable_EMail)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 19 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Denomination = TableauData(1)
                TypeEtablissement = TableauData(2)
                Numero_et_NomdelaRue = TableauData(3)
                Code_Postal = TableauData(4)
                BureauDistributeur = TableauData(5)
                Responsable_Nom = TableauData(6)
                Responsable_Fonction = TableauData(7)
                Responsable_Telephone = TableauData(8)
                Responsable_Fax = TableauData(9)
                Immatriculation = TableauData(10)
                Region = TableauData(11)
                Departement = TableauData(12)
                Arrondisement = TableauData(13)
                Canton = TableauData(14)
                Population = TableauData(15)
                Superficie = TableauData(16)
                Tranche_Effectif = TableauData(17)
                Code_Pays = TableauData(18)
                If TableauData.Count > 19 Then
                    Responsable_EMail = TableauData(19)
                End If
                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesHoraires() As List(Of ETA_HORAIRE)
            Get
                If WsLstHoraires Is Nothing Then
                    Return Nothing
                End If
                Return WsLstHoraires.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesAccords() As List(Of ACCORD_RTT)
            Get
                If WsLstAccords Is Nothing Then
                    Return Nothing
                End If
                Return WsLstAccords.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesReglesDC() As List(Of ETA_DEBIT_CREDIT)
            Get
                If WsLstReglesDC Is Nothing Then
                    Return Nothing
                End If
                Return WsLstReglesDC.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesReglesValo() As List(Of ETA_REGLE_VALTEMPS)
            Get
                If WsLstReglesValo Is Nothing Then
                    Return Nothing
                End If
                Return WsLstReglesValo.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesReglesConge() As List(Of DROITS_CONGE)
            Get
                If WsLstReglesConge Is Nothing Then
                    Return Nothing
                End If
                Return WsLstReglesConge.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesStructures() As List(Of ETA_STRUCTURE)
            Get
                If WsLstStructures Is Nothing Then
                    Return Nothing
                End If
                Return WsLstStructures.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesParametres() As List(Of ETA_PARAMETRES)
            Get
                If WsLstParametres Is Nothing Then
                    Return Nothing
                End If
                Return WsLstParametres.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesRegleEffectifs() As List(Of ETA_REGLE_EFFECTIF)
            Get
                If WsLstReglesEffectif Is Nothing Then
                    Return Nothing
                End If
                Return WsLstReglesEffectif.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesMandatements() As List(Of ETA_MANDATEMENT)
            Get
                If WsLstMandatements Is Nothing Then
                    Return Nothing
                End If
                Return WsLstMandatements.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesCompteRendu() As List(Of ETA_COMPTE_RENDU)
            Get
                If WsLstComptesRendu Is Nothing Then
                    Return Nothing
                End If
                Return WsLstComptesRendu.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesCodeFonction() As List(Of ETA_FONCTIONS)
            Get
                If WsLstCodeFonctions Is Nothing Then
                    Return Nothing
                End If
                Return WsLstCodeFonctions.OrderBy(Function(m) m.Code).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesFichesNavette() As List(Of ETA_FICHE_NAVETTE)
            Get
                If WsLstFichesNavette Is Nothing Then
                    Return Nothing
                End If
                Return (From instance In WsLstFichesNavette Select instance Order By instance.Date_Valeur_ToDate Ascending, instance.Rang Ascending).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesNaturesAbsence() As List(Of ETA_INTRANET_ABSENCE)
            Get
                If WsLstSelfAbsence Is Nothing Then
                    Return Nothing
                End If
                Return WsLstSelfAbsence.OrderBy(Function(m) m.Intitule).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Horaire(ByVal Fiche As ETA_HORAIRE) As Integer

            If WsLstHoraires Is Nothing Then
                WsLstHoraires = New List(Of ETA_HORAIRE)
            End If
            WsLstHoraires.Add(Fiche)
            Return WsLstHoraires.Count

        End Function

        Public Function Fiche_Horaire(ByVal DateValeur As String) As ETA_HORAIRE

            If WsLstHoraires Is Nothing Then
                Return Nothing
            End If

            Return (From eh As ETA_HORAIRE In ListedesHoraires Where EstPlusGrand_Ou_Egal(DateValeur, eh.Date_de_Valeur) Select eh).FirstOrDefault()

        End Function

        Public Function Ajouter_Accord(ByVal Fiche As ACCORD_RTT) As Integer

            If WsLstAccords Is Nothing Then
                WsLstAccords = New List(Of ACCORD_RTT)
            End If
            WsLstAccords.Add(Fiche)
            Return WsLstAccords.Count

        End Function

        Public Function Fiche_Accord(ByVal DateValeur As String) As ACCORD_RTT

            If WsLstAccords Is Nothing Then
                Return Nothing
            End If

            Return (From it As ACCORD_RTT In ListedesAccords Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_RegleDC(ByVal Fiche As ETA_DEBIT_CREDIT) As Integer

            If WsLstReglesDC Is Nothing Then
                WsLstReglesDC = New List(Of ETA_DEBIT_CREDIT)
            End If
            WsLstReglesDC.Add(Fiche)
            Return WsLstReglesDC.Count

        End Function

        Public Function Fiche_RegleDC(ByVal DateValeur As String) As ETA_DEBIT_CREDIT

            If WsLstReglesDC Is Nothing Then
                Return Nothing
            End If

            Return (From it As ETA_DEBIT_CREDIT In ListedesReglesDC _
                    Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) _
                    Select it).FirstOrDefault()
        End Function

        Public Function Ajouter_RegleValo(ByVal Fiche As ETA_REGLE_VALTEMPS) As Integer

            If WsLstReglesValo Is Nothing Then
                WsLstReglesValo = New List(Of ETA_REGLE_VALTEMPS)
            End If
            WsLstReglesValo.Add(Fiche)
            Return WsLstReglesValo.Count

        End Function

        Public Function Fiche_RegleValo(ByVal DateValeur As String, ByVal Index As Integer) As ETA_REGLE_VALTEMPS

            If WsLstReglesValo Is Nothing Then
                Return Nothing
            End If

            Dim I As Integer
            Dim IndiceV As Integer = 0
            Dim LstTriee As List(Of ETA_REGLE_VALTEMPS) = ListedesReglesValo

            For I = LstTriee.Count - 1 To 0 Step -1
                Select Case VirRhDate.ComparerDates(DateValeur, LstTriee.Item(I).Date_de_Valeur)
                    Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                        If LstTriee.Item(I).Date_de_Fin = "" Then
                            If Index = IndiceV Then
                                Return LstTriee.Item(I)
                            End If
                            IndiceV += 1
                        End If
                        Select Case VirRhDate.ComparerDates(DateValeur, LstTriee.Item(I).Date_de_Valeur)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                If Index = IndiceV Then
                                    Return LstTriee.Item(I)
                                End If
                                IndiceV += 1
                        End Select
                End Select
            Next I
            Return Nothing

        End Function

        Public Function Ajouter_RegleConge(ByVal Fiche As DROITS_CONGE) As Integer

            If WsLstReglesConge Is Nothing Then
                WsLstReglesConge = New List(Of DROITS_CONGE)
            End If
            WsLstReglesConge.Add(Fiche)
            Return WsLstReglesConge.Count

        End Function

        Public Function Fiche_RegleConge(ByVal DateValeur As String) As DROITS_CONGE

            If WsLstReglesConge Is Nothing Then
                Return Nothing
            End If

            Return (From it As DROITS_CONGE In ListedesReglesConge _
                Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) _
                Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_Structure(ByVal Fiche As ETA_STRUCTURE) As Integer

            If WsLstStructures Is Nothing Then
                WsLstStructures = New List(Of ETA_STRUCTURE)
            End If
            WsLstStructures.Add(Fiche)
            Return WsLstStructures.Count

        End Function

        Public Function Fiche_Structure(ByVal DateValeur As String) As ETA_STRUCTURE

            If WsLstStructures Is Nothing Then
                Return Nothing
            End If

            Return (From it As ETA_STRUCTURE In ListedesStructures _
                Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) _
                Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_Parametre(ByVal Fiche As ETA_PARAMETRES) As Integer

            If WsLstParametres Is Nothing Then
                WsLstParametres = New List(Of ETA_PARAMETRES)
            End If
            WsLstParametres.Add(Fiche)
            Return WsLstParametres.Count

        End Function

        Public Function Fiche_Parametre(ByVal DateValeur As String) As ETA_PARAMETRES

            If WsLstParametres Is Nothing Then
                Return Nothing
            End If

            Return (From it As ETA_PARAMETRES In ListedesParametres _
                Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) _
                Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_RegleEffectif(ByVal Fiche As ETA_REGLE_EFFECTIF) As Integer

            If WsLstReglesEffectif Is Nothing Then
                WsLstReglesEffectif = New List(Of ETA_REGLE_EFFECTIF)
            End If
            WsLstReglesEffectif.Add(Fiche)
            Return WsLstReglesEffectif.Count

        End Function

        Public Function Fiche_RegleEffectif(ByVal DateValeur As String) As ETA_REGLE_EFFECTIF

            If WsLstReglesEffectif Is Nothing Then
                Return Nothing
            End If

            Return (From it As ETA_REGLE_EFFECTIF In ListedesRegleEffectifs _
                Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) _
                Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_Mandatement(ByVal Fiche As ETA_MANDATEMENT) As Integer

            If WsLstMandatements Is Nothing Then
                WsLstMandatements = New List(Of ETA_MANDATEMENT)
            End If
            WsLstMandatements.Add(Fiche)
            Return WsLstMandatements.Count

        End Function

        Public Function Fiche_Mandatment(ByVal DateValeur As String, ByVal Numero As Integer) As ETA_MANDATEMENT

            If WsLstReglesEffectif Is Nothing Then
                Return Nothing
            End If

            Return (From it As ETA_MANDATEMENT In ListedesMandatements _
                Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) And it.Rang = Numero _
                Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_CompteRendu(ByVal Fiche As ETA_COMPTE_RENDU) As Integer

            If WsLstComptesRendu Is Nothing Then
                WsLstComptesRendu = New List(Of ETA_COMPTE_RENDU)
            End If
            WsLstComptesRendu.Add(Fiche)
            Return WsLstComptesRendu.Count

        End Function

        Public Function Fiche_CompteRendu(ByVal DateValeur As String, ByVal TypeCr As String) As ETA_COMPTE_RENDU

            If WsLstReglesEffectif Is Nothing Then
                Return Nothing
            End If

            Return (From it As ETA_COMPTE_RENDU In ListedesCompteRendu _
                    Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) And it.Type_CR = TypeCr _
                    Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_CodeFonction(ByVal Fiche As ETA_FONCTIONS) As Integer
            If WsLstCodeFonctions Is Nothing Then
                WsLstCodeFonctions = New List(Of ETA_FONCTIONS)
            End If
            WsLstCodeFonctions.Add(Fiche)
            Return WsLstCodeFonctions.Count

        End Function

        Public Function Fiche_CodeFonction(ByVal CodeF As String) As ETA_FONCTIONS
            If WsLstCodeFonctions Is Nothing Then
                Return Nothing
            End If
            Return (From it As ETA_FONCTIONS In ListedesCodeFonction Where it.Code = CodeF Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_FicheNavette(ByVal Fiche As ETA_FICHE_NAVETTE) As Integer
            If WsLstFichesNavette Is Nothing Then
                WsLstFichesNavette = New List(Of ETA_FICHE_NAVETTE)
            End If
            WsLstFichesNavette.Add(Fiche)
            Return WsLstFichesNavette.Count

        End Function

        Public Function Fiche_Navette(ByVal DateValeur As String, ByVal Rang As Integer) As ETA_FICHE_NAVETTE
            If WsLstFichesNavette Is Nothing Then
                Return Nothing
            End If
            Return (From it As ETA_FICHE_NAVETTE In WsLstFichesNavette _
                    Where it.Date_de_Valeur = DateValeur And it.Rang = Rang Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_NatureAbsenceSelf(ByVal Fiche As ETA_INTRANET_ABSENCE) As Integer
            If WsLstSelfAbsence Is Nothing Then
                WsLstSelfAbsence = New List(Of ETA_INTRANET_ABSENCE)
            End If
            WsLstSelfAbsence.Add(Fiche)
            Return WsLstSelfAbsence.Count

        End Function

        Public Function Fiche_AbsenceSelf(ByVal Intitule As String) As ETA_INTRANET_ABSENCE
            If WsLstSelfAbsence Is Nothing Then
                Return Nothing
            End If
            Return (From it As ETA_INTRANET_ABSENCE In WsLstSelfAbsence _
                    Where it.Intitule = Intitule Select it).FirstOrDefault()

        End Function

    End Class
End Namespace

