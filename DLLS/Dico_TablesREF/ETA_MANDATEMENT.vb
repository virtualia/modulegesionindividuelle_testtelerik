﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ETA_MANDATEMENT
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsRang As Integer
        Private WsLibelle As String
        Private WsImputation As String
        Private WsCodeFonction As String
        Private WsCodeBudget As String
        Private WsMontant As Double

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ETA_MANDATEMENT"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueEtablissement
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 11
            End Get
        End Property

        Public Property Rang() As Integer
            Get
                Return WsRang
            End Get
            Set(ByVal value As Integer)
                WsRang = value
                MyBase.Clef = value
            End Set
        End Property

        Public Property Libelle() As String
            Get
                Return WsLibelle
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsLibelle = value
                    Case Else
                        WsLibelle = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Imputation() As String
            Get
                Return WsImputation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsImputation = value
                    Case Else
                        WsImputation = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Code_Fonction() As String
            Get
                Return WsCodeFonction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsCodeFonction = value
                    Case Else
                        WsCodeFonction = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Code_Budget() As String
            Get
                Return WsCodeBudget
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsCodeBudget = value
                    Case Else
                        WsCodeBudget = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Montant() As Double
            Get
                Return WsMontant
            End Get
            Set(ByVal value As Double)
                WsMontant = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Rang.ToString & VI.Tild)
                Chaine.Append(Libelle & VI.Tild)
                Chaine.Append(Imputation & VI.Tild)
                Chaine.Append(Code_Fonction & VI.Tild)
                Chaine.Append(Code_Budget & VI.Tild)
                Select Case Montant
                    Case Is = 0
                        Chaine.Append("0")
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Montant.ToString))
                End Select

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then Rang = CInt(TableauData(2))
                Libelle = TableauData(3)
                Imputation = TableauData(4)
                Code_Fonction = TableauData(5)
                Code_Budget = TableauData(6)
                If TableauData(7) <> "" Then Montant = VirRhFonction.ConversionDouble(TableauData(7))

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

