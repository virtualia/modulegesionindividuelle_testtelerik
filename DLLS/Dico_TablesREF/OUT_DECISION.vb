﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class OUT_DECISION
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsRang As Integer
        Private WsValeurInterne(6) As String
        Private WsCodeExterne(6) As String
        'Dictionnaire Virtuelle
        Private WsLstDico As List(Of Outils.DictionnaireVirtuel)
        Private WsLibelleInterne(6) As String
        Private WsTableInterne(6) As String
        Private WsLibelleExterne(6) As String
        Private WsTableExterne(6) As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "OUT_DECISION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueInterface
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 4
            End Get
        End Property

        Public Property Rang() As Integer
            Get
                Return WsRang
            End Get
            Set(ByVal value As Integer)
                WsRang = value
            End Set
        End Property

        Public Property ValeurInterne(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To 6
                        Return WsValeurInterne(Index)
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 6
                        WsValeurInterne(Index) = F_FormatAlpha(value, 120)
                End Select
            End Set
        End Property

        Public Property CodeExterne(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To 6
                        Return WsCodeExterne(Index)
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 6
                        WsCodeExterne(Index) = F_FormatAlpha(value, 20)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder
                Dim IndiceI As Integer
                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Rang.ToString & VI.Tild)
                For IndiceI = 0 To 6
                    Chaine.Append(ValeurInterne(IndiceI) & VI.Tild)
                Next IndiceI
                For IndiceI = 0 To 6
                    Chaine.Append(CodeExterne(IndiceI) & VI.Tild)
                Next IndiceI

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 16 Then
                    Exit Property
                End If
                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) <> "" Then Rang = CInt(TableauData(1))
                For IndiceI = 0 To 6
                    ValeurInterne(IndiceI) = TableauData(IndiceI + 2)
                    CodeExterne(IndiceI) = TableauData(IndiceI + 9)
                Next IndiceI

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_DonneeGrid As String
            Get
                If WsLstDico Is Nothing Then
                    Call FaireDictionnaire()
                End If
                Dim Chaine As New StringBuilder
                Dim IndiceI As Integer
                For IndiceI = 0 To WsLibelleInterne.Count - 1
                    Chaine.Append(ValeurInterne(IndiceI) & VI.Tild)
                Next IndiceI
                For IndiceI = 0 To WsLibelleExterne.Count - 1
                    Chaine.Append(CodeExterne(IndiceI) & VI.Tild)
                Next IndiceI
                Chaine.Append(Rang)

                Return Chaine.ToString
            End Get
        End Property

        Public ReadOnly Property ListeDico As List(Of Outils.DictionnaireVirtuel)
            Get
                If WsLstDico Is Nothing Then
                    Call FaireDictionnaire()
                End If
                Return WsLstDico
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Private Sub FaireDictionnaire()
            Dim InfoBase As Outils.DictionnaireVirtuel
            Dim IndiceI As Integer
            For IndiceI = 0 To WsLibelleInterne.Count - 1
                WsLibelleInterne(IndiceI) = ""
                WsTableInterne(IndiceI) = ""
                WsLibelleExterne(IndiceI) = ""
                WsTableExterne(IndiceI) = 0
            Next IndiceI
            Select Case MyBase.Ide_Dossier
                Case 22
                    WsLibelleInterne(0) = "Statut"
                    WsTableInterne(0) = "Statut"
                    WsLibelleInterne(1) = "Situation statutaire"
                    WsTableInterne(1) = "Situation"
                    WsLibelleInterne(2) = "Position administrative"
                    WsTableInterne(2) = CStr(VI.PointdeVue.PVuePosition)
                    WsLibelleInterne(3) = "Modalité de service"
                    WsTableInterne(3) = "Modalité de service"
                    WsLibelleInterne(4) = "Taux d'activité"
                    WsLibelleInterne(5) = "Motif de la position"
                    WsTableInterne(5) = "Motif"

                    WsLibelleExterne(0) = "Code SS"
                    WsTableExterne(0) = 11
                    WsLibelleExterne(1) = "Code RC"
                    WsTableExterne(1) = 13
                    WsLibelleExterne(2) = "Code REM"
                    WsTableExterne(2) = 14
                    WsLibelleExterne(3) = "Code STAT"
                    WsTableExterne(3) = 15
                    WsLibelleExterne(4) = "Imputation PCE"
                    WsTableExterne(4) = 42

                Case 23
                    WsLibelleInterne(0) = "Administration"
                    WsTableInterne(0) = CStr(VI.PointdeVue.PVueEtablissement)
                    WsLibelleInterne(1) = "Statut"
                    WsTableInterne(1) = "Statut"
                    WsLibelleInterne(2) = "Code Zone de résidence"
                    WsLibelleInterne(3) = "Grade"
                    WsTableInterne(3) = CStr(VI.PointdeVue.PVueGrades)
                    WsLibelleInterne(4) = "Fonction exercée"
                    WsTableInterne(4) = CStr(VI.PointdeVue.PVueMetier)
                    WsLibelleInterne(5) = "Résidence administrative"
                    WsTableInterne(5) = "Résidence"

                    WsLibelleExterne(0) = "Code Adm gestionnaire"
                    WsLibelleExterne(1) = "Code Dept gestionnaire"
                    WsLibelleExterne(2) = "Code poste 1ère partie"
                    WsLibelleExterne(3) = "Code poste 2ème partie"
                    WsLibelleExterne(4) = "Libellé du code poste"

                Case 24
                    WsLibelleInterne(0) = "Nature de l'Indemnité"
                    WsTableInterne(0) = CStr(VI.PointdeVue.PVuePrimes)
                    WsLibelleInterne(1) = "Personnel concerné"

                    WsLibelleExterne(0) = "Code mouvement"
                    WsLibelleExterne(1) = "Code prime"
                    WsTableExterne(1) = 22
                    WsLibelleExterne(2) = "Mode de calcul"
                    WsTableExterne(2) = 24
                    WsLibelleExterne(3) = "Périodicité"
                    WsTableExterne(3) = 23
                    WsLibelleExterne(4) = "Règle interne"
                    WsLibelleExterne(5) = "Code origine"
                    WsTableExterne(5) = 27
                    WsLibelleExterne(6) = "Code sens ou N° d'ordre"

                Case 25
                    WsLibelleInterne(0) = "Résidence Administrative"
                    WsTableInterne(0) = "Résidence"

                    WsLibelleExterne(0) = "Province / Ile de France"
                    WsLibelleExterne(1) = "Valeur Zone de résidence"

                Case 101 'Virtualia - Cohérence Position Activité et Motif de changement de position
                    WsLibelleInterne(0) = "Motif du changement"
                    WsTableInterne(0) = "Motif"

                    WsLibelleExterne(0) = "Compatibilité (Oui/Non)"

                Case 1076 'Barême ACF
                    WsLibelleInterne(0) = "Province / Ile de France"
                    WsLibelleInterne(1) = "Grade"
                    WsTableInterne(1) = CStr(VI.PointdeVue.PVueGrades)
                    WsLibelleInterne(2) = "Echelon"
                    WsTableInterne(2) = "Echelon"

                    WsLibelleExterne(0) = "Montant mensuel"
                    WsLibelleExterne(1) = "Montant annuel"
                    WsLibelleExterne(2) = "Barême / Montant individuel"

                Case 1102
                    WsLibelleInterne(0) = "Administration"
                    WsTableInterne(0) = CStr(VI.PointdeVue.PVueEtablissement)
                    WsLibelleInterne(1) = "Fonction exercée"
                    WsTableInterne(1) = CStr(VI.PointdeVue.PVueMetier)
                    WsLibelleInterne(2) = "Grade"
                    WsTableInterne(2) = CStr(VI.PointdeVue.PVueGrades)

                    WsLibelleExterne(0) = "Code taux"

                Case 1103, 1104
                    WsLibelleInterne(0) = "Grade"
                    WsTableInterne(0) = CStr(VI.PointdeVue.PVueGrades)
                    WsLibelleInterne(1) = "Echelon"
                    WsTableInterne(1) = "Echelon"
                    WsLibelleInterne(2) = "Ancienneté échelon"

                    WsLibelleExterne(0) = "Code taux"

                Case 1469
                    WsLibelleInterne(0) = "Grade"
                    WsTableInterne(0) = CStr(VI.PointdeVue.PVueGrades)
                    WsLibelleInterne(1) = "Echelon"
                    WsTableInterne(1) = "Echelon"
                    WsLibelleInterne(2) = "Fonction exercée"
                    WsTableInterne(2) = CStr(VI.PointdeVue.PVueMetier)

                    WsLibelleExterne(0) = "Code taux"

            End Select

            WsLstDico = New List(Of Outils.DictionnaireVirtuel)

            For IndiceI = 0 To WsLibelleInterne.Count - 1
                InfoBase = New Outils.DictionnaireVirtuel
                InfoBase.Etiquette = WsLibelleInterne(IndiceI)
                If WsLibelleInterne(IndiceI) <> "" Then
                    InfoBase.Longueur = 120
                    InfoBase.NatureDonnee = "Alpha"
                    InfoBase.FormatDonnee = "TelleQue"
                    InfoBase.IdentifiantTable = 0
                    Select Case WsTableInterne(IndiceI)
                        Case Is <> ""
                            InfoBase.NatureDonnee = "Table"
                            InfoBase.FormatDonnee = "Alpha"
                            If IsNumeric(WsTableInterne(IndiceI)) Then
                                InfoBase.PointdeVue = CInt(WsTableInterne(IndiceI))
                                InfoBase.NomTable = ""
                            Else
                                InfoBase.PointdeVue = VI.PointdeVue.PVueGeneral
                                InfoBase.NomTable = WsTableInterne(IndiceI)
                            End If
                    End Select
                Else
                    InfoBase.SiVisible = False
                End If
                WsLstDico.Add(InfoBase)
            Next IndiceI
            For IndiceI = 0 To WsLibelleExterne.Count - 1
                InfoBase = New Outils.DictionnaireVirtuel
                InfoBase.Etiquette = WsLibelleExterne(IndiceI)
                If WsLibelleExterne(IndiceI) <> "" Then
                    InfoBase.Longueur = 20
                    InfoBase.NatureDonnee = "Alpha"
                    InfoBase.FormatDonnee = "TelleQue"
                    InfoBase.IdentifiantTable = 0
                    If WsTableExterne(IndiceI) > 0 Then
                        InfoBase.NatureDonnee = "Table"
                        InfoBase.FormatDonnee = "Alpha"
                        InfoBase.IdentifiantTable = WsTableExterne(IndiceI)
                    End If
                Else
                    InfoBase.SiVisible = False
                End If
                WsLstDico.Add(InfoBase)
            Next IndiceI

        End Sub


    End Class
End Namespace


