﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class GRD_AVANCEMENT_GRADE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsNumero_ordre As Integer
        Private WsGrade_avancement As String
        Private WsVoie_avancement As Integer
        Private WsReferences_Texte As String
        Private WsCondition_age As Integer
        Private WsCondition_echelonatteint As String
        Private WsCondition_echelonanciennete As String
        Private WsAncienneterequise_echelon As Integer = 0
        Private WsAncienneterequise_grade As Integer = 0
        Private WsAncienneterequise_corps As Integer = 0
        Private WsAncienneterequise_categorie As Integer = 0
        Private WsAncienneterequise_serviceseffectifs As Integer = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "GRD_AVANCEMENT_GRADE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueGrades
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Numero_ordre() As Integer
            Get
                Return WsNumero_ordre
            End Get
            Set(ByVal value As Integer)
                WsNumero_ordre = value
            End Set
        End Property

        Public Property Grade_avancement() As String
            Get
                Return WsGrade_avancement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsGrade_avancement = value
                    Case Else
                        WsGrade_avancement = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Voie_avancement() As Integer
            Get
                Return WsVoie_avancement
            End Get
            Set(ByVal value As Integer)
                WsVoie_avancement = value
            End Set
        End Property

        Public Property References_Texte() As String
            Get
                Return WsReferences_Texte
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsReferences_Texte = value
                    Case Else
                        WsReferences_Texte = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property Condition_age() As Integer
            Get
                Return WsCondition_age
            End Get
            Set(ByVal value As Integer)
                WsCondition_age = value
            End Set
        End Property

        Public Property Condition_echelonatteint() As String
            Get
                Return WsCondition_echelonatteint
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsCondition_echelonatteint = value
                    Case Else
                        WsCondition_echelonatteint = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Condition_echelonanciennete() As String
            Get
                Return WsCondition_echelonanciennete
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsCondition_echelonanciennete = value
                    Case Else
                        WsCondition_echelonanciennete = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Ancienneterequise_echelon() As Integer
            Get
                Return WsAncienneterequise_echelon
            End Get
            Set(ByVal value As Integer)
                WsAncienneterequise_echelon = value
            End Set
        End Property

        Public Property Ancienneterequise_grade() As Integer
            Get
                Return WsAncienneterequise_grade
            End Get
            Set(ByVal value As Integer)
                WsAncienneterequise_grade = value
            End Set
        End Property

        Public Property Ancienneterequise_corps() As Integer
            Get
                Return WsAncienneterequise_corps
            End Get
            Set(ByVal value As Integer)
                WsAncienneterequise_corps = value
            End Set
        End Property

        Public Property Ancienneterequise_categorie() As Integer
            Get
                Return WsAncienneterequise_categorie
            End Get
            Set(ByVal value As Integer)
                WsAncienneterequise_categorie = value
            End Set
        End Property

        Public Property Ancienneterequise_serviceseffectifs() As Integer
            Get
                Return WsAncienneterequise_serviceseffectifs
            End Get
            Set(ByVal value As Integer)
                WsAncienneterequise_serviceseffectifs = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Numero_ordre.ToString & VI.Tild)
                Chaine.Append(Grade_avancement & VI.Tild)
                Chaine.Append(Voie_avancement.ToString & VI.Tild)
                Chaine.Append(References_Texte & VI.Tild)
                Chaine.Append(Condition_age.ToString & VI.Tild)
                Chaine.Append(Condition_echelonatteint & VI.Tild)
                Chaine.Append(Condition_echelonanciennete & VI.Tild)
                Chaine.Append(Ancienneterequise_echelon.ToString & VI.Tild)
                Chaine.Append(Ancienneterequise_grade.ToString & VI.Tild)
                Chaine.Append(Ancienneterequise_corps.ToString & VI.Tild)
                Chaine.Append(Ancienneterequise_categorie.ToString & VI.Tild)
                Chaine.Append(Ancienneterequise_serviceseffectifs.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 13 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) <> "" Then Numero_ordre = CInt(TableauData(1))
                Grade_avancement = TableauData(2)
                If TableauData(3) <> "" Then Voie_avancement = CInt(TableauData(3))
                References_Texte = TableauData(4)
                If TableauData(5) <> "" Then Condition_age = CInt(TableauData(5))
                Condition_echelonatteint = TableauData(6)
                Condition_echelonanciennete = TableauData(7)
                If TableauData(8) <> "" Then Ancienneterequise_echelon = CInt(TableauData(8))
                If TableauData(9) <> "" Then Ancienneterequise_grade = CInt(TableauData(9))
                If TableauData(10) <> "" Then Ancienneterequise_corps = CInt(TableauData(10))
                If TableauData(11) <> "" Then Ancienneterequise_categorie = CInt(TableauData(11))
                If TableauData(12) <> "" Then Ancienneterequise_serviceseffectifs = CInt(TableauData(12))

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace



