﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAI_MALADIE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsDroit_Titulaire_100 As Integer = 0
        Private WsDroit_Titulaire_50 As Integer = 0
        Private WsDroit_NonTitulaire_100_T1 As Integer = 0
        Private WsDroit_NonTitulaire_50_T1 As Integer = 0
        Private WsPlancher_NonTitulaire_T1 As Integer = 0
        Private WsDroit_NonTitulaire_100_T2 As Integer = 0
        Private WsDroit_NonTitulaire_50_T2 As Integer = 0
        Private WsPlancher_NonTitulaire_T2 As Integer = 0
        Private WsDroit_NonTitulaire_100_T3 As Integer = 0
        Private WsDroit_NonTitulaire_50_T3 As Integer = 0
        Private WsPlancher_NonTitulaire_T3 As Integer = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAI_MALADIE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePaie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 21
            End Get
        End Property

        Public Property Droit_Titulaire_100() As Integer
            Get
                Return WsDroit_Titulaire_100
            End Get
            Set(ByVal value As Integer)
                WsDroit_Titulaire_100 = value
            End Set
        End Property

        Public Property Droit_Titulaire_50() As Integer
            Get
                Return WsDroit_Titulaire_50
            End Get
            Set(ByVal value As Integer)
                WsDroit_Titulaire_50 = value
            End Set
        End Property

        Public Property Droit_NonTitulaire_100_T1() As Integer
            Get
                Return WsDroit_NonTitulaire_100_T1
            End Get
            Set(ByVal value As Integer)
                WsDroit_NonTitulaire_100_T1 = value
            End Set
        End Property

        Public Property Droit_NonTitulaire_50_T1() As Integer
            Get
                Return WsDroit_NonTitulaire_50_T1
            End Get
            Set(ByVal value As Integer)
                WsDroit_NonTitulaire_50_T1 = value
            End Set
        End Property

        Public Property Plancher_NonTitulaire_T1() As Integer
            Get
                Return WsPlancher_NonTitulaire_T1
            End Get
            Set(ByVal value As Integer)
                WsPlancher_NonTitulaire_T1 = value
            End Set
        End Property

        Public Property Droit_NonTitulaire_100_T2() As Integer
            Get
                Return WsDroit_NonTitulaire_100_T2
            End Get
            Set(ByVal value As Integer)
                WsDroit_NonTitulaire_100_T2 = value
            End Set
        End Property

        Public Property Droit_NonTitulaire_50_T2() As Integer
            Get
                Return WsDroit_NonTitulaire_50_T2
            End Get
            Set(ByVal value As Integer)
                WsDroit_NonTitulaire_50_T2 = value
            End Set
        End Property

        Public Property Plancher_NonTitulaire_T2() As Integer
            Get
                Return WsPlancher_NonTitulaire_T2
            End Get
            Set(ByVal value As Integer)
                WsPlancher_NonTitulaire_T2 = value
            End Set
        End Property

        Public Property Droit_NonTitulaire_100_T3() As Integer
            Get
                Return WsDroit_NonTitulaire_100_T3
            End Get
            Set(ByVal value As Integer)
                WsDroit_NonTitulaire_100_T3 = value
            End Set
        End Property

        Public Property Droit_NonTitulaire_50_T3() As Integer
            Get
                Return WsDroit_NonTitulaire_50_T3
            End Get
            Set(ByVal value As Integer)
                WsDroit_NonTitulaire_50_T3 = value
            End Set
        End Property

        Public Property Plancher_NonTitulaire_T3() As Integer
            Get
                Return WsPlancher_NonTitulaire_T3
            End Get
            Set(ByVal value As Integer)
                WsPlancher_NonTitulaire_T3 = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Droit_Titulaire_100.ToString & VI.Tild)
                Chaine.Append(Droit_Titulaire_50.ToString & VI.Tild)
                Chaine.Append(Droit_NonTitulaire_100_T1.ToString & VI.Tild)
                Chaine.Append(Droit_NonTitulaire_50_T1.ToString & VI.Tild)
                Chaine.Append(Plancher_NonTitulaire_T1.ToString & VI.Tild)
                Chaine.Append(Droit_NonTitulaire_100_T2.ToString & VI.Tild)
                Chaine.Append(Droit_NonTitulaire_50_T2.ToString & VI.Tild)
                Chaine.Append(Plancher_NonTitulaire_T2.ToString & VI.Tild)
                Chaine.Append(Droit_NonTitulaire_100_T3.ToString & VI.Tild)
                Chaine.Append(Droit_NonTitulaire_50_T3.ToString & VI.Tild)
                Chaine.Append(Plancher_NonTitulaire_T3.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 13 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then Droit_Titulaire_100 = CInt(TableauData(2))
                If TableauData(3) <> "" Then Droit_Titulaire_50 = CInt(TableauData(3))
                If TableauData(4) <> "" Then Droit_NonTitulaire_100_T1 = CInt(TableauData(4))
                If TableauData(5) <> "" Then Droit_NonTitulaire_50_T1 = CInt(TableauData(5))
                If TableauData(6) <> "" Then Plancher_NonTitulaire_T1 = CInt(TableauData(6))
                If TableauData(7) <> "" Then Droit_NonTitulaire_100_T2 = CInt(TableauData(7))
                If TableauData(8) <> "" Then Droit_NonTitulaire_50_T2 = CInt(TableauData(8))
                If TableauData(9) <> "" Then Plancher_NonTitulaire_T2 = CInt(TableauData(9))
                If TableauData(10) <> "" Then Droit_NonTitulaire_100_T3 = CInt(TableauData(10))
                If TableauData(11) <> "" Then Droit_NonTitulaire_50_T3 = CInt(TableauData(11))
                If TableauData(12) <> "" Then Plancher_NonTitulaire_T3 = CInt(TableauData(12))

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
