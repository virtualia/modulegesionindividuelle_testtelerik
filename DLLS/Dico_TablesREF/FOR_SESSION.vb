﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class FOR_SESSION
        Inherits VIR_FICHE

        Private WsLstInscrits As List(Of FOR_INSCRIPTIONS)
        Private WsFicheLue As StringBuilder
        '
        Private WsNbMinimum As Integer = 0
        Private WsNbMaximum As Integer = 0
        Private WsNbJours As Double = 0
        Private WsObservations As String = ""
        Private WsQuotaHoraireMinimum As Integer = 0
        Private WsFormateur As String = ""
        Private WsAdresseStage As String = ""
        Private WsDateCloture As String = ""
        Private WsDureeEnJours As Double = 0
        Private WsDureeEnHeures As Double = 0
        Private WsHeureDebut_Matin As String = ""
        Private WsHeureFin_Matin As String = ""
        Private WsHeureDebut_AM As String = ""
        Private WsHeureFin_AM As String = ""
        Private WsDateFinSession As String = ""
        Private WsOrganismePrestataire As String = ""
        Private WsLieuHebergement As String = ""
        Private WsIntituleSession As String = ""
        Private WsReferenceSession As String = ""
        Private WsCursusSession As String = ""
        Private WsModuleSession As String = ""
        Private WsLieuSession As String = ""
        Private WsCodePostal As String = ""
        Private WsVille As String = ""
        Private WsLieuFormation As String = ""
        Private WsSiFacturation As String = ""
        Private WsSiPasInfluenceTT As String = ""
        Private WsRang As Integer = 0 'Issu de la date de début en mode datetime

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "FOR_SESSION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueFormation
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 5
            End Get
        End Property

        Public Property DateSession() As String
            Get
                Return MyBase.Date_de_Valeur
            End Get
            Set(ByVal value As String)
                MyBase.Date_de_Valeur = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property NombreMinimum_Inscrits() As Integer
            Get
                Return WsNbMinimum
            End Get
            Set(ByVal value As Integer)
                WsNbMinimum = value
            End Set
        End Property

        Public Property NombreMaximum_Inscrits() As Integer
            Get
                Return WsNbMaximum
            End Get
            Set(ByVal value As Integer)
                WsNbMaximum = value
            End Set
        End Property

        Public Property NombredeJours_Formation() As Double
            Get
                Return WsNbJours
            End Get
            Set(ByVal value As Double)
                WsNbJours = value
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property QuotaHoraireMinimum() As Integer
            Get
                Return WsQuotaHoraireMinimum
            End Get
            Set(ByVal value As Integer)
                WsQuotaHoraireMinimum = value
            End Set
        End Property

        Public Property Formateur() As String
            Get
                Return WsFormateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsFormateur = value
                    Case Else
                        WsFormateur = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property AdresseduStage() As String
            Get
                Return WsAdresseStage
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 150
                        WsAdresseStage = value
                    Case Else
                        WsAdresseStage = Strings.Left(value, 150)
                End Select
            End Set
        End Property

        Public Property DatedeCloture_Inscriptions() As String
            Get
                Return WsDateCloture
            End Get
            Set(ByVal value As String)
                WsDateCloture = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property DureeSession_Jours() As Double
            Get
                Return WsDureeEnJours
            End Get
            Set(ByVal value As Double)
                WsDureeEnJours = value
            End Set
        End Property

        Public Property DureeSession_Heures() As Double
            Get
                Return WsDureeEnHeures
            End Get
            Set(ByVal value As Double)
                WsDureeEnHeures = value
            End Set
        End Property

        Public Property HeureDebut_Matin() As String
            Get
                Return WsHeureDebut_Matin
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureDebut_Matin = value
                    Case Else
                        WsHeureDebut_Matin = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property HeureFin_Matin() As String
            Get
                Return WsHeureFin_Matin
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureFin_Matin = value
                    Case Else
                        WsHeureFin_Matin = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property HeureDebut_ApresMidi() As String
            Get
                Return WsHeureDebut_AM
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureDebut_AM = value
                    Case Else
                        WsHeureDebut_AM = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property HeureFin_ApresMidi() As String
            Get
                Return WsHeureFin_AM
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureFin_AM = value
                    Case Else
                        WsHeureFin_AM = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property DatedeFin_Session() As String
            Get
                Return WsDateFinSession
            End Get
            Set(ByVal value As String)
                WsDateFinSession = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Fin = value
            End Set
        End Property

        Public Property OrganismePrestataire() As String
            Get
                Return WsOrganismePrestataire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsOrganismePrestataire = value
                    Case Else
                        WsOrganismePrestataire = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property LieuHebergement() As String
            Get
                Return WsLieuHebergement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 150
                        WsLieuHebergement = value
                    Case Else
                        WsLieuHebergement = Strings.Left(value, 150)
                End Select
            End Set
        End Property

        Public Property IntituleSession() As String
            Get
                Return WsIntituleSession
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntituleSession = value
                    Case Else
                        WsIntituleSession = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property ReferenceSession() As String
            Get
                Return WsReferenceSession
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsReferenceSession = value
                    Case Else
                        WsReferenceSession = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property CursusSession() As String
            Get
                Return WsCursusSession
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsCursusSession = value
                    Case Else
                        WsCursusSession = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property ModuleSession() As String
            Get
                Return WsModuleSession
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsModuleSession = value
                    Case Else
                        WsModuleSession = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property LieuSession() As String
            Get
                Return WsLieuSession
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsLieuSession = value
                    Case Else
                        WsLieuSession = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property CodePostal() As String
            Get
                Return WsCodePostal
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsCodePostal = value
                    Case Else
                        WsCodePostal = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property VilleAdresse() As String
            Get
                Return WsVille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsVille = value
                    Case Else
                        WsVille = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property LieuFormation() As String
            Get
                Return WsLieuFormation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsLieuFormation = value
                    Case Else
                        WsLieuFormation = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property SiSessionFacturee() As String
            Get
                Return WsSiFacturation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiFacturation = value
                    Case Else
                        WsSiFacturation = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property SiPas_Influence_SurTempsTravail() As String
            Get
                Return WsSiPasInfluenceTT
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiPasInfluenceTT = value
                    Case Else
                        WsSiPasInfluenceTT = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Rang() As Integer
            Get
                Return WsRang
            End Get
            Set(ByVal value As Integer)
                WsRang = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder = New StringBuilder

                Chaine.Append(DateSession & Strings.Space(1) & Strings.Format(Rang, "00") & ":00:00" & VI.Tild)
                Chaine.Append(NombreMinimum_Inscrits.ToString & VI.Tild)
                Chaine.Append(NombreMaximum_Inscrits.ToString & VI.Tild)
                Chaine.Append(NombredeJours_Formation.ToString & VI.Tild)
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(QuotaHoraireMinimum.ToString & VI.Tild)
                Chaine.Append(Formateur & VI.Tild)
                Chaine.Append(AdresseduStage & VI.Tild)
                Chaine.Append(DatedeCloture_Inscriptions & VI.Tild)
                Chaine.Append(DureeSession_Jours.ToString & VI.Tild)
                Chaine.Append(DureeSession_Heures.ToString & VI.Tild)
                Chaine.Append(HeureDebut_Matin & VI.Tild)
                Chaine.Append(HeureFin_Matin & VI.Tild)
                Chaine.Append(HeureDebut_ApresMidi & VI.Tild)
                Chaine.Append(HeureFin_ApresMidi & VI.Tild)
                Chaine.Append(DatedeFin_Session & VI.Tild)
                Chaine.Append(OrganismePrestataire & VI.Tild)
                Chaine.Append(LieuHebergement & VI.Tild)
                Chaine.Append(IntituleSession & VI.Tild)
                Chaine.Append(ReferenceSession & VI.Tild)
                Chaine.Append(CursusSession & VI.Tild)
                Chaine.Append(ModuleSession & VI.Tild)
                Chaine.Append(LieuSession & VI.Tild)
                Chaine.Append(CodePostal & VI.Tild)
                Chaine.Append(VilleAdresse & VI.Tild)
                Chaine.Append(LieuFormation & VI.Tild)
                Chaine.Append(SiSessionFacturee & VI.Tild)
                Chaine.Append(SiPas_Influence_SurTempsTravail & VI.Tild)
                Chaine.Append(Rang)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 29 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1).Length > 10 Then
                    DateSession = Strings.Left(TableauData(1), 10)
                    If Strings.Left(Strings.Right(TableauData(1), TableauData(1).Length - 11), 2) <> "" _
                        AndAlso IsNumeric(Strings.Left(Strings.Right(TableauData(1), TableauData(1).Length - 11), 2)) Then
                        Rang = CInt(Strings.Left(Strings.Right(TableauData(1), TableauData(1).Length - 11), 2))
                    End If
                Else
                    DateSession = TableauData(1)
                End If
                If TableauData(2) <> "" Then NombreMinimum_Inscrits = CInt(TableauData(2))
                If TableauData(3) <> "" Then NombreMaximum_Inscrits = CInt(TableauData(3))
                If TableauData(4) <> "" Then NombredeJours_Formation = VirRhFonction.ConversionDouble(TableauData(4))
                Observations = TableauData(5)
                If TableauData(6) <> "" Then QuotaHoraireMinimum = CInt(TableauData(6))
                Formateur = TableauData(7)
                AdresseduStage = TableauData(8)
                DatedeCloture_Inscriptions = TableauData(9)
                If TableauData(10) <> "" Then DureeSession_Jours = VirRhFonction.ConversionDouble(TableauData(10))
                If TableauData(11) <> "" Then DureeSession_Heures = VirRhFonction.ConversionDouble(TableauData(11))
                HeureDebut_Matin = TableauData(12)
                HeureFin_Matin = TableauData(13)
                HeureDebut_ApresMidi = TableauData(14)
                HeureFin_ApresMidi = TableauData(15)
                DatedeFin_Session = TableauData(16)
                OrganismePrestataire = TableauData(17)
                LieuHebergement = TableauData(18)
                IntituleSession = TableauData(19)
                ReferenceSession = TableauData(20)
                CursusSession = TableauData(21)
                ModuleSession = TableauData(22)
                LieuSession = TableauData(23)
                CodePostal = TableauData(24)
                VilleAdresse = TableauData(25)
                LieuFormation = TableauData(26)
                SiSessionFacturee = TableauData(27)
                SiPas_Influence_SurTempsTravail = TableauData(28)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public ReadOnly Property VTypeJour() As Integer
            Get
                Select Case NombredeJours_Formation
                    Case Is > 1
                        Return VI.NumeroPlage.Jour_Formation
                    Case Else
                        If HeureDebut_Matin <> "" And HeureFin_Matin <> "" And HeureDebut_ApresMidi <> "" And HeureFin_ApresMidi <> "" Then
                            Return VI.NumeroPlage.Jour_Formation
                        End If
                        If HeureDebut_Matin <> "" And HeureFin_Matin <> "" Then
                            If HeureDebut_ApresMidi = "" Or HeureFin_ApresMidi = "" Then
                                Select Case VCaracteristiqueHoraires
                                    Case "AM"
                                        Return VI.NumeroPlage.Plage1_Formation
                                    Case "PM"
                                        Return VI.NumeroPlage.Plage2_Formation
                                    Case "DAY"
                                        Return VI.NumeroPlage.Jour_Formation
                                End Select
                            End If
                        End If
                        If HeureDebut_ApresMidi <> "" And HeureFin_ApresMidi <> "" Then
                            If HeureDebut_Matin = "" Or HeureFin_Matin = "" Then
                                Select Case VCaracteristiqueHoraires
                                    Case "AM"
                                        Return VI.NumeroPlage.Plage1_Formation
                                    Case "PM"
                                        Return VI.NumeroPlage.Plage2_Formation
                                    Case "DAY"
                                        Return VI.NumeroPlage.Jour_Formation
                                End Select
                            End If
                        End If
                        Return VI.NumeroPlage.Jour_Formation
                End Select
            End Get
        End Property

        Private ReadOnly Property VCaracteristiqueHoraires As String
            Get
                If HeureDebut_Matin <> "" And HeureFin_Matin <> "" And HeureDebut_ApresMidi <> "" And HeureFin_ApresMidi <> "" Then
                    Return "DAY"
                End If
                If HeureDebut_Matin <> "" And HeureFin_Matin <> "" Then
                    If HeureDebut_ApresMidi = "" Or HeureFin_ApresMidi = "" Then
                        Select Case VirRhDate.ComparerHeures(HeureFin_Matin, "13:30")
                            Case VI.ComparaisonDates.PlusPetit
                                Return "AM"
                            Case Else
                                Select Case VirRhDate.ComparerHeures(HeureDebut_Matin, "13:30")
                                    Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                                        Return "PM"
                                    Case Else
                                        Return "DAY"
                                End Select
                        End Select
                    End If
                End If
                If HeureDebut_ApresMidi <> "" And HeureFin_ApresMidi <> "" Then
                    If HeureDebut_Matin = "" Or HeureFin_Matin = "" Then
                        Select Case VirRhDate.ComparerHeures(HeureDebut_ApresMidi, "13:30")
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                                Return "PM"
                            Case Else
                                Select Case VirRhDate.ComparerHeures(HeureFin_ApresMidi, "13:30")
                                    Case VI.ComparaisonDates.PlusPetit
                                        Return "AM"
                                    Case Else
                                        Return "DAY"
                                End Select
                        End Select
                    End If
                End If
                Return "DAY"
            End Get
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property LD_Horaires As String
            Get
                Select Case LD_TestHoraire()
                    Case "0000"
                        Return ""
                    Case "1100"
                        Return "de " & HeureDebut_Matin & " à " & HeureFin_Matin
                    Case "0011"
                        Return "de " & HeureDebut_ApresMidi & " à " & HeureFin_ApresMidi
                    Case "1001"
                        Return "de " & HeureDebut_Matin & " à " & HeureFin_ApresMidi
                    Case "1111"
                        Return "de " & HeureDebut_Matin & " à " & HeureFin_Matin & " et de " & HeureDebut_ApresMidi & " à " & HeureFin_ApresMidi
                End Select
                Return ""
            End Get
        End Property

        Public Function LD_TestHoraire() As String
            Dim Retour As String = ""

            If HeureDebut_Matin.Trim = "" Then
                Retour &= "0"
            Else
                Retour &= "1"
            End If

            If HeureFin_Matin.Trim = "" Then
                Retour &= "0"
            Else
                Retour &= "1"
            End If

            If HeureDebut_ApresMidi.Trim = "" Then
                Retour &= "0"
            Else
                Retour &= "1"
            End If

            If HeureFin_ApresMidi.Trim = "" Then
                Retour &= "0"
            Else
                Retour &= "1"
            End If

            Return Retour
        End Function

        Public Function Ajouter_Inscription(ByVal Fiche As FOR_INSCRIPTIONS) As Integer
            If WsLstInscrits Is Nothing Then
                WsLstInscrits = New List(Of FOR_INSCRIPTIONS)()
            End If
            WsLstInscrits.Add(Fiche)
            Return WsLstInscrits.Count
        End Function

        Public Function ListedesInscrits(ByVal CritereTri As String) As List(Of FOR_INSCRIPTIONS)

            If WsLstInscrits Is Nothing Then
                Return Nothing
            End If
            Select Case CritereTri
                Case Is = "Date"
                    Return WsLstInscrits.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
                Case Is = "Direction"
                    Return WsLstInscrits.OrderBy(Function(m) m.Direction_stagiaire).ToList
                Case Is = "Etablissement"
                    Return WsLstInscrits.OrderBy(Function(m) m.Etablissement_stagiaire).ToList
                Case Is = "Suivi"
                    Return WsLstInscrits.OrderBy(Function(m) m.Suivi_inscription).ToList
                Case Else
                    Return WsLstInscrits.OrderBy(Function(m) m.Nom_stagiaire).ToList
            End Select

        End Function

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace


