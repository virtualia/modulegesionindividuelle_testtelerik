﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class CORRESP_DEFINITION
        Inherits VIR_FICHE

        Private WsLstValeurs As List(Of CORRESP_EXTERNES)
        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsPtdeVueInverse As Integer
        Private WsNomTable As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "CORRESP_DEFINITION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueTranscodification
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 128
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 128)
                End Select
            End Set
        End Property

        Public Property PointdeVue_Inverse() As Integer
            Get
                Return WsPtdeVueInverse
            End Get
            Set(ByVal value As Integer)
                WsPtdeVueInverse = value
            End Set
        End Property

        Public Property Nom_Table_Inverse() As String
            Get
                Return WsNomTable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 128
                        WsNomTable = value
                    Case Else
                        WsNomTable = Strings.Left(value, 128)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(PointdeVue_Inverse.ToString & VI.Tild)
                Chaine.Append(Nom_Table_Inverse)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                If TableauData(2) = "" Then
                    TableauData(2) = "0"
                End If
                PointdeVue_Inverse = CInt(TableauData(2))
                Nom_Table_Inverse = TableauData(3)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesValeurs() As List(Of CORRESP_EXTERNES)
            Get
                If WsLstValeurs Is Nothing Then
                    Return Nothing
                End If
                Return WsLstValeurs.OrderBy(Function(m) m.Valeur_Virtualia).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Fiche_Valeur(ByVal IntituleVirtualia As String) As CORRESP_EXTERNES
            If WsLstValeurs Is Nothing Then
                Return Nothing
            End If

            Return (From ce As CORRESP_EXTERNES In WsLstValeurs Where ce.Valeur_Virtualia = IntituleVirtualia Select ce).FirstOrDefault()
        End Function

        Public Function Ajouter_Valeurs(ByVal Fiche As CORRESP_EXTERNES) As Integer
            If WsLstValeurs Is Nothing Then
                WsLstValeurs = New List(Of CORRESP_EXTERNES)
            End If
            WsLstValeurs.Add(Fiche)
            Return WsLstValeurs.Count
        End Function

    End Class
End Namespace
