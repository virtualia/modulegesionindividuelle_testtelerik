﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAI_COTIS_PRIVE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsTaux_PartOuvriere_Totalite As Double = 0
        Private WsTaux_PartOuvriere_TA As Double = 0
        Private WsTaux_PartOuvriere_TB As Double = 0
        Private WsTaux_PartOuvriere_TC As Double = 0
        Private WsTaux_PartPatronale_Totalite As Double = 0
        Private WsTaux_PartPatronale_TA As Double = 0
        Private WsTaux_PartPatronale_TB As Double = 0
        Private WsTaux_PartPatronale_TC As Double = 0
        Private WsNomCaisse As String
        Private WsModedeCalcul As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAI_COTIS_PRIVE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePaie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 13
            End Get
        End Property

        Public Property Taux_PartOuvriere_Totalite() As Double
            Get
                Return WsTaux_PartOuvriere_Totalite
            End Get
            Set(ByVal value As Double)
                WsTaux_PartOuvriere_Totalite = value
            End Set
        End Property

        Public Property Taux_PartOuvriere_TrancheA() As Double
            Get
                Return WsTaux_PartOuvriere_TA
            End Get
            Set(ByVal value As Double)
                WsTaux_PartOuvriere_TA = value
            End Set
        End Property

        Public Property Taux_PartOuvriere_TrancheB() As Double
            Get
                Return WsTaux_PartOuvriere_TB
            End Get
            Set(ByVal value As Double)
                WsTaux_PartOuvriere_TB = value
            End Set
        End Property

        Public Property Taux_PartOuvriere_TrancheC() As Double
            Get
                Return WsTaux_PartOuvriere_TC
            End Get
            Set(ByVal value As Double)
                WsTaux_PartOuvriere_TC = value
            End Set
        End Property

        Public Property Taux_PartPatronale_Totalite() As Double
            Get
                Return WsTaux_PartPatronale_Totalite
            End Get
            Set(ByVal value As Double)
                WsTaux_PartPatronale_Totalite = value
            End Set
        End Property

        Public Property Taux_PartPatronale_TrancheA() As Double
            Get
                Return WsTaux_PartPatronale_TA
            End Get
            Set(ByVal value As Double)
                WsTaux_PartPatronale_TA = value
            End Set
        End Property

        Public Property Taux_PartPatronale_TrancheB() As Double
            Get
                Return WsTaux_PartPatronale_TB
            End Get
            Set(ByVal value As Double)
                WsTaux_PartPatronale_TB = value
            End Set
        End Property

        Public Property Taux_PartPatronale_TrancheC() As Double
            Get
                Return WsTaux_PartPatronale_TC
            End Get
            Set(ByVal value As Double)
                WsTaux_PartPatronale_TC = value
            End Set
        End Property

        Public Property NomdelaCaisse() As String
            Get
                Return WsNomCaisse
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsNomCaisse = value
                    Case Else
                        WsNomCaisse = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property ModedeCalcul() As String
            Get
                Return WsModedeCalcul
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsModedeCalcul = value
                    Case Else
                        WsModedeCalcul = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Taux_PartOuvriere_Totalite.ToString & VI.Tild)
                Chaine.Append(Taux_PartOuvriere_TrancheA.ToString & VI.Tild)
                Chaine.Append(Taux_PartOuvriere_TrancheB.ToString & VI.Tild)
                Chaine.Append(Taux_PartOuvriere_TrancheC.ToString & VI.Tild)
                Chaine.Append(Taux_PartPatronale_Totalite.ToString & VI.Tild)
                Chaine.Append(Taux_PartPatronale_TrancheA.ToString & VI.Tild)
                Chaine.Append(Taux_PartPatronale_TrancheB.ToString & VI.Tild)
                Chaine.Append(Taux_PartPatronale_TrancheC.ToString & VI.Tild)
                Chaine.Append(NomdelaCaisse & VI.Tild)
                Chaine.Append(ModedeCalcul)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 12 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then Taux_PartOuvriere_Totalite = VirRhFonction.ConversionDouble(TableauData(2))
                If TableauData(3) <> "" Then Taux_PartOuvriere_TrancheA = CInt(TableauData(3))
                If TableauData(4) <> "" Then Taux_PartOuvriere_TrancheB = VirRhFonction.ConversionDouble(TableauData(4))
                If TableauData(5) <> "" Then Taux_PartOuvriere_TrancheC = VirRhFonction.ConversionDouble(TableauData(5))
                If TableauData(6) <> "" Then Taux_PartPatronale_Totalite = CInt(TableauData(6))
                If TableauData(7) <> "" Then Taux_PartPatronale_TrancheA = VirRhFonction.ConversionDouble(TableauData(7))
                If TableauData(8) <> "" Then Taux_PartPatronale_TrancheB = VirRhFonction.ConversionDouble(TableauData(8))
                If TableauData(9) <> "" Then Taux_PartPatronale_TrancheC = CInt(TableauData(9))
                NomdelaCaisse = TableauData(10)
                ModedeCalcul = TableauData(11)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
