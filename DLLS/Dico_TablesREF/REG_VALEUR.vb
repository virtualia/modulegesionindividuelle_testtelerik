﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class REG_VALEUR
        Inherits VIR_FICHE
        Implements ICloneable

        Private WsFicheLue As StringBuilder
        Private WsSiMajuscule As Boolean
        Private WsIde_Critere As Integer = 0
        Private WsIde_Valeur As Integer = 0
        Private WsValeur As String = ""
        Private WsIde_Clef As String = ""

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueRegles
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 7
            End Get
        End Property

        Public ReadOnly Property Ide_Clef() As String
            Get
                Return WsIde_Clef
            End Get
        End Property

        Public Property Ide_Critere() As Integer
            Get
                Return WsIde_Critere
            End Get
            Set(ByVal value As Integer)
                WsIde_Critere = value
                WsIde_Clef = String.Concat(WsIde_Critere.ToString, "-", WsIde_Valeur.ToString)
            End Set
        End Property

        Public Property Ide_Valeur() As Integer
            Get
                Return WsIde_Valeur
            End Get
            Set(ByVal value As Integer)
                WsIde_Valeur = value
                WsIde_Clef = String.Concat(WsIde_Critere.ToString, "-", WsIde_Valeur.ToString)
            End Set
        End Property

        Public Property Valeur() As String
            Get
                Return WsValeur
            End Get
            Set(ByVal value As String)
                WsValeur = F_FormatAlpha(value, 250)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Ide_Clef.ToString & VI.Tild)
                Chaine.Append(Valeur)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                Dim TableauData(0) As String
                Dim TableauIndex(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)

                Ide_Dossier = CInt(TableauData(0))
                TableauIndex = Strings.Split(TableauData(1), VI.Tiret, -1)
                If TableauIndex.Count >= 2 Then
                    If TableauIndex(0) = "" Then TableauIndex(0) = "0"
                    Ide_Critere = CInt(TableauIndex(0))
                    If TableauIndex(1) = "" Then TableauIndex(1) = "0"
                    Ide_Valeur = CInt(TableauIndex(1))
                Else
                    Ide_Critere = 0
                    Ide_Valeur = 0
                End If

                Valeur = TableauData(2)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To UBound(TableauData) - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get

                Select Case INumInfo
                    Case 0
                        Return ""
                    Case 1
                        Return WsIde_Clef.ToString
                    Case 2
                        Return WsValeur
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
        Public Sub New(ByVal Contenu As String)
            MyBase.New(Contenu)
        End Sub

        Public Overridable Function Clone() As Object Implements ICloneable.Clone
            Return New REG_VALEUR(Ide_Dossier.ToString & ContenuTable) 'Non daté
        End Function

    End Class
End Namespace