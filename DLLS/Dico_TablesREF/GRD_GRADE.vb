Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class GRD_GRADE
        Inherits VIR_FICHE

        Private WsLstGrilles As List(Of GRD_GRILLE)
        Private WsLstAvancement As List(Of GRD_AVANCEMENT_GRADE)
        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsCategorie As String
        Private WsEchelle_de_remuneration As String
        Private WsCorps As String
        Private WsGroupe As String
        Private WsFiliere As String
        Private WsDuree_du_stage As Integer
        Private WsDescriptif As String
        Private WsConditions_d_acces As String
        Private WsSecteur As String
        Private WsTri_du_corps As String
        Private WsTri_du_grade As String
        Private WsNomenclature As String
        Private WsGestion As String
        Private WsEna As String
        Private WsCorpsparticulier As String
        Private WsDateouverture As String
        Private WsDatefermeture As String
        Private WsCodeCorps As String
        Private WsCodeNNE As String
        Private WsCodeADAGE As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "GRD_GRADE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueGrades
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Categorie() As String
            Get
                If WsCategorie = "" Then
                    Return "Non d�fini"
                Else
                    Return WsCategorie
                End If
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsCategorie = value
                    Case Else
                        WsCategorie = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Echelle_de_remuneration() As String
            Get
                Return WsEchelle_de_remuneration
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsEchelle_de_remuneration = value
                    Case Else
                        WsEchelle_de_remuneration = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Corps() As String
            Get
                If WsCorps = "" Then
                    Return "Non d�fini"
                Else
                    Return WsCorps
                End If
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsCorps = value
                    Case Else
                        WsCorps = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Groupe() As String
            Get
                Return WsGroupe
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsGroupe = value
                    Case Else
                        WsGroupe = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Filiere() As String
            Get
                If WsFiliere = "" Then
                    Return "Non d�fini"
                Else
                    Return WsFiliere
                End If
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsFiliere = value
                    Case Else
                        WsFiliere = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Duree_du_stage() As Integer
            Get
                Return WsDuree_du_stage
            End Get
            Set(ByVal value As Integer)
                WsDuree_du_stage = value
            End Set
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsDescriptif = value
                    Case Else
                        WsDescriptif = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Conditions_d_acces() As String
            Get
                Return WsConditions_d_acces
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsConditions_d_acces = value
                    Case Else
                        WsConditions_d_acces = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Secteur() As String
            Get
                Return WsSecteur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsSecteur = value
                    Case Else
                        WsSecteur = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Tri_du_corps() As Integer
            Get
                Return WsTri_du_corps
            End Get
            Set(ByVal value As Integer)
                WsTri_du_corps = value
            End Set
        End Property

        Public Property Tri_du_grade() As Integer
            Get
                Return WsTri_du_grade
            End Get
            Set(ByVal value As Integer)
                WsTri_du_grade = value
            End Set
        End Property

        Public Property Nomenclature() As String
            Get
                Return WsNomenclature
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNomenclature = value
                    Case Else
                        WsNomenclature = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Gestion() As String
            Get
                If WsGestion = "" Then
                    Return "Non d�fini"
                Else
                    Return WsGestion
                End If
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsGestion = value
                    Case Else
                        WsGestion = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Ena() As String
            Get
                Return WsEna
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsEna = value
                    Case Else
                        WsEna = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Corpsparticulier() As String
            Get
                Return WsCorpsparticulier
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsCorpsparticulier = value
                    Case Else
                        WsCorpsparticulier = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Dateouverture() As String
            Get
                Return WsDateouverture
            End Get
            Set(ByVal value As String)
                WsDateouverture = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Datefermeture() As String
            Get
                Return WsDatefermeture
            End Get
            Set(ByVal value As String)
                WsDatefermeture = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Codecorps() As String
            Get
                Return WsCodeCorps
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsCodeCorps = value
                    Case Else
                        WsCodeCorps = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property CodeNNE() As String
            Get
                Return WsCodeNNE
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsCodeNNE = value
                    Case Else
                        WsCodeNNE = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property CodeADAGE() As String
            Get
                Return WsCodeADAGE
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsCodeADAGE = value
                    Case Else
                        WsCodeADAGE = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Categorie & VI.Tild)
                Chaine.Append(Echelle_de_remuneration & VI.Tild)
                Chaine.Append(Corps & VI.Tild)
                Chaine.Append(Groupe & VI.Tild)
                Chaine.Append(Filiere & VI.Tild)
                Chaine.Append(Duree_du_stage.ToString & VI.Tild)
                Chaine.Append(Descriptif & VI.Tild)
                Chaine.Append(Conditions_d_acces & VI.Tild)
                Chaine.Append(Secteur & VI.Tild)
                Chaine.Append(Tri_du_corps.ToString & VI.Tild)
                Chaine.Append(Tri_du_grade.ToString & VI.Tild)
                Chaine.Append(Nomenclature & VI.Tild)
                Chaine.Append(Gestion & VI.Tild)
                Chaine.Append(Ena & VI.Tild)
                Chaine.Append(Corpsparticulier & VI.Tild)
                Chaine.Append(Dateouverture & VI.Tild)
                Chaine.Append(Datefermeture & VI.Tild)
                Chaine.Append(Codecorps & VI.Tild)
                Chaine.Append(CodeNNE & VI.Tild)
                Chaine.Append(CodeADAGE)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 20 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                Categorie = TableauData(2)
                Echelle_de_remuneration = TableauData(3)
                Corps = TableauData(4)
                Groupe = TableauData(5)
                Filiere = TableauData(6)
                Select Case TableauData(7)
                    Case Is = ""
                        TableauData(7) = "0"
                End Select
                Duree_du_stage = CInt(TableauData(7))
                Descriptif = TableauData(8)
                Conditions_d_acces = TableauData(9)
                Secteur = TableauData(10)
                For IndiceI = 11 To 12
                    Select Case TableauData(IndiceI)
                        Case ""
                            TableauData(IndiceI) = "0"
                    End Select
                Next IndiceI
                Tri_du_corps = CInt(TableauData(11))
                Tri_du_grade = CInt(TableauData(12))
                Nomenclature = TableauData(13)
                Gestion = TableauData(14)
                Ena = TableauData(15)
                Corpsparticulier = TableauData(16)
                Dateouverture = TableauData(17)
                Datefermeture = TableauData(18)
                Codecorps = TableauData(19)
                If TableauData.Count > 21 Then
                    CodeNNE = TableauData(20)
                    CodeADAGE = TableauData(21)
                End If

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesRegles() As List(Of GRD_AVANCEMENT_GRADE)
            Get
                If WsLstAvancement Is Nothing Then
                    Return Nothing
                End If
                Return WsLstAvancement.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesGrilles() As List(Of GRD_GRILLE)
            Get
                If WsLstGrilles Is Nothing OrElse WsLstGrilles.Count = 0 Then
                    Return Nothing
                End If
                Return WsLstGrilles.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Grille(ByVal Fiche As GRD_GRILLE) As Integer
            If WsLstGrilles Is Nothing Then
                WsLstGrilles = New List(Of GRD_GRILLE)
            End If
            WsLstGrilles.Add(Fiche)
            Return WsLstGrilles.Count
        End Function

        Public Function Fiche_Grille_Precise(ByVal DateValeur As String) As GRD_GRILLE
            If WsLstGrilles Is Nothing OrElse WsLstGrilles.Count = 0 Then
                Return Nothing
            End If
            Return (From it As GRD_GRILLE In ListedesGrilles Where EstEgal(DateValeur, it.Date_de_Valeur) Select it).FirstOrDefault()
        End Function

        Public Function Fiche_Grille_Valable(ByVal DateValeur As String) As GRD_GRILLE
            If WsLstGrilles Is Nothing OrElse WsLstGrilles.Count = 0 Then
                Return Nothing
            End If
            Return (From it As GRD_GRILLE In ListedesGrilles Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) Select it).FirstOrDefault()
        End Function

        Public Function Ajouter_RegleAvancement(ByVal Fiche As GRD_AVANCEMENT_GRADE) As Integer
            If WsLstAvancement Is Nothing Then
                WsLstAvancement = New List(Of GRD_AVANCEMENT_GRADE)
            End If
            WsLstAvancement.Add(Fiche)
            Return WsLstAvancement.Count
        End Function

        Public Function Fiche_RegleAvancement(ByVal DateValeur As String) As GRD_AVANCEMENT_GRADE
            If WsLstAvancement Is Nothing OrElse WsLstAvancement.Count = 0 Then
                Return Nothing
            End If
            Return (From it As GRD_AVANCEMENT_GRADE In ListedesRegles Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) Select it).FirstOrDefault()
        End Function
    End Class
End Namespace
