Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class GEO_DESCRIPTIF
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsIdentification As String
        Private WsAdresse As String
        Private WsTelephone As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "GEO_DESCRIPTIF"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueSiteGeo
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Identification() As String
            Get
                Return WsIdentification
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsIdentification = value
                    Case Else
                        WsIdentification = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property Adresse() As String
            Get
                Return WsAdresse
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsAdresse = value
                    Case Else
                        WsAdresse = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property Telephone() As String
            Get
                Return WsTelephone
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsTelephone = value
                    Case Else
                        WsTelephone = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Identification & VI.Tild)
                Chaine.Append(Adresse & VI.Tild)
                Chaine.Append(Telephone)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Identification = TableauData(1)
                Adresse = TableauData(2)
                Telephone = TableauData(3)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
