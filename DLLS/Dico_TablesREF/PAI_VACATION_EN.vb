﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAI_VACATION_EN
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsTauxHoraire_HED As Double = 0
        Private WsNbHeuresPlancher As Double = 0
        Private WsHED_CoursMagistraux As Double = 0
        Private WsHED_TravauxDiriges As Double = 0
        Private WsHED_TravauxPratiques As Double = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAI_VACATION_EN"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePaie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 23
            End Get
        End Property

        Public Property TauxHoraire_HED() As Double
            Get
                Return WsTauxHoraire_HED
            End Get
            Set(ByVal value As Double)
                WsTauxHoraire_HED = value
            End Set
        End Property

        Public Property Nombre_Heures_HED_Plancher() As Double
            Get
                Return WsNbHeuresPlancher
            End Get
            Set(ByVal value As Double)
                WsNbHeuresPlancher = value
            End Set
        End Property

        Public Property EquivalentHED_CoursMagistraux() As Double
            Get
                Return WsHED_CoursMagistraux
            End Get
            Set(ByVal value As Double)
                WsHED_CoursMagistraux = value
            End Set
        End Property

        Public Property EquivalentHED_TravauxDiriges() As Double
            Get
                Return WsHED_TravauxDiriges
            End Get
            Set(ByVal value As Double)
                WsHED_TravauxDiriges = value
            End Set
        End Property

        Public Property EquivalentHED_TravauxPratiques() As Double
            Get
                Return WsHED_TravauxPratiques
            End Get
            Set(ByVal value As Double)
                WsHED_TravauxPratiques = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(TauxHoraire_HED.ToString & VI.Tild)
                Chaine.Append(Nombre_Heures_HED_Plancher.ToString & VI.Tild)
                Chaine.Append(EquivalentHED_CoursMagistraux.ToString & VI.Tild)
                Chaine.Append(EquivalentHED_TravauxDiriges.ToString & VI.Tild)
                Chaine.Append(EquivalentHED_TravauxPratiques.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then TauxHoraire_HED = VirRhFonction.ConversionDouble(TableauData(2))
                If TableauData(3) <> "" Then Nombre_Heures_HED_Plancher = VirRhFonction.ConversionDouble(TableauData(3))
                If TableauData(4) <> "" Then EquivalentHED_CoursMagistraux = VirRhFonction.ConversionDouble(TableauData(4))
                If TableauData(5) <> "" Then EquivalentHED_TravauxDiriges = VirRhFonction.ConversionDouble(TableauData(5))
                If TableauData(6) <> "" Then EquivalentHED_TravauxPratiques = VirRhFonction.ConversionDouble(TableauData(6))

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

