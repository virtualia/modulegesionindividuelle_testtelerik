﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ETA_PARAMETRES
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsIntituleN1 As String
        Private WsIntituleN2 As String
        Private WsIntituleN3 As String
        Private WsIntituleN4 As String
        Private WsIntituleN5 As String
        Private WsIntituleN6 As String
        Private WsDroitCongeN1 As String
        Private WsDroitCongeN2 As String
        Private WsDroitCongeN3 As String
        Private WsDroitCongeN4 As String
        Private WsDroitCongeN5 As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ETA_PARAMETRES"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueEtablissement
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 9
            End Get
        End Property

        Public Property Intitule_Niveau1() As String
            Get
                Return WsIntituleN1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsIntituleN1 = value
                    Case Else
                        WsIntituleN1 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Intitule_Niveau2() As String
            Get
                Return WsIntituleN2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsIntituleN2 = value
                    Case Else
                        WsIntituleN2 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Intitule_Niveau3() As String
            Get
                Return WsIntituleN3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsIntituleN3 = value
                    Case Else
                        WsIntituleN3 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Intitule_Niveau4() As String
            Get
                Return WsIntituleN4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsIntituleN4 = value
                    Case Else
                        WsIntituleN4 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Intitule_Niveau5() As String
            Get
                Return WsIntituleN5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsIntituleN5 = value
                    Case Else
                        WsIntituleN5 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Intitule_Niveau6() As String
            Get
                Return WsIntituleN6
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsIntituleN6 = value
                    Case Else
                        WsIntituleN6 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Intitule_CompteurConge1() As String
            Get
                Return WsDroitCongeN1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsDroitCongeN1 = value
                    Case Else
                        WsDroitCongeN1 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Intitule_CompteurConge2() As String
            Get
                Return WsDroitCongeN2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsDroitCongeN2 = value
                    Case Else
                        WsDroitCongeN2 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Intitule_CompteurConge3() As String
            Get
                Return WsDroitCongeN3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsDroitCongeN3 = value
                    Case Else
                        WsDroitCongeN3 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Intitule_CompteurConge4() As String
            Get
                Return WsDroitCongeN4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsDroitCongeN4 = value
                    Case Else
                        WsDroitCongeN4 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Intitule_CompteurConge5() As String
            Get
                Return WsDroitCongeN5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsDroitCongeN5 = value
                    Case Else
                        WsDroitCongeN5 = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Intitule_Niveau1 & VI.Tild)
                Chaine.Append(Intitule_Niveau2 & VI.Tild)
                Chaine.Append(Intitule_Niveau3 & VI.Tild)
                Chaine.Append(Intitule_Niveau4 & VI.Tild)
                Chaine.Append(Intitule_Niveau5 & VI.Tild)
                Chaine.Append(Intitule_Niveau6 & VI.Tild)
                Chaine.Append(Intitule_CompteurConge1 & VI.Tild)
                Chaine.Append(Intitule_CompteurConge2 & VI.Tild)
                Chaine.Append(Intitule_CompteurConge3 & VI.Tild)
                Chaine.Append(Intitule_CompteurConge4 & VI.Tild)
                Chaine.Append(Intitule_CompteurConge5)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 13 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Intitule_Niveau1 = TableauData(2)
                Intitule_Niveau2 = TableauData(3)
                Intitule_Niveau3 = TableauData(4)
                Intitule_Niveau4 = TableauData(5)
                Intitule_Niveau5 = TableauData(6)
                Intitule_Niveau6 = TableauData(7)
                Intitule_CompteurConge1 = TableauData(8)
                Intitule_CompteurConge2 = TableauData(9)
                Intitule_CompteurConge3 = TableauData(10)
                Intitule_CompteurConge4 = TableauData(11)
                Intitule_CompteurConge5 = TableauData(12)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

