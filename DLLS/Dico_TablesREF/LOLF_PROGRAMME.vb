﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class LOLF_PROGRAMME
        Inherits VIR_FICHE

        Private WsLstActions As List(Of LOLF_ACTION)
        Private WsLstBOPs As List(Of LOLF_BOP)
        Private WsLstTrieeActions As New List(Of LOLF_ACTION)
        Private WsLstTrieeBOPs As New List(Of LOLF_BOP)
        Private WsFicheLue As StringBuilder
        '
        Private WsNumero As Integer = 0
        Private WsIntitule As String
        Private WsDateCreation As String
        Private WsDescriptif As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "LOLF_PROGRAMME"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueLolf
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Numero() As Integer
            Get
                Return WsNumero
            End Get
            Set(ByVal value As Integer)
                WsNumero = value
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property DatedeCreation() As String
            Get
                Return WsDateCreation
            End Get
            Set(ByVal value As String)
                WsDateCreation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsDescriptif = value
                    Case Else
                        WsDescriptif = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Numero.ToString & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(DatedeCreation & VI.Tild)
                Chaine.Append(Descriptif)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 5 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) <> "" Then Numero = CInt(TableauData(1))
                Intitule = TableauData(2)
                DatedeCreation = TableauData(3)
                Descriptif = TableauData(4)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Fiche_Action(ByVal Intitule As String) As LOLF_ACTION

            If WsLstActions Is Nothing Then
                Return Nothing
            End If

            Return (From it As LOLF_ACTION In WsLstActions Where it.Intitule = Intitule Select it).FirstOrDefault()

        End Function

        Public Function Fiche_Action(ByVal NumProgramme As Integer, ByVal NumAction As String) As LOLF_ACTION

            If WsLstActions Is Nothing Then
                Return Nothing
            End If

            Return (From it As LOLF_ACTION In WsLstActions
                    Where it.NumeroduProgramme = NumProgramme And it.NumerodelAction = NumAction
                    Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_Action(ByVal Fiche As LOLF_ACTION) As Integer

            If WsLstActions Is Nothing Then
                WsLstActions = New List(Of LOLF_ACTION)
            End If
            WsLstActions.Add(Fiche)
            Return WsLstActions.Count

        End Function

        Public Function ListedesActions(ByVal CritereTri As String) As List(Of LOLF_ACTION)

            If WsLstActions Is Nothing Then
                Return Nothing
            End If
            Select Case CritereTri
                Case Is = "Intitulé"
                    Return WsLstActions.OrderBy(Function(m) m.Intitule).ToList
                Case Is = "Numero"
                    WsLstTrieeActions = (From instance In WsLstActions Select instance Order By instance.NumeroduProgramme Ascending, instance.NumerodelAction Ascending).ToList
                    Return WsLstTrieeActions
                Case Else
                    Return WsLstActions
            End Select

        End Function

        Public Function Fiche_BOP(ByVal NumProgramme As Integer, ByVal NumBOP As Integer) As LOLF_BOP

            If WsLstBOPs Is Nothing Then
                Return Nothing
            End If

            Return (From it As LOLF_BOP In WsLstBOPs Where it.NumeroduProgramme = NumProgramme And it.NumeroduBOP = NumBOP Select it).FirstOrDefault()

        End Function

        Public Function Fiche_BOP(ByVal Intitule As String) As LOLF_BOP

            If WsLstBOPs Is Nothing Then
                Return Nothing
            End If

            Return (From it As LOLF_BOP In WsLstBOPs Where it.Intitule = Intitule Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_BOP(ByVal FicheBOP As LOLF_BOP) As Integer

            If WsLstBOPs Is Nothing Then
                WsLstBOPs = New List(Of LOLF_BOP)
            End If
            WsLstBOPs.Add(FicheBOP)
            Return WsLstBOPs.Count

        End Function

        Public Function ListedesBOPs(ByVal CritereTri As String) As List(Of LOLF_BOP)

            If WsLstBOPs Is Nothing Then
                Return Nothing
            End If
            Select Case CritereTri
                Case Is = "Intitulé"
                    Return WsLstBOPs.OrderBy(Function(m) m.Intitule).ToList
                Case Is = "Numero"
                    WsLstTrieeBOPs = (From instance In WsLstBOPs Select instance Order By instance.NumeroduProgramme Ascending, instance.NumeroduBOP Ascending).ToList
                    Return WsLstTrieeBOPs
                Case Else
                    Return WsLstTrieeBOPs
            End Select

        End Function


    End Class
End Namespace

