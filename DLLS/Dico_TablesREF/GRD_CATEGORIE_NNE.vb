﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class GRD_CATEGORIE_NNE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsMinistere As Integer
        Private WsCategorie As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "GRD_CATEGORIE_NNE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueGrilles
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 3
            End Get
        End Property

        Public Property Ministere As Integer
            Get
                Return WsMinistere
            End Get
            Set(ByVal value As Integer)
                WsMinistere = value
                MyBase.Clef = value.ToString
            End Set
        End Property

        Public Property Categorie As Integer
            Get
                Return WsCategorie
            End Get
            Set(ByVal value As Integer)
                WsCategorie = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Ministere.ToString & VI.Tild)
                Chaine.Append(Categorie.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 3 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Ministere = CInt(F_FormatNumerique(TableauData(1)))
                Categorie = CInt(F_FormatNumerique(TableauData(2)))

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property CodeGEST As String
            Get
                Return Ministere.ToString & Categorie.ToString
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
