﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class DROITS_CONGE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsDroitAnnuel_CA As Double = 0
        Private WsDroitMensuel_CA As Double = 0
        Private WsDebutPeriode_CA As String
        Private WsFinPeriode_CA As String
        Private WsFinPeriode_Fractionnement As String
        Private WsDebutPeriode_Fractionnement As String
        Private WsDroit_Ouverture_Fraction1 As Double = 0
        Private WsDroit_Fermeture_Fraction1 As Double = 0
        Private WsDroit_Fraction1 As Double = 0
        Private WsDroit_Ouverture_Fraction2 As Double = 0
        Private WsDroit_Fraction2 As Double = 0
        Private WsDroit_RTT As Double = 0
        Private WsDebutPeriode_RTT As String
        Private WsFinPeriode_RTT As String
        Private WsSiCalculPredictif As String
        Private WsPeriode_LimiteReports As String
        'Traitement
        Private TsDateActivation As String
        Private TsDebutFirstPeriodeLegaleCA As String
        Private TsFinFirstPeriodeLegaleCA As String
        Private TsDebutSecondPeriodeLegaleCA As String
        Private TsFinSecondPeriodeLegaleCA As String
        Private TsDateDebutCA As String
        Private TsDateFinCa As String
        Private TsDateDebutHP As String
        Private TsDateFinHP As String
        Private TsDateDebutHP1 As String
        Private TsDateFinHP1 As String
        Private TsDateDebutHP2 As String
        Private TsDateFinHP2 As String
        Private TsDateDebutARTT As String
        Private TsDateFinARTT As String
        Private TsDateFinReports As String
        Private TsSiCalculHP As Boolean
        Private TsSiPredictif As Boolean
        '
        Private WsPvue As Integer
        Private WsNumObjet As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "DROITS_CONGE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return WsPvue
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return WsNumObjet
            End Get
        End Property

        Public Property Droit_Annuel_CA() As Double
            Get
                Return WsDroitAnnuel_CA
            End Get
            Set(ByVal value As Double)
                WsDroitAnnuel_CA = value
            End Set
        End Property

        Public Property Droit_Mensuel_CA() As Double
            Get
                Return WsDroitMensuel_CA
            End Get
            Set(ByVal value As Double)
                WsDroitMensuel_CA = value
            End Set
        End Property

        Public Property DebutPeriode_CA() As String
            Get
                Return WsDebutPeriode_CA
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsDebutPeriode_CA = value
                    Case Else
                        WsDebutPeriode_CA = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property FinPeriode_CA() As String
            Get
                Return WsFinPeriode_CA
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsFinPeriode_CA = value
                    Case Else
                        WsFinPeriode_CA = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property FinPeriode_Fractionnement() As String
            Get
                Return WsFinPeriode_Fractionnement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsFinPeriode_Fractionnement = value
                    Case Else
                        WsFinPeriode_Fractionnement = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property DebutPeriode_Fractionnement() As String
            Get
                Return WsDebutPeriode_Fractionnement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsDebutPeriode_Fractionnement = value
                    Case Else
                        WsDebutPeriode_Fractionnement = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Ouverture_Droit_Fraction1() As Double
            Get
                Return WsDroit_Ouverture_Fraction1
            End Get
            Set(ByVal value As Double)
                WsDroit_Ouverture_Fraction1 = value
            End Set
        End Property

        Public Property Fermeture_Droit_Fraction1() As Double
            Get
                Return WsDroit_Fermeture_Fraction1
            End Get
            Set(ByVal value As Double)
                WsDroit_Fermeture_Fraction1 = value
            End Set
        End Property

        Public Property Droit_Conge_Fraction1() As Double
            Get
                Return WsDroit_Fraction1
            End Get
            Set(ByVal value As Double)
                WsDroit_Fraction1 = value
            End Set
        End Property

        Public Property Ouverture_Droit_Fraction2() As Double
            Get
                Return WsDroit_Ouverture_Fraction2
            End Get
            Set(ByVal value As Double)
                WsDroit_Ouverture_Fraction2 = value
            End Set
        End Property

        Public Property Droit_Conge_Fraction2() As Double
            Get
                Return WsDroit_Fraction2
            End Get
            Set(ByVal value As Double)
                WsDroit_Fraction2 = value
            End Set
        End Property

        Public Property Droit_RTT() As Double
            Get
                Return WsDroit_RTT
            End Get
            Set(ByVal value As Double)
                WsDroit_RTT = value
            End Set
        End Property

        Public Property DebutPeriode_RTT() As String
            Get
                Return WsDebutPeriode_RTT
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsDebutPeriode_RTT = value
                    Case Else
                        WsDebutPeriode_RTT = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property FinPeriode_RTT() As String
            Get
                Return WsFinPeriode_RTT
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsFinPeriode_RTT = value
                    Case Else
                        WsFinPeriode_RTT = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property SiCalculPredictif() As String
            Get
                Return WsSiCalculPredictif
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsSiCalculPredictif = "Non"
                    Case Is = "0", "Non"
                        WsSiCalculPredictif = "Non"
                    Case Else
                        WsSiCalculPredictif = "Oui"
                End Select
            End Set
        End Property

        Public Property Periode_LimiteReport() As String
            Get
                Return WsPeriode_LimiteReports
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsPeriode_LimiteReports = value
                    Case Else
                        WsPeriode_LimiteReports = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Select Case Droit_Annuel_CA
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Droit_Annuel_CA.ToString) & VI.Tild)
                End Select
                Select Case Droit_Mensuel_CA
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Droit_Mensuel_CA.ToString) & VI.Tild)
                End Select
                Chaine.Append(DebutPeriode_CA & VI.Tild)
                Chaine.Append(FinPeriode_CA & VI.Tild)
                Chaine.Append(FinPeriode_Fractionnement & VI.Tild)
                Chaine.Append(DebutPeriode_Fractionnement & VI.Tild)
                Select Case Ouverture_Droit_Fraction1
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Ouverture_Droit_Fraction1.ToString) & VI.Tild)
                End Select
                Select Case Fermeture_Droit_Fraction1
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Fermeture_Droit_Fraction1.ToString) & VI.Tild)
                End Select
                Select Case Droit_Conge_Fraction1
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Droit_Conge_Fraction1.ToString) & VI.Tild)
                End Select
                Select Case Ouverture_Droit_Fraction2
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Ouverture_Droit_Fraction2.ToString) & VI.Tild)
                End Select
                Select Case Droit_Conge_Fraction2
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Droit_Conge_Fraction2.ToString) & VI.Tild)
                End Select
                Select Case Droit_RTT
                    Case Is = 0
                        Chaine.Append("0" & VI.Tild)
                    Case Else
                        Chaine.Append(VirRhFonction.MontantStocke(Droit_RTT.ToString) & VI.Tild)
                End Select
                Chaine.Append(DebutPeriode_RTT & VI.Tild)
                Chaine.Append(FinPeriode_RTT & VI.Tild)
                Chaine.Append(SiCalculPredictif & VI.Tild)
                Chaine.Append(Periode_LimiteReport)
                Return Chaine.ToString

            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 18 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)

                If TableauData(2) <> "" Then Droit_Annuel_CA = VirRhFonction.ConversionDouble(TableauData(2))
                If TableauData(3) <> "" Then Droit_Mensuel_CA = VirRhFonction.ConversionDouble(TableauData(3))
                DebutPeriode_CA = TableauData(4)
                FinPeriode_CA = TableauData(5)
                FinPeriode_Fractionnement = TableauData(6)
                DebutPeriode_Fractionnement = TableauData(7)
                If TableauData(8) <> "" Then Ouverture_Droit_Fraction1 = VirRhFonction.ConversionDouble(TableauData(8))
                If TableauData(9) <> "" Then Fermeture_Droit_Fraction1 = VirRhFonction.ConversionDouble(TableauData(9))
                If TableauData(10) <> "" Then Droit_Conge_Fraction1 = VirRhFonction.ConversionDouble(TableauData(10))
                If TableauData(11) <> "" Then Ouverture_Droit_Fraction2 = VirRhFonction.ConversionDouble(TableauData(8))
                If TableauData(12) <> "" Then Droit_Conge_Fraction2 = VirRhFonction.ConversionDouble(TableauData(10))
                If TableauData(13) <> "" Then Droit_RTT = VirRhFonction.ConversionDouble(TableauData(13))
                DebutPeriode_RTT = TableauData(14)
                FinPeriode_RTT = TableauData(15)
                SiCalculPredictif = TableauData(16)
                Periode_LimiteReport = TableauData(17)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Property Date_Activation() As String
            Get
                Return TsDateActivation
            End Get
            Set(ByVal value As String)
                TsDateActivation = value
                If TsDateActivation = "" Then
                    TsDateActivation = VirRhDate.DateduJour
                End If
                DeterminerDatesPeriode()
            End Set
        End Property

        Public ReadOnly Property Date_de_Debut_CA() As String
            Get
                Return TsDateDebutCA
            End Get
        End Property

        Public ReadOnly Property Date_de_Fin_CA() As String
            Get
                Return TsDateFinCa
            End Get
        End Property

        Public ReadOnly Property Date_de_Debut_PremierePeriode_Legale_CA() As String
            Get
                Return TsDebutFirstPeriodeLegaleCA
            End Get
        End Property

        Public ReadOnly Property Date_de_Fin_PremierePeriode_Legale_CA() As String
            Get
                Return TsFinFirstPeriodeLegaleCA
            End Get
        End Property

        Public ReadOnly Property Date_de_Debut_SecondePeriode_Legale_CA() As String
            Get
                Return TsDebutSecondPeriodeLegaleCA
            End Get
        End Property

        Public ReadOnly Property Date_de_Fin_SecondePeriode_Legale_CA() As String
            Get
                Return TsFinSecondPeriodeLegaleCA
            End Get
        End Property

        Public ReadOnly Property Date_de_Debut_RTT() As String
            Get
                Return TsDateDebutARTT
            End Get
        End Property

        Public ReadOnly Property Date_de_Fin_RTT() As String
            Get
                Return TsDateFinARTT
            End Get
        End Property

        Public ReadOnly Property Date_Limite_des_Reports() As String
            Get
                Return TsDateFinReports
            End Get
        End Property

        Public ReadOnly Property SiActiver_LeCalculPredictif() As Boolean
            Get
                Select Case VirRhDate.ComparerDates(TsDateActivation, TsDateFinHP2)
                    Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                        Return TsSiPredictif
                    Case Else
                        Return False
                End Select
            End Get
        End Property

        Public ReadOnly Property SiCalculer_HorsPeriode() As Boolean
            Get
                Return TsSiCalculHP
            End Get
        End Property

        Public ReadOnly Property Date_Debut_HorsPeriode() As String
            Get
                Return TsDateDebutHP
            End Get
        End Property

        Public ReadOnly Property Date_Fin_HorsPeriode() As String
            Get
                Return TsDateFinHP
            End Get
        End Property

        Public ReadOnly Property Date_Debut_T1_HorsPeriode() As String
            Get
                Return TsDateDebutHP1
            End Get
        End Property

        Public ReadOnly Property Date_Fin_T1_HorsPeriode() As String
            Get
                Return TsDateFinHP1
            End Get
        End Property

        Public ReadOnly Property Date_Debut_T2_HorsPeriode() As String
            Get
                Return TsDateDebutHP2
            End Get
        End Property

        Public ReadOnly Property Date_Fin_T2_HorsPeriode() As String
            Get
                Return TsDateFinHP2
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal NumPointdeVue As Integer, ByVal NumObjet As Integer)
            Me.New()
            WsPvue = NumPointdeVue
            WsNumObjet = NumObjet
        End Sub

        Private Sub DeterminerDatesPeriode()
            Dim DateD As String
            Dim DateF As String
            '
            DateD = DebutPeriode_CA & "/" & Strings.Right(TsDateActivation, 4)
            DateF = FinPeriode_CA & "/" & Strings.Right(TsDateActivation, 4)



            Select Case VirRhDate.ComparerDates(DateD, DateF)
                Case VI.ComparaisonDates.PlusGrand 'Cas du secteur privé du 01/06 au 31/05
                    Select Case VirRhDate.ComparerDates(TsDateActivation, DateD)
                        Case VI.ComparaisonDates.PlusPetit
                            TsDateDebutCA = DebutPeriode_CA & "/" & Strings.Trim(Val(Strings.Right(TsDateActivation, 4)) - 1)
                            TsDateFinCa = DateF
                        Case Else
                            TsDateDebutCA = DateD
                            TsDateFinCa = FinPeriode_CA & "/" & Strings.Trim(Val(Strings.Right(TsDateActivation, 4)) + 1)
                    End Select
                    TsDateDebutHP = DebutPeriode_Fractionnement & "/" & Strings.Right(TsDateDebutCA, 4)
                    TsDateFinHP = FinPeriode_Fractionnement & "/" & Strings.Right(TsDateDebutCA, 4)
                    Select Case VirRhDate.ComparerDates(TsDateDebutHP, TsDateDebutCA)
                        Case VI.ComparaisonDates.PlusPetit
                            TsDateDebutHP1 = VirRhDate.CalcDateMoinsJour(FinPeriode_Fractionnement & "/" & Strings.Right(TsDateDebutCA, 4), "1", "0")
                            TsDateFinHP1 = VirRhDate.CalcDateMoinsJour(DebutPeriode_Fractionnement & "/" & Strings.Right(TsDateFinCa, 4), "0", "1")
                            TsDateDebutHP2 = ""
                            TsDateFinHP2 = ""
                        Case Else
                            TsDateDebutHP1 = TsDateDebutCA
                            TsDateFinHP1 = VirRhDate.CalcDateMoinsJour(TsDateDebutHP, "0", "1")
                            TsDateDebutHP2 = VirRhDate.CalcDateMoinsJour(FinPeriode_Fractionnement & "/" & Strings.Right(TsDateDebutCA, 4), "1", "0")
                            TsDateFinHP2 = TsDateFinCa
                    End Select
                Case Else  'Cas de l'année civile du 01/01 au 31/12
                    TsDateDebutCA = DateD
                    TsDateFinCa = DateF
                    TsDateDebutHP1 = TsDateDebutCA
                    TsDateFinHP1 = VirRhDate.CalcDateMoinsJour(DebutPeriode_Fractionnement & "/" & Strings.Right(TsDateActivation, 4), "0", "1")
                    TsDateDebutHP2 = VirRhDate.CalcDateMoinsJour(FinPeriode_Fractionnement & "/" & Strings.Right(TsDateActivation, 4), "1", "0")
                    TsDateFinHP2 = TsDateFinCa
                    TsDateDebutHP = DebutPeriode_Fractionnement & "/" & Strings.Right(TsDateActivation, 4)
                    TsDateFinHP = FinPeriode_Fractionnement & "/" & Strings.Right(TsDateActivation, 4)
            End Select

            'Calcul de la période légale de prise de congés
            TsDebutFirstPeriodeLegaleCA = ""
            TsFinFirstPeriodeLegaleCA = ""
            TsDebutSecondPeriodeLegaleCA = ""
            TsFinSecondPeriodeLegaleCA = ""
            If TsDateDebutHP2 = "" And TsDateFinHP2 = "" Then
                TsDebutFirstPeriodeLegaleCA = TsDateDebutCA
                TsFinFirstPeriodeLegaleCA = VirRhDate.CalcDateMoinsJour(TsDateDebutHP1, "0", "1")
                TsDebutSecondPeriodeLegaleCA = VirRhDate.CalcDateMoinsJour(TsDateFinHP1, "1", "0")
                TsFinSecondPeriodeLegaleCA = TsDateFinCa
            Else
                TsDebutFirstPeriodeLegaleCA = VirRhDate.CalcDateMoinsJour(TsDateFinHP1, "1", "0")
                TsFinFirstPeriodeLegaleCA = VirRhDate.CalcDateMoinsJour(TsDateDebutHP2, "0", "1")
                TsDebutSecondPeriodeLegaleCA = ""
                TsFinSecondPeriodeLegaleCA = ""
            End If

            'ARTT
            DateD = DebutPeriode_RTT & "/" & Strings.Right(TsDateActivation, 4)
            DateF = FinPeriode_RTT & "/" & Strings.Right(TsDateActivation, 4)
            Select Case VirRhDate.ComparerDates(DateD, DateF)
                Case VI.ComparaisonDates.PlusGrand 'Cas du  01/06 au 31/05
                    Select Case VirRhDate.ComparerDates(TsDateActivation, DateD)
                        Case VI.ComparaisonDates.PlusPetit
                            TsDateDebutARTT = DebutPeriode_RTT & "/" & Strings.Trim(Val(Strings.Right(TsDateActivation, 4)) - 1)
                            TsDateFinARTT = DateF
                        Case Else
                            TsDateDebutARTT = DateD
                            TsDateFinARTT = FinPeriode_RTT & "/" & Strings.Trim(Val(Strings.Right(TsDateActivation, 4)) + 1)
                    End Select
                Case Else  'Cas de l'année civile du 01/01 au 31/12
                    TsDateDebutARTT = DebutPeriode_RTT & "/" & Strings.Right(TsDateFinCa, 4)
                    TsDateFinARTT = FinPeriode_RTT & "/" & Strings.Right(TsDateFinCa, 4)
            End Select

            TsDateFinReports = ""
            If (Periode_LimiteReport <> "") Then
                TsDateFinReports = Periode_LimiteReport & "/" & Strings.Right(TsDateFinCa, 4)
            End If

            TsSiPredictif = (SiCalculPredictif = "Oui")
            TsSiCalculHP = (DebutPeriode_Fractionnement <> "" And FinPeriode_Fractionnement <> "")

        End Sub
    End Class

    Public Class ETA_CONGE_ANNUEL
        Inherits DROITS_CONGE

        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "ETA_CONGE_ANNUEL"
            End Get
        End Property
        Public Sub New()
            MyBase.New(VI.PointdeVue.PVueEtablissement, 5)
        End Sub
    End Class

    Public Class PAI_CONGE_ANNUEL
        Inherits DROITS_CONGE

        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "PAI_CONGE_ANNUEL"
            End Get
        End Property
        Public Sub New()
            MyBase.New(VI.PointdeVue.PVuePaie, 16)
        End Sub
    End Class
End Namespace



