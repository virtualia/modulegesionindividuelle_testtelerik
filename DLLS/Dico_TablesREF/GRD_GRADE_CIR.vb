﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class GRD_GRADE_CIR
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsType As String
        Private WsCode As String
        Private WsIntitule As String
        Private WsCompte As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "GRD_GRADE_CIR"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueGrades
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 3
            End Get
        End Property

        Public Property Type_Grade() As Integer
            Get
                Return WsType
            End Get
            Set(ByVal value As Integer)
                WsType = value
            End Set
        End Property

        Public Property CodeNomenclature() As String
            Get
                Return WsCode
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsCode = value
                    Case Else
                        WsCode = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 88
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 88)
                End Select
            End Set
        End Property

        Public Property Compte() As String
            Get
                Return WsCompte
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1
                        WsCompte = value
                    Case Else
                        WsCompte = Strings.Left(value, 1)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Type_Grade.ToString & VI.Tild)
                Chaine.Append(CodeNomenclature & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Compte)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then Type_Grade = CInt(TableauData(2))
                CodeNomenclature = TableauData(3)
                Intitule = TableauData(4)
                MyBase.Date_de_Fin = TableauData(5)
                Compte = TableauData(6)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

