Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class REG_DEFINITION
        Inherits VIR_FICHE
        Implements ICloneable

        Private WsFicheLue As StringBuilder
        Private WsSiMajuscule As Boolean
        '
        Private WsIntitule As String
        Private WsThemathique As String
        Private WsFamille As String
        Private WsNumero_Regle As String
        Private WsDescriptif As String
        Private WsReferences As String
        Private WsNature As String
        Private WsType As String
        Private WsNiveau_Gravite As String

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueRegles
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                WsIntitule = F_FormatAlpha(value, 200)
            End Set
        End Property

        Public Property Themathique() As String
            Get
                Return WsThemathique
            End Get
            Set(ByVal value As String)
                WsThemathique = F_FormatAlpha(value, 200)
            End Set
        End Property

        Public Property Famille() As String
            Get
                Return WsFamille
            End Get
            Set(ByVal value As String)
                WsFamille = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Numero_Regle() As String
            Get
                Return WsNumero_Regle
            End Get
            Set(ByVal value As String)
                WsNumero_Regle = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                WsDescriptif = F_FormatAlpha(value, 300)
            End Set
        End Property

        Public Property References() As String
            Get
                Return WsReferences
            End Get
            Set(ByVal value As String)
                WsReferences = F_FormatAlpha(value, 200)
            End Set
        End Property

        Public Property Nature() As String
            Get
                Return WsNature
            End Get
            Set(ByVal value As String)
                WsNature = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Type() As String
            Get
                Return WsType
            End Get
            Set(ByVal value As String)
                WsType = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Property Niveau_Gravite() As String
            Get
                Return WsNiveau_Gravite
            End Get
            Set(ByVal value As String)
                WsNiveau_Gravite = F_FormatAlpha(value, 50)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Themathique & VI.Tild)
                Chaine.Append(Famille & VI.Tild)
                Chaine.Append(Numero_Regle & VI.Tild)
                Chaine.Append(Descriptif & VI.Tild)
                Chaine.Append(References & VI.Tild)
                Chaine.Append(Nature & VI.Tild)
                Chaine.Append(Type & VI.Tild)
                Chaine.Append(Niveau_Gravite)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)

                Ide_Dossier = CInt(TableauData(0))

                Intitule = TableauData(1)
                Themathique = TableauData(2)
                Famille = TableauData(3)
                Numero_Regle = TableauData(4)
                Descriptif = TableauData(5)
                References = TableauData(6)
                Nature = TableauData(7)
                Type = TableauData(8)
                Niveau_Gravite = TableauData(9)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To UBound(TableauData) - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return ""
                    Case 1
                        Return WsIntitule
                    Case 2
                        Return WsThemathique
                    Case 3
                        Return WsFamille
                    Case 4
                        Return WsNumero_Regle
                    Case 5
                        Return WsDescriptif
                    Case 6
                        Return WsReferences
                    Case 7
                        Return WsNature
                    Case 8
                        Return WsType
                    Case 9
                        Return WsNiveau_Gravite
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
        Public Sub New(ByVal Contenu As String)
            MyBase.New(Contenu)
        End Sub

        Public Overridable Function Clone() As Object Implements ICloneable.Clone
            Return New REG_DEFINITION(Ide_Dossier.ToString & ContenuTable)
        End Function

    End Class
End Namespace

