﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class EDITION_FILTRE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsNumeroLigneIndice As Integer
        Private WsValeurFiltre As String
        Private WsNoInformation As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "EDITION_FILTRE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueScriptEdition
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 3
            End Get
        End Property

        Public Property Numero_LigneIndice() As Integer
            Get
                Return WsNumeroLigneIndice
            End Get
            Set(ByVal value As Integer)
                WsNumeroLigneIndice = value
            End Set
        End Property

        Public Property ValeurFiltre() As String
            Get
                Return WsValeurFiltre
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 128
                        WsValeurFiltre = value
                    Case Else
                        WsValeurFiltre = Strings.Left(value, 128)
                End Select
            End Set
        End Property

        Public Property Numero_Information() As Integer
            Get
                Return WsNoInformation
            End Get
            Set(ByVal value As Integer)
                WsNoInformation = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(CStr(Numero_LigneIndice) & VI.Tild)
                Chaine.Append(ValeurFiltre & VI.Tild)
                Chaine.Append(CStr(Numero_Information))

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) = "" Then TableauData(1) = "0"
                Numero_LigneIndice = CInt(TableauData(1))
                ValeurFiltre = TableauData(2)
                If TableauData(3) = "" Then TableauData(3) = "0"
                Numero_Information = CInt(TableauData(3))

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property V_NumeroLigne() As Integer
            Get
                Dim Numalpha = Numero_LigneIndice.ToString
                If Numalpha.Length = 4 Then
                    Return CInt(Strings.Left(Numalpha, 2))
                Else
                    Return 0
                End If
            End Get
        End Property

        Public ReadOnly Property V_NumeroIndice() As Integer
            Get
                Dim Numalpha = Numero_LigneIndice.ToString
                If Numalpha.Length = 4 Then
                    Return CInt(Strings.Right(Numalpha, 2))
                Else
                    Return 0
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
