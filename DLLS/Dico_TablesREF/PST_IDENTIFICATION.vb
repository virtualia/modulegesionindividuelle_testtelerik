﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PST_IDENTIFICATION
        Inherits VIR_FICHE

        Private WsLstAffectations As List(Of PST_ORGANIGRAMME)
        Private WsLstBudgets As List(Of PST_BUDGETAIRE)
        Private WsFicheLue As StringBuilder
        '
        Private WsNumero_du_poste As String
        Private WsIntitule As String
        Private WsDateouverture As String
        Private WsDatefermeture As String
        Private WsEmploi_type As String
        Private WsDatepublication_interne As String
        Private WsDatepublication_externe As String
        Private WsDateconcours As String
        Private WsDateprevisionnelle_arrivee As String
        Private WsDescriptif As String
        Private WsBonification As Integer
        Private WsOrdre_preseance As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PST_IDENTIFICATION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePosteFct
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Numero_du_poste() As String
            Get
                Return WsNumero_du_poste
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNumero_du_poste = value
                    Case Else
                        WsNumero_du_poste = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 150
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 150)
                End Select
            End Set
        End Property

        Public Property Dateouverture() As String
            Get
                Return WsDateouverture
            End Get
            Set(ByVal value As String)
                WsDateouverture = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = WsDateouverture
            End Set
        End Property

        Public Property Datefermeture() As String
            Get
                Return WsDatefermeture
            End Get
            Set(ByVal value As String)
                WsDatefermeture = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Fin = WsDatefermeture
            End Set
        End Property

        Public Property Emploi_type() As String
            Get
                Return WsEmploi_type
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsEmploi_type = value
                    Case Else
                        WsEmploi_type = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Datepublication_interne() As String
            Get
                Return WsDatepublication_interne
            End Get
            Set(ByVal value As String)
                WsDatepublication_interne = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Datepublication_externe() As String
            Get
                Return WsDatepublication_externe
            End Get
            Set(ByVal value As String)
                WsDatepublication_externe = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Dateconcours() As String
            Get
                Return WsDateconcours
            End Get
            Set(ByVal value As String)
                WsDateconcours = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Dateprevisionnelle_arrivee() As String
            Get
                Return WsDateprevisionnelle_arrivee
            End Get
            Set(ByVal value As String)
                WsDateprevisionnelle_arrivee = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 6000
                        WsDescriptif = value
                    Case Else
                        WsDescriptif = Strings.Left(value, 6000)
                End Select
            End Set
        End Property

        Public Property Bonification() As Integer
            Get
                Return WsBonification
            End Get
            Set(ByVal value As Integer)
                WsBonification = value
            End Set
        End Property

        Public Property Ordre_preseance() As Integer
            Get
                Return WsOrdre_preseance
            End Get
            Set(ByVal value As Integer)
                WsOrdre_preseance = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Numero_du_poste & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Dateouverture & VI.Tild)
                Chaine.Append(Datefermeture & VI.Tild)
                Chaine.Append(Emploi_type & VI.Tild)
                Chaine.Append(Datepublication_interne & VI.Tild)
                Chaine.Append(Datepublication_externe & VI.Tild)
                Chaine.Append(Dateconcours & VI.Tild)
                Chaine.Append(Dateprevisionnelle_arrivee & VI.Tild)
                Chaine.Append(Descriptif & VI.Tild)
                Chaine.Append(Bonification.ToString & VI.Tild)
                Chaine.Append(Ordre_preseance.ToString)

                Return Chaine.ToString
            End Get

            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer
                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 13 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Numero_du_poste = TableauData(1)
                Intitule = TableauData(2)
                Dateouverture = TableauData(3)
                Datefermeture = TableauData(4)
                Emploi_type = TableauData(5)
                Datepublication_interne = TableauData(6)
                Datepublication_externe = TableauData(7)
                Dateconcours = TableauData(8)
                Dateprevisionnelle_arrivee = TableauData(9)
                Descriptif = TableauData(10)
                If TableauData(11) = "" Then
                    TableauData(11) = "0"
                End If
                Bonification = CInt(TableauData(11))
                If TableauData(12) = "" Then
                    TableauData(12) = "0"
                End If
                Ordre_preseance = CInt(TableauData(12))

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesAffectations() As List(Of PST_ORGANIGRAMME)
            Get
                If WsLstAffectations Is Nothing Then
                    Return Nothing
                End If
                Return WsLstAffectations.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesBudgets() As List(Of PST_BUDGETAIRE)
            Get
                If WsLstBudgets Is Nothing Then
                    Return Nothing
                End If
                Return WsLstBudgets.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function SiEmploiEnGestion(ByVal DateValeur As String) As Boolean

            Select Case VirRhDate.ComparerDates(DateValeur, MyBase.Date_de_Valeur)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                    If MyBase.Date_de_Fin = "" Then
                        Return True
                    End If
                    Select Case VirRhDate.ComparerDates(DateValeur, MyBase.Date_de_Fin)
                        Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                            Return True
                    End Select
            End Select
            Return False

        End Function

        Public Function Ajouter_Affectation(ByVal Fiche As PST_ORGANIGRAMME) As Integer

            If WsLstAffectations Is Nothing Then
                WsLstAffectations = New List(Of PST_ORGANIGRAMME)
            End If
            WsLstAffectations.Add(Fiche)
            Return WsLstAffectations.Count

        End Function

        Public Function Fiche_Affectation(ByVal DateValeur As String) As PST_ORGANIGRAMME

            If WsLstAffectations Is Nothing Then
                Return Nothing
            End If

            Return (From it As PST_ORGANIGRAMME In ListedesAffectations Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_Budget(ByVal Fiche As PST_BUDGETAIRE) As Integer

            If WsLstBudgets Is Nothing Then
                WsLstBudgets = New List(Of PST_BUDGETAIRE)
            End If
            WsLstBudgets.Add(Fiche)
            Return WsLstBudgets.Count

        End Function

        Public Function Fiche_Budget(ByVal DateValeur As String) As PST_BUDGETAIRE

            If WsLstBudgets Is Nothing Then
                Return Nothing
            End If

            Return (From it As PST_BUDGETAIRE In ListedesBudgets Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) Select it).FirstOrDefault()

        End Function

        Public Function Affectation_Organigramme(ByVal DateValeur As String, ByVal Niveau As Integer) As String

            If DateValeur = "" Then
                DateValeur = VirRhDate.DateduJour
            End If
            Dim FicheValable As PST_ORGANIGRAMME = Fiche_Affectation(DateValeur)
            If FicheValable Is Nothing Then
                Return ""
            End If
            Select Case Niveau
                Case 1
                    Return FicheValable.Niveau1
                Case 2
                    Return FicheValable.Niveau2
                Case 3
                    Return FicheValable.Niveau3
                Case 4
                    Return FicheValable.Niveau4
                Case 5
                    Return FicheValable.Niveau5
                Case 6
                    Return FicheValable.Niveau6
                Case Else
                    Return FicheValable.Sitegeographique
            End Select

        End Function


    End Class
End Namespace
