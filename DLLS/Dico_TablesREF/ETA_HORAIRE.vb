﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ETA_HORAIRE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsHoraireHebdo As String
        Private WsHoraireMensuel As String
        Private WsHoraireJournalier As String
        Private WsHeureDebut_Plage1 As String
        Private WsHeureFin_Plage1 As String
        Private WsHeureDebut_Plage2 As String
        Private WsHeureFin_Plage2 As String
        '
        Private TsDate_de_fin As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ETA_HORAIRE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueEtablissement
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property HoraireHebdomadaire() As String
            Get
                Return WsHoraireHebdo
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHoraireHebdo = value
                    Case Else
                        WsHoraireHebdo = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property HoraireMensuel() As String
            Get
                Return WsHoraireMensuel
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHoraireMensuel = value
                    Case Else
                        WsHoraireMensuel = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property HoraireJournalier() As String
            Get
                Return WsHoraireJournalier
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsHoraireJournalier = value
                    Case Else
                        WsHoraireJournalier = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Heure_Debut_Plage1() As String
            Get
                Return WsHeureDebut_Plage1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureDebut_Plage1 = value
                    Case Else
                        WsHeureDebut_Plage1 = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Heure_Fin_Plage1() As String
            Get
                Return WsHeureFin_Plage1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureFin_Plage1 = value
                    Case Else
                        WsHeureFin_Plage1 = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Heure_Debut_Plage2() As String
            Get
                Return WsHeureDebut_Plage2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureDebut_Plage2 = value
                    Case Else
                        WsHeureDebut_Plage2 = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Heure_Fin_Plage2() As String
            Get
                Return WsHeureFin_Plage2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureFin_Plage2 = value
                    Case Else
                        WsHeureFin_Plage2 = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(HoraireHebdomadaire & VI.Tild)
                Chaine.Append(HoraireMensuel & VI.Tild)
                Chaine.Append(HoraireJournalier & VI.Tild)
                Chaine.Append(Heure_Debut_Plage1 & VI.Tild)
                Chaine.Append(Heure_Fin_Plage1 & VI.Tild)
                Chaine.Append(Heure_Debut_Plage2 & VI.Tild)
                Chaine.Append(Heure_Fin_Plage2)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 9 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                HoraireHebdomadaire = TableauData(2)
                HoraireMensuel = TableauData(3)
                HoraireJournalier = TableauData(4)
                Heure_Debut_Plage1 = TableauData(5)
                Heure_Fin_Plage1 = TableauData(6)
                Heure_Debut_Plage2 = TableauData(7)
                Heure_Fin_Plage2 = TableauData(8)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = ""
            End Set
        End Property

        Public Property VDate_de_Fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace


