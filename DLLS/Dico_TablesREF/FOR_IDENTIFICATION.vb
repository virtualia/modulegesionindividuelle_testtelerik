﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaREF

Namespace ShemaREF
    Public Class FOR_IDENTIFICATION
        Inherits VIR_FICHE

        Private WsLstSessions As List(Of FOR_SESSION)
        Private WsLstCouts As List(Of FOR_COUTS)
        Private WsLstFactures As List(Of FOR_FACTURE)
        Private WsFicheDescriptif As FOR_DESCRIPTIF = Nothing
        Private WsFicheCaracteristique As FOR_CARACTERISTIC = Nothing
        Private WsFicheEval As FOR_EVALUATION = Nothing
        Private WsLstIntervenants As List(Of FOR_INTERVENANT) = Nothing
        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String = ""
        Private WsOrganisme As String = ""
        Private WsDomaine As String = ""
        Private WsTheme As String = ""
        Private WsPlan As String = ""
        Private WsReference As String = ""
        Private WsDateConvention As String = ""
        Private WsDateCloture As String = ""
        Private WsCursus As String = ""
        Private WsOrdonnancementCursus As Integer = 0
        Private WsModule As String = ""
        Private WsDureeHoraire As Integer = 0
        Private WsQuota As Integer = 0
        Private WsUrl As String = ""
        Private WsEtablissement As String = ""
        Private WsRubrique As String = "" 'Sous-thème
        Private WsDateSaisie As String = ""

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "FOR_IDENTIFICATION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueFormation
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Organisme() As String
            Get
                Return WsOrganisme
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsOrganisme = value
                    Case Else
                        WsOrganisme = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Domaine() As String
            Get
                Return WsDomaine
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsDomaine = value
                    Case Else
                        WsDomaine = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Theme() As String
            Get
                Return WsTheme
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsTheme = value
                    Case Else
                        WsTheme = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property PlandeFormation() As String
            Get
                Return WsPlan
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsPlan = value
                    Case Else
                        WsPlan = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property ReferenceduStage() As String
            Get
                Return WsReference
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsReference = value
                    Case Else
                        WsReference = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property DateConvention() As String
            Get
                Return WsDateConvention
            End Get
            Set(ByVal value As String)
                WsDateConvention = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_Cloture() As String
            Get
                Return WsDateCloture
            End Get
            Set(ByVal value As String)
                WsDateCloture = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Cursus() As String
            Get
                Return WsCursus
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsCursus = value
                    Case Else
                        WsCursus = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Ordonnancement_Cursus() As Integer
            Get
                Return WsOrdonnancementCursus
            End Get
            Set(ByVal value As Integer)
                WsOrdonnancementCursus = value
            End Set
        End Property

        Public Property ModuledeFormation() As String
            Get
                Return WsModule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsModule = value
                    Case Else
                        WsModule = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property DureeHoraire() As Integer
            Get
                Return WsDureeHoraire
            End Get
            Set(ByVal value As Integer)
                WsDureeHoraire = value
            End Set
        End Property

        Public Property Quota() As Integer
            Get
                Return WsQuota
            End Get
            Set(ByVal value As Integer)
                WsQuota = value
            End Set
        End Property

        Public Property UrlduStage() As String
            Get
                Return WsUrl
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsUrl = value
                    Case Else
                        WsUrl = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Etablissement() As String
            Get
                Return WsEtablissement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsEtablissement = value
                    Case Else
                        WsEtablissement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Rubrique() As String
            Get
                Return WsRubrique
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsRubrique = value
                    Case Else
                        WsRubrique = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property DateSaisie() As String
            Get
                Return WsDateSaisie
            End Get
            Set(ByVal value As String)
                WsDateSaisie = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Organisme & VI.Tild)
                Chaine.Append(Domaine & VI.Tild)
                Chaine.Append(Theme & VI.Tild)
                Chaine.Append(PlandeFormation & VI.Tild)
                Chaine.Append(ReferenceduStage & VI.Tild)
                Chaine.Append(DateConvention & VI.Tild)
                Chaine.Append(Date_de_Cloture & VI.Tild)
                Chaine.Append(Cursus & VI.Tild)
                Chaine.Append(Ordonnancement_Cursus.ToString & VI.Tild)
                Chaine.Append(ModuledeFormation & VI.Tild)
                Chaine.Append(DureeHoraire.ToString & VI.Tild)
                Chaine.Append(Quota.ToString & VI.Tild)
                Chaine.Append(UrlduStage & VI.Tild)
                Chaine.Append(Etablissement & VI.Tild)
                Chaine.Append(Rubrique & VI.Tild)
                Chaine.Append(DateSaisie)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 18 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                Organisme = TableauData(2)
                Domaine = TableauData(3)
                Theme = TableauData(4)
                PlandeFormation = TableauData(5)
                ReferenceduStage = TableauData(6)
                DateConvention = TableauData(7)
                Date_de_Cloture = TableauData(8)
                Cursus = TableauData(9)
                If TableauData(10) <> "" Then Ordonnancement_Cursus = CInt(TableauData(10))
                ModuledeFormation = TableauData(11)
                If TableauData(12) <> "" Then DureeHoraire = CInt(TableauData(12))
                If TableauData(13) <> "" Then Quota = CInt(TableauData(13))
                UrlduStage = TableauData(14)
                If TableauData(15) <> "" Then
                    Etablissement = TableauData(15)
                Else
                    Etablissement = "Commun"
                End If
                Rubrique = TableauData(16)
                DateSaisie = TableauData(17)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesFactures() As List(Of FOR_FACTURE)
            Get
                If WsLstFactures Is Nothing Then
                    Return Nothing
                End If
                Return WsLstFactures.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesIntervenants() As List(Of FOR_INTERVENANT)
            Get
                If WsLstIntervenants Is Nothing Then
                    Return Nothing
                End If
                Return WsLstIntervenants.OrderBy(Function(m) m.Intervenant).ToList
            End Get
        End Property

        Public Property FicheDescriptif() As FOR_DESCRIPTIF
            Get
                Return WsFicheDescriptif
            End Get
            Set(ByVal value As FOR_DESCRIPTIF)
                WsFicheDescriptif = value
            End Set
        End Property

        Public Property FicheCaracteristique() As FOR_CARACTERISTIC
            Get
                Return WsFicheCaracteristique
            End Get
            Set(ByVal value As FOR_CARACTERISTIC)
                WsFicheCaracteristique = value
            End Set
        End Property

        Public Property FicheEvaluation() As FOR_EVALUATION
            Get
                Return WsFicheEval
            End Get
            Set(ByVal value As FOR_EVALUATION)
                WsFicheEval = value
            End Set
        End Property

        Public ReadOnly Property ListedesSessions() As List(Of FOR_SESSION)
            Get
                If WsLstSessions Is Nothing Then
                    Return Nothing
                End If
                Return WsLstSessions.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesCouts() As List(Of FOR_COUTS)
            Get
                If WsLstCouts Is Nothing Then
                    Return Nothing
                End If
                Return WsLstCouts.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public Function Ajouter_Session(ByVal Fiche As FOR_SESSION) As Integer

            If WsLstSessions Is Nothing Then
                WsLstSessions = New List(Of FOR_SESSION)
            End If

            WsLstSessions.Add(Fiche)

            Return WsLstSessions.Count

        End Function

        Public Function Fiche_Session_Precise(ByVal DateValeur As String) As FOR_SESSION
            If WsLstSessions Is Nothing Then
                Return Nothing
            End If

            Return (From s As FOR_SESSION In WsLstSessions Where s.Date_de_Valeur = DateValeur Select s).FirstOrDefault()
        End Function

        Public Function Fiche_Session_Valable(ByVal DateValeur As String) As FOR_SESSION
            If WsLstSessions Is Nothing Then
                Return Nothing
            End If

            Return (From s As FOR_SESSION In ListedesSessions Where EstPlusGrand_Ou_Egal(DateValeur, s.Date_de_Valeur) And EstPlusPetit_Ou_Egal(DateValeur, s.Date_de_Valeur) Select s).FirstOrDefault()

        End Function

        Public Function Ajouter_Cout(ByVal Fiche As FOR_COUTS) As Integer

            If WsLstCouts Is Nothing Then
                WsLstCouts = New List(Of FOR_COUTS)
            End If
            WsLstCouts.Add(Fiche)
            Return WsLstCouts.Count

        End Function

        Public Function Fiche_Cout(ByVal DateValeur As String) As FOR_COUTS

            If WsLstCouts Is Nothing Then
                Return Nothing
            End If

            Return (From c As FOR_COUTS In ListedesCouts Where EstPlusGrand_Ou_Egal(DateValeur, c.DateValeurcouts) Select c).FirstOrDefault()

        End Function

        Public Function Ajouter_Facture(ByVal Fiche As FOR_FACTURE) As Integer

            If WsLstFactures Is Nothing Then
                WsLstFactures = New List(Of FOR_FACTURE)
            End If
            WsLstFactures.Add(Fiche)
            Return WsLstFactures.Count

        End Function

        Public Function Ajouter_Intervenant(ByVal Fiche As FOR_INTERVENANT) As Integer
            If WsLstIntervenants Is Nothing Then
                WsLstIntervenants = New List(Of FOR_INTERVENANT)
            End If
            WsLstIntervenants.Add(Fiche)
            Return WsLstIntervenants.Count
        End Function

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace


