﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ETA_COMPTE_RENDU
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsHeureDebut As String
        Private WsHeureFin As String
        Private WsTypeCR As String
        Private WsNomFichier As String
        Private WsNbDossiers As Integer
        Private WsDateFichier As String
        Private WsObservations As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ETA_COMPTE_RENDU"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueEtablissement
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 12
            End Get
        End Property

        Public Property Heure_Debut() As String
            Get
                Return WsHeureDebut
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureDebut = value
                    Case Else
                        WsHeureDebut = Strings.Left(value, 5)
                End Select
                MyBase.Clef = WsHeureDebut
            End Set
        End Property

        Public Property Heure_Fin() As String
            Get
                Return WsHeureFin
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureFin = value
                    Case Else
                        WsHeureFin = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Type_CR() As String
            Get
                Return WsTypeCR
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsTypeCR = value
                    Case Else
                        WsTypeCR = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Nom_FichierTraite() As String
            Get
                Return WsNomFichier
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNomFichier = value
                    Case Else
                        WsNomFichier = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Nombre_DossiersTraites() As Integer
            Get
                Return WsNbDossiers
            End Get
            Set(ByVal value As Integer)
                WsNbDossiers = value
            End Set
        End Property

        Public Property Date_Fichier() As String
            Get
                Return WsDateFichier
            End Get
            Set(ByVal value As String)
                WsDateFichier = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Heure_Debut & VI.Tild)
                Chaine.Append(Heure_Fin & VI.Tild)
                Chaine.Append(Type_CR & VI.Tild)
                Chaine.Append(Nom_FichierTraite & VI.Tild)
                Chaine.Append(Nombre_DossiersTraites.ToString & VI.Tild)
                Chaine.Append(Date_Fichier & VI.Tild)
                Chaine.Append(Observations)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 9 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Heure_Debut = TableauData(2)
                Heure_Fin = TableauData(3)
                Type_CR = TableauData(4)
                Nom_FichierTraite = TableauData(5)
                If TableauData(6) <> "" Then Nombre_DossiersTraites = CInt(TableauData(6))
                Date_Fichier = TableauData(7)
                Observations = TableauData(8)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

