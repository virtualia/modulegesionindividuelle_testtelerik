﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ITI_DISTANCE
        Inherits VIR_FICHE
        Private WsFicheLue As StringBuilder
        '
        Private WsVilledepart As String
        Private WsVillearrivee As String
        Private WsDistancekilometrique As Integer
        Private WsDepartement_villedepart As String
        Private WsDepartement_villearrivee As String
        Private WsDatecertification As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ITI_DISTANCE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueItineraire
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Villedepart() As String
            Get
                Return WsVilledepart
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsVilledepart = value
                    Case Else
                        WsVilledepart = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Villearrivee() As String
            Get
                Return WsVillearrivee
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsVillearrivee = value
                    Case Else
                        WsVillearrivee = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Distancekilometrique() As Integer
            Get
                Return WsDistancekilometrique
            End Get
            Set(ByVal value As Integer)
                WsDistancekilometrique = value
            End Set
        End Property

        Public Property Departement_villedepart() As String
            Get
                Return WsDepartement_villedepart
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsDepartement_villedepart = value
                    Case Else
                        WsDepartement_villedepart = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Departement_villearrivee() As String
            Get
                Return WsDepartement_villearrivee
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsDepartement_villearrivee = value
                    Case Else
                        WsDepartement_villearrivee = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Datecertification() As String
            Get
                Return WsDatecertification
            End Get
            Set(ByVal value As String)
                WsDatecertification = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Villedepart & VI.Tild)
                Chaine.Append(Villearrivee & VI.Tild)
                Chaine.Append(Distancekilometrique.ToString & VI.Tild)
                Chaine.Append(Departement_villedepart & VI.Tild)
                Chaine.Append(Departement_villearrivee & VI.Tild)
                Chaine.Append(Datecertification)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))

                Villedepart = TableauData(1)
                Villearrivee = TableauData(2)
                If TableauData(3) <> "" Then Distancekilometrique = CInt(TableauData(3))
                Departement_villedepart = TableauData(4)
                Departement_villearrivee = TableauData(5)
                Datecertification = TableauData(6)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

