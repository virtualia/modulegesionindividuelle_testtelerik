﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAI_REGLE_TYPE_REMUN
        Inherits VIR_FICHE


        Private WsFicheLue As StringBuilder
        '
        Private WsNumObjet As Integer
        Private WsValeur As String



        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAI_REGLE_TYPE_REMUN"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePaie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return WsNumObjet
            End Get
        End Property

        Public Property Valeur() As String
            Get
                Return WsValeur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 240
                        WsValeur = value
                    Case Else
                        WsValeur = Strings.Left(value, 240)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Valeur)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 2 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Valeur = TableauData(1)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal NumObjet As Integer)
            MyBase.New()
            WsNumObjet = NumObjet
        End Sub

    End Class

    Public Class PAI_REGLE_TYPE_REMUN_19
        Inherits PAI_REGLE_TYPE_REMUN

        Public Sub New()
            MyBase.New(Integer.Parse("19"))
        End Sub
    End Class

    Public Class PAI_REGLE_TYPE_REMUN_20
        Inherits PAI_REGLE_TYPE_REMUN

        Public Sub New()
            MyBase.New(Integer.Parse("20"))
        End Sub
    End Class

End Namespace

