﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class HIE_STRUCTURE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsRang As String
        Private WsNoeud_N1 As String
        Private WsCode_N1 As String
        Private WsNoeud_N2 As String
        Private WsCode_N2 As String
        Private WsComplement As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "HIE_STRUCTURE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueTableHie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Rang() As String
            Get
                Return WsRang
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsRang = value
                    Case Else
                        WsRang = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Noeud_N1() As String
            Get
                Return WsNoeud_N1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNoeud_N1 = value
                    Case Else
                        WsNoeud_N1 = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Code_N1() As String
            Get
                Return WsCode_N1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsCode_N1 = value
                    Case Else
                        WsCode_N1 = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Noeud_N2() As String
            Get
                Return WsNoeud_N2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNoeud_N2 = value
                    Case Else
                        WsNoeud_N2 = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Code_N2() As String
            Get
                Return WsCode_N2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsCode_N2 = value
                    Case Else
                        WsCode_N2 = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Complement() As String
            Get
                Return WsComplement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsComplement = value
                    Case Else
                        WsComplement = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Rang & VI.Tild)
                Chaine.Append(Noeud_N1 & VI.Tild)
                Chaine.Append(Code_N1 & VI.Tild)
                Chaine.Append(Noeud_N2 & VI.Tild)
                Chaine.Append(Code_N2 & VI.Tild)
                Chaine.Append(Complement)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Rang = TableauData(1)
                Noeud_N1 = TableauData(2)
                Code_N1 = TableauData(3)
                Noeud_N2 = TableauData(4)
                Code_N2 = TableauData(5)
                Complement = TableauData(6)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
