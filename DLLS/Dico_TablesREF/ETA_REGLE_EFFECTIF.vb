﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ETA_REGLE_EFFECTIF
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsRang As String
        Private WsService As String
        Private WsEmploi As String
        Private WsMnemonique(4) As String
        Private WsEffectifTheorique_Semaine(4) As Double
        Private WsEffectifTheorique_WE_JF(4) As Double
        Private WsObservations As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ETA_REGLE_EFFECTIF"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueEtablissement
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 10
            End Get
        End Property

        Public Property Rang() As String
            Get
                Return WsRang
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 220
                        WsRang = value
                    Case Else
                        WsRang = Strings.Left(value, 220)
                End Select
            End Set
        End Property

        Public Property Service() As String
            Get
                Return WsService
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsService = value
                    Case Else
                        WsService = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Emploi() As String
            Get
                Return WsEmploi
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsEmploi = value
                    Case Else
                        WsEmploi = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Mnemonique(ByVal Index As Integer) As String
            Get
                Select Case Index
                    Case 0 To 4
                        Return WsMnemonique(Index)
                End Select
                Return ""
            End Get
            Set(ByVal value As String)
                Select Case Index
                    Case 0 To 4
                        Select Case value.Length
                            Case Is <= 3
                                WsMnemonique(Index) = value
                            Case Else
                                WsMnemonique(Index) = Strings.Left(value, 3)
                        End Select
                End Select
            End Set
        End Property

        Public Property EffectifTheorique_Semaine(ByVal Index As Integer) As Double
            Get
                Select Case Index
                    Case 0 To 4
                        Return WsEffectifTheorique_Semaine(Index)
                End Select
                Return 0
            End Get
            Set(ByVal value As Double)
                Select Case Index
                    Case 0 To 4
                        WsEffectifTheorique_Semaine(Index) = value
                End Select
            End Set
        End Property

        Public Property EffectifTheorique_WE_JF(ByVal Index As Integer) As Double
            Get
                Select Case Index
                    Case 0 To 4
                        Return WsEffectifTheorique_WE_JF(Index)
                End Select
                Return 0
            End Get
            Set(ByVal value As Double)
                Select Case Index
                    Case 0 To 4
                        WsEffectifTheorique_WE_JF(Index) = value
                End Select
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder
                Dim I As Integer

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Rang & VI.Tild)
                Chaine.Append(Service & VI.Tild)
                Chaine.Append(Emploi & VI.Tild)
                For I = 0 To 4
                    Chaine.Append(Mnemonique(I) & VI.Tild)
                    Chaine.Append(EffectifTheorique_Semaine(I) & VI.Tild)
                    Chaine.Append(EffectifTheorique_WE_JF(I) & VI.Tild)
                Next I
                Chaine.Append(Observations & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer
                Dim IndiceK As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 22 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Rang = TableauData(2)
                Service = TableauData(3)
                Emploi = TableauData(4)
                IndiceK = 0
                For IndiceI = 5 To 20 Step 3
                    Mnemonique(IndiceK) = TableauData(IndiceI)
                    If TableauData(IndiceI + 1) = "" Then
                        TableauData(IndiceI + 1) = "0"
                    End If
                    EffectifTheorique_Semaine(IndiceK) = VirRhFonction.ConversionDouble(TableauData(IndiceI + 1))
                    If TableauData(IndiceI + 2) = "" Then
                        TableauData(IndiceI + 2) = "0"
                    End If
                    EffectifTheorique_WE_JF(IndiceK) = VirRhFonction.ConversionDouble(TableauData(IndiceI + 2))
                    IndiceK += 1
                Next IndiceI

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

