﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAI_OUVRABLE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsJourOuvrable As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAI_OUVRABLE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePaie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 3
            End Get
        End Property

        Public Property JourOuvrable() As String
            Get
                Return WsJourOuvrable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 15
                        WsJourOuvrable = value
                    Case Else
                        WsJourOuvrable = Strings.Left(value, 15)
                End Select
            End Set
        End Property

        Public ReadOnly Property JourOuvrableNumeric() As Integer
            Get
                Select Case JourOuvrable
                    Case Is = "Lundi"
                        Return VI.Jours.Lundi
                    Case Is = "Mardi"
                        Return VI.Jours.Mardi
                    Case Is = "Mercredi"
                        Return VI.Jours.Mercredi
                    Case Is = "Jeudi"
                        Return VI.Jours.Jeudi
                    Case Is = "Vendredi"
                        Return VI.Jours.Vendredi
                    Case Is = "Samedi"
                        Return VI.Jours.Samedi
                    Case Is = "Dimanche"
                        Return VI.Jours.Dimanche
                    Case Else
                        Return VI.Jours.Samedi
                End Select
            End Get
        End Property

        Public ReadOnly Property JourReposNumeric() As Integer
            Get
                Select Case JourOuvrable
                    Case Is = "Lundi"
                        Return VI.Jours.Mardi
                    Case Is = "Mardi"
                        Return VI.Jours.Mercredi
                    Case Is = "Mercredi"
                        Return VI.Jours.Jeudi
                    Case Is = "Jeudi"
                        Return VI.Jours.Vendredi
                    Case Is = "Vendredi"
                        Return VI.Jours.Samedi
                    Case Is = "Samedi"
                        Return VI.Jours.Dimanche
                    Case Is = "Dimanche"
                        Return VI.Jours.Lundi
                    Case Else
                        Return VI.Jours.Lundi
                End Select
            End Get
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(JourOuvrable)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 2 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                JourOuvrable = TableauData(1)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

