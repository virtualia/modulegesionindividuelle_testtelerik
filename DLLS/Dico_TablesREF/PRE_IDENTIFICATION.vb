﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PRE_IDENTIFICATION
        Inherits VIR_FICHE

        Private WsLstAccords As List(Of ACCORD_RTT)
        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsMnemonique As String
        Private WsInfluenceDC As String
        Private WsRegroupement As String
        Private WsDateouverture As String
        Private WsDatefermeture As String
        Private WsInfluenceBadgeage As String
        Private WsSiHeureSup As String
        Private WsCoefficient As Double = 0
        Private WsForfaitHeure As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PRE_IDENTIFICATION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePresences
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Mnemonique() As String
            Get
                Return WsMnemonique
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsMnemonique = value
                    Case Else
                        WsMnemonique = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Influence_DebitCredit() As String
            Get
                Return WsInfluenceDC
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsInfluenceDC = value
                    Case Else
                        WsInfluenceDC = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Regroupement() As String
            Get
                Return WsRegroupement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsRegroupement = value
                    Case Else
                        WsRegroupement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property DateOuverture() As String
            Get
                Return WsDateouverture
            End Get
            Set(ByVal value As String)
                WsDateouverture = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property DateFermeture() As String
            Get
                Return WsDatefermeture
            End Get
            Set(ByVal value As String)
                WsDatefermeture = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Influence_Badgeage() As String
            Get
                Return WsInfluenceBadgeage
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsInfluenceBadgeage = value
                    Case Else
                        WsInfluenceBadgeage = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property SiHeure_Sup() As String
            Get
                Return WsSiHeureSup
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiHeureSup = value
                    Case Else
                        WsSiHeureSup = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Coefficient() As Double
            Get
                Return WsCoefficient
            End Get
            Set(ByVal value As Double)
                WsCoefficient = value
            End Set
        End Property

        Public Property Forfait_Heures() As String
            Get
                Return WsForfaitHeure
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsForfaitHeure = value
                    Case Else
                        WsForfaitHeure = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Mnemonique & VI.Tild)
                Chaine.Append(Influence_DebitCredit & VI.Tild)
                Chaine.Append(Regroupement & VI.Tild)
                Chaine.Append(DateOuverture & VI.Tild)
                Chaine.Append(DateFermeture & VI.Tild)
                Chaine.Append(Influence_Badgeage & VI.Tild)
                Chaine.Append(SiHeure_Sup & VI.Tild)
                Chaine.Append(Coefficient.ToString & VI.Tild)
                Chaine.Append(Forfait_Heures)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                Mnemonique = TableauData(2)
                Influence_DebitCredit = TableauData(3)
                Regroupement = TableauData(4)
                DateOuverture = TableauData(5)
                DateFermeture = TableauData(6)
                Influence_Badgeage = TableauData(7)
                If TableauData.Count > 8 Then
                    SiHeure_Sup = TableauData(8)
                    If TableauData(9) <> "" Then Coefficient = VirRhFonction.ConversionDouble(TableauData(9))
                    Forfait_Heures = TableauData(10)
                End If
                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesAccords() As List(Of ACCORD_RTT)
            Get
                If WsLstAccords Is Nothing Then
                    Return Nothing
                End If
                Return WsLstAccords.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Accord(ByVal Fiche As ACCORD_RTT) As Integer

            If WsLstAccords Is Nothing Then
                WsLstAccords = New List(Of ACCORD_RTT)
            End If
            WsLstAccords.Add(Fiche)
            Return WsLstAccords.Count

        End Function

        Public Function Fiche_Accord(ByVal DateValeur As String) As ACCORD_RTT

            If WsLstAccords Is Nothing Then
                Return Nothing
            End If

            Return (From it As ACCORD_RTT In ListedesAccords Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) Select it).FirstOrDefault()

        End Function


    End Class
End Namespace

