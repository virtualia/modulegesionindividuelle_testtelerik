﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ONIC_MISSION
        Inherits VIR_FICHE

        Private WsLstMesures As List(Of ONIC_MESURE)
        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsDescriptif As String
        Private WsEtablissement As String


        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ONIC_MISSION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueActivite
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsDescriptif = value
                    Case Else
                        WsDescriptif = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property Etablissement() As String
            Get
                Return WsEtablissement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsEtablissement = value
                    Case Else
                        WsEtablissement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Descriptif & VI.Tild)
                Chaine.Append(Etablissement)


                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                Descriptif = TableauData(2)
                If TableauData(3) <> "" Then
                    Etablissement = TableauData(3)
                Else
                    Etablissement = "Commun"
                End If

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesMesures() As List(Of ONIC_MESURE)
            Get
                If WsLstMesures Is Nothing Then
                    Return Nothing
                End If
                Dim LstRes As List(Of ONIC_MESURE)
                LstRes = (From instance In WsLstMesures Select instance
                          Where instance.Rang_Numero > 0
                          Order By instance.Activite Ascending, instance.Rang_Numero Ascending).ToList

                Return LstRes
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Mesure(ByVal Fiche As ONIC_MESURE) As Integer

            If WsLstMesures Is Nothing Then
                WsLstMesures = New List(Of ONIC_MESURE)
            End If
            WsLstMesures.Add(Fiche)
            Return WsLstMesures.Count

        End Function

        Public Function Fiche_Mesure(ByVal Rang As Integer) As ONIC_MESURE

            If WsLstMesures Is Nothing Then
                Return Nothing
            End If

            Return (From it As ONIC_MESURE In WsLstMesures Where it.Rang_Numero = Rang Select it).FirstOrDefault()

        End Function

        Public Function Fiche_Mesure(ByVal Activite As String) As ONIC_MESURE

            If WsLstMesures Is Nothing Then
                Return Nothing
            End If

            Return (From it As ONIC_MESURE In WsLstMesures Where it.Activite = Activite Select it).FirstOrDefault()

        End Function

    End Class
End Namespace
