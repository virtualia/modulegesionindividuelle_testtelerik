﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAI_MISSION
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsDateArrete As String
        Private WsDateJO As String
        Private WsIJ_Repas_Paris As Double = 0
        Private WsIJ_Repas_Province As Double = 0
        Private WsIJ_Nuit_Paris As Double = 0
        Private WsIJ_Nuit_Province As Double = 0
        Private WsIJ_Journee_Paris As Double = 0
        Private WsIJ_Journee_Province As Double = 0
        Private WsIJ_Repas_Local As Double = 0
        Private WsIJ_Nuit_Local As Double = 0
        Private WsIJ_Journee_Local As Double = 0
        Private WsTaux_Abattement_RestoAdm As Double = 0
        Private WsTaux_Abattement_LogementOffert As Double = 0
        Private WsTaux_Abattement_RepasOffert As Double = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAI_MISSION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePaie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 18
            End Get
        End Property

        Public Property Date_Arrete() As String
            Get
                Return WsDateArrete
            End Get
            Set(ByVal value As String)
                WsDateArrete = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_du_JO() As String
            Get
                Return WsDateJO
            End Get
            Set(ByVal value As String)
                WsDateJO = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property IJ_Repas_Paris() As Double
            Get
                Return WsIJ_Repas_Paris
            End Get
            Set(ByVal value As Double)
                WsIJ_Repas_Paris = value
            End Set
        End Property

        Public Property IJ_Repas_Province() As Double
            Get
                Return WsIJ_Repas_Province
            End Get
            Set(ByVal value As Double)
                WsIJ_Repas_Province = value
            End Set
        End Property

        Public Property IJ_Nuit_Paris() As Double
            Get
                Return WsIJ_Nuit_Paris
            End Get
            Set(ByVal value As Double)
                WsIJ_Nuit_Paris = value
            End Set
        End Property

        Public Property IJ_Nuit_Province() As Double
            Get
                Return WsIJ_Nuit_Province
            End Get
            Set(ByVal value As Double)
                WsIJ_Nuit_Province = value
            End Set
        End Property

        Public Property IJ_Journee_Paris() As Double
            Get
                Return WsIJ_Journee_Paris
            End Get
            Set(ByVal value As Double)
                WsIJ_Journee_Paris = value
            End Set
        End Property

        Public Property IJ_Journee_Province() As Double
            Get
                Return WsIJ_Journee_Province
            End Get
            Set(ByVal value As Double)
                WsIJ_Journee_Province = value
            End Set
        End Property

        Public Property IJ_Repas_AccordLocal() As Double
            Get
                Return WsIJ_Repas_Local
            End Get
            Set(ByVal value As Double)
                WsIJ_Repas_Local = value
            End Set
        End Property

        Public Property IJ_Nuit_AccordLocal() As Double
            Get
                Return WsIJ_Nuit_Local
            End Get
            Set(ByVal value As Double)
                WsIJ_Nuit_Local = value
            End Set
        End Property

        Public Property IJ_Journee_AccordLocal() As Double
            Get
                Return WsIJ_Journee_Local
            End Get
            Set(ByVal value As Double)
                WsIJ_Journee_Local = value
            End Set
        End Property

        Public Property Taux_Abattemenent_RestoAdministratif() As Double
            Get
                Return WsTaux_Abattement_RestoAdm
            End Get
            Set(ByVal value As Double)
                WsTaux_Abattement_RestoAdm = value
            End Set
        End Property

        Public Property Taux_Abattemenent_LogementOffert() As Double
            Get
                Return WsTaux_Abattement_LogementOffert
            End Get
            Set(ByVal value As Double)
                WsTaux_Abattement_LogementOffert = value
            End Set
        End Property

        Public Property Taux_Abattemenent_RepasOffert() As Double
            Get
                Return WsTaux_Abattement_RepasOffert
            End Get
            Set(ByVal value As Double)
                WsTaux_Abattement_RepasOffert = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Date_Arrete & VI.Tild)
                Chaine.Append(Date_du_JO & VI.Tild)
                Chaine.Append(IJ_Repas_Paris.ToString & VI.Tild)
                Chaine.Append(IJ_Repas_Province.ToString & VI.Tild)
                Chaine.Append(IJ_Nuit_Paris.ToString & VI.Tild)
                Chaine.Append(IJ_Nuit_Province.ToString & VI.Tild)
                Chaine.Append(IJ_Journee_Paris.ToString & VI.Tild)
                Chaine.Append(IJ_Journee_Province.ToString & VI.Tild)
                Chaine.Append(IJ_Repas_AccordLocal.ToString & VI.Tild)
                Chaine.Append(IJ_Nuit_AccordLocal.ToString & VI.Tild)
                Chaine.Append(IJ_Journee_AccordLocal.ToString & VI.Tild)
                Chaine.Append(Taux_Abattemenent_RestoAdministratif.ToString & VI.Tild)
                Chaine.Append(Taux_Abattemenent_LogementOffert.ToString & VI.Tild)
                Chaine.Append(Taux_Abattemenent_RepasOffert.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 16 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Date_Arrete = TableauData(2)
                Date_du_JO = TableauData(3)
                If TableauData(4) <> "" Then IJ_Repas_Paris = VirRhFonction.ConversionDouble(TableauData(4))
                If TableauData(5) <> "" Then IJ_Repas_Province = VirRhFonction.ConversionDouble(TableauData(5))
                If TableauData(6) <> "" Then IJ_Nuit_Paris = VirRhFonction.ConversionDouble(TableauData(6))
                If TableauData(7) <> "" Then IJ_Nuit_Province = VirRhFonction.ConversionDouble(TableauData(7))
                If TableauData(8) <> "" Then IJ_Journee_Paris = VirRhFonction.ConversionDouble(TableauData(8))
                If TableauData(9) <> "" Then IJ_Journee_Province = VirRhFonction.ConversionDouble(TableauData(9))
                If TableauData(10) <> "" Then IJ_Repas_AccordLocal = VirRhFonction.ConversionDouble(TableauData(10))
                If TableauData(11) <> "" Then IJ_Nuit_AccordLocal = VirRhFonction.ConversionDouble(TableauData(11))
                If TableauData(12) <> "" Then IJ_Nuit_AccordLocal = VirRhFonction.ConversionDouble(TableauData(12))
                If TableauData(13) <> "" Then Taux_Abattemenent_RestoAdministratif = VirRhFonction.ConversionDouble(TableauData(13))
                If TableauData(14) <> "" Then Taux_Abattemenent_LogementOffert = VirRhFonction.ConversionDouble(TableauData(14))
                If TableauData(15) <> "" Then Taux_Abattemenent_RepasOffert = VirRhFonction.ConversionDouble(TableauData(15))

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
