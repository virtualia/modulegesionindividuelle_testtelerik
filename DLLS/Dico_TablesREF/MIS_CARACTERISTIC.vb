﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class MIS_CARACTERISTIC
        Inherits VIR_FICHE

        Private WsLstAffectations As List(Of MIS_AFFECTATION)
        Private WsLstTransports As List(Of MIS_TRANSPORT)
        Private WsFicheLue As StringBuilder
        '
        Private WsNumero As String
        Private WsIntitule As String
        Private WsObjetMission As String
        Private WsDateDebut As String
        Private WsLieu As String
        Private WsSecteurGeo As Integer
        Private WsChapitreBudget As String
        Private WsSousChapitreBudget As String
        Private WsArticleBudget As String
        Private WsEngagement As String
        Private WsCodePays As String
        Private WsTauxChangeChancellerie As Double = 0
        Private WsSiZoneEuro As Integer
        Private WsTauxChangeInverse As Double = 0
        Private WsEtablissement As String
        Private WsObservations As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "MIS_CARACTERISTIC"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueMission
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Numero() As String
            Get
                Return WsNumero
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsNumero = value
                    Case Else
                        WsNumero = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property ObjetdelaMission() As String
            Get
                Return WsObjetMission
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsObjetMission = value
                    Case Else
                        WsObjetMission = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Date_de_Debut() As String
            Get
                Return WsDateDebut
            End Get
            Set(ByVal value As String)
                WsDateDebut = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = value
            End Set
        End Property

        Public Property Lieu() As String
            Get
                Return WsLieu
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsLieu = value
                    Case Else
                        WsLieu = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property SecteurGeographique() As Integer
            Get
                Return WsSecteurGeo
            End Get
            Set(ByVal value As Integer)
                WsSecteurGeo = value
            End Set
        End Property

        Public Property Budget_Chapitre() As String
            Get
                Return WsChapitreBudget
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsChapitreBudget = value
                    Case Else
                        WsChapitreBudget = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Budget_SousChapitre() As String
            Get
                Return WsSousChapitreBudget
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsSousChapitreBudget = value
                    Case Else
                        WsSousChapitreBudget = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Budget_Article() As String
            Get
                Return WsArticleBudget
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsArticleBudget = value
                    Case Else
                        WsArticleBudget = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Engagement() As String
            Get
                Return WsEngagement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsEngagement = value
                    Case Else
                        WsEngagement = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Code_Pays() As String
            Get
                Return WsCodePays
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsCodePays = value
                    Case Else
                        WsCodePays = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property TauxChancellerie() As Double
            Get
                Return WsTauxChangeChancellerie
            End Get
            Set(ByVal value As Double)
                WsTauxChangeChancellerie = value
            End Set
        End Property

        Public Property SiZoneEuro() As Integer
            Get
                Return WsSiZoneEuro
            End Get
            Set(ByVal value As Integer)
                WsSiZoneEuro = value
            End Set
        End Property

        Public Property TauxdeChange() As Double
            Get
                Return WsTauxChangeInverse
            End Get
            Set(ByVal value As Double)
                WsTauxChangeInverse = value
            End Set
        End Property

        Public Property Etablissement() As String
            Get
                Return WsEtablissement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsEtablissement = value
                    Case Else
                        WsEtablissement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Observations() As String
            Get
                Return WsObservations
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 800
                        WsObservations = value
                    Case Else
                        WsObservations = Strings.Left(value, 800)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Numero & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(ObjetdelaMission & VI.Tild)
                Chaine.Append(Date_de_Debut & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin & VI.Tild)
                Chaine.Append(Lieu & VI.Tild)
                Chaine.Append(SecteurGeographique.ToString & VI.Tild)
                Chaine.Append(Budget_Chapitre & VI.Tild)
                Chaine.Append(Budget_SousChapitre & VI.Tild)
                Chaine.Append(Budget_Article & VI.Tild)
                Chaine.Append(Engagement & VI.Tild)
                Chaine.Append(Code_Pays & VI.Tild)
                Chaine.Append(TauxChancellerie.ToString & VI.Tild)
                Chaine.Append(SiZoneEuro.ToString & VI.Tild)
                Chaine.Append(TauxdeChange.ToString & VI.Tild)
                Chaine.Append(Etablissement & VI.Tild)
                Chaine.Append(Observations)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 18 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Numero = TableauData(1)
                Intitule = TableauData(2)
                ObjetdelaMission = TableauData(3)
                Date_de_Debut = TableauData(4)
                MyBase.Date_de_Fin = TableauData(5)
                Lieu = TableauData(6)
                If TableauData(7) = "" Then TableauData(7) = "0"
                SecteurGeographique = CInt(TableauData(7))
                Budget_Chapitre = TableauData(8)
                Budget_SousChapitre = TableauData(9)
                Budget_Article = TableauData(10)
                Engagement = TableauData(11)
                Code_Pays = TableauData(12)
                If TableauData(13) <> "" Then TauxChancellerie = VirRhFonction.ConversionDouble(TableauData(13))
                If TableauData(14) = "" Then TableauData(14) = "0"
                SiZoneEuro = CInt(TableauData(14))
                If TableauData(15) <> "" Then TauxdeChange = VirRhFonction.ConversionDouble(TableauData(15))
                Etablissement = TableauData(16)
                Observations = TableauData(17)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property AnneeMission() As String
            Get
                Return Strings.Right(Date_de_Debut, 4)
            End Get
        End Property

        Public ReadOnly Property MoisMission() As Integer
            Get
                Return CInt(Strings.Mid(Date_de_Debut, 4, 2))
            End Get
        End Property

        Public ReadOnly Property ListedesAffectations() As List(Of MIS_AFFECTATION)
            Get
                If WsLstAffectations Is Nothing Then
                    Return Nothing
                End If
                Return WsLstAffectations.OrderBy(Function(m) m.Rang).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesTransports() As List(Of MIS_TRANSPORT)
            Get
                If WsLstTransports Is Nothing Then
                    Return Nothing
                End If
                Return WsLstTransports.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Affectation(ByVal Fiche As MIS_AFFECTATION) As Integer

            If WsLstAffectations Is Nothing Then
                WsLstAffectations = New List(Of MIS_AFFECTATION)
            End If
            WsLstAffectations.Add(Fiche)
            Return WsLstAffectations.Count

        End Function

        Public Function Fiche_Affectation(ByVal Rang As Integer) As MIS_AFFECTATION

            If WsLstAffectations Is Nothing Then
                Return Nothing
            End If

            Return (From it As MIS_AFFECTATION In ListedesAffectations Where it.Rang = Rang Select it).FirstOrDefault()

        End Function

        Public Function Fiche_Affectation(ByVal Nom As String, ByVal Prenom As String) As MIS_AFFECTATION

            If WsLstAffectations Is Nothing Then
                Return Nothing
            End If

            Return (From it As MIS_AFFECTATION In ListedesAffectations Where it.Nom = Nom And it.Prenom = Prenom Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_Transport(ByVal Fiche As MIS_TRANSPORT) As Integer

            If WsLstTransports Is Nothing Then
                WsLstTransports = New List(Of MIS_TRANSPORT)
            End If
            WsLstTransports.Add(Fiche)
            Return WsLstTransports.Count

        End Function

        Public Function Fiche_Transport(ByVal Rang As Integer) As MIS_TRANSPORT

            If WsLstTransports Is Nothing Then
                Return Nothing
            End If

            Return (From it As MIS_TRANSPORT In ListedesTransports Where it.Numero = Rang Select it).FirstOrDefault()

        End Function

        Public Function Fiche_Transport(ByVal Ide As Integer, ByVal Nature As Integer) As MIS_TRANSPORT

            If WsLstTransports Is Nothing Then
                Return Nothing
            End If

            Return (From it As MIS_TRANSPORT In ListedesTransports Where it.Ide_Personne = Ide And it.Nature_Personne = Nature Select it).FirstOrDefault()

        End Function


    End Class
End Namespace


