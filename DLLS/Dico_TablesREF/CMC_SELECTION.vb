﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class CMC_SELECTION
        Inherits VIR_FICHE
        Private WsLstValeurs As List(Of CMC_VALEURS)
        Private WsFicheLue As StringBuilder
        '
        Private WsNumeroLigne As Integer
        Private WsNumObjet As Integer
        Private WsNumInfo As Integer
        Private WsOperateurComparaison As Integer
        Private WsOptionExperte As Integer
        Private WsValeurExperte As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "CMC_SELECTION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueScriptCmc
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Numero_Ligne() As Integer
            Get
                Return WsNumeroLigne
            End Get
            Set(ByVal value As Integer)
                WsNumeroLigne = value
            End Set
        End Property

        Public Property Numero_ObjetCMC() As Integer
            Get
                Return WsNumObjet
            End Get
            Set(ByVal value As Integer)
                WsNumObjet = value
            End Set
        End Property

        Public Property Numero_InfoCMC() As Integer
            Get
                Return WsNumInfo
            End Get
            Set(ByVal value As Integer)
                WsNumInfo = value
            End Set
        End Property

        Public Property OperateurComparaison() As Integer
            Get
                Return WsOperateurComparaison
            End Get
            Set(ByVal value As Integer)
                WsOperateurComparaison = value
            End Set
        End Property

        Public Property OptionExperte() As Integer
            Get
                Return WsOptionExperte
            End Get
            Set(ByVal value As Integer)
                WsOptionExperte = value
            End Set
        End Property

        Public Property ValeurExperte() As String
            Get
                Return WsValeurExperte
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 6
                        WsValeurExperte = value
                    Case Else
                        WsValeurExperte = Strings.Left(value, 6)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(CStr(Numero_Ligne) & VI.Tild)
                Chaine.Append(CStr(Numero_ObjetCMC) & VI.Tild)
                Chaine.Append(CStr(Numero_InfoCMC) & VI.Tild)
                Chaine.Append(CStr(OperateurComparaison) & VI.Tild)
                Chaine.Append(CStr(OptionExperte) & VI.Tild)
                Chaine.Append(ValeurExperte)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                For IndiceI = 0 To 5
                    If TableauData(IndiceI) = "" Then
                        TableauData(IndiceI) = "0"
                    End If
                Next IndiceI

                Ide_Dossier = CInt(TableauData(0))
                Numero_Ligne = CInt(TableauData(1))
                Numero_ObjetCMC = CInt(TableauData(2))
                Numero_InfoCMC = CInt(TableauData(3))
                OperateurComparaison = CInt(TableauData(4))
                OptionExperte = CInt(TableauData(5))
                ValeurExperte = TableauData(6)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesValeurs() As List(Of CMC_VALEURS)
            Get
                If WsLstValeurs Is Nothing Then
                    Return Nothing
                End If
                Return WsLstValeurs.OrderBy(Function(m) m.Numero_LigneIndice).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub


        Public Function Ajouter_Valeur(ByVal Fiche As CMC_VALEURS) As Integer
            If WsLstValeurs Is Nothing Then
                WsLstValeurs = New List(Of CMC_VALEURS)
            End If
            WsLstValeurs.Add(Fiche)
            Return WsLstValeurs.Count
        End Function

    End Class
End Namespace
