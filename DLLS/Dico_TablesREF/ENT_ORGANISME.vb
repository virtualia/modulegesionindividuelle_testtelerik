﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ENT_ORGANISME
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsRaisonsociale As String
        Private WsNumeroetRue As String
        Private WsCodepostal As String
        Private WsBureaudistributeur As String
        Private WsTelephone As String
        Private WsFax As String
        Private WsImmatriculation As String
        Private WsCodeactivite As String
        Private WsSecteuractivite As String
        Private WsEffectifmoyen As String
        Private WsNumeroagrement As String
        Private WsTierscomptable As String
        Private WsTypeorganisme As String
        Private WsURL As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ENT_ORGANISME"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueEntreprise
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Raisonsociale() As String
            Get
                Return WsRaisonsociale
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsRaisonsociale = value
                    Case Else
                        WsRaisonsociale = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property NumeroetRue() As String
            Get
                Return WsNumeroetRue
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsNumeroetRue = value
                    Case Else
                        WsNumeroetRue = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property Codepostal() As String
            Get
                Return WsCodepostal
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsCodepostal = value
                    Case Else
                        WsCodepostal = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Bureaudistributeur() As String
            Get
                Return WsBureaudistributeur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsBureaudistributeur = value
                    Case Else
                        WsBureaudistributeur = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Telephone() As String
            Get
                Return WsTelephone
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsTelephone = value
                    Case Else
                        WsTelephone = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Fax() As String
            Get
                Return WsFax
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsFax = value
                    Case Else
                        WsFax = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Immatriculation() As String
            Get
                Return WsImmatriculation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsImmatriculation = value
                    Case Else
                        WsImmatriculation = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Codeactivite() As String
            Get
                Return WsCodeactivite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsCodeactivite = value
                    Case Else
                        WsCodeactivite = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Secteuractivite() As String
            Get
                Return WsSecteuractivite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsSecteuractivite = value
                    Case Else
                        WsSecteuractivite = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Effectifmoyen() As String
            Get
                Return WsEffectifmoyen
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsEffectifmoyen = value
                    Case Else
                        WsEffectifmoyen = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Numeroagrement() As String
            Get
                Return WsNumeroagrement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsNumeroagrement = value
                    Case Else
                        WsNumeroagrement = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Tierscomptable() As String
            Get
                Return WsTierscomptable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsTierscomptable = value
                    Case Else
                        WsTierscomptable = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Typeorganisme() As String
            Get
                Return WsTypeorganisme
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsTypeorganisme = value
                    Case Else
                        WsTypeorganisme = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property URL() As String
            Get
                Return WsURL
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsURL = value
                    Case Else
                        WsURL = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Raisonsociale & VI.Tild)
                Chaine.Append(NumeroetRue & VI.Tild)
                Chaine.Append(Codepostal & VI.Tild)
                Chaine.Append(Bureaudistributeur & VI.Tild)
                Chaine.Append(Telephone & VI.Tild)
                Chaine.Append(Fax & VI.Tild)
                Chaine.Append(Immatriculation & VI.Tild)
                Chaine.Append(Codeactivite & VI.Tild)
                Chaine.Append(Secteuractivite & VI.Tild)
                Chaine.Append(Effectifmoyen & VI.Tild)
                Chaine.Append(Numeroagrement & VI.Tild)
                Chaine.Append(Tierscomptable & VI.Tild)
                Chaine.Append(Typeorganisme & VI.Tild)
                Chaine.Append(URL)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 15 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))

                Raisonsociale = TableauData(1)
                NumeroetRue = TableauData(2)
                Codepostal = TableauData(3)
                Bureaudistributeur = TableauData(4)
                Telephone = TableauData(5)
                Fax = TableauData(6)
                Immatriculation = TableauData(7)
                Codeactivite = TableauData(8)
                Secteuractivite = TableauData(9)
                Effectifmoyen = TableauData(10)
                Numeroagrement = TableauData(11)
                Tierscomptable = TableauData(12)
                Typeorganisme = TableauData(13)
                URL = TableauData(14)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

