﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class LIAISON_EXPORT
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        Private WsNoScriptExport As Integer
        Private WsIntituleExport As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "LIAISON_EXPORT"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueLiaisonWord
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Numero_ScriptExport() As Integer
            Get
                Return WsNoScriptExport
            End Get
            Set(ByVal value As Integer)
                WsNoScriptExport = value
            End Set
        End Property

        Public Property IntituleExport() As String
            Get
                Return WsIntituleExport
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 128
                        WsIntituleExport = value
                    Case Else
                        WsIntituleExport = Strings.Left(value, 128)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(CStr(Numero_ScriptExport) & VI.Tild)
                Chaine.Append(IntituleExport)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 3 Then
                    Exit Property
                End If
                For IndiceI = 0 To 1
                    If TableauData(IndiceI) = "" Then TableauData(IndiceI) = "0"
                Next IndiceI
                Ide_Dossier = CInt(TableauData(0))
                Numero_ScriptExport = CInt(TableauData(1))
                IntituleExport = TableauData(2)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

