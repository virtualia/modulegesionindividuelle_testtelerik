﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ORG_RATTACHEMENT
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsStructure_no1 As String
        Private WsTypestructure_no1 As Integer = 0
        Private WsNbrEB_no1 As Double = 0
        Private WsCompteanalytique_no1 As String
        Private WsStructure_no2 As String
        Private WsTypestructure_no2 As Integer = 0
        Private WsNbrEB_no2 As Double = 0
        Private WsCompteanalytique_no2 As String
        Private WsStructure_no3 As String
        Private WsTypestructure_no3 As Integer = 0
        Private WsNbrEB_no3 As Double = 0
        Private WsCompteanalytique_no3 As String
        Private WsStructure_no4 As String
        Private WsTypestructure_no4 As Integer = 0
        Private WsNbrEB_no4 As Double = 0
        Private WsCompteanalytique_no4 As String
        Private WsStructure_no5 As String
        Private WsTypestructure_no5 As Integer = 0
        Private WsNbrEB_no5 As Double = 0
        Private WsCompteanalytique_no5 As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ORG_RATTACHEMENT"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePosteBud
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 3
            End Get
        End Property

        Public Property Structure_no1() As String
            Get
                Return WsStructure_no1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsStructure_no1 = value
                    Case Else
                        WsStructure_no1 = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Typestructure_no1() As Integer
            Get
                Return WsTypestructure_no1
            End Get
            Set(ByVal value As Integer)
                WsTypestructure_no1 = value
            End Set
        End Property

        Public Property NbrEB_no1() As Double
            Get
                Return WsNbrEB_no1
            End Get
            Set(ByVal value As Double)
                WsNbrEB_no1 = value
            End Set
        End Property

        Public Property Compteanalytique_no1() As String
            Get
                Return WsCompteanalytique_no1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsCompteanalytique_no1 = value
                    Case Else
                        WsCompteanalytique_no1 = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Property Structure_no2() As String
            Get
                Return WsStructure_no2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsStructure_no2 = value
                    Case Else
                        WsStructure_no2 = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Typestructure_no2() As Integer
            Get
                Return WsTypestructure_no2
            End Get
            Set(ByVal value As Integer)
                WsTypestructure_no2 = value
            End Set
        End Property

        Public Property NbrEB_no2() As Double
            Get
                Return WsNbrEB_no2
            End Get
            Set(ByVal value As Double)
                WsNbrEB_no2 = value
            End Set
        End Property

        Public Property Compteanalytique_no2() As String
            Get
                Return WsCompteanalytique_no2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsCompteanalytique_no2 = value
                    Case Else
                        WsCompteanalytique_no2 = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Property Structure_no3() As String
            Get
                Return WsStructure_no3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsStructure_no3 = value
                    Case Else
                        WsStructure_no3 = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Typestructure_no3() As Integer
            Get
                Return WsTypestructure_no3
            End Get
            Set(ByVal value As Integer)
                WsTypestructure_no3 = value
            End Set
        End Property

        Public Property NbrEB_no3() As Double
            Get
                Return WsNbrEB_no3
            End Get
            Set(ByVal value As Double)
                WsNbrEB_no3 = value
            End Set
        End Property

        Public Property Compteanalytique_no3() As String
            Get
                Return WsCompteanalytique_no3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsCompteanalytique_no3 = value
                    Case Else
                        WsCompteanalytique_no3 = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Property Structure_no4() As String
            Get
                Return WsStructure_no4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsStructure_no4 = value
                    Case Else
                        WsStructure_no4 = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Typestructure_no4() As Integer
            Get
                Return WsTypestructure_no4
            End Get
            Set(ByVal value As Integer)
                WsTypestructure_no4 = value
            End Set
        End Property

        Public Property NbrEB_no4() As Double
            Get
                Return WsNbrEB_no4
            End Get
            Set(ByVal value As Double)
                WsNbrEB_no4 = value
            End Set
        End Property

        Public Property Compteanalytique_no4() As String
            Get
                Return WsCompteanalytique_no4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsCompteanalytique_no4 = value
                    Case Else
                        WsCompteanalytique_no4 = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Property Structure_no5() As String
            Get
                Return WsStructure_no5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsStructure_no5 = value
                    Case Else
                        WsStructure_no5 = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Typestructure_no5() As Integer
            Get
                Return WsTypestructure_no5
            End Get
            Set(ByVal value As Integer)
                WsTypestructure_no5 = value
            End Set
        End Property

        Public Property NbrEB_no5() As Double
            Get
                Return WsNbrEB_no5
            End Get
            Set(ByVal value As Double)
                WsNbrEB_no5 = value
            End Set
        End Property

        Public Property Compteanalytique_no5() As String
            Get
                Return WsCompteanalytique_no5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsCompteanalytique_no5 = value
                    Case Else
                        WsCompteanalytique_no5 = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Structure_no1 & VI.Tild)
                Chaine.Append(Typestructure_no1.ToString & VI.Tild)
                Chaine.Append(NbrEB_no1.ToString & VI.Tild)
                Chaine.Append(Compteanalytique_no1 & VI.Tild)
                Chaine.Append(Structure_no2 & VI.Tild)
                Chaine.Append(Typestructure_no2.ToString & VI.Tild)
                Chaine.Append(NbrEB_no2.ToString & VI.Tild)
                Chaine.Append(Compteanalytique_no2 & VI.Tild)
                Chaine.Append(Structure_no3 & VI.Tild)
                Chaine.Append(Typestructure_no3.ToString & VI.Tild)
                Chaine.Append(NbrEB_no3.ToString & VI.Tild)
                Chaine.Append(Compteanalytique_no3 & VI.Tild)
                Chaine.Append(Structure_no4 & VI.Tild)
                Chaine.Append(Typestructure_no4.ToString & VI.Tild)
                Chaine.Append(NbrEB_no4.ToString & VI.Tild)
                Chaine.Append(Compteanalytique_no4 & VI.Tild)
                Chaine.Append(Structure_no5 & VI.Tild)
                Chaine.Append(Typestructure_no5.ToString & VI.Tild)
                Chaine.Append(NbrEB_no5.ToString & VI.Tild)
                Chaine.Append(Compteanalytique_no5)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 22 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))

                MyBase.Date_de_Valeur = TableauData(1)
                Structure_no1 = TableauData(2)
                If TableauData(3) <> "" Then Typestructure_no1 = CInt(TableauData(3))
                If TableauData(4) <> "" Then NbrEB_no1 = VirRhFonction.ConversionDouble(TableauData(4))
                Compteanalytique_no1 = TableauData(5)
                Structure_no2 = TableauData(6)
                If TableauData(7) <> "" Then Typestructure_no2 = CInt(TableauData(7))
                If TableauData(8) <> "" Then NbrEB_no2 = VirRhFonction.ConversionDouble(TableauData(8))
                Compteanalytique_no2 = TableauData(9)
                Structure_no3 = TableauData(10)
                If TableauData(11) <> "" Then Typestructure_no3 = CInt(TableauData(11))
                If TableauData(12) <> "" Then NbrEB_no3 = VirRhFonction.ConversionDouble(TableauData(12))
                Compteanalytique_no3 = TableauData(13)
                Structure_no4 = TableauData(14)
                If TableauData(15) <> "" Then Typestructure_no4 = CInt(TableauData(15))
                If TableauData(16) <> "" Then NbrEB_no4 = VirRhFonction.ConversionDouble(TableauData(16))
                Compteanalytique_no4 = TableauData(17)
                Structure_no5 = TableauData(18)
                If TableauData(19) <> "" Then Typestructure_no5 = CInt(TableauData(19))
                If TableauData(20) <> "" Then NbrEB_no5 = VirRhFonction.ConversionDouble(TableauData(20))
                Compteanalytique_no5 = TableauData(21)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace


