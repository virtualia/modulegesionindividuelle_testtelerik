﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class MIS_TRANSPORT
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsNumero As String
        Private WsIde_Personne As Integer = 0
        Private WsNature As Integer
        Private WsDateDepart As String
        Private WsHeureDepart As String
        Private WsDateArrivee As String
        Private WsHeureArrivee As String
        Private WsVilleDepart As String
        Private WsVilleArrivee As String
        Private WsDateNaissance As String
        Private WsSiAllerRetour As Integer
        Private WsDistance As Integer
        Private WsMontant As Double = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "MIS_TRANSPORT"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueMission
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 3
            End Get
        End Property

        Public Property Numero() As String
            Get
                Return WsNumero
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsNumero = value
                    Case Else
                        WsNumero = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Ide_Personne() As Integer
            Get
                Return WsIde_Personne
            End Get
            Set(ByVal value As Integer)
                WsIde_Personne = value
            End Set
        End Property

        Public Property Nature_Personne() As Integer
            Get
                Return WsNature
            End Get
            Set(ByVal value As Integer)
                WsNature = value
            End Set
        End Property

        Public Property Date_de_Depart() As String
            Get
                Return WsDateDepart
            End Get
            Set(ByVal value As String)
                WsDateDepart = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = value
            End Set
        End Property

        Public Property Heure_de_Depart() As String
            Get
                Return WsHeureDepart
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureDepart = value
                    Case Else
                        WsHeureDepart = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Date_Arrivee() As String
            Get
                Return WsDateArrivee
            End Get
            Set(ByVal value As String)
                WsDateArrivee = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Fin = value
            End Set
        End Property

        Public Property Heure_Arrivee() As String
            Get
                Return WsHeureArrivee
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureArrivee = value
                    Case Else
                        WsHeureArrivee = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Ville_de_Depart() As String
            Get
                Return WsVilleDepart
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsVilleDepart = value
                    Case Else
                        WsVilleDepart = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Ville_Arrivee() As String
            Get
                Return WsVilleArrivee
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsVilleArrivee = value
                    Case Else
                        WsVilleArrivee = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property SiAllerRetour() As Integer
            Get
                Return WsSiAllerRetour
            End Get
            Set(ByVal value As Integer)
                WsSiAllerRetour = value
            End Set
        End Property

        Public Property Distance() As Integer
            Get
                Return WsDistance
            End Get
            Set(ByVal value As Integer)
                WsDistance = value
            End Set
        End Property

        Public Property Montant() As Double
            Get
                Return WsMontant
            End Get
            Set(ByVal value As Double)
                WsMontant = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Numero & VI.Tild)
                Chaine.Append(Ide_Personne.ToString & VI.Tild)
                Chaine.Append(Nature_Personne.ToString & VI.Tild)
                Chaine.Append(Date_de_Depart & VI.Tild)
                Chaine.Append(Heure_de_Depart & VI.Tild)
                Chaine.Append(Date_Arrivee & VI.Tild)
                Chaine.Append(Heure_Arrivee & VI.Tild)
                Chaine.Append(Ville_de_Depart & VI.Tild)
                Chaine.Append(Ville_Arrivee & VI.Tild)
                Chaine.Append(SiAllerRetour.ToString & VI.Tild)
                Chaine.Append(Distance.ToString & VI.Tild)
                Chaine.Append(Montant.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 13 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Numero = TableauData(1)
                If TableauData(2) = "" Then TableauData(2) = "0"
                Ide_Personne = CInt(TableauData(2))
                If TableauData(3) = "" Then TableauData(3) = "0"
                Nature_Personne = CInt(TableauData(3))
                Date_de_Depart = TableauData(4)
                Heure_de_Depart = TableauData(5)
                Date_Arrivee = TableauData(6)
                Heure_Arrivee = TableauData(7)
                Ville_de_Depart = TableauData(8)
                Ville_Arrivee = TableauData(9)
                If TableauData(10) = "" Then TableauData(10) = "0"
                SiAllerRetour = CInt(TableauData(10))
                If TableauData(11) = "" Then TableauData(11) = "0"
                Distance = CInt(TableauData(11))
                If TableauData(12) <> "" Then Montant = VirRhFonction.ConversionDouble(TableauData(12))

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace



