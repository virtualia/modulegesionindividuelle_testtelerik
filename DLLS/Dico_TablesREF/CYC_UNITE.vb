﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class CYC_UNITE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsHeureDebut_Plage1 As String
        Private WsHeureFin_Plage1 As String
        Private WsDenomination_Plage1 As String
        Private WsNbHeures_Plage1 As String
        Private WsHeureDebut_Plage2 As String
        Private WsHeureFin_Plage2 As String
        Private WsDenomination_Plage2 As String
        Private WsNbHeures_Plage2 As String
        Private WsNbHeures_Total As String
        '
        Private WsNumObjet As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "CYC_UNITE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueBaseHebdo
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return WsNumObjet
            End Get
        End Property

        Public Property Heure_Debut_Plage1() As String
            Get
                Return WsHeureDebut_Plage1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureDebut_Plage1 = value
                    Case Else
                        WsHeureDebut_Plage1 = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Heure_Fin_Plage1() As String
            Get
                Return WsHeureFin_Plage1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureFin_Plage1 = value
                    Case Else
                        WsHeureFin_Plage1 = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Denomination_Plage1() As String
            Get
                Return WsDenomination_Plage1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsDenomination_Plage1 = value
                    Case Else
                        WsDenomination_Plage1 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property NombreHeures_Plage1() As String
            Get
                Return WsNbHeures_Plage1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsNbHeures_Plage1 = value
                    Case Else
                        WsNbHeures_Plage1 = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Heure_Debut_Plage2() As String
            Get
                Return WsHeureDebut_Plage2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureDebut_Plage2 = value
                    Case Else
                        WsHeureDebut_Plage2 = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Heure_Fin_Plage2() As String
            Get
                Return WsHeureFin_Plage2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureFin_Plage2 = value
                    Case Else
                        WsHeureFin_Plage2 = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Denomination_Plage2() As String
            Get
                Return WsDenomination_Plage2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsDenomination_Plage2 = value
                    Case Else
                        WsDenomination_Plage2 = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property NombreHeures_Plage2() As String
            Get
                Return WsNbHeures_Plage2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsNbHeures_Plage2 = value
                    Case Else
                        WsNbHeures_Plage2 = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property NombreHeures_Total() As String
            Get
                Return WsNbHeures_Total
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsNbHeures_Total = value
                    Case Else
                        WsNbHeures_Total = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Heure_Debut_Plage1 & VI.Tild)
                Chaine.Append(Heure_Fin_Plage1 & VI.Tild)
                Chaine.Append(Denomination_Plage1 & VI.Tild)
                Chaine.Append(NombreHeures_Plage1 & VI.Tild)
                Chaine.Append(Heure_Debut_Plage2 & VI.Tild)
                Chaine.Append(Heure_Fin_Plage2 & VI.Tild)
                Chaine.Append(Denomination_Plage2 & VI.Tild)
                Chaine.Append(NombreHeures_Plage2 & VI.Tild)
                Chaine.Append(NombreHeures_Total)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 10 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Heure_Debut_Plage1 = TableauData(1)
                Heure_Fin_Plage1 = TableauData(2)
                Denomination_Plage1 = TableauData(3)
                NombreHeures_Plage1 = TableauData(4)
                Heure_Debut_Plage2 = TableauData(5)
                Heure_Fin_Plage2 = TableauData(6)
                Denomination_Plage2 = TableauData(7)
                NombreHeures_Plage2 = TableauData(8)
                NombreHeures_Total = TableauData(9)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property NombreHeures_Plage1_Minutes As Integer
            Get
                If WsNbHeures_Plage1 = "" Then
                    Return 0
                End If
                Return Val(VirRhDate.CalcHeure(WsNbHeures_Plage1, "", 1))
            End Get
        End Property

        Public ReadOnly Property NombreHeures_Plage2_Minutes As Integer
            Get
                If WsNbHeures_Plage2 = "" Then
                    Return 0
                End If
                Return Val(VirRhDate.CalcHeure(WsNbHeures_Plage2, "", 1))
            End Get
        End Property

        Public ReadOnly Property NombreHeures_Total_Minutes As Integer
            Get
                If WsNbHeures_Total = "" Then
                    Return 0
                End If
                Return Val(VirRhDate.CalcHeure(WsNbHeures_Total, "", 1))
            End Get
        End Property
        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal NumObjet As Integer)
            Me.New()
            WsNumObjet = NumObjet
        End Sub

    End Class

    Public Class CYC_PREMIER
        Inherits CYC_UNITE

        Public Sub New()
            MyBase.New(2)
        End Sub
        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "CYC_PREMIER"
            End Get
        End Property
    End Class
    Public Class CYC_DEUXIEME
        Inherits CYC_UNITE

        Public Sub New()
            MyBase.New(3)
        End Sub
        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "CYC_DEUXIEME"
            End Get
        End Property
    End Class
    Public Class CYC_TROISIEME
        Inherits CYC_UNITE

        Public Sub New()
            MyBase.New(4)
        End Sub
        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "CYC_TROISIEME"
            End Get
        End Property
    End Class
    Public Class CYC_QUATRIEME
        Inherits CYC_UNITE

        Public Sub New()
            MyBase.New(5)
        End Sub
        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "CYC_QUATRIEME"
            End Get
        End Property
    End Class
    Public Class CYC_CINQUIEME
        Inherits CYC_UNITE

        Public Sub New()
            MyBase.New(6)
        End Sub
        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "CYC_CINQUIEME"
            End Get
        End Property
    End Class
    Public Class CYC_SIXIEME
        Inherits CYC_UNITE

        Public Sub New()
            MyBase.New(7)
        End Sub
        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "CYC_SIXIEME"
            End Get
        End Property
    End Class
    Public Class CYC_SEPTIEME
        Inherits CYC_UNITE

        Public Sub New()
            MyBase.New(8)
        End Sub
        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "CYC_SEPTIEME"
            End Get
        End Property
    End Class
End Namespace


