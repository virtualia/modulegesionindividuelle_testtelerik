﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class CONFIG_CLEF
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsClefRang As String
        Private WsIntitule As String
        Private WsValeur As String
        Private WsDescriptif As String
        '
        Private TsClefSousSection As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "CONFIG_CLEF"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueConfiguration
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 3
            End Get
        End Property

        Public Property Clef_Rang() As String
            Get
                Return WsClefRang
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 160
                        WsClefRang = value
                    Case Else
                        WsClefRang = Strings.Left(value, 160)
                End Select
                MyBase.Clef = WsClefRang
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Valeur() As String
            Get
                Return WsValeur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsValeur = value
                    Case Else
                        WsValeur = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsDescriptif = value
                    Case Else
                        WsDescriptif = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Clef & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Valeur & VI.Tild)
                Chaine.Append(Descriptif)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 5 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Clef = TableauData(1)
                Intitule = TableauData(2)
                Valeur = TableauData(3)
                Descriptif = TableauData(4)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TableauData = Strings.Split(Clef, VI.Tiret, -1)
                TsClefSousSection = TableauData(0)
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Property ClefSousSection() As String
            Get
                Return TsClefSousSection
            End Get
            Set(ByVal value As String)
                TsClefSousSection = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
