﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ABS_ABSENCE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsSiVacanceBudgetaire As String
        Private WsSiVacanceFonction As String
        Private WsCategorie As String
        Private WsMnemonique As String
        Private WsInfluenceDC As String
        Private WsRegroupement As String
        Private WsSiADeduireRepas As String
        Private WsSiDansTauxAbsenteisme As String
        Private WsSiADeduireERM As String
        Private WsDateouverture As String
        Private WsDatefermeture As String
        Private WsReference As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ABS_ABSENCE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueAbsences
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property SiVacance_PosteBudgetaire() As String
            Get
                Select Case WsSiVacanceBudgetaire
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsSiVacanceBudgetaire
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiVacanceBudgetaire = value
                    Case Else
                        WsSiVacanceBudgetaire = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property SiVacance_Fonction() As String
            Get
                Select Case WsSiVacanceFonction
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsSiVacanceFonction
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiVacanceFonction = value
                    Case Else
                        WsSiVacanceFonction = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Categorie() As String
            Get
                Return WsCategorie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsCategorie = value
                    Case Else
                        WsCategorie = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Mnemonique() As String
            Get
                Return WsMnemonique
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 6
                        WsMnemonique = value
                    Case Else
                        WsMnemonique = Strings.Left(value, 6)
                End Select
            End Set
        End Property

        Public Property Influence_DebitCredit() As String
            Get
                Return WsInfluenceDC
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsInfluenceDC = value
                    Case Else
                        WsInfluenceDC = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Regroupement() As String
            Get
                Return WsRegroupement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsRegroupement = value
                    Case Else
                        WsRegroupement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property SiaDeduire_TitreRepas() As String
            Get
                Select Case WsSiADeduireRepas
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsSiADeduireRepas
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiADeduireRepas = value
                    Case Else
                        WsSiADeduireRepas = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property SiParticpe_TauxAbsenteisme() As String
            Get
                Select Case WsSiDansTauxAbsenteisme
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsSiDansTauxAbsenteisme
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiDansTauxAbsenteisme = value
                    Case Else
                        WsSiDansTauxAbsenteisme = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property SiaDeduire_ERM() As String
            Get
                Select Case WsSiADeduireERM
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsSiADeduireERM
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiADeduireERM = value
                    Case Else
                        WsSiADeduireERM = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property DateOuverture() As String
            Get
                Return WsDateouverture
            End Get
            Set(ByVal value As String)
                WsDateouverture = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property DateFermeture() As String
            Get
                Return WsDatefermeture
            End Get
            Set(ByVal value As String)
                WsDatefermeture = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Reference() As String
            Get
                Return WsReference
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsReference = value
                    Case Else
                        WsReference = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(SiVacance_PosteBudgetaire & VI.Tild)
                Chaine.Append(SiVacance_Fonction & VI.Tild)
                Chaine.Append(Categorie & VI.Tild)
                Chaine.Append(Mnemonique & VI.Tild)
                Chaine.Append(Influence_DebitCredit & VI.Tild)
                Chaine.Append(Regroupement & VI.Tild)
                Chaine.Append(SiaDeduire_TitreRepas & VI.Tild)
                Chaine.Append(SiParticpe_TauxAbsenteisme & VI.Tild)
                Chaine.Append(SiaDeduire_ERM & VI.Tild)
                Chaine.Append(DateOuverture & VI.Tild)
                Chaine.Append(DateFermeture & VI.Tild)
                Chaine.Append(Reference)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 13 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                SiVacance_PosteBudgetaire = TableauData(2)
                SiVacance_Fonction = TableauData(3)
                Categorie = TableauData(4)
                Mnemonique = TableauData(5)
                Influence_DebitCredit = TableauData(6)
                Regroupement = TableauData(7)
                SiaDeduire_TitreRepas = TableauData(8)
                SiParticpe_TauxAbsenteisme = TableauData(9)
                SiaDeduire_ERM = TableauData(10)
                DateOuverture = TableauData(11)
                DateFermeture = TableauData(12)
                If TableauData.Count > 13 Then
                    Reference = TableauData(13)
                End If

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property CodeRegroupement As String
            Get
                If Regroupement = "" Then
                    Return ""
                End If
                Dim TableauDecompose(0) As String
                TableauDecompose = Strings.Split(Regroupement, "(")
                If TableauDecompose.Count = 1 Then
                    Return Regroupement
                End If
                Return Strings.Left(TableauDecompose(1), TableauDecompose(1).Length - 1)
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

