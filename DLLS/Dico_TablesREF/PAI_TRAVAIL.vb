﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAI_TRAVAIL
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsHoraireMensuel As Double = 0
        Private WsHoraireHebdo As Double = 0
        Private WsAgeDepartRetraite As Integer = 0
        Private WsAgeMiseRetraite As Integer = 0
        Private WsAncienneteRequiseRetraite As Integer = 0
        Private WsAgeRequisRetraite As Integer = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAI_TRAVAIL"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePaie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 15
            End Get
        End Property

        Public Property Horaire_Mensuel() As Double
            Get
                Return WsHoraireMensuel
            End Get
            Set(ByVal value As Double)
                WsHoraireMensuel = value
            End Set
        End Property

        Public Property Horaire_Hebdomadaire() As Double
            Get
                Return WsHoraireHebdo
            End Get
            Set(ByVal value As Double)
                WsHoraireHebdo = value
            End Set
        End Property

        Public Property Age_Depart_Retraite() As Integer
            Get
                Return WsAgeDepartRetraite
            End Get
            Set(ByVal value As Integer)
                WsAgeDepartRetraite = value
            End Set
        End Property

        Public Property Age_Mise_Retraite() As Integer
            Get
                Return WsAgeMiseRetraite
            End Get
            Set(ByVal value As Integer)
                WsAgeMiseRetraite = value
            End Set
        End Property

        Public Property Anciennete_Recquise_Retraite() As Integer
            Get
                Return WsAncienneteRequiseRetraite
            End Get
            Set(ByVal value As Integer)
                WsAncienneteRequiseRetraite = value
            End Set
        End Property

        Public Property Age_Recquis_Retraite() As Integer
            Get
                Return WsAgeRequisRetraite
            End Get
            Set(ByVal value As Integer)
                WsAgeRequisRetraite = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Horaire_Mensuel.ToString & VI.Tild)
                Chaine.Append(Horaire_Hebdomadaire.ToString & VI.Tild)
                Chaine.Append(Age_Depart_Retraite.ToString & VI.Tild)
                Chaine.Append(Age_Mise_Retraite.ToString & VI.Tild)
                Chaine.Append(Anciennete_Recquise_Retraite.ToString & VI.Tild)
                Chaine.Append(Age_Recquis_Retraite.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then Horaire_Mensuel = VirRhFonction.ConversionDouble(TableauData(2))
                If TableauData(3) <> "" Then Horaire_Hebdomadaire = VirRhFonction.ConversionDouble(TableauData(3))
                If TableauData(4) <> "" Then Age_Depart_Retraite = CInt(TableauData(4))
                If TableauData(5) <> "" Then Age_Mise_Retraite = CInt(TableauData(5))
                If TableauData(6) <> "" Then Anciennete_Recquise_Retraite = CInt(TableauData(6))
                If TableauData(7) <> "" Then Age_Recquis_Retraite = CInt(TableauData(7))

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
