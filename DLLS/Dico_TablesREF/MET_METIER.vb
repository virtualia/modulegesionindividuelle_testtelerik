Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class MET_METIER
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsFamille As String
        Private WsSous_famille As String
        Private WsReference As String
        Private WsCode_administratif As String
        Private WsMetier_porteur As String
        Private WsMetier_sensible As String
        Private WsHorizon As Integer
        Private WsDefinition As String
        Private WsUrl As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "MET_METIER"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueMetier
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Famille() As String
            Get
                Return WsFamille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsFamille = value
                    Case Else
                        WsFamille = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Sous_Famille() As String
            Get
                Return WsSous_famille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsSous_famille = value
                    Case Else
                        WsSous_famille = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Reference() As String
            Get
                Return WsReference
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsReference = value
                    Case Else
                        WsReference = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Code_administratif() As String
            Get
                Return WsCode_administratif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsCode_administratif = value
                    Case Else
                        WsCode_administratif = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Metier_porteur() As String
            Get
                Select Case WsMetier_porteur
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsMetier_porteur
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsMetier_porteur = value
                    Case Else
                        WsMetier_porteur = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Metier_sensible() As String
            Get
                Select Case WsMetier_sensible
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsMetier_sensible
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsMetier_sensible = value
                    Case Else
                        WsMetier_sensible = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Horizon() As Integer
            Get
                Return WsHorizon
            End Get
            Set(ByVal value As Integer)
                WsHorizon = value
            End Set
        End Property

        Public Property Definition() As String
            Get
                Return WsDefinition
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1500
                        WsDefinition = value
                    Case Else
                        WsDefinition = Strings.Left(value, 1500)
                End Select
            End Set
        End Property

        Public Property UrlMetier() As String
            Get
                Return WsUrl
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsUrl = value
                    Case Else
                        WsUrl = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Famille & VI.Tild)
                Chaine.Append(Sous_Famille & VI.Tild)
                Chaine.Append(Reference & VI.Tild)
                Chaine.Append(Code_administratif & VI.Tild)
                Chaine.Append(Metier_porteur & VI.Tild)
                Chaine.Append(Metier_sensible & VI.Tild)
                Chaine.Append(Horizon.ToString & VI.Tild)
                Chaine.Append(Definition & VI.Tild)
                Chaine.Append(UrlMetier)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                Famille = TableauData(2)
                Sous_Famille = TableauData(3)
                Reference = TableauData(4)
                Code_administratif = TableauData(5)
                Metier_porteur = TableauData(6)
                Metier_sensible = TableauData(7)
                Select Case TableauData(8)
                    Case Is = ""
                        TableauData(8) = "1"
                End Select
                Horizon = CInt(TableauData(8))
                Definition = TableauData(9)
                UrlMetier = TableauData(10)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

