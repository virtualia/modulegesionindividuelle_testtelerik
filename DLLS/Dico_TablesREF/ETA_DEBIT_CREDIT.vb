﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ETA_DEBIT_CREDIT
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsNatureBadgeuse As String
        Private WsModeIdentication As String
        Private WsAnneeInstall As Integer = 0
        Private WsTolerance As Integer = 0
        Private WsDelaiRoute As Integer = 0
        Private WsSiPrivilegierReport As String
        Private WsSiReportAnnuel As String
        Private WsPlafondAcquisition As Double = 0
        Private WsPlafondAnnuel As Double = 0
        Private WsSiValoriserPrevu As String
        Private WsSiEcretementHoraireReference As String
        Private WsSiEcretementJour As String
        Private WsTempsComplet As Integer = 0
        Private WsTempsPartiel90 As Integer = 0
        Private WsTempsPartiel80 As Integer = 0
        Private WsTempsPartiel70 As Integer = 0
        Private WsTempsPartiel60 As Integer = 0
        Private WsTempsPartiel50 As Integer = 0
        '
        Private TsDate_de_fin As String



        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ETA_DEBIT_CREDIT"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueEtablissement
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 7
            End Get
        End Property

        Public Property NaturedelaBadgeuse() As String
            Get
                Return WsNatureBadgeuse
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNatureBadgeuse = value
                    Case Else
                        WsNatureBadgeuse = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property ModedIdentification() As String
            Get
                Return WsModeIdentication
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsModeIdentication = value
                    Case Else
                        WsModeIdentication = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property AnneedelInstallation() As Integer
            Get
                Return WsAnneeInstall
            End Get
            Set(ByVal value As Integer)
                WsAnneeInstall = value
            End Set
        End Property

        Public Property Tolerance_Deux_Consecutifs() As Integer
            Get
                Return WsTolerance
            End Get
            Set(ByVal value As Integer)
                WsTolerance = value
            End Set
        End Property

        Public Property Delai_de_Route() As Integer
            Get
                Return WsDelaiRoute
            End Get
            Set(ByVal value As Integer)
                WsDelaiRoute = value
            End Set
        End Property

        Public Property SiPrivilegier_Report_Mensuel() As String
            Get
                Return WsSiPrivilegierReport
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiPrivilegierReport = value
                    Case Else
                        WsSiPrivilegierReport = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property SiReport_Annuel() As String
            Get
                Return WsSiReportAnnuel
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiReportAnnuel = value
                    Case Else
                        WsSiReportAnnuel = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Plafonds_Acquisition() As Double
            Get
                Return WsPlafondAcquisition
            End Get
            Set(ByVal value As Double)
                WsPlafondAcquisition = value
            End Set
        End Property

        Public Property Plafonds_Annuel_Maximum() As Double
            Get
                Return WsPlafondAnnuel
            End Get
            Set(ByVal value As Double)
                WsPlafondAnnuel = value
            End Set
        End Property

        Public Property SiValoriser_Previsionnel() As String
            Get
                Return WsSiValoriserPrevu
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiValoriserPrevu = value
                    Case Else
                        WsSiValoriserPrevu = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property SiEcretement_Sur_HorairedeReference() As String
            Get
                Return WsSiEcretementHoraireReference
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiEcretementHoraireReference = value
                    Case Else
                        WsSiEcretementHoraireReference = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property SiEcretement_Sur_Journee() As String
            Get
                Return WsSiEcretementJour
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiEcretementJour = value
                    Case Else
                        WsSiEcretementJour = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Ecretement_TempsComplet() As Integer
            Get
                Return WsTempsComplet
            End Get
            Set(ByVal value As Integer)
                WsTempsComplet = value
            End Set
        End Property

        Public Property Ecretement_TempsPartiel90() As Integer
            Get
                Return WsTempsPartiel90
            End Get
            Set(ByVal value As Integer)
                WsTempsPartiel90 = value
            End Set
        End Property

        Public Property Ecretement_TempsPartiel80() As Integer
            Get
                Return WsTempsPartiel80
            End Get
            Set(ByVal value As Integer)
                WsTempsPartiel80 = value
            End Set
        End Property

        Public Property Ecretement_TempsPartiel70() As Integer
            Get
                Return WsTempsPartiel70
            End Get
            Set(ByVal value As Integer)
                WsTempsPartiel70 = value
            End Set
        End Property

        Public Property Ecretement_TempsPartiel60() As Integer
            Get
                Return WsTempsPartiel60
            End Get
            Set(ByVal value As Integer)
                WsTempsPartiel60 = value
            End Set
        End Property

        Public Property Ecretement_TempsPartiel50() As Integer
            Get
                Return WsTempsPartiel50
            End Get
            Set(ByVal value As Integer)
                WsTempsPartiel50 = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(NaturedelaBadgeuse & VI.Tild)
                Chaine.Append(ModedIdentification & VI.Tild)
                Chaine.Append(AnneedelInstallation.ToString & VI.Tild)
                Chaine.Append(Tolerance_Deux_Consecutifs.ToString & VI.Tild)
                Chaine.Append(Delai_de_Route.ToString & VI.Tild)
                Chaine.Append(SiPrivilegier_Report_Mensuel & VI.Tild)
                Chaine.Append(SiReport_Annuel & VI.Tild)
                Chaine.Append(Plafonds_Acquisition.ToString & VI.Tild)
                Chaine.Append(Plafonds_Annuel_Maximum.ToString & VI.Tild)
                Chaine.Append(SiValoriser_Previsionnel & VI.Tild)
                Chaine.Append(SiEcretement_Sur_HorairedeReference & VI.Tild)
                Chaine.Append(SiEcretement_Sur_Journee.ToString & VI.Tild)
                Chaine.Append(Ecretement_TempsComplet.ToString & VI.Tild)
                Chaine.Append(Ecretement_TempsPartiel90.ToString & VI.Tild)
                Chaine.Append(Ecretement_TempsPartiel80.ToString & VI.Tild)
                Chaine.Append(Ecretement_TempsPartiel70.ToString & VI.Tild)
                Chaine.Append(Ecretement_TempsPartiel60.ToString & VI.Tild)
                Chaine.Append(Ecretement_TempsPartiel50.ToString & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin)
                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 21 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                NaturedelaBadgeuse = TableauData(2)
                ModedIdentification = TableauData(3)
                If TableauData(4) <> "" Then AnneedelInstallation = CInt(TableauData(4))
                If TableauData(5) <> "" Then Tolerance_Deux_Consecutifs = CInt(TableauData(5))
                If TableauData(6) <> "" Then Delai_de_Route = CInt(TableauData(6))
                SiPrivilegier_Report_Mensuel = TableauData(7)
                SiReport_Annuel = TableauData(8)
                If TableauData(9) <> "" Then Plafonds_Acquisition = VirRhFonction.ConversionDouble(TableauData(9))
                If TableauData(10) <> "" Then Plafonds_Annuel_Maximum = VirRhFonction.ConversionDouble(TableauData(10))
                SiValoriser_Previsionnel = (11)
                SiEcretement_Sur_HorairedeReference = TableauData(12)
                SiEcretement_Sur_Journee = (13)
                If TableauData(14) <> "" Then Ecretement_TempsComplet = CInt(TableauData(14))
                If TableauData(15) <> "" Then Ecretement_TempsPartiel90 = CInt(TableauData(15))
                If TableauData(16) <> "" Then Ecretement_TempsPartiel80 = CInt(TableauData(16))
                If TableauData(17) <> "" Then Ecretement_TempsPartiel70 = CInt(TableauData(17))
                If TableauData(18) <> "" Then Ecretement_TempsPartiel60 = CInt(TableauData(18))
                If TableauData(19) <> "" Then Ecretement_TempsPartiel50 = CInt(TableauData(19))
                MyBase.Date_de_Fin = TableauData(20)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = Date_de_Fin
            End Set
        End Property

        Public Property VDate_de_Fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace


