﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class EDITION_DESCRIPTIF
        Inherits VIR_FICHE

        Private WsLstSelections As List(Of EDITION_SELECTION)
        Private WsFicheLue As StringBuilder
        '
        Private WsPtdeVue As Integer
        Private WsIntitule As String
        Private WsUtilisateur As String
        Private WsEncadrement As Integer
        Private WsTotalisation As Integer
        Private WsRupture As Integer
        Private WsColonneObservation As Integer
        Private WsOrientation As Integer
        Private WsPoliceName As String
        Private WsPoliceSize As Integer
        Private WsPoliceCouleur As Integer
        Private WsPoliceBold As Integer
        Private WsPoliceItalic As Integer
        Private WsTri_1 As Integer
        Private WsSensTri_1 As Integer
        Private WsTri_2 As Integer
        Private WsSensTri_2 As Integer
        Private WsTri_3 As Integer
        Private WsSensTri_3 As Integer
        Private WsNiveauRuptureTri As Integer
        Private WsDateDebutExe As String
        Private WsDateFinExe As String
        Private WsSiHistorique As Integer
        Private WsCategorieRH As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "EDITION_DESCRIPTIF"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueScriptEdition
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property PointdeVueEdition() As Integer
            Get
                Return WsPtdeVue
            End Get
            Set(ByVal value As Integer)
                WsPtdeVue = value
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 128
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 128)
                End Select
            End Set
        End Property

        Public Property Utilisateur() As String
            Get
                Return WsUtilisateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsUtilisateur = value
                    Case Else
                        WsUtilisateur = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property SiEncadrement() As Boolean
            Get
                If WsEncadrement = 0 Then
                    Return False
                Else
                    Return True
                End If
            End Get
            Set(ByVal value As Boolean)
                Select Case value
                    Case False
                        WsEncadrement = 0
                    Case True
                        WsEncadrement = 1
                End Select
            End Set
        End Property

        Public Property SiTotalisation() As Boolean
            Get
                If WsTotalisation = 0 Then
                    Return False
                Else
                    Return True
                End If
            End Get
            Set(ByVal value As Boolean)
                Select Case value
                    Case False
                        WsTotalisation = 0
                    Case True
                        WsTotalisation = 1
                End Select
            End Set
        End Property

        Public Property SiRupturePage() As Boolean
            Get
                If WsRupture = 0 Then
                    Return False
                Else
                    Return True
                End If
            End Get
            Set(ByVal value As Boolean)
                Select Case value
                    Case False
                        WsRupture = 0
                    Case True
                        WsRupture = 1
                End Select
            End Set
        End Property

        Public Property SiColonneObservation() As Boolean
            Get
                If WsColonneObservation = 0 Then
                    Return False
                Else
                    Return True
                End If
            End Get
            Set(ByVal value As Boolean)
                Select Case value
                    Case False
                        WsColonneObservation = 0
                    Case True
                        WsColonneObservation = 1
                End Select
            End Set
        End Property

        Public Property Orientation() As Integer
            Get
                Return WsOrientation
            End Get
            Set(ByVal value As Integer)
                WsOrientation = value
            End Set
        End Property

        Public Property Police_Name() As String
            Get
                Return WsPoliceName
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsPoliceName = value
                End Select
            End Set
        End Property

        Public Property Police_Taille() As Integer
            Get
                Return WsPoliceSize
            End Get
            Set(ByVal value As Integer)
                WsPoliceSize = value
            End Set
        End Property

        Public Property Police_Couleur() As Integer
            Get
                Return WsPoliceCouleur
            End Get
            Set(ByVal value As Integer)
                WsPoliceCouleur = value
            End Set
        End Property

        Public Property SiPolice_Bold() As Boolean
            Get
                If WsPoliceBold = 0 Then
                    Return False
                Else
                    Return True
                End If
            End Get
            Set(ByVal value As Boolean)
                Select Case value
                    Case False
                        WsPoliceBold = 0
                    Case True
                        WsPoliceBold = 1
                End Select
            End Set
        End Property

        Public Property SiPolice_Italic() As Boolean
            Get
                If WsPoliceItalic = 0 Then
                    Return False
                Else
                    Return True
                End If
            End Get
            Set(ByVal value As Boolean)
                Select Case value
                    Case False
                        WsPoliceItalic = 0
                    Case True
                        WsPoliceItalic = 1
                End Select
            End Set
        End Property

        Public Property ColonneTri_1() As Integer
            Get
                Return WsTri_1
            End Get
            Set(ByVal value As Integer)
                WsTri_1 = value
            End Set
        End Property

        Public Property SensTri_1() As Integer
            Get
                Return WsSensTri_1
            End Get
            Set(ByVal value As Integer)
                WsSensTri_1 = value
            End Set
        End Property

        Public Property ColonneTri_2() As Integer
            Get
                Return WsTri_2
            End Get
            Set(ByVal value As Integer)
                WsTri_2 = value
            End Set
        End Property

        Public Property SensTri_2() As Integer
            Get
                Return WsSensTri_2
            End Get
            Set(ByVal value As Integer)
                WsSensTri_2 = value
            End Set
        End Property

        Public Property ColonneTri_3() As Integer
            Get
                Return WsTri_3
            End Get
            Set(ByVal value As Integer)
                WsTri_3 = value
            End Set
        End Property

        Public Property SensTri_3() As Integer
            Get
                Return WsSensTri_3
            End Get
            Set(ByVal value As Integer)
                WsSensTri_3 = value
            End Set
        End Property

        Public Property RuptureTri() As Integer
            Get
                Return WsNiveauRuptureTri
            End Get
            Set(ByVal value As Integer)
                WsNiveauRuptureTri = value
            End Set
        End Property

        Public Property DatedeDebutExe() As String
            Get
                Return WsDateDebutExe
            End Get
            Set(ByVal value As String)
                WsDateDebutExe = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property DatedeFinExe() As String
            Get
                Return WsDateFinExe
            End Get
            Set(ByVal value As String)
                WsDateFinExe = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property SiHistoriqueCoche() As Boolean
            Get
                If WsSiHistorique = 0 Then
                    Return False
                Else
                    Return True
                End If
            End Get
            Set(ByVal value As Boolean)
                Select Case value
                    Case False
                        WsSiHistorique = 0
                    Case True
                        WsSiHistorique = 1
                End Select
            End Set
        End Property

        Public Property CategorieRH() As Integer
            Get
                Return WsCategorieRH
            End Get
            Set(ByVal value As Integer)
                WsCategorieRH = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(CStr(PointdeVueEdition) & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Utilisateur & VI.Tild)
                Chaine.Append(CStr(WsEncadrement) & VI.Tild)
                Chaine.Append(CStr(WsTotalisation) & VI.Tild)
                Chaine.Append(CStr(WsRupture) & VI.Tild)
                Chaine.Append(CStr(WsColonneObservation) & VI.Tild)
                Chaine.Append(CStr(Orientation) & VI.Tild)
                Chaine.Append(Police_Name & VI.Tild)
                Chaine.Append(CStr(Police_Taille) & VI.Tild)
                Chaine.Append(CStr(Police_Couleur) & VI.Tild)
                Chaine.Append(CStr(WsPoliceBold) & VI.Tild)
                Chaine.Append(CStr(WsPoliceItalic) & VI.Tild)
                Chaine.Append(CStr(WsTri_1) & VI.Tild)
                Chaine.Append(CStr(WsSensTri_1) & VI.Tild)
                Chaine.Append(CStr(WsTri_2) & VI.Tild)
                Chaine.Append(CStr(WsSensTri_2) & VI.Tild)
                Chaine.Append(CStr(WsTri_3) & VI.Tild)
                Chaine.Append(CStr(WsSensTri_3) & VI.Tild)
                Chaine.Append(CStr(WsNiveauRuptureTri) & VI.Tild)
                Chaine.Append(DatedeDebutExe & VI.Tild)
                Chaine.Append(DatedeFinExe & VI.Tild)
                Chaine.Append(CStr(WsSiHistorique) & VI.Tild)
                Chaine.Append(CStr(CategorieRH))

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 25 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) = "" Then TableauData(1) = "0"
                PointdeVueEdition = CInt(TableauData(1))
                Intitule = TableauData(2)
                Utilisateur = TableauData(3)
                For IndiceI = 4 To 8
                    If TableauData(IndiceI) = "" Then TableauData(IndiceI) = "0"
                Next IndiceI
                WsEncadrement = CInt(TableauData(4))
                WsTotalisation = CInt(TableauData(5))
                WsRupture = CInt(TableauData(6))
                WsColonneObservation = CInt(TableauData(7))
                WsOrientation = CInt(TableauData(8))
                WsPoliceName = TableauData(9)
                For IndiceI = 10 To 20
                    If TableauData(IndiceI) = "" Then TableauData(IndiceI) = "0"
                Next IndiceI
                WsPoliceSize = CInt(TableauData(10))
                WsPoliceCouleur = CInt(TableauData(11))
                WsPoliceBold = CInt(TableauData(12))
                WsPoliceItalic = CInt(TableauData(13))
                WsTri_1 = CInt(TableauData(14))
                WsSensTri_1 = CInt(TableauData(15))
                WsTri_2 = CInt(TableauData(16))
                WsSensTri_2 = CInt(TableauData(17))
                WsTri_3 = CInt(TableauData(18))
                WsSensTri_3 = CInt(TableauData(19))
                WsNiveauRuptureTri = CInt(TableauData(20))
                DatedeDebutExe = TableauData(21)
                MyBase.Date_de_Fin = TableauData(22)
                If TableauData(23) = "" Then TableauData(23) = "0"
                WsSiHistorique = CInt(TableauData(23))
                If TableauData(24) = "" Then TableauData(24) = "0"
                CategorieRH = CInt(TableauData(24))

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesValeurs() As List(Of EDITION_SELECTION)
            Get
                If WsLstSelections Is Nothing Then
                    Return Nothing
                End If
                Return WsLstSelections.OrderBy(Function(m) m.Numero_Ligne).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Fiche_Selection(ByVal NoLigne As Integer) As EDITION_SELECTION
            If WsLstSelections Is Nothing Then
                Return Nothing
            End If
            Return (From es As EDITION_SELECTION In WsLstSelections Where es.Numero_Ligne = NoLigne Select es).FirstOrDefault()
        End Function

        Public Function Ajouter_Valeur(ByVal Fiche As EDITION_SELECTION) As Integer
            If WsLstSelections Is Nothing Then
                WsLstSelections = New List(Of EDITION_SELECTION)
            End If
            WsLstSelections.Add(Fiche)
            Return WsLstSelections.Count
        End Function
    End Class
End Namespace
