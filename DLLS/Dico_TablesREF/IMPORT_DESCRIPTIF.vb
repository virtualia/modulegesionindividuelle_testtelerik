﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class IMPORT_DESCRIPTIF
        Inherits VIR_FICHE

        Private WsLstSelections As List(Of IMPORT_SELECTION)
        Private WsFicheLue As StringBuilder
        '
        Private WsPtdeVue As Integer
        Private WsIntitule As String
        Private WsUtilisateur As String
        Private WsNomFichierEntree As String
        Private WsFormat As Integer
        Private WsSeparateur As Integer
        Private WsChoixIde As Integer
        Private WsScriptChaine As String
        Private WsNomFichierTranscodif As String
        Private WsCategorieRH As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "IMPORT_DESCRIPTIF"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueScriptImport
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property PointdeVueImport() As Integer
            Get
                Return WsPtdeVue
            End Get
            Set(ByVal value As Integer)
                WsPtdeVue = value
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 128
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 128)
                End Select
            End Set
        End Property

        Public Property Utilisateur() As String
            Get
                Return WsUtilisateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsUtilisateur = value
                    Case Else
                        WsUtilisateur = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property NomFichierEnEntree() As String
            Get
                Return WsNomFichierEntree
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsNomFichierEntree = value
                    Case Else
                        WsNomFichierEntree = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property FormatEntree() As Integer
            Get
                Return WsFormat
            End Get
            Set(ByVal value As Integer)
                WsFormat = value
            End Set
        End Property

        Public Property Separateur() As Integer
            Get
                Return WsSeparateur
            End Get
            Set(ByVal value As Integer)
                WsSeparateur = value
            End Set
        End Property

        Public Property Choix_Identifiant() As Integer
            Get
                Return WsChoixIde
            End Get
            Set(ByVal value As Integer)
                WsChoixIde = value
            End Set
        End Property

        Public Property ScriptChaine() As String
            Get
                Return WsScriptChaine
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 128
                        WsScriptChaine = value
                    Case Else
                        WsScriptChaine = Strings.Left(value, 128)
                End Select
            End Set
        End Property

        Public Property NomFichierTranscodification() As String
            Get
                Return WsNomFichierTranscodif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsNomFichierTranscodif = value
                    Case Else
                        WsNomFichierTranscodif = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property CategorieRH() As Integer
            Get
                Return WsCategorieRH
            End Get
            Set(ByVal value As Integer)
                WsCategorieRH = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(CStr(PointdeVueImport) & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Utilisateur & VI.Tild)
                Chaine.Append(NomFichierEnEntree & VI.Tild)
                Chaine.Append(CStr(FormatEntree) & VI.Tild)
                Chaine.Append(CStr(Separateur) & VI.Tild)
                Chaine.Append(CStr(Choix_Identifiant) & VI.Tild)
                Chaine.Append(ScriptChaine & VI.Tild)
                Chaine.Append(NomFichierTranscodification & VI.Tild)
                Chaine.Append(CStr(CategorieRH))
               
                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) = "" Then TableauData(1) = "0"
                PointdeVueImport = CInt(TableauData(1))
                Intitule = TableauData(2)
                Utilisateur = TableauData(3)
                NomFichierEnEntree = TableauData(4)
                For IndiceI = 5 To 7
                    If TableauData(IndiceI) = "" Then TableauData(IndiceI) = "0"
                Next IndiceI
                FormatEntree = CInt(TableauData(5))
                Separateur = CInt(TableauData(6))
                Choix_Identifiant = CInt(TableauData(7))
                ScriptChaine = TableauData(8)
                NomFichierTranscodification = TableauData(9)
                If TableauData(10) = "" Then TableauData(10) = "0"
                CategorieRH = CInt(TableauData(10))

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesSelections() As List(Of IMPORT_SELECTION)
            Get
                If WsLstSelections Is Nothing Then
                    Return Nothing
                End If
                Return WsLstSelections.OrderBy(Function(m) m.Numero_Ligne).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Selection(ByVal Fiche As IMPORT_SELECTION) As Integer
            If WsLstSelections Is Nothing Then
                WsLstSelections = New List(Of IMPORT_SELECTION)
            End If
            WsLstSelections.Add(Fiche)
            Return WsLstSelections.Count
        End Function

        Public Function Fiche_Selection(ByVal NoLigne As Integer) As IMPORT_SELECTION
            If WsLstSelections Is Nothing Then
                Return Nothing
            End If
            Return (From it As IMPORT_SELECTION In WsLstSelections Where it.Numero_Ligne = NoLigne Select it).FirstOrDefault()
        End Function
    End Class
End Namespace
