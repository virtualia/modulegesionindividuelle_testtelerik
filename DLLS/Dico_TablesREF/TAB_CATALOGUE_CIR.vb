﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class TAB_CATALOGUE_CIR
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsValeur_CIR As String
        Private WsReference As String
        Private WsValeurVirtualia As String
        Private WsDecret As String
        Private WsZActif As String
        Private WsSiActif As Boolean = True

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "TAB_CATALOGUE_CIR"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueGeneral
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 4
            End Get
        End Property

        Public Property Reference() As String
            Get
                Return WsReference
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsReference = value
                    Case Else
                        WsReference = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Valeur_CIR() As String
            Get
                Return WsValeur_CIR
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 230
                        WsValeur_CIR = value
                    Case Else
                        WsValeur_CIR = Strings.Left(value, 230)
                End Select
            End Set
        End Property

        Public Property Valeur_Virtualia() As String
            Get
                Return WsValeurVirtualia
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsValeurVirtualia = value
                    Case Else
                        WsValeurVirtualia = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Private Property ZActif() As String
            Get
                Select Case WsSiActif
                    Case False
                        Return "N"
                    Case Else
                        Return "O"
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1
                        WsZActif = value
                    Case Else
                        WsZActif = Strings.Left(value, 1)
                End Select
                If WsZActif = "N" Then
                    SiActif = False
                Else
                    SiActif = True
                End If
            End Set
        End Property

        Public Property SiActif() As Boolean
            Get
                Return WsSiActif
            End Get
            Set(ByVal value As Boolean)
                WsSiActif = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Reference & VI.Tild)
                Chaine.Append(Valeur_CIR & VI.Tild)
                Chaine.Append(Valeur_Virtualia & VI.Tild)
                Chaine.Append(ZActif)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 5 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Reference = TableauData(1)
                Valeur_CIR = TableauData(2)
                Valeur_Virtualia = TableauData(3)
                ZActif = TableauData(4)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
