﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class LOLF_UO
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsNumeroPgm As Integer = 0
        Private WsNumeroBOP As Integer = 0
        Private WsNumeroUO As Integer = 0
        Private WsIntitule As String
        Private WsDateCreation As String
        Private WsResponsable As String
        Private WsIdeResponsable As Integer = 0
        Private WsDescriptif As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "LOLF_UO"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueLolf
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 6
            End Get
        End Property

        Public Property NumeroduProgramme() As Integer
            Get
                Return WsNumeroPgm
            End Get
            Set(ByVal value As Integer)
                WsNumeroPgm = value
            End Set
        End Property

        Public Property NumeroduBOP() As Integer
            Get
                Return WsNumeroBOP
            End Get
            Set(ByVal value As Integer)
                WsNumeroBOP = value
            End Set
        End Property

        Public Property NumerodelUO() As Integer
            Get
                Return WsNumeroUO
            End Get
            Set(ByVal value As Integer)
                WsNumeroUO = value
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property DatedeCreation() As String
            Get
                Return WsDateCreation
            End Get
            Set(ByVal value As String)
                WsDateCreation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Responsable() As String
            Get
                Return WsResponsable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsResponsable = value
                    Case Else
                        WsResponsable = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Identifiant_Responsable() As Integer
            Get
                Return WsIdeResponsable
            End Get
            Set(ByVal value As Integer)
                WsIdeResponsable = value
            End Set
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsDescriptif = value
                    Case Else
                        WsDescriptif = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(NumeroduProgramme.ToString & VI.Tild)
                Chaine.Append(NumeroduBOP.ToString & VI.Tild)
                Chaine.Append(NumerodelUO.ToString & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(DatedeCreation & VI.Tild)
                Chaine.Append(Responsable & VI.Tild)
                Chaine.Append(Identifiant_Responsable & VI.Tild)
                Chaine.Append(Descriptif)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 9 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) <> "" Then NumeroduProgramme = CInt(TableauData(1))
                If TableauData(2) <> "" Then NumeroduBOP = CInt(TableauData(2))
                If TableauData(3) <> "" Then NumerodelUO = CInt(TableauData(3))
                Intitule = TableauData(4)
                DatedeCreation = TableauData(5)
                Responsable = TableauData(6)
                If TableauData(7) <> "" Then Identifiant_Responsable = CInt(TableauData(7))
                Descriptif = TableauData(8)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

