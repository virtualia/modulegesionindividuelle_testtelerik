﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class UTI_COULEUR_DOSSIER
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsNumeroIcone As Integer
        Private WsNumeroCouleur As Integer
        Private WsValeurs As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "UTI_COULEUR_DOSSIER"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueUtilisateur
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 5
            End Get
        End Property

        Public Property Numero_Icone() As Integer
            Get
                Return WsNumeroIcone
            End Get
            Set(ByVal value As Integer)
                WsNumeroIcone = value
            End Set
        End Property

        Public Property Numero_Couleur() As Integer
            Get
                Return WsNumeroCouleur
            End Get
            Set(ByVal value As Integer)
                WsNumeroCouleur = value
            End Set
        End Property

        Public Property Valeurs() As String
            Get
                Return WsValeurs
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsValeurs = value
                    Case Else
                        WsValeurs = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Numero_Icone & VI.Tild)
                Chaine.Append(Numero_Couleur & VI.Tild)
                Chaine.Append(Valeurs)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) = "" Then
                    TableauData(1) = "0"
                End If
                Numero_Icone = CInt(TableauData(1))
                If TableauData(2) = "" Then
                    TableauData(2) = "0"
                End If
                Numero_Couleur = CInt(TableauData(2))
                Valeurs = TableauData(3)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
