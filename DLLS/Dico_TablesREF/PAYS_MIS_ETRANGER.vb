﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAYS_MIS_ETRANGER
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsGroupe_I As Double = 0
        Private WsGroupe_II As Double = 0
        Private WsGroupe_III As Double = 0
        Private WsGroupe_IV As Double = 0
        Private WsGroupe_V As Double = 0

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAYS_MIS_ETRANGER"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePays
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 6
            End Get
        End Property

        Public Property Groupe_I() As Double
            Get
                Return WsGroupe_I
            End Get
            Set(ByVal value As Double)
                WsGroupe_I = value
            End Set
        End Property

        Public Property Groupe_II() As Double
            Get
                Return WsGroupe_II
            End Get
            Set(ByVal value As Double)
                WsGroupe_II = value
            End Set
        End Property

        Public Property Groupe_III() As Double
            Get
                Return WsGroupe_III
            End Get
            Set(ByVal value As Double)
                WsGroupe_III = value
            End Set
        End Property

        Public Property Groupe_IV() As Double
            Get
                Return WsGroupe_IV
            End Get
            Set(ByVal value As Double)
                WsGroupe_IV = value
            End Set
        End Property

        Public Property Groupe_V() As Double
            Get
                Return WsGroupe_V
            End Get
            Set(ByVal value As Double)
                WsGroupe_V = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Groupe_I.ToString & VI.Tild)
                Chaine.Append(Groupe_II.ToString & VI.Tild)
                Chaine.Append(Groupe_III.ToString & VI.Tild)
                Chaine.Append(Groupe_IV.ToString & VI.Tild)
                Chaine.Append(Groupe_V.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 7 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then Groupe_I = VirRhFonction.ConversionDouble(TableauData(2))
                If TableauData(3) <> "" Then Groupe_II = VirRhFonction.ConversionDouble(TableauData(3))
                If TableauData(4) <> "" Then Groupe_III = VirRhFonction.ConversionDouble(TableauData(4))
                If TableauData(5) <> "" Then Groupe_IV = VirRhFonction.ConversionDouble(TableauData(5))
                If TableauData(6) <> "" Then Groupe_V = VirRhFonction.ConversionDouble(TableauData(6))

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

