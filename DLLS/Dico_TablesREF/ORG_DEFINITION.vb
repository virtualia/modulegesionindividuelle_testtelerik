﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ORG_DEFINITION
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsBudget_EB As String
        Private WsBrancheactivite_EB As String
        Private WsSpecialite_EB As String
        Private WsCategorie_EB As String
        Private WsFiliere_EB As String
        Private WsSecteur_EB As String
        Private WsIntitule_EB As String
        Private WsIndicemoyen_EB As Integer = 0
        Private WsChapitrebudget_EB As String
        Private WsChapitrebudgetministere_EB As String
        Private WsTraitementmoyen_EB As Double = 0
        Private WsBornesIB_EB As String
        Private WsBornesIM_EB As String
        Private WsGrade_EB As String
        Private WsGel_EB As String
        Private WsEmploigage_no1 As String
        Private WsEmploigage_no2 As String
        Private WsEmploigage_no3 As String
        Private WsEmploigage_no4 As String
        Private WsNumeroemploigage As String
        Private WsDatefinsituation_EB As String



        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ORG_DEFINITION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePosteBud
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Budget_EB() As String
            Get
                Return WsBudget_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsBudget_EB = value
                    Case Else
                        WsBudget_EB = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Brancheactivite_EB() As String
            Get
                Return WsBrancheactivite_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsBrancheactivite_EB = value
                    Case Else
                        WsBrancheactivite_EB = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Specialite_EB() As String
            Get
                Return WsSpecialite_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsSpecialite_EB = value
                    Case Else
                        WsSpecialite_EB = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Categorie_EB() As String
            Get
                Return WsCategorie_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsCategorie_EB = value
                    Case Else
                        WsCategorie_EB = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property Filiere_EB() As String
            Get
                Return WsFiliere_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsFiliere_EB = value
                    Case Else
                        WsFiliere_EB = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Secteur_EB() As String
            Get
                Return WsSecteur_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsSecteur_EB = value
                    Case Else
                        WsSecteur_EB = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Intitule_EB() As String
            Get
                Return WsIntitule_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule_EB = value
                    Case Else
                        WsIntitule_EB = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Indicemoyen_EB() As Integer
            Get
                Return WsIndicemoyen_EB
            End Get
            Set(ByVal value As Integer)
                WsIndicemoyen_EB = value
            End Set
        End Property

        Public Property Chapitrebudget_EB() As String
            Get
                Return WsChapitrebudget_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsChapitrebudget_EB = value
                    Case Else
                        WsChapitrebudget_EB = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Chapitrebudgetministere_EB() As String
            Get
                Return WsChapitrebudgetministere_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsChapitrebudgetministere_EB = value
                    Case Else
                        WsChapitrebudgetministere_EB = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Traitementmoyen_EB() As Double
            Get
                Return WsTraitementmoyen_EB
            End Get
            Set(ByVal value As Double)
                WsTraitementmoyen_EB = value
            End Set
        End Property

        Public Property BornesIB_EB() As String
            Get
                Return WsBornesIB_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsBornesIB_EB = value
                    Case Else
                        WsBornesIB_EB = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property BornesIM_EB() As String
            Get
                Return WsBornesIM_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsBornesIM_EB = value
                    Case Else
                        WsBornesIM_EB = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Grade_EB() As String
            Get
                Return WsGrade_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsGrade_EB = value
                    Case Else
                        WsGrade_EB = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Gel_EB() As String
            Get
                Return WsGel_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsGel_EB = value
                    Case Else
                        WsGel_EB = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Emploigage_no1() As String
            Get
                Return WsEmploigage_no1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsEmploigage_no1 = value
                    Case Else
                        WsEmploigage_no1 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Emploigage_no2() As String
            Get
                Return WsEmploigage_no2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsEmploigage_no2 = value
                    Case Else
                        WsEmploigage_no2 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Emploigage_no3() As String
            Get
                Return WsEmploigage_no3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsEmploigage_no3 = value
                    Case Else
                        WsEmploigage_no3 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Emploigage_no4() As String
            Get
                Return WsEmploigage_no4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsEmploigage_no4 = value
                    Case Else
                        WsEmploigage_no4 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Numeroemploigage() As String
            Get
                Return WsNumeroemploigage
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNumeroemploigage = value
                    Case Else
                        WsNumeroemploigage = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Budget_EB & VI.Tild)
                Chaine.Append(Brancheactivite_EB & VI.Tild)
                Chaine.Append(Specialite_EB & VI.Tild)
                Chaine.Append(Categorie_EB & VI.Tild)
                Chaine.Append(Filiere_EB & VI.Tild)
                Chaine.Append(Secteur_EB & VI.Tild)
                Chaine.Append(Intitule_EB & VI.Tild)
                Chaine.Append(Indicemoyen_EB.ToString & VI.Tild)
                Chaine.Append(Chapitrebudget_EB & VI.Tild)
                Chaine.Append(Chapitrebudgetministere_EB & VI.Tild)
                Chaine.Append(Traitementmoyen_EB.ToString & VI.Tild)
                Chaine.Append(BornesIB_EB & VI.Tild)
                Chaine.Append(BornesIM_EB & VI.Tild)
                Chaine.Append(Grade_EB & VI.Tild)
                Chaine.Append(Gel_EB & VI.Tild)
                Chaine.Append(Emploigage_no1 & VI.Tild)
                Chaine.Append(Emploigage_no2 & VI.Tild)
                Chaine.Append(Emploigage_no3 & VI.Tild)
                Chaine.Append(Emploigage_no4 & VI.Tild)
                Chaine.Append(Numeroemploigage & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 23 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))

                MyBase.Date_de_Valeur = TableauData(1)
                Budget_EB = TableauData(2)
                Brancheactivite_EB = TableauData(3)
                Specialite_EB = TableauData(4)
                Categorie_EB = TableauData(5)
                Filiere_EB = TableauData(6)
                Secteur_EB = TableauData(7)
                Intitule_EB = TableauData(8)
                If TableauData(9) <> "" Then Indicemoyen_EB = CInt(TableauData(9))
                Chapitrebudget_EB = TableauData(10)
                Chapitrebudgetministere_EB = TableauData(11)
                If TableauData(12) <> "" Then Traitementmoyen_EB = VirRhFonction.ConversionDouble(TableauData(12))
                BornesIB_EB = TableauData(13)
                BornesIM_EB = TableauData(14)
                Grade_EB = TableauData(15)
                Gel_EB = TableauData(16)
                Emploigage_no1 = TableauData(17)
                Emploigage_no2 = TableauData(18)
                Emploigage_no3 = TableauData(19)
                Emploigage_no4 = TableauData(20)
                Numeroemploigage = TableauData(21)
                MyBase.Date_de_Fin = TableauData(22)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace


