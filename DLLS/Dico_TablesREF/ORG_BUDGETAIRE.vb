﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ORG_BUDGETAIRE
        Inherits VIR_FICHE

        Private WsLstDefinitions As List(Of ORG_DEFINITION)
        Private WsLstAffectations As List(Of ORG_RATTACHEMENT)
        Private WsFicheLue As StringBuilder
        '
        Private WsNumero_EB As String
        Private WsDateouverture_EB As String
        Private WsDatefermeture_EB As String
        Private WsReferenceministere_EB As String
        Private WsNature_EB As String
        Private WsTauxactivite_EB As String
        Private WsAnnotation_EB As String
        '
        Private TsFiliere_Def As String
        Private TsSecteur_Def As String
        Private TsIntitule_Def As String
        Private TsBudget_Def As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ORG_BUDGETAIRE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePosteBud
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Numero_EB() As String
            Get
                Return WsNumero_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsNumero_EB = value
                    Case Else
                        WsNumero_EB = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Dateouverture_EB() As String
            Get
                Return WsDateouverture_EB
            End Get
            Set(ByVal value As String)
                WsDateouverture_EB = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Valeur = value
            End Set
        End Property

        Public Property Datefermeture_EB() As String
            Get
                Return WsDatefermeture_EB
            End Get
            Set(ByVal value As String)
                WsDatefermeture_EB = VirRhDate.DateStandardVirtualia(value)
                MyBase.Date_de_Fin = value
            End Set
        End Property

        Public Property Referenceministere_EB() As String
            Get
                Return WsReferenceministere_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsReferenceministere_EB = value
                    Case Else
                        WsReferenceministere_EB = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Nature_EB() As String
            Get
                Return WsNature_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsNature_EB = value
                    Case Else
                        WsNature_EB = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Tauxactivite_EB() As String
            Get
                Return WsTauxactivite_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsTauxactivite_EB = value
                    Case Else
                        WsTauxactivite_EB = Strings.Left(value, 8)
                End Select
            End Set
        End Property

        Public Property Annotation_EB() As String
            Get
                Return WsAnnotation_EB
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsAnnotation_EB = value
                    Case Else
                        WsAnnotation_EB = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Numero_EB & VI.Tild)
                Chaine.Append(Dateouverture_EB & VI.Tild)
                Chaine.Append(Datefermeture_EB & VI.Tild)
                Chaine.Append(Referenceministere_EB & VI.Tild)
                Chaine.Append(Nature_EB & VI.Tild)
                Chaine.Append(Tauxactivite_EB & VI.Tild)
                Chaine.Append(Annotation_EB)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 8 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))

                Numero_EB = TableauData(1)
                Dateouverture_EB = TableauData(2)
                Datefermeture_EB = TableauData(3)
                Referenceministere_EB = TableauData(4)
                Nature_EB = TableauData(5)
                Tauxactivite_EB = TableauData(6)
                Annotation_EB = TableauData(7)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesDefinitions() As List(Of ORG_DEFINITION)
            Get
                If WsLstDefinitions Is Nothing Then
                    Return Nothing
                End If
                Return WsLstDefinitions.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesRattachements() As List(Of ORG_RATTACHEMENT)
            Get
                If WsLstAffectations Is Nothing Then
                    Return Nothing
                End If
                Return WsLstAffectations.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Intitule__Definition() As String

            If TsIntitule_Def = "" Then
                EnrichirObjet("")
            End If
            Return TsIntitule_Def

        End Function

        Public Function Budget__Definition() As String

            If TsBudget_Def = "" Then
                EnrichirObjet("")
            End If
            Return TsBudget_Def

        End Function

        Public Function Filiere_Definition() As String

            If TsFiliere_Def = "" Then
                EnrichirObjet("")
            End If
            Return TsFiliere_Def

        End Function

        Public Function Secteur_Definition() As String

            If TsSecteur_Def = "" Then
                EnrichirObjet("")
            End If
            Return TsSecteur_Def

        End Function

        Public Function SiEmploiEnGestion(ByVal DateValeur As String) As Boolean

            Select Case VirRhDate.ComparerDates(DateValeur, MyBase.Date_de_Valeur)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                    If MyBase.Date_de_Fin = "" Then
                        Return True
                    End If
                    Select Case VirRhDate.ComparerDates(DateValeur, MyBase.Date_de_Fin)
                        Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                            Return True
                    End Select
            End Select
            Return False

        End Function

        Public Function Ajouter_Definition(ByVal Fiche As ORG_DEFINITION) As Integer

            If WsLstDefinitions Is Nothing Then
                WsLstDefinitions = New List(Of ORG_DEFINITION)
            End If
            WsLstDefinitions.Add(Fiche)
            Return WsLstDefinitions.Count

        End Function

        Public Function Fiche_Definition(ByVal DateValeur As String) As ORG_DEFINITION

            If WsLstDefinitions Is Nothing Then
                Return Nothing
            End If
            Dim I As Integer
            Dim LstTriee As List(Of ORG_DEFINITION) = ListedesDefinitions
            For I = LstTriee.Count - 1 To 0 Step -1
                Select Case VirRhDate.ComparerDates(DateValeur, LstTriee.Item(I).Date_de_Valeur)
                    Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                        If LstTriee.Item(I).Date_de_Fin = "" Then
                            Return LstTriee.Item(I)
                        End If
                        Select Case VirRhDate.ComparerDates(DateValeur, LstTriee.Item(I).Date_de_Fin)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                Return LstTriee.Item(I)
                        End Select
                End Select
            Next I
            Return Nothing

        End Function

        Public Function Ajouter_Rattachement(ByVal Fiche As ORG_RATTACHEMENT) As Integer

            If WsLstAffectations Is Nothing Then
                WsLstAffectations = New List(Of ORG_RATTACHEMENT)
            End If
            WsLstAffectations.Add(Fiche)
            Return WsLstAffectations.Count

        End Function

        Public Function Fiche_Rattachement(ByVal DateValeur As String) As ORG_RATTACHEMENT

            If WsLstAffectations Is Nothing Then
                Return Nothing
            End If
            Dim I As Integer
            Dim LstTriee As List(Of ORG_RATTACHEMENT) = ListedesRattachements
            For I = LstTriee.Count - 1 To 0 Step -1
                Select Case VirRhDate.ComparerDates(DateValeur, LstTriee.Item(I).Date_de_Valeur)
                    Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                        If LstTriee.Item(I).Date_de_Fin = "" Then
                            Return LstTriee.Item(I)
                        End If
                        Select Case VirRhDate.ComparerDates(DateValeur, LstTriee.Item(I).Date_de_Fin)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                Return LstTriee.Item(I)
                        End Select
                End Select
            Next I
            Return Nothing

        End Function

        Private Sub EnrichirObjet(ByVal DateValeur As String)
            Dim FicheValable As ORG_DEFINITION
            TsBudget_Def = ""
            TsFiliere_Def = ""
            TsSecteur_Def = ""
            TsIntitule_Def = ""
            If DateValeur = "" Then
                DateValeur = VirRhDate.DateduJour
            End If
            FicheValable = Fiche_Definition(DateValeur)
            If FicheValable IsNot Nothing Then
                TsBudget_Def = FicheValable.Budget_EB
                TsFiliere_Def = FicheValable.Filiere_EB
                TsSecteur_Def = FicheValable.Secteur_EB
                TsIntitule_Def = FicheValable.Intitule_EB
            End If
        End Sub

        Public Function Budget__Definition(ByVal DateValeur As String) As String

            EnrichirObjet(DateValeur)
            Return TsBudget_Def

        End Function

        Public Function Filiere_Definition(ByVal DateValeur As String) As String

            EnrichirObjet(DateValeur)
            Return TsFiliere_Def

        End Function

        Public Function Secteur_Definition(ByVal DateValeur As String) As String

            EnrichirObjet(DateValeur)
            Return TsSecteur_Def

        End Function

        Public Function Intitule__Definition(ByVal DateValeur As String) As String

            EnrichirObjet(DateValeur)
            Return TsIntitule_Def

        End Function


    End Class
End Namespace

