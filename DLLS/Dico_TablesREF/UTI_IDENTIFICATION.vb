﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class UTI_IDENTIFICATION
        Inherits VIR_FICHE

        Private WsFicheFiltre As UTI_FILTRE
        Private WsFichePerimetre As UTI_PERIMETRE
        Private WsFicheCritere As UTI_CRITERE_DOSSIER
        Private WsLstCouleurs As List(Of UTI_COULEUR_DOSSIER)
        Private WsFicheLue As StringBuilder
        '
        Private WsNom As String
        Private WsPrenom As String
        Private WsQualite As String
        Private WsInitiales As String
        Private WsTypeUtilisateur As String
        Private WsGroupeGestion As String
        Private WsPassword As String
        Private WsDatePassword As String
        Private WsBaseCourante As Integer
        Private WsDateMaj As String
        Private WsEmail As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "UTI_IDENTIFICATION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueUtilisateur
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Nom() As String
            Get
                Return WsNom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsNom = value
                    Case Else
                        WsNom = Strings.Left(value, 35)
                End Select
            End Set
        End Property

        Public Property Prenom() As String
            Get
                Return WsPrenom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsPrenom = value
                    Case Else
                        WsPrenom = Strings.Left(value, 35)
                End Select
            End Set
        End Property

        Public Property Qualite() As String
            Get
                Return WsQualite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsQualite = value
                    Case Else
                        WsQualite = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Initiales() As String
            Get
                Return WsInitiales
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsInitiales = value
                    Case Else
                        WsInitiales = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property TypeUtilisateur() As String
            Get
                Return WsTypeUtilisateur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsTypeUtilisateur = value
                    Case Else
                        WsTypeUtilisateur = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property GroupeUtilisateur() As String
            Get
                Return WsGroupeGestion
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsGroupeGestion = value
                    Case Else
                        WsGroupeGestion = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property MotdePasse() As String
            Get
                Return WsPassword
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsPassword = value
                    Case Else
                        WsPassword = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Date_MotdePasse() As String
            Get
                Return WsDatePassword
            End Get
            Set(ByVal value As String)
                WsDatePassword = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property DatabaseCourante() As Integer
            Get
                Return WsBaseCourante
            End Get
            Set(ByVal value As Integer)
                WsBaseCourante = value
            End Set
        End Property

        Public Property Date_Maj() As String
            Get
                Return WsDateMaj
            End Get
            Set(ByVal value As String)
                WsDateMaj = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Email() As String
            Get
                Return WsEmail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsEmail = value
                    Case Else
                        WsEmail = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Nom & VI.Tild)
                Chaine.Append(Prenom & VI.Tild)
                Chaine.Append(Qualite & VI.Tild)
                Chaine.Append(Initiales & VI.Tild)
                Chaine.Append(TypeUtilisateur & VI.Tild)
                Chaine.Append(GroupeUtilisateur & VI.Tild)
                Chaine.Append(MotdePasse & VI.Tild)
                Chaine.Append(Date_MotdePasse & VI.Tild)
                Chaine.Append(DatabaseCourante.ToString & VI.Tild)
                Chaine.Append(Date_Maj & VI.Tild)
                Chaine.Append(Email)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Nom = TableauData(1)
                Prenom = TableauData(2)
                Qualite = TableauData(3)
                Initiales = TableauData(4)
                TypeUtilisateur = TableauData(5)
                GroupeUtilisateur = TableauData(6)
                MotdePasse = TableauData(7)
                Date_MotdePasse = TableauData(8)
                If TableauData(9) = "" Then
                    TableauData(9) = "0"
                End If
                DatabaseCourante = CInt(TableauData(9))
                Date_Maj = TableauData(10)
                If TableauData.Count > 11 Then
                    Email = TableauData(11)
                End If

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesCouleurs() As List(Of UTI_COULEUR_DOSSIER)
            Get
                If WsLstCouleurs Is Nothing Then
                    Return Nothing
                End If
                Return WsLstCouleurs.OrderBy(Function(m) m.Numero_Icone).ToList
            End Get
        End Property

        Public Property FicheFiltre() As UTI_FILTRE
            Get
                Return WsFicheFiltre
            End Get
            Set(ByVal value As UTI_FILTRE)
                WsFicheFiltre = value
            End Set
        End Property

        Public Property FichePerimetre() As UTI_PERIMETRE
            Get
                Return WsFichePerimetre
            End Get
            Set(ByVal value As UTI_PERIMETRE)
                WsFichePerimetre = value
            End Set
        End Property

        Public Property FicheEvaluation() As UTI_CRITERE_DOSSIER
            Get
                Return WsFicheCritere
            End Get
            Set(ByVal value As UTI_CRITERE_DOSSIER)
                WsFicheCritere = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Couleur(ByVal Fiche As UTI_COULEUR_DOSSIER) As Integer

            If WsLstCouleurs Is Nothing Then
                WsLstCouleurs = New List(Of UTI_COULEUR_DOSSIER)
            End If
            WsLstCouleurs.Add(Fiche)
            Return WsLstCouleurs.Count

        End Function

        Public Function Fiche_Couleur(ByVal NumeroIcone As Integer) As UTI_COULEUR_DOSSIER

            If WsLstCouleurs Is Nothing Then
                Return Nothing
            End If

            Return (From it As UTI_COULEUR_DOSSIER In WsLstCouleurs Where it.Numero_Icone = NumeroIcone Select it).FirstOrDefault()

        End Function

    End Class
End Namespace
