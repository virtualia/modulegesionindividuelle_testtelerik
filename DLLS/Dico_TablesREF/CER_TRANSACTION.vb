﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class CER_TRANSACTION
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '** Données Cryptées
        Private WsEmpreinteCryptee As String = ""
        Private WsDataCryptes As String = ""
        '** Données Décryptées
        Private WsClefPublique As Byte() = Nothing
        Private WsObjetEmpreinte As CER_EMPREINTE
        Private Datas_Significatives As String = ""
        '
        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "CER_TRANSACTION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return 41
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property EmpreinteCryptee() As String
            Get
                Return WsEmpreinteCryptee
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsEmpreinteCryptee = value
                    Case Else
                        WsEmpreinteCryptee = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property DataCryptes() As String
            Get
                Return WsDataCryptes
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsDataCryptes = value
                    Case Else
                        WsDataCryptes = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(EmpreinteCryptee & VI.Tild)
                Chaine.Append(DataCryptes)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 3 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                EmpreinteCryptee = TableauData(1)
                DataCryptes = TableauData(2)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Function Decrypter(ByVal Clef As String) As Boolean
            Try
                WsClefPublique = Convert.FromBase64String(Clef)
            Catch ex As Exception
                WsClefPublique = Nothing
                Return False
            End Try
            If WsDataCryptes = "" Or WsEmpreinteCryptee = "" Then
                Return False
            End If
            Dim Chaine As String

            Chaine = VirRhFonction.ChaineDecryptee(WsEmpreinteCryptee, WsClefPublique)
            WsObjetEmpreinte = New CER_EMPREINTE
            WsObjetEmpreinte.ContenuTable = Chaine
            If WsObjetEmpreinte.SiValide = False Then
                WsObjetEmpreinte = Nothing
                Return False
            End If
            Datas_Significatives = VirRhFonction.ChaineDecryptee(WsDataCryptes, WsClefPublique)

            Return True
        End Function

        Public Function Crypter(ByVal Clef As String) As Boolean
            Try
                WsClefPublique = Convert.FromBase64String(Clef)
            Catch ex As Exception
                WsClefPublique = Nothing
                Return False
            End Try
            If ObjetEmpreinte Is Nothing Or Donnees_Significatives = "" Then
                Return False
            End If
            If ObjetEmpreinte.SiValide = False Then
                Return False
            End If
            WsEmpreinteCryptee = VirRhFonction.ChaineCryptee(ObjetEmpreinte.ContenuTable, WsClefPublique)

            WsDataCryptes = VirRhFonction.ChaineCryptee(Donnees_Significatives, WsClefPublique)

            Return True
        End Function

        Public Property ObjetEmpreinte As CER_EMPREINTE
            Get
                Return WsObjetEmpreinte
            End Get
            Set(value As CER_EMPREINTE)
                WsObjetEmpreinte = value
            End Set
        End Property

        Public Property Donnees_Significatives As String
            Get
                Return Datas_Significatives
            End Get
            Set(value As String)
                Datas_Significatives = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
