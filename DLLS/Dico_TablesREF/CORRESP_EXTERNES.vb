﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class CORRESP_EXTERNES
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsValeurVirtualia As String
        Private WsValeurExterne As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "CORRESP_EXTERNES"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueTranscodification
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Valeur_Virtualia() As String
            Get
                Return WsValeurVirtualia
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsValeurVirtualia = value
                    Case Else
                        WsValeurVirtualia = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Valeur_Externe() As String
            Get
                Return WsValeurExterne
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsValeurExterne = value
                    Case Else
                        WsValeurExterne = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Valeur_Virtualia & VI.Tild)
                Chaine.Append(Valeur_Externe)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 3 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Valeur_Virtualia = TableauData(1)
                Valeur_Externe = TableauData(2)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

