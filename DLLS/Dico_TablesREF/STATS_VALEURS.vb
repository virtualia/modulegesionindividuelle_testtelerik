﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class STATS_VALEURS
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        Private WsRang As Integer
        Private WsPlage As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "STATS_VALEURS"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueScriptStats
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Numero_Rang() As Integer
            Get
                Return WsRang
            End Get
            Set(ByVal value As Integer)
                WsRang = value
            End Set
        End Property

        Public Property Plage() As Integer
            Get
                Return WsPlage
            End Get
            Set(ByVal value As Integer)
                WsPlage = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(CStr(Numero_Rang) & VI.Tild)
                Chaine.Append(CStr(Plage))

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 3 Then
                    Exit Property
                End If
                For IndiceI = 0 To 2
                    If TableauData(IndiceI) = "" Then TableauData(IndiceI) = "0"
                Next IndiceI
                Ide_Dossier = CInt(TableauData(0))
                Numero_Rang = CInt(TableauData(1))
                Plage = CInt(TableauData(2))

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

