﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class MIS_AFFECTATION
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsRang As String
        Private WsNom As String
        Private WsPrenom As String
        Private WsDateNaissance As String
        Private WsNature As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "MIS_AFFECTATION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueMission
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Rang() As String
            Get
                Return WsRang
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 6
                        WsRang = value
                    Case Else
                        WsRang = Strings.Left(value, 6)
                End Select
            End Set
        End Property

        Public Property Nom() As String
            Get
                Return WsNom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsNom = value
                    Case Else
                        WsNom = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Prenom() As String
            Get
                Return WsPrenom
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsPrenom = value
                    Case Else
                        WsPrenom = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Date_de_Naissance() As String
            Get
                Return WsDateNaissance
            End Get
            Set(ByVal value As String)
                WsDateNaissance = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property NaturePersonne() As String
            Get
                Return WsNature
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1
                        WsNature = value
                    Case Else
                        WsNature = Strings.Left(value, 1)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Rang & VI.Tild)
                Chaine.Append(Nom & VI.Tild)
                Chaine.Append(Prenom & VI.Tild)
                Chaine.Append(Date_de_Naissance & VI.Tild)
                Chaine.Append(NaturePersonne)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 6 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Rang = TableauData(1)
                Nom = TableauData(2)
                Prenom = TableauData(3)
                Date_de_Naissance = TableauData(4)
                NaturePersonne = TableauData(5)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace


