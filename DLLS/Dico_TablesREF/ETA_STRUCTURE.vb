﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ETA_STRUCTURE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsRang As Integer
        Private WsIntitule As String
        Private WsNiveauHierarchique As Integer
        Private WsOrdrePresentation As Integer
        Private WsNomResponsable As String
        Private WsIdeResponsable As Integer
        Private WsNoBdResponsable As Integer
        Private WsDelegataire As String
        Private WsIdeDelegataire As Integer
        Private WsNoBdDelegataire As Integer
        Private WsDateDebutDelegation As String
        Private WsDateFinDelegation As String
        Private WsRangParent As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ETA_STRUCTURE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueEtablissement
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 8
            End Get
        End Property

        Public Property Rang() As Integer
            Get
                Return WsRang
            End Get
            Set(ByVal value As Integer)
                WsRang = value
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property NiveauHierarchique() As Integer
            Get
                Return WsNiveauHierarchique
            End Get
            Set(ByVal value As Integer)
                WsNiveauHierarchique = value
            End Set
        End Property

        Public Property OrdredePresentation() As Integer
            Get
                Return WsOrdrePresentation
            End Get
            Set(ByVal value As Integer)
                WsOrdrePresentation = value
            End Set
        End Property

        Public Property NomduResponsable() As String
            Get
                Return WsNomResponsable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsNomResponsable = value
                    Case Else
                        WsNomResponsable = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property IdentifiantduResponsable() As Integer
            Get
                Return WsIdeResponsable
            End Get
            Set(ByVal value As Integer)
                WsIdeResponsable = value
            End Set
        End Property

        Public Property NoBdResponsable() As Integer
            Get
                Select Case WsNoBdResponsable
                    Case Is = 0
                        Return 1
                    Case Else
                        Return WsNoBdResponsable
                End Select
            End Get
            Set(ByVal value As Integer)
                WsNoBdResponsable = value
            End Set
        End Property

        Public Property NomduDelegataire() As String
            Get
                Return WsDelegataire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsDelegataire = value
                    Case Else
                        WsDelegataire = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property IdentifiantduDelegataire() As Integer
            Get
                Return WsIdeDelegataire
            End Get
            Set(ByVal value As Integer)
                WsIdeDelegataire = value
            End Set
        End Property

        Public Property NoBdDelegataire() As Integer
            Get
                Select Case WsNoBdDelegataire
                    Case Is = 0
                        Return 1
                    Case Else
                        Return WsNoBdDelegataire
                End Select
            End Get
            Set(ByVal value As Integer)
                WsNoBdDelegataire = value
            End Set
        End Property

        Public Property Date_de_Debut_Delegation() As String
            Get
                Return WsDateDebutDelegation
            End Get
            Set(ByVal value As String)
                WsDateDebutDelegation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_Fin_Delegation() As String
            Get
                Return WsDateFinDelegation
            End Get
            Set(ByVal value As String)
                WsDateFinDelegation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Rang_Parent() As Integer
            Get
                Return WsRangParent
            End Get
            Set(ByVal value As Integer)
                WsRangParent = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Rang.ToString & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(NiveauHierarchique.ToString & VI.Tild)
                Chaine.Append(OrdredePresentation.ToString & VI.Tild)
                Chaine.Append(NomduResponsable & VI.Tild)
                Chaine.Append(IdentifiantduResponsable.ToString & VI.Tild)
                Chaine.Append(NoBdResponsable.ToString & VI.Tild)
                Chaine.Append(NomduDelegataire & VI.Tild)
                Chaine.Append(IdentifiantduDelegataire.ToString & VI.Tild)
                Chaine.Append(NoBdDelegataire.ToString & VI.Tild)
                Chaine.Append(Date_de_Debut_Delegation & VI.Tild)
                Chaine.Append(Date_de_Fin_Delegation & VI.Tild)
                Chaine.Append(Rang_Parent.ToString & VI.Tild)
                Chaine.Append(MyBase.Date_de_Fin)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 16 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then Rang = CInt(TableauData(2))
                Intitule = TableauData(3)
                If TableauData(4) <> "" Then NiveauHierarchique = CInt(TableauData(4))
                If TableauData(5) <> "" Then OrdredePresentation = CInt(TableauData(5))
                NomduResponsable = TableauData(6)
                If TableauData(7) <> "" Then IdentifiantduResponsable = CInt(TableauData(7))
                If TableauData(8) <> "" Then NoBdResponsable = CInt(TableauData(8))
                NomduDelegataire = TableauData(9)
                If TableauData(10) <> "" Then IdentifiantduDelegataire = CInt(TableauData(10))
                If TableauData(11) <> "" Then NoBdDelegataire = CInt(TableauData(11))
                Date_de_Debut_Delegation = TableauData(12)
                Date_de_Fin_Delegation = TableauData(13)
                If TableauData(14) <> "" Then Rang_Parent = CInt(TableauData(14))
                MyBase.Date_de_Fin = TableauData(15)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

