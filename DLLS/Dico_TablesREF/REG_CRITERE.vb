﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class REG_CRITERE
        Inherits VIR_FICHE
        Implements ICloneable

        Private WsFicheLue As StringBuilder
        Private WsSiMajuscule As Boolean

        Private WsIde_Critere As Integer = 0
        Private WsListe_Valeurs As List(Of REG_VALEUR) = New List(Of REG_VALEUR)()

        Private WsObjet As Integer = 0
        Private WsNomObjet As String = ""
        Private WsCondition As String = ""
        Private WsInformation1 As Integer = 0
        Private WsInformation2 As Integer = -1
        Private WsNomInformation1 As String = ""
        Private WsNomInformation2 As String = ""
        Private WsValeurApplicable As String = ""
        Private WsActifDefaultValue As String = ""

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueRegles
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 6
            End Get
        End Property

        Public Property Ide_Critere() As Integer
            Get
                Return WsIde_Critere
            End Get
            Set(ByVal value As Integer)
                WsIde_Critere = value
            End Set
        End Property

        Public Property Objet() As Integer
            Get
                Return WsObjet
            End Get
            Set(ByVal value As Integer)
                WsObjet = value
            End Set
        End Property

        Public Property NomObjet() As String
            Get
                Return WsNomObjet
            End Get
            Set(ByVal value As String)
                WsNomObjet = value
            End Set
        End Property

        Public Property Condition() As String
            Get
                Return WsCondition
            End Get
            Set(ByVal value As String)
                WsCondition = F_FormatAlpha(value, 20)
            End Set
        End Property

        Public Property Information1() As Integer
            Get
                Return WsInformation1
            End Get
            Set(ByVal value As Integer)
                WsInformation1 = value
                WsNomInformation1 = value.ToString
            End Set
        End Property

        Public Property Information2() As Integer
            Get
                Return WsInformation2
            End Get
            Set(ByVal value As Integer)
                WsInformation2 = value
                WsNomInformation2 = value.ToString
            End Set
        End Property

        Public Property NomInformation1() As String
            Get
                Return WsNomInformation1
            End Get
            Set(ByVal value As String)
                WsNomInformation1 = value
            End Set
        End Property

        Public Property NomInformation2() As String
            Get
                Return WsNomInformation2
            End Get
            Set(ByVal value As String)
                WsNomInformation2 = value
            End Set
        End Property

        Public Property ValeurApplicable() As String
            Get
                Return WsValeurApplicable
            End Get
            Set(ByVal value As String)
                WsValeurApplicable = F_FormatAlpha(value, 250)
            End Set
        End Property

        Public Property ActifDefaultValue() As String
            Get
                Return WsActifDefaultValue
            End Get
            Set(ByVal value As String)
                Select Case value
                    Case Is = ""
                        WsActifDefaultValue = ""
                    Case Is = "0", "Non"
                        WsActifDefaultValue = "Non"
                    Case Else
                        WsActifDefaultValue = "Oui"
                End Select
            End Set
        End Property

        Public ReadOnly Property Intitule_Condition As String
            Get
                Select Case Condition
                    Case "=", "IN"
                        Return "Egalité avec"
                    Case "<>", "NOT IN"
                        Return "Différence avec"
                    Case ">"
                        Return "Supérieur à"
                    Case "<"
                        Return "Inférieur à"
                    Case "<="
                        Return "Inférieur ou égal à"
                    Case ">="
                        Return "Supérieur ou égal à"
                    Case "BETWEEN"
                        Return "Compris entre un minimun et un maximun"
                    Case "NOT BETWEEN"
                        Return "Non compris entre un minimun et un maximun"
                    Case "INSTR"
                        Return "Comporte"
                    Case "NOT INSTR"
                        Return "Ne comporte pas"
                    Case Else
                        Return Condition
                End Select
            End Get
        End Property

        Public Sub Add_REG_VALEUR(ByRef Reg_Valeur As REG_VALEUR)
            WsListe_Valeurs.Add(Reg_Valeur)
        End Sub

        Public ReadOnly Property Liste_REG_VALEUR() As List(Of REG_VALEUR)
            Get
                Return WsListe_Valeurs
            End Get
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Ide_Critere.ToString & VI.Tild)
                Chaine.Append(Objet.ToString & VI.Tild)
                Chaine.Append(Condition & VI.Tild)
                Chaine.Append(Information1.ToString & VI.Tild)
                Chaine.Append(Information2.ToString & VI.Tild)
                Chaine.Append(ValeurApplicable & VI.Tild)
                Chaine.Append(ActifDefaultValue)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)

                Ide_Dossier = CInt(TableauData(0))

                If TableauData(1) = "" Then TableauData(1) = "0"
                Ide_Critere = CInt(TableauData(1))
                If TableauData(2) = "" Then TableauData(2) = "0"
                Objet = CInt(TableauData(2))
                Condition = TableauData(3)
                If TableauData(4) = "" Then TableauData(4) = "0"
                Information1 = CInt(TableauData(4))
                If TableauData(5) = "" Then TableauData(5) = "0"
                Information2 = CInt(TableauData(5))
                ValeurApplicable = TableauData(6)
                ActifDefaultValue = TableauData(7)


                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To UBound(TableauData) - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return ""
                    Case 1
                        Return WsIde_Critere.ToString
                    Case 2
                        Return WsObjet.ToString
                    Case 3
                        Return WsCondition
                    Case 4
                        Return WsInformation1.ToString
                    Case 5
                        Return WsInformation2.ToString
                    Case 6
                        Return WsValeurApplicable
                    Case 7
                        Return WsActifDefaultValue
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Overridable Function Clone() As Object _
                                   Implements ICloneable.Clone
            Dim CloneREG_CRITERE As New REG_CRITERE
            Dim CloneREG_VALEUR As REG_VALEUR
            Dim K As Integer

            CloneREG_CRITERE.ContenuTable = Ide_Dossier.ToString & ContenuTable 'Non daté
            CloneREG_CRITERE.NomInformation1 = Me.NomInformation1
            CloneREG_CRITERE.NomInformation2 = Me.NomInformation2
            CloneREG_CRITERE.NomObjet = Me.NomObjet
            For K = 0 To WsListe_Valeurs.Count - 1
                CloneREG_VALEUR = New REG_VALEUR
                CloneREG_VALEUR = WsListe_Valeurs(K).Clone
                CloneREG_CRITERE.WsListe_Valeurs.Add(CloneREG_VALEUR)
            Next K

            Return CloneREG_CRITERE
        End Function

    End Class
End Namespace