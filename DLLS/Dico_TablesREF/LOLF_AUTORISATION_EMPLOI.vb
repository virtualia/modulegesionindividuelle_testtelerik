﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class LOLF_AUTORISATION_EMPLOI
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsNumeroPgm As Integer = 0
        Private WsNumeroAction As String
        Private WsNumeroAE As Integer = 0
        Private WsCategorieFonctionnelle As String
        Private WsNombre As Integer = 0
        Private WsDateOuverture As String
        Private WsDateFermeture As String
        Private WsChapitre As String
        Private WsCredits As Double = 0
        Private WsDescriptif As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "LOLF_AUTORISATION_EMPLOI"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueLolf
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 4
            End Get
        End Property

        Public Property NumeroduProgramme() As Integer
            Get
                Return WsNumeroPgm
            End Get
            Set(ByVal value As Integer)
                WsNumeroPgm = value
            End Set
        End Property

        Public Property NumerodelAction() As String
            Get
                Return WsNumeroAction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 7
                        WsNumeroAction = value
                    Case Else
                        WsNumeroAction = Strings.Left(value, 7)
                End Select
            End Set
        End Property

        Public Property NumerodelAutorisation() As Integer
            Get
                Return WsNumeroAE
            End Get
            Set(ByVal value As Integer)
                WsNumeroAE = value
            End Set
        End Property

        Public Property CategorieFonctionnelle() As String
            Get
                Return WsCategorieFonctionnelle
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsCategorieFonctionnelle = value
                    Case Else
                        WsCategorieFonctionnelle = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Nombre() As Integer
            Get
                Return WsNombre
            End Get
            Set(ByVal value As Integer)
                WsNombre = value
            End Set
        End Property

        Public Property DatedOuverture() As String
            Get
                Return WsDateOuverture
            End Get
            Set(ByVal value As String)
                WsDateOuverture = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property DatedeFermeture() As String
            Get
                Return WsDateFermeture
            End Get
            Set(ByVal value As String)
                WsDateFermeture = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Chapitre() As String
            Get
                Return WsChapitre
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsChapitre = value
                    Case Else
                        WsChapitre = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Credits() As Double
            Get
                Return WsCredits
            End Get
            Set(ByVal value As Double)
                WsCredits = value
            End Set
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsDescriptif = value
                    Case Else
                        WsDescriptif = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(NumeroduProgramme.ToString & VI.Tild)
                Chaine.Append(NumerodelAction & VI.Tild)
                Chaine.Append(NumerodelAutorisation.ToString & VI.Tild)
                Chaine.Append(CategorieFonctionnelle & VI.Tild)
                Chaine.Append(Nombre & VI.Tild)
                Chaine.Append(DatedOuverture & VI.Tild)
                Chaine.Append(DatedeFermeture & VI.Tild)
                Chaine.Append(Chapitre & VI.Tild)
                Chaine.Append(Credits.ToString & VI.Tild)
                Chaine.Append(Descriptif)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) <> "" Then NumeroduProgramme = CInt(TableauData(1))
                NumerodelAction = TableauData(2)
                If TableauData(3) <> "" Then NumerodelAutorisation = CInt(TableauData(3))
                CategorieFonctionnelle = TableauData(4)
                If TableauData(5) <> "" Then Nombre = CInt(TableauData(5))
                DatedOuverture = TableauData(6)
                DatedeFermeture = TableauData(7)
                Chapitre = TableauData(8)
                If TableauData(9) <> "" Then Credits = VirRhFonction.ConversionDouble(TableauData(9))
                Descriptif = TableauData(10)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

