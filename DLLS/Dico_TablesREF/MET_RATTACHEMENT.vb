﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class MET_RATTACHEMENT
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsRattachement As String
        Private WsDirection As String
        Private WsService As String
        Private WsCategorie As String
        Private WsTendances_evolution As String
        Private WsPasserellemobilite_no1 As String
        Private WsPasserellemobilite_no2 As String
        Private WsPasserellemobilite_no3 As String
        Private WsPasserellemobilite_no4 As String
        Private WsPasserellemobilite_no5 As String


        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "MET_RATTACHEMENT"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueMetier
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 6
            End Get
        End Property

        Public Property Rattachement() As String
            Get
                Return WsRattachement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsRattachement = value
                    Case Else
                        WsRattachement = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Direction() As String
            Get
                Return WsDirection
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsDirection = value
                    Case Else
                        WsDirection = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Service() As String
            Get
                Return WsService
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsService = value
                    Case Else
                        WsService = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Categorie() As String
            Get
                Return WsCategorie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsCategorie = value
                    Case Else
                        WsCategorie = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Tendances_evolution() As String
            Get
                Return WsTendances_evolution
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsTendances_evolution = value
                    Case Else
                        WsTendances_evolution = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Passerellemobilite_no1() As String
            Get
                Return WsPasserellemobilite_no1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsPasserellemobilite_no1 = value
                    Case Else
                        WsPasserellemobilite_no1 = Strings.Left(value, 120)
                End Select
            End Set
        End Property
        Public Property Passerellemobilite_no2() As String
            Get
                Return WsPasserellemobilite_no2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsPasserellemobilite_no2 = value
                    Case Else
                        WsPasserellemobilite_no2 = Strings.Left(value, 120)
                End Select
            End Set
        End Property
        Public Property Passerellemobilite_no3() As String
            Get
                Return WsPasserellemobilite_no3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsPasserellemobilite_no3 = value
                    Case Else
                        WsPasserellemobilite_no3 = Strings.Left(value, 120)
                End Select
            End Set
        End Property
        Public Property Passerellemobilite_no4() As String
            Get
                Return WsPasserellemobilite_no4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsPasserellemobilite_no4 = value
                    Case Else
                        WsPasserellemobilite_no4 = Strings.Left(value, 120)
                End Select
            End Set
        End Property
        Public Property Passerellemobilite_no5() As String
            Get
                Return WsPasserellemobilite_no5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsPasserellemobilite_no5 = value
                    Case Else
                        WsPasserellemobilite_no5 = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Rattachement & VI.Tild)
                Chaine.Append(Direction & VI.Tild)
                Chaine.Append(Service & VI.Tild)
                Chaine.Append(Categorie & VI.Tild)
                Chaine.Append(Tendances_evolution & VI.Tild)
                Chaine.Append(Passerellemobilite_no1 & VI.Tild)
                Chaine.Append(Passerellemobilite_no2 & VI.Tild)
                Chaine.Append(Passerellemobilite_no3 & VI.Tild)
                Chaine.Append(Passerellemobilite_no4 & VI.Tild)
                Chaine.Append(Passerellemobilite_no5)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Rattachement = TableauData(1)
                Direction = TableauData(2)
                Service = TableauData(3)
                Categorie = TableauData(4)
                Tendances_evolution = TableauData(5)
                Passerellemobilite_no1 = TableauData(6)
                Passerellemobilite_no2 = TableauData(7)
                Passerellemobilite_no3 = TableauData(8)
                Passerellemobilite_no4 = TableauData(9)
                Passerellemobilite_no5 = TableauData(10)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace


