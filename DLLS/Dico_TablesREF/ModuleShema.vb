﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports Virtualia.Systeme.Fonctions
Imports VI = Virtualia.Systeme.Constantes

Module ModuleShema

    Private WsRhDate As CalculDates = Nothing
    Private WsRhFonction As Generales = Nothing

    Public ReadOnly Property VirRhDate() As CalculDates
        Get
            If WsRhDate Is Nothing Then
                WsRhDate = New CalculDates
            End If
            Return WsRhDate
        End Get
    End Property

    Public ReadOnly Property VirRhFonction() As Generales
        Get
            If WsRhFonction Is Nothing Then
                WsRhFonction = New Generales
            End If
            Return WsRhFonction
        End Get
    End Property

    Public Function EstPlusGrand(DateValeur As String, DateCible As String)
        Return VirRhDate.ComparerDates(DateValeur, DateCible) = VI.ComparaisonDates.PlusGrand
    End Function
    Public Function EstPlusPetit(DateValeur As String, DateCible As String)
        Return VirRhDate.ComparerDates(DateValeur, DateCible) = VI.ComparaisonDates.PlusPetit
    End Function
    Public Function EstEgal(DateValeur As String, DateCible As String)
        Return VirRhDate.ComparerDates(DateValeur, DateCible) = VI.ComparaisonDates.Egalite
    End Function
    Public Function EstPlusGrand_Ou_Egal(DateValeur As String, DateCible As String)
        Return VirRhDate.ComparerDates(DateValeur, DateCible) = VI.ComparaisonDates.Egalite Or VirRhDate.ComparerDates(DateValeur, DateCible) = VI.ComparaisonDates.PlusGrand
    End Function
    Public Function EstPlusPetit_Ou_Egal(DateValeur As String, DateCible As String)
        Return VirRhDate.ComparerDates(DateValeur, DateCible) = VI.ComparaisonDates.Egalite Or VirRhDate.ComparerDates(DateValeur, DateCible) = VI.ComparaisonDates.PlusPetit
    End Function


End Module