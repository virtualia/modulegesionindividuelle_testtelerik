﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class CYC_IDENTIFICATION
        Inherits VIR_FICHE

        Private WsLstCycles As List(Of CYC_UNITE)
        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsCommentaire As String
        Private WsBaseUnite As Integer
        Private WsRegle_Valo_N1 As String
        Private WsRegle_Valo_N2 As String
        Private WsRegle_Valo_N3 As String
        Private WsRegle_Valo_N4 As String
        Private WsRegle_Valo_N5 As String
        Private WsJFTravailles As Integer
        Private WsEtablissement As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "CYC_IDENTIFICATION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueBaseHebdo
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Commentaire() As String
            Get
                Return WsCommentaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsCommentaire = value
                    Case Else
                        WsCommentaire = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property BasedelUnite() As Integer
            Get
                Return WsBaseUnite
            End Get
            Set(ByVal value As Integer)
                WsBaseUnite = value
            End Set
        End Property

        Public Property Regle_Valorisation_N1() As String
            Get
                Return WsRegle_Valo_N1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsRegle_Valo_N1 = value
                    Case Else
                        WsRegle_Valo_N1 = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Regle_Valorisation_N2() As String
            Get
                Return WsRegle_Valo_N2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsRegle_Valo_N2 = value
                    Case Else
                        WsRegle_Valo_N2 = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Regle_Valorisation_N3() As String
            Get
                Return WsRegle_Valo_N3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsRegle_Valo_N3 = value
                    Case Else
                        WsRegle_Valo_N3 = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Regle_Valorisation_N4() As String
            Get
                Return WsRegle_Valo_N4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsRegle_Valo_N4 = value
                    Case Else
                        WsRegle_Valo_N4 = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property Regle_Valorisation_N5() As String
            Get
                Return WsRegle_Valo_N5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 40
                        WsRegle_Valo_N5 = value
                    Case Else
                        WsRegle_Valo_N5 = Strings.Left(value, 40)
                End Select
            End Set
        End Property

        Public Property JoursFeries_Travailles() As Integer
            Get
                Return WsJFTravailles
            End Get
            Set(ByVal value As Integer)
                WsJFTravailles = value
            End Set
        End Property

        Public Property Etablissement() As String
            Get
                Return WsEtablissement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsEtablissement = value
                    Case Else
                        WsEtablissement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Commentaire & VI.Tild)
                Chaine.Append(BasedelUnite.ToString & VI.Tild)
                Chaine.Append(Regle_Valorisation_N1 & VI.Tild)
                Chaine.Append(Regle_Valorisation_N2 & VI.Tild)
                Chaine.Append(Regle_Valorisation_N3 & VI.Tild)
                Chaine.Append(Regle_Valorisation_N4 & VI.Tild)
                Chaine.Append(Regle_Valorisation_N5 & VI.Tild)
                Chaine.Append(JoursFeries_Travailles.ToString & VI.Tild)
                Chaine.Append(Etablissement)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                Commentaire = TableauData(2)
                If TableauData(3) = "" Then TableauData(3) = "0"
                BasedelUnite = CInt(TableauData(3))
                Regle_Valorisation_N1 = TableauData(4)
                Regle_Valorisation_N2 = TableauData(5)
                Regle_Valorisation_N3 = TableauData(6)
                Regle_Valorisation_N4 = TableauData(7)
                Regle_Valorisation_N5 = TableauData(8)
                If TableauData(9) = "" Then TableauData(9) = "0"
                JoursFeries_Travailles = CInt(TableauData(9))
                If TableauData(10) <> "" Then
                    Etablissement = TableauData(10)
                Else
                    Etablissement = "Commun"
                End If

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property NombreHeures_Total As String
            Get
                If WsLstCycles Is Nothing Then
                    Return ""
                End If

                Dim Total As Integer = (From c As CYC_UNITE In WsLstCycles Select c.NombreHeures_Total_Minutes).Sum()

                Return VirRhDate.CalcHeure(Total.ToString, "", 2)
            End Get
        End Property

        Public ReadOnly Property ListedesCycles() As List(Of CYC_UNITE)
            Get
                If WsLstCycles Is Nothing Then
                    Return Nothing
                End If
                Return WsLstCycles.OrderBy(Function(m) m.NumeroObjet).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Fiche_Cycle(ByVal NumObjet As Integer) As CYC_UNITE

            If WsLstCycles Is Nothing Then
                Return Nothing
            End If

            Return (From c As CYC_UNITE In WsLstCycles Where c.NumeroObjet = NumObjet Select c).FirstOrDefault()

        End Function

        Public Function Ajouter_Cycle(ByVal Fiche As CYC_UNITE) As Integer
            If WsLstCycles Is Nothing Then
                WsLstCycles = New List(Of CYC_UNITE)
            End If
            WsLstCycles.Add(Fiche)
            Return WsLstCycles.Count
        End Function

    End Class
End Namespace


