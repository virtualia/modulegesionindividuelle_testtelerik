Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAYS_DESCRIPTION
        Inherits VIR_FICHE

        Private WsLstJFFixe As List(Of PAYS_JOURFERIE_FIXE)
        Private WsLstJFMobile As List(Of PAYS_JOURFERIE_MOBILE)
        Private WsLstConge As List(Of PAYS_DROITS_CONGE)
        Private WsLstVoyage As List(Of PAYS_VOYAGE_CONGE)
        Private WsLstBareme As List(Of PAYS_MIS_ETRANGER)
        Private WsLstTaux As List(Of PAYS_TAUX_CHANGE)
        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsCodepays As String
        Private WsSuffixeinternet As String
        Private WsLangue As String
        Private WsCodemonnaie As String
        Private WsMonnaie As String
        Private WsZone_euro As String
        Private WsJour_ouvrable As String
        Private WsJourrepos As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAYS_DESCRIPTION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePays
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Codepays() As String
            Get
                Return WsCodepays
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsCodepays = value
                    Case Else
                        WsCodepays = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Suffixeinternet() As String
            Get
                Return WsSuffixeinternet
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSuffixeinternet = value
                    Case Else
                        WsSuffixeinternet = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Langue() As String
            Get
                Return WsLangue
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsLangue = value
                    Case Else
                        WsLangue = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Codemonnaie() As String
            Get
                Return WsCodemonnaie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsCodemonnaie = value
                    Case Else
                        WsCodemonnaie = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Monnaie() As String
            Get
                Return WsMonnaie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsMonnaie = value
                    Case Else
                        WsMonnaie = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Zone_euro() As String
            Get
                Select Case WsZone_euro
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsZone_euro
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsZone_euro = value
                    Case Else
                        WsZone_euro = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Jour_ouvrable() As String
            Get
                Return WsJour_ouvrable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsJour_ouvrable = value
                    Case Else
                        WsJour_ouvrable = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Jour_Repos() As String
            Get
                Return WsJourrepos
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsJourrepos = value
                    Case Else
                        WsJourrepos = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Codepays & VI.Tild)
                Chaine.Append(Suffixeinternet & VI.Tild)
                Chaine.Append(Langue & VI.Tild)
                Chaine.Append(Codemonnaie & VI.Tild)
                Chaine.Append(Monnaie & VI.Tild)
                Chaine.Append(Zone_euro & VI.Tild)
                Chaine.Append(Jour_ouvrable & VI.Tild)
                Chaine.Append(Jour_Repos)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 10 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                Codepays = TableauData(2)
                Suffixeinternet = TableauData(3)
                Langue = TableauData(4)
                Codemonnaie = TableauData(5)
                Monnaie = TableauData(6)
                Zone_euro = TableauData(7)
                Jour_ouvrable = TableauData(8)
                Jour_Repos = TableauData(9)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public ReadOnly Property JourOuvrableNumeric() As Integer
            Get
                Select Case Jour_ouvrable
                    Case Is = VI.Jours.Lundi.ToString
                        Return VI.Jours.Lundi
                    Case Is = VI.Jours.Mardi.ToString
                        Return VI.Jours.Mardi
                    Case Is = VI.Jours.Mercredi.ToString
                        Return VI.Jours.Mercredi
                    Case Is = VI.Jours.Jeudi.ToString
                        Return VI.Jours.Jeudi
                    Case Is = VI.Jours.Vendredi.ToString
                        Return VI.Jours.Vendredi
                    Case Is = VI.Jours.Samedi.ToString
                        Return VI.Jours.Samedi
                    Case Is = VI.Jours.Dimanche.ToString
                        Return VI.Jours.Dimanche
                    Case Else
                        Return VI.Jours.Samedi
                End Select
            End Get
        End Property

        Public ReadOnly Property JourReposNumeric() As Integer
            Get
                Select Case Jour_Repos
                    Case Is = VI.Jours.Lundi.ToString
                        Return VI.Jours.Lundi
                    Case Is = VI.Jours.Mardi.ToString
                        Return VI.Jours.Mardi
                    Case Is = VI.Jours.Mercredi.ToString
                        Return VI.Jours.Mercredi
                    Case Is = VI.Jours.Jeudi.ToString
                        Return VI.Jours.Jeudi
                    Case Is = VI.Jours.Vendredi.ToString
                        Return VI.Jours.Vendredi
                    Case Is = VI.Jours.Samedi.ToString
                        Return VI.Jours.Samedi
                    Case Is = VI.Jours.Dimanche.ToString
                        Return VI.Jours.Dimanche
                    Case Else
                        Return VI.Jours.Dimanche
                End Select
            End Get
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesDroitsConge() As List(Of PAYS_DROITS_CONGE)
            Get
                If WsLstConge Is Nothing Then
                    Return Nothing
                End If
                Return WsLstConge.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesJF_Fixe() As List(Of PAYS_JOURFERIE_FIXE)
            Get
                If WsLstJFFixe Is Nothing Then
                    Return Nothing
                End If
                Return WsLstJFFixe.OrderBy(Function(m) m.Intitule).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesJF_Mobile() As List(Of PAYS_JOURFERIE_MOBILE)
            Get
                If WsLstJFMobile Is Nothing Then
                    Return Nothing
                End If
                Return WsLstJFMobile.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesVoyagesConge() As List(Of PAYS_VOYAGE_CONGE)
            Get
                If WsLstConge Is Nothing Then
                    Return Nothing
                End If
                Return WsLstVoyage.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesBaremes() As List(Of PAYS_MIS_ETRANGER)
            Get
                If WsLstBareme Is Nothing Then
                    Return Nothing
                End If
                Return WsLstBareme.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public ReadOnly Property ListedesTauxChange() As List(Of PAYS_TAUX_CHANGE)
            Get
                If WsLstBareme Is Nothing Then
                    Return Nothing
                End If
                Return WsLstTaux.OrderBy(Function(m) m.Date_Valeur_ToDate).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_JF_Fixe(ByVal Fiche As PAYS_JOURFERIE_FIXE) As Integer

            If WsLstJFFixe Is Nothing Then
                WsLstJFFixe = New List(Of PAYS_JOURFERIE_FIXE)
            End If
            WsLstJFFixe.Add(Fiche)
            Return WsLstJFFixe.Count

        End Function

        Public Function Fiche_JF_Fixe(ByVal DateValeur As String) As PAYS_JOURFERIE_FIXE

            If WsLstJFFixe Is Nothing Then
                Return Nothing
            End If

            Return (From it As PAYS_JOURFERIE_FIXE In WsLstJFFixe Where Strings.Left(DateValeur, 5) = it.JourFerie Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_JF_Mobile(ByVal Fiche As PAYS_JOURFERIE_MOBILE) As Integer

            If WsLstJFMobile Is Nothing Then
                WsLstJFMobile = New List(Of PAYS_JOURFERIE_MOBILE)
            End If
            WsLstJFMobile.Add(Fiche)
            Return WsLstJFMobile.Count

        End Function

        Public Function Fiche_JF_Mobile(ByVal DateValeur As String) As PAYS_JOURFERIE_MOBILE

            If WsLstJFMobile Is Nothing Then
                Return Nothing
            End If

            Return (From it As PAYS_JOURFERIE_MOBILE In ListedesJF_Mobile() Where EstEgal(DateValeur, it.Date_de_Valeur) Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_DroitConge(ByVal Fiche As PAYS_DROITS_CONGE) As Integer

            If WsLstConge Is Nothing Then
                WsLstConge = New List(Of PAYS_DROITS_CONGE)
            End If
            WsLstConge.Add(Fiche)
            Return WsLstConge.Count

        End Function

        Public Function Fiche_DroitCongee(ByVal DateValeur As String) As PAYS_DROITS_CONGE

            If WsLstConge Is Nothing Then
                Return Nothing
            End If

            Return (From it As PAYS_DROITS_CONGE In ListedesDroitsConge Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_VoyageConge(ByVal Fiche As PAYS_VOYAGE_CONGE) As Integer

            If WsLstVoyage Is Nothing Then
                WsLstVoyage = New List(Of PAYS_VOYAGE_CONGE)
            End If
            WsLstVoyage.Add(Fiche)
            Return WsLstVoyage.Count

        End Function

        Public Function Fiche_VoyageConge(ByVal DateValeur As String) As PAYS_VOYAGE_CONGE

            If WsLstConge Is Nothing Then
                Return Nothing
            End If

            Return (From it As PAYS_VOYAGE_CONGE In ListedesVoyagesConge Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_Bareme(ByVal Fiche As PAYS_MIS_ETRANGER) As Integer

            If WsLstBareme Is Nothing Then
                WsLstBareme = New List(Of PAYS_MIS_ETRANGER)
            End If
            WsLstBareme.Add(Fiche)
            Return WsLstBareme.Count

        End Function

        Public Function Fiche_Bareme(ByVal DateValeur As String) As PAYS_MIS_ETRANGER

            If WsLstConge Is Nothing Then
                Return Nothing
            End If

            Return (From it As PAYS_MIS_ETRANGER In ListedesBaremes Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) Select it).FirstOrDefault()

        End Function

        Public Function Ajouter_TauxChange(ByVal Fiche As PAYS_TAUX_CHANGE) As Integer

            If WsLstTaux Is Nothing Then
                WsLstTaux = New List(Of PAYS_TAUX_CHANGE)
            End If
            WsLstTaux.Add(Fiche)
            Return WsLstTaux.Count

        End Function

        Public Function Fiche_TauxChange(ByVal DateValeur As String) As PAYS_TAUX_CHANGE

            If WsLstConge Is Nothing Then
                Return Nothing
            End If

            Return (From it As PAYS_TAUX_CHANGE In ListedesTauxChange Where EstPlusGrand_Ou_Egal(DateValeur, it.Date_de_Valeur) Select it).FirstOrDefault()

        End Function

    End Class
End Namespace

