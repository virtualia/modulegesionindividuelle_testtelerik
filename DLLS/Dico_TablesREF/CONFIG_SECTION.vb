﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class CONFIG_SECTION
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsCategorie As String
        Private WsDescriptif As String
        '
        Private WsListe_SousSection As List(Of CONFIG_SOUS_SECTION) = Nothing

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "CONFIG_SECTION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueConfiguration
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Categorie() As String
            Get
                Return WsCategorie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsCategorie = value
                    Case Else
                        WsCategorie = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsDescriptif = value
                    Case Else
                        WsDescriptif = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder
                Chaine = New StringBuilder
                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Categorie & VI.Tild)
                Chaine.Append(Descriptif)
                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If
                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                Categorie = TableauData(2)
                Descriptif = TableauData(3)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesSousSections() As List(Of CONFIG_SOUS_SECTION)
            Get
                If WsListe_SousSection Is Nothing Then
                    Return Nothing
                End If
                Return WsListe_SousSection.OrderBy(Function(m) m.Intitule).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Paragraphe(ByVal Fiche As CONFIG_SOUS_SECTION) As Integer
            If WsListe_SousSection Is Nothing Then
                WsListe_SousSection = New List(Of CONFIG_SOUS_SECTION)
            End If
            WsListe_SousSection.Add(Fiche)
            Return WsListe_SousSection.Count
        End Function

        Public Function Sous_Section(ByVal Intitule As String) As CONFIG_SOUS_SECTION
            If (WsListe_SousSection Is Nothing) Then
                Return Nothing
            End If
            Return (From s As CONFIG_SOUS_SECTION In WsListe_SousSection Where s.Intitule = Intitule Select s).FirstOrDefault()
        End Function

    End Class
End Namespace
