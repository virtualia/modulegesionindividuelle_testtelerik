﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class MET_VECTEUR
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsVecteur_connaissancesrequises As String
        Private WsVecteur_qualitesrequises As String
        Private WsContexte_connaissancesrequises As String
        Private WsContexte_qualitesrequises As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "MET_VECTEUR"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueMetier
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 7
            End Get
        End Property

        Public Property Vecteur_connaissancesrequises() As String
            Get
                Return WsVecteur_connaissancesrequises
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 96
                        WsVecteur_connaissancesrequises = value
                    Case Else
                        WsVecteur_connaissancesrequises = Strings.Left(value, 96)
                End Select
            End Set
        End Property
        Public Property Vecteur_qualitesrequises() As String
            Get
                Return WsVecteur_qualitesrequises
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 8
                        WsVecteur_qualitesrequises = value
                    Case Else
                        WsVecteur_qualitesrequises = Strings.Left(value, 8)
                End Select
            End Set
        End Property
        Public Property Contexte_connaissancesrequises() As String
            Get
                Return WsContexte_connaissancesrequises
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsContexte_connaissancesrequises = value
                    Case Else
                        WsContexte_connaissancesrequises = Strings.Left(value, 200)
                End Select
            End Set
        End Property
        Public Property Contexte_qualitesrequises() As String
            Get
                Return WsContexte_qualitesrequises
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsContexte_qualitesrequises = value
                    Case Else
                        WsContexte_qualitesrequises = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Vecteur_connaissancesrequises & VI.Tild)
                Chaine.Append(Vecteur_qualitesrequises & VI.Tild)
                Chaine.Append(Contexte_connaissancesrequises & VI.Tild)
                Chaine.Append(Contexte_qualitesrequises)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 5 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                WsVecteur_connaissancesrequises = TableauData(1)
                WsVecteur_qualitesrequises = TableauData(2)
                Contexte_connaissancesrequises = TableauData(3)
                Contexte_qualitesrequises = TableauData(4)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

