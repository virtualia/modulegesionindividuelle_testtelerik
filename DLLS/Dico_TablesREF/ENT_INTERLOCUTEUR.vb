﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ENT_INTERLOCUTEUR
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsNom_interlocuteur As String
        Private WsFonction_interlocuteur As String
        Private WsTelephone_interlocuteur As String
        Private WsIntervenants As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ENT_INTERLOCUTEUR"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueEntreprise
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Nom_interlocuteur() As String
            Get
                Return WsNom_interlocuteur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsNom_interlocuteur = value
                    Case Else
                        WsNom_interlocuteur = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property Fonction_interlocuteur() As String
            Get
                Return WsFonction_interlocuteur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsFonction_interlocuteur = value
                    Case Else
                        WsFonction_interlocuteur = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Telephone_interlocuteur() As String
            Get
                Return WsTelephone_interlocuteur
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsTelephone_interlocuteur = value
                    Case Else
                        WsTelephone_interlocuteur = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Intervenants() As String
            Get
                Return WsIntervenants
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsIntervenants = value
                    Case Else
                        WsIntervenants = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Nom_interlocuteur & VI.Tild)
                Chaine.Append(Fonction_interlocuteur & VI.Tild)
                Chaine.Append(Telephone_interlocuteur & VI.Tild)
                Chaine.Append(Intervenants)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 5 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))

                Nom_interlocuteur = TableauData(1)
                Fonction_interlocuteur = TableauData(2)
                Telephone_interlocuteur = TableauData(3)
                Intervenants = TableauData(4)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

