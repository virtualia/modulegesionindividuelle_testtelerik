﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class TAB_DESCRIPTION_GEST
        Inherits VIR_FICHE

        Private WsLstValeurs As List(Of TAB_LISTE_GEST)
        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule_GEST As String
        Private WsIntitule_Virtualia As String
        Private WsObservation As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "TAB_DESCRIPTION_GEST"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueGeneral
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 5
            End Get
        End Property

        Public Property Intitule_GEST() As String
            Get
                Return WsIntitule_GEST
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsIntitule_GEST = value
                    Case Else
                        WsIntitule_GEST = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Intitule_Virtualia() As String
            Get
                Return WsIntitule_Virtualia
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsIntitule_Virtualia = value
                    Case Else
                        WsIntitule_Virtualia = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Observation() As String
            Get
                Return WsObservation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservation = value
                    Case Else
                        WsObservation = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule_GEST & VI.Tild)
                Chaine.Append(Intitule_Virtualia & VI.Tild)
                Chaine.Append(Observation)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule_GEST = TableauData(1)
                Intitule_Virtualia = TableauData(2)
                Observation = TableauData(3)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Valeur(ByVal Fiche As TAB_LISTE_GEST) As Integer

            If WsLstValeurs Is Nothing Then
                WsLstValeurs = New List(Of TAB_LISTE_GEST)
            End If
            WsLstValeurs.Add(Fiche)
            Return WsLstValeurs.Count

        End Function

        Public Function ListedesValeurs() As List(Of TAB_LISTE_GEST)

            If WsLstValeurs Is Nothing Then
                Return Nothing
            End If
            Return WsLstValeurs.OrderBy(Function(m) m.Reference).ToList

        End Function

        Public Function ListedesValeurs(ByVal SiOk As Boolean) As List(Of TAB_LISTE_GEST)

            If WsLstValeurs Is Nothing Then
                Return Nothing
            End If
            Return (From Instance In WsLstValeurs Where Instance.SiActif = SiOk Order By Instance.Reference).ToList

        End Function

        Public Function FicheValeur_Ref(ByVal Code As String) As TAB_LISTE_GEST

            If WsLstValeurs Is Nothing Then
                Return Nothing
            End If
            Return (From Instance In WsLstValeurs Where Instance.Reference = Code And Instance.SiActif = True Select Instance).FirstOrDefault()

        End Function

        Public Function FicheValeur_V(ByVal IntituleVirtualia As String) As TAB_LISTE_GEST

            If WsLstValeurs Is Nothing Then
                Return Nothing
            End If
            Return (From Instance In WsLstValeurs Where Instance.Valeur_Virtualia = IntituleVirtualia And Instance.SiActif = True Select Instance).FirstOrDefault()

        End Function

        Public Function FicheValeur_V(ByVal IntituleVirtualia As String, ByVal Decret As String) As TAB_LISTE_GEST

            If WsLstValeurs Is Nothing Then
                Return Nothing
            End If
            Return (From Instance In WsLstValeurs Where Instance.Valeur_Virtualia = IntituleVirtualia And Instance.Decret = Decret And Instance.SiActif = True Select Instance).FirstOrDefault()

        End Function

        Public Function FicheValeur_G(ByVal Valeur As String) As TAB_LISTE_GEST

            If WsLstValeurs Is Nothing Then
                Return Nothing
            End If
            Return (From Instance In WsLstValeurs Where Instance.Valeur_GEST = Valeur And Instance.SiActif = True Select Instance).FirstOrDefault()

        End Function


    End Class
End Namespace
