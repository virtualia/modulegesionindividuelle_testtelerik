﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ORG_ANNUAIRE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsOrdrePresentation As Integer
        Private WsNomResponsable As String
        Private WsIdeResponsable As Integer
        Private WsMnemonique As String
        Private WsUrl As String
        Private WsDescriptif As String
        Private WsEmail As String
        Private WsDelegataire As String
        Private WsIdeDelegataire As Integer
        Private WsDateDebutDelegation As String
        Private WsDateFinDelegation As String
        Private WsGestionnaireEntite As String
        Private WsGestionnaireConge As String
        Private WsGestionnaireFormation As String


        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ORG_ANNUAIRE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueDirection
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property OrdredePresentation() As Integer
            Get
                Return WsOrdrePresentation
            End Get
            Set(ByVal value As Integer)
                WsOrdrePresentation = value
            End Set
        End Property

        Public Property NomduResponsable() As String
            Get
                Return WsNomResponsable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsNomResponsable = value
                    Case Else
                        WsNomResponsable = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property IdentifiantduResponsable() As Integer
            Get
                Return WsIdeResponsable
            End Get
            Set(ByVal value As Integer)
                WsIdeResponsable = value
            End Set
        End Property

        Public Property Mnemonique() As String
            Get
                Return WsMnemonique
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsMnemonique = value
                    Case Else
                        WsMnemonique = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property UrlEntite() As String
            Get
                Return WsUrl
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsUrl = value
                    Case Else
                        WsUrl = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsDescriptif = value
                    Case Else
                        WsDescriptif = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property EmailEntite() As String
            Get
                Return WsEmail
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsEmail = value
                    Case Else
                        WsEmail = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property NomduDelegataire() As String
            Get
                Return WsDelegataire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 70
                        WsDelegataire = value
                    Case Else
                        WsDelegataire = Strings.Left(value, 70)
                End Select
            End Set
        End Property

        Public Property IdentifiantduDelegataire() As Integer
            Get
                Return WsIdeDelegataire
            End Get
            Set(ByVal value As Integer)
                WsIdeDelegataire = value
            End Set
        End Property

        Public Property Date_de_Debut_Delegation() As String
            Get
                Return WsDateDebutDelegation
            End Get
            Set(ByVal value As String)
                WsDateDebutDelegation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Date_de_Fin_Delegation() As String
            Get
                Return WsDateFinDelegation
            End Get
            Set(ByVal value As String)
                WsDateFinDelegation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property GestionnaireEntite() As String
            Get
                Return WsGestionnaireEntite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsGestionnaireEntite = value
                    Case Else
                        WsGestionnaireEntite = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property GestionnaireConges() As String
            Get
                Return WsGestionnaireConge
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsGestionnaireConge = value
                    Case Else
                        WsGestionnaireConge = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property GestionnaireFormation() As String
            Get
                Return WsGestionnaireFormation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsGestionnaireFormation = value
                    Case Else
                        WsGestionnaireFormation = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(OrdredePresentation.ToString & VI.Tild)
                Chaine.Append(NomduResponsable & VI.Tild)
                Chaine.Append(IdentifiantduResponsable.ToString & VI.Tild)
                Chaine.Append(Mnemonique & VI.Tild)
                Chaine.Append(UrlEntite & VI.Tild)
                Chaine.Append(Descriptif & VI.Tild)
                Chaine.Append(EmailEntite & VI.Tild)
                Chaine.Append(NomduDelegataire & VI.Tild)
                Chaine.Append(IdentifiantduDelegataire.ToString & VI.Tild)
                Chaine.Append(Date_de_Debut_Delegation & VI.Tild)
                Chaine.Append(Date_de_Fin_Delegation & VI.Tild)
                Chaine.Append(GestionnaireEntite & VI.Tild)
                Chaine.Append(GestionnaireConges & VI.Tild)
                Chaine.Append(GestionnaireFormation)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 16 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                If TableauData(2) <> "" Then OrdredePresentation = CInt(TableauData(2))
                NomduResponsable = TableauData(3)
                If TableauData(4) <> "" Then IdentifiantduResponsable = CInt(TableauData(4))
                Mnemonique = TableauData(5)
                UrlEntite = TableauData(6)
                Descriptif = TableauData(7)
                EmailEntite = TableauData(8)
                NomduDelegataire = TableauData(9)
                If TableauData(10) <> "" Then IdentifiantduDelegataire = CInt(TableauData(10))
                Date_de_Debut_Delegation = TableauData(11)
                Date_de_Fin_Delegation = TableauData(12)
                GestionnaireEntite = TableauData(13)
                GestionnaireConges = TableauData(14)
                GestionnaireFormation = TableauData(15)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class

End Namespace

