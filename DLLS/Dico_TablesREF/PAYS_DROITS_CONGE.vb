﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAYS_DROITS_CONGE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsDroit_CA As Integer = 0
        Private WsDroit_Cumul As Integer = 0
        Private WsCommentaire As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAYS_DROITS_CONGE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePaie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 4
            End Get
        End Property

        Public Property Droit_CA() As Integer
            Get
                Return WsDroit_CA
            End Get
            Set(ByVal value As Integer)
                WsDroit_CA = value
            End Set
        End Property

        Public Property Droit_Cumul() As Integer
            Get
                Return WsDroit_Cumul
            End Get
            Set(ByVal value As Integer)
                WsDroit_Cumul = value
            End Set
        End Property

        Public Property Commentaire() As String
            Get
                Return WsCommentaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsCommentaire = value
                    Case Else
                        WsCommentaire = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Droit_CA.ToString & VI.Tild)
                Chaine.Append(Droit_Cumul.ToString & VI.Tild)
                Chaine.Append(Commentaire)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 5 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                If TableauData(2) <> "" Then Droit_CA = CInt(TableauData(2))
                If TableauData(3) <> "" Then Droit_Cumul = CInt(TableauData(3))
                Commentaire = TableauData(4)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

