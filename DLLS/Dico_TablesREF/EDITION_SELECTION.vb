﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class EDITION_SELECTION
        Inherits VIR_FICHE

        Private WsLstFilres As List(Of EDITION_FILTRE)
        Private WsFicheLue As StringBuilder
        '
        Private WsNumeroLigne As Integer
        Private WsNumObjet As Integer
        Private WsNumInfo As Integer
        Private WsAlias As String
        Private WsParametre As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "EDITION_SELECTION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueScriptEdition
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Numero_Ligne() As Integer
            Get
                Return WsNumeroLigne
            End Get
            Set(ByVal value As Integer)
                WsNumeroLigne = value
            End Set
        End Property

        Public Property Numero_ObjetEdition() As Integer
            Get
                Return WsNumObjet
            End Get
            Set(ByVal value As Integer)
                WsNumObjet = value
            End Set
        End Property

        Public Property Numero_InfoEdition() As Integer
            Get
                Return WsNumInfo
            End Get
            Set(ByVal value As Integer)
                WsNumInfo = value
            End Set
        End Property

        Public Property AliasColonne() As String
            Get
                Return WsAlias
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsAlias = value
                    Case Else
                        WsAlias = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Parametre() As String
            Get
                Return WsParametre
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsParametre = value
                    Case Else
                        WsParametre = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder
                Chaine.Append(VI.Tild)
                Chaine.Append(CStr(Numero_Ligne) & VI.Tild)
                Chaine.Append(CStr(Numero_ObjetEdition) & VI.Tild)
                Chaine.Append(CStr(Numero_InfoEdition) & VI.Tild)
                Chaine.Append(AliasColonne & VI.Tild)
                Chaine.Append(Parametre)
                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 6 Then
                    Exit Property
                End If

                For IndiceI = 0 To 3
                    If TableauData(IndiceI) = "" Then
                        TableauData(IndiceI) = "0"
                    End If
                Next IndiceI

                Ide_Dossier = CInt(TableauData(0))
                Numero_Ligne = CInt(TableauData(1))
                Numero_ObjetEdition = CInt(TableauData(2))
                Numero_InfoEdition = CInt(TableauData(3))
                AliasColonne = TableauData(4)
                Parametre = TableauData(5)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesFiltres() As List(Of EDITION_FILTRE)
            Get
                If WsLstFilres Is Nothing Then
                    Return Nothing
                End If
                Return WsLstFilres.OrderBy(Function(m) m.Numero_LigneIndice).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Filtre(ByVal Fiche As EDITION_FILTRE) As Integer
            If WsLstFilres Is Nothing Then
                WsLstFilres = New List(Of EDITION_FILTRE)
            End If
            WsLstFilres.Add(Fiche)
            Return WsLstFilres.Count

        End Function
    End Class
End Namespace
