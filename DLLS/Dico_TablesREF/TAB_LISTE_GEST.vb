﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class TAB_LISTE_GEST
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsValeur_GEST As String
        Private WsReference As String
        Private WsValeurVirtualia As String
        Private WsDecret As String
        Private WsZActif As String
        Private WsSiActif As Boolean = True
        '
        Private WsLstDico As List(Of Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel)

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "TAB_LISTE_GEST"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueGeneral
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 6
            End Get
        End Property

        Public Property Reference() As String
            Get
                Return WsReference
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 30
                        WsReference = value
                    Case Else
                        WsReference = Strings.Left(value, 30)
                End Select
            End Set
        End Property

        Public Property Valeur_GEST() As String
            Get
                Return WsValeur_GEST
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 300
                        WsValeur_GEST = value
                    Case Else
                        WsValeur_GEST = Strings.Left(value, 300)
                End Select
            End Set
        End Property

        Public Property Valeur_Virtualia() As String
            Get
                Return WsValeurVirtualia
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsValeurVirtualia = value
                    Case Else
                        WsValeurVirtualia = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Decret() As String
            Get
                Return WsDecret
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsDecret = value
                    Case Else
                        WsDecret = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Private Property ZActif() As String
            Get
                Select Case WsSiActif
                    Case False
                        Return "N"
                    Case Else
                        Return "O"
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 1
                        WsZActif = value
                    Case Else
                        WsZActif = Strings.Left(value, 1)
                End Select
                If WsZActif = "N" Then
                    SiActif = False
                Else
                    SiActif = True
                End If
            End Set
        End Property

        Public Property SiActif() As Boolean
            Get
                Return WsSiActif
            End Get
            Set(ByVal value As Boolean)
                WsSiActif = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Reference & VI.Tild)
                Chaine.Append(Valeur_GEST & VI.Tild)
                Chaine.Append(Valeur_Virtualia & VI.Tild)
                Chaine.Append(Decret & VI.Tild)
                Chaine.Append(ZActif)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 6 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Reference = TableauData(1)
                Valeur_GEST = TableauData(2)
                Valeur_Virtualia = TableauData(3)
                Decret = TableauData(4)
                ZActif = TableauData(5)

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListeDico As List(Of Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel)
            Get
                If WsLstDico Is Nothing Then
                    Call FaireDictionnaire()
                End If
                Return WsLstDico
            End Get
        End Property

        Public ReadOnly Property V_DonneeGrid As String
            Get
                If WsLstDico Is Nothing Then
                    FaireDictionnaire()
                End If
                Dim Chaine As New StringBuilder
                Chaine.Append(Reference & VI.Tild)
                Chaine.Append(Valeur_GEST & VI.Tild)
                Chaine.Append(Valeur_Virtualia & VI.Tild)
                Chaine.Append(Decret & VI.Tild)
                Chaine.Append(ZActif & VI.Tild)
                Chaine.Append(Reference)
                Return Chaine.ToString
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Private Sub FaireDictionnaire()
            Dim InfoBase As Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel

            WsLstDico = New List(Of Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Code DGFIP"
            InfoBase.Longueur = 30
            InfoBase.NatureDonnee = "Alpha"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            WsLstDico.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Valeur DGFIP"
            InfoBase.Longueur = 300
            InfoBase.NatureDonnee = "Alpha"
            InfoBase.FormatDonnee = "TelleQue"
            InfoBase.IdentifiantTable = 0
            WsLstDico.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Valeur Virtualia"
            InfoBase.Longueur = 120
            InfoBase.NatureDonnee = "Alpha"
            InfoBase.FormatDonnee = "TelleQue"
            InfoBase.IdentifiantTable = 0
            WsLstDico.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Texte règlementaire"
            InfoBase.Longueur = 250
            InfoBase.NatureDonnee = "Alpha"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            WsLstDico.Add(InfoBase)

            InfoBase = New Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel
            InfoBase.Etiquette = "Indicateur Code Actif (O/N)"
            InfoBase.Longueur = 1
            InfoBase.NatureDonnee = "Alpha"
            InfoBase.FormatDonnee = ""
            InfoBase.IdentifiantTable = 0
            WsLstDico.Add(InfoBase)
        End Sub

    End Class
End Namespace
