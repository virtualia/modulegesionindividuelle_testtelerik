Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class POS_POSITION
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsPrise_en_compte_selection As String
        Private WsInterruption_de_carriere As String
        Private WsPourcentage_d_interruption As Double = 0
        Private WsInterruption_retraite As String
        Private WsVacance_poste_budgetaire As String
        Private WsVacance_fonction_exercee As String
        Private WsNotion_d_activite As String
        Private WsDateouverture As String
        Private WsDatefermeture As String
        Private WsReference As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "POS_POSITION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePosition
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Prise_en_compte_selection() As String
            Get
                Select Case WsPrise_en_compte_selection
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsPrise_en_compte_selection
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsPrise_en_compte_selection = value
                    Case Else
                        WsPrise_en_compte_selection = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Interruption_de_carriere() As String
            Get
                Select Case WsInterruption_de_carriere
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsInterruption_de_carriere
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsInterruption_de_carriere = value
                    Case Else
                        WsInterruption_de_carriere = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Pourcentage_d_interruption() As Double
            Get
                Return WsPourcentage_d_interruption
            End Get
            Set(ByVal value As Double)
                WsPourcentage_d_interruption = value
            End Set
        End Property

        Public Property Interruption_retraite() As String
            Get
                Select Case WsInterruption_retraite
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsInterruption_retraite
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsInterruption_retraite = value
                    Case Else
                        WsInterruption_retraite = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Vacance_poste_budgetaire() As String
            Get
                Select Case WsVacance_poste_budgetaire
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsVacance_poste_budgetaire
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsVacance_poste_budgetaire = value
                    Case Else
                        WsVacance_poste_budgetaire = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public ReadOnly Property SiAdeduire_ETPFinancier As Boolean
            Get
                Select Case Vacance_poste_budgetaire
                    Case Is = "Non"
                        Return False
                    Case Else
                        Return True
                End Select
            End Get
        End Property

        Public Property Vacance_fonction_exercee() As String
            Get
                Select Case WsVacance_fonction_exercee
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsVacance_fonction_exercee
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsVacance_fonction_exercee = value
                    Case Else
                        WsVacance_fonction_exercee = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public ReadOnly Property SiAdeduire_ETPActivite As Boolean
            Get
                Select Case WsVacance_fonction_exercee
                    Case Is = "Non"
                        Return False
                    Case Else
                        Return True
                End Select
            End Get
        End Property

        Public Property Notion_d_activite() As String
            Get
                Select Case WsNotion_d_activite
                    Case Is = ""
                        Return "Non"
                    Case Else
                        Return WsNotion_d_activite
                End Select
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsNotion_d_activite = value
                    Case Else
                        WsNotion_d_activite = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Dateouverture() As String
            Get
                Return WsDateouverture
            End Get
            Set(ByVal value As String)
                WsDateouverture = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Datefermeture() As String
            Get
                Return WsDatefermeture
            End Get
            Set(ByVal value As String)
                WsDatefermeture = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Reference() As String
            Get
                Return WsReference
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsReference = value
                    Case Else
                        WsReference = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Prise_en_compte_selection & VI.Tild)
                Chaine.Append(Interruption_de_carriere & VI.Tild)
                Chaine.Append(VirRhFonction.VirgulePoint(Strings.Format(Pourcentage_d_interruption, "0.00")) & VI.Tild)
                Chaine.Append(Interruption_retraite & VI.Tild)
                Chaine.Append(Vacance_poste_budgetaire & VI.Tild)
                Chaine.Append(Vacance_fonction_exercee & VI.Tild)
                Chaine.Append(Notion_d_activite & VI.Tild)
                Chaine.Append(Dateouverture & VI.Tild)
                Chaine.Append(Datefermeture & VI.Tild)
                Chaine.Append(Reference)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                Prise_en_compte_selection = TableauData(2)
                Interruption_de_carriere = TableauData(3)
                If TableauData(4) <> "" Then Pourcentage_d_interruption = VirRhFonction.ConversionDouble(TableauData(4))
                Interruption_retraite = TableauData(5)
                Vacance_poste_budgetaire = TableauData(6)
                Vacance_fonction_exercee = TableauData(7)
                Notion_d_activite = TableauData(8)
                Dateouverture = TableauData(9)
                Datefermeture = TableauData(10)
                If TableauData.Count > 11 Then
                    Reference = TableauData(11)
                End If

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
