﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class REG_FORMULE
        Inherits VIR_FICHE
        Implements ICloneable

        Private WsFicheLue As StringBuilder

        Private WsOperateur As String = "ET"
        Private WsListe_Critere As List(Of REG_CRITERE) = New List(Of REG_CRITERE)()

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueRegles
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 5
            End Get
        End Property

        Public Property Operateur() As String
            Get
                Return WsOperateur
            End Get
            Set(ByVal value As String)
                WsOperateur = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Operateur)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)

                Ide_Dossier = CInt(TableauData(0))

                Operateur = TableauData(1)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To UBound(TableauData) - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Overrides ReadOnly Property V_Regle(ByVal INumInfo As Integer) As String
            Get
                Select Case INumInfo
                    Case 0
                        Return ""
                    Case 1
                        Return WsOperateur
                    Case 99
                        Return Ide_Dossier.ToString
                    Case Else
                        Return ""
                End Select
            End Get

        End Property

        ''' <summary>
        ''' Fournit une liste uniquement constituée des fiches REG_CRITERE triée par Objet
        ''' </summary>
        Public ReadOnly Property Liste_REG_CRITERE() As List(Of REG_CRITERE)
            Get
                Return (From instance In WsListe_Critere Select instance Order By instance.Objet Ascending).ToList
            End Get
        End Property

        ''' <summary>
        ''' Fournit une liste uniquement constituée des fiches REG_CRITERE correspondant au numéro d'objet demandé
        ''' </summary>
        Public ReadOnly Property Liste_REG_CRITERE(ByVal INumObjet As Integer) As List(Of REG_CRITERE)
            Get
                Return WsListe_Critere.Where(Function(m) m.Objet = INumObjet).ToList
            End Get
        End Property

        Public Overrides Property Inum_objet As Integer
            Get
                Return Me.NumeroObjet
            End Get
            Set(value As Integer)
                MyBase.INum_Objet = value
            End Set
        End Property

        ''' <summary>
        ''' Donne l'Ide Critere Max
        ''' </summary>
        Public ReadOnly Property MaxIde_Critere As Integer
            Get

                If (WsListe_Critere.Count <= 0) Then
                    Return 0
                End If

                Return (From it As REG_CRITERE In WsListe_Critere Select it.Ide_Critere).Max()

                'Dim Ide_Critere_Max As Integer = 0
                'Dim I As Integer

                'For I = 0 To WsListe_Critere.Count - 1
                '    If WsListe_Critere(I).Ide_Critere > Ide_Critere_Max Then
                '        Ide_Critere_Max = WsListe_Critere(I).Ide_Critere
                '    End If
                'Next I

                'Return Ide_Critere_Max
            End Get
        End Property

        ''' <summary>
        ''' Donne la liste des objets concernés par la formule
        ''' </summary>
        Public ReadOnly Property ListeObjets_Critere() As List(Of Integer)
            Get
                Dim Objet_1 As Integer = 0
                Dim Objet_2 As Integer = 0
                Dim Fiche_REG_CRITERE As REG_CRITERE
                Dim I As Integer

                If Liste_REG_CRITERE.Count > 0 Then
                    For I = 0 To Liste_REG_CRITERE.Count - 1
                        Fiche_REG_CRITERE = WsListe_Critere(I)

                        If Fiche_REG_CRITERE.Objet <> 0 Then
                            If Objet_1 = 0 Then
                                Objet_1 = Fiche_REG_CRITERE.Objet
                            Else
                                If Objet_1 <> Fiche_REG_CRITERE.Objet Then
                                    If Objet_2 = 0 Then
                                        Objet_2 = Fiche_REG_CRITERE.Objet
                                    End If
                                End If
                            End If
                        End If

                    Next I
                End If
                Dim LstObjets As New List(Of Integer)

                If Objet_1 <> 0 Then
                    LstObjets.Add(Objet_1)
                End If
                If Objet_2 <> 0 Then
                    LstObjets.Add(Objet_2)
                End If

                Return LstObjets
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub
        Public Sub New(ByVal Contenu As String)
            MyBase.New(Contenu)
        End Sub

        Public Overridable Function Clone() As Object Implements ICloneable.Clone

            Dim CloneREG_FORMULE As REG_FORMULE = New REG_FORMULE(Ide_Dossier.ToString & ContenuTable)

            For Each rc As REG_CRITERE In WsListe_Critere
                CloneREG_FORMULE.WsListe_Critere.Add(rc.Clone())
            Next

            Return CloneREG_FORMULE

        End Function

        ''' <summary>
        ''' Suppression d'une fiche REG_CRITERE dans la liste des critères
        ''' </summary>
        Public Sub RemoveAt_REG_CRITERE(ByVal Indice As Integer)

            If (Indice < 0 Or Indice >= WsListe_Critere.Count) Then
                Return
            End If

            Me.WsListe_Critere.RemoveAt(Indice)
        End Sub

        ''' <summary>
        ''' Ajout d'une fiche REG_CRITERE
        ''' </summary>
        Public Sub Add_REG_CRITERE(ByRef Reg_Critere As REG_CRITERE)
            WsListe_Critere.Add(Reg_Critere)
        End Sub

        ''' <summary>
        ''' Ajout d'une fiche REG_CRITERE
        ''' </summary>
        Public Sub Add_New_REG_CRITERE(ByRef Reg_Critere As REG_CRITERE)

            Dim Ide_Critere_Max As Integer = 0
            Dim REG_VALEUR As REG_VALEUR
            Dim I As Integer

            Ide_Critere_Max = MaxIde_Critere
            Reg_Critere.Ide_Critere = Ide_Critere_Max + 1

            For I = 0 To Reg_Critere.Liste_REG_VALEUR.Count - 1
                REG_VALEUR = Reg_Critere.Liste_REG_VALEUR(I)
                REG_VALEUR.Ide_Critere = Reg_Critere.Ide_Critere
            Next I

            Me.WsListe_Critere.Add(Reg_Critere)

        End Sub

        ''' <summary>
        ''' Remplace le critère précédent
        ''' </summary>
        Public Function Replace_Critere(ByRef Reg_Critere As REG_CRITERE) As Boolean
            Dim I As Integer
            For I = 0 To WsListe_Critere.Count - 1
                If WsListe_Critere(I).Ide_Critere = Reg_Critere.Ide_Critere Then
                    WsListe_Critere(I) = Reg_Critere
                    Return True
                End If
            Next I
            Return False
        End Function

    End Class
End Namespace