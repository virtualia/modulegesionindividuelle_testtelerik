﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele
Namespace ShemaREF
    Public Class TAB_DESCRIPTION
        Inherits VIR_FICHE
        Private WsLstValeurs As List(Of TAB_LISTE)
        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsSiModifiable As String
        Private WsLongueur As Integer

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "TAB_DESCRIPTION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueGeneral
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property SiModifiable() As String
            Get
                Return WsSiModifiable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiModifiable = value
                    Case Else
                        WsSiModifiable = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Longueur() As Integer
            Get
                Return WsLongueur
            End Get
            Set(ByVal value As Integer)
                WsLongueur = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(SiModifiable & VI.Tild)
                Chaine.Append(Longueur.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 4 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                SiModifiable = TableauData(2)
                If TableauData(3) = "" Then
                    TableauData(3) = "0"
                End If
                Longueur = CInt(TableauData(3))

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Valeur(ByVal Fiche As TAB_LISTE) As Integer

            If WsLstValeurs Is Nothing Then
                WsLstValeurs = New List(Of TAB_LISTE)
            End If
            WsLstValeurs.Add(Fiche)
            Return WsLstValeurs.Count

        End Function

        Public Function ListedesValeurs() As List(Of TAB_LISTE)

            If WsLstValeurs Is Nothing Then
                Return Nothing
            End If
            Return WsLstValeurs.OrderBy(Function(m) m.Valeur).ToList

        End Function

        Public Function ListedesValeurs(ByVal Ide As Integer) As List(Of TAB_LISTE)

            If WsLstValeurs Is Nothing Then
                Return Nothing
            End If

            Return (From Instance As TAB_LISTE In WsLstValeurs Where Instance.Ide_Dossier = Ide Order By Instance.Valeur).ToList()

        End Function

    End Class
End Namespace
