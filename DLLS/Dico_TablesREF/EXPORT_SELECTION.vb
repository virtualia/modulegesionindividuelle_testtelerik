﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class EXPORT_SELECTION
        Inherits VIR_FICHE

        Private WsLstFilres As List(Of EXPORT_FILTRE)
        Private WsFicheLue As StringBuilder
        '
        Private WsNumeroLigne As Integer
        Private WsNumObjet As Integer
        Private WsNumInfo As Integer
        Private WsAlias As String
        Private WsTableTranscodification As Integer
        Private WsLgInfo As Integer
        Private WsFormatInfo As Integer
        Private WsPeriode As String
        Private WsNbColonnes As Integer
        Private WsParametre As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "EXPORT_SELECTION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueScriptExport
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 2
            End Get
        End Property

        Public Property Numero_Ligne() As Integer
            Get
                Return WsNumeroLigne
            End Get
            Set(ByVal value As Integer)
                WsNumeroLigne = value
            End Set
        End Property

        Public Property Numero_ObjetExtrait() As Integer
            Get
                Return WsNumObjet
            End Get
            Set(ByVal value As Integer)
                WsNumObjet = value
            End Set
        End Property

        Public Property Numero_InfoExtraite() As Integer
            Get
                Return WsNumInfo
            End Get
            Set(ByVal value As Integer)
                WsNumInfo = value
            End Set
        End Property

        Public Property AliasColonne() As String
            Get
                Return WsAlias
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsAlias = value
                    Case Else
                        WsAlias = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property TabledeTranscodification() As Integer
            Get
                Return WsTableTranscodification
            End Get
            Set(ByVal value As Integer)
                WsTableTranscodification = value
            End Set
        End Property

        Public Property LongueurDonnee() As Integer
            Get
                Return WsLgInfo
            End Get
            Set(ByVal value As Integer)
                WsLgInfo = value
            End Set
        End Property

        Public Property FormatDonnee() As Integer
            Get
                Return WsFormatInfo
            End Get
            Set(ByVal value As Integer)
                WsFormatInfo = value
            End Set
        End Property

        Public Property Periode() As String
            Get
                Return WsPeriode
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsPeriode = value
                    Case Else
                        WsPeriode = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property NombredeColonnes() As Integer
            Get
                Return WsNbColonnes
            End Get
            Set(ByVal value As Integer)
                WsNbColonnes = value
            End Set
        End Property

        Public Property Parametre() As String
            Get
                Return WsParametre
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsParametre = value
                    Case Else
                        WsParametre = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(CStr(Numero_Ligne) & VI.Tild)
                Chaine.Append(CStr(Numero_ObjetExtrait) & VI.Tild)
                Chaine.Append(CStr(Numero_InfoExtraite) & VI.Tild)
                Chaine.Append(AliasColonne & VI.Tild)
                Chaine.Append(CStr(TabledeTranscodification) & VI.Tild)
                Chaine.Append(CStr(LongueurDonnee) & VI.Tild)
                Chaine.Append(CStr(FormatDonnee) & VI.Tild)
                Chaine.Append(Periode & VI.Tild)
                Chaine.Append(CStr(NombredeColonnes) & VI.Tild)
                Chaine.Append(Parametre)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                For IndiceI = 0 To 3
                    If TableauData(IndiceI) = "" Then
                        TableauData(IndiceI) = "0"
                    End If
                Next IndiceI

                Ide_Dossier = CInt(TableauData(0))
                Numero_Ligne = CInt(TableauData(1))
                Numero_ObjetExtrait = CInt(TableauData(2))
                Numero_InfoExtraite = CInt(TableauData(3))
                AliasColonne = TableauData(4)
                For IndiceI = 5 To 7
                    If TableauData(IndiceI) = "" Then
                        TableauData(IndiceI) = "0"
                    End If
                Next IndiceI
                TabledeTranscodification = CInt(TableauData(5))
                LongueurDonnee = CInt(TableauData(6))
                FormatDonnee = CInt(TableauData(7))
                Periode = TableauData(8)
                If TableauData(9) = "" Then TableauData(9) = "0"
                NombredeColonnes = CInt(TableauData(9))
                Parametre = TableauData(10)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property ListedesFiltres() As List(Of EXPORT_FILTRE)
            Get
                If WsLstFilres Is Nothing Then
                    Return Nothing
                End If
                Return WsLstFilres.OrderBy(Function(m) m.Numero_LigneIndice).ToList
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Filtre(ByVal Fiche As EXPORT_FILTRE) As Integer
            If WsLstFilres Is Nothing Then
                WsLstFilres = New List(Of EXPORT_FILTRE)
            End If
            WsLstFilres.Add(Fiche)
            Return WsLstFilres.Count
        End Function

    End Class
End Namespace
