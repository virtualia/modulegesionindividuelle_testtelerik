﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class FCT_SUPPORT
        Inherits VIR_FICHE
        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsDefinition As String
        Private WsTaches As String
        Private WsEtablissement As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "FCT_SUPPORT"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueFonctionSupport
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Definition() As String
            Get
                Return WsDefinition
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 800
                        WsDefinition = value
                    Case Else
                        WsDefinition = Strings.Left(value, 800)
                End Select
            End Set
        End Property

        Public Property Taches() As String
            Get
                Return WsTaches
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 800
                        WsTaches = value
                    Case Else
                        WsTaches = Strings.Left(value, 800)
                End Select
            End Set
        End Property

        Public Property Etablissement() As String
            Get
                Return WsEtablissement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsEtablissement = value
                    Case Else
                        WsEtablissement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Definition & VI.Tild)
                Chaine.Append(Taches & VI.Tild)
                Chaine.Append(Etablissement)


                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 5 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                Definition = TableauData(2)
                Taches = TableauData(3)
                If TableauData(4) <> "" Then
                    Etablissement = TableauData(4)
                Else
                    Etablissement = "Commun"
                End If

                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace
