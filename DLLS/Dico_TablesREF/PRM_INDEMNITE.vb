﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PRM_INDEMNITE
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsModecalcul As Integer
        Private WsRegimeindemnitaire As String
        Private WsPeriodicite As String
        Private WsParametrecalcul_no1 As String
        Private WsParametrecalcul_no2 As String
        Private WsParametrecalcul_no3 As String
        Private WsParametrecalcul_no4 As String
        Private WsParametrecalcul_no5 As String
        Private WsFiliere As String
        Private WsReference As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PRM_INDEMNITE"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePrimes
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Modecalcul() As Integer
            Get
                Return WsModecalcul
            End Get
            Set(ByVal value As Integer)
                WsModecalcul = value
            End Set
        End Property

        Public Property Regimeindemnitaire() As String
            Get
                Return WsRegimeindemnitaire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 100
                        WsRegimeindemnitaire = value
                    Case Else
                        WsRegimeindemnitaire = Strings.Left(value, 100)
                End Select
            End Set
        End Property

        Public Property Periodicite() As String
            Get
                Return WsPeriodicite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsPeriodicite = value
                    Case Else
                        WsPeriodicite = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Parametrecalcul_no1() As String
            Get
                Return WsParametrecalcul_no1
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsParametrecalcul_no1 = value
                    Case Else
                        WsParametrecalcul_no1 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Parametrecalcul_no2() As String
            Get
                Return WsParametrecalcul_no2
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsParametrecalcul_no2 = value
                    Case Else
                        WsParametrecalcul_no2 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Parametrecalcul_no3() As String
            Get
                Return WsParametrecalcul_no3
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsParametrecalcul_no3 = value
                    Case Else
                        WsParametrecalcul_no3 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Parametrecalcul_no4() As String
            Get
                Return WsParametrecalcul_no4
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsParametrecalcul_no4 = value
                    Case Else
                        WsParametrecalcul_no4 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Parametrecalcul_no5() As String
            Get
                Return WsParametrecalcul_no5
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsParametrecalcul_no5 = value
                    Case Else
                        WsParametrecalcul_no5 = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Filiere() As String
            Get
                Return WsFiliere
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsFiliere = value
                    Case Else
                        WsFiliere = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Reference() As String
            Get
                Return WsReference
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsReference = value
                    Case Else
                        WsReference = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Modecalcul.ToString & VI.Tild)
                Chaine.Append(Regimeindemnitaire & VI.Tild)
                Chaine.Append(Periodicite & VI.Tild)
                Chaine.Append(Parametrecalcul_no1 & VI.Tild)
                Chaine.Append(Parametrecalcul_no2 & VI.Tild)
                Chaine.Append(Parametrecalcul_no3 & VI.Tild)
                Chaine.Append(Parametrecalcul_no4 & VI.Tild)
                Chaine.Append(Parametrecalcul_no5 & VI.Tild)
                Chaine.Append(Filiere & VI.Tild)
                Chaine.Append(Reference)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 11 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))

                Intitule = TableauData(1)
                If TableauData(2) <> "" Then Modecalcul = CInt(TableauData(2))
                Regimeindemnitaire = TableauData(3)
                Periodicite = TableauData(4)
                Parametrecalcul_no1 = TableauData(5)
                Parametrecalcul_no2 = TableauData(6)
                Parametrecalcul_no3 = TableauData(7)
                Parametrecalcul_no4 = TableauData(8)
                Parametrecalcul_no5 = TableauData(9)
                Filiere = TableauData(10)
                If TableauData.Count > 11 Then
                    Reference = TableauData(11)
                End If

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

