﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class LOLF_ACTION
        Inherits VIR_FICHE

        Private WsLstAutorisations As List(Of LOLF_AUTORISATION_EMPLOI)
        Private WsLstTrieeAutorisations As New List(Of LOLF_AUTORISATION_EMPLOI)
        Private WsFicheLue As StringBuilder
        '
        Private WsNumeroPgm As Integer = 0
        Private WsNumeroAction As String
        Private WsIntitule As String
        Private WsDateCreation As String
        Private WsDescriptif As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "LOLF_ACTION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueLolf
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 3
            End Get
        End Property

        Public Property NumeroduProgramme() As Integer
            Get
                Return WsNumeroPgm
            End Get
            Set(ByVal value As Integer)
                WsNumeroPgm = value
            End Set
        End Property

        Public Property NumerodelAction() As String
            Get
                Return WsNumeroAction
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 7
                        WsNumeroAction = value
                    Case Else
                        WsNumeroAction = Strings.Left(value, 7)
                End Select
            End Set
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property DatedeCreation() As String
            Get
                Return WsDateCreation
            End Get
            Set(ByVal value As String)
                WsDateCreation = VirRhDate.DateStandardVirtualia(value)
            End Set
        End Property

        Public Property Descriptif() As String
            Get
                Return WsDescriptif
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsDescriptif = value
                    Case Else
                        WsDescriptif = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(NumeroduProgramme.ToString & VI.Tild)
                Chaine.Append(NumerodelAction & VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(DatedeCreation & VI.Tild)
                Chaine.Append(Descriptif)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 6 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                If TableauData(1) <> "" Then NumeroduProgramme = CInt(TableauData(1))
                NumerodelAction = TableauData(2)
                Intitule = TableauData(3)
                DatedeCreation = TableauData(4)
                Descriptif = TableauData(5)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Autorisation(ByVal FicheAE As LOLF_AUTORISATION_EMPLOI) As Integer

            If WsLstAutorisations Is Nothing Then
                WsLstAutorisations = New List(Of LOLF_AUTORISATION_EMPLOI)
            End If
            WsLstAutorisations.Add(FicheAE)
            Return WsLstAutorisations.Count

        End Function

        Public Function ListedesAutorisations(ByVal CritereTri As String) As List(Of LOLF_AUTORISATION_EMPLOI)

            If WsLstAutorisations Is Nothing Then
                Return Nothing
            End If

            Select Case CritereTri
                Case Is = "Intitulé"
                    Return WsLstAutorisations.OrderBy(Function(m) m.Nombre).ToList
                Case Is = "Numero"
                    WsLstTrieeAutorisations = (From instance In WsLstAutorisations Select instance Order By instance.NumeroduProgramme Ascending,
                                instance.NumerodelAction Ascending, instance.NumerodelAutorisation Ascending).ToList
                    Return WsLstTrieeAutorisations
                Case Else
                    Return WsLstAutorisations
            End Select

        End Function


    End Class
End Namespace

