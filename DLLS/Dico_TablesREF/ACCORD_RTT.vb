﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ACCORD_RTT
        Inherits VIR_FICHE

        Private WsNumPVue As Integer
        Private WsNumObjet As Integer
        Private WsFicheLue As StringBuilder
        '
        Private WsDureeHebdoMoyenne As String
        Private WsDureeHebdoMaxi As String
        Private WsDureeHebdo12Semaines As String
        Private WsDureeJourMini As String
        Private WsDureeJourMaxi As String
        Private WsAmplitudeJourMaxi As String
        Private WsDureeReposMini As String
        Private WsHeureDebutNuit As String
        Private WsHeureFinNuit As String
        Private WsHeuresNuitMinimum As String
        Private WsDureePauseDejeuner As String
        Private WsDureeMiniPauseDejeuner As String
        Private WsDureeMaxiPauseDejeuner As String
        Private WsHeureDebutPauseDejeuner As String
        Private WsHeureFinPauseDejeuner As String
        Private WsSiHoraireVariable As String
        Private WsHeureDebutPlageVariable As String
        Private WsHeureDebutPlageFixe As String
        Private WsHeureFinPlageVariable As String
        Private WsHeureFinPlageFixe As String
        Private WsSiPasdeRecuperation As String
        Private WsSiRAZduDC As String
        '
        Private TsDate_de_fin As String

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ACCORD_RTT"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return WsNumPVue
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return WsNumObjet
            End Get
        End Property

        Public Property Duree_Hebdo_Moyenne() As String
            Get
                Return WsDureeHebdoMoyenne
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsDureeHebdoMoyenne = value
                    Case Else
                        WsDureeHebdoMoyenne = Strings.Left(value, 9)
                End Select
                MyBase.Clef = WsDureeHebdoMoyenne
            End Set
        End Property

        Public Property Duree_Hebdo_Maximale() As String
            Get
                Return WsDureeHebdoMaxi
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsDureeHebdoMaxi = value
                    Case Else
                        WsDureeHebdoMaxi = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Duree_Hebdo_Sur_12Semaines() As String
            Get
                Return WsDureeHebdo12Semaines
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsDureeHebdo12Semaines = value
                    Case Else
                        WsDureeHebdo12Semaines = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Duree_Journee_Minimale() As String
            Get
                Return WsDureeJourMini
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsDureeJourMini = value
                    Case Else
                        WsDureeJourMini = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Duree_Journee_Maximale() As String
            Get
                Return WsDureeJourMaxi
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsDureeJourMaxi = value
                    Case Else
                        WsDureeJourMaxi = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Amplitude_Journee_Maxi() As String
            Get
                Return WsAmplitudeJourMaxi
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsAmplitudeJourMaxi = value
                    Case Else
                        WsAmplitudeJourMaxi = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Duree_Repos_Minimal() As String
            Get
                Return WsDureeReposMini
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsDureeReposMini = value
                    Case Else
                        WsDureeReposMini = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Heure_Debut_Nuit() As String
            Get
                Return WsHeureDebutNuit
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureDebutNuit = value
                    Case Else
                        WsHeureDebutNuit = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Heure_Fin_Nuit() As String
            Get
                Return WsHeureFinNuit
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureFinNuit = value
                    Case Else
                        WsHeureFinNuit = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Heures_de_Nuit_Minimum() As String
            Get
                Return WsHeuresNuitMinimum
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsHeuresNuitMinimum = value
                    Case Else
                        WsHeuresNuitMinimum = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Duree_PauseDejeuner() As String
            Get
                Return WsDureePauseDejeuner
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsDureePauseDejeuner = value
                    Case Else
                        WsDureePauseDejeuner = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Duree_PauseDejeuner_Minimum() As String
            Get
                Return WsDureeMiniPauseDejeuner
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsDureeMiniPauseDejeuner = value
                    Case Else
                        WsDureeMiniPauseDejeuner = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Duree_PauseDejeuner_Maximum() As String
            Get
                Return WsDureeMaxiPauseDejeuner
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 9
                        WsDureeMaxiPauseDejeuner = value
                    Case Else
                        WsDureeMaxiPauseDejeuner = Strings.Left(value, 9)
                End Select
            End Set
        End Property

        Public Property Heure_Debut_PauseDejeuner() As String
            Get
                Return WsHeureDebutPauseDejeuner
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureDebutPauseDejeuner = value
                    Case Else
                        WsHeureDebutPauseDejeuner = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Heure_Fin_PauseDejeuner() As String
            Get
                Return WsHeureFinPauseDejeuner
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureFinPauseDejeuner = value
                    Case Else
                        WsHeureFinPauseDejeuner = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property SiHoraireVariable() As String
            Get
                Return WsSiHoraireVariable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiHoraireVariable = value
                    Case Else
                        WsSiHoraireVariable = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property Heure_Debut_PlageVariable() As String
            Get
                Return WsHeureDebutPlageVariable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureDebutPlageVariable = value
                    Case Else
                        WsHeureDebutPlageVariable = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Heure_Debut_PlageFixe() As String
            Get
                Return WsHeureDebutPlageFixe
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureDebutPlageFixe = value
                    Case Else
                        WsHeureDebutPlageFixe = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Heure_Fin_PlageVariable() As String
            Get
                Return WsHeureFinPlageVariable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureFinPlageVariable = value
                    Case Else
                        WsHeureFinPlageVariable = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property Heure_Fin_PlageFixe() As String
            Get
                Return WsHeureFinPlageFixe
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 5
                        WsHeureFinPlageFixe = value
                    Case Else
                        WsHeureFinPlageFixe = Strings.Left(value, 5)
                End Select
            End Set
        End Property

        Public Property SiPasdeCredit_Heures() As String
            Get
                Return WsSiPasdeRecuperation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiPasdeRecuperation = value
                    Case Else
                        WsSiPasdeRecuperation = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Property SiRAZ_CompteurDC() As String
            Get
                Return WsSiRAZduDC
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 3
                        WsSiRAZduDC = value
                    Case Else
                        WsSiRAZduDC = Strings.Left(value, 3)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(MyBase.Date_de_Valeur & VI.Tild)
                Chaine.Append(Duree_Hebdo_Moyenne & VI.Tild)
                Chaine.Append(Duree_Hebdo_Maximale & VI.Tild)
                Chaine.Append(Duree_Hebdo_Sur_12Semaines & VI.Tild)
                Chaine.Append(Duree_Journee_Minimale & VI.Tild)
                Chaine.Append(Duree_Journee_Maximale & VI.Tild)
                Chaine.Append(Amplitude_Journee_Maxi & VI.Tild)
                Chaine.Append(Duree_Repos_Minimal & VI.Tild)
                Chaine.Append(Heure_Debut_Nuit & VI.Tild)
                Chaine.Append(Heure_Fin_Nuit & VI.Tild)
                Chaine.Append(Heures_de_Nuit_Minimum & VI.Tild)
                Chaine.Append(Duree_PauseDejeuner & VI.Tild)
                Chaine.Append(Duree_PauseDejeuner_Minimum & VI.Tild)
                Chaine.Append(Duree_PauseDejeuner_Maximum & VI.Tild)
                Chaine.Append(Heure_Debut_PauseDejeuner & VI.Tild)
                Chaine.Append(Heure_Fin_PauseDejeuner & VI.Tild)
                Chaine.Append(SiHoraireVariable & VI.Tild)
                Chaine.Append(Heure_Debut_PlageVariable & VI.Tild)
                Chaine.Append(Heure_Debut_PlageFixe & VI.Tild)
                Chaine.Append(Heure_Fin_PlageVariable & VI.Tild)
                Chaine.Append(Heure_Fin_PlageFixe & VI.Tild)
                Chaine.Append(SiPasdeCredit_Heures & VI.Tild)
                Chaine.Append(SiRAZ_CompteurDC)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 22 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                MyBase.Date_de_Valeur = TableauData(1)
                Duree_Hebdo_Moyenne = TableauData(2)
                Duree_Hebdo_Maximale = TableauData(3)
                Duree_Hebdo_Sur_12Semaines = TableauData(4)
                Duree_Journee_Minimale = TableauData(5)
                Duree_Journee_Maximale = TableauData(6)
                Amplitude_Journee_Maxi = TableauData(7)
                Duree_Repos_Minimal = TableauData(8)
                Heure_Debut_Nuit = TableauData(9)
                Heure_Fin_Nuit = TableauData(10)
                Heures_de_Nuit_Minimum = TableauData(11)
                Duree_PauseDejeuner = TableauData(12)
                Duree_PauseDejeuner_Minimum = TableauData(13)
                Duree_PauseDejeuner_Maximum = TableauData(14)
                Heure_Debut_PauseDejeuner = TableauData(15)
                Heure_Fin_PauseDejeuner = TableauData(16)
                SiHoraireVariable = TableauData(17)
                Heure_Debut_PlageVariable = TableauData(18)
                Heure_Debut_PlageFixe = TableauData(19)
                Heure_Fin_PlageVariable = TableauData(20)
                Heure_Fin_PlageFixe = TableauData(21)
                If TableauData.Count > 23 Then
                    SiPasdeCredit_Heures = TableauData(22)
                    SiRAZ_CompteurDC = TableauData(23)
                End If
                WsFicheLue = New StringBuilder
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI

                TsDate_de_fin = ""
            End Set
        End Property

        Public Property VDate_de_Fin() As String
            Get
                Return TsDate_de_fin
            End Get
            Set(ByVal value As String)
                TsDate_de_fin = value
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal NumPointdeVue As Integer, ByVal NumObjet As Integer)
            Me.New()
            WsNumPVue = NumPointdeVue
            WsNumObjet = NumObjet
        End Sub

    End Class

    Public Class TRA_ACCORD_RTT
        Inherits ACCORD_RTT

        Public Sub New()
            MyBase.New(VI.PointdeVue.PVueCycle, 3)
        End Sub

        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "TRA_ACCORD_RTT"
            End Get
        End Property
    End Class

    Public Class ETA_ACCORD_RTT
        Inherits ACCORD_RTT

        Public Sub New()
            MyBase.New(VI.PointdeVue.PVueEtablissement, 3)
        End Sub

        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "ETA_ACCORD_RTT"
            End Get
        End Property
    End Class

    Public Class PRE_ACCORD_RTT
        Inherits ACCORD_RTT

        Public Sub New()
            MyBase.New(VI.PointdeVue.PVuePresences, 2)
        End Sub

        Public Overloads Shared ReadOnly Property ID_Sgbd As String
            Get
                Return "PRE_ACCORD_RTT"
            End Get
        End Property
    End Class

End Namespace

