﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ORG_DIRECTION
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsNumeroetRue As String
        Private WsNomResponsable As String
        Private WsCodeDepartement As Integer
        Private WsCodeInseeCommune As Integer
        Private WsNumeroOrdre As Integer
        Private WsOrdrePresentation As Integer
        Private WsTelephone As String
        Private WsReferenceComptable As String
        Private WsMnemonique As String
        Private WsObservation As String
        Private WsComplementAdresse As String
        Private WsCodePostal As String
        Private WsVille As String
        Private WsFax As String
        Private WsEtablissement As String
        Private WsNomDelegataire As String
        Private WsPrenomDelegataire As String
        Private WsPwDelegataire As String



        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ORG_DIRECTION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueDirection
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Denomination() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Numero_et_NomdelaRue() As String
            Get
                Return WsNumeroetRue
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNumeroetRue = value
                    Case Else
                        WsNumeroetRue = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property NomduResponsable() As String
            Get
                Return WsNomResponsable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsNomResponsable = value
                    Case Else
                        WsNomResponsable = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property CodeduDepartement() As Integer
            Get
                Return WsCodeDepartement
            End Get
            Set(ByVal value As Integer)
                WsCodeDepartement = value
            End Set
        End Property

        Public Property CodeInseeCommune() As Integer
            Get
                Return WsCodeInseeCommune
            End Get
            Set(ByVal value As Integer)
                WsCodeInseeCommune = value
            End Set
        End Property

        Public Property NumerodOrdre() As Integer
            Get
                Return WsNumeroOrdre
            End Get
            Set(ByVal value As Integer)
                WsNumeroOrdre = value
            End Set
        End Property

        Public Property OrdredePresentation() As Integer
            Get
                Return WsOrdrePresentation
            End Get
            Set(ByVal value As Integer)
                WsOrdrePresentation = value
            End Set
        End Property

        Public Property Telephone() As String
            Get
                Return WsTelephone
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsTelephone = value
                    Case Else
                        WsTelephone = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property ReferenceComptable() As String
            Get
                Return WsReferenceComptable
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsReferenceComptable = value
                    Case Else
                        WsReferenceComptable = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Property Mnemonique() As String
            Get
                Return WsMnemonique
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsMnemonique = value
                    Case Else
                        WsMnemonique = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Observation() As String
            Get
                Return WsObservation
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsObservation = value
                    Case Else
                        WsObservation = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property ComplementAdresse() As String
            Get
                Return WsComplementAdresse
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsComplementAdresse = value
                    Case Else
                        WsComplementAdresse = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property Code_Postal() As String
            Get
                Return WsCodePostal
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 6
                        WsCodePostal = value
                    Case Else
                        WsCodePostal = Strings.Left(value, 6)
                End Select
            End Set
        End Property

        Public Property Ville() As String
            Get
                Return WsVille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsVille = value
                    Case Else
                        WsVille = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Fax() As String
            Get
                Return WsFax
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 25
                        WsFax = value
                    Case Else
                        WsFax = Strings.Left(value, 25)
                End Select
            End Set
        End Property

        Public Property Etablissement() As String
            Get
                Return WsEtablissement
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsEtablissement = value
                    Case Else
                        WsEtablissement = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property NomduDelegataire() As String
            Get
                Return WsNomDelegataire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsNomDelegataire = value
                    Case Else
                        WsNomDelegataire = Strings.Left(value, 35)
                End Select
            End Set
        End Property

        Public Property PrenomduDelegataire() As String
            Get
                Return WsPrenomDelegataire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 35
                        WsPrenomDelegataire = value
                    Case Else
                        WsPrenomDelegataire = Strings.Left(value, 35)
                End Select
            End Set
        End Property

        Public Property MotdePasseduDelegataire() As String
            Get
                Return WsPwDelegataire
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 48
                        WsPwDelegataire = value
                    Case Else
                        WsPwDelegataire = Strings.Left(value, 48)
                End Select
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Denomination & VI.Tild)
                Chaine.Append(Numero_et_NomdelaRue & VI.Tild)
                Chaine.Append(NomduResponsable & VI.Tild)
                Chaine.Append(CodeduDepartement.ToString & VI.Tild)
                Chaine.Append(CodeInseeCommune.ToString & VI.Tild)
                Chaine.Append(NumerodOrdre.ToString & VI.Tild)
                Chaine.Append(OrdredePresentation.ToString & VI.Tild)
                Chaine.Append(Telephone & VI.Tild)
                Chaine.Append(ReferenceComptable & VI.Tild)
                Chaine.Append(Mnemonique & VI.Tild)
                Chaine.Append(Observation & VI.Tild)
                Chaine.Append(ComplementAdresse & VI.Tild)
                Chaine.Append(Code_Postal & VI.Tild)
                Chaine.Append(Ville & VI.Tild)
                Chaine.Append(Fax & VI.Tild)
                Chaine.Append(Etablissement & VI.Tild)
                Chaine.Append(NomduDelegataire & VI.Tild)
                Chaine.Append(PrenomduDelegataire & VI.Tild)
                Chaine.Append(MotdePasseduDelegataire)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 20 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Denomination = TableauData(1)
                Numero_et_NomdelaRue = TableauData(2)
                NomduResponsable = TableauData(3)
                If TableauData(4) <> "" Then CodeduDepartement = CInt(TableauData(4))
                If TableauData(5) <> "" Then CodeInseeCommune = CInt(TableauData(5))
                If TableauData(6) <> "" Then NumerodOrdre = CInt(TableauData(6))
                If TableauData(7) <> "" Then OrdredePresentation = CInt(TableauData(7))
                Telephone = TableauData(8)
                ReferenceComptable = TableauData(9)
                Mnemonique = TableauData(10)
                Observation = TableauData(11)
                ComplementAdresse = TableauData(12)
                Code_Postal = TableauData(13)
                Ville = TableauData(14)
                Fax = TableauData(15)
                Etablissement = TableauData(16)
                NomduDelegataire = TableauData(17)
                PrenomduDelegataire = TableauData(18)
                MotdePasseduDelegataire = TableauData(19)

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace

