﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class PAI_MIS_ETRANGER
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsCodePays As String
        Private WsLibellePays As String
        Private WsCodeMonnaie As String
        Private WsLibelleMonnaie As String
        Private WsGroupe_I As Double = 0
        Private WsGroupe_II As Double = 0
        Private WsGroupe_III As Double = 0
        Private WsGroupe_IV As Double = 0
        Private WsGroupe_V As Double = 0
        Private WsTauxChangeEuro As Double = 0
        Private WsZoneEuro As Integer = 0
        Private WsTauxChancellerie As Double = 0


        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "PAI_MIS_ETRANGER"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVuePaie
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 22
            End Get
        End Property

        Public Property CodeduPays() As String
            Get
                Return WsCodePays
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsCodePays = value
                    Case Else
                        WsCodePays = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property LibelleduPays() As String
            Get
                Return WsLibellePays
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsLibellePays = value
                    Case Else
                        WsLibellePays = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property CodedelaMonnaie() As String
            Get
                Return WsCodeMonnaie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2
                        WsCodeMonnaie = value
                    Case Else
                        WsCodeMonnaie = Strings.Left(value, 2)
                End Select
            End Set
        End Property

        Public Property LibelledelaMOnnaie() As String
            Get
                Return WsLibelleMonnaie
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 50
                        WsLibelleMonnaie = value
                    Case Else
                        WsLibelleMonnaie = Strings.Left(value, 50)
                End Select
            End Set
        End Property

        Public Property Groupe_I() As Double
            Get
                Return WsGroupe_I
            End Get
            Set(ByVal value As Double)
                WsGroupe_I = value
            End Set
        End Property

        Public Property Groupe_II() As Double
            Get
                Return WsGroupe_II
            End Get
            Set(ByVal value As Double)
                WsGroupe_II = value
            End Set
        End Property

        Public Property Groupe_III() As Double
            Get
                Return WsGroupe_III
            End Get
            Set(ByVal value As Double)
                WsGroupe_III = value
            End Set
        End Property

        Public Property Groupe_IV() As Double
            Get
                Return WsGroupe_IV
            End Get
            Set(ByVal value As Double)
                WsGroupe_IV = value
            End Set
        End Property

        Public Property Groupe_V() As Double
            Get
                Return WsGroupe_V
            End Get
            Set(ByVal value As Double)
                WsGroupe_V = value
            End Set
        End Property

        Public Property TauxdeChange_Euro() As Double
            Get
                Return WsTauxChangeEuro
            End Get
            Set(ByVal value As Double)
                WsTauxChangeEuro = value
            End Set
        End Property

        Public Property SiZoneEuro() As Integer
            Get
                Return WsZoneEuro
            End Get
            Set(ByVal value As Integer)
                WsZoneEuro = value
            End Set
        End Property

        Public Property Taux_Chancellerie() As Double
            Get
                Return WsTauxChancellerie
            End Get
            Set(ByVal value As Double)
                WsTauxChancellerie = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(CodeduPays & VI.Tild)
                Chaine.Append(LibelleduPays & VI.Tild)
                Chaine.Append(CodedelaMonnaie & VI.Tild)
                Chaine.Append(LibelledelaMOnnaie & VI.Tild)
                Chaine.Append(Groupe_I.ToString & VI.Tild)
                Chaine.Append(Groupe_II.ToString & VI.Tild)
                Chaine.Append(Groupe_III.ToString & VI.Tild)
                Chaine.Append(Groupe_IV.ToString & VI.Tild)
                Chaine.Append(Groupe_V.ToString & VI.Tild)
                Chaine.Append(TauxdeChange_Euro.ToString & VI.Tild)
                Chaine.Append(SiZoneEuro.ToString & VI.Tild)
                Chaine.Append(Taux_Chancellerie.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 13 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                CodeduPays = TableauData(1)
                LibelleduPays = TableauData(2)
                CodedelaMonnaie = TableauData(3)
                LibelledelaMOnnaie = TableauData(4)
                If TableauData(5) <> "" Then Groupe_I = VirRhFonction.ConversionDouble(TableauData(5))
                If TableauData(6) <> "" Then Groupe_II = VirRhFonction.ConversionDouble(TableauData(6))
                If TableauData(7) <> "" Then Groupe_III = VirRhFonction.ConversionDouble(TableauData(7))
                If TableauData(8) <> "" Then Groupe_IV = VirRhFonction.ConversionDouble(TableauData(8))
                If TableauData(9) <> "" Then Groupe_V = VirRhFonction.ConversionDouble(TableauData(9))
                If TableauData(10) <> "" Then TauxdeChange_Euro = VirRhFonction.ConversionDouble(TableauData(10))
                If TableauData(11) <> "" Then SiZoneEuro = CInt(TableauData(11))
                If TableauData(12) <> "" Then Taux_Chancellerie = VirRhFonction.ConversionDouble(TableauData(12))

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

    End Class
End Namespace


