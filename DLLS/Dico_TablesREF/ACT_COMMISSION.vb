﻿Option Strict Off
Option Explicit On
Option Compare Text

Imports VI = Virtualia.Systeme.Constantes
Imports System.Text
Imports Virtualia.Systeme.MetaModele

Namespace ShemaREF
    Public Class ACT_COMMISSION
        Inherits VIR_FICHE

        Private WsFicheLue As StringBuilder
        '
        Private WsIntitule As String
        Private WsMission As String
        Private WsNumero As String
        Private WsType As String
        Private WsSecteurActivite As String
        Private WsOrganismeCompetent As String
        Private WsTextesApplicables As String
        Private WsNoEtNomRue As String
        Private WsComplementAdresse As String
        Private WsCodePostal As String
        Private WsVille As String
        Private WsTelephone As String
        Private WsFax As String
        Private WsMailContact As String
        Private WsAutreTelephone As String
        Private WsSiteWeb As String
        Private WsNoOrdreTableau As Integer
        Private WsNoOrdreInterne As Integer
        '
        Private WsListeFonctions As List(Of ACT_FONCTIONS) = New List(Of ACT_FONCTIONS)()

        Public Overrides ReadOnly Property ID_Sgbd() As String
            Get
                Return "ACT_COMMISSION"
            End Get
        End Property

        Public Overrides ReadOnly Property PointdeVue() As Integer
            Get
                Return VI.PointdeVue.PVueCommission
            End Get
        End Property

        Public Overrides ReadOnly Property NumeroObjet() As Integer
            Get
                Return 1
            End Get
        End Property

        Public Property Intitule() As String
            Get
                Return WsIntitule
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsIntitule = value
                    Case Else
                        WsIntitule = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property Mission() As String
            Get
                Return WsMission
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 2000
                        WsMission = value
                    Case Else
                        WsMission = Strings.Left(value, 2000)
                End Select
            End Set
        End Property

        Public Property Numero() As String
            Get
                Return WsNumero
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 10
                        WsNumero = value
                    Case Else
                        WsNumero = Strings.Left(value, 10)
                End Select
            End Set
        End Property

        Public Property Type_Commission() As String
            Get
                Return WsType
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsType = value
                    Case Else
                        WsType = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Secteur_Activite() As String
            Get
                Return WsSecteurActivite
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsSecteurActivite = value
                    Case Else
                        WsSecteurActivite = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Organisme_Competent() As String
            Get
                Return WsOrganismeCompetent
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 80
                        WsOrganismeCompetent = value
                    Case Else
                        WsOrganismeCompetent = Strings.Left(value, 80)
                End Select
            End Set
        End Property

        Public Property Textes_Applicables() As String
            Get
                Return WsTextesApplicables
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 500
                        WsTextesApplicables = value
                    Case Else
                        WsTextesApplicables = Strings.Left(value, 500)
                End Select
            End Set
        End Property

        Public Property NumeroEtNomRue() As String
            Get
                Return WsNoEtNomRue
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsNoEtNomRue = value
                    Case Else
                        WsNoEtNomRue = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property ComplementAdresse() As String
            Get
                Return WsComplementAdresse
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 120
                        WsComplementAdresse = value
                    Case Else
                        WsComplementAdresse = Strings.Left(value, 120)
                End Select
            End Set
        End Property

        Public Property CodePostal() As String
            Get
                Return WsCodePostal
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 6
                        WsCodePostal = value
                    Case Else
                        WsCodePostal = Strings.Left(value, 6)
                End Select
            End Set
        End Property

        Public Property Ville() As String
            Get
                Return WsVille
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 60
                        WsVille = value
                    Case Else
                        WsVille = Strings.Left(value, 60)
                End Select
            End Set
        End Property

        Public Property Telephone() As String
            Get
                Return WsTelephone
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsTelephone = value
                    Case Else
                        WsTelephone = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Fax() As String
            Get
                Return WsFax
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsFax = value
                    Case Else
                        WsFax = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property Mail_Contact() As String
            Get
                Return WsMailContact
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 200
                        WsMailContact = value
                    Case Else
                        WsMailContact = Strings.Left(value, 200)
                End Select
            End Set
        End Property

        Public Property AutreTelephone() As String
            Get
                Return WsAutreTelephone
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 20
                        WsAutreTelephone = value
                    Case Else
                        WsAutreTelephone = Strings.Left(value, 20)
                End Select
            End Set
        End Property

        Public Property SiteWeb() As String
            Get
                Return WsSiteWeb
            End Get
            Set(ByVal value As String)
                Select Case value.Length
                    Case Is <= 250
                        WsSiteWeb = value
                    Case Else
                        WsSiteWeb = Strings.Left(value, 250)
                End Select
            End Set
        End Property

        Public Property OrdreTriTableau() As Integer
            Get
                Return WsNoOrdreTableau
            End Get
            Set(ByVal value As Integer)
                WsNoOrdreTableau = value
            End Set
        End Property

        Public Property OrdreTriInterne() As Integer
            Get
                Return WsNoOrdreInterne
            End Get
            Set(ByVal value As Integer)
                WsNoOrdreInterne = value
            End Set
        End Property

        Public Overrides Property ContenuTable() As String
            Get
                Dim Chaine As StringBuilder

                Chaine = New StringBuilder

                Chaine.Append(VI.Tild)
                Chaine.Append(Intitule & VI.Tild)
                Chaine.Append(Mission & VI.Tild)
                Chaine.Append(Numero & VI.Tild)
                Chaine.Append(Type_Commission & VI.Tild)
                Chaine.Append(Secteur_Activite & VI.Tild)
                Chaine.Append(Organisme_Competent & VI.Tild)
                Chaine.Append(Textes_Applicables & VI.Tild)
                Chaine.Append(NumeroEtNomRue & VI.Tild)
                Chaine.Append(ComplementAdresse & VI.Tild)
                Chaine.Append(CodePostal & VI.Tild)
                Chaine.Append(Ville & VI.Tild)
                Chaine.Append(Telephone & VI.Tild)
                Chaine.Append(Fax & VI.Tild)
                Chaine.Append(Mail_Contact & VI.Tild)
                Chaine.Append(AutreTelephone & VI.Tild)
                Chaine.Append(SiteWeb & VI.Tild)
                Chaine.Append(OrdreTriTableau.ToString & VI.Tild)
                Chaine.Append(OrdreTriInterne.ToString)

                Return Chaine.ToString
            End Get
            Set(ByVal value As String)
                If value = "" Then
                    Exit Property
                End If
                Dim TableauData(0) As String
                Dim IndiceI As Integer

                TableauData = Strings.Split(value, VI.Tild, -1)
                If TableauData.Count < 19 Then
                    Exit Property
                End If

                Ide_Dossier = CInt(TableauData(0))
                Intitule = TableauData(1)
                Mission = TableauData(2)
                Numero = TableauData(3)
                Type_Commission = TableauData(4)
                Secteur_Activite = TableauData(5)
                Organisme_Competent = TableauData(6)
                Textes_Applicables = TableauData(7)
                NumeroEtNomRue = TableauData(8)
                ComplementAdresse = TableauData(9)
                CodePostal = TableauData(10)
                Ville = TableauData(11)
                Telephone = TableauData(12)
                Fax = TableauData(13)
                Mail_Contact = TableauData(14)
                AutreTelephone = TableauData(15)
                SiteWeb = TableauData(16)
                If TableauData(17) <> "" Then OrdreTriTableau = CInt(TableauData(17))
                If TableauData(18) <> "" Then OrdreTriInterne = CInt(TableauData(18))

                WsFicheLue = New StringBuilder
                WsFicheLue.Append(VI.Tild)
                For IndiceI = 1 To TableauData.Count - 1
                    WsFicheLue.Append(TableauData(IndiceI) & VI.Tild)
                Next IndiceI
            End Set
        End Property

        Public Overrides ReadOnly Property FicheLue() As String
            Get
                If WsFicheLue Is Nothing Then
                    Return ""
                Else
                    Return WsFicheLue.ToString
                End If
            End Get
        End Property

        Public ReadOnly Property SiInterne() As Boolean
            Get
                Select Case Strings.InStr(Type_Commission, "interne")
                    Case Is > 0
                        Return True
                    Case Else
                        Return False
                End Select
            End Get
        End Property

        Public ReadOnly Property ListeDesFonctions() As List(Of ACT_FONCTIONS)
            Get
                Return WsListeFonctions
            End Get
        End Property

        Public ReadOnly Property ListeTrieeDesFonctions() As List(Of ACT_FONCTIONS)
            Get
                If WsListeFonctions Is Nothing Then
                    Return Nothing
                End If
                Return (From instance In WsListeFonctions Select instance _
                                  Order By instance.Rang Ascending, instance.Nature Ascending).ToList
            End Get
        End Property

        Public WriteOnly Property FonctionLiee() As String 'Cf WebServiceAffectation Conseil d'Etat
            Set(ByVal value As String)
                Dim ItemFonction As ACT_FONCTIONS = New ACT_FONCTIONS(value)
                WsListeFonctions.Add(ItemFonction)
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Function Ajouter_Fonction(ByVal Fiche As ACT_FONCTIONS) As Integer

            If WsListeFonctions Is Nothing Then
                WsListeFonctions = New List(Of ACT_FONCTIONS)
            End If
            WsListeFonctions.Add(Fiche)
            Return WsListeFonctions.Count

        End Function

    End Class
End Namespace


