﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele
Imports Virtualia.TablesObjet.ShemaPER

Namespace Individuel
    Public Class DossierIndividu
        Inherits List(Of VIR_FICHE)
        Private WsParent As Individuel.EnsembleDossiers
        Private Frm As Virtualia.Net.FrmInfosPersonnelles
        Private WsIdentifiant As Integer
        Private WebFct As Virtualia.Net.Controles.WebFonctions
        Private obwc As Virtualia.Net.Controles.ObjetWebControle
        Private WsNomStateCache As String = "VControle"
        Private WsCtl_Cache As Virtualia.Net.VCaches.CacheWebControle
        Private ModeFonctionnement As String = System.Configuration.ConfigurationManager.AppSettings("ModeVirtualia")
        Private WsClientSessionWcf As Virtualia.Net.WebService.ServiceWcfSessions = Nothing
        Private WsClientServiceWcf As Virtualia.Net.WebService.ServiceWcfServeur = Nothing
        Private WsClientServiceWeb As Virtualia.Net.WebService.ServiceWebServeur = Nothing
        Private WsRhModele As Donnees.ModeleRH
        Private WsNomUtilisateurBd As String = ""
        Private WsNoBd As Integer
        Private WsIde As Integer
        Private WsSuccess As Integer = 0
        Private WsficheCivil As String = ""
        Private WsficheIdentite As String = ""
        Private WsficheDomicile As String = ""
        Private WsficheBanque As String = ""
        Private WsficheGrade As String = ""
        Private WsfichePrevenir As String = ""
        Private WsficheConjoint As String = ""
        Private WsficheStatut As String = ""
        Private WsfichePosition As String = ""
        Private WsficheAffectation As String = ""
        Private WsficheAdressePro As String = ""
        Private WsficheEtablissement As String = ""
        Private chaineposition As String = ""
        Private WslistEnfant As List(Of String)
        Private WslistDiplome As List(Of String)
        Private WslistQualif As List(Of String)
        Private WslistLangue As List(Of String)
        Private WsDNEnfant As String
        Private WsNomEnfant As String
        Private WsPrenomEnfant As String
        Private WsLieuNEnfant As String
        Private WsNvDossierClicked As Boolean = False
        Private WsControleDossier As Boolean = False
        Private WsEncours As Boolean = False
        Private WsAnnuler As Boolean = False
        Private WsArmoireclicked As Integer = 0
        Private WsDossierEntier As Boolean = False
        Private WsDossierClick As Boolean = False
        Private WsButtonClicked As Integer = 0
        Private WsCacheCivil As List(Of String)
        Private WsCacheIdentite As List(Of String)
        Private WsVirFiche As VIR_FICHE = Nothing
        Dim table As New DataTable()
        Dim WsNom_NV As String = ""
        Dim WsPrenom_NV As String = ""
        Dim WsDN_NV As String = ""
        Dim WsNSS_NV As String = ""
        Dim WsVue As Integer = 99
        Dim WsErreur As Boolean = False
        Dim WsPlusDossier As Boolean = False
        Private WsEnsembleDossier As Virtualia.Net.Individuel.EnsembleDossiers
        Private WsEnsemble As Virtualia.Net.Individuel.EnsembleDossiers
        Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates
        Private WsRhFct As Virtualia.Systeme.Fonctions.Generales


        Public Property Identifiant() As Integer
            Get
                Return WsIdentifiant
            End Get
            Set(ByVal value As Integer)
                Dim Lst As List(Of VIR_FICHE)
                Me.Clear()
                WsIdentifiant = value
                Lst = WsParent.PointeurGlobal.VirServiceServeur.LectureDossier_ToFiches(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, WsIdentifiant, False, WsParent.ObjetsDllIndividu)
                If Lst IsNot Nothing Then
                    Me.AddRange(Lst)
                    Call Complementer_Fiches()
                End If
            End Set
        End Property

        Public Property DossierEtatCivil As VIR_FICHE
            Get
                Return WsVirFiche
            End Get
            Set(value As VIR_FICHE)
                WsVirFiche = value
            End Set
        End Property

        Public ReadOnly Property FicheEtatCivil As PER_ETATCIVIL
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches("PER_ETATCIVIL")
                If LstFiches Is Nothing Or LstFiches.Count = 0 Then
                    Return Nothing
                End If
                Return CType(LstFiches.Item(0), PER_ETATCIVIL)
            End Get
        End Property

        Public Property ChaineEtatCivilSaisi As String
            Get

                If WsficheCivil Is Nothing Then
                    Return Nothing
                End If
                Return WsficheCivil
            End Get
            Set(value As String)
                WsficheCivil = value
            End Set
        End Property

        Public ReadOnly Property FichePieceIdentite As PER_PIECE_IDENTITE
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches("PER_PIECE_IDENTITE")
                If LstFiches Is Nothing Or LstFiches.Count = 0 Then
                    Return Nothing
                End If
                Return CType(LstFiches.Item(0), PER_PIECE_IDENTITE)
            End Get
        End Property

        Public Property ChaineIdentiteSaisi As String
            Get

                If WsficheIdentite Is Nothing Then
                    Return Nothing
                End If
                Return WsficheIdentite
            End Get
            Set(value As String)
                WsficheIdentite = value
            End Set
        End Property

        Public Property ListeEnfants As List(Of PER_ENFANT)
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                Dim LstEnfants As List(Of PER_ENFANT)
                Dim FicheBase As PER_ENFANT
                LstFiches = Liste_Fiches("PER_ENFANT")
                If LstFiches Is Nothing Then
                    Return Nothing
                End If
                '** Spécifique - Traitement des jumeaux 
                LstEnfants = New List(Of PER_ENFANT)
                For Each Element In LstFiches
                    FicheBase = New PER_ENFANT
                    FicheBase.Ide_Dossier = Element.Ide_Dossier
                    FicheBase.Date_de_naissance = CType(Element, PER_ENFANT).Date_de_naissance
                    FicheBase.Nom = CType(Element, PER_ENFANT).Nom
                    FicheBase.Prenom = CType(Element, PER_ENFANT).Prenom
                    FicheBase.Sexe = CType(Element, PER_ENFANT).Sexe
                    FicheBase.A_charge = CType(Element, PER_ENFANT).A_charge
                    LstEnfants.Add(FicheBase)
                    If CType(Element, PER_ENFANT).Jumeau <> "" Then
                        FicheBase = New PER_ENFANT
                        FicheBase.Ide_Dossier = Element.Ide_Dossier
                        FicheBase.Date_de_naissance = CType(Element, PER_ENFANT).Date_de_naissance
                        FicheBase.Nom = CType(Element, PER_ENFANT).Nom
                        FicheBase.Prenom = CType(Element, PER_ENFANT).Jumeau
                        FicheBase.Sexe = CType(Element, PER_ENFANT).Sexe_du_jumeau
                        FicheBase.A_charge = CType(Element, PER_ENFANT).A_charge
                        LstEnfants.Add(FicheBase)
                    End If
                    If CType(Element, PER_ENFANT).Triple <> "" Then
                        FicheBase = New PER_ENFANT
                        FicheBase.Ide_Dossier = Element.Ide_Dossier
                        FicheBase.Date_de_naissance = CType(Element, PER_ENFANT).Date_de_naissance
                        FicheBase.Nom = CType(Element, PER_ENFANT).Nom
                        FicheBase.Prenom = CType(Element, PER_ENFANT).Triple
                        FicheBase.Sexe = CType(Element, PER_ENFANT).Sexe_du_Triple
                        FicheBase.A_charge = CType(Element, PER_ENFANT).A_charge
                        LstEnfants.Add(FicheBase)
                    End If
                Next
                Return WsParent.PointeurGlobal.VirRhFonction.ConvertisseurListeFiches(Of PER_ENFANT)(LstFiches)
            End Get
            Set(value As List(Of PER_ENFANT))
                Dim LstEnfants As List(Of PER_ENFANT)
                Dim LstFiches As List(Of VIR_FICHE)
                Dim FichePER As PER_ENFANT = Nothing
                Dim IndiceI As Integer

                If value Is Nothing Then
                    Exit Property
                End If
                '** Spécifique - Traitement des jumeaux 
                LstEnfants = New List(Of PER_ENFANT)
                For Each Element In value
                    If FichePER Is Nothing OrElse Element.Date_de_naissance <> FichePER.Date_de_naissance Then
                        FichePER = New PER_ENFANT
                        FichePER.Ide_Dossier = Element.Ide_Dossier
                        FichePER.Date_de_naissance = Element.Date_de_naissance
                        FichePER.Nom = Element.Nom
                        FichePER.Prenom = Element.Prenom
                        FichePER.Sexe = Element.Sexe
                        FichePER.A_charge = Element.A_charge
                        LstEnfants.Add(FichePER)
                    ElseIf FichePER.Jumeau = "" Then
                        FichePER.Jumeau = Element.Prenom
                        FichePER.Sexe_du_jumeau = Element.Sexe
                    ElseIf FichePER.Triple = "" Then
                        FichePER.Triple = Element.Prenom
                        FichePER.Sexe_du_Triple = Element.Sexe
                    End If
                Next
                LstFiches = Liste_Fiches("PER_ENFANT")
                If LstFiches Is Nothing Then
                    For Each FichePER In LstEnfants
                        Me.Add(FichePER)
                    Next
                    Exit Property
                End If
                For IndiceI = 0 To LstEnfants.Count - 1
                    FichePER = LstEnfants(IndiceI)
                    If Fiche_ParRang(VI.ObjetPer.ObaEnfant, IndiceI) Is Nothing Then
                        Me.Add(FichePER)
                    Else
                        FichePER = CType(Fiche_ParRang(VI.ObjetPer.ObaEnfant, IndiceI), PER_ENFANT)
                        FichePER.Date_de_naissance = LstEnfants.Item(IndiceI).Date_de_naissance
                        FichePER.Nom = LstEnfants.Item(IndiceI).Nom
                        FichePER.Prenom = LstEnfants.Item(IndiceI).Prenom
                        FichePER.Sexe = LstEnfants.Item(IndiceI).Sexe
                        FichePER.A_charge = LstEnfants.Item(IndiceI).A_charge
                        'FichePER.Jumeau = LstEnfants.Item(IndiceI).Prenom
                        'FichePER.Sexe_du_jumeau = LstEnfants.Item(IndiceI).Sexe
                        'FichePER.Triple = LstEnfants.Item(IndiceI).Prenom
                        'FichePER.Sexe_du_Triple = LstEnfants.Item(IndiceI).Sexe
                    End If
                Next
            End Set
        End Property

        Public Property ListChaineEnfantSaisi As List(Of String)
            Get

                If WslistEnfant Is Nothing Then
                    Return Nothing
                End If
                Return WslistEnfant
            End Get
            Set(value As List(Of String))
                WslistEnfant = value
            End Set
        End Property


        Public ReadOnly Property DonneeLue(ByVal NumObjet As Integer, ByVal NumInfo As Integer) As String
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches(NumObjet)
                If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                    Return ""
                End If
                Return LstFiches.Item(0).V_TableauData(NumInfo).ToString
            End Get
        End Property
        Public Property V_NvDossier_Clicked As Boolean
            Get
                Return WsDossierEntier
            End Get
            Set(value As Boolean)
                WsDossierEntier = value
            End Set
        End Property
        Public Property V_NvDossier As Boolean
            Get
                Return WsDossierClick
            End Get
            Set(value As Boolean)
                WsDossierClick = value
            End Set
        End Property

        Public Property V_ArmoireClicked As Integer
            Get
                Return WsArmoireclicked
            End Get
            Set(value As Integer)
                WsArmoireclicked = value
            End Set
        End Property


        Public Property Dossier_entier_clicked As Boolean
            Get
                Return WsNvDossierClicked
            End Get
            Set(value As Boolean)
                WsNvDossierClicked = value
            End Set
        End Property


        Public Property SiCreationOK() As Boolean = False

        Public ReadOnly Property DonneeLue(ByVal NumObjet As Integer, ByVal NumInfo As Integer, ByVal NoRang As Integer) As String
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches(NumObjet)
                If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                    Return ""
                End If
                If NoRang > LstFiches.Count - 1 Then
                    Return ""
                End If
                Return LstFiches.Item(NoRang).V_TableauData(NumInfo).ToString
            End Get
        End Property

        Public WriteOnly Property TableauMaj(ByVal NumObjet As Integer, ByVal NumInfo As Integer) As String
            Set(value As String)
                Dim LstFiches As List(Of VIR_FICHE)
                Dim FicheMAJ As VIR_FICHE
                Dim TableauData As ArrayList
                Dim TableauMaj As ArrayList
                Dim IndiceI As Integer

                LstFiches = Liste_Fiches(NumObjet)
                If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                    Dim ConstructeurFiche As VConstructeur = New VConstructeur(WsParent.PointeurGlobal.VirModele.InstanceProduit.ClefModele)
                    FicheMAJ = ConstructeurFiche.V_NouvelleFiche(NumObjet, WsIdentifiant)
                    Me.Add(FicheMAJ)
                Else
                    FicheMAJ = LstFiches.Item(0)
                End If
                TableauData = FicheMAJ.V_TableauData
                TableauData(NumInfo) = value
                Select Case WsParent.NatureObjet(VI.PointdeVue.PVueApplicatif, NumObjet, NumInfo)
                    Case VI.TypeObjet.ObjetDate, VI.TypeObjet.ObjetDateRang, VI.TypeObjet.ObjetHeure
                        FicheMAJ.V_TableauData = TableauData
                    Case Else
                        TableauMaj = New ArrayList
                        For IndiceI = 1 To TableauData.Count - 1
                            TableauMaj.Add(TableauData(IndiceI))
                        Next IndiceI
                        FicheMAJ.V_TableauData = TableauMaj
                End Select
            End Set
        End Property

        Public WriteOnly Property TableauMaj(ByVal NumObjet As Integer, ByVal NumInfo As Integer, ByVal NoRang As Integer) As String
            Set(value As String)
                Dim LstFiches As List(Of VIR_FICHE)
                Dim FicheMAJ As VIR_FICHE
                Dim ConstructeurFiche As VConstructeur
                Dim TableauData As ArrayList

                LstFiches = Liste_Fiches(NumObjet)
                If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                    ConstructeurFiche = New VConstructeur(WsParent.PointeurGlobal.VirModele.InstanceProduit.ClefModele)
                    FicheMAJ = ConstructeurFiche.V_NouvelleFiche(NumObjet, WsIdentifiant)
                    Me.Add(FicheMAJ)
                ElseIf NoRang > LstFiches.Count - 1 Then
                    ConstructeurFiche = New VConstructeur(WsParent.PointeurGlobal.VirModele.InstanceProduit.ClefModele)
                    FicheMAJ = ConstructeurFiche.V_NouvelleFiche(NumObjet, WsIdentifiant)
                    Me.Add(FicheMAJ)
                Else
                    FicheMAJ = LstFiches.Item(NoRang)
                End If
                TableauData = FicheMAJ.V_TableauData
                TableauData(NumInfo) = value
                FicheMAJ.V_TableauData = TableauData
            End Set
        End Property


        Public WriteOnly Property TableauMaj_un(ByVal NumObjet As Integer, ByVal NumInfo As Integer, ByVal NoRang As Integer) As String
            Set(value As String)
                Dim LstFiches As List(Of VIR_FICHE)
                Dim FicheMAJ As VIR_FICHE
                Dim ConstructeurFiche As VConstructeur
                Dim TableauData As ArrayList

                LstFiches = Liste_Fiches(NumObjet)
                If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                    ConstructeurFiche = New VConstructeur(WsParent.PointeurGlobal.VirModele.InstanceProduit.ClefModele)
                    FicheMAJ = ConstructeurFiche.V_NouvelleFiche(NumObjet, WsIdentifiant)
                    Me.Add(FicheMAJ)
                ElseIf NoRang > LstFiches.Count - 1 Then
                    ConstructeurFiche = New VConstructeur(WsParent.PointeurGlobal.VirModele.InstanceProduit.ClefModele)
                    FicheMAJ = ConstructeurFiche.V_NouvelleFiche(NumObjet, WsIdentifiant)
                    Me.Add(FicheMAJ)
                Else
                    FicheMAJ = LstFiches.Item(NoRang)
                End If
                TableauData = FicheMAJ.V_TableauData
                TableauData.RemoveAt(0)
                TableauData(NumInfo) = value
                FicheMAJ.V_TableauData = TableauData
            End Set
        End Property

        Public ReadOnly Property Liste_Fiches(ByVal NomTable As String) As List(Of VIR_FICHE)
            Get
                Return (From Fiche In Me Select Fiche Where Fiche.GetType.Name = NomTable).ToList
            End Get
        End Property

        Public ReadOnly Property Liste_Fiches(ByVal NumObjet As Integer) As List(Of VIR_FICHE)
            Get
                Return (From Fiche In Me Select Fiche Where Fiche.NumeroObjet = NumObjet).ToList
            End Get
        End Property

        Public ReadOnly Property Fiche_ParRang(ByVal NumObjet As Integer, ByVal NoRang As Integer) As VIR_FICHE
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches(NumObjet)
                If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                    Return Nothing
                End If
                If NoRang > LstFiches.Count - 1 Then
                    Return Nothing
                End If
                Return LstFiches.Item(NoRang)
            End Get
        End Property


        Public ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
            Get
                If WebFct Is Nothing Then
                    WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
                End If
                Return WebFct
            End Get
        End Property

        Private Sub Complementer_Fiches()
            Dim LstFiches As List(Of VIR_FICHE)
            Dim IndiceO As Integer
            Dim VdateFin As String

            LstFiches = Liste_Fiches("PER_ETATCIVIL")
            If LstFiches Is Nothing Then
                Exit Sub
            End If

            '** Positionnemnt des dates de fin virtuelles
            For IndiceO = 0 To Me.Count - 1
                Select Case Me.Item(IndiceO).GetType.Name
                    Case "PER_COLLECTIVITE"
                        If CType(Me.Item(IndiceO), PER_COLLECTIVITE).VDate_de_Fin = "" Then
                            Select Case Me.Item(IndiceO - 1).GetType.Name
                                Case Is = "PER_COLLECTIVITE"
                                    VdateFin = WsParent.PointeurGlobal.VirRhDates.CalcDateMoinsJour(CType(Me.Item(IndiceO - 1), PER_COLLECTIVITE).Date_d_effet, "0", "1")
                                    CType(Me.Item(IndiceO), PER_COLLECTIVITE).VDate_de_Fin = VdateFin
                            End Select
                        End If
                    Case "PER_SOCIETE"
                        If CType(Me.Item(IndiceO), PER_SOCIETE).VDate_de_Fin = "" Then
                            Select Case Me.Item(IndiceO - 1).GetType.Name
                                Case Is = "PER_SOCIETE"
                                    VdateFin = WsParent.PointeurGlobal.VirRhDates.CalcDateMoinsJour(CType(Me.Item(IndiceO - 1), PER_SOCIETE).Date_d_effet, "0", "1")
                                    CType(Me.Item(IndiceO), PER_SOCIETE).VDate_de_Fin = VdateFin
                            End Select
                        End If
                    Case "PER_STATUT"
                        If CType(Me.Item(IndiceO), PER_STATUT).VDate_de_Fin = "" Then
                            Select Case Me.Item(IndiceO - 1).GetType.Name
                                Case Is = "PER_STATUT"
                                    VdateFin = WsParent.PointeurGlobal.VirRhDates.CalcDateMoinsJour(CType(Me.Item(IndiceO - 1), PER_STATUT).Date_de_Valeur, "0", "1")
                                    CType(Me.Item(IndiceO), PER_STATUT).VDate_de_Fin = VdateFin
                            End Select
                        End If
                    Case "PER_CONTRAT"
                        If CType(Me.Item(IndiceO), PER_CONTRAT).VDate_de_Fin = "" Then
                            Select Case Me.Item(IndiceO - 1).GetType.Name
                                Case Is = "PER_CONTRAT"
                                    VdateFin = WsParent.PointeurGlobal.VirRhDates.CalcDateMoinsJour(CType(Me.Item(IndiceO), PER_CONTRAT).Date_de_Valeur, "0", "1")
                                    CType(Me.Item(IndiceO), PER_CONTRAT).VDate_de_Fin = VdateFin
                            End Select
                        End If
                    Case "PER_POSITION"
                        If CType(Me.Item(IndiceO), PER_POSITION).VDate_de_Fin = "" Then
                            Select Case Me.Item(IndiceO - 1).GetType.Name
                                Case Is = "PER_POSITION"
                                    VdateFin = WsParent.PointeurGlobal.VirRhDates.CalcDateMoinsJour(CType(Me.Item(IndiceO - 1), PER_POSITION).Date_de_Valeur, "0", "1")
                                    CType(Me.Item(IndiceO), PER_POSITION).VDate_de_Fin = VdateFin
                            End Select
                        End If
                    Case "PER_ACTIVITE"
                        If CType(Me.Item(IndiceO), PER_ACTIVITE).VDate_de_fin = "" Then
                            Select Case Me.Item(IndiceO - 1).GetType.Name
                                Case Is = "PER_ACTIVITE"
                                    VdateFin = WsParent.PointeurGlobal.VirRhDates.CalcDateMoinsJour(CType(Me.Item(IndiceO - 1), PER_ACTIVITE).Date_de_Valeur, "0", "1")
                                    CType(Me.Item(IndiceO), PER_ACTIVITE).VDate_de_fin = VdateFin
                            End Select
                        End If

                End Select
            Next IndiceO

        End Sub




        Public ReadOnly Property FicheDomicile As PER_DOMICILE
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches("PER_DOMICILE")
                If LstFiches.Count = 0 Then
                    Return Nothing
                End If
                Return CType(LstFiches.Item(0), PER_DOMICILE)
            End Get
        End Property

        Public Property ChaineDomicileSaisi As String
            Get

                If WsficheDomicile Is Nothing Then
                    Return Nothing
                End If
                Return WsficheDomicile
            End Get
            Set(value As String)
                WsficheDomicile = value
            End Set
        End Property

        Public ReadOnly Property FicheBanque As PER_BANQUE
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches("PER_BANQUE")
                If LstFiches.Count = 0 Then
                    Return Nothing
                End If
                Return CType(LstFiches.Item(0), PER_BANQUE)
            End Get
        End Property

        Public Property ChaineBanqueSaisi As String
            Get

                If WsficheBanque Is Nothing Then
                    Return Nothing
                End If
                Return WsficheBanque
            End Get
            Set(value As String)
                WsficheBanque = value
            End Set
        End Property


        Public ReadOnly Property FichePrevenir As PER_APREVENIR
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches("PER_APREVENIR")
                If LstFiches.Count = 0 Then
                    Return Nothing
                End If
                Return CType(LstFiches.Item(0), PER_APREVENIR)
            End Get
        End Property

        Public Property ChainePrevenirSaisi As String
            Get

                If WsfichePrevenir Is Nothing Then
                    Return Nothing
                End If
                Return WsfichePrevenir
            End Get
            Set(value As String)
                WsfichePrevenir = value
            End Set
        End Property



        Public ReadOnly Property FicheConjoint As PER_CONJOINT
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches("PER_CONJOINT")
                If LstFiches.Count = 0 Then
                    Return Nothing
                End If
                Return CType(LstFiches.Item(0), PER_CONJOINT)
            End Get
        End Property
        Public Property ChaineConjointSaisi As String
            Get

                If WsficheConjoint Is Nothing Then
                    Return Nothing
                End If
                Return WsficheConjoint
            End Get
            Set(value As String)
                WsficheConjoint = value
            End Set
        End Property


        Public ReadOnly Property FicheStatut As PER_STATUT
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches("PER_STATUT")
                If LstFiches.Count = 0 Then
                    Return Nothing
                End If
                Return CType(LstFiches.Item(0), PER_STATUT)
            End Get
        End Property

        Public Property ChaineStatutSaisi As String
            Get

                If WsficheStatut Is Nothing Then
                    Return Nothing
                End If
                Return WsficheStatut
            End Get
            Set(value As String)
                WsficheStatut = value
            End Set
        End Property

        Public ReadOnly Property FichePosition As PER_POSITION
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches("PER_POSITION")
                If LstFiches.Count = 0 Then
                    Return Nothing
                End If
                Return CType(LstFiches.Item(0), PER_POSITION)
            End Get
        End Property
        Public Property ChainePositionSaisi As String
            Get

                If WsfichePosition Is Nothing Then
                    Return Nothing
                End If
                Return WsfichePosition
            End Get
            Set(value As String)
                WsfichePosition = value
            End Set
        End Property


        Public ReadOnly Property FicheGrade As PER_GRADE
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches("PER_GRADE")
                If LstFiches.Count = 0 Then
                    Return Nothing
                End If
                Return CType(LstFiches.Item(0), PER_GRADE)
            End Get
        End Property
        Public Property ChaineGradeSaisi As String
            Get

                If WsficheGrade Is Nothing Then
                    Return Nothing
                End If
                Return WsficheGrade
            End Get
            Set(value As String)
                WsficheGrade = value
            End Set
        End Property

        Public Property ListChaineDiplomeSaisi As List(Of String)
            Get

                If WslistDiplome Is Nothing Then
                    Return Nothing
                End If
                Return WslistDiplome
            End Get
            Set(value As List(Of String))
                WslistDiplome = value
            End Set

        End Property

        Public Property ListChaineQualifSaisi As List(Of String)
            Get

                If WslistQualif Is Nothing Then
                    Return Nothing
                End If
                Return WslistQualif
            End Get
            Set(value As List(Of String))
                WslistQualif = value
            End Set

        End Property

        Public Property ListChaineLangueSaisi As List(Of String)
            Get

                If WslistLangue Is Nothing Then
                    Return Nothing
                End If
                Return WslistLangue
            End Get
            Set(value As List(Of String))
                WslistLangue = value
            End Set

        End Property

        Public ReadOnly Property FicheAffectation As PER_AFFECTATION
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches("PER_AFFECTATION")
                If LstFiches.Count = 0 Then
                    Return Nothing
                End If
                Return CType(LstFiches.Item(0), PER_AFFECTATION)
            End Get
        End Property
        Public Property ChaineAffectationSaisi As String
            Get

                If WsficheAffectation Is Nothing Then
                    Return Nothing
                End If
                Return WsficheAffectation
            End Get
            Set(value As String)
                WsficheAffectation = value
            End Set
        End Property

        Public ReadOnly Property FicheAdressePro As PER_ADRESSEPRO
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches("PER_ADRESSEPRO")
                If LstFiches.Count = 0 Then
                    Return Nothing
                End If
                Return CType(LstFiches.Item(0), PER_ADRESSEPRO)
            End Get
        End Property
        Public Property ChaineAdresseProSaisi As String
            Get

                If WsficheAdressePro Is Nothing Then
                    Return Nothing
                End If
                Return WsficheAdressePro
            End Get
            Set(value As String)
                WsficheAdressePro = value
            End Set
        End Property

        Public ReadOnly Property FicheEtablissement As PER_COLLECTIVITE
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches("PER_COLLECTIVITE")
                If LstFiches.Count = 0 Then
                    Return Nothing
                End If
                Return CType(LstFiches.Item(0), PER_COLLECTIVITE)
            End Get
        End Property
        Public Property ChaineEtablissementSaisi As String
            Get

                If WsficheEtablissement Is Nothing Then
                    Return Nothing
                End If
                Return WsficheEtablissement
            End Get
            Set(value As String)
                WsficheEtablissement = value
            End Set
        End Property

        Public ReadOnly Property getIdNouveau As Integer
            Get
                WsIde = WsParent.PointeurGlobal.NouvelIdentifiant("PER_ETATCIVIL")
                Return WsIde
            End Get
        End Property

        Public ReadOnly Property getIdMAJ As Integer
            Get
                WsIde = WsParent.PointeurGlobal.NouvelIdentifiant("PER_ETATCIVIL") - 1
                Return WsIde
            End Get
        End Property



        Public ReadOnly Property FicheRefExterne As PER_REFEXTERNE
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches("PER_REFEXTERNE")
                If LstFiches.Count = 0 Then
                    Return Nothing
                End If
                Return CType(LstFiches.Item(0), PER_REFEXTERNE)
            End Get
        End Property


        Public ReadOnly Property FicheCollectivite As PER_COLLECTIVITE
            Get
                Dim LstFiches As List(Of VIR_FICHE)
                LstFiches = Liste_Fiches("PER_COLLECTIVITE")
                If LstFiches.Count = 0 Then
                    Return Nothing
                End If
                Return CType(LstFiches.Item(0), PER_COLLECTIVITE)
            End Get
        End Property


        Public Function MettreAJour(ByVal id As Integer, ByVal CodeMaj As String) As Boolean

            Dim DossierPER As Virtualia.Ressources.Datas.ObjetDossierPER = Nothing
            Dim ChaineLue As String
            Dim ChaineMaj As String
            Dim SiOK As Boolean
            Dim IndiceI As Integer

            If FicheEtatCivil Is Nothing Then
                Return False
            End If


            ChaineLue = ""
            Dim lst_enfant = Liste_Fiches("PER_ENFANT")
            Dim lst_diplome = Liste_Fiches("PER_DIPLOME")
            Dim lst_qualif = Liste_Fiches("PER_BREVETPRO")
            Dim lst_langue = Liste_Fiches("PER_SPECIALITE")




            If FicheEtatCivil IsNot Nothing Then
                ChaineMaj = FicheEtatCivil.FicheLue
                ChaineLue = ChaineEtatCivilSaisi
                SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaCivil, id, CodeMaj, ChaineLue, ChaineMaj)
                If SiOK Then
                    ChaineEtatCivilSaisi = ChaineMaj
                End If
            End If

            If FichePieceIdentite IsNot Nothing Then
                ChaineMaj = FichePieceIdentite.FicheLue
                ChaineLue = ChaineIdentiteSaisi
                If ChaineLue = "" Then
                    CodeMaj = "C"
                Else
                    CodeMaj = "M"
                End If
                SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaEtranger, id, CodeMaj, ChaineLue, ChaineMaj)
                If SiOK Then
                    ChaineIdentiteSaisi = ChaineMaj
                End If
            End If

            If FicheDomicile IsNot Nothing Then
                ChaineMaj = FicheDomicile.FicheLue
                ChaineLue = ChaineDomicileSaisi
                If ChaineLue = "" Then
                    CodeMaj = "C"
                Else
                    CodeMaj = "M"
                End If
                SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaAdresse, id, CodeMaj, ChaineLue, ChaineMaj)
                If SiOK Then
                    ChaineDomicileSaisi = ChaineMaj
                End If
            End If

            If FicheBanque IsNot Nothing Then
                'Dim iban = FicheBanque.IBAN
                'iban = iban.Replace(" ", "")
                'If iban.Substring(0, 2).ToUpper = "FR" Then
                '    FicheBanque.Code_Banque = iban.Substring(4, 5)
                '    FicheBanque.Code_Guichet = iban.Substring(11, 3)
                '    FicheBanque.NumeroduCompte = iban.Substring(14, 11)
                '    FicheBanque.CleRIB = iban.Substring(25, 2)
                'End If
                ChaineMaj = FicheBanque.FicheLue
                ChaineLue = ChaineBanqueSaisi
                If ChaineLue = "" Then
                    CodeMaj = "C"
                Else
                    CodeMaj = "M"
                End If
                SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaBanque, id, CodeMaj, ChaineLue, ChaineMaj)
                If SiOK Then
                    ChaineBanqueSaisi = ChaineMaj
                End If
            End If

            If FichePrevenir IsNot Nothing Then
                ChaineMaj = FichePrevenir.FicheLue
                ChaineLue = ChainePrevenirSaisi
                If ChaineLue = "" Then
                    CodeMaj = "C"
                Else
                    CodeMaj = "M"
                End If
                SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaPrevenir, id, CodeMaj, ChaineLue, ChaineMaj)
                If SiOK Then
                    ChainePrevenirSaisi = ChaineMaj
                End If
            End If

            If FicheConjoint IsNot Nothing Then
                ChaineMaj = FicheConjoint.FicheLue
                ChaineLue = ChaineConjointSaisi
                If ChaineLue = "" Then
                    CodeMaj = "C"
                Else
                    CodeMaj = "M"
                End If
                SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaConjoint, id, CodeMaj, ChaineLue, ChaineMaj)
                If SiOK Then
                    ChaineConjointSaisi = ChaineMaj
                End If
            End If

            If FicheStatut IsNot Nothing Then
                ChaineMaj = FicheStatut.FicheLue
                ChaineLue = ChaineStatutSaisi
                If ChaineLue = "" Then
                    CodeMaj = "C"
                Else
                    CodeMaj = "M"
                End If
                SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaStatut, id, CodeMaj, ChaineLue, ChaineMaj)
                If SiOK Then
                    ChaineStatutSaisi = ChaineMaj
                End If
            End If

            If FicheGrade IsNot Nothing Then
                ChaineMaj = FicheGrade.FicheLue
                ChaineLue = ChaineGradeSaisi
                If ChaineLue = "" Then
                    CodeMaj = "C"
                Else
                    CodeMaj = "M"
                End If
                SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaGrade, id, CodeMaj, ChaineLue, ChaineMaj)
                If SiOK Then
                    ChaineGradeSaisi = ChaineMaj
                End If
            End If

            If FichePosition IsNot Nothing Then
                'LstFiches = DossierPER.V_ListeDesFiches(13)
                'If LstFiches IsNot Nothing Then
                '    For Each fiche As VIR_FICHE In LstFiches
                '        ChaineLue = fiche.FicheLue
                '        ChaineMaj = FichePosition.FicheLue
                '        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaActivite, Ide, CodeMaj, ChaineLue, ChaineMaj)
                '    Next

                'End If

                ChaineMaj = FichePosition.FicheLue
                ChaineLue = ChainePositionSaisi
                If ChaineLue = "" Then
                    CodeMaj = "C"
                Else
                    CodeMaj = "M"
                End If
                SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaActivite, id, CodeMaj, ChaineLue, ChaineMaj)
                If SiOK Then
                    ChainePositionSaisi = ChaineMaj
                End If
            End If

            If FicheAffectation IsNot Nothing Then
                ChaineMaj = FicheAffectation.FicheLue
                ChaineLue = ChaineAffectationSaisi
                If ChaineLue = "" Then
                    CodeMaj = "C"
                Else
                    CodeMaj = "M"
                End If
                SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaOrganigramme, id, CodeMaj, ChaineLue, ChaineMaj)
                If SiOK Then
                    ChaineAffectationSaisi = ChaineMaj
                End If
            End If

            If FicheAdressePro IsNot Nothing Then
                ChaineMaj = FicheAdressePro.FicheLue
                ChaineLue = ChaineAdresseProSaisi
                If ChaineLue = "" Then
                    CodeMaj = "C"
                Else
                    CodeMaj = "M"
                End If
                SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaAdrPro, id, CodeMaj, ChaineLue, ChaineMaj)
                If SiOK Then
                    ChaineAdresseProSaisi = ChaineMaj
                End If
            End If

            If FicheEtablissement IsNot Nothing Then
                ChaineMaj = FicheEtablissement.FicheLue
                ChaineLue = ChaineEtablissementSaisi
                If ChaineLue = "" Then
                    CodeMaj = "C"
                Else
                    CodeMaj = "M"
                End If
                SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaSociete, id, CodeMaj, ChaineLue, ChaineMaj)
                If SiOK Then
                    ChaineEtablissementSaisi = ChaineMaj
                End If
            End If

            If lst_enfant.Count <> 0 Then
                Dim listtemp = New List(Of String)
                For IndiceI = 0 To lst_enfant.Count - 1
                    If lst_enfant.Item(IndiceI) IsNot Nothing Then
                        ChaineMaj = lst_enfant.Item(IndiceI).FicheLue
                        If ListChaineEnfantSaisi IsNot Nothing AndAlso ListChaineEnfantSaisi.Count > 0 Then
                            Try
                                ChaineLue = ListChaineEnfantSaisi(IndiceI)
                            Catch ex As Exception
                                ChaineLue = ""
                            End Try

                        Else
                            ChaineLue = ""
                        End If
                        If ChaineLue = "" Then
                            CodeMaj = "C"
                        Else
                            CodeMaj = "M"
                        End If
                        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaEnfant, id, CodeMaj, ChaineLue, ChaineMaj)
                        If SiOK Then
                            listtemp.Add(ChaineMaj)
                        End If
                    End If
                Next
                ListChaineEnfantSaisi = listtemp


            End If

            If lst_diplome.Count <> 0 Then
                Dim listtemp = New List(Of String)
                For IndiceI = 0 To lst_diplome.Count - 1
                    If lst_diplome.Item(IndiceI) IsNot Nothing Then
                        ChaineMaj = lst_diplome.Item(IndiceI).FicheLue
                        If ListChaineDiplomeSaisi IsNot Nothing AndAlso ListChaineDiplomeSaisi.Count > 0 Then
                            Try
                                ChaineLue = ListChaineDiplomeSaisi(IndiceI)
                            Catch ex As Exception
                                ChaineLue = ""
                            End Try

                        Else
                            ChaineLue = ""
                        End If
                        If ChaineLue = "" Then
                            CodeMaj = "C"
                        Else
                            CodeMaj = "M"
                        End If
                        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaDiplome, id, CodeMaj, ChaineLue, ChaineMaj)
                        If SiOK Then
                            listtemp.Add(ChaineMaj)
                        End If
                    End If
                Next
                ListChaineDiplomeSaisi = listtemp
            End If

            If lst_qualif.Count <> 0 Then
                Dim listtemp = New List(Of String)
                For IndiceI = 0 To lst_qualif.Count - 1
                    If lst_qualif.Item(IndiceI) IsNot Nothing Then
                        ChaineMaj = lst_qualif.Item(IndiceI).FicheLue
                        If ListChaineQualifSaisi IsNot Nothing AndAlso ListChaineQualifSaisi.Count > 0 Then
                            Try
                                ChaineLue = ListChaineQualifSaisi(IndiceI)
                            Catch ex As Exception
                                ChaineLue = ""
                            End Try

                        Else
                            ChaineLue = ""
                        End If
                        If ChaineLue = "" Then
                            CodeMaj = "C"
                        Else
                            CodeMaj = "M"
                        End If
                        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaCompetence, id, CodeMaj, ChaineLue, ChaineMaj)
                        If SiOK Then
                            listtemp.Add(ChaineMaj)
                        End If
                    End If
                Next
                ListChaineQualifSaisi = listtemp
            End If


            If lst_langue.Count <> 0 Then
                Dim listtemp = New List(Of String)
                For IndiceI = 0 To lst_langue.Count - 1
                    If lst_langue.Item(IndiceI) IsNot Nothing Then
                        ChaineMaj = lst_langue.Item(IndiceI).FicheLue
                        If ListChaineLangueSaisi IsNot Nothing AndAlso ListChaineLangueSaisi.Count > 0 Then
                            Try
                                ChaineLue = ListChaineLangueSaisi(IndiceI)
                            Catch ex As Exception
                                ChaineLue = ""
                            End Try

                        Else
                            ChaineLue = ""
                        End If
                        If ChaineLue = "" Then
                            CodeMaj = "C"
                        Else
                            CodeMaj = "M"
                        End If
                        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaSpecialite, id, CodeMaj, ChaineLue, ChaineMaj)
                        If SiOK Then
                            listtemp.Add(ChaineMaj)
                        End If
                    End If
                Next
                ListChaineLangueSaisi = listtemp
            End If
            Return SiOK
        End Function
        ''**** FIN AKR

        Public Property buttonclicked As Integer
            Get
                'WsButtonClicked = WsCompteurButtonClicked
                Return WsButtonClicked
            End Get
            Set(value As Integer)
                WsButtonClicked = value
            End Set
        End Property

        Public Property DateNaissanceEnfant As String
            Get
                'WsButtonClicked = WsCompteurButtonClicked
                Return WsDNEnfant
            End Get
            Set(value As String)
                WsDNEnfant = value
            End Set
        End Property


        Public Property NomEnfant As String
            Get
                'WsButtonClicked = WsCompteurButtonClicked
                Return WsNomEnfant
            End Get
            Set(value As String)
                WsNomEnfant = value
            End Set
        End Property

        Public Property PrenomEnfant As String
            Get
                'WsButtonClicked = WsCompteurButtonClicked
                Return WsPrenomEnfant
            End Get
            Set(value As String)
                WsPrenomEnfant = value
            End Set
        End Property

        Public Property LieuNEnfant As String
            Get
                'WsButtonClicked = WsCompteurButtonClicked
                Return WsLieuNEnfant
            End Get
            Set(value As String)
                WsLieuNEnfant = value
            End Set
        End Property


        Public Property tableenfant As DataTable
            Get

                Return table
            End Get
            Set(value As DataTable)
                table = value
            End Set
        End Property

        Public Sub New(ByVal Host As Individuel.EnsembleDossiers)
            WsParent = Host
        End Sub



        Public Function Maj_PER(ByVal Fiche As VIR_FICHE, ByVal noObjet As VI.ObjetPer, ByVal chaine As String, ByVal id As Integer) As Boolean
            Dim ChaineLue As String
            Dim ChaineMaj As String
            Dim SiOK As Boolean

            If Fiche IsNot Nothing Then
                ChaineMaj = Fiche.FicheLue
                ChaineLue = chaine
                SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, noObjet, id, "M", ChaineLue, ChaineMaj)
                If SiOK = True Then
                    chaine = ChaineMaj
                End If
            End If
            Return SiOK
        End Function

        Public Property CacheCivilReplace As List(Of String)
            Get
                Return WsCacheCivil
            End Get
            Set(value As List(Of String))
                WsCacheCivil = value
            End Set
        End Property

        Public Property CacheIdentiteReplace As List(Of String)
            Get
                Return WsCacheIdentite
            End Get
            Set(value As List(Of String))
                WsCacheIdentite = value
            End Set
        End Property


        Public Property Nom_NV As String
            Get

                If WsNom_NV Is Nothing Then
                    Return Nothing
                End If
                Return WsNom_NV
            End Get
            Set(value As String)
                WsNom_NV = value
            End Set
        End Property

        Public Property Prenom_NV As String
            Get

                If WsPrenom_NV Is Nothing Then
                    Return Nothing
                End If
                Return WsPrenom_NV
            End Get
            Set(value As String)
                WsPrenom_NV = value
            End Set
        End Property


        Public Property DN_NV As String
            Get

                If WsDN_NV Is Nothing Then
                    Return Nothing
                End If
                Return WsDN_NV
            End Get
            Set(value As String)
                WsDN_NV = value
            End Set
        End Property


        Public Property NSS_NV As String
            Get

                If WsNSS_NV Is Nothing Then
                    Return Nothing
                End If
                Return WsNSS_NV
            End Get
            Set(value As String)
                WsNSS_NV = value
            End Set
        End Property

        Public Property V_WsDossierPer As Virtualia.Net.Individuel.EnsembleDossiers
            Get

                Return WsEnsembleDossier
            End Get
            Set(value As Virtualia.Net.Individuel.EnsembleDossiers)
                WsEnsembleDossier = value
            End Set
        End Property


        Public Property TableErreur As Boolean
            Get

                Return WsErreur
            End Get
            Set(value As Boolean)
                WsErreur = value
            End Set
        End Property
        Public Property ControleNvDossier As Boolean
            Get

                Return WsControleDossier
            End Get
            Set(value As Boolean)
                WsControleDossier = value
            End Set
        End Property
        Public Property V_plusDossier As Boolean
            Get

                Return WsPlusDossier
            End Get
            Set(value As Boolean)
                WsPlusDossier = value
            End Set
        End Property


        Public Property Success_NvDossier As Integer
            Get
                Return WsSuccess
            End Get
            Set(value As Integer)
                WsSuccess = value
            End Set
        End Property


        Public Property EnCours_Creation As Boolean
            Get
                Return WsEncours
            End Get
            Set(value As Boolean)
                WsEncours = value
            End Set
        End Property
        Public Property creation_annuler As Boolean
            Get
                Return WsAnnuler
            End Get
            Set(value As Boolean)
                WsAnnuler = value
            End Set
        End Property


        Public Property V_vue As Integer
            Get

                Return WsVue
            End Get
            Set(value As Integer)
                WsVue = value
            End Set
        End Property


    End Class



    'Public Function MettreAJour(ByVal id As Integer) As Boolean

    '    Dim DossierPER As Virtualia.Ressources.Datas.ObjetDossierPER = Nothing
    '    Dim ChaineLue As String
    '    Dim ChaineMaj As String
    '    Dim SiOK As Boolean
    '    Dim IndiceI As Integer
    '    Dim CodeMaj = "C"

    '    If FicheEtatCivil Is Nothing Then
    '        Return False
    '    End If


    '    ChaineLue = ""
    '    Dim lst_enfant = Liste_Fiches("PER_ENFANT")
    '    Dim lst_diplome = Liste_Fiches("PER_DIPLOME")
    '    Dim lst_qualif = Liste_Fiches("PER_BREVETPRO")
    '    Dim lst_langue = Liste_Fiches("PER_LANGUE_PRATIQUEE")









    '    If FicheEtatCivil IsNot Nothing Then
    '        ChaineMaj = FicheEtatCivil.FicheLue
    '        ChaineLue = ChaineEtatCivilSaisi
    '        If id <> getIdNouveau Then
    '            CodeMaj = "M"
    '        Else
    '            If ChaineLue = "" Or V_NvDossier_Clicked = True Then
    '                CodeMaj = "C"
    '            Else
    '                CodeMaj = "M"
    '            End If
    '        End If
    '        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaCivil, id, CodeMaj, ChaineLue, ChaineMaj)
    '        If SiOK Then
    '            ChaineEtatCivilSaisi = ChaineMaj
    '        End If
    '    End If

    '    If FichePieceIdentite IsNot Nothing Then
    '        ChaineMaj = FichePieceIdentite.FicheLue
    '        ChaineLue = ChaineIdentiteSaisi
    '        If id <> getIdNouveau Then
    '            CodeMaj = "M"
    '        Else
    '            If ChaineLue = "" Or V_NvDossier_Clicked = True Then
    '                CodeMaj = "C"
    '            Else
    '                CodeMaj = "M"
    '            End If
    '        End If
    '        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaEtranger, id, CodeMaj, ChaineLue, ChaineMaj)
    '        If SiOK Then
    '            ChaineIdentiteSaisi = ChaineMaj
    '        End If
    '    End If

    '    If FicheDomicile IsNot Nothing Then
    '        ChaineMaj = FicheDomicile.FicheLue
    '        ChaineLue = ChaineDomicileSaisi
    '        If ChaineLue = "" Or V_NvDossier_Clicked = True Then
    '            CodeMaj = "C"
    '        Else
    '            CodeMaj = "M"
    '        End If
    '        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaAdresse, id, CodeMaj, ChaineLue, ChaineMaj)
    '        If SiOK Then
    '            ChaineDomicileSaisi = ChaineMaj
    '        End If
    '    End If

    '    If FicheBanque IsNot Nothing Then
    '        'Dim iban = FicheBanque.IBAN
    '        'iban = iban.Replace(" ", "")
    '        'If iban.Substring(0, 2).ToUpper = "FR" Then
    '        '    FicheBanque.Code_Banque = iban.Substring(4, 5)
    '        '    FicheBanque.Code_Guichet = iban.Substring(11, 3)
    '        '    FicheBanque.NumeroduCompte = iban.Substring(14, 11)
    '        '    FicheBanque.CleRIB = iban.Substring(25, 2)
    '        'End If
    '        ChaineMaj = FicheBanque.FicheLue
    '        ChaineLue = ChaineBanqueSaisi
    '        If ChaineLue = "" Or V_NvDossier_Clicked = True Then
    '            CodeMaj = "C"
    '        Else
    '            CodeMaj = "M"
    '        End If
    '        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaBanque, id, CodeMaj, ChaineLue, ChaineMaj)
    '        If SiOK Then
    '            ChaineBanqueSaisi = ChaineMaj
    '        End If
    '    End If

    '    If FichePrevenir IsNot Nothing Then
    '        ChaineMaj = FichePrevenir.FicheLue
    '        ChaineLue = ChainePrevenirSaisi
    '        If ChaineLue = "" Or V_NvDossier_Clicked = True Then
    '            CodeMaj = "C"
    '        Else
    '            CodeMaj = "M"
    '        End If
    '        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaPrevenir, id, CodeMaj, ChaineLue, ChaineMaj)
    '        If SiOK Then
    '            ChainePrevenirSaisi = ChaineMaj
    '        End If
    '    End If

    '    If FicheConjoint IsNot Nothing Then
    '        ChaineMaj = FicheConjoint.FicheLue
    '        ChaineLue = ChaineConjointSaisi
    '        If ChaineLue = "" Or V_NvDossier_Clicked = True Then
    '            CodeMaj = "C"
    '        Else
    '            CodeMaj = "M"
    '        End If
    '        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaConjoint, id, CodeMaj, ChaineLue, ChaineMaj)
    '        If SiOK Then
    '            ChaineConjointSaisi = ChaineMaj
    '        End If
    '    End If

    '    If FicheStatut IsNot Nothing Then
    '        ChaineMaj = FicheStatut.FicheLue
    '        ChaineLue = ChaineStatutSaisi
    '        If ChaineLue = "" Or V_NvDossier_Clicked = True Then
    '            CodeMaj = "C"
    '        Else
    '            CodeMaj = "M"
    '        End If
    '        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaStatut, id, CodeMaj, ChaineLue, ChaineMaj)
    '        If SiOK Then
    '            ChaineStatutSaisi = ChaineMaj
    '        End If
    '    End If

    '    If FicheGrade IsNot Nothing Then
    '        ChaineMaj = FicheGrade.FicheLue
    '        ChaineLue = ChaineGradeSaisi
    '        If ChaineLue = "" Or V_NvDossier_Clicked = True Then
    '            CodeMaj = "C"
    '        Else
    '            CodeMaj = "M"
    '        End If
    '        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaGrade, id, CodeMaj, ChaineLue, ChaineMaj)
    '        If SiOK Then
    '            ChaineGradeSaisi = ChaineMaj
    '        End If
    '    End If

    '    If FichePosition IsNot Nothing Then
    '        'LstFiches = DossierPER.V_ListeDesFiches(13)
    '        'If LstFiches IsNot Nothing Then
    '        '    For Each fiche As VIR_FICHE In LstFiches
    '        '        ChaineLue = fiche.FicheLue
    '        '        ChaineMaj = FichePosition.FicheLue
    '        '        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaActivite, Ide, CodeMaj, ChaineLue, ChaineMaj)
    '        '    Next

    '        'End If

    '        ChaineMaj = FichePosition.FicheLue
    '        ChaineLue = ChainePositionSaisi
    '        If ChaineLue = "" Or V_NvDossier_Clicked = True Then
    '            CodeMaj = "C"
    '        Else
    '            CodeMaj = "M"
    '        End If
    '        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaActivite, id, CodeMaj, ChaineLue, ChaineMaj)
    '        If SiOK Then
    '            ChainePositionSaisi = ChaineMaj
    '        End If
    '    End If

    '    If FicheAffectation IsNot Nothing Then
    '        ChaineMaj = FicheAffectation.FicheLue
    '        ChaineLue = ChaineAffectationSaisi
    '        If ChaineLue = "" Or V_NvDossier_Clicked = True Then
    '            CodeMaj = "C"
    '        Else
    '            CodeMaj = "M"
    '        End If
    '        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaOrganigramme, id, CodeMaj, ChaineLue, ChaineMaj)
    '        If SiOK Then
    '            ChaineAffectationSaisi = ChaineMaj
    '        End If
    '    End If

    '    If FicheAdressePro IsNot Nothing Then
    '        ChaineMaj = FicheAdressePro.FicheLue
    '        ChaineLue = ChaineAdresseProSaisi
    '        If ChaineLue = "" Or V_NvDossier_Clicked = True Then
    '            CodeMaj = "C"
    '        Else
    '            CodeMaj = "M"
    '        End If
    '        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaAdrPro, id, CodeMaj, ChaineLue, ChaineMaj)
    '        If SiOK Then
    '            ChaineAdresseProSaisi = ChaineMaj
    '        End If
    '    End If

    '    If FicheEtablissement IsNot Nothing Then
    '        ChaineMaj = FicheEtablissement.FicheLue
    '        ChaineLue = ChaineEtablissementSaisi
    '        If ChaineLue = "" Or V_NvDossier_Clicked = True Then
    '            CodeMaj = "C"
    '        Else
    '            CodeMaj = "M"
    '        End If
    '        SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaSociete, id, CodeMaj, ChaineLue, ChaineMaj)
    '        If SiOK Then
    '            ChaineEtablissementSaisi = ChaineMaj
    '        End If
    '    End If

    '    If lst_enfant.Count <> 0 Then
    '        Dim listtemp = New List(Of String)
    '        For IndiceI = 0 To lst_enfant.Count - 1
    '            If lst_enfant.Item(IndiceI) IsNot Nothing Then
    '                ChaineMaj = lst_enfant.Item(IndiceI).FicheLue
    '                If ListChaineEnfantSaisi IsNot Nothing AndAlso ListChaineEnfantSaisi.Count > 0 Then
    '                    Try
    '                        ChaineLue = ListChaineEnfantSaisi(IndiceI)
    '                    Catch ex As Exception
    '                        ChaineLue = ""
    '                    End Try

    '                Else
    '                    ChaineLue = ""
    '                End If
    '                If ChaineLue = "" Or V_NvDossier_Clicked = True Then
    '                    CodeMaj = "C"
    '                Else
    '                    CodeMaj = "M"
    '                End If
    '                SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaEnfant, id, CodeMaj, ChaineLue, ChaineMaj)
    '                If SiOK Then
    '                    listtemp.Add(ChaineMaj)
    '                End If
    '            End If
    '        Next
    '        ListChaineEnfantSaisi = listtemp


    '    End If

    '    If lst_diplome.Count <> 0 Then
    '        Dim listtemp = New List(Of String)
    '        For IndiceI = 0 To lst_diplome.Count - 1
    '            If lst_diplome.Item(IndiceI) IsNot Nothing Then
    '                ChaineMaj = lst_diplome.Item(IndiceI).FicheLue
    '                If ListChaineDiplomeSaisi IsNot Nothing AndAlso ListChaineDiplomeSaisi.Count > 0 Then
    '                    Try
    '                        ChaineLue = ListChaineDiplomeSaisi(IndiceI)
    '                    Catch ex As Exception
    '                        ChaineLue = ""
    '                    End Try

    '                Else
    '                    ChaineLue = ""
    '                End If
    '                If ChaineLue = "" Or V_NvDossier_Clicked = True Then
    '                    CodeMaj = "C"
    '                Else
    '                    CodeMaj = "M"
    '                End If
    '                SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaDiplome, id, CodeMaj, ChaineLue, ChaineMaj)
    '                If SiOK Then
    '                    listtemp.Add(ChaineMaj)
    '                End If
    '            End If
    '        Next
    '        ListChaineDiplomeSaisi = listtemp
    '    End If

    '    If lst_qualif.Count <> 0 Then
    '        Dim listtemp = New List(Of String)
    '        For IndiceI = 0 To lst_qualif.Count - 1
    '            If lst_qualif.Item(IndiceI) IsNot Nothing Then
    '                ChaineMaj = lst_qualif.Item(IndiceI).FicheLue
    '                If ListChaineQualifSaisi IsNot Nothing AndAlso ListChaineQualifSaisi.Count > 0 Then
    '                    Try
    '                        ChaineLue = ListChaineQualifSaisi(IndiceI)
    '                    Catch ex As Exception
    '                        ChaineLue = ""
    '                    End Try

    '                Else
    '                    ChaineLue = ""
    '                End If
    '                If ChaineLue = "" Or V_NvDossier_Clicked = True Then
    '                    CodeMaj = "C"
    '                Else
    '                    CodeMaj = "M"
    '                End If
    '                SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaCompetence, id, CodeMaj, ChaineLue, ChaineMaj)
    '                If SiOK Then
    '                    listtemp.Add(ChaineMaj)
    '                End If
    '            End If
    '        Next
    '        ListChaineQualifSaisi = listtemp
    '    End If


    '    If lst_langue.Count <> 0 Then
    '        Dim listtemp = New List(Of String)
    '        For IndiceI = 0 To lst_langue.Count - 1
    '            If lst_langue.Item(IndiceI) IsNot Nothing Then
    '                ChaineMaj = lst_langue.Item(IndiceI).FicheLue
    '                If ListChaineLangueSaisi IsNot Nothing AndAlso ListChaineLangueSaisi.Count > 0 Then
    '                    Try
    '                        ChaineLue = ListChaineLangueSaisi(IndiceI)
    '                    Catch ex As Exception
    '                        ChaineLue = ""
    '                    End Try

    '                Else
    '                    ChaineLue = ""
    '                End If
    '                If ChaineLue = "" Or V_NvDossier_Clicked = True Then
    '                    CodeMaj = "C"
    '                Else
    '                    CodeMaj = "M"
    '                End If
    '                SiOK = WsParent.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaLanguePratiquee, id, CodeMaj, ChaineLue, ChaineMaj)
    '                If SiOK Then
    '                    listtemp.Add(ChaineMaj)
    '                End If
    '            End If
    '        Next
    '        ListChaineLangueSaisi = listtemp
    '    End If
    '    Return SiOK
    'End Function


End Namespace